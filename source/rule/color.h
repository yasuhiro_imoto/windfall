#pragma once
#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#include <iosfwd>
#include <type_traits>

#include <boost/operators.hpp>

// ---------------------------------------------------------------------------
// 手番
// ---------------------------------------------------------------------------
struct Color : private boost::operators<Color> {
  enum Value {
    BLACK,  //<! 先手
    WHITE,  //<! 後手
    SIZE,
    ZERO = 0
  };

#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Color() : value(ZERO) {}
#pragma warning(pop)
  constexpr Color(const Value c) : value(c) {}

  constexpr operator int() const noexcept { return static_cast<int>(value); }

  constexpr bool operator==(const Color other) const noexcept {
    return value == other.value;
  }
  constexpr bool operator==(const Value other) const noexcept {
    return value == other;
  }
  constexpr bool operator!=(const Value other) const noexcept {
    return value != other;
  }

  constexpr std::underlying_type_t<Value> operator>>(
      const int n) const noexcept {
    return value >> n;
  }
  constexpr std::underlying_type_t<Value> operator<<(
      const int n) const noexcept {
    return value << n;
  }

  //! 相手番
  constexpr Color operator~() const noexcept {
    return Color(static_cast<Value>(value ^ 1));
  }

  constexpr bool ok() const noexcept { return value >= ZERO && value < SIZE; }

  constexpr Color& operator++() noexcept {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr Color operator++(int) noexcept {
    const Color tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }
  constexpr Color& operator--() noexcept {
    value = static_cast<Value>(value - 1);
    return *this;
  }
  constexpr Color operator--(int) noexcept {
    const Color tmp(value);
    value = static_cast<Value>(value - 1);
    return tmp;
  }

  Value value;
};

constexpr Color::Value operator~(const Color::Value color) noexcept {
  return static_cast<Color::Value>(color ^ 1);
}

//! range based forで使うためのColor型のオブジェクト
constexpr Color COLOR(Color::SIZE);
constexpr Color operator*(const Color color) noexcept { return color; }
constexpr Color begin(const Color color) noexcept { return Color::ZERO; }
constexpr Color end(const Color color) noexcept { return Color::SIZE; }

//! デバッグ用出力(USI形式ではない)
std::ostream& operator<<(std::ostream& os, const Color c);

#endif  // !COLOR_H_INCLUDED
