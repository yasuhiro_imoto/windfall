#pragma once
#ifndef ENTERING_OU_H_INCLUDED
#define ENTERING_OU_H_INCLUDED

#include <string>

//! 入玉のルール
enum class EnteringOuRule {
  NONE,      //!< 入玉ルールなし
  POINTS24,  //!< 24点法(31点以上で宣言勝ち)
  POINTS27,  //!< 27点法 = CSAルール
  TRY_RULE   //!< トライルール
};

EnteringOuRule ParseEnteringOuRule(const std::string& rule);
#endif  // !ENTERING_OU_H_INCLUDED
