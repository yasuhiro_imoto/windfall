// clang-format off
#include "stdafx.h"
// clang-format on

#include "piece.h"

std::ostream& operator<<(std::ostream& os, const Piece p) {
  // 駒打ちならば先頭に追加
  auto s = std::string((p & 32) ? "D" : "") +
           std::string(Piece::usi_piece).substr((p & 0x1F) * 2, 2);
  if (s.back() == ' ') {
    // triming
    s.pop_back();
  }

  os << s;
  return os;
}

/**
 * @brief 駒の種類
 * 
 * 空白の表現が半角と全角が1対2ということを前提にしている
*/
std::string usi_piece_kanji[] = {
    "___",                                                   //
    "+歩", "+香", "+桂", "+銀", "+角", "+飛", "+金", "+玉",  //
    "+と", "+杏", "+圭", "+全", "+馬", "+龍", "+菌", "+王",  //
    "-歩", "-香", "-桂", "-銀", "-角", "-飛", "-金", "-玉",  //
    "-と", "-杏", "-圭", "-全", "-馬", "-龍", "-菌", "-王"};

std::string Piece::pretty() const { return usi_piece_kanji[value]; }
