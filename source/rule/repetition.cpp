// clang-format off
#include "stdafx.h"
// clang-format on

#include "repetition.h"
#include "../utility/enum_cast.h"

constexpr char repetition_string_table[enum_cast<>(RepetitionState::SIZE)][9] =
    {"rep_none", "rep_win", "rep_lose", "rep_draw", "rep_sup", "rep_inf"};

std::ostream& operator<<(std::ostream& os, const RepetitionState repetition) {
  os << repetition_string_table[enum_cast<>(repetition)];
  return os;
}
