#pragma once
#ifndef REPETITION_H_INCLUDED
#define REPETITION_H_INCLUDED

#include <iosfwd>

enum class RepetitionState {
  NONE,  //!< 千日手ではない
  WIN,   //!< 連続王手の千日手で勝ち
  LOSE,  //!< 連続王手の千日手で負け
  DRAW,  //!< 普通の千日手
  SUPERIOR,  //!< 優等局面(盤上の駒が同じで手駒が相手より優れている)
  INFERIOR,  //!< 劣等局面(盤上の駒が同じで手駒が相手より劣っている)
  SIZE
};

std::ostream& operator<<(std::ostream& os, const RepetitionState repetition);
#endif  // !REPETITION_H_INCLUDED
