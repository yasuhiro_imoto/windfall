#include "stdafx.h"

#include "color.h"

std::ostream& operator<<(std::ostream& os, const Color c) {
  switch (c.value) {
    case Color::BLACK:
      os << "Color::BLACK";
      break;
    case Color::WHITE:
      os << "Color::WHITE";
      break;
    default:
      os << "Color(" << c.value << ')';
      break;
  }
  return os;
}
