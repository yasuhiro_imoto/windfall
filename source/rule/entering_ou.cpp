#include "stdafx.h"

#include "entering_ou.h"

#include "../utility/level_assert.h"

const std::array<std::string, 4> ek_rules = {"NoEnteringKing", "CSARule24",
                                             "CSARule27", "TryRule"};

EnteringOuRule ParseEnteringOuRule(const std::string& rule) {
  const auto it = boost::find(ek_rules, rule);
  if (it == ek_rules.end()) {
    ASSERT_LV1(false);
    return EnteringOuRule::NONE;
  }

  const auto i = std::distance(ek_rules.begin(), it);
  return static_cast<EnteringOuRule>(i);
}
