#pragma once
#ifndef PIECE_H_INCLUDED
#define PIECE_H_INCLUDED

#include <boost/operators.hpp>
#include <cstdint>
#include <iosfwd>
#include <string>

#include "../utility/enum_cast.h"
#include "../utility/level_assert.h"
#include "color.h"

// ---------------------------------------------------------------------------
// 駒
// ---------------------------------------------------------------------------
struct Piece : private boost::less_than_comparable<Piece> {
  enum Value : int32_t {
    EMPTY,
    FU,
    KY,
    KE,
    GI,
    KA,
    HI,
    KI,
    OU,
    TO,
    NY,
    NK,
    NG,
    UM,
    RY,
    QUEEN,  //!< 不使用

    BLACK_FU = FU,
    BLACK_KY,
    BLACK_KE,
    BLACK_GI,
    BLACK_KA,
    BLACK_HI,
    BLACK_KI,
    BLACK_OU,
    BLACK_TO,
    BLACK_NY,
    BLACK_NK,
    BLACK_NG,
    BLACK_UM,
    BLACK_RY,
    BLACK_QUEEN,

    WHITE_FU = 17,
    WHITE_KY,
    WHITE_KE,
    WHITE_GI,
    WHITE_KA,
    WHITE_HI,
    WHITE_KI,
    WHITE_OU,
    WHITE_TO,
    WHITE_NY,
    WHITE_NK,
    WHITE_NG,
    WHITE_UM,
    WHITE_RY,
    WHITE_QUEEN,

    SIZE,
    ZERO = 0,

    PROMOTION = 8,
    TYPE_SIZE = 16,  //!< 駒種の数(成りを含める)
    BLACK = 0,
    WHITE = 16,
    RAW_SIZE = 8,  //!< 非成駒の終端
#ifdef USE_DROPBIT_IN_STATS
    DROP = 32,
#else
    DROP = 0,
#endif  // USE_DROPBIT_IN_STATS

    HAND_ZERO = FU,  //!< 手駒の開始位置
    HAND_SIZE = OU,  //!<手駒になる駒種の終端

    // PositionでBitboardを取得するときの特殊な定数

    ALL_PIECES = 0,  //!< 駒がある升
    GOLDS = QUEEN,   //!< 金と同じ動きの駒集合
    HDK,             //!< Horse, Dragon, King
    BISHOP_HORSE,    //!<
    ROOK_DRAGON,     //!<
    SILVER_HDK,      //!<
    GOLDS_HDK,       //!<
    BB_SIZE,

    // 指し手生成(GeneratePieceMove =
    // GPM)でtemplateの引数として使うマーカー的な値
    // 変更する可能性があるのでユーザーは使わないでください。
    // 連続的な値にしておくことで、テーブルジャンプしやすくする。

    GPM_BR = 16,   //!< Bishop, Rook
    GPM_GBR = 17,  //!< Gold, Bishop, Rook
    GPM_GHD = 18,  //!< Gold, Horse, Dragon
    GPM_GHDK = 19  //!< Gold, Horse, Dragon, King
  };

#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Piece() : value(EMPTY) {}
#pragma warning(pop)
  constexpr Piece(const Value p) : value(p) {}
  explicit constexpr Piece(const int p) : value(static_cast<Value>(p)) {}
  constexpr Piece(const Color c, const Piece p)
      : value(static_cast<Value>((c << 4) + p.value)) {}
  constexpr Piece(const Color c, const Piece p, const bool promotion)
      : value(static_cast<Value>((c << 4) + p.value +
                                 (static_cast<int>(promotion) << 3))) {}

  constexpr bool operator<(const Piece rhs) const { return value < rhs.value; }
  constexpr bool operator==(const Piece rhs) const {
    return value == rhs.value;
  }

  constexpr Piece& operator++() {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr Piece operator++(int) {
    const Piece tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }

  constexpr operator int() const { return static_cast<int>(value); }

  //! USIプロトコルで駒を表す文字列
  /*! USE_DROPBIT_IN_STATSが定義されているなら駒打ちの駒なら先頭に"D"*/
  operator std::string() const {
    const auto index = (value & 31) * 2;
    return std::string((value & DROP) ? "D" : "") +
           std::string(usi_piece).substr(index, 2);
  }

  //! 駒が先後のどちらに属しているか
  constexpr Color color() const {
    static_assert(WHITE == 16 && enum_cast<>(Color::WHITE) == 1, "");
    static_assert(BLACK == 0 && enum_cast<>(Color::BLACK) == 0, "");
    return static_cast<Color::Value>((value & WHITE) >> 4);
  }

  //! 手番の情報を取り除く
  constexpr Piece type() const { return Piece(value & 15); }

  //! 成っていない駒
  /*! 手番も取り除く
   *  OUに対しての呼び出しはEMPTYが返るものとする */
  constexpr Piece raw_type() const { return Piece(value & 7); }

  constexpr bool HasLongEffect() const {
    return (type().value == KY) || (((value + 1) & 6) == 6);
  }

  constexpr bool ok() const { return value >= EMPTY && value < SIZE; }

  //! Pieceを綺麗に出力する(USI形式ではない)
  /*! 先手の駒は大文字、後手の駒は小文字、成り駒は先頭に+がつく
   *  "PRETTY_JP"をdefineしていれば、日本語文字での表示になる */
  std::string pretty() const;
  //! pretty()だと先手の駒を表示したときに先頭にスペースが入るので、
  //! それが嫌な場合はこちらを用いる
  std::string pretty2() const {
    ASSERT_LV1(color() == Color::BLACK);
    const auto s = pretty();
    return s.substr(1, s.length() - 1);
  }

  //! note: テンプレート関数の特殊化の都合でこの名前の方がいいかと思う
  Value value;

  static constexpr const char* usi_piece =
      ". P L N S B R G K +P+L+N+S+B+R+G+.p l n s b r g k +p+l+n+s+b+r+g+k";
};

constexpr bool operator==(const Piece lhs, const Piece::Value rhs) {
  return lhs.value == rhs;
}
constexpr bool operator==(const Piece::Value lhs, const Piece rhs) {
  return lhs == rhs.value;
}

constexpr bool operator<(const Piece lhs, const Piece::Value rhs) {
  return lhs.value < rhs;
}
constexpr bool operator<(const Piece::Value lhs, const Piece rhs) {
  return lhs < rhs.value;
}

constexpr bool operator<=(const Piece lhs, const Piece::Value rhs) {
  return lhs.value <= rhs;
}
constexpr bool operator<=(const Piece::Value lhs, const Piece rhs) {
  return lhs <= rhs.value;
}

constexpr bool operator>(const Piece lhs, const Piece::Value rhs) {
  return lhs.value > rhs;
}
constexpr bool operator>(const Piece::Value lhs, const Piece rhs) {
  return lhs > rhs.value;
}

constexpr bool operator>=(const Piece lhs, const Piece::Value rhs) {
  return lhs.value >= rhs;
}
constexpr bool operator>=(const Piece::Value lhs, const Piece rhs) {
  return lhs >= rhs.value;
}

std::ostream& operator<<(std::ostream& os, const Piece p);

constexpr Piece operator+(const Piece lhs, const Piece rhs) {
  return Piece(lhs.value + rhs.value);
}
constexpr int operator-(const Piece p, const int i) { return p.value - i; }

// constexpr Piece operator+(const Piece x, const Color y) noexcept {
//  return Piece(x.value + (y << 4));
//}
// constexpr Piece operator+(const Color x, const Piece y) noexcept {
//  return Piece((x << 4) + y);
//}

/**
 * @brief range based forで使うためのPiece型のオブジェクト
 *
 * ZEROからSIZEまでなので、途中に駒が存在しない値も含んでいる
 * @param
 * @return
 */
constexpr Piece PIECE(Piece::SIZE);
constexpr Piece operator*(const Piece piece) { return piece; }
constexpr Piece begin(const Piece) { return Piece::ZERO; }
constexpr Piece end(const Piece) { return Piece::SIZE; }

// ---------------------------------------------------------------------------
// 駒箱
// ---------------------------------------------------------------------------

// Positionクラスで用いる
// 駒リスト(どの駒がどこにあるのか)を管理するときの番号
struct PieceNumber {
  enum Value : uint8_t {
    FU = 0,
    KY = 18,
    KE = 22,
    GI = 26,
#ifdef NNUE_LITE
    KA = 30,
    HI = 32,
    KI = 34,
#else
    KI = 30,
    KA = 34,
    HI = 36,
#endif  // NNUE_LITE
    OU = 38,
    BLACK_OU = 38,
    WHITE_OU = 39,
    SIZE,
    ZERO = 0
  };

#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr PieceNumber() : value(ZERO) {}
#pragma warning(pop)
  constexpr PieceNumber(const Value n) : value(n) {}
  explicit constexpr PieceNumber(const int n) : value(static_cast<Value>(n)) {}

  constexpr PieceNumber& operator++() {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr PieceNumber operator++(int) {
    const PieceNumber tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }

  constexpr bool ok() const { return value < SIZE; }

  constexpr operator int() const { return static_cast<int>(value); }

  Value value;
};

// constexpr PieceNumber operator+(const PieceNumber number,
//                                const Color color) noexcept {
//  return static_cast<PieceNumber>(enum_cast<>(number.value) + color.value);
//}
constexpr PieceNumber operator+(const PieceNumber::Value number,
                                const Color color) noexcept {
  return static_cast<PieceNumber>(enum_cast<>(number) + color.value);
}

#endif  // !PIECE_H_INCLUDED
