// clang-format off
#include "stdafx.h"
// clang-format on

#include "square.h"
#include "../utility/enum_cast.h"

std::ostream& operator<<(std::ostream& os, const File f) {
  os << static_cast<char>('1' + f.value);
  return os;
}

std::ostream& operator<<(std::ostream& os, const Rank r) {
  os << static_cast<char>('a' + r.value);
  return os;
}

std::ostream& operator<<(std::ostream& os, const Square square) {
  os << square.file() << square.rank();
  return os;
}

std::ostream& operator<<(std::ostream& os, const SquareWithWall value) {
  os << static_cast<Square>(value);
  return os;
}
