#pragma once
#ifndef SQUARE_H_INCLUDED
#define SQUARE_H_INCLUDED

#include <array>
#include <boost/operators.hpp>
#include <iosfwd>
#include <limits>
#include <string>
#include <type_traits>

#include "../utility/enum_cast.h"
#include "color.h"

// ---------------------------------------------------------------------------
// 筋
// ---------------------------------------------------------------------------

struct File : private boost::less_than_comparable<File> {
  enum Value : int {
    // clang-format off
    _1, _2, _3, _4, _5, _6, _7, _8, _9,
    SIZE,
    ZERO = 0
    // clang-format on
  };
#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr File() : value(ZERO) {}
#pragma warning(pop)
  constexpr File(const Value f) : value(f) {}
  //! char以外の整数
  template <typename T, std::enable_if_t<!std::is_same_v<T, char>,
                                         std::nullptr_t> = nullptr>
  constexpr File(const T file) : value(static_cast<Value>(file)) {}
  //! USIの指し手の文字列から筋に変換
  constexpr File(const char c) : value(static_cast<Value>(c - '1')) {}

  constexpr bool operator==(const File f) const { return value == f.value; }
  constexpr bool operator<(const File f) const { return value < f.value; }

  //! 整数型にキャスト
  constexpr operator std::underlying_type_t<Value>() const noexcept {
    return enum_cast<>(value);
  }

  constexpr File& operator++() noexcept {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr File operator++(int) noexcept {
    const File tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }
  constexpr File& operator--() noexcept {
    value = static_cast<Value>(value - 1);
    return *this;
  }
  constexpr File operator--(int) noexcept {
    const File tmp(value);
    value = static_cast<Value>(value - 1);
    return tmp;
  }

  constexpr File operator+(const int v) const noexcept {
    return static_cast<Value>(value + v);
  }
  constexpr File operator-(const int v) const noexcept {
    return static_cast<Value>(value - v);
  }

  //! 正常な値か確認
  constexpr bool ok() const noexcept { return value >= ZERO && value < SIZE; }

  //! Fileを綺麗に出力する(USI形式ではない)
  /* "PRETTY_JP"をdefineしていれば、日本語文字での表示になる。例 → ８
   * "PRETTY_JP"をdefineしていなければ、数字のみの表示になる。例 → 8 */
  std::string pretty() const;

  Value value;
};

constexpr bool operator<(const File lhs, const File::Value rhs) noexcept {
  return enum_cast<>(lhs.value) < enum_cast<>(rhs);
}
constexpr bool operator<(const File::Value lhs, const File rhs) noexcept {
  return enum_cast<>(lhs) < enum_cast<>(rhs.value);
}

constexpr bool operator<=(const File lhs, const File::Value rhs) noexcept {
  return enum_cast<>(lhs.value) <= enum_cast<>(rhs);
}
constexpr bool operator<=(const File::Value lhs, const File rhs) noexcept {
  return enum_cast<>(lhs) <= enum_cast<>(rhs.value);
}

constexpr bool operator>(const File lhs, const File::Value rhs) noexcept {
  return enum_cast<>(lhs.value) > enum_cast<>(rhs);
}
constexpr bool operator>(const File::Value lhs, const File rhs) noexcept {
  return enum_cast<>(lhs) > enum_cast<>(rhs.value);
}

constexpr bool operator>=(const File lhs, const File::Value rhs) noexcept {
  return enum_cast<>(lhs.value) >= enum_cast<>(rhs);
}
constexpr bool operator>=(const File::Value lhs, const File rhs) noexcept {
  return enum_cast<>(lhs) >= enum_cast<>(rhs.value);
}

constexpr bool operator==(const File lhs, const File::Value rhs) noexcept {
  return enum_cast<>(lhs.value) == enum_cast<>(rhs);
}
constexpr bool operator==(const File::Value lhs, const File rhs) noexcept {
  return enum_cast<>(lhs) == enum_cast<>(rhs.value);
}

//! USI形式で出力
std::ostream& operator<<(std::ostream& os, const File f);

// ---------------------------------------------------------------------------
// 段
// ---------------------------------------------------------------------------
struct Rank : private boost::less_than_comparable<Rank> {
  enum Value : int {
    // clang-format off
    _1, _2, _3, _4, _5, _6, _7, _8, _9,
    SIZE,
    ZERO = 0
    // clang-format on
  };
#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Rank() : value(ZERO) {}
#pragma warning(pop)
  constexpr Rank(const Value f) : value(f) {}
  //! char以外の整数
  template <typename T, std::enable_if_t<!std::is_same_v<T, char>,
                                         std::nullptr_t> = nullptr>
  constexpr Rank(const T rank) : value(static_cast<Value>(rank)) {}
  //! USIの指し手の文字列から筋に変換
  constexpr Rank(const char c) : value(static_cast<Value>(c - 'a')) {}

  constexpr bool operator==(const Rank r) const { return value == r.value; }
  constexpr bool operator<(const Rank r) const { return value < r.value; }

  //! 整数型にキャスト
  constexpr operator std::underlying_type_t<Value>() const noexcept {
    return enum_cast<>(value);
  }

  constexpr Rank& operator++() noexcept {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr Rank operator++(int) noexcept {
    const Rank tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }
  constexpr Rank& operator--() noexcept {
    value = static_cast<Value>(value - 1);
    return *this;
  }
  constexpr Rank operator--(int) noexcept {
    const Rank tmp(value);
    value = static_cast<Value>(value - 1);
    return tmp;
  }

  constexpr Rank operator+(const int v) const noexcept {
    return static_cast<Value>(value + v);
  }
  constexpr Rank operator-(const int v) const noexcept {
    return static_cast<Value>(value - v);
  }

  //! 正常な値か確認
  constexpr bool ok() const noexcept { return value >= ZERO && value < SIZE; }

  //! Rankを綺麗に出力する(USI形式ではない)
  /* "PRETTY_JP"をdefineしていれば、日本語文字での表示になる。例 → 八
   * "PRETTY_JP"をdefineしていなければ、数字のみの表示になる。例 → 8 */
  std::string pretty() const;

  //! 成れるかどうか判定
  constexpr bool promotable(const Color c) const noexcept {
    return static_cast<bool>(0x1C00007u & (1u << ((c << 4) + value)));
  }

  //! BLACKならそのまま、WHITEなら反転した位置を返す
  constexpr Rank relative(const Color c) const noexcept {
    return c == Color::BLACK ? value : static_cast<Value>(8 - value);
  }

  Value value;
};

constexpr bool operator<(const Rank lhs, const Rank::Value rhs) noexcept {
  return enum_cast<>(lhs.value) < enum_cast<>(rhs);
}
constexpr bool operator<(const Rank::Value lhs, const Rank rhs) noexcept {
  return enum_cast<>(lhs) < enum_cast<>(rhs.value);
}

constexpr bool operator<=(const Rank lhs, const Rank::Value rhs) noexcept {
  return enum_cast<>(lhs.value) <= enum_cast<>(rhs);
}
constexpr bool operator<=(const Rank::Value lhs, const Rank rhs) noexcept {
  return enum_cast<>(lhs) <= enum_cast<>(rhs.value);
}

constexpr bool operator>(const Rank lhs, const Rank::Value rhs) noexcept {
  return enum_cast<>(lhs.value) > enum_cast<>(rhs);
}
constexpr bool operator>(const Rank::Value lhs, const Rank rhs) noexcept {
  return enum_cast<>(lhs) > enum_cast<>(rhs.value);
}

constexpr bool operator>=(const Rank lhs, const Rank::Value rhs) noexcept {
  return enum_cast<>(lhs.value) >= enum_cast<>(rhs);
}
constexpr bool operator>=(const Rank::Value lhs, const Rank rhs) noexcept {
  return enum_cast<>(lhs) >= enum_cast<>(rhs.value);
}

constexpr bool operator==(const Rank lhs, const Rank::Value rhs) noexcept {
  return enum_cast<>(lhs.value) == enum_cast<>(rhs);
}
constexpr bool operator==(const Rank::Value lhs, const Rank rhs) noexcept {
  return enum_cast<>(lhs) == enum_cast<>(rhs.value);
}

//! USI形式で出力
std::ostream& operator<<(std::ostream& os, const Rank r);

// ---------------------------------------------------------------------------
// 升目
// ---------------------------------------------------------------------------
namespace detail {
constexpr File file_table[] = {
    // clang-format off
  File::_1, File::_1, File::_1, File::_1, File::_1, File::_1, File::_1, File::_1, File::_1,
  File::_2, File::_2, File::_2, File::_2, File::_2, File::_2, File::_2, File::_2, File::_2,
  File::_3, File::_3, File::_3, File::_3, File::_3, File::_3, File::_3, File::_3, File::_3,
  File::_4, File::_4, File::_4, File::_4, File::_4, File::_4, File::_4, File::_4, File::_4,
  File::_5, File::_5, File::_5, File::_5, File::_5, File::_5, File::_5, File::_5, File::_5,
  File::_6, File::_6, File::_6, File::_6, File::_6, File::_6, File::_6, File::_6, File::_6,
  File::_7, File::_7, File::_7, File::_7, File::_7, File::_7, File::_7, File::_7, File::_7,
  File::_8, File::_8, File::_8, File::_8, File::_8, File::_8, File::_8, File::_8, File::_8,
  File::_9, File::_9, File::_9, File::_9, File::_9, File::_9, File::_9, File::_9, File::_9,
  File::SIZE
    // clang-format on
};
constexpr Rank rank_table[] = {
    // clang-format off
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::_1, Rank::_2, Rank::_3, Rank::_4, Rank::_5, Rank::_6, Rank::_7, Rank::_8, Rank::_9,
  Rank::SIZE
    // clang-format on
};
}  // namespace detail

struct Square : private boost::less_than_comparable<Square> {
  enum Value : int {
    // clang-format off
    _11, _12, _13, _14, _15, _16, _17, _18, _19,
    _21, _22, _23, _24, _25, _26, _27, _28, _29,
    _31, _32, _33, _34, _35, _36, _37, _38, _39,
    _41, _42, _43, _44, _45, _46, _47, _48, _49,
    _51, _52, _53, _54, _55, _56, _57, _58, _59,
    _61, _62, _63, _64, _65, _66, _67, _68, _69,
    _71, _72, _73, _74, _75, _76, _77, _78, _79,
    _81, _82, _83, _84, _85, _86, _87, _88, _89,
    _91, _92, _93, _94, _95, _96, _97, _98, _99,
    SIZE, ZERO = 0,
    //! 王がいない場合を表現するために一つ大きい定数
    SIZE_PLUS1 = SIZE + 1,

    D = +1,  //!< 下
    R = -9,  //!< 右
    U = -1,  //!< 上
    L = +9,  //!< 左

    RU = U + R,
    RD = D + R,
    LU = U + L,
    LD = D + L,
    RUU = RU + U,
    LUU = LU + U,
    RDD = RD + D,
    LDD = LD + D
    // clang-format on
  };

  Square() = default;
#pragma warning(push)
#pragma warning(disable : 26812)
  explicit constexpr Square(const int square)
      : value(static_cast<Value>(square)) {}
#pragma warning(pop)
  constexpr Square(const Value square) : value(square) {}
  constexpr Square(const File f, const Rank r)
      : value(static_cast<Value>(f.value * 9 + r.value)) {}

  constexpr bool operator==(const Square rhs) const {
    return value == rhs.value;
  }
  constexpr bool operator<(const Square rhs) const { return value < rhs.value; }

  constexpr operator std::underlying_type_t<Value>() const { return value; }

  // constexpr Square& operator+=(const Square other) {
  //  value =
  //      static_cast<Value>(enum_cast<>(value) + enum_cast<>(other.value));
  //  return *this;
  //}
  // constexpr Square& operator-=(const Square other) {
  //  value =
  //      static_cast<Value>(enum_cast<>(value) - enum_cast<>(other.value));
  //  return *this;
  //}

  constexpr Square& operator++() noexcept {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr Square operator++(int) noexcept {
    const Square tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }
  constexpr Square& operator--() noexcept {
    value = static_cast<Value>(value - 1);
    return *this;
  }
  constexpr Square operator--(int) noexcept {
    const Square tmp(value);
    value = static_cast<Value>(value - 1);
    return tmp;
  }

  //! 正常な値か確認
  //! 駒落ちなどの場合があるので、SQ_SIZEまで許容
  constexpr bool ok() const noexcept {
    return value >= ZERO && value < SIZE_PLUS1;
  }

  constexpr File file() const noexcept { return detail::file_table[value]; }
  constexpr Rank rank() const noexcept { return detail::rank_table[value]; }

  constexpr bool promotable(const Color c) const noexcept {
    return rank().promotable(c);
  }

  //! 盤を180度回転させた時の升
  constexpr Square inverse() const noexcept { return Square(SIZE - value - 1); }
  //! 盤を左右反転させた時の升
  constexpr Square mirror() const noexcept {
    return Square(File::SIZE - file().value - 1, rank());
  }

  /**
   * @brief USI形式ではない表示
   * 
   * "PRETTY_JP"をdefineしていれば、日本語文字での表示になる。例 → ８八
   * "PRETTY_JP"をdefineしていなければ、数字のみの表示になる。例 → 88
   * @return 
  */
  std::string pretty() const { return file().pretty() + rank().pretty(); }

  Value value;
};

constexpr bool operator==(const Square x, const Square::Value y) noexcept {
  return x.value == y;
}

//! range based forで使うためのSquare型のオブジェクト
constexpr Square SQUARE(82);
constexpr Square operator*(const Square square) noexcept { return square; }
constexpr Square begin(const Square) noexcept { return Square::ZERO; }
constexpr Square end(const Square) noexcept { return Square::SIZE; }

/**
 * @brief 二つの升のfileの差とrankの差の大きい方の値
 * 
 * どちらか一方が盤外ならintの最大値を距離とする
 * @param s1 
 * @param s2 
 * @return 
*/
constexpr int distance(const Square s1, const Square s2) {
  return (s1.ok() && s2.ok())
             ? std::max(abs(s1.file() - s2.file()), abs(s1.rank() - s2.rank()))
             : std::numeric_limits<int32_t>::max();
}

constexpr bool IsPromotable(const Color c, const Square source,
                            const Square target) {
  return source.promotable(c) || target.promotable(c);
}

constexpr Square operator+(const Square lhs, const Square rhs) {
  // SquareとSquare::Valueのoperator+に該当するのでキャスト
  return Square(enum_cast<>(lhs.value) + rhs.value);
}
constexpr Square operator+(const Square x, const Square::Value y) {
  return static_cast<Square::Value>(enum_cast<>(x.value) + y);
}
constexpr Square operator+(const Square::Value x, const Square y) {
  return static_cast<Square::Value>(enum_cast<>(x) + y.value);
}

constexpr bool operator<(const Square lhs, const Square::Value rhs) {
  return lhs.value < rhs;
}
constexpr bool operator<(const Square::Value lhs, const Square rhs) {
  return lhs < rhs.value;
}

constexpr bool operator<=(const Square lhs, const Square::Value rhs) {
  return lhs.value <= rhs;
}
constexpr bool operator<=(const Square::Value lhs, const Square rhs) {
  return lhs <= rhs.value;
}

constexpr bool operator>(const Square lhs, const Square::Value rhs) {
  return lhs.value > rhs;
}
constexpr bool operator>(const Square::Value lhs, const Square rhs) {
  return lhs > rhs.value;
}

constexpr bool operator>=(const Square lhs, const Square::Value rhs) {
  return lhs.value >= rhs;
}
constexpr bool operator>=(const Square::Value lhs, const Square rhs) {
  return lhs >= rhs.value;
}

//! USI形式で出力
std::ostream& operator<<(std::ostream& os, const Square s);

// ---------------------------------------------------------------------------
// 壁つきの升目
// ---------------------------------------------------------------------------
namespace detail {
//! SquareWithWallの値を設定するための補助関数
constexpr int32_t WallValue(const Square s) {
  return s | (1 << 8) | (file_table[s] << 9) | (rank_table[s] << 14) |
         ((Rank::SIZE - rank_table[s].value - 1) << 19) |
         ((File::SIZE - file_table[s].value - 1) << 24);
}
}  // namespace detail

struct SquareWithWall {
  /*! 長い利きを更新するときにある升からある方向に駒にぶつかるまで
   *  ずっと利きを更新していきたいことがあるが、
   *  sqの升が盤外であるかどうかを判定する簡単な方法がない。
   *  そこで、Squareの表現を拡張して盤外であることを検出できるようにする。
   *  bit 0..7   : Squareと同じ意味
   *  bit 8      : Squareからのborrow用に1にしておく
   *  bit 9..13  : いまの升から盤外まで何升右に升があるか
   *               (ここがマイナスになるとborrowでbit13が1になる)
   *  bit 14..18 : いまの升から盤外まで何升上に(略
   *  bit 19..23 : いまの升から盤外まで何升下に(略
   *  bit 24..28 : いまの升から盤外まで何升左に(略 */
  enum Value : int32_t {
    // 上下左右の差分値

    R = Square::R - (1 << 9) + (1 << 24),
    U = Square::U - (1 << 14) + (1 << 19),
    D = -U,
    L = -R,
    RU = R + U,
    RD = R + D,
    LU = L + U,
    LD = L + D,
    // 桂馬の動き
    RUU = RU + U,
    RDD = RD + D,
    LUU = LU + U,
    LDD = LD + D,

    //! Square::_1A 右に0升、上に0升、下に8升、左に8升
    _11 = Square::_11 | (1 << 8) | (0 << 9) | (0 << 14) | (8 << 19) | (8 << 24),
    _12 = detail::WallValue(Square::_12),
    _13 = detail::WallValue(Square::_13),
    _14 = detail::WallValue(Square::_14),
    _15 = detail::WallValue(Square::_15),
    _16 = detail::WallValue(Square::_16),
    _17 = detail::WallValue(Square::_17),
    _18 = detail::WallValue(Square::_18),
    _19 = detail::WallValue(Square::_19),
    _21 = detail::WallValue(Square::_21),
    _22 = detail::WallValue(Square::_22),
    _23 = detail::WallValue(Square::_23),
    _24 = detail::WallValue(Square::_24),
    _25 = detail::WallValue(Square::_25),
    _26 = detail::WallValue(Square::_26),
    _27 = detail::WallValue(Square::_27),
    _28 = detail::WallValue(Square::_28),
    _29 = detail::WallValue(Square::_29),
    _31 = detail::WallValue(Square::_31),
    _32 = detail::WallValue(Square::_32),
    _33 = detail::WallValue(Square::_33),
    _34 = detail::WallValue(Square::_34),
    _35 = detail::WallValue(Square::_35),
    _36 = detail::WallValue(Square::_36),
    _37 = detail::WallValue(Square::_37),
    _38 = detail::WallValue(Square::_38),
    _39 = detail::WallValue(Square::_39),
    _41 = detail::WallValue(Square::_41),
    _42 = detail::WallValue(Square::_42),
    _43 = detail::WallValue(Square::_43),
    _44 = detail::WallValue(Square::_44),
    _45 = detail::WallValue(Square::_45),
    _46 = detail::WallValue(Square::_46),
    _47 = detail::WallValue(Square::_47),
    _48 = detail::WallValue(Square::_48),
    _49 = detail::WallValue(Square::_49),
    _51 = detail::WallValue(Square::_51),
    _52 = detail::WallValue(Square::_52),
    _53 = detail::WallValue(Square::_53),
    _54 = detail::WallValue(Square::_54),
    _55 = detail::WallValue(Square::_55),
    _56 = detail::WallValue(Square::_56),
    _57 = detail::WallValue(Square::_57),
    _58 = detail::WallValue(Square::_58),
    _59 = detail::WallValue(Square::_59),
    _61 = detail::WallValue(Square::_61),
    _62 = detail::WallValue(Square::_62),
    _63 = detail::WallValue(Square::_63),
    _64 = detail::WallValue(Square::_64),
    _65 = detail::WallValue(Square::_65),
    _66 = detail::WallValue(Square::_66),
    _67 = detail::WallValue(Square::_67),
    _68 = detail::WallValue(Square::_68),
    _69 = detail::WallValue(Square::_69),
    _71 = detail::WallValue(Square::_71),
    _72 = detail::WallValue(Square::_72),
    _73 = detail::WallValue(Square::_73),
    _74 = detail::WallValue(Square::_74),
    _75 = detail::WallValue(Square::_75),
    _76 = detail::WallValue(Square::_76),
    _77 = detail::WallValue(Square::_77),
    _78 = detail::WallValue(Square::_78),
    _79 = detail::WallValue(Square::_79),
    _81 = detail::WallValue(Square::_81),
    _82 = detail::WallValue(Square::_82),
    _83 = detail::WallValue(Square::_83),
    _84 = detail::WallValue(Square::_84),
    _85 = detail::WallValue(Square::_85),
    _86 = detail::WallValue(Square::_86),
    _87 = detail::WallValue(Square::_87),
    _88 = detail::WallValue(Square::_88),
    _89 = detail::WallValue(Square::_89),
    _91 = detail::WallValue(Square::_91),
    _92 = detail::WallValue(Square::_92),
    _93 = detail::WallValue(Square::_93),
    _94 = detail::WallValue(Square::_94),
    _95 = detail::WallValue(Square::_95),
    _96 = detail::WallValue(Square::_96),
    _97 = detail::WallValue(Square::_97),
    _98 = detail::WallValue(Square::_98),
    _99 = detail::WallValue(Square::_99),
    //! 盤外に到達した際のborrow bit
    BORROW_MASK = (1 << 13) | (1 << 18) | (1 << 23) | (1 << 28)
  };

 private:
  // clang-format off
  static constexpr std::array<Value, Square::SIZE> _table = {
    _11, _12, _13, _14, _15, _16, _17, _18, _19,
    _21, _22, _23, _24, _25, _26, _27, _28, _29,
    _31, _32, _33, _34, _35, _36, _37, _38, _39,
    _41, _42, _43, _44, _45, _46, _47, _48, _49,
    _51, _52, _53, _54, _55, _56, _57, _58, _59,
    _61, _62, _63, _64, _65, _66, _67, _68, _69,
    _71, _72, _73, _74, _75, _76, _77, _78, _79,
    _81, _82, _83, _84, _85, _86, _87, _88, _89,
    _91, _92, _93, _94, _95, _96, _97, _98, _99
  };
  // clang-format on

 public:
#pragma warning(push)
#pragma warning(disable : 26812)
   constexpr SquareWithWall() : value(BORROW_MASK) {}
#pragma warning(pop)
  constexpr SquareWithWall(const Value s) : value(s) {}
  constexpr SquareWithWall(const Square s) : value(_table[s]) {}

  constexpr operator Square() const { return Square(value & 0xFF); }

  // キャストを両立できない
  // constexpr operator std::underlying_type_t<Value>() const {
  //  return value;
  //}

  constexpr bool operator==(const SquareWithWall other) const {
    return value == other.value;
  }

  //! 盤内ならtrue, 盤外はfalse
  constexpr bool ok() const { return (value & BORROW_MASK) == 0; }

  Value value;
};

constexpr SquareWithWall operator+(const SquareWithWall sq,
                                   const SquareWithWall delta) {
  return SquareWithWall(
      static_cast<SquareWithWall::Value>(sq.value + delta.value));
}
constexpr SquareWithWall& operator+=(SquareWithWall& sq,
                                     const SquareWithWall delta) {
  sq.value = static_cast<SquareWithWall::Value>(sq.value + delta.value);
  return sq;
}

//! Squareとして出力
std::ostream& operator<<(std::ostream& os, const SquareWithWall value);
#endif  // !SQUARE_H_INCLUDED
