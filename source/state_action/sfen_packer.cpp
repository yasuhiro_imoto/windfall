#include "stdafx.h"

#include "packed_sfen.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/force_inline.h"
#include "../utility/level_assert.h"
#include "hand.h"
#include "position.h"

namespace state_action {
class BitStreamWriter {
 public:
  BitStreamWriter(const PackedSfen::Array::pointer p) : data_(p), cursor_(0) {}

  FORCE_INLINE void Write(const int b) {
    if (b) {
      data_[cursor_ / 8] |= 1 << (cursor_ & 7);
    }
    ++cursor_;
  }
  void Write(const int d, const int n) {
    for (int i = 0; i < n; ++i) {
      Write(d & (1 << i));
    }
  }

  int size() const { return cursor_; }

 private:
  /**
   * @brief 次に値を読み書きするbit位置
   */
  int cursor_;

  /**
   * @brief データの実体へのポインタ
   */
  PackedSfen::Array::pointer data_;
};

class BitStreamReader {
 public:
  BitStreamReader(const PackedSfen::Array::const_pointer p)
      : data_(p), cursor_(0) {}

  FORCE_INLINE int Read() {
    const int b = (data_[cursor_ / 8] >> (cursor_ & 7)) & 1;
    ++cursor_;
    return b;
  }
  int Read(const int n) {
    int result = 0;
    for (int i = 0; i < n; ++i) {
      result |= Read() ? (1 << i) : 0;
    }
    return result;
  }

  int size() const { return cursor_; }

 private:
  /**
   * @brief 次に値を読み書きするbit位置
   */
  int cursor_;

  /**
   * @brief データの実体へのポインタ
   */
  PackedSfen::Array::const_pointer data_;
};

/*
ハフマン符号化

盤上の1升(EMPTY以外) = 2～6bit ( + 成りフラグ1bit+ 先後1bit )
手駒の1枚            = 1～5bit ( + 成りフラグ1bit+ 先後1bit )

空 xxxxx0 + 0  (none)
歩 xxxx01 + 2  xxxx0 + 2
香 xx0011 + 2  xx001 + 2
桂 xx1011 + 2  xx101 + 2
銀 xx0111 + 2  xx011 + 2
金 x01111 + 1  x0111 + 1 // 金は成りフラグはない。
角 011111 + 2  01111 + 2
飛 111111 + 2  11111 + 2

すべての駒が盤上にあるとして、
空 81 - 40駒 = 41升 = 41bit
歩      4bit*18駒   = 72bit
香      6bit* 4駒   = 24bit
桂      6bit* 4駒   = 24bit
銀      6bit* 4駒   = 24bit
金      6bit* 4駒   = 24bit
角      8bit* 2駒   = 16bit
飛      8bit* 2駒   = 16bit
                     -------
                     241bit + 1bit(手番) + 7bit×2(王の位置先後) = 256bit

盤上の駒が手駒に移動すると盤上の駒が空になるので盤上のその升は1bitで表現でき、
手駒は、盤上の駒より1bit少なく表現できるので結局、全体のbit数に変化はない。
ゆえに、この表現において、どんな局面でもこのbit数で表現できる。
手駒に成りフラグは不要だが、これも含めておくと盤上の駒のbit数-1になるので
全体のbit数が固定化できるのでこれも含めておくことにする。
*/

struct HuffmanedPiece {
  //! ビット列
  int code;
  //! bit幅
  int bits;
};

constexpr std::array<HuffmanedPiece, 8> huffman_table = {{
    {0x00, 1},  // empty
    {0x01, 2},  // 歩
    {0x03, 4},  // 香
    {0x0B, 4},  // 桂
    {0x07, 4},  // 銀
    {0x1F, 6},  // 角
    {0x3F, 6},  // 飛
    {0x0F, 5}   // 金
}};

struct SfenPacker {
  PackedSfen::Array::pointer data;

  void Pack(const Position& position) {
    std::memset(data, 0, 256 / sizeof(PackedSfen::Array::value_type));
    BitStreamWriter stream(data);

    // 手番
    stream.Write(static_cast<int>(position.side()));

    // 先後それぞれの王の位置
    // 7bits
    for (const auto color : COLOR) {
      stream.Write(static_cast<int>(position.ou_square(color)), 7);
    }

    // 王以外の駒
    for (const auto square : SQUARE) {
      const auto piece = position[square];
      if (piece.type() == Piece::OU) {
        continue;
      }
      WriteBoard(piece, stream);
    }

    // 持ち駒
    for (const auto color : COLOR) {
      for (Piece piece = Piece::FU; piece < Piece::OU; ++piece) {
        const int n = position[color].count(piece);
        for (int i = 0; i < n; ++i) {
          WriteHand(Piece(color, piece), stream);
        }
      }
    }

    ASSERT_LV3(stream.size() == 256);
  }

  static void WriteBoard(const Piece piece, BitStreamWriter& stream) {
    const auto raw_type = piece.raw_type();
    const auto h = huffman_table[raw_type];
    stream.Write(h.code, h.bits);

    if (piece == Piece::EMPTY) {
      return;
    }

    // 成りのフラグ
    // ただし、金にフラグはない
    if (piece != Piece::KI) {
      stream.Write((piece & Piece::PROMOTION) ? 1 : 0);
    }

    // 先後のフラグ
    stream.Write(static_cast<int>(piece.color()));
  }

  static void WriteHand(const Piece piece, BitStreamWriter& stream) {
    ASSERT_LV3(piece != Piece::EMPTY);

    const auto raw_type = piece.raw_type();
    const auto c = huffman_table[raw_type];
    stream.Write(c.code >> 1, c.bits - 1);

    // 成りのフラグ
    if (raw_type != Piece::KI) {
      stream.Write(0);
    }

    // 先後のフラグ
    stream.Write(static_cast<int>(piece.color()));
  }
};

struct SfenUnpacker {
  PackedSfen::Array::const_pointer data;

  void Unpack(Position& position) { BitStreamReader stream(data); }

  static Piece ReadBoard(BitStreamReader& stream) {
    Piece raw_type = Piece::EMPTY;
    int code = 0, bits = 0;
    bool found = false;
    while (!found) {
      code |= stream.Read() << bits;
      ++bits;
      ASSERT_LV3(bits <= 6);

      for (raw_type = Piece::EMPTY; raw_type < Piece::OU; ++raw_type) {
        if (huffman_table[raw_type].code == code &&
            huffman_table[raw_type].bits == bits) {
          found = true;
          break;
        }
      }
    }

    if (raw_type == Piece::EMPTY) {
      return raw_type;
    }

    // 成りのフラグ
    const bool promotion = raw_type == Piece::KI ? false : stream.Read();

    // 先後のフラグ
    const Color color = static_cast<Color::Value>(stream.Read());

    return Piece(color, raw_type, promotion);
  }

  static Piece ReadHand(BitStreamReader& stream) {
    Piece raw_type = Piece::EMPTY;
    int code = 0, bits = 0;
    bool found = false;
    while (!found) {
      code |= stream.Read() << bits;
      ++bits;
      ASSERT_LV3(bits <= 6);

      for (raw_type = Piece::FU; raw_type < Piece::OU; ++raw_type) {
        if ((huffman_table[raw_type].code >> 1) == code &&
            (huffman_table[raw_type].bits - 1) == bits) {
          found = true;
          break;
        }
      }
    }
    ASSERT_LV3(raw_type != Piece::EMPTY);

    // 成りのフラグを捨てる
    if (raw_type != Piece::KI) {
      stream.Read();
    }

    // 先後のフラグ
    const Color color = static_cast<Color::Value>(stream.Read());

    return Piece(color, raw_type);
  }
};

int Position::Set(const PackedSfen& sfen, Rollback* rb, search::Thread* thread,
                  const bool mirror, const int ply) {
  BitStreamReader stream(sfen.data.data());

  std::memset(this, 0, sizeof(Position));
  std::memset(rb, 0, sizeof(Rollback));
  // memsetで全部を初期化したので、要らないはず
  evaluation_list_.clear();

  rb_ = rb;

  // 手番を読み込み
  side_ = static_cast<Color::Value>(stream.Read());

  std::array<PieceNumber, Piece::OU> piece_no_count = {
      PieceNumber::ZERO, PieceNumber::FU, PieceNumber::KY, PieceNumber::KE,
      PieceNumber::GI,   PieceNumber::KA, PieceNumber::HI, PieceNumber::KI};

  // 王を読み込み
  for (const auto color : COLOR) {
    const Square s(stream.Read(7));
    if (mirror) {
      location_.Add(s.mirror(), Piece(color, Piece::OU));
    } else {
      location_.Add(s, Piece(color, Piece::OU));
    }
  }

  // 盤上の駒を読み込み
  for (const auto square : SQUARE) {
    const auto s = mirror ? square.mirror() : square;

    Piece piece;
    PieceNumber piece_no;
    if (location_[s].type() == Piece::OU) {
      piece = location_[s];

      piece_no = piece.color() == Color::BLACK ? PieceNumber::BLACK_OU
                                               : PieceNumber::WHITE_OU;
    } else {
      ASSERT_LV3(location_[s] == Piece::EMPTY);

      piece = SfenUnpacker::ReadBoard(stream);
      if (piece == Piece::EMPTY) {
        continue;
      }

      location_.Add(s, piece);
      piece_no = piece_no_count[piece.raw_type()]++;
    }
    evaluation_list_.Update(piece_no, s, piece);

    if (stream.size() > 256) {
      // このタイミングで256bitを超えているならエラー
      return -1;
    }
  }

  int i = 0;
  Piece last_piece = Piece::EMPTY;
  while (stream.size() < 256) {
    const auto piece = SfenUnpacker::ReadHand(stream);
    location_.Add(piece.color(), piece.type());

    if (last_piece != piece) {
      i = 0;
    }
    last_piece = piece;

    const auto raw_type = piece.raw_type();
    const PieceNumber piece_no = piece_no_count[raw_type]++;
    ASSERT_LV1(piece_no.ok());
    evaluation_list_.Update(piece_no, piece.color(), raw_type, i);

    ++i;
  }

  if (stream.size() != 256) {
    return -1;
  }

  game_ply_ = ply;

  location_.Update();
  location_.UpdateOuSquare();

  InitializeState(rb);

#ifdef LONG_EFFECT_LIBRARY
  // 利きを設定
#endif  // LONG_EFFECT_LIBRARY

  rb->material_value = evaluation::InitializeMaterial(*this);
  evaluation::ComputeEvaluation(*this);

  ptr_thread_ = thread;

  return 0;
}
}  // namespace state_action
