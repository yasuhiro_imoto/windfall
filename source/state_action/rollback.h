#pragma once
#ifndef ROLLBACK_H_INCLUDED
#define ROLLBACK_H_INCLUDED

#include <array>
#include <cstdint>
#include <deque>
#include <memory>

#include "../evaluation/dirty_piece.h"
#include "../evaluation/nn/accumulator.h"
#include "../evaluation/value.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../utility/hash_key.h"
#include "bitboard.h"
#include "hand.h"

namespace state_action {
class Position;

#pragma warning(push)
#pragma warning(disable : 26495)
class Rollback {
  friend class Position;

 public:
  //! 遡り可能な手数(previousポインタを用いて局面を遡るときに用いる)
  /*! pliesFromNull */
  int plies_from_null;

  //! この手番側の連続王手は何手前からやっているのか
  //! (連続王手の千日手の検出のときに必要)
  /*! continuousCheck */
  std::array<int, Color::SIZE> continuous_check;

  //! 現局面で手番側に対して王手をしている駒のbitboard
  /*! checkersBB */
  Bitboard checkers_bb;
  //! 自玉に対して(敵駒によって)pinされている駒
  /*! color = 手番側 なら pinされている駒(動かすと開き王手になる)
      color = 相手側 なら 両王手の候補となる駒
      blockersFroKing */
  std::array<Bitboard, Color::SIZE> blockers_for_ou;
  //! 自玉に対してpinしている(可能性のある)敵の大駒
  /*! 自玉に対して上下左右方向にある敵の飛車、斜め十字方向にある敵の角、
      玉の前方向にある敵の香
      pinners */
  std::array<Bitboard, Color::SIZE> pinners_for_ou;
  //! 自駒の駒種Xによって敵玉が王手となる升のbitboard
  /*! checkSquares */
  std::array<Bitboard, Piece::WHITE> check_squares;

  //! 現局面のハッシュキー
  /*! do_moveで進むときに最終的な値が設定される
      key */
  Key key() const { return board_key() + hand_key(); }
  /*! board_key */
  Key board_key() const { return static_cast<Key>(long_board_key()); }
  /*! hand_key */
  Key hand_key() const { return static_cast<Key>(long_hand_key()); }

  /*! HASH_KEY_BITSが128の時はKey128、256の時はKey256
      long_key */
  HashKey long_key() const { return long_board_key() + long_hand_key(); }
  /*! long_board_key */
  HashKey long_board_key() const { return board_key_; }
  /*! long_hand_key */
  HashKey long_hand_key() const { return hand_key_; }

  //! この局面における手番側の持ち駒
  //! 優等局面の判定に利用
  /*! hand */
  Hand hand;

  //! 捕獲された駒
  /*! 次の局面にdo_moveで進むときにこの値が設定される
      先後の区別はなし、成りの区別はある
      capturedPiece */
  Piece captured_piece;

  //! この局面の駒割
  evaluation::Value material_value;

  /*! accumulator */
  evaluation::nn::Accumulator accumulator;

  //! 評価値の差分計算の管理用
  /*! dirtyPiece */
  evaluation::DirtyPiece dirty_piece;

#if defined(KEEP_LAST_MOVE)
  //! 直前の指し手
  /*! デバッグ時にこの局面までの指し手がわかると便利
      lastMove */
  Move last_move;

  //! last_moveで移動させた駒(先後の区別なし)
  /*! lastMovedPieceType */
  Piece last_moved_piece_type;
#endif  // defined(KEEP_LAST_MOVE)

 private:
  HashKey board_key_, hand_key_;

 public:
  //! 一つ前の局面のポインタ
  /*! これがnullptrになる場合は
      1)現局面がルート
      2)一つ前の局面でnull move
      評価値の計算は 1)差分で計算しない 2)do_move_nullで必要な情報が
      コピーされているからいずれの場合も前の局面を必要としない
      previous */
  Rollback* previous;

  //! StateListPtrがこのクラスをnewするときにalignasを無視するので、カスタムアロケータで対応
  void* operator new(std::size_t size);
  void operator delete(void* p) noexcept;
};
#pragma warning(pop)

/*! setup moves("position"コマンドで設定される
    現局面までの指し手)に沿った局面の状態を追跡するために使う
    千日手の判定のためにこれが必要。
    std::dequeを使っているのは、StateInfoがポインターを内包しているので、resizeに対して
    無効化されないように。*/
using StateList = std::deque<Rollback, AlignedAllocator<Rollback>>;
using StateListPtr = std::unique_ptr<StateList>;

}  // namespace state_action
#endif  // !ROLLBACK_H_INCLUDED
