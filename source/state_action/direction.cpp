// clang-format off
#include "stdafx.h"
// clang-format on

#include "direction.h"
#include "../rule/square.h"

namespace state_action {
namespace effect8 {
namespace detail {
//! sq1にとってsq2がどのdirectionにあるか
/*! direc_table */
DirectionSet direction_table[Square::SIZE_PLUS1][Square::SIZE_PLUS1];

void InitializeDirectionTable() {
  for (const auto sq1 : SQUARE) {
    for (effect8::Direction d = effect8::Direction::ZERO;
         d < effect8::Direction::SIZE; ++d) {
      // dの方向に壁にぶつかるまで延長する
      // sq1からみてsq2の方向は 1 << d となる
      const auto delta = static_cast<SquareWithWall>(d);
      for (SquareWithWall sq2 = SquareWithWall(sq1) + delta; sq2.ok();
           sq2 += delta) {
        effect8::detail::direction_table[sq1][static_cast<Square>(sq2)] = d;
      }
    }
  }
}
}  // namespace detail
}  // namespace effect8
}  // namespace state_action
