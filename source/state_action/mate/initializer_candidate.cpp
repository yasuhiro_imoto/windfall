#include "stdafx.h"

#include "mate_1ply.h"

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"

namespace state_action {
namespace mate {
namespace detail {
template <Color::Value Us, PieceTypeCheck Type>
struct CandidateInitializerSquare;

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::FU_WITHOUT_PROMOTION> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = GetEffect<Piece::FU, ~Us>(square) & ~GetEnemyField(Us);
    if (!bb) {
      return bb;
    }

    const Square target = bb.Pop();
    bb = GetEffect<Piece::FU, ~Us>(target);
    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::FU_WITH_PROMOTION> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = GetEffect<Piece::KI, ~Us>(square) & GetEnemyField(Us);
    bb = GetFuEffect(~Us, bb);
    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::KY> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = state_action::detail::GetKyNaiveEffect(~Us, square);
    if (GetEnemyField(Us) & square) {
      if (square.file() != File::_1) {
        bb |= state_action::detail::GetKyNaiveEffect(~Us, square + Square::R);
      }
      if (square.file() != File::_9) {
        bb |= state_action::detail::GetKyNaiveEffect(~Us, square + Square::L);
      }
    }
    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::KE> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    // 成らずに王手する場合と成って王手する場合
    Bitboard tmp = GetEffect<Piece::KE, ~Us>(square) |
                   (GetEffect<Piece::KI, ~Us>(square) & GetEnemyField(Us));
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= GetEffect<Piece::KE, ~Us>(target);
    }
    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::GI> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    // 成らずに王手する場合と成って王手する場合
    Bitboard tmp = GetEffect<Piece::GI, ~Us>(square) |
                   (GetEffect<Piece::KI, ~Us>(square) & GetEnemyField(Us));
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= GetEffect<Piece::GI, ~Us>(target);
    }

    // 敵陣から出るときに成る場合が残っている
    if (square.rank() == Rank::_5 ||
        (Us == Color::BLACK && square.rank() == Rank::_4) ||
        (Us == Color::WHITE && square.rank() == Rank::_6)) {
      return bb;
    }

    const auto& mask = Us == Color::BLACK ? RANK4_BB : RANK6_BB;
    tmp = GetEffect<Piece::KI, ~Us>(square) & mask;
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= GetEffect<Piece::GI, ~Us>(target) & GetEnemyField(Us);
    }

    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::GOLDS> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    Bitboard tmp = GetEffect<Piece::KI, ~Us>(square);
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= GetEffect<Piece::KI, ~Us>(target);
    }
    return bb;
  }
};

template <Color::Value Us>
struct CandidateInitializerSquare<Us, PieceTypeCheck::NON_SLIDER> {
  Bitboard operator()(const Square square) const {
    // 他のテーブルに依存しているので、最後に呼ぶ必要がある
    Bitboard bb =
        check_candidate_bb[square][enum_cast<>(PieceTypeCheck::GOLDS)][Us] |
        check_candidate_bb[square][enum_cast<>(PieceTypeCheck::GI)][Us] |
        check_candidate_bb[square][enum_cast<>(PieceTypeCheck::KE)][Us] |
        check_candidate_bb[square]
                          [enum_cast<>(PieceTypeCheck::FU_WITH_PROMOTION)][Us] |
        check_candidate_bb[square][enum_cast<>(
            PieceTypeCheck::FU_WITHOUT_PROMOTION)][Us];
    return bb;
  }
};

template <Color::Value Us, PieceTypeCheck Type>
struct CandidateInitializer {
  static void Call() {
    const CandidateInitializerSquare<Us, Type> initializer;
    for (const Square square : SQUARE) {
      Bitboard tmp = initializer(square);
      // squareは王の位置なので、消しておく
      tmp &= ~Bitboard(square);

      check_candidate_bb[square][enum_cast<>(Type)][Us] = tmp;
    }
  }
};

template <PieceTypeCheck Head, PieceTypeCheck... Tails>
void InitializeCandidateTable() {
  CandidateInitializer<Color::BLACK, Head>::Call();
  CandidateInitializer<Color::WHITE, Head>::Call();

  InitializeCandidateTable<Tails...>();
}
// PieceTypeCheck::NON_SLIDERは最後に呼ぶ
template <>
void InitializeCandidateTable<PieceTypeCheck::NON_SLIDER>() {
  CandidateInitializer<Color::BLACK, PieceTypeCheck::NON_SLIDER>::Call();
  CandidateInitializer<Color::WHITE, PieceTypeCheck::NON_SLIDER>::Call();
}

void InitializeCandidate() {
  InitializeCandidateTable<
      PieceTypeCheck::FU_WITHOUT_PROMOTION, PieceTypeCheck::FU_WITH_PROMOTION,
      PieceTypeCheck::KY, PieceTypeCheck::KE, PieceTypeCheck::GI,
      PieceTypeCheck::GOLDS, PieceTypeCheck::NON_SLIDER>();
}
}  // namespace detail
}  // namespace mate
}  // namespace state_action
