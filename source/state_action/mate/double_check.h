#pragma once
#ifndef DOUBLE_CHECK_H_INCLUDED
#define DOUBLE_CHECK_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "check_data.h"

namespace state_action {
namespace mate {
namespace near_move {
/**
 * @brief sourceからtargetへ移動することで両王手になっているかどうか
 *
 * Pは移動前と移動後が混在している
 * 歩、香、桂は移動後、それ以外は移動前
 * 歩、香、桂が移動して成る場合はそれぞれTO, NY, NK
 * 不成の場合はそのままFU, KY, KEを指定する
 * と、成香、成桂が動く場合は金と同じとして処理する
 * @param data
 * @param source
 * @param target
 * @return
 */
template <Color::Value Us, Piece::Value P>
bool IsDoubleCheck(const CheckData<Us>& data, const Square source,
                   const Square target) {
  static_assert(P != Piece::OU, "");

  switch (P) {
      // 両王手はない
    case Piece::FU:
    case Piece::KY:
    case Piece::RY:  // 角の利きを遮りつつ移動後に隣接して王手するためには
                     // 最初から隣接している必要がある
      return false;
      // 両王手の候補が移動して王手しているということだけで直線と違う方向
    case Piece::KE:
    case Piece::KA:
    case Piece::HI:
    case Piece::TO:  // 角の利きを塞いているパターンのみ
    case Piece::NY:  // 角の利きを塞いているパターンのみ
    case Piece::NK:
      return data.dc_candidates & source;
    default:
      // 両王手の候補 and 直線と違う方向に移動 -> 両王手が成立
      return (data.dc_candidates & source) &&
             !IsAligned(source, target, data.their_ou);
  }
}

}  // namespace near_move
}  // namespace mate
}  // namespace state_action
#endif  // !DOUBLE_CHECK_H_INCLUDED
