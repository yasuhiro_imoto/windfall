#pragma once
#ifndef CHECKMATE_NEAR_MOVE_H_INCLUDED
#define CHECKMATE_NEAR_MOVE_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "mate_1ply.h"
#include "position_helper.h"
#include "checkmate_far.h"
#include "double_check.h"
#include "mate_helper.h"

namespace state_action {
namespace mate {
template <Color::Value Us, Piece::Value P>
struct Effect;

template <Color::Value Us>
struct Effect<Us, Piece::RY> {
  inline static Bitboard Get(const Square ou_square, const Square target,
                             const Bitboard& occupied) {
    // 王の斜めの位置への移動か
    // 処理を分けた方が多分速い
    if (state_action::detail::GetKaNaiveEffect(ou_square) & target) {
      return GetEffect<Piece::HI, Us>(target, occupied);
    } else {
      return state_action::detail::GetHiNaiveEffect(target) |
             GetEffect<Piece::OU, Us>(target);
    }
  }
};

template <Color::Value Us>
struct Effect<Us, Piece::UM> {
  inline static Bitboard Get(const Square ou_square, const Square target,
                             const Bitboard& occupied) {
    // 王の上下左右の位置への移動か
    // 処理を分けた方が多分速い
    if (state_action::detail::GetHiNaiveEffect(ou_square) & target) {
      // 王の8近傍と馬の8近傍の共通する部分に利きがあれば十分
      return GetEffect<Piece::OU, Us>(target);
    } else {
      return state_action::detail::GetKaNaiveEffect(target) |
             GetEffect<Piece::OU, Us>(target);
    }
  }
};

template <Color::Value Us>
struct Effect<Us, Piece::HI> {
  /**
   * @brief 成りで王手するの場合の利きを取得する
   * @param ou_square
   * @param targe
   * @param occupied
   * @return
   */
  inline static Bitboard Get(const Square ou_square, const Square target,
                             const Bitboard& occupied) {
    Bitboard tmp = state_action::detail::GetHiNaiveEffect(target);
    // 王の斜めの位置への移動か
    // 処理を分けた方が多分速い
    if (tmp & ou_square) {
      tmp |= GetEffect<Piece::OU, Us>(target);
    } else {
      // 他の駒で利きがさえぎられていないか正確に計算する必要がある
      tmp = GetEffect<Piece::HI, Us>(target, occupied);
    }
    return tmp;
  }
};

template <Color::Value Us>
struct Effect<Us, Piece::KA> {
  /**
   * @brief 成りで王手するの場合の利きを取得する
   * @param ou_square
   * @param targe
   * @param occupied
   * @return
   */
  inline static Bitboard Get(const Square ou_square, const Square target,
                             const Bitboard& occupied) {
    // 大した処理ではないので、王の斜めの位置に移動するかどうかで分岐しない
    // むしろ分岐するコストの方が大きい気がする
    return state_action::detail::GetKaNaiveEffect(target) |
           GetEffect<Piece::OU, Us>(target);
  }
};

template <Color::Value Us, Piece::Value P>
Move IsCheckmatePromotedMajor(const CheckData<Us>& data,
                              const Bitboard& target_mask) {
  constexpr Color::Value them = ~Us;

  Bitboard occupied, effected, candidate, their_pinned;
  Bitboard bb = data.position.template color<Us, P>();
  while (bb) {
    const Square source = bb.Pop();
    occupied = data.position.types() ^ source;
    // 王の8近傍への移動
    candidate = GetEffect<P, Us>(source, occupied) & target_mask;
    if (!candidate) {
      continue;
    }

    // 龍が動くことによってピンが変化する
    // 例えば
    // +HI+RY-KI-OU  ->  +HI * -KI-OU
    //  *  *  *  *        *  * +RY *
    // のようなケースでは-KIが新たにピンされる
    //
    // 馬が動くことによってピンが変化する
    // 例えば
    // +HI+UM-KI-OU     +HI * -KI-OU
    //  *  *  *  *   ->  *  * +UM *
    their_pinned = data.position.pinned(them, source);

    while (candidate) {
      const Square target = candidate.Pop();
      // sourceの駒以外にtargetに利きがあるか
      if (!(data.position.attacker(Us, target, occupied) ^ source)) {
        // 王で捕れる
        continue;
      }

      if (data.position.revealed(source, target, data.our_ou,
                                  data.our_pinned)) {
        // 素抜きをくらうので動かせない
        continue;
      }

      effected = Effect<Us, P>::Get(data.their_ou, target, occupied);

      if (IsEscapable<them>(data.position, target, effected, occupied)) {
        // 逃げられる
        continue;
      }

      // 両王手でない場合は捕ることを考える
      if (!near_move::IsDoubleCheck<Us, P>(data, source, target) &&
          IsCapturable<them>(data.position, target, their_pinned, occupied)) {
        // 捕れる
        continue;
      }

      return Move(source, target);
    }
  }
  return Move::NONE;
}

template <Color::Value Us, Piece::Value P>
Move IsCheckmateMajor(const CheckData<Us>& data, const Bitboard& target_mask) {
  constexpr Color::Value them = ~Us;

  Bitboard occupied, effected, candidate, their_pinned;
  Bitboard bb = data.position.template color<Us, P>();
  while (bb) {
    const Square source = bb.Pop();
    occupied = data.position.types() ^ source;
    // 王の8近傍への移動
    candidate = GetEffect<P, Us>(source, occupied) & target_mask;
    if (!candidate) {
      continue;
    }

    // 飛が動くことによってピンが変化する
    // 例えば
    //  *  *  * +KA      *  *  * +KA
    //  *  * -KE *   ->  *  * -KE *
    //  * +HI *  *       *  *  *  *
    // -OU *  *  *      -OU+HI *  *
    // のようなケースでは-KEが新たにピンされる
    // TODO: 更新が必要なのはかなりのレアケースなので、処理を効率化したい
    their_pinned = data.position.pinned(them, source);

    while (candidate) {
      const Square target = candidate.Pop();
      // sourceの駒以外にtargetに利きがあるか
      if (!(data.position.attacker(Us, target, occupied) ^ source)) {
        // 王で捕れる
        continue;
      }

      if (IsPromotable(Us, source, target)) {
        effected = Effect<Us, P>::Get(data.their_ou, target, occupied);
      } else {
        effected = GetNaiveEffect<P>(target);

        if (!(effected & data.their_ou)) {
          // 王手になっていない
          continue;
        }
      }

      if (data.position.revealed(source, target, data.our_ou,
                                  data.our_pinned)) {
        // 素抜きをくらうので動かせない
        continue;
      }

      if (IsEscapable<them>(data.position, target, effected, occupied)) {
        // 逃げられる
        continue;
      }

      // 両王手でない場合は捕ることを考える
      if (!near_move::IsDoubleCheck<Us, P>(data, source, target) &&
          IsCapturable<them>(data.position, target, their_pinned, occupied)) {
        // 捕れる
        continue;
      }

      return Move(source, target, IsPromotable(Us, source, target));
    }
  }
  return Move::NONE;
}

template <Color::Value Us, Piece::Value P>
struct CheckmateMove;

template <Color::Value Us>
struct CheckmateMove<Us, Piece::RY> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    return IsCheckmatePromotedMajor<Us, Piece::RY>(data, target_mask);
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::HI> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    return IsCheckmateMajor<Us, Piece::HI>(data, target_mask);
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::UM> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    return IsCheckmatePromotedMajor<Us, Piece::UM>(data, target_mask);
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::KA> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    return IsCheckmateMajor<Us, Piece::KA>(data, target_mask);
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::KY> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard occupied, candidate, check_bb;
    Bitboard bb = data.position.template color<Us, Piece::KY>() &
                  GetCheckCandidate<Us, PieceTypeCheck::KY>(data.their_ou);
    while (bb) {
      const Square source = bb.Pop();
      occupied = data.position.types() ^ source;
      // 王の8近傍への移動
      candidate = GetEffect<Piece::KY, Us>(source, occupied) & target_mask;

      // 成りで王手するパターンを基本とする
      // 王の8近傍へ移動するので、成らずのまま王手する場合は王の正面なので含まれる
      check_bb = candidate & GetEffect<Piece::KI, them>(data.their_ou);

      // 香の場合は、ピンが変化するパターンはない

      while (check_bb) {
        const Square target = check_bb.Pop();

        if (target.file() == data.their_ou.file()) {
          // 長い利きで串刺しにする
          // 不成の場合
          if (near_move::IsCheckmate<Us, Piece::KY>(
                  data, occupied, data.their_pinned, source, target)) {
            return Move(source, target);
          }
        }

        if (!target.promotable(Us)) {
          continue;
        }

        // 成りの場合
        if (near_move::IsCheckmate<Us, Piece::NY>(
                data, occupied, data.their_pinned, source, target)) {
          return Move(source, target, true);
        }
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::KI> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard occupied, candidate, check_bb, pinned;
    Bitboard bb = data.position.template color<Us, Piece::GOLDS>() &
                  GetCheckCandidate<Us, PieceTypeCheck::GOLDS>(data.their_ou);

    while (bb) {
      const Square source = bb.Pop();
      occupied = data.position.types() ^ source;
      // 金で王手できる位置への移動
      candidate = GetEffect<Piece::KI, Us>(source) & target_mask;
      if (!candidate) {
        continue;
      }

      occupied = data.position.types() ^ source;
      // ピンが変化するのは王の正面の直線上にいる場合だけ
      // -OU    -OU
      // -KI -> -KI+KI
      // +KI     * 
      // +HI    +HI
      // 他の方向では駒があっても王手するためには捕るしかないのでピンにならない
      pinned = data.position.pinned(them, source);

      while (candidate) {
        const Square target = candidate.Pop();

        if (near_move::IsCheckmate<Us, Piece::KI>(data, occupied, pinned,
                                                  source, target)) {
          return Move(source, target);
        }
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::GI> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard occupied, effected, candidate, check_bb, pinned;
    Bitboard bb = data.position.template color<Us, Piece::GI>() &
                  GetCheckCandidate<Us, PieceTypeCheck::GI>(data.their_ou);

    while (bb) {
      const Square source = bb.Pop();
      // 王の8近傍への移動
      // 不成と成りの両方を考えるので、結局は8近傍になる
      candidate = GetEffect<Piece::OU, Us>(source) & target_mask;
      if (!candidate) {
        continue;
      }

      occupied = data.position.types() ^ source;
      // 斜め方向から王手する場合以外はピンが変化する
      pinned = data.position.pinned(them, source);

      while (candidate) {
        const Square target = candidate.Pop();

        // 不成の場合
        check_bb = GetEffect<Piece::GI, Us>(target);
        if ((check_bb & data.their_ou) &&
            near_move::IsCheckmate<Us, Piece::GI>(data, check_bb, occupied,
                                                  pinned, source, target)) {
          return Move(source, target);
        }

        if (!IsPromotable(Us, source, target)) {
          continue;
        }
        // 成りの場合
        check_bb = GetEffect<Piece::KI, Us>(target);
        if ((check_bb & data.their_ou) &&
            near_move::IsCheckmate<Us, Piece::NG>(data, check_bb, occupied,
                                                  pinned, source, target)) {
          return Move(source, target, true);
        }
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::KE> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard occupied, effected, candidate, check_bb, pinned;
    Bitboard bb = data.position.template color<Us, Piece::KE>() &
                  GetCheckCandidate<Us, PieceTypeCheck::KE>(data.their_ou);

    while (bb) {
      const Square source = bb.Pop();
      candidate = GetEffect<Piece::KE, Us>(source) & target_mask;
      if (!candidate) {
        continue;
      }

      occupied = data.position.types() ^ source;
      // sourceが王の正面と斜め前の場合にピンが変化する
      pinned = data.position.pinned(them, source);

      while (candidate) {
        const Square target = candidate.Pop();

        // 不成の場合
        check_bb = GetEffect<Piece::KE, Us>(target);
        if ((check_bb & data.their_ou) &&
            near_move::IsCheckmate<Us, Piece::KE>(data, check_bb, occupied,
                                                  pinned, source, target)) {
          return Move(source, target);
        }

        if (!IsPromotable(Us, source, target)) {
          continue;
        }
        // 成りの場合
        check_bb = GetEffect<Piece::KI, Us>(target);
        if ((check_bb & data.their_ou) &&
            near_move::IsCheckmate<Us, Piece::NK>(data, check_bb, occupied,
                                                  pinned, source, target)) {
          return Move(source, target, true);
        }
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateMove<Us, Piece::FU> {
  inline static Move IsCheckmateWithNearMove(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard occupied, effected, candidate, check_bb, pinned;
    if (data.position.template color<Us, Piece::FU>() &
        GetCheckCandidate<Us, PieceTypeCheck::FU_WITHOUT_PROMOTION>(
            data.their_ou)) {
      // if文の条件で盤の外でないことが保証される
      const Square target = data.their_ou + Forward<them>::value;
      const Square source = target + Forward<them>::value;
      if (data.position[target] != Piece::EMPTY &&
          data.position[target].color() != Us && !target.promotable(Us)) {
        // 成れない場合のみを扱う
        occupied = data.position.types() ^ source;
        effected = Bitboard::Zero;
        if (near_move::IsCheckmate<Us, Piece::FU>(
                data, effected, occupied, data.their_pinned, source, target)) {
          return Move(source, target);
        }
      }
    }

    Bitboard bb =
        data.position.template color<Us, Piece::FU>() &
        GetCheckCandidate<Us, PieceTypeCheck::FU_WITH_PROMOTION>(data.their_ou);
    while (bb) {
      const Square source = bb.Pop();
      const Square target = source + Forward<Us>::value;
      if (data.position[target] != Piece::EMPTY &&
          data.position[target].color() == Us) {
        continue;
      }

      check_bb = GetEffect<Piece::KI, Us>(target);

      occupied = data.position.types() ^ source;
      if (near_move::IsCheckmate<Us, Piece::TO>(
              data, check_bb, occupied, data.their_pinned, source, target)) {
        return Move(source, target, true);
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
inline Move IsCheckmateWithNearMove(const CheckData<Us>& data) {
  const Bitboard movable = ~data.position.template color<Us>();
  const Bitboard target_mask1 = GetEffect<Piece::OU, ~Us>(data.their_ou);
  const Bitboard target_mask2 = GetEffect<Piece::KI, ~Us>(data.their_ou);

  // 可能性が高そうな順に調べる
  // 途中に離れて王手する場合をねじ込む
  Move move;
  // clang-format off
  if ((move = CheckmateMove<Us, Piece::RY>::IsCheckmateWithNearMove(data, target_mask1)) ||
      (move = CheckmateMove<Us, Piece::UM>::IsCheckmateWithNearMove(data, target_mask1)) ||
      (move = CheckmateMove<Us, Piece::HI>::IsCheckmateWithNearMove(data, target_mask1)) ||
      (move = CheckmateMove<Us, Piece::KA>::IsCheckmateWithNearMove(data, target_mask1)) ||
      (move = CheckmateMove<Us, Piece::KY>::IsCheckmateWithNearMove(data, target_mask2)) ||
      (move = IsCheckmateFar<Us>(data)) ||  
      (move = CheckmateMove<Us, Piece::KI>::IsCheckmateWithNearMove(data, target_mask2)) ||
      (move = CheckmateMove<Us, Piece::GI>::IsCheckmateWithNearMove(data, target_mask1)) ||
      (move = CheckmateMove<Us, Piece::KE>::IsCheckmateWithNearMove(data, movable)) ||
      (move = CheckmateMove<Us, Piece::FU>::IsCheckmateWithNearMove(data, target_mask2))) {
    return move;
  }
  // clang-format on
  return Move::NONE;
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_NEAR_MOVE_H_INCLUDED
