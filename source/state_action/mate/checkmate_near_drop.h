#pragma once
#ifndef CHECKMATE_NEAR_DROP_H_INCLUDED
#define CHECKMATE_NEAR_DROP_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
template <Color::Value Us, Piece::Value P>
struct CheckmateDrop {
  static_assert(P == Piece::KI || P == Piece::GI, "");

  inline static Move IsCheckmateWithNearDrop(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard bb = GetEffect<P, them>(data.their_ou) & target_mask;
    while (bb) {
      const Square target = bb.Pop();
      if (!data.position.attacker(Us, target)) {
        // 打った駒に利きがないので駄目
        continue;
      }

      const Bitboard& occupied = data.position.types();
      // 打った駒の利きがあるのでここに王は移動できない
      const Bitboard& effected = GetEffect<P, Us>(target);
      if (IsEscapable<them>(data.position, target, effected, occupied)) {
        // 王は逃げられる
        continue;
      }
      if (IsCapturable<them>(data.position, target, data.their_pinned,
                             occupied)) {
        // 王以外の駒で捕れる
        continue;
      }

      return Move(P, target);
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateDrop<Us, Piece::HI> {
  inline static Move IsCheckmateWithNearDrop(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    // 王に隣接した打てる場所の内の上下左右
    Bitboard bb =
        state_action::detail::GetHiNaiveEffect(data.their_ou) & target_mask;
    while (bb) {
      const Square target = bb.Pop();
      if (!data.position.attacker(Us, target)) {
        // 打った駒に利きがないので駄目
        continue;
      }

      const Bitboard& occupied = data.position.types();
      // 打った飛の利きがあるのでここに王は移動できない
      const Bitboard& effected = state_action::detail::GetHiNaiveEffect(target);
      if (IsEscapable<them>(data.position, target, effected, occupied)) {
        // 王は逃げられる
        continue;
      }
      if (IsCapturable<them>(data.position, target, data.their_pinned,
                             occupied)) {
        // 王以外の駒で捕れる
        continue;
      }

      return Move(Piece::HI, target);
    }

    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateDrop<Us, Piece::KY> {
  inline static Move IsCheckmateWithNearDrop(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    // 王に隣接させるので歩の利きで十分
    Bitboard bb = GetEffect<Piece::FU, them>(data.their_ou) & target_mask;
    if (!bb) {
      return Move::NONE;
    }

    const Square target = data.their_ou + Forward<them>::value;
    if (!data.position.attacker(Us, target)) {
      // 打った駒に利きがないので駄目
      return Move::NONE;
    }

    const Bitboard& occupied = data.position.types();
    // 香の利きの及ぶ範囲
    const Bitboard& effected =
        state_action::detail::GetKyNaiveEffect<Us>(target);
    if (IsEscapable<them>(data.position, target, effected, occupied)) {
      // 王は逃げられる
      return Move::NONE;
    }
    if (IsCapturable<them>(data.position, target, data.their_pinned,
                           occupied)) {
      // 王以外の駒で捕れる
      return Move::NONE;
    }

    return Move(Piece::KY, target);
  }
};

template <Color::Value Us>
struct CheckmateDrop<Us, Piece::KA> {
  inline static Move IsCheckmateWithNearDrop(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    // 王に隣接した打てる場所の内の斜め4方向
    Bitboard bb =
        state_action::detail::GetKaNaiveEffect(data.their_ou) & target_mask;
    while (bb) {
      const Square target = bb.Pop();
      if (!data.position.attacker(Us, target)) {
        // 打った駒に利きがないので駄目
        continue;
      }

      const Bitboard& occupied = data.position.types();
      // 打った角の利きがあるのでここに王は移動できない
      const Bitboard& effected = state_action::detail::GetKaNaiveEffect(target);
      if (IsEscapable<them>(data.position, target, effected, occupied)) {
        // 王は逃げられる
        continue;
      }
      if (IsCapturable<them>(data.position, target, data.their_pinned,
                             occupied)) {
        // 王以外の駒で捕れる
        continue;
      }

      return Move(Piece::KA, target);
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct CheckmateDrop<Us, Piece::KE> {
  inline static Move IsCheckmateWithNearDrop(const CheckData<Us>& data,
                                             const Bitboard& target_mask) {
    constexpr Color::Value them = ~Us;

    Bitboard bb = GetEffect<Piece::KE, them>(data.their_ou) & target_mask;
    while (bb) {
      const Square target = bb.Pop();
      // 王で捕れるかは確認するまでもない

      const Bitboard& occupied = data.position.types();
      if (IsEscapable<them>(data.position, target, Bitboard::Zero, occupied)) {
        // 王は逃げられる
        continue;
      }
      if (IsCapturable<them>(data.position, target, data.their_pinned,
                             occupied)) {
        // 王以外の駒で捕れる
        continue;
      }

      return Move(Piece::KE, target);
    }
    return Move::NONE;
  }
};

template <Color::Value Us, Piece::Value P>
struct NearDrop {
  inline static Move IsCheckmate(const CheckData<Us>& data,
                                 const Bitboard& target_mask) {
    if (data.position[Us].Exists(P)) {
      return CheckmateDrop<Us, P>::IsCheckmateWithNearDrop(data, target_mask);
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct NearDrop<Us, Piece::KY> {
  inline static Move IsCheckmate(const CheckData<Us>& data,
                                 const Bitboard& target_mask) {
    if (data.position[Us].Exists(Piece::KY) &&
        !data.position[Us].Exists(Piece::HI)) {
      return CheckmateDrop<Us, Piece::KY>::IsCheckmateWithNearDrop(data,
                                                                   target_mask);
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct NearDrop<Us, Piece::KI> {
  inline static Move IsCheckmate(const CheckData<Us>& data,
                                 const Bitboard& target_mask) {
    if (data.position[Us].Exists(Piece::KI)) {
      Bitboard tmp = target_mask;
      if (data.position[Us].Exists(Piece::HI)) {
        // 王の後ろから王手するパターンで詰まないのは飛と同じ
        // 王の後ろを候補から取り除く
        tmp &= ~GetEffect<Piece::FU, Us>(data.their_ou);
      }
      return CheckmateDrop<Us, Piece::KI>::IsCheckmateWithNearDrop(data, tmp);
    }
    return Move::NONE;
  }
};

template <Color::Value Us>
struct NearDrop<Us, Piece::GI> {
  inline static Move IsCheckmate(const CheckData<Us>& data,
                                 const Bitboard& target_mask) {
    if (data.position[Us].Exists(Piece::GI)) {
      Bitboard tmp = target_mask;
      if (data.position[Us].Exists(Piece::KI)) {
        if (data.position[Us].Exists(Piece::KA)) {
          // 角と金の両方で詰まないなら銀では詰まない
          return Move::NONE;
        }
        // 王の前から王手するパターンでは詰まない
        // 王の前、3か所を取り除く
        tmp &= ~GetEffect<Piece::KI, ~Us>(data.their_ou);
      }
      return CheckmateDrop<Us, Piece::GI>::IsCheckmateWithNearDrop(data, tmp);
    }
    return Move::NONE;
  }
};

/**
 * @brief
 * @param data 相手側の王に隣接させて駒を打った場合に詰んでいるか
 * @param cache
 * @return 詰んでいる場合はその指し手(16bit)を返す。それ以外はMove::NONEを返す。
 */
template <Color::Value Us>
inline Move IsCheckmateWithNearDrop(const CheckData<Us>& data) {
  const Bitboard empties = data.position.empties();
  const Bitboard target_mask =
      GetEffect<Piece::OU, ~Us>(data.their_ou) & empties;

  Move move;
  if ((move = NearDrop<Us, Piece::HI>::IsCheckmate(data, target_mask)) ||
      (move = NearDrop<Us, Piece::KY>::IsCheckmate(data, target_mask)) ||
      (move = NearDrop<Us, Piece::KA>::IsCheckmate(data, target_mask)) ||
      (move = NearDrop<Us, Piece::KI>::IsCheckmate(data, target_mask)) ||
      (move = NearDrop<Us, Piece::GI>::IsCheckmate(data, target_mask)) ||
      (move = NearDrop<Us, Piece::KE>::IsCheckmate(data, empties))) {
    return move;
  }
  return Move::NONE;
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_NEAR_DROP_H_INCLUDED
