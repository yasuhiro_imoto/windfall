#include "stdafx.h"

#include "mate_1ply.h"

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../position.h"
#include "../../utility/level_assert.h"
#include "position_helper.h"
#include "check_data.h"
#include "checkmate_near_drop.h"
#include "checkmate_near_move.h"

namespace state_action {
namespace mate {
namespace detail {
/**
 * @brief 王手になる候補の駒の位置のテーブル
 *
 * CHECK_CAND_BB
 */
Bitboard check_candidate_bb[Square::SIZE_PLUS1][Piece::TYPE_SIZE][Color::SIZE];

/**
 * @brief 王手の周辺に利きを発生させる駒のテーブル
 *        colorは王手する側の手番
 *
 * CHECK_AROUND_BB
 */
Bitboard check_around_bb[Square::SIZE_PLUS1][Piece::RAW_SIZE][Color::SIZE];

/**
 * @brief
 *
 * NextSquare
 */
uint8_t next_square[Square::SIZE_PLUS1][Square::SIZE_PLUS1];

}  // namespace detail


template<Color::Value Us>
Move IsMateIn1Ply(const Position& position) {
  ASSERT_LV3(!position.checked());

  constexpr Color::Value them = ~Us;
  const CheckData<Us> data(position);

  // 1. 王に隣接して駒を打つ
  //    判定が比較的簡単でかつ最も詰みの可能性が高いと思われる
  // 2. 利きの長い駒で王から離して打つ
  //    判定が簡単な方
  // 3. 空き王手
  //    動かす駒は全パターンを調べる
  //    王の逃げ道を塞いだり移動合を防いだりといくつかパターンが考えられる
  //    両王手になる場合も含む
  // 4. 利きの長い駒で王に隣接して王手
  // 5. 利きの長い駒で王から離れて王手
  // 6. 利きの短い駒で王手

  // 王に隣接して駒を打つ
  if (const auto move = IsCheckmateWithNearDrop<Us>(data); move) {
    return move;
  }
  // 王に隣接した位置へ駒を動かす
  if (const auto move = IsCheckmateWithNearMove<Us>(data); move) {
    return move;
  }
  return Move::NONE;
}
}  // namespace mate

Move state_action::Position::Mate1ply() const {
  if (side() == Color::BLACK) {
    return mate::IsMateIn1Ply<Color::BLACK>(*this);
  } else {
    return mate::IsMateIn1Ply<Color::WHITE>(*this);
  }
}
}  // namespace state_action
