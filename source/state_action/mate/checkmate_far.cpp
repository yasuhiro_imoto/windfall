#include "stdafx.h"

#include "checkmate_far.h"

#include "../bitboard.h"
#include "../direction.h"

namespace state_action {
namespace mate {
std::array<MateDirectionSet, 256> single_table;

effect8::DirectionSet GetEscapableDirections(const Bitboard& escape_candidate,
                                             const Square ou_square) {
  Bitboard bb = escape_candidate;
  effect8::DirectionSet d = effect8::DirectionSet::ZERO;

  while (bb) {
    const auto square = bb.Pop();
    d |= effect8::GetDirection(ou_square, square);
  }
  return d;
}

void InitializeSingleMateTable() {
  // ほとんどのパターンでは詰まないので先に詰まないことを示す値で初期化する
  std::memset(single_table.data(), 0, sizeof(MateDirectionSet) * 256);

  // 逃げ道がないので、何でもいいから王手すればいい
  single_table[0].direction = effect8::DirectionSet::ALL;

  constexpr auto ru = effect8::DirectionSet::RU, ld = effect8::DirectionSet::LD;
  constexpr auto r = effect8::DirectionSet::R, l = effect8::DirectionSet::L;
  constexpr auto rd = effect8::DirectionSet::RD, lu = effect8::DirectionSet::LU;
  constexpr auto u = effect8::DirectionSet::U, d = effect8::DirectionSet::D;

  // 直線に並んでいるなら詰むかもしれない
  constexpr effect8::DirectionSet::Value direction_pair[4][2] = {
      {ru, ld}, {r, l}, {rd, lu}, {u, d}};
  for (int i = 0; i < 4; ++i) {
    const auto& d = direction_pair[i];

    MateDirectionSet tmp;
    tmp.direction = d[0] | d[1];
    if (d[0] & effect8::DirectionSet::DIAGONAL) {
      tmp.use_ka = true;
    } else {
      tmp.use_hi = true;
      if (d[0] & effect8::DirectionSet::VERTICAL) {
        tmp.enable_ky = true;
      }
    }

    single_table[d[0]] = tmp;
    single_table[d[1]] = tmp;
    single_table[d[0] | d[1]] = tmp;
  }

  // 一間龍の形で詰むかもしれない
  MateDirectionSet tmp_r, tmp_l, tmp_u, tmp_d;
  tmp_r.direction = r;
  tmp_l.direction = l;
  tmp_u.direction = u;
  tmp_d.direction = d;
  tmp_r.require_ry = tmp_l.require_ry = tmp_u.require_ry = tmp_d.require_ry =
      true;

  const std::array<MateDirectionSet, 4> directions{tmp_r, tmp_l, tmp_u, tmp_d};
  const effect8::DirectionSet::Value patterns[4][3] = {{ru, rd, ru | rd},
                                                       {lu, ld, lu | ld},
                                                       {ru, lu, ru | lu},
                                                       {rd, ld, rd | ld}};
  const effect8::DirectionSet::Value bases[2][3] = {{r, l, r | l},
                                                    {u, d, u | d}};

  for (int i = 0; i < 4; ++i) {
    for (const auto d1 : bases[i / 2]) {
      for (const auto d2 : patterns[i]) {
        single_table[d1 | d2] = directions[i];
      }
    }
    // 角が単独で詰むかもしれないけど、龍でも詰むかもしれない
    for (const auto d2 : patterns[i]) {
      ASSERT_LV1(d2 == patterns[i][2] || single_table[d2].use_ka);

      single_table[d2].direction |= directions[i].direction;
      single_table[d2].use_hi = single_table[d2].require_ry = true;
    }
  }
}

void InitializeMateTable() {}
}  // namespace mate
}  // namespace state_action
