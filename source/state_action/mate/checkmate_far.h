#pragma once
#ifndef CHECKMATE_FAR_H_INCLUDED
#define CHECKMATE_FAR_H_INCLUDED

#include <array>

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "checkmate_far_drop.h"
#include "checkmate_far_move.h"
#include "mate_1ply.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
effect8::DirectionSet GetEscapableDirections(const Bitboard& escape_candidate,
                                             const Square ou_square);

struct MateDirectionSet {
  effect8::DirectionSet direction;
  bool use_ka : 2;
  bool use_hi : 2;
  /**
   * @brief 縦方向の場合
  */
  bool enable_ky : 2;
  /**
   * @brief 詰ますために龍が必要か
  */
  bool require_ry : 2;
};

/**
 * @brief 単純に王手するときに詰むかどうかのパターンのテーブル
*/
extern std::array<MateDirectionSet, 256> single_table;

void InitializeMateTable();

template <Color::Value Us>
Move IsCheckmateFar(const CheckData<Us>& data) {
  constexpr Color::Value them = ~Us;

  if (data.position[them].hand & 0xFFFFFF00) {
    // 歩以外の駒があるのできっと合駒できるので詰まない
    return Move::NONE;
  }

  const Square their_ou = data.position.template ou_square<them>();
  // 王が移動できるマスの候補
  const Bitboard ou_movable =
      ~data.position.template color<them>() & GetEffect<Piece::OU, them>(data.their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(data.position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(data.position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  bool only_ry;
  const effect8::DirectionSet directions =
      GetCheckLine(escape_candidate, data.their_ou, only_ry);

  if (directions == effect8::DirectionSet::ZERO) {
    return Move::NONE;
  }

  if (only_ry) {
    // 一間龍の形なら詰むかもしれない

    // 方向の候補は最大で2個
    auto tmp = directions;
    auto d = tmp.Pop();
    // TODO: 候補が2個あることを見逃していたので引数が良くない
    //       修正する
    const auto d1 = effect8::DirectionSet(d);
    auto move = IsCheckmateWithRy<Us>(data, d1);
    if (move) {
      return move;
    }
    if (tmp == effect8::DirectionSet::ZERO) {
      return state_action::Move::NONE;
    }
    // 残りの方向
    return IsCheckmateWithRy<Us>(data, tmp);
  }

  // 長い利きで王手すれば詰むかもしれない

  bool drop_ky;
  const effect8::DirectionSet directions_drop =
    GetDropDirection(directions, data.position[them], drop_ky);

  const Bitboard& occupied = data.position.types();

  // TODO: 王手をかけるべき方向はわかっているので、その方向でマスクをかけるべき
  Bitboard attack_bb =
    data.position.empties() & GetEffect<Piece::OU, them>(data.their_ou);
  while (attack_bb) {
    const Square one = attack_bb.Pop();
    const auto d = effect8::GetDirection(one, data.their_ou);
    if (!(d & directions)) {
      // 逃げ道に利きを発生させるように駒を打つ必要がある
      continue;
    }

    bool enable_drop = static_cast<bool>(d & directions_drop);
    // 逃げ道がなくて王手するだけでいい場合もあるので、
    // ky_dropがtrueだからといっても今の調べている方向が正面とは限らない
    if (enable_drop && drop_ky && (d & effect8::DirectionSet::VERTICAL)) {
      // 香なら正面から王手する必要がある
      if (one != data.their_ou + Forward<them>::value) {
        enable_drop = false;
      }
    }

    const Square target = GetNextSquare(data.their_ou, one);
    if (target == Square::SIZE) {
      continue;
    }

    if (data.position[target] != Piece::EMPTY) {
      enable_drop = false;
      if (data.position[target].color() == Us) {
        continue;
      }
    }
    if (IsFuDroppable<them>(data.position, one) &&
        one.rank() != EndRank<them>::value) {
      continue;
    }

    if (enable_drop) {
      const Move move =
          IsCheckmateFarDrop<Us>(data, d, occupied, ou_movable,
                                 non_sliding_around, one, target, drop_ky);
      if (move != Move::NONE) {
        return move;
      }
    }

    const Move move = IsCheckmateFarMove<Us>(data, d, one, occupied);
    if (move != Move::NONE) {
      return move;
    }
  }

  return Move::NONE;
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_FAR_H_INCLUDED
