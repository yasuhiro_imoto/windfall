#pragma once
#ifndef POSITION_HELPER_H_INCLUDED
#define POSITION_HELPER_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../position.h"
#include "check_data.h"

namespace state_action {
namespace mate {
/**
 * @brief Usのsliderの利きを列挙する
 * @param position
 * @param slide
 * @return
 * AttacksSlider
 */
template <Color::Value Us>
Bitboard GetSlidingEffects(const Position& position, const Bitboard& slide) {
  Bitboard bb, sum = Bitboard::Zero;
  Square source;

  bb = position.color<Us, Piece::KY>();
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KY, Us>(source, slide);
  }

  bb = position.color<Us, Piece::BISHOP_HORSE>();
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KA, Us>(source, slide);
  }

  bb = position.color<Us, Piece::ROOK_DRAGON>();
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::HI, Us>(source, slide);
  }

  return sum;
}

/**
 * @brief Usのsliderの利きを列挙する
 * @param position
 * @param excluded このマスの駒は利きを列挙する対象から除外する
 * @param occupied
 * @return
 * AttacksSlider
 */
template <Color::Value Us>
Bitboard GetSlidingEffects(const Position& position, const Square excluded,
                           const Bitboard& occupied) {
  Bitboard bb, sum = Bitboard::Zero;
  Bitboard mask = ~Bitboard(excluded);
  Square source;

  bb = position.color<Us, Piece::KY>() & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KY, Us>(source, occupied);
  }

  bb = position.color<Us, Piece::BISHOP_HORSE>() & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KA, Us>(source, occupied);
  }

  bb = position.color<Us, Piece::ROOK_DRAGON>() & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::HI, Us>(source, occupied);
  }

  return sum;
}

/**
 * @brief Usの王の周辺に対する敵の駒の利きを列挙する
 * @param position
 * @return
 * AttacksAroundKingNonSlider
 */
template <Color::Value Us>
Bitboard GetNonSlidingEffectsAround(const Position& position) {
  const Square ou_square = position.ou_square<Us>();
  constexpr Color::Value them = ~Us;
  Square source;
  Bitboard bb;

  Bitboard sum = GetFuEffect(them, position.color<them, Piece::FU>());

  bb = position.color<them, Piece::KE>() &
       GetCheckAround<Piece::KE>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KE, them>(source);
  }

  bb = position.color<them, Piece::GI>() &
       GetCheckAround<Piece::GI>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::GI, them>(source);
  }

  bb = position.color<them, Piece::GOLDS>() &
       GetCheckAround<Piece::KI>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KI, them>(source);
  }

  bb = position.color<them, Piece::HDK>() &
       GetCheckAround<Piece::OU>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::OU, them>(source);
  }

  return sum;
}

/**
 * @brief Usの王の周辺に対する敵の駒の利きを列挙する
 * @param position
 * @return
 * AttacksAroundKingSlider
 */
template <Color::Value Us>
Bitboard GetSlidingEffectsAround(const Position& position) {
  const Square ou_square = position.ou_square<Us>();
  constexpr Color::Value them = ~Us;
  Square source;
  Bitboard bb;
  const Bitboard& occupied = position.types();

  Bitboard sum = Bitboard::Zero;

  bb = position.color<them, Piece::KY>() &
       GetCheckAround<Piece::KY>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KY, them>(source, occupied);
  }

  bb = position.color<them, Piece::BISHOP_HORSE>() &
       GetCheckAround<Piece::KA>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KA, them>(source, occupied);
  }

  bb = position.color<them, Piece::ROOK_DRAGON>() &
       GetCheckAround<Piece::HI>(them, ou_square);
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::HI, them>(source, occupied);
  }

  return sum;
}

/**
 * @brief Usの王の周辺に対する敵の駒の利きを列挙する
 * @param position
 * @param excluded このマスの駒は利きを列挙する対象から除外する
 * @return
 * AttacksAroundKingNonSliderInAvoiding
 */
template <Color::Value Us>
Bitboard GetNonSlidingEffectsAround(const Position& position,
                                    const Square excluded) {
  constexpr Color::Value them = ~Us;
  Square source;
  Bitboard bb;
  const Bitboard mask = ~Bitboard(excluded);
  const Bitboard& occupied = position.types();
  const Square ou_square = position.ou_square<them>();

  Bitboard sum = GetFuEffect(them, position.color<them, Piece::FU>()) & mask;

  bb = position.color<them, Piece::KE>() &
       GetCheckAround<Piece::KE>(them, ou_square) & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KE, them>(source);
  }

  bb = position.color<them, Piece::GI>() &
       GetCheckAround<Piece::GI>(them, ou_square) & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::GI, them>(source);
  }

  bb = position.color<them, Piece::GOLDS>() &
       GetCheckAround<Piece::KI>(them, ou_square) & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::KI, them>(source);
  }

  bb = position.color<them, Piece::HDK>() &
       GetCheckAround<Piece::OU>(them, ou_square) & mask;
  while (bb) {
    source = bb.Pop();
    sum |= GetEffect<Piece::OU, them>(source);
  }

  return sum;
}

/**
 * @brief Usの玉の周辺に対する相手の駒の利きを求める
 * @param position
 * @param excluded このマスの駒は利きを列挙する対象から除外する
 *                 このマスの駒は利きの長い駒
 * @param occupied 相手が駒を動かした後に王を動かせる場所を考えるので、
 *                 相手の動かす駒と王を取り除く必要がある
 * @return
 * AttacksAroundKingInAvoiding
 */
template <Color::Value Us>
inline Bitboard GetEffectsAround(const Position& position,
                                 const Square excluded,
                                 const Bitboard& occupied) {
  return GetNonSlidingEffectsAround<Us>(position, excluded) |
         GetSlidingEffects<~Us>(position, excluded, occupied);
}

/**
 * @brief 歩を打てるかどうか
 *        二歩の判定も含む
 * @param position
 * @param target
 * @return
 * can_pawn_drop
 */
template <Color::Value Us>
inline bool IsFuDroppable(const Position& position, const Square target) {
  return position[Us].Exists(Piece::FU) &&
         !(position.color<Us, Piece::FU>() & FILE_BB[target.file()]);
}

/**
 * @brief Usの王がtargetとexcluded_bb以外の升に移動できるか
 * @param position
 * @param target ここに駒が移動してくる
 * @param excluded_bb
 * @param slide 王を取り除いたoccupied bitboard
 * @return
 * can_king_escape
 */
template <Color::Value Us>
bool IsEscapable(const Position& position, const Square target,
                 const Bitboard& excluded_bb, const Bitboard& slide) {
  // targetに駒が移動する
  // 王がslideの中に含まれないことは呼び出し側の責任で保証されている
  const Bitboard occupied = slide | target;
  const Square ou_square = position.ou_square<Us>();

  Bitboard bb = GetEffect<Piece::OU, Us>(ou_square) &
                ~(excluded_bb | target | position.color<Us>());
  while (bb) {
    const Square candidate = bb.Pop();
    if (!position.attacker(~Us, candidate, occupied)) {
      return true;
    }
  }
  return false;
}

/**
 * @brief Usの王がtargetとexcluded_bb以外の升に移動できるか
 * @param position
 * @param source ここの駒は移動する
 * @param target ここに駒が移動してくる
 * @param excluded_bb
 * @param slide occupied bitboard
 * @return
 * can_king_escape
 */
template <Color::Value Us>
bool IsEscapable(const Position& position, const Square source,
                 const Square target, const Bitboard& excluded_bb,
                 const Bitboard& slide) {
  const Square ou_square = position.ou_square<Us>();
  // targetに駒が移動する
  const Bitboard occupied = (slide | target) ^ Bitboard(ou_square);

  const Bitboard mask = ~Bitboard(source);

  Bitboard bb = GetEffect<Piece::OU, Us>(ou_square) &
                ~(excluded_bb | target | position.color<Us>());
  while (bb) {
    const Square candidate = bb.Pop();
    if (!(position.attacker(~Us, candidate, occupied) & mask)) {
      return true;
    }
  }
  return false;
}
/**
 * @brief Usの王がexcluded_bb以外の升に移動できるか
 * @param position
 * @param source ここの駒は移動する
 * @param target ここに駒が移動してくる
 * @param excluded_bb
 * @param slide occupied bitboard
 * @return
 * can_king_escape_cangoto
 */
template <Color::Value Us>
bool IsEscapable2(const Position& position, const Square source,
                  const Square target, const Bitboard& excluded_bb,
                  const Bitboard& slide) {
  const Square ou_square = position.ou_square<Us>();
  // targetに駒が移動する
  const Bitboard occupied = (slide | target) ^ Bitboard(ou_square);

  const Bitboard mask = ~Bitboard(source);

  // targetに自分の駒がいても捕られるので、移動可能な候補になる
  // Bitboard bb = GetEffect<Piece::OU, Us>(ou_square) &
  //              ~((excluded_bb | position.color<Us>()) & ~Bitboard(target));
  Bitboard bb = GetEffect<Piece::OU, Us>(ou_square) &
                (~(excluded_bb | position.color<Us>()) | target);
  while (bb) {
    const Square candidate = bb.Pop();
    if (!(position.attacker(~Us, candidate, occupied) & mask)) {
      return true;
    }
  }
  return false;
}

/**
 * @brief 王以外の駒でtargetの位置の駒を捕れるか
 *
 * targetの位置には敵の駒の利きがあるか王では届かない位置とする
 * @param position
 * @param target
 * @param pinned
 * @param slide occupied bitboard
 * @return
 * can_piece_capture
 */
template <Color::Value Us>
bool IsCapturable(const Position& position, const Square target,
                  const Bitboard& pinned, const Bitboard& slide) {
  const Square ou_square = position.ou_square<Us>();
  // targetの位置への利きのある駒
  // ただし、王は除く
  Bitboard bb =
      position.attacker(Us, target, slide) & ~position.types<Piece::OU>();
  while (bb) {
    const Square source = bb.Pop();
    if (!pinned || !(pinned & source) || IsAligned(source, target, ou_square)) {
      return true;
    }
  }
  return false;
}

/**
 * @brief 王以外の駒でtargetの位置の駒を捕れるか
 *
 * targetの位置には敵の駒の利きがあるか王では届かない位置とする
 * excludedのマスの駒では捕らないものとする
 * @param position
 * @param target
 * @param excluded この位置の駒では捕らない
 * @param pinned
 * @param slide occupied bitboard
 * @return
 * can_piece_capture
 */
template <Color::Value Us>
bool IsCapturable(const Position& position, const Square target,
                  const Square excluded, const Bitboard& pinned,
                  const Bitboard& slide) {
  const Square ou_square = position.ou_square<Us>();
  // targetの位置への利きのある駒
  // ただし、王は除く
  Bitboard bb = position.attacker(Us, target, slide) &
                ~(position.types<Piece::OU>() | excluded);
  while (bb) {
    const Square source = bb.Pop();
    if (!pinned || !(pinned & source) || IsAligned(source, target, ou_square)) {
      return true;
    }
  }
  return false;
}

/**
 * @brief 王以外の駒でtargetの位置の駒を捕れるか
 *
 * targetの位置には敵の駒の利きがあるか王では届かない位置とする
 * excludedの駒では捕らないものとする
 * excludedのどこかに駒が移動するので、本当は捕れない可能性がある
 * @param position 
 * @param target 
 * @param excluded 相手に取られる可能性があるので、この駒では捕らない
 * @param pinned 
 * @param occupied 
 * @return 
*/
template <Color::Value Us>
bool IsCapturable(const Position& position, const Square target,
                  const Bitboard& excluded, const Bitboard& pinned,
                  const Bitboard& occupied) {
  // targetの位置への利きのある駒
  // ただし、王は除く
  Bitboard bb = position.attacker(Us, target, occupied) &
                ~(position.types<Piece::OU>() | excluded);

  // ピンされていない駒があれば、それで捕れる
  if (bb & ~pinned) {
    return true;
  }

  // ピンされているものを調べる
  const Square ou_square = position.ou_square<Us>();
  bb &= pinned;
  while (bb) {
    const Square source = bb.Pop();
    if (IsAligned(source, target, ou_square)) {
      return true;
    }
  }

  return false;
}

inline Bitboard GetDirectionEffect(const effect8::DirectionSet direction,
                                   const Bitboard& occupied,
                                   const Square square) {
  Bitboard effect;
  switch (direction.value) {
    case effect8::DirectionSet::U:
    case effect8::DirectionSet::D:
      effect = state_action::detail::GetHiFileEffect(square, occupied);
      break;
    case effect8::DirectionSet::R:
    case effect8::DirectionSet::L:
      effect = state_action::detail::GetHiRankEffect(square, occupied);
      break;
    case effect8::DirectionSet::RU:
    case effect8::DirectionSet::LD:
      effect = state_action::detail::GetKaDiagonalEffect<0>(square, occupied);
      break;
    case effect8::DirectionSet::RD:
    case effect8::DirectionSet::LU:
      effect = state_action::detail::GetKaDiagonalEffect<1>(square, occupied);
      break;
    default:
      UNREACHABLE;
      break;
  }
  return effect;
}

/**
 * @brief 空き王手の時に利きの発生源を求める
 * @param direction 
 * @param occupied 
 * @param ou_square 
 * @param pieces 王手する側の駒
 * @return 
*/
inline Square GetSourceSquare(const effect8::DirectionSet direction,
                              const Bitboard& occupied, const Square square,
                              const Bitboard& pieces) {
  Bitboard effect = GetDirectionEffect(direction, occupied, square);
  // 自分の駒があるマスだけが残る
  effect &= pieces;
  const Square source = effect.Pop();
  return source;
}

/**
 * @brief Usの駒がsquareマスに移動可能かどうか
 *
 * squareに自分の駒がなければ移動可能とする
 * @param data 
 * @param square 
 * @return 
*/
template <Color::Value Us>
bool IsMovable(const CheckData<Us>& data, const Square square) {
  return data.position[square] == Piece::EMPTY ||
         data.position[square].color() != Us;
}
}  // namespace mate
}  // namespace state_action
#endif  // !POSITION_HELPER_H_INCLUDED
