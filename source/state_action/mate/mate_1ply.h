#pragma once
#ifndef MATE_1PLY_H_INCLUDED
#define MATE_1PLY_H_INCLUDED

#include <cstdint>

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../move_helper.h"

namespace state_action {
namespace mate {
enum class PieceTypeCheck {
  FU_WITHOUT_PROMOTION,  //!< 不成のまま王手(成れる場合は含まない)
  FU_WITH_PROMOTION,     //!< 成りで王手
  KY,
  KE,
  GI,
  GOLDS,
  KA,
  HI,
  UM,
  RY,
  NON_SLIDER,  //!< 王手になる非遠方駒の移動元
  SIZE,
  ZERO = 0
};

namespace detail {
/**
 * @brief 王手になる候補の駒の位置のテーブル
 *
 * CHECK_CAND_BB
 */
extern Bitboard check_candidate_bb[Square::SIZE_PLUS1][Piece::TYPE_SIZE]
                                  [Color::SIZE];

/**
 * @brief 王手の周辺に利きを発生させる駒のテーブル
 *        colorは王手する側の手番
 *
 * CHECK_AROUND_BB
 */
extern Bitboard check_around_bb[Square::SIZE_PLUS1][Piece::RAW_SIZE]
                               [Color::SIZE];

/**
 * @brief s1からs2の方向に直線を伸ばしてs2の一つ向こう側のマスのテーブル
 *
 * NextSquare
 */
extern uint8_t next_square[Square::SIZE_PLUS1][Square::SIZE_PLUS1];

void InitializeCandidate();
void InitializeAround();
void InitializeNextSquare();
}  // namespace detail
/**
 * @brief 移動すると王手できる駒の今の位置を返す
 *        Us側が王手する
 * @param ou_square
 * @return
 * check_cand_bb
 */
template <Color::Value Us, PieceTypeCheck Type>
FORCE_INLINE const Bitboard& GetCheckCandidate(const Square ou_square) {
  return detail::check_candidate_bb[ou_square][enum_cast<>(Type)][Us];
}

template <Color::Value Us>
FORCE_INLINE bool IsCheckCandidate(const Square source, const Piece piece_type,
                                   const Square ou_square) {
  switch (piece_type.value) {
    case Piece::FU:
      if ((source + Forward<Us>::value).promotable(Us)) {
        return GetCheckCandidate<Us, PieceTypeCheck::FU_WITH_PROMOTION>(
                   ou_square) &
               source;
      } else {
        return GetCheckCandidate<Us, PieceTypeCheck::FU_WITHOUT_PROMOTION>(
                   ou_square) &
               source;
      }
    case Piece::KY:
      return GetCheckCandidate<Us, PieceTypeCheck::KY>(ou_square) & source;
    case Piece::KE:
      return GetCheckCandidate<Us, PieceTypeCheck::KE>(ou_square) & source;
    case Piece::GI:
      return GetCheckCandidate<Us, PieceTypeCheck::GI>(ou_square) & source;
    case Piece::KA:
      return GetCheckCandidate<Us, PieceTypeCheck::KA>(ou_square) & source;
    case Piece::HI:
      return GetCheckCandidate<Us, PieceTypeCheck::HI>(ou_square) & source;
    case Piece::KI:
    case Piece::TO:
    case Piece::NY:
    case Piece::NK:
    case Piece::NG:
      return GetCheckCandidate<Us, PieceTypeCheck::GOLDS>(ou_square) & source;
    case Piece::OU:
      return false;
    case Piece::UM:
      return GetCheckCandidate<Us, PieceTypeCheck::UM>(ou_square) & source;
    case Piece::RY:
      return GetCheckCandidate<Us, PieceTypeCheck::RY>(ou_square) & source;
    default:
      UNREACHABLE;
      break;
  }
}

/**
 * @brief 敵玉の8近傍に利きを付けるUs側の駒の位置を返す
 * @param us
 * @param ou_square
 * @return
 * check_around_bb
 */
template <Piece::Value P>
FORCE_INLINE const Bitboard& GetCheckAround(const Color us,
                                            const Square ou_square) {
  return detail::check_around_bb[ou_square][P - 1][us];
}
/**
 * @brief 敵玉の8近傍に利きを付けるUs側の駒の位置を返す
 *
 * 今いる場所で王の周辺に利きを作っている駒のマス
 * @param piece 
 * @param ou_square 
 * @return 
*/
template <Color::Value Us>
FORCE_INLINE const Bitboard& GetCheckAround(const Piece piece,
                                            const Square ou_square) {
  return detail::check_around_bb[ou_square][piece - 1][Us];
}

/**
 * @brief sourceにある駒が王の周辺に利きを作られるか
 * @param us 
 * @param source 
 * @param piece 
 * @param ou_square 
 * @return 
*/
FORCE_INLINE bool IsCheckAround(const Color us, const Square source,
                                const Piece piece, const Square ou_square) {
  return detail::check_around_bb[ou_square][piece - 1][us] & source;
}

/**
 * @brief
 * @param sq1
 * @param sq2
 * @return
 * nextSquare
 */
FORCE_INLINE Square GetNextSquare(const Square sq1, const Square sq2) {
  return static_cast<Square::Value>(detail::next_square[sq1][sq2]);
}

/**
 * @brief テーブルを初期化
 *
 * init_check_bb
 */
inline void InitializeCheck() {
  detail::InitializeCandidate();
  detail::InitializeAround();
  detail::InitializeNextSquare();
}
}  // namespace mate
}  // namespace state_action
#endif  // !MATE_1PLY_H_INCLUDED
