#pragma once
#ifndef MATE_HELPER_H_INCLUDED
#define MATE_HELPER_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "double_check.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
/**
 * @brief 利きの長い駒で王手されているときに歩を打つか駒を移動させるかして
 *        王手を防げるか
 *        Usは守り方
 *        空き王手の時の合駒判定
 *
 * @param data 攻め方のデータ
 * @param effect_source 利きを発生させているマス
 * @param excluded このマスの駒は守りに含めない
 * @param pin ピンされているUsの駒
 * @param occupied
 * @return
 */
template <Color::Value Us>
bool IsInterceptable(const CheckData<~Us>& data, const Square effect_source,
                     const Square excluded, const Bitboard& pin,
                     const Bitboard& occupied) {
  Bitboard between = GetBetweenBB(data.template ou_square<Us>(), effect_source);
  while (between) {
    const Square s = between.Pop();

    if (IsFuDroppable<Us>(data.position, s)) {
      return true;
    }
    if (IsCapturable<Us>(data.position, s, excluded, pin, occupied)) {
      // 移動合ができる
      return true;
    }
  }
  if (IsCapturable<Us>(data.position, effect_source, excluded, pin, occupied)) {
    // 利きを発生させている駒を捕れる
    return true;
  }
  return false;
}

/**
 * @brief 王手されているときに王が逃げられるかどうか
 *        Usは守り方
 *
 * @param data 攻め方のデータ
 * @param source この駒を移動させて王手にする
 *               空き王手もある
 *               とにかく移動させる駒
 * @param effected sourceにあった駒が移動した後の利き
 * @param occupied sourceからtargetへ駒が移動することを考慮したoccupied bitboard
 * @return
 */
template <Color::Value Us>
bool IsEscapable(const CheckData<~Us>& data, const Square source,
                 const Bitboard& effected, const Bitboard& occupied) {
  // 王の周辺の利き
  //
  // 空き王手でないならばeffectedは王手している駒の利き
  // その時、王手している駒の利きが長いならnaiveな利きをしているするので、
  // occupied bitboardから王を取り除く必要はない
  //
  // TODO: 空き王手用の周辺の利きを求める関数を作る
  //       王手している駒の長い利きはnaiveで良い
  const Bitboard effect_around = GetEffectsAround<Us>(
      data.position, source, occupied ^ data.template ou_square<Us>());
  return GetEffect<Piece::OU, Us>(data.template ou_square<Us>()) &
         ~(effect_around | effected | data.position.template color<Us>());
}

// 空き王手関係
namespace revealed {
/**
 * @brief 空き王手の時に王手している利きの長い駒のマスを求める
 * @param data
 * @param source 空き王手の時に移動させる駒のあるマス
 * @param occupied
 * @return
 */
template <Color::Value Us>
Square GetEffectSource(const CheckData<Us>& data, const Square source,
                       const Bitboard& occupied) {
  const effect8::DirectionSet direction =
      effect8::GetDirection(source, data.template ou_square<~Us>());
  Bitboard tmp = GetDirectionEffect(direction, occupied, source) &
                 data.position.template color<Us>();
  ASSERT_LV3(tmp);

  const Square effect_source = tmp.Pop();

  return effect_source;
}

/**
 * @brief 空き王手の時に合駒か王手している駒を捕れるか
 * @param data
 * @param effect_source 空き王手の際に王手している利きの長い駒のあるマス
 * @param target_mask 空き王手の際に取られるかもしれない駒の候補
 * @return
 */
template <Color::Value Us>
bool IsInterceptable(const CheckData<~Us>& data, const Square effect_source,
                     const Bitboard& target_mask) {
  Bitboard bb = GetBetweenBB(data.template ou_square<Us>(), effect_source);
  while (bb) {
    const Square s = bb.Pop();
    if (IsFuDroppable<Us>(data.position, s)) {
      // 歩で合駒できる
      return true;
    }
    if (IsCapturable<Us>(data.position, s, target_mask, data.pinned[Us],
                         data.position.types())) {
      // 移動合ができる
      return true;
    }
  }
  if (IsCapturable<Us>(data.position, effect_source, target_mask,
                       data.pinned[Us], data.position.types())) {
    // 王手している駒を捕れる
    return true;
  }

  return false;
}

/**
 * @brief target_maskにある駒で移動合ができるかどうか
 * @param data
 * @param effect_source 王手している利きの長い駒のあるマス
 * @param target_mask
 * @param source 空き王手の際に移動させた駒の移動元
 * @param target 空き王手の際に移動させた駒の移動先
 * @return
 */
template <Color::Value Us>
bool IsInterceptable(const CheckData<~Us>& data, const Square effect_source,
                     const Bitboard& target_mask, const Square source,
                     const Square target) {
  const Bitboard& between = GetBetweenBB(data.template ou_square<Us>(), effect_source);
  // targetへ移動したことで、ピンが解除されるかも
  const Bitboard new_pin = data.position.pinned(Us, source, target);

  Bitboard bb = target_mask & ~(data.position.empties() | target);
  while (bb) {
    const Square s = bb.Pop();
    Bitboard tmp =
        between & GetEffect(data.position[s], s, data.position.types());
    if (!tmp) {
      // 利きを遮る位置へ移動できない
      continue;
    }

    if (new_pin && (new_pin & s)) {
      // ピンされている
      while (tmp) {
        const Square p = tmp.Pop();
        if (IsAligned(s, p, data.template ou_square<Us>())) {
          // 大丈夫な方向へ移動する
          return true;
        }
      }
    } else {
      // ピンされていないので、移動して問題ない
      return true;
    }
  }
  return false;
}

template <Color::Value Us>
bool IsFuDroppable(const CheckData<~Us>& data, const Square source,
                   const Square target, const Square effect_source) {
  const effect8::DirectionSet direction =
      effect8::GetDirection(data.template ou_square<Us>(), source);
  if (!(direction & effect8::DirectionSet::VERTICAL) &&
      data.position[target].type() == Piece::FU) {
    // 歩を捕ったので歩で合駒できるかも

    const auto f1 = data.template ou_square<Us>().file(),
               f2 = effect_source.file();
    if (f1 > f2) {
      if (f1 > target.file() && target.file() > f2) {
        // 合駒できる
        return true;
      }
    } else {
      if (f2 > target.file() && target.file() > f1) {
        // 合駒できる
        return true;
      }
    }
  }
  return false;
}
}  // namespace revealed

// 王に隣接する位置に駒を移動させる
namespace near_move {
namespace detail {
/**
 * @brief 王に隣接した位置に移動して王手するときの詰むかどうかの判定の前半
 * @param data
 * @param occupied
 * @param source
 * @param target
 * @return
 */
template <Color::Value Us, Piece::Value P>
FORCE_INLINE bool IsCheckmate1(const CheckData<Us>& data,
                               const Bitboard& occupied, const Square source,
                               const Square target) noexcept {
  // sourceの駒以外にtargetに利きがあるか
  if (P != Piece::KE &&
      !(data.position.attacker(Us, target, occupied) ^ source)) {
    // 王で捕れる
    return false;
  }
  if (data.position.revealed(source, target, data.template ou_square<Us>(),
                             data.pinned[Us])) {
    // 素抜きをくらうので動かせない
    return false;
  }
  return true;
}

/**
 * @brief 王に隣接した位置に移動して王手するときの詰むかどうかの判定の後半
 * @param data
 * @param effected
 * @param occupied
 * @param their_pinned
 * @param source
 * @param target
 * @return
 */
template <Color::Value Us, Piece::Value P>
FORCE_INLINE bool IsCheckmate2(const CheckData<Us>& data,
                               const Bitboard& effected,
                               const Bitboard& occupied,
                               const Bitboard& their_pinned,
                               const Square source, const Square target) {
  constexpr Color::Value them = ~Us;

  if (IsEscapable<them>(data.position, target, effected, occupied)) {
    // 逃げられる
    return false;
  }
  // 両王手でない場合は捕ることを考える
  if (!near_move::IsDoubleCheck<Us, P>(data, source, target) &&
      IsCapturable<them>(data.position, target, their_pinned, occupied)) {
    // 捕れる
    return false;
  }

  return true;
}

template <Color::Value Us, Piece::Value P>
struct EffectHelper {
  FORCE_INLINE static const Bitboard& call(const Square square) noexcept {
    return GetEffect<P, Us>(square);
  }
};

template <Color::Value Us>
struct EffectHelper<Us, Piece::KY> {
  FORCE_INLINE static const Bitboard& call(const Square square) noexcept {
    return state_action::detail::GetKyNaiveEffect<Us>(square);
  }
};

/**
 * @brief 駒を移動させて王手するときの移動した駒の利きを求める
 *        利きの長い駒は王の向こう側にも利きがあるようにnaiveな利きを求める
 * @param square
 * @return
 */
template <Color::Value Us, Piece::Value P>
FORCE_INLINE const Bitboard& GetEffectHelper(const Square square) noexcept {
  return EffectHelper<Us, P>::call(square);
}
}  // namespace detail

/**
 * @brief 王に隣接した位置に移動して王手するときの詰むかどうか
 * @param data
 * @param effected
 * @param occupied
 * @param their_pinned
 * @param source
 * @param target
 * @return
 */
template <Color::Value Us, Piece::Value P>
inline bool IsCheckmate(const CheckData<Us>& data, const Bitboard& effected,
                        const Bitboard& occupied, const Bitboard& their_pinned,
                        const Square source, const Square target) {
  return detail::IsCheckmate1<Us, P>(data, occupied, source, target) &&
         detail::IsCheckmate2<Us, P>(data, effected, occupied, their_pinned,
                                     source, target);
}

/**
 * @brief 王に隣接した位置に移動して王手するときの詰むかどうか
 *        移動した駒の利きの計算をできるだけ遅延させたバージョン
 * @param data
 * @param occupied
 * @param their_pinned
 * @param source
 * @param target
 * @return
 */
template <Color::Value Us, Piece::Value P>
inline bool IsCheckmate(const CheckData<Us>& data, const Bitboard& occupied,
                        const Bitboard& their_pinned, const Square source,
                        const Square target) {
  if (!detail::IsCheckmate1<Us, P>(data, occupied, source, target)) {
    return false;
  }
  // 利きの計算をできるだけ後回し
  const Bitboard& effected = detail::GetEffectHelper<Us, P>(target);
  return detail::IsCheckmate2<Us, P>(data, effected, occupied, their_pinned,
                                     source, target);
}

}  // namespace near_move
}  // namespace mate
}  // namespace state_action
#endif  // !MATE_HELPER_H_INCLUDED
