#pragma once
#ifndef CHECKMATE_FAR_DROP_H_INCLUDED
#define CHECKMATE_FAR_DROP_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "mate_1ply.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
/**
 * @brief 利きの長い駒で王手する方向を求める
 * @param escape_candidate
 * @param their_ou
 * @return
 */
inline effect8::DirectionSet GetCheckLine(const Bitboard& escape_candidate,
                                          const Square their_ou) {
  const int escape_count = escape_candidate.PopCount();
  effect8::DirectionSet directions;
  if (escape_count > 2) {
    // 利きの長い駒を打っても詰まない
    return effect8::DirectionSet::ZERO;
  } else if (escape_count == 2) {
    Bitboard tmp = escape_candidate;
    const Square s1 = tmp.Pop();
    const Square s2 = tmp.Pop();

    directions = effect8::GetDirection(s1, their_ou);
    const auto d = effect8::GetDirection(their_ou, s2);
    if (directions != d) {
      // 直線でない
      // この2か所は利きの長い駒で同時には塞げない
      return effect8::DirectionSet::ZERO;
    }
    directions |= effect8::GetDirection(their_ou, s1);
  } else if (escape_count == 1) {
    Bitboard tmp = escape_candidate;
    const Square s1 = tmp.Pop();

    directions = effect8::GetDirection(s1, their_ou) |
                 effect8::GetDirection(their_ou, s1);
  } else {
    // 全方向どこからでもいいので王手すればOK
    directions = effect8::DirectionSet::ALL;
  }
  return directions;
}

FORCE_INLINE effect8::DirectionSet GetDropDirection(
    const effect8::DirectionSet directions, const Hand our_hand,
    bool& ky_drop) {
  ky_drop = false;

  if (directions == effect8::DirectionSet::ALL) {
    effect8::DirectionSet d = directions;
    if (!our_hand.Exists(Piece::KA)) {
      d ^= effect8::DirectionSet::DIAGONAL;
    }
    if (!our_hand.Exists(Piece::HI)) {
      d ^= effect8::DirectionSet::CROSS;
    }

    // 角、飛、香のどれか一つは少なくとも持っている
    if (our_hand.Exists(Piece::KY)) {
      ky_drop = true;
      d |= effect8::DirectionSet::VERTICAL;
    }
    return d;
  } else if (directions & effect8::DirectionSet::DIAGONAL) {
    if (our_hand.Exists(Piece::KA)) {
      return directions;
    }
  } else {
    if (our_hand.Exists(Piece::HI)) {
      return directions;
    }

    // 縦方向なら香が打てる
    if (directions & effect8::DirectionSet::VERTICAL) {
      if (our_hand.Exists(Piece::KY)) {
        ky_drop = true;
      }
    }
  }
  return effect8::DirectionSet::ZERO;
}

/**
 * @brief 王から離れて王手するときの駒を打つ場合に固有な処理
 * @param data
 * @param directions
 * @param occupied
 * @param non_sliding_around
 * @param drop_ky
 * @return
 */
template <Color::Value Us>
Move IsCheckmateFarDrop(const CheckData<Us>& data,
                        const effect8::DirectionSet directions,
                        const Bitboard& occupied, const Bitboard& ou_movable,
                        const Bitboard& non_sliding_around, const Square one,
                        const Square target, const bool drop_ky) {
  constexpr Color::Value them = ~Us;

  // 移動合
  // targetより遠くに打ってもoneに移動できるなら必ず利きを遮断できる
  if (IsCapturable<them>(data.position, one, data.their_pinned, occupied)) {
    return Move::NONE;
  }
  if (IsCapturable<them>(data.position, target, data.their_pinned, occupied)) {
    return Move::NONE;
  }

  // 打つことで逃げ道を作ってしまうのは飛か香で角の利きを止めてしまう場合だけ
  if (directions & effect8::DirectionSet::CROSS) {
    const Bitboard occupied2 = occupied | target;
    // 何故かoccupiedを指定できる関数は利きを全列挙している
    // TODO: 特定の方向の角、馬を取り除くだけでいいので差分計算ができる
    const Bitboard sliding_around2 =
        GetSlidingEffects<Us>(data.position, occupied2);
    const Bitboard around_effects2 = non_sliding_around | sliding_around2;

    // TODO: 特定の2マスに利きがあるか、変化したかを検証したいだけ
    //       もう少しいい書き方にしたい

    // 香の場合でも王手しているのは保証されているので、縦の利きだけが重要
    // 飛と区別する必要はない
    if ((ou_movable &
         ~(around_effects2 | state_action::detail::GetHiNaiveEffect(target)))) {
      // 逃げ道ができてしまった

      // もう1マス離して打つ

      const Square target2 = GetNextSquare(one, target);
      if (target2 == Square::SIZE) {
        return Move::NONE;
      }
      if (data.position[target2] != Piece::EMPTY) {
        return Move::NONE;
      }
      if (IsCapturable<them>(data.position, target2, data.their_pinned,
                             occupied)) {
        return Move::NONE;
      }

      // 今度は詰んでいる
    }
  }

  Piece piece_type;
  if (directions & effect8::DirectionSet::DIAGONAL) {
    piece_type = Piece::KA;
  } else {
    // どの方向を調べるかは予め考慮しているので、drop_kyがtrueなら香を打つ
    // drop_kyがfalseなら常に飛でOK
    if (drop_ky) {
      piece_type = Piece::KY;
    } else {
      piece_type = Piece::HI;
    }
  }

  return Move(piece_type, target);
}

/**
 * @brief 利きの長い駒を離して打った時に詰むかどうか
 * @param data
 * @return
 */
template <Color::Value Us>
Move IsCheckmateFarDrop(const CheckData<Us>& data) {
  constexpr Color::Value them = ~Us;

  const auto our_hand = data.position[Us];
  if (!our_hand.Exists(Piece::KY) && !our_hand.Exists(Piece::KA) &&
      !our_hand.Exists(Piece::HI)) {
    return Move::NONE;
  }

  const Square their_ou = data.position.template ou_square<them>();
  // 王が移動できるマスの候補
  const Bitboard ou_movable = ~data.position.template color<them>() &
                              GetEffect<Piece::OU, them>(data.their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(data.position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(data.position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  effect8::DirectionSet directions =
      GetCheckLine(escape_candidate, data.their_ou);

  if (directions == effect8::DirectionSet::ZERO) {
    return Move::NONE;
  } else if (directions == effect8::DirectionSet::ALL) {
    if (!our_hand.Exists(Piece::KA)) {
      directions ^= effect8::DirectionSet::DIAGONAL;
    }
    if (!our_hand.Exists(Piece::HI)) {
      directions ^= effect8::DirectionSet::CROSS;
    }

    // 角、飛、香のどれか一つは少なくとも持っている
    if (our_hand.Exists(Piece::KY)) {
      directions |= effect8::DirectionSet::U | effect8::DirectionSet::D;
    }
  } else if (directions & effect8::DirectionSet::DIAGONAL) {
    if (!our_hand.Exists(Piece::KA)) {
      return Move::NONE;
    }
  } else if (directions &
             (effect8::DirectionSet::R | effect8::DirectionSet::L)) {
    if (!our_hand.Exists(Piece::HI)) {
      return Move::NONE;
    }
  } else {
    if (!our_hand.Exists(Piece::KY) && !our_hand.Exists(Piece::HI)) {
      return Move::NONE;
    }
  }

  const Bitboard& occupied = data.position.template types<>();

  // TODO: 駒を打つべき方向はわかっているので、その方向でマスクをかけるべき
  Bitboard attack_bb =
      data.position.empties() & GetEffect<Piece::OU, them>(data.their_ou);
  while (attack_bb) {
    const Square one = attack_bb.Pop();
    if (!(effect8::GetDirection(one, data.their_ou) & directions)) {
      // 逃げ道に利きを発生させるように駒を打つ必要がある
      continue;
    }

    const Square target = GetNextSquare(data.their_ou, one);
    if (target == Square::SIZE) {
      continue;
    }

    if (data.position[target] != Piece::EMPTY &&
        data.position[target].color() == Us) {
      continue;
    }
    if (IsFuDroppable<them>(data.position, one) &&
        target.rank() != EndRank<them>::value) {
      continue;
    }

    Piece piece_type;
    bool is_ky_attakable = false;
    if (directions & effect8::DirectionSet::DIAGONAL) {
      piece_type = Piece::KA;
    } else {
      piece_type = Piece::HI;

      // 正面なら香が使える
      is_ky_attakable = their_ou - one == Forward<Us>::value;
      if (is_ky_attakable && our_hand.Exists(Piece::KY)) {
        piece_type = Piece::KY;
      }
    }

    // 移動合
    // targetより遠くに打ってもoneに移動できるなら必ず利きを遮断できる
    if (IsCapturable<them>(data.position, one, data.their_pinned, occupied)) {
      continue;
    }
    if (IsCapturable<them>(data.position, target, data.their_pinned,
                           occupied)) {
      continue;
    }

    // 打つことで逃げ道を作ってしまうのは飛か香で角の利きを止めてしまう場合だけ
    if (directions & effect8::DirectionSet::CROSS) {
      const Bitboard occupied2 = occupied | target;
      // 何故かoccupiedを指定できる関数は利きを全列挙している
      // TODO: 特定の方向の角、馬を取り除くだけでいいので差分計算ができる
      const Bitboard sliding_around2 =
          GetSlidingEffects<Us>(data.position, occupied2);
      const Bitboard around_effects2 = non_sliding_around | sliding_around2;

      // TODO: 特定の2マスに利きがあるか、変化したかを検証したいだけ
      //       もう少しいい書き方にしたい

      // 香の場合でも王手しているのは保証されているので、縦の利きだけが重要
      // 飛と区別する必要はない
      if ((ou_movable & ~(around_effects2 |
                          state_action::detail::GetHiNaiveEffect(target)))) {
        // 逃げ道ができてしまった

        // もう1マス離して打つ

        const Square target2 = GetNextSquare(one, target);
        if (target2 == Square::SIZE) {
          continue;
        }
        if (data.position[target2] != Piece::EMPTY) {
          continue;
        }
        if (IsCapturable<them>(data.position, target2, data.their_pinned,
                               occupied)) {
          continue;
        }

        // 今度は詰んでいる
      }
    }

    return Move(piece_type, target);
  }

  return Move::NONE;
}

/**
 * @brief 横から王手して詰むかどうか
 *
 * 持ち駒に歩、香、桂があって、王が1段目で合駒ができない場合
 * また、持ち駒が桂だけ、王が2段目で合駒ができない場合
 * @param position
 * @return
 */
template <Color::Value Us>
Move IsCheckmateFarDropSide(const Position& position) {
  constexpr Color::Value them = ~Us;
  const Square their_ou = position.template ou_square<them>();

  // 相手の持ち駒や王の位置は呼び出し側でチェックしている

  if (!position[Us].Exists(Piece::HI)) {
    // 横から離れて王手したいのに飛がなかった
    return Move::NONE;
  }

  // 王が移動できるマスの候補
  const Bitboard ou_movable =
      ~position.template color<them>() & GetEffect<Piece::OU, them>(their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  const effect8::DirectionSet directions =
      GetCheckLine(escape_candidate, their_ou);

  // 横方向
  constexpr effect8::DirectionSet horizontal =
      effect8::DirectionSet::R | effect8::DirectionSet::L;
  if (!(directions & horizontal)) {
    // 逃げ道を塞げない
    return Move::NONE;
  }

  const Bitboard empty = position.empties();
  Bitboard attack_bb =
      GetEffect<Piece::OU, them>(their_ou) & empty & RANK_BB[their_ou.rank()];
  if (!attack_bb) {
    // 他の駒が邪魔で利きが通らない
    return Move::NONE;
  }

  const Bitboard their_pinned =
      position.blocker(them) & position.template color<them>();
  const Bitboard& occupied = position.types();

  while (attack_bb) {
    // 左右のどちらから打つか両方を試す必要がある
    const Square one = attack_bb.Pop();
    const Square target = GetNextSquare(their_ou, one);
    if (target == Square::SIZE) {
      continue;
    }
    if (position[target] != Piece::EMPTY) {
      // 駒が既にあるので打てない
      continue;
    }

    // 移動合
    // targetより遠くに打ってもoneに移動できるなら必ず利きを遮断できる
    if (IsCapturable<them>(position, one, their_pinned, occupied)) {
      continue;
    }
    if (IsCapturable<them>(position, target, their_pinned, occupied)) {
      continue;
    }

    // 飛を打つことで角の利きを遮って逃げ道が発生している可能性がある
    // NOTE: この判定をどのタイミングで行うか
    //       角の利きを遮るのはレアケースだと思う
    //       移動合ができる方が可能性は高いと思うので、このタイミングで判定する
    //       移動合の判定は絶対に必要なので、上の処理は無駄ではない

    // NOTE: 上の処理でもtargetに駒を打ったことを考慮すべきか
    //       targetに駒を打った影響はoneに移動合ができるかどうかの時に現れるが、
    //       oneに到達できない代わりに必ずtargetには到達できるので、
    //       最終的に違いはない
    const Bitboard occupied2 = occupied | target;
    // 何故かoccupiedを指定できる関数は利きを全列挙している
    // TODO: 特定の方向の角、馬を取り除くだけでいいので差分計算ができる
    const Bitboard sliding_around2 = GetSlidingEffects<Us>(position, occupied2);
    const Bitboard around_effects2 = non_sliding_around | sliding_around2;

    // TODO: 特定の2マスに利きがあるか、変化したかを検証したいだけ
    //       もう少しいい書き方にしたい
    if ((ou_movable &
         ~(around_effects2 | state_action::detail::GetHiNaiveEffect(target)))) {
      // 逃げ道ができてしまった

      // もう1マス離して打つ

      const Square target2 = GetNextSquare(one, target);
      if (target2 == Square::SIZE) {
        continue;
      }
      if (position[target2] != Piece::EMPTY) {
        continue;
      }
      if (IsCapturable<them>(position, target2, their_pinned, occupied)) {
        continue;
      }

      // 今度は詰んでいる
    }
    return Move(Piece::HI, target);
  }

  return Move::NONE;
}

/**
 * @brief 前方から王手して詰むかどうか
 *
 * 持ち駒が桂しかない時に王が3段目で合駒できない場合
 * @param position
 * @return
 */
template <Color::Value Us>
Move IsCheckmateFarDropFront(const Position& position) {
  constexpr Color::Value them = ~Us;
  const Square their_ou = position.template ou_square<them>();

  // 相手の持ち駒や王の位置は呼び出し側でチェックしている

  if (!position[Us].Exists(Piece::KA) && !position[Us].Exists(Piece::HI)) {
    // 離れて王手できる駒がなかった
    return Move::NONE;
  }

  // 王が移動できるマスの候補
  const Bitboard ou_movable =
      ~position.template color<them>() & GetEffect<Piece::OU, them>(their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  const effect8::DirectionSet directions =
      GetCheckLine(escape_candidate, their_ou);

  // なぜかgcc, clangではオペレータが見つからないらしく、エラーになる
  constexpr effect8::DirectionSet vertical =
      effect8::DirectionSet::U | effect8::DirectionSet::D;
  Piece piece_type;
  if (directions & vertical) {
    if (!position[Us].Exists(Piece::HI)) {
      // 後ろに長い利きの駒がない
      return Move::NONE;
    }
    piece_type = Piece::HI;
  } else if (directions & effect8::DirectionSet::DIAGONAL) {
    if (!position[Us].Exists(Piece::KA)) {
      // 斜めの長い利きの駒がない
      return Move::NONE;
    }
    piece_type = Piece::KA;
  } else {
    // 詰まない
    // 前方向からでは逃げ道を塞げない
    return Move::NONE;
  }

  const Bitboard empty = position.empties();
  Bitboard attack_bb;
  if (piece_type == Piece::HI) {
    attack_bb = empty & (their_ou + Forward<them>::value);
  } else {
    if (them == Color::BLACK) {
      if (directions & effect8::DirectionSet::RU) {
        attack_bb = empty & (their_ou + Forward<them>::value + Square::R);
      } else {
        attack_bb = empty & (their_ou + Forward<them>::value + Square::L);
      }
    } else {
      if (directions & effect8::DirectionSet::RU) {
        attack_bb = empty & (their_ou + Forward<them>::value + Square::L);
      } else {
        attack_bb = empty & (their_ou + Forward<them>::value + Square::R);
      }
    }
  }
  if (!attack_bb) {
    // 他の駒が邪魔で利きが通らない
    return Move::NONE;
  }

  const Bitboard their_pinned =
      position.blocker(them) & position.template color<them>();
  const Bitboard& occupied = position.types();

  // 候補は一つだけなのでループは不要
  const Square one = attack_bb.Pop();
  const Square target = GetNextSquare(their_ou, one);
  // 斜め方向だと盤の外側かもしれないので、チェックが必要
  if (target == Square::SIZE) {
    return Move::NONE;
  }
  if (position[target] != Piece::EMPTY) {
    // 駒が既にあるので打てない
    return Move::NONE;
  }

  // 移動合
  // targetより遠くに打ってもoneに移動できるなら必ず利きを遮断できる
  if (IsCapturable<them>(position, one, their_pinned, occupied)) {
    return Move::NONE;
  }
  if (IsCapturable<them>(position, target, their_pinned, occupied)) {
    return Move::NONE;
  }

  // 王が3段目で、前から打つので、利きを遮ることはない

  return Move(piece_type, target);
}

template <Color::Value Us>
Move IsCheckmateFar(const Position& position) {
  constexpr Color::Value them = ~Us;

  const Square their_ou = position.ou_square<them>();
  // 王が移動できるマスの候補
  const Bitboard ou_movable =
      ~position.color<them>() & GetEffect<Piece::OU, them>(their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  const int escape_count = escape_candidate.PopCount();
  if (escape_count >= 4) {
    // TODO: 両王手で最大5か所を防げる
    //       飛・龍を動かして龍で王手する。同時に角・馬で王手する
    //       逃げられるマスを全て潰せるかの判定は組み合わせが多いのでテーブルで処理するのがいい
    return Move::NONE;
  }

  // 退路に利きを発生させる方法を一つずつ探す
  Bitboard attack_bb =
      position.empties() & GetEffect<Piece::OU, them>(their_ou);

  Square source, target;
  if (escape_count == 2) {
    // 2回取り出せる
    Bitboard tmp = escape_candidate;
    source = tmp.Pop();
    target = tmp.Pop();
  }

  while (attack_bb) {
    const Square one = attack_bb.Pop();

    // 1.
    // their_ouのone側のもう一つ先のマスにtheir_ouに利きのある駒を持ってこれる
    // 2. そのマスに利きがない
    //    利きがあると捕獲もしくは移動合ができる
    // 3. oneに歩を打てない
    // 以上の条件を満たすなら詰み

    target = GetNextSquare(their_ou, one);
    if (target == Square::SIZE) {
      // 駒が移動できる場所がない
      continue;
    }

    if (position[one] != Piece::EMPTY && position[one].color() == Us) {
      // 自分の駒があって移動できない
      continue;
    }

    if (IsFuDroppable<them>(position, one) &&
        EndRank<them>::value != one.rank()) {
      // 二歩でなくて、一番奥の段でないなら歩を打てる
      continue;
    }

    if (position[target].type() == Piece::FU && target.file() == one.file() &&
        position[them].Exists(Piece::FU)) {
      // targetのマスにある駒が歩だと捕ってしまうので、改めてoneに打てる
      continue;
    }
  }

  return Move::NONE;
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_FAR_DROP_H_INCLUDED
