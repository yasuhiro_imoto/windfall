#pragma once
#ifndef CHECK_DATA_H_INCLUDED
#define CHECK_DATA_H_INCLUDED

#include <cstdint>

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../position.h"

namespace state_action {
namespace mate {
template <Color::Value Us>
struct CheckData {
  CheckData(const Position& pos)
      : position(pos),
        movable(~pos.color<Us>()),
        our_pinned(pos.blocker(Us) & pos.color<Us>()),
        their_pinned(pos.blocker(~Us) & pos.color<~Us>()),
        our_ou(pos.ou_square<Us>()),
        their_ou(pos.ou_square<~Us>()),
        dc_candidates(pos.blocker(~Us) & pos.color<Us>()),
        mask(movable & GetEffect<Piece::OU, ~Us>(their_ou)),
        pinned{pos.blocker(Color::BLACK) & pos.color<Color::BLACK>(),
               pos.blocker(Color::WHITE) & pos.color<Color::WHITE>()} {}

  const Position& position;
  /**
   * @brief 自分の駒が移動可能な場所の候補
   */
  const Bitboard movable;
  /**
   * @brief 自分のピンされている駒
   */
  const Bitboard our_pinned;
  /**
   * @brief 相手のピンされている駒
   */
  const Bitboard their_pinned;
  /**
   * @brief 自分の王があるマス
   */
  const Square our_ou;
  /**
   * @brief 相手の王があるマス
   */
  const Square their_ou;
  /**
   * @brief 移動することで両王手になる可能性のある駒
   */
  const Bitboard dc_candidates;

  /**
   * @brief 相手の王の周囲で移動可能なマス
   */
  const Bitboard mask;

  template<Color::Value C>
  Square ou_square()const noexcept {
    return position.ou_square<C>();
  }

  /**
   * @brief それぞれの手番側のピンされている駒
  */
  const Bitboard pinned[2];
};

}  // namespace mate
}  // namespace state_action
#endif  // !CHECK_DATA_H_INCLUDED
