#include "stdafx.h"

#include "mate_1ply.h"

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"

namespace state_action {
namespace mate {
namespace detail {
template <Color::Value Us, Piece::Value P>
struct AroundInitializerSquare {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    Bitboard tmp = GetEffect<Piece::OU, ~Us>(square);
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= GetEffect<P, ~Us>(target);
    }
    return bb;
  }
};

template <Color::Value Us>
struct AroundInitializerSquare<Us, Piece::FU> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = GetEffect<Piece::OU, ~Us>(square);
    bb = GetFuEffect(Us, bb);
    // シフトで63bit目が立つとまずいので対応
    bb &= Bitboard::All;
    return bb;
  }
};

template <Color::Value Us>
struct AroundInitializerSquare<Us, Piece::KY> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = state_action::detail::GetKyNaiveEffect(~Us, square);

    if (square.file() != File::_1) {
      const auto s = square + Square::R;
      // 王の斜め後ろに利きを発生させるので王の真横も対象に含まれる
      bb |= state_action::detail::GetKyNaiveEffect(~Us, s) | s;
    }
    if (square.file() != File::_9) {
      const auto s = square + Square::L;
      // 王の斜め後ろに利きを発生させるので王の真横も対象に含まれる
      bb |= state_action::detail::GetKyNaiveEffect(~Us, s) | s;
    }
    return bb;
  }
};

template <Color::Value Us>
struct AroundInitializerSquare<Us, Piece::KA> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    Bitboard tmp = GetEffect<Piece::OU, ~Us>(square);
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= state_action::detail::GetKaNaiveEffect(target);
    }
    return bb;
  }
};

template <Color::Value Us>
struct AroundInitializerSquare<Us, Piece::HI> {
  Bitboard operator()(const Square square) const {
    Bitboard bb = Bitboard::Zero;
    Bitboard tmp = GetEffect<Piece::OU, ~Us>(square);
    while (tmp) {
      const Square target = tmp.Pop();
      bb |= state_action::detail::GetHiNaiveEffect(target);
    }
    return bb;
  }
};

template <Color::Value Us, Piece::Value P>
struct AroundInitializer {
  static void Call() {
    const AroundInitializerSquare<Us, P> initializer;
    for (const Square square : SQUARE) {
      Bitboard tmp = initializer(square);
      // squareは王の位置なので、消しておく
      tmp &= ~Bitboard(square);

      check_around_bb[square][P - 1][Us] = tmp;
    }
  }
};

template <Piece::Value... Ps>
void InitializeAroundTable();

template <Piece::Value Head, Piece::Value... Tails>
void InitializeAroundTableHelper() {
  AroundInitializer<Color::BLACK, Head>::Call();
  AroundInitializer<Color::WHITE, Head>::Call();

  InitializeAroundTable<Tails...>();
}

template <Piece::Value... Ps>
void InitializeAroundTable() {
  InitializeAroundTableHelper<Ps...>();
}
template <>
void InitializeAroundTable<>() {}

void InitializeAround() {
  InitializeAroundTable<Piece::FU, Piece::KY, Piece::KE, Piece::GI, Piece::KA,
                        Piece::HI, Piece::KI, Piece::OU>();
}
}  // namespace detail
}  // namespace mate
}  // namespace state_action
