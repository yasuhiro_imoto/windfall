#pragma once
#ifndef CHECKMATE_REVEALED_H_INCLUDED
#define CHECKMATE_REVEALED_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../const_bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "mate_1ply.h"
#include "mate_helper.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
template <Color::Value Us, Piece::Value P>
struct RevealedCheck;

template <Color::Value Us>
struct RevealedCheck<Us, Piece::FU> {
  static Move call(const CheckData<Us>& data, const Square source,
                   const Bitboard& target_candidate) {
    constexpr Color::Value them = ~Us;

    // ここが盤の外側でないことは歩がsourceに存在できることから保証されている
    const Square target = source + Forward<Us>::value;

    if (!IsMovable<Us>(data, target)) {
      // 自分の駒がある
      return Move::NONE;
    }
    if (data.position[target].type() == Piece::FU &&
        data.position[them].Exists(Piece::FU)) {
      // 歩を捕ったので、相手はsourceに歩を打てる
      return Move::NONE;
    }

    if (!target.promotable(Us)) {
      // 歩のままで詰むのは相当にレアケースと思う
      return Move::NONE;
    }

    if (IsAligned(source, target, data.their_ou)) {
      // 空き王手になっていない
      return Move::NONE;
    }
    if (data.position.revealed(source, target, data.our_ou, data.our_pinned)) {
      return Move::NONE;
    }

    // 成りの場合だけを考える
    Bitboard effected = GetEffect<Piece::KI, Us>(target);
    if (effected & data.their_ou) {
      // 両王手は既に調べた
      return Move::NONE;
    }

    // ピンが解除されるかも
    // むしろ変更にならないパターンの方が多い
    const Bitboard new_pin = data.position.pinned(them, source, target);

    const Bitboard occupied = data.position.types() | target ^ source;

    // 長い利きの発生源を求める
    const Square effect_source =
        revealed::GetEffectSource<Us>(data, source, occupied);

    // targetの位置の駒は捕られるので、守りから除外
    if (IsInterceptable<them>(data, effect_source, target, new_pin, occupied)) {
      // 合駒できる
      return Move::NONE;
    }
    if (IsEscapable<them>(data, source, effected, occupied)) {
      // 逃げ道がある
      return Move::NONE;
    }
    return Move(source, target, true);
  }
};

template <Color::Value Us>
struct RevealedCheck<Us, Piece::KY> {
  static Move call(const CheckData<Us>& data, const Square source,
                   const Bitboard& target_candidate) {
    constexpr Color::Value them = ~Us;

    // 香がsourceにあるので、一つ前のマスは必ず存在する
    // 正面に王がある場合でも今は王手になっていないので、他の駒が間にある
    // すなわちsourceの一つ前は王のマスではない
    const Square forward = source + Forward<Us>::value;

    if (IsAligned(source, forward, data.their_ou)) {
      // 他の候補も空き王手にはならないので、調べる必要はない
      return Move::NONE;
    }
    if (data.position.revealed(source, forward, data.our_ou,
                                data.our_pinned)) {
      // 素抜きをくらう
      return Move::NONE;
    }

    Square effect_source = Square::SIZE;

    Bitboard candidate = target_candidate;
    while (candidate) {
      const Square target = candidate.Pop();

      if (!IsMovable<Us>(data, target)) {
        // 自分の駒がある
        continue;
      }
      if (data.position[target].type() == Piece::FU &&
          data.position[them].Exists(Piece::FU)) {
        // 歩を捕ったので、相手はsourceに歩を打てる
        continue;
      }

      // 空き王手ができる配置で、成らない場合で詰むならば、
      // 成りの両王手でも詰んでいる
      // 両王手は既に調べているはずなので、成りで王手にならない場合のみを調べる

      // 成りの場合だけを考える
      Bitboard effected = GetEffect<Piece::KI, Us>(target);
      if (effected & data.their_ou) {
        // 両王手は既に調べた
        continue;
      }

      // ピンが解除されるかも
      // むしろ変更にならないパターンの方が多い
      const Bitboard new_pin = data.position.pinned(them, source, target);

      const Bitboard occupied = data.position.types() | target ^ source;
      if (effect_source == Square::SIZE) {
        // 長い利きの発生源を求める
        effect_source = revealed::GetEffectSource<Us>(data, source, occupied);
      }

      // targetの位置の駒は捕られるので、守りから除外
      if (IsInterceptable<them>(data, effect_source, target, new_pin,
                                occupied)) {
        // 合駒できる
        continue;
      }
      if (IsEscapable<them>(data, source, effected, occupied)) {
        // 逃げ道がある
        continue;
      }
      return Move(source, target, true);
    }

    return Move::NONE;
  }
};

template <Color::Value Us, Piece::Value Umpromoted, Piece::Value Promoted>
bool IsCheckmate(const CheckData<Us>& data, const Square source,
                 const Square target, Square& effect_source,
                 const Bitboard& ou_around, bool& promotion) {
  constexpr Color::Value them = ~Us;

  const Bitboard& effected_u = GetEffect<Umpromoted, Us>(target);
  if (!(effected_u & data.ou_square<them>()) && (ou_around & effected_u)) {
    // 桂の場合は周囲8マスに利きがあるなら王手でない

    const Bitboard occupied = data.position.types() | target ^ source;
    if (effect_source == Square::SIZE) {
      // 長い利きの発生源を求める
      effect_source = revealed::GetEffectSource<Us>(data, source, occupied);
    }

    // targetの位置の駒は捕られるので、守りから除外
    if (IsInterceptable<them>(data, effect_source, target, data.their_pinned,
                              occupied)) {
      // 合駒できる
      return false;
    }
    if (!IsEscapable<them>(data, source, effected_u, occupied)) {
      // 詰んだ
      promotion = false;
      return true;
    }

    if (!IsPromotable(Us, source, target)) {
      return false;
    }
    const Bitboard& effected_p = GetEffect<Promoted, Us>(target);
    if ((effected_p & data.ou_square<them>()) || !(effected_p & ou_around)) {
      // 調べる条件に該当しない
      return false;
    }

    promotion = true;
    return IsEscapable<them>(data, source, effected_p, occupied);
  } else {
    // 不成は駄目なので、成りの場合を検討する

    if (!IsPromotable(Us, source, target)) {
      return false;
    }
    const Bitboard& effected_p = GetEffect<Promoted, Us>(target);
    if ((effected_p & data.ou_square<them>()) || !(effected_p & ou_around)) {
      // 調べる条件に該当しない
      return false;
    }

    const Bitboard occupied = data.position.types() | target ^ source;
    if (effect_source == Square::SIZE) {
      // 長い利きの発生源を求める
      effect_source = revealed::GetEffectSource<Us>(data, source, occupied);
    }

    // targetの位置の駒は捕られるので、守りから除外
    if (IsInterceptable<them>(data, effect_source, target, data.their_pinned,
                              occupied)) {
      // 合駒できる
      return false;
    }

    promotion = true;
    return IsEscapable<them>(data, source, effected_p, occupied);
  }
}

template <Color::Value Us>
struct RevealedCheck<Us, Piece::KE> {
  static Move call(const CheckData<Us>& data, const Square source,
                   const Bitboard& target_candidate) {
    constexpr Color::Value them = ~Us;

    const Bitboard& ou_around = GetEffect<Piece::OU, them>(data.their_ou);

    Square effect_source = Square::SIZE;

    Bitboard bb = target_candidate;
    // ほとんどの場合で候補は一つだけだが、
    // 王の正面から王の斜め後ろへ移動する場合は候補が二つになる
    while (bb) {
      const Square target = bb.Pop();

      // 空き王手になるのは確実

      if (!IsMovable<Us>(data, target)) {
        // 自分の駒がある
        continue;
      }

      const effect8::DirectionSet direction =
          effect8::GetDirection(data.their_ou, source);
      if (direction & effect8::DirectionSet::DIAGONAL) {
        if (data.position[target].type() == Piece::FU &&
            data.position[them].Exists(Piece::FU)) {
          // 歩を捕ったので、相手はtargetの後ろに歩を打てる
          // 王に近づく方だけが候補なので、sourceの筋よりtargetの筋は王に近い
          // targetの後ろは利きを遮る位置関係
          continue;
        }
      }

      // ピンされているなら絶対に素抜きをくらう
      if (data.position.revealed(source, target, data.our_ou,
                                  data.our_pinned)) {
        // 素抜きをくらう
        continue;
      }

      bool promotion;
      if (IsCheckmate<Us, Piece::KE, Piece::NK>(
              data, source, target, effect_source, ou_around, promotion)) {
        return Move(source, target, promotion);
      }
    }

    return Move::NONE;
  }
};

template <Color::Value Us, Piece::Value Umpromoted, Piece::Value Promoted>
Move IsCheckmate(const CheckData<Us>& data, const Square source,
                 const Square target, const Square effect_source,
                 const Bitboard& ou_around, const Bitboard& target_candidate) {
  constexpr Color::Value them = ~Us;

  bool searched = false;

  const Bitboard occupied = data.position.types() | target ^ source;
  const Bitboard& effected_u = GetEffect<Umpromoted, Us>(target);
  if (!(effected_u & data.ou_square<them>()) && (ou_around & effected_u)) {
    if (revealed::IsInterceptable<them>(data, effect_source, target_candidate,
                                        source, target)) {
      // 移動合ができる
      return Move::NONE;
    }
    searched = true;

    if (!IsEscapable<them>(data, source, effected_u, occupied)) {
      // 詰んだ
      return Move(source, target);
    }
  }

  // 不成は駄目なので、成りの場合を検討する

  if (!IsPromotable(Us, source, target)) {
    return Move::NONE;
  }

  const Bitboard& effected_p = GetEffect<Promoted, Us>(target);
  if (!(effected_p & data.ou_square<them>()) && (effected_p & ou_around)) {
    if (!searched &&
        revealed::IsInterceptable<them>(data, effect_source, target_candidate,
                                        source, target)) {
      // 移動合ができる
      return Move::NONE;
    }

    if (!IsEscapable<them>(data, source, effected_p, occupied)) {
      // 詰んだ
      return Move(source, target, true);
    }
  }

  return Move::NONE;
}

template <Color::Value Us>
struct RevealedCheck<Us, Piece::GI> {
  static Move call(const CheckData<Us>& data, const Square source,
                   const Bitboard& target_candidate) {
    constexpr Color::Value them = ~Us;

    // 合駒の判定はできるだけ後回しにしたい気がするが、
    // 移動先の候補は複数あると思うので、ここで評価しても無駄にはならないと思う
    // 素抜きになるのはかなりのレアケースだと思うので、気にしない

    // 長い利きの発生源を求める
    const Square effect_source =
        revealed::GetEffectSource<Us>(data, source, data.position.types());
    if (revealed::IsInterceptable<them>(data, effect_source,
                                        target_candidate)) {
      // 合駒できるので詰まない
      // target_candidateへの移動の関係でfalse negativeの可能性があるが許容する
      return Move::NONE;
    }

    const Bitboard& ou_around =
        GetEffect<Piece::OU, them>(data.ou_square<them>());

    Bitboard bb = target_candidate;
    while (bb) {
      const Square target = bb.Pop();

      if (IsAligned(source, target, data.ou_square<them>())) {
        // 空き王手になっていない
        continue;
      }

      if (data.position.revealed(source, target, data.ou_square<Us>(),
                                  data.pinned[Us])) {
        // 素抜きをくらう
        continue;
      }

      if (revealed::IsFuDroppable<them>(data, source, target, effect_source)) {
        // 合駒できる
        continue;
      }

      const Move move = IsCheckmate<Us, Piece::GI, Piece::NG>(
          data, source, target, effect_source, ou_around, target_candidate);
      if (move) {
        return move;
      }
    }
    return Move::NONE;
  }
};

template <Color::Value Us, Piece::Value P>
Move IsCheckmate(const CheckData<Us>& data, const Square source,
                 const Square target, const Square effect_source,
                 const Bitboard& ou_around, const Bitboard& target_candidate) {
  constexpr Color::Value them = ~Us;

  const Bitboard occupied = data.position.types() | target ^ source;
  const Bitboard& effected = GetEffect<P, Us>(target);
  if (!(effected & data.ou_square<them>()) && (ou_around & effected)) {
    if (revealed::IsInterceptable<them>(data, effect_source, target_candidate,
                                        source, target)) {
      // 移動合ができる
      return Move::NONE;
    }

    if (!IsEscapable<them>(data, source, effected_u, occupied)) {
      // 詰んだ
      return Move(source, target);
    }
  }

  return Move::NONE;
}

template <Color::Value Us>
struct RevealedCheck<Us, Piece::KI> {
  static Move call(const CheckData<Us>& data, const Square source,
                   const Bitboard& target_candidate) {
    constexpr Color::Value them = ~Us;

    // 長い利きの発生源を求める
    const Square effect_source =
        revealed::GetEffectSource<Us>(data, source, data.position.types());
    if (revealed::IsInterceptable<them>(data, effect_source,
                                        target_candidate)) {
      // 合駒できるので詰まない
      // target_candidateへの移動の関係でfalse negativeの可能性があるが許容する
      return Move::NONE;
    }

    const Bitboard& ou_around =
        GetEffect<Piece::OU, them>(data.ou_square<them>());

    Bitboard bb = target_candidate;
    while (bb) {
      const Square target = bb.Pop();

      if (IsAligned(source, target, data.ou_square<them>())) {
        // 空き王手になっていない
        continue;
      }

      if (data.position.revealed(source, target, data.ou_square<Us>(),
                                  data.pinned[Us])) {
        // 素抜きをくらう
        continue;
      }

      if (revealed::IsFuDroppable<them>(data, source, target, effect_source)) {
        // 合駒できる
        continue;
      }

      const Move move = IsCheckmate<Us, Piece::KI>(
          data, source, target, effect_source, ou_around, target_candidate);
      if (move) {
        return move;
      }
    }
    return Move::NONE;
  }
};

/**
 * @brief 空き王手で詰むかどうかを判定する
 *
 * 両王手は動かした駒で王手するときに一緒に調べているので、対象外
 * @param data
 * @return
 */
template <Color::Value Us>
Move IsCheckmateRevealed(const CheckData<Us>& data) {
  constexpr Color::Value them = ~Us;

  if (!data.dc_candidates) {
    // 空き王手はない
    return Move::NONE;
  }
  if (data.position[them].hand & 0xFFFFFF00) {
    // 歩以外の駒を持っているのでたぶん合駒できる
    return Move::NONE;
  }

  const Bitboard& around24 = GetAround24BB(data.their_ou);

  Bitboard bb = data.dc_candidates;
  while (bb) {
    const Square source = bb.Pop();

    const effect8::DirectionSet direction =
        effect8::GetDirection(source, data.their_ou);
    const Piece source_piece = data.position[source];

    const Bitboard effected =
        GetEffect(source_piece, source, data.position.types());
    Bitboard around_candidate =
        GetCheckAround<Us>(source_piece.type(), data.their_ou);
    if (source_piece.type() < Piece::KI) {
      around_candidate |= GetCheckAround<Us>(
          source_piece.type() + Piece::PROMOTION, data.their_ou);
    }

    // 王の周辺に利きを作られる場所へ移動する
    Bitboard target_candidate = effected & around_candidate & ~data.position.color<Us>();
    if (!target_candidate) {
      return Move::NONE;
    }

    switch (source_piece.type().value) {
      case Piece::FU:

        break;
      default:
        break;
    }
  }
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_REVEALED_H_INCLUDED
