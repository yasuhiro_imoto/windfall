#include "stdafx.h"

#include "mate_1ply.h"

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../../utility/level_assert.h"

namespace state_action {
namespace mate {
namespace detail {
template <typename T>
constexpr int sgn(T val)noexcept {
  return (T(0) < val) - (val < T(0));
}

void InitializeNextSquare() {
  for (const auto s1 : SQUARE) {
    for (const auto s2 : SQUARE) {
      Square next = Square::SIZE;
      if (state_action::detail::GetQueenNaiveEffect(s1) & s2) {
        // s2はs1上下左右斜めのいずれかの方向にある

        const File vf(sgn(s2.file() - s1.file()));
        const Rank vr(sgn(s2.rank() - s1.rank()));
        ASSERT_LV1(enum_cast<>(vf.value) >= -1 && enum_cast<>(vf.value) <= 1);
        ASSERT_LV1(enum_cast<>(vr.value) >= -1 && enum_cast<>(vr.value) <= 1);

        File f(s2.file() + vf);
        Rank r(s2.rank() + vr);
        // 盤の外側でないかチェック
        if (f.ok() && r.ok()) {
          next = Square(f, r);
        }

        next_square[s1][s2] = next;
      }
    }
  }
}
}
}
}