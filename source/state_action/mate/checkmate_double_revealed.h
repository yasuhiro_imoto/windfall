#pragma once
#ifndef CHECKMATE_DOUBLE_REVEALED_H_INCLUDED
#define CHECKMATE_DOUBLE_REVEALED_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "mate_1ply.h"
#include "position_helper.h"
#include "check_data.h"

namespace state_action {
namespace mate {
/**
 * @brief 空き王手をされたときに防げるか
 * @param data 
 * @param source ここから駒が移動する 
 * @param target ここに駒が移動してくる
 * @param occupied 駒の移動を考慮したoccupied bitboard
 * @return 
*/
template <Color::Value Us>
bool IsInterceptable(const CheckData<Us>& data, const Square source,
                     const Square target, const Bitboard& occupied) {
  constexpr Color::Value them = ~Us;

  const auto hand = data.position[them];
  if (hand.Exists(Piece::GI) || hand.Exists(Piece::KA) ||
      hand.Exists(Piece::HI) || hand.Exists(Piece::KI)) {
    // 両王手のみ
    return true;
  } else {
    const effect8::DirectionSet d =
        effect8::GetDirection(data.their_ou, source);
    const Square effect_source =
        GetSourceSquare(d, occupied, source, data.position.color<Us>());
    auto between = GetBetweenBB(data.their_ou, effect_source);

    // sourceからtargetに移動する
    const auto new_pin = data.position.pinned(them, source, target);
    while (between) {
      const Square s = between.Pop();
      if (hand.Exists(Piece::KE)) {
        if (them == Color::BLACK && s.rank() >= Rank::_3 ||
            them == Color::WHITE && s.rank() <= Rank::_7) {
          // 桂を打てる
          return true;
        }
      }
      if (hand.Exists(Piece::KY)) {
        if (them == Color::BLACK && s.rank() >= Rank::_2 ||
            them == Color::WHITE && s.rank() <= Rank::_8) {
          // 香を打てる
          return true;
        }
      }
      if (hand.Exists(Piece::FU) && data.position.IsLegalFuDropEx(s)) {
        // 歩を打てる
        return true;
      }

      if (IsCapturable<them>(data.position, s, Bitboard::Zero, new_pin,
                             occupied)) {
        // 移動できる
        return true;
      }
    }

    if (IsCapturable<them>(data.position, s, Bitboard::Zero, new_pin,
                           occupied)) {
      // 捕れる
      return true;
    }
  }
  return false;
}

/**
 * @brief 両王手と空き王手を一緒に調べる
 *        候補の駒を動かすので、両王手と空き王手のどちらでもない場合もある
 * @param data 
 * @param cache 
 * @return 
*/
template<Color::Value Us>
inline Move IsCheckmateDR(const CheckData<Us>& data, Cache&cache) {
  constexpr Color::Value them = ~Us;

  if (!data.dc_candidates) {
    return Move::NONE;
  }

  // 数は少ないと思うので全列挙する

  Bitboard dc_candidates = data.dc_candidates;
  while (dc_candidates)
  {
    const Square source = dc_candidates.Pop();
    const Piece source_piece = data.position[source];
    const Piece piece_type = source_piece.type();

    const Bitboard& occupied = data.position.types<>();
    // 利きを止めている駒が移動する候補
    Bitboard move_candidate = GetEffect(source_piece, source, occupied) & ~data.position.color<Us>();
    if (!move_candidate) {
      continue;
    }

    const bool is_double_candidate = IsCheckCandidate(source, piece_type, data.their_ou);
    const bool is_around_candidate = IsCheckAround(Us, source, source_piece, data.their_ou);
    bool is_pinned = false;

    if (piece_type != Piece::OU) {
      is_pinned = data.our_pinned & source;
    } 

    while (move_candidate) {
      const Square target = move_candidate.Pop();
      if (piece_type == Piece::OU) {
        if (data.position.IsEffected(them, target)) {
          // 相手の利きがないマスへ移動する必要がある
          continue;
        }
      } else {
        if (is_pinned && !IsAligned(source, target, data.our_ou)) {
          // 素抜きをくらう
          continue;
        }
      }

      const Bitboard occ = (occupied | target) ^ source;

      // 少なくとも空き王手にはなる

      // 空き王手を駒を防げるか
      // 防げるなら両王手しかない
      bool dc_only = IsInterceptable<Us>(data, source, target, occ);
      if (dc_only && !is_double_candidate) {
        continue;
      }

      Bitboard effected = GetEffect(source_piece, source, occ);

    }
  }
}
}
}
#endif // !CHECKMATE_DOUBLE_REVEALED_H_INCLUDED

