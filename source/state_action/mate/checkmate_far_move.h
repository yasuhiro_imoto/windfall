#pragma once
#ifndef CHECKMATE_FAR_MOVE_H_INCLUDED
#define CHECKMATE_FAR_MOVE_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/enum_cast.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../direction.h"
#include "../effect.h"
#include "../move_helper.h"
#include "../position.h"
#include "check_data.h"
#include "mate_1ply.h"
#include "mate_helper.h"
#include "position_helper.h"

namespace state_action {
namespace mate {
namespace detail {
constexpr effect8::DirectionSet GetDiagonalMask(
    const effect8::DirectionSet d) noexcept {
  if (d & (effect8::DirectionSet::RU | effect8::DirectionSet::LU)) {
    return static_cast<effect8::DirectionSet::Value>(d.value << 1) |
           effect8::DirectionSet::U;
  } else {
    return static_cast<effect8::DirectionSet::Value>(d.value >> 1) |
           effect8::DirectionSet::U;
  }
}
}  // namespace detail

/**
 * @brief 利きの長い駒を王から離れた位置に移動させて詰む可能性があるかを調べる
 *        一部のパターンでfalse negativeがある
 *        ビットが立っている方向から王手をかける
 * @param escape_candidate
 * @param their_ou
 * @param only_ry [output] 一間龍の形のみ可能性があるかどうか
 * @return
 */
inline effect8::DirectionSet GetCheckLine(const Bitboard& escape_candidate,
                                          const Square their_ou,
                                          bool& only_ry) {
  only_ry = false;

  const int escape_count = escape_candidate.PopCount();
  if (escape_count >= 5) {
    // 両王手が必要
    return effect8::DirectionSet::ZERO;
  }

  Bitboard tmp =
      escape_candidate & state_action::detail::GetKaNaiveEffect(their_ou);
  const int diagonal_count = tmp.PopCount();
  const int cross_count = escape_count - diagonal_count;
  if (cross_count >= 3 || diagonal_count >= 3) {
    // 両王手が必要
    return effect8::DirectionSet::ZERO;
  }

  effect8::DirectionSet directions;
  if (cross_count > 0 && diagonal_count > 0) {
    // 一間龍なら詰むかも

    // 一間龍以外にも以下のようなパターンがあるが、かなりレアケースと思われる
    // このパターンを検出するのはコストが高いので諦める
    //  * -OU *  *  *  *      * -OU *  *  *  *
    //  *  *  *  * +KA+HI ->  *  *  *  *  * +HI
    //  *  *  *  *  *  *      *  *  * +KA *  *

    tmp = escape_candidate;
    // 龍を移動させる位置の候補
    directions = effect8::DirectionSet::CROSS;
    while (tmp) {
      const Square s = tmp.Pop();
      effect8::DirectionSet d = effect8::GetDirection(their_ou, s);
      if (d & effect8::DirectionSet::DIAGONAL) {
        // 斜めの逃げ道がある側に移動する必要がある
        directions &= detail::GetDiagonalMask(d);
        if (directions == effect8::DirectionSet::ZERO) {
          // 候補がなくなった
          // 一間龍でカバーできない
          return effect8::DirectionSet::ZERO;
        }
      } else {
        if (!((d | d.reverse()) & directions)) {
          // 一間龍でカバーできない
          return effect8::DirectionSet::ZERO;
        }
      }
    }

    only_ry = true;
    return directions;
  }

  if (escape_count == 2) {
    Bitboard tmp = escape_candidate;
    const Square s1 = tmp.Pop();
    const Square s2 = tmp.Pop();

    directions = effect8::GetDirection(s1, their_ou);
    const auto d = effect8::GetDirection(their_ou, s2);
    if (directions != d) {
      // 直線でない
      // この2か所は利きの長い駒で同時には塞げない
      return effect8::DirectionSet::ZERO;
    }
    directions |= effect8::GetDirection(their_ou, s1);
  } else if (escape_count == 1) {
    Bitboard tmp = escape_candidate;
    const Square s1 = tmp.Pop();

    directions = effect8::GetDirection(s1, their_ou) |
                 effect8::GetDirection(their_ou, s1);
  } else {
    // 全方向どこからでもいいので王手すればOK
    directions = effect8::DirectionSet::ALL;
  }
  return directions;
}

/**
 * @brief 一間龍の形で詰むかどうか
 * @param data
 * @param directions
 * @return
 */
template <Color::Value Us>
Move IsCheckmateWithRy(const CheckData<Us>& data,
                       const effect8::DirectionSet directions) {
  constexpr Color::Value them = ~Us;

  Square delta;
  switch (directions.value) {
    case effect8::DirectionSet::U:
      if (data.their_ou.rank() <= Rank::_2) {
        // 一マス離れた場所がない
        return Move::NONE;
      }
      delta = Square::U;
      break;
    case effect8::DirectionSet::D:
      if (data.their_ou.rank() >= Rank::_8) {
        // 一マス離れた場所がない
        return Move::NONE;
      }
      delta = Square::D;
      break;
    case effect8::DirectionSet::R:
      if (data.their_ou.file() <= File::_2) {
        // 一マス離れた場所がない
        return Move::NONE;
      }
      delta = Square::R;
      break;
    case effect8::DirectionSet::L:
      if (data.their_ou.file() >= File::_8) {
        // 一マス離れた場所がない
        return Move::NONE;
      }
      delta = Square::L;
      break;
    default:
      UNREACHABLE;
      break;
  }
  const Square one = data.their_ou + delta;
  if (data.position[one] != Piece::EMPTY) {
    // 駒がある
    return Move::NONE;
  }

  if (IsFuDroppable<them>(data.position, one)) {
    return Move::NONE;
  }

  const Square target = one + delta;
  if (!IsMovable<Us>(data, target)) {
    // 自分の駒がある
    return Move::NONE;
  }

  if (data.position[target].type() == Piece::FU &&
      one.file() == target.file() && data.position[them].Exists(Piece::FU)) {
    // targetの歩を捕ってしまうとまたoneに歩が打てる
    return Move::NONE;
  }

  // targetに移動できる飛、龍を調べる
  // NOTE: 飛の場合は成りを考えないといけないが、候補になること自体が
  //       レアケースと思うので、後から調べる
  const Bitboard& occupied = data.position.types();
  Bitboard candidate =
      (GetEffect<Piece::HI, Us>(target, occupied) &
       data.position.template color<Us, Piece::ROOK_DRAGON>()) |
      (GetEffect<Piece::OU, Us>(target) &
       data.position.template color<Us, Piece::RY>());
  while (candidate) {
    const Square source = candidate.Pop();

    bool promotion = false;
    if (data.position[source].type() == Piece::HI) {
      if (!IsPromotable(Us, source, target)) {
        // 龍で王手する必要がある
        continue;
      }
      promotion = true;
    }

    if (data.position.revealed(source, target, data.our_ou, data.our_pinned)) {
      continue;
    }

    const Bitboard occupied2 = (occupied ^ source) | target;
    if (!(data.dc_candidates & source)) {
      // 両王手でないなら合駒を考える

      const Bitboard new_pin = data.position.pinned(them, source, target);
      if (IsCapturable<them>(data.position, target, new_pin, occupied2)) {
        continue;
      }
      if (IsCapturable<them>(data.position, one, new_pin, occupied2)) {
        continue;
      }
    }

    // 龍の短い利きはnon sliderに含まれているので、全部計算し直す必要がある
    if (IsEscapable<them>(data, source, GetNaiveEffect<Piece::HI>(target),
                          occupied2)) {
      // 詰んでいる
      return Move(source, target, promotion);
    }
  }
  // 一間龍で詰まない
  return Move::NONE;
}

/**
 * @brief 大駒で離れて王手で詰むかどうか
 *        香も一緒に判定する
 * @param data
 * @param direction
 * @param one
 * @param occupied
 * @return
 */
template <Color::Value Us, Piece::Value Umpromoted, Piece::Value Promoted,
          Piece::Value Group>
Move IsCheckmateFarMoveHelper(const CheckData<Us>& data,
                              const effect8::DirectionSet direction,
                              const Square one, const Bitboard& occupied) {
  constexpr Color::Value them = ~Us;

  Bitboard target_candidate;
  if (Group == Piece::BISHOP_HORSE) {
    if (direction == effect8::DirectionSet::SLASH) {
      target_candidate =
          state_action::detail::GetKaDiagonalEffect<0>(data.their_ou, occupied);
    } else

    {
      target_candidate =
          state_action::detail::GetKaDiagonalEffect<1>(data.their_ou, occupied);
    }
  } else {
    if (direction == effect8::DirectionSet::VERTICAL) {
      target_candidate =
          state_action::detail::GetHiFileEffect(data.their_ou, occupied);
    } else {
      target_candidate =
          state_action::detail::GetHiRankEffect(data.their_ou, occupied);
    }
  }

  Bitboard source_candidate;
  if (Group == Piece::ROOK_DRAGON) {
    source_candidate = data.position.template color<Us>() &
                       ((FORWARD_RANK_BB[them][data.their_ou.rank()] &
                         data.position.template types<Piece::KY>()) |
                        data.position.template types<Group>());
  } else {
    source_candidate = data.position.template color<Us, Group>();
  }

  while (source_candidate) {
    const Square source = source_candidate.Pop();

    Square target;
    bool found = false;
    if (data.position[source].type() == Promoted) {
      Bitboard tmp = target_candidate & GetEffect<Piece::OU, Us>(source);

      // 短い利きで王手できるなら長い利きは計算しなくてよい
      //
      // 以下の様なパターンで、短い利きでマスAに移動できる
      // もう一か所王手できるマスBがあるが、
      // 離れて王手する場合を考えているので、候補にならない
      //  *  * -OU *
      //  *  *  *  B
      //  A  *  *  *
      //  * +UM *  *

      // 候補は最大で馬なら2か所、龍なら3か所
      while (tmp) {
        const Square s = tmp.Pop();
        const auto d = effect8::GetDirection(s, data.their_ou);
        if (d != direction) {
          // 王手を考えている方角ではない
          continue;
        }

        if (data.position[s] != Piece::EMPTY &&
            data.position[s].color() == Us) {
          // 自分の駒がある
          continue;
        }

        if (!found) {
          target = s;
          found = true;
        } else {
          // 王に近し方だけを確かめればいい
          // 近い方で合駒か駒を捕れるなら遠い方でも合駒できる
          if (Promoted == Piece::UM) {
            if (abs(s.file() - data.their_ou.file()) <
                abs(target.file() - data.their_ou.file())) {
              // 近い方に変更
              target = s;
            }
          } else {
            if (d & effect8::DirectionSet::VERTICAL) {
              if (abs(s.rank() - data.their_ou.rank()) <
                  abs(target.rank() - data.their_ou.rank())) {
                // 近い方に変更
                target = s;
              }
            } else {
              if (abs(s.file() - data.their_ou.file()) <
                  abs(target.file() - data.their_ou.file())) {
                // 近い方に変更
                target = s;
              }
            }
          }
        }
      }
    }

    if (!found && (Group != Piece::ROOK_DRAGON ||
                   direction != effect8::DirectionSet::VERTICAL ||
                   data.position[source].type() != Piece::KY ||
                   source.file() == data.their_ou.file())) {
      // 短い利きの移動では王手できなかった

      Bitboard tmp =
          target_candidate & (data.position[source].type() != Piece::KY
                                  ? GetEffect<Umpromoted, Us>(source, occupied)
                                  : GetEffect<Piece::KY, Us>(source, occupied));
      // 候補は最大で2か所
      // 考えている方向の王手になるのは一つだけ
      // 他は直交するか反対の方向
      while (tmp) {
        const Square s = tmp.Pop();
        const auto d = effect8::GetDirection(s, data.their_ou);
        if (d != direction) {
          // 王手を考えている方角ではない
          continue;
        }

        target = s;
        found = true;

        break;
      }
    }

    if (!found) {
      continue;
    }

    if (data.position.revealed(source, target, data.our_ou, data.our_pinned)) {
      continue;
    }

    const Bitboard occupied2 = (occupied ^ source) | target;
    if (!(data.dc_candidates & source)) {
      // 両王手でないなら合駒を考える

      if (data.position[target].type() == Piece::FU &&
          (direction & effect8::DirectionSet::VERTICAL) &&
          data.position[them].Exists(Piece::FU)) {
        // 歩を捕ってしまうとまた打てる
        continue;
      }

      const Bitboard new_pin = data.position.pinned(them, source, target);

      Bitboard between = GetBetweenBB(one, target);
      bool interceptable = false;
      while (between) {
        const Square s = between.Pop();

        if (IsFuDroppable<them>(data.position, s)) {
          // 合駒できる
          interceptable = true;
          break;
        }
        if (IsCapturable<them>(data.position, s, new_pin, occupied2)) {
          // 移動合
          interceptable = true;
          break;
        }
      }
      if (interceptable) {
        continue;
      }
      if (IsCapturable<them>(data.position, target, new_pin, occupied2)) {
        // 捕れる
        continue;
      }
    }

    const Bitboard& effected = GetNaiveEffect<Umpromoted>(target);
    if (IsEscapable<them>(data, source, effected, occupied2)) {
      // 詰んでいる
      return Move(source, target);
    }
  }

  return Move::NONE;
}

template <Color::Value Us>
Move IsCheckmateFarMove(const CheckData<Us>& data,
                        const effect8::DirectionSet direction, const Square one,
                        const Bitboard& occupied) {
  constexpr Color::Value them = ~Us;

  // 王手できるマス
  Bitboard target_candidate =
      GetDirectionEffect(direction, occupied, data.their_ou);
  if (direction & effect8::DirectionSet::DIAGONAL) {
    const Move move =
        IsCheckmateFarMoveHelper<Us, Piece::KA, Piece::UM, Piece::BISHOP_HORSE>(
            data, direction, one, occupied);
    if (move != Move::NONE) {
      return move;
    }
  } else {
    const Move move =
        IsCheckmateFarMoveHelper<Us, Piece::HI, Piece::RY, Piece::ROOK_DRAGON>(
            data, direction, one, occupied);
    if (move != Move::NONE) {
      return move;
    }
  }
  return Move::NONE;
}

template <Color::Value Us>
Move IsCheckmateFarMove(const CheckData<Us>& data) {
  constexpr Color::Value them = ~Us;

  const Square their_ou = data.position.template ou_square<them>();
  // 王が移動できるマスの候補
  const Bitboard ou_movable = ~data.position.template color<them>() &
                              GetEffect<Piece::OU, them>(data.their_ou);

  const Bitboard non_sliding_around =
      GetNonSlidingEffectsAround<them>(data.position);
  const Bitboard sliding_around = GetSlidingEffectsAround<them>(data.position);
  const Bitboard around_effects = non_sliding_around | sliding_around;

  // 王が逃げられるマス
  const Bitboard escape_candidate = ou_movable & ~around_effects;
  bool only_ry;
  const effect8::DirectionSet directions =
      GetCheckLine(escape_candidate, data.their_ou, only_ry);

  if (directions == effect8::DirectionSet::ZERO) {
    return Move::NONE;
  }

  if (only_ry) {
    // 一間龍の形なら詰むかもしれない

    // 方向の候補は最大で2個
    auto tmp = directions;
    auto d = tmp.Pop();
    // TODO: 候補が2個あることを見逃していたので引数が良くない
    //       修正する
    const auto d1 = effect8::DirectionSet(d);
    auto move = IsCheckmateWithRy<Us>(data, d1);
    if (move) {
      return move;
    }
    if (tmp == effect8::DirectionSet::ZERO) {
      return state_action::Move::NONE;
    }
    // 残りの方向
    return IsCheckmateWithRy<Us>(data, tmp);
  }

  const Bitboard& occupied = data.position.types();
  // 長い利きで王手できる方向
  // NOTE: directionsでマスクした方が良さそうだが、pop命令を使うので、
  //       コストはそんなに変わらないと思う
  //       テーブルを作っておくのはキャッシュ効率の面で良くなさそう
  Bitboard attack =
      data.position.empties() & GetEffect<Piece::OU, them>(data.their_ou);
  while (attack) {
    const Square one = attack.Pop();
    const effect8::DirectionSet d = effect8::GetDirection(data.their_ou, one);
    if (!(d & directions)) {
      continue;
    }

    const Square next_square = GetNextSquare(data.their_ou, one);
    if (next_square == Square::SIZE) {
      // 1マス離れた場所がない
      continue;
    }
    if (data.position[next_square] != Piece::EMPTY &&
        data.position[next_square].color() == Us) {
      // 自分の駒がある
      continue;
    }

    if (IsFuDroppable<them>(data.position, one)) {
      // 歩で合駒できる
      continue;
    }
    if (data.position[next_square].type() == Piece::FU &&
        one.file() == next_square.file() &&
        data.position[them].Exists(Piece::FU)) {
      // next_squareの歩を捕ってしまうとまたoneに歩が打てる
      continue;
    }

    Bitboard target_candidate =
        GetDirectionEffect(directions, occupied, data.their_ou);
    while (target_candidate) {
      const Square target = target_candidate.Pop();
      const auto tmp = effect8::GetDirection(data.their_ou, target);
      if (tmp != d) {
        // 移動したい側と反対方向
        continue;
      }

      if (data.position[target] != Piece::EMPTY &&
          data.position[target].color() == Us) {
        // 自分の駒がある
        continue;
      }

      Bitboard source_candidate;
      if (d & effect8::DirectionSet::DIAGONAL) {
        source_candidate =
            (GetEffect<Piece::KA, Us>(target, occupied) &
             data.position.template color<Us, Piece::BISHOP_HORSE>()) |
            (GetEffect<Piece::OU, Us>(target) |
             data.position.template color<Us, Piece::UM>());
      } else {
        const auto tmp = GetEffect<Piece::HI, Us>(target, occupied);
        source_candidate =
            (tmp & data.position.template color<Us, Piece::ROOK_DRAGON>()) |
            (GetEffect<Piece::OU, Us>(target) |
             data.position.template color<Us, Piece::RY>());

        if ((Us == Color::BLACK && d == effect8::DirectionSet::D) ||
            (Us == Color::WHITE && d == effect8::DirectionSet::U)) {
          if (data.position[target] != Piece::EMPTY) {
            // 香で王手するということは、今から利きを遮っている相手の駒を捕る
            source_candidate &= tmp & FORWARD_RANK_BB[data.their_ou.rank()] &
                                data.position.template color<Us, Piece::KY>();
          }
        }
      }
      if (!source_candidate) {
        continue;
      }

      while (source_candidate) {
        const Square source = source_candidate.Pop();
        if (data.position.revealed(source, target, data.our_ou,
                                   data.our_pinned)) {
          continue;
        }

        const Bitboard occupied2 = (occupied ^ source) | target;
        if (!(data.dc_candidates & source)) {
          // 両王手でないなら合駒を考える

          const Bitboard new_pin = data.position.pinned(them, source, target);

          Bitboard between = GetBetweenBB(data.their_ou, target);
          bool interceptable = false;
          while (between) {
            const Square s = between.Pop();

            if (data.position[s].type() == Piece::FU &&
                one.file() == s.file() &&
                data.position[them].Exists(Piece::FU)) {
              // sの歩を捕ってしまうとまたoneに歩が打てる
              interceptable = true;
              break;
            }
            if (IsCapturable<them>(data.position, s, new_pin, occupied2)) {
              // 移動合
              interceptable = true;
              break;
            }
          }
          if (interceptable) {
            continue;
          }
          if (IsCapturable<them>(data.position, target, new_pin, occupied2)) {
            // 捕れる
            continue;
          }
        }

        Bitboard effected;
        if (d & effect8::DirectionSet::DIAGONAL) {
          effected = state_action::detail::GetKaNaiveEffect(target);
        } else {
          effected = state_action::detail::GetHiNaiveEffect(target);
        }
        // 龍でないと詰まないパターンは上で分岐してここでは扱わない

        if (!(GetEffect<Piece::OU, them>(data.their_ou) &
              ~(data.position.template color<them>() |
                GetEffectsAround<them>(data.position, source, occupied2) |
                effected))) {
          // 詰んでいる
          return Move(source, target);
        }
      }
    }
  }
  return Move::NONE;
}
}  // namespace mate
}  // namespace state_action
#endif  // !CHECKMATE_FAR_MOVE_H_INCLUDED
