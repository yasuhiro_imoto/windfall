#pragma once
#ifndef MOVE_GENERATOR_HELPER_H_INCLUDED
#define MOVE_GENERATOR_HELPER_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../move.h"
#include "move_piece_value.h"

namespace state_action {
namespace move_generator {
/**
 * @brief sourceにある駒を移動させる指し手を生成
 *
 * @param target_bb sourceにある駒の移動先のBitboard
 * @param source 移動元
 * @param move_list
 */
template <Piece::Value P, Color::Value Us, bool Promotion>
void MakeMove(const Bitboard& target_bb, const Square source,
              ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(source, target, Promotion),
                        MoveUpper(Us, P, Promotion));
  }
}

/**
 * @brief sourceにある駒を移動させる指し手で成る場合と成らない場合の両方を生成
 *
 * 成れるかどうかの判定は行わない
 * target_bbのマス全てへ移動する指し手を生成する
 * @param target_bb sourceにある駒の移動先のBitboard
 * @param source 移動元
 * @param move_list
 */
template <Piece::Value P, Color::Value Us>
void MakeMove(const Bitboard& target_bb, const Square source,
              ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(source, target, true), MoveUpper(Us, P, true));
    move_list.push_back(Move(source, target, false), MoveUpper(Us, P, false));
  }
}

/**
 * @brief All==trueで不成の指し手も生成する
 *
 * 角と飛専用
 * 成れることは確定している
 * @param target_bb
 * @param source
 * @param move_list
 */
template <Piece::Value P, Color::Value Us, bool All>
void MakeMoveBR(const Bitboard& target_bb, const Square source,
                ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(source, target, true), MoveUpper(Us, P, true));
    if (All) {
      move_list.push_back(Move(source, target, false), MoveUpper(Us, P, false));
    }
  }
}

/**
 * @brief 成れない駒について複数種類を同時に扱う場合
 *
 * @param target_bb
 * @param source
 * @param src_piece
 * @param move_list
 */
template <Color::Value Us>
void MakeMove(const Bitboard& target_bb, const Square source,
              const Piece src_piece, ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(source, target, false),
                        MoveUpper(Us, src_piece, false));
  }
}

/**
 * @brief 角と飛を同時に扱う場合
 *
 * 成れることは確定している
 * @param target_bb
 * @param source
 * @param src_piece
 * @param move_list
 */
template <Color::Value Us, bool All>
void MakeMoveBR(const Bitboard& target_bb, const Square source,
                const Piece src_piece, ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(source, target, true),
                        MoveUpper(Us, src_piece, true));
    if (All) {
      move_list.push_back(Move(source, target, false),
                          MoveUpper(Us, src_piece, false));
    }
  }
}

/**
 * @brief 駒を打つ指し手を生成
 * @param target_bb
 * @param move_list
 */
template <Piece::Value P, Color::Value Us>
void MakeDrop(const Bitboard& target_bb, ExtendedMoveArray& move_list) {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(P, target), DropUpper(Us, P));
  }
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_GENERATOR_HELPER_H_INCLUDED
