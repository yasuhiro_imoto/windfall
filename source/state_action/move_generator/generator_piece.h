#pragma once
#ifndef MOVE_GENERATOR_PIECE_H_INCLUDED
#define MOVE_GENERATOR_PIECE_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "../position.h"
#include "generator_helper.h"
#include "generator_type.h"
#include "move_target.h"
#include "promotion_proxy.h"

namespace state_action {
namespace move_generator {
template <MoveGeneratorType T, Piece::Value P, Color::Value Us, bool All>
struct GeneratePieceMoves {
  // 桂と銀のみで、 それ以外は特殊化する
  // 特殊化にも該当しないものは呼び出さない
  static_assert(P == Piece::KE || P == Piece::GI, "");

  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb = position.color<Us, P>();

    const auto generator =
        MakeMoveTarget<P, Us, PromotionProxy<P, All>::value>();
    while (source_bb) {
      const Square source = source_bb.Pop();
      Bitboard bb = GetEffect<P, Us>(source) & target_bb;

      generator(position, source, bb, move_list);
    }
  }
};

template <MoveGeneratorType T, Color::Value Us, bool All>
struct GeneratePieceMoves<T, Piece::KY, Us, All> {
  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb = position.color<Us, Piece::KY>();
    const Bitboard& occupied = position.types();

    const auto generator = MakeMoveTarget<Piece::KY, Us, All>();
    while (source_bb) {
      const Square source = source_bb.Pop();
      Bitboard bb = GetEffect<Piece::KY, Us>(source, occupied) & target_bb;

      generator(position, source, bb, move_list);
    }
  }
};

template <MoveGeneratorType T, Color::Value Us, bool All>
struct GeneratePieceMoves<T, Piece::FU, Us, All> {
  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb = position.color<Us, Piece::FU>();
    // sourceから移動した後にtargetと一致するかを調べる代わりに
    // targetから手前に戻ったときにsourceと一致するかを調べる
    // 1段目と9段目の間で回り込みが発生する可能性があるが、
    // 回り込んだ先に歩は存在しないので問題ない
    Bitboard bb = GetFuEffect(~Us, target_bb) & source_bb;

    const auto generator = MakeMoveTarget<Piece::FU, Us, All>();
    while (bb) {
      const Square source = bb.Pop();

      // bbは利用しないのでBitboard型なら何でもいい
      generator(position, source, bb, move_list);
    }
  }
};

template <MoveGeneratorType T, Color::Value Us, bool All>
struct GeneratePieceMoves<T, Piece::GPM_BR, Us, All> {
  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb = position.color<Us, Piece::KA, Piece::HI>();
    const Bitboard& occupied = position.types();

    const auto generator = MakeMoveTarget<Piece::GPM_BR, Us, All>();
    while (source_bb) {
      const Square source = source_bb.Pop();
      Bitboard bb = GetEffect(position[source], source, occupied) & target_bb;

      generator(position, source, bb, move_list);
    }
  }
};

template <MoveGeneratorType T, Color::Value Us, bool All>
struct GeneratePieceMoves<T, Piece::GPM_GHDK, Us, All> {
  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb = position.color<Us, Piece::GOLDS_HDK>();
    const Bitboard& occupied = position.types();

    const auto generator = MakeMoveTarget<Piece::GPM_GHDK, Us, false>();
    while (source_bb) {
      const Square source = source_bb.Pop();
      Bitboard bb = GetEffect(position[source], source, occupied) & target_bb;

      generator(position, source, bb, move_list);
    }
  }
};

/**
 * @brief 王を除いた成れない駒
 */
template <MoveGeneratorType T, Color::Value Us, bool All>
struct GeneratePieceMoves<T, Piece::GPM_GHD, Us, All> {
  FORCE_INLINE void operator()(const Position& position,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) {
    Bitboard source_bb =
        position.color<Us, Piece::GOLDS, Piece::UM, Piece::RY>();
    const Bitboard& occupied = position.types();

    // 王はsourceとして渡さないので、GHDKで問題ない
    const auto generator = MakeMoveTarget<Piece::GPM_GHDK, Us, false>();
    while (source_bb) {
      const Square source = source_bb.Pop();
      Bitboard bb = GetEffect(position[source], source, occupied) & target_bb;

      generator(position, source, bb, move_list);
    }
  }
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_GENERATOR_PIECE_H_INCLUDED
