#pragma once
#ifndef DROP_CHECK_H_INCLUDED
#define DROP_CHECK_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../../utility/level_assert.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "move_piece_value.h"
#include "move_target.h"
#include "move_target_promotion.h"

namespace state_action {
namespace move_generator {
template <Piece::Value P, Color::Value Us>
struct DropCheckGenerator {
  void operator()(const Position&, const Bitboard& target_bb,
                  ExtendedMoveArray& move_list) {
    Bitboard bb = target_bb;
    while (bb) {
      const Square target = bb.Pop();
      move_list.push_back(Move(P, target), DropUpper(Us, P));
    }
  }
};

template <Color::Value Us>
struct DropCheckGenerator<Piece::FU, Us> {
  void operator()(const Position& position, const Bitboard& target_bb,
                  ExtendedMoveArray& move_list) {
    Bitboard bb = target_bb;
    if (bb) {
      const Square target = bb.Pop();

      if (position.IsLegalFuDropEx(target)) {
        move_list.push_back(Move(Piece::FU, target), DropUpper(Us, Piece::FU));
      }
    }
  }
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !DROP_CHECK_H_INCLUDED
