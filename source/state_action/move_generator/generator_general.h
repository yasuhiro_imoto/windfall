#pragma once
#ifndef GENERATOR_GENERAL
#define GENERATOR_GENERAL

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "../position.h"
#include "generator_drop.h"
#include "generator_helper.h"
#include "generator_piece.h"
#include "generator_type.h"
#include "move_target.h"
#include "piece_iterator.h"

namespace state_action {
namespace move_generator {
/**
 * @brief 駒のない升へ移動する
 */
struct EmptyTarget {
  FORCE_INLINE static Bitboard Generate(const Position& position,
                                        const Square) {
    return position.empties();
  }
};
/**
 * @brief 相手の駒を捕る
 */
template <Color::Value Us>
struct CaptureTarget {
  FORCE_INLINE static Bitboard Generate(const Position& position,
                                        const Square) {
    return position.color<~Us>();
  }
};
/**
 * @brief 歩以外の駒の移動先と同じ
 */
struct SameTarget {
  FORCE_INLINE static Bitboard GenerateFu(const Bitboard& target,
                                          const Position&) {
    return target;
  }
};

template <MoveGeneratorType Type, Color::Value Us>
struct TargetType;

template <Color::Value Us>
struct TargetType<MoveGeneratorType::NON_CAPTURES, Us> : public EmptyTarget,
                                                         public SameTarget {};
template <Color::Value Us>
struct TargetType<MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS, Us>
    : public EmptyTarget {
  /**
   * @brief 駒を捕らない、かつ、成らない
   * @param target
   * @param
   * @return
   */
  FORCE_INLINE static Bitboard GenerateFu(const Bitboard& target,
                                          const Position&) {
    return target & ~GetEnemyField(Us);
  }
};
template <Color::Value Us>
struct TargetType<MoveGeneratorType::CAPTURES, Us> : public CaptureTarget<Us>,
                                                     public SameTarget {};
template <Color::Value Us>
struct TargetType<MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS, Us>
    : public CaptureTarget<Us> {
  /**
   * @brief 駒を捕る、かつ、成る
   * @param target
   * @param position
   * @return
   */
  FORCE_INLINE static Bitboard GenerateFu(const Bitboard& target,
                                          const Position& position) {
    return target | (~position.color<Us>() & GetEnemyField(Us));
  }
};
template <Color::Value Us>
struct TargetType<MoveGeneratorType::NON_EVASIONS, Us> : public SameTarget {
  /**
   * @brief 自分の駒以外の場所全て
   * @param position
   * @param
   * @return
   */
  FORCE_INLINE static Bitboard Generate(const Position& position,
                                        const Square) {
    return ~position.color<Us>();
  }
};
template <Color::Value Us>
struct TargetType<MoveGeneratorType::RECAPTURES, Us> : public SameTarget {
  FORCE_INLINE static Bitboard Generate(const Position&,
                                        const Square recapture_square) {
    return Bitboard(recapture_square);
  }
};

/**
 * @brief 指し手生成器本体
 *
 * GenerateMovesの方から内部的に呼び出される
 * @param position
 * @param move_list
 * @param recapture_square
 * generate_general
 */
template <MoveGeneratorType Type, Color::Value Us, bool All>
void GenerateGeneral(const Position& position, ExtendedMoveArray& move_list,
                     const Square recapture_square = Square::SIZE) {
  static_assert(Type != MoveGeneratorType::EVASIONS_ALL &&
                    Type != MoveGeneratorType::NON_EVASIONS_ALL &&
                    Type != MoveGeneratorType::RECAPTURES_ALL,
                "*_ALL is not allowed");

  const Bitboard target =
      TargetType<Type, Us>::Generate(position, recapture_square);
  const Bitboard target_fu = TargetType<Type, Us>::GenerateFu(target, position);

  GeneratePieceMoves<Type, Piece::FU, Us, All>()(position, target_fu,
                                                 move_list);
  Apply<Type, Us, All, Piece::KY, Piece::KE, Piece::GI, Piece::GPM_BR,
        Piece::GPM_GHDK>(position, target, move_list);

  if (Type == MoveGeneratorType::NON_CAPTURES ||
      Type == MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS ||
      Type == MoveGeneratorType::NON_EVASIONS) {
    GenerateDropMoves<Us>()(position, position.empties(), move_list);
  }
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !GENERATOR_GENERAL
