#pragma once
#ifndef GENERATOR_CHECK_H_INCLUDED
#define GENERATOR_CHECK_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../../utility/level_assert.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "drop_check.h"
#include "generator_type.h"
#include "move_check.h"
#include "move_piece_value.h"
#include "move_target.h"
#include "move_target_general.h"
#include "move_target_promotion.h"

namespace state_action {
namespace move_generator {
template <Color::Value Us, Piece::Value P, Piece::Value Q = P>
Bitboard GetCheckCandidate(const Position& position, const Square ou_square) {
  return position.types<P>() & GetCheckCandidateBB(Us, Q, ou_square);
}

enum class CheckCategory { CHECKS, QUIET_CHECKS, NONE };
constexpr CheckCategory GetCheckCategory(const MoveGeneratorType type) {
  switch (type) {
    case MoveGeneratorType::CHECKS:
    case MoveGeneratorType::CHECKS_ALL:
      return CheckCategory::CHECKS;
    case MoveGeneratorType::QUIET_CHECKS:
    case MoveGeneratorType::QUIET_CHECKS_ALL:
      return CheckCategory::QUIET_CHECKS;
    default:
      break;
  }
  return CheckCategory::NONE;
}

template <CheckCategory Category, Color::Value Us>
struct CheckTargetGenerator;

template <Color::Value Us>
struct CheckTargetGenerator<CheckCategory::CHECKS, Us> {
  FORCE_INLINE static Bitboard Generate(const Position& position) {
    // 自分の駒以外の場所全て
    return ~position.color<Us>();
  }
};
template <Color::Value Us>
struct CheckTargetGenerator<CheckCategory::QUIET_CHECKS, Us> {
  FORCE_INLINE static Bitboard Generate(const Position& position) {
    // 空いているマス全て
    return position.empties();
  }
};

/**
 * @brief 王手をかける指し手を生成
 * @param position
 * @param move_list
 */
template <MoveGeneratorType Type, Color::Value Us, bool All>
void GenerateChecks(const Position& position, ExtendedMoveArray& move_list) {
  // x = 移動した駒で王手
  // y = 空き王手

  const Square ou_square = position.ou_square<~Us>();

  // 移動して王手できる駒の候補
  const Bitboard x =
      (GetCheckCandidate<Us, Piece::FU>(position, ou_square) |
       GetCheckCandidate<Us, Piece::KY>(position, ou_square) |
       GetCheckCandidate<Us, Piece::KE>(position, ou_square) |
       GetCheckCandidate<Us, Piece::GI>(position, ou_square) |
       GetCheckCandidate<Us, Piece::KA>(position, ou_square) |
       position.types<Piece::ROOK_DRAGON>() |  // 無条件に候補
       GetCheckCandidate<Us, Piece::GOLDS, Piece::KI>(position, ou_square) |
       // HI, RYの部分をUMに割り当てた
       GetCheckCandidate<Us, Piece::UM, Piece::HI>(position, ou_square)) &
      position.color<Us>();
  // 利きを遮っている駒
  const Bitboard y = position.blocker(~Us) & position.color<Us>();

  using CheckTarget = CheckTargetGenerator<GetCheckCategory(Type), Us>;
  const Bitboard target = CheckTarget::Generate(position);

  Bitboard source_bb = y;
  const auto target_generator = MakeMoveTargetGeneral<Us, All>();
  while (source_bb) {
    const Square source = source_bb.Pop();
    // 両王手の可能性がある
    // 利きのラインから外れれば空き王手になる
    const Bitboard pin_line = GetLineBB(ou_square, source);
    target_generator(position, position[source], source, target & ~pin_line,
                     move_list);
    if (x & source) {
      // 直接王手をかける
      // 上と重複しないように空き王手でない場所のみを生成
      MakeMoveCheck<Us, All>(position, position[source], source, ou_square,
                             target & pin_line, move_list);
    }
  }

  // 空き王手関係で生成した分を取り除く
  source_bb = (x | y) ^ y;
  while (source_bb) {
    const Square source = source_bb.Pop();
    MakeMoveCheck<Us, All>(position, position[source], source, ou_square,
                           target, move_list);
  }

  const Bitboard empties = position.empties();
  const Hand hand = position[Us];
  if (hand.Exists(Piece::FU)) {
    DropCheckGenerator<Piece::FU, Us>()(
        position, position.check_square(Piece::FU) & empties, move_list);
  }
  if (hand.Exists(Piece::KY)) {
    DropCheckGenerator<Piece::KY, Us>()(
        position, position.check_square(Piece::KY) & empties, move_list);
  }
  if (hand.Exists(Piece::KE)) {
    DropCheckGenerator<Piece::KE, Us>()(
        position, position.check_square(Piece::KE) & empties, move_list);
  }
  if (hand.Exists(Piece::GI)) {
    DropCheckGenerator<Piece::GI, Us>()(
        position, position.check_square(Piece::GI) & empties, move_list);
  }
  if (hand.Exists(Piece::KA)) {
    DropCheckGenerator<Piece::KA, Us>()(
        position, position.check_square(Piece::KA) & empties, move_list);
  }
  if (hand.Exists(Piece::HI)) {
    DropCheckGenerator<Piece::HI, Us>()(
        position, position.check_square(Piece::HI) & empties, move_list);
  }
  if (hand.Exists(Piece::KI)) {
    DropCheckGenerator<Piece::KI, Us>()(
        position, position.check_square(Piece::KI) & empties, move_list);
  }
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !GENERATOR_CHECK_H_INCLUDED
