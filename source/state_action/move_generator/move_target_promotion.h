#pragma once
#ifndef MOVE_TARGET_PROMOTION_H_INCLUDED
#define MOVE_TARGET_PROMOTION_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../move_helper.h"
#include "move_piece_value.h"

namespace state_action {
namespace move_generator {
template <Piece::Value P, Color::Value Us, bool All>
struct UmpromotedMoveCondition {
  static_assert(P == Piece::KA || P == Piece::HI, "");
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return All || !(target.promotable(Us) || source.promotable(Us));
  }
};

template <Color::Value Us>
struct UmpromotedMoveCondition<Piece::FU, Us, false> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return !target.promotable(Us);
  }
};
template <Color::Value Us>
struct UmpromotedMoveCondition<Piece::FU, Us, true> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return target.rank() != EndRank<Us>::value;
  }
};

template <Color::Value Us>
struct UmpromotedMoveCondition<Piece::KY, Us, false> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return Us == Color::BLACK && target.rank() >= Rank::_3 ||
           Us == Color::WHITE && target.rank() <= Rank::_7;
  }
};
template <Color::Value Us>
struct UmpromotedMoveCondition<Piece::KY, Us, true> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return target.rank() != EndRank<Us>::value;
  }
};

template <Color::Value Us, bool All>
struct UmpromotedMoveCondition<Piece::KE, Us, All> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return Us == Color::BLACK && target.rank() >= Rank::_3 ||
           Us == Color::WHITE && target.rank() <= Rank::_7;
  }
};

template <Color::Value Us, bool All>
struct UmpromotedMoveCondition<Piece::GI, Us, All> {
  FORCE_INLINE static constexpr bool Satisfy(const Square source,
                                             const Square target) noexcept {
    return true;
  }
};

template <Piece::Value P, Color::Value Us, bool All, bool PROMOTION>
void MakeMoveTargetPromotion(const Square source, const Bitboard& target_bb,
                             ExtendedMoveArray& move_list) noexcept {
  Bitboard bb = target_bb;
  while (bb) {
    const Square target = bb.Pop();
    if (PROMOTION) {
      move_list.push_back(Move(source, target, true), MoveUpper(Us, P, true));
    } else {
      if (UmpromotedMoveCondition<P, Us, All>::Satisfy(source, target)) {
        move_list.push_back(Move(source, target, false),
                            MoveUpper(Us, P, false));
      }
    }
  }
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_TARGET_PROMOTION_H_INCLUDED
