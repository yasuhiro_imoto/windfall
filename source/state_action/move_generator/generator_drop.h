#pragma once
#ifndef GENERATOR_DROP_H_INCLUDED
#define GENERATOR_DROP_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../hand.h"
#include "../move.h"
#include "../move_helper.h"
#include "move_piece_value.h"

namespace state_action {
namespace move_generator {
template <int N>
struct Unroll {
  template <typename F>
  static void call(const Square square, const F& f, const int offset) {
    Unroll<N - 1>::call(square, f, offset);
    f(N - 1 + offset, square);
  }
};
template <>
struct Unroll<1> {
  template <typename F>
  static void call(const Square square, const F& f, const int offset) {
    f(0 + offset, square);
  }
};

template <Color::Value Us>
struct GenerateDropMoves {
  void operator()(const Position& position, const Bitboard& target_bb,
                  ExtendedMoveArray& move_list) {
    const Hand hand = position[Us];
    if (hand == 0) {
      return;
    }

    // 扱いやすいように変換
    const HandKind hk(hand);

    if (hk.Exists(Piece::FU)) {
      // 歩を打つ場合は
      // 1. 最奥の段でない
      // 2. 二歩でない
      // 3. 打ち歩詰めでない
      // 必要がある

      // 二歩の判定にソフトウェア飽和加算に似たテクニックを用いる
      // cf.
      // http://yaneuraou.yaneu.com/2015/10/15/%E7%B8%A6%E5%9E%8Bbitboard%E3%81%AE%E5%94%AF%E4%B8%80%E3%81%AE%E5%BC%B1%E7%82%B9%E3%82%92%E5%85%8B%E6%9C%8D%E3%81%99%E3%82%8B/
      // 歩と1段目から8段目までのマスクの和
      // 歩があれば9段目に繰り上がりが発生する
      Bitboard a = position.color<Us, Piece::FU>() +
                   GetForwardRankBB(Color::BLACK, Rank::_8);

      const uint32_t index1 =
          static_cast<uint32_t>(PEXT64(a.Extract<0>(), RANK9_BB.Extract<0>()));
      const uint32_t index2 = static_cast<uint32_t>(
          PEXT32(static_cast<uint32_t>(a.Extract<1>()), RANK9_BB.Extract<1>()));
      // 歩を打てる場所
      Bitboard bb = Bitboard(fu_drop_mask_bb_table[index1].p[0],
                             fu_drop_mask_bb_table[index2].p[1]) &
                    GetForwardRankBB(~Us, Rank::_8) & target_bb;

      // 打ち歩詰めを確認
      // 王が最奥まで進んでいる可能性があるので、王の正面がない場合もある
      const Bitboard pe = GetEffect<Piece::FU, ~Us>(position.ou_square<~Us>());
      if (pe & bb) {
        // 王の正面に歩を打っている
        const Square target = pe.PopC();
        if (!position.IsLegalFuDrop(target)) {
          // 打ち歩詰めだった
          bb ^= pe;
        }
      }

      MakeDrop<Piece::FU, Us>(bb, move_list);
    }

    if (!hk.ExistsExceptFu()) {
      return;
    }

    std::array<Move, 6> drop_base;
    int count = 0;
    const auto f = [&](const Piece piece) {
      if (hk.Exists(piece)) {
        drop_base[count++] =
            Move(Move(piece, Square::ZERO), DropUpper(Us, piece));
      }
    };

    f(Piece::KE);
    const int next_to_ke_index = count;
    f(Piece::KY);
    const int next_to_ky_index = count;
    f(Piece::GI);
    f(Piece::KA);
    f(Piece::HI);
    f(Piece::KI);

    const auto g = [&](const int i, const Square target) {
      move_list.push_back(drop_base[i], target);
    };

    if (next_to_ky_index == 0) {
      // 香と桂はない
      switch (count) {
        case 1:
          target_bb.loop(
              [&](const Square target) { Unroll<1>::call(target, g, 0); });
          break;
        case 2:
          target_bb.loop(
              [&](const Square target) { Unroll<2>::call(target, g, 0); });
          break;
        case 3:
          target_bb.loop(
              [&](const Square target) { Unroll<3>::call(target, g, 0); });
          break;
        case 4:
          target_bb.loop(
              [&](const Square target) { Unroll<4>::call(target, g, 0); });
          break;
        default:
          UNREACHABLE;
          break;
      }
    } else {
      // 先手の場合で1段目
      const Bitboard bb1 = target_bb & GetForwardRankBB(Us, Rank::_1);
      // 先手の場合で2段目
      const Bitboard bb2 = target_bb & RANK_BB[EndRank<Us>::value2];
      // 先手の場合で3段目～9段目
      const Bitboard bb3 = target_bb & GetForwardRankBB(~Us, Rank::_7);

      // 先手の場合で1段目に対する香と桂以外の駒打ち
      switch (count - next_to_ky_index) {
        case 0:  // 持ち駒は香と桂のみ
          break;
        case 1:
          bb1.loop([&](const Square target) {
            Unroll<1>::call(target, g, next_to_ky_index);
          });
          break;
        case 2:
          bb1.loop([&](const Square target) {
            Unroll<2>::call(target, g, next_to_ky_index);
          });
          break;
        case 3:
          bb1.loop([&](const Square target) {
            Unroll<3>::call(target, g, next_to_ky_index);
          });
          break;
        case 4:
          bb1.loop([&](const Square target) {
            Unroll<4>::call(target, g, next_to_ky_index);
          });
          break;
        default:
          UNREACHABLE;
          break;
      }

      // 先手の場合で2段目に対する桂以外の駒打ち
      switch (count - next_to_ke_index) {
        case 0:  // 持ち駒は桂のみ
          break;
        case 1:
          bb2.loop([&](const Square target) {
            Unroll<1>::call(target, g, next_to_ke_index);
          });
          break;
        case 2:
          bb2.loop([&](const Square target) {
            Unroll<2>::call(target, g, next_to_ke_index);
          });
          break;
        case 3:
          bb2.loop([&](const Square target) {
            Unroll<3>::call(target, g, next_to_ke_index);
          });
          break;
        case 4:
          bb2.loop([&](const Square target) {
            Unroll<4>::call(target, g, next_to_ke_index);
          });
          break;
        case 5:
          bb2.loop([&](const Square target) {
            Unroll<5>::call(target, g, next_to_ke_index);
          });
          break;
        default:
          UNREACHABLE;
          break;
      }

      // 先手の場合で3段目～9段目に対する全種類の駒打ち
      switch (count) {
        case 1:
          bb3.loop([&](const Square target) { Unroll<1>::call(target, g, 0); });
          break;
        case 2:
          bb3.loop([&](const Square target) { Unroll<2>::call(target, g, 0); });
          break;
        case 3:
          bb3.loop([&](const Square target) { Unroll<3>::call(target, g, 0); });
          break;
        case 4:
          bb3.loop([&](const Square target) { Unroll<4>::call(target, g, 0); });
          break;
        case 5:
          bb3.loop([&](const Square target) { Unroll<5>::call(target, g, 0); });
          break;
        case 6:
          bb3.loop([&](const Square target) { Unroll<6>::call(target, g, 0); });
          break;
        default:
          UNREACHABLE;
          break;
      }
    }
  }
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !GENERATOR_DROP_H_INCLUDED
