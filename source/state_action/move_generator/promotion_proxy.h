#pragma once
#ifndef PROMOTION_PROXY_H_INCLUDED
#define PROMOTION_PROXY_H_INCLUDED

#include "../../rule/piece.h"

namespace state_action {
namespace move_generator {
/**
 * @brief 不成の生成フラグをまとめる
 *        不成の生成フラグに関係ない駒の場合は常にfalse
 */
template <Piece::Value P, bool All>
struct PromotionProxy {
  static constexpr bool value =
      (P == Piece::FU || P == Piece::KY || P == Piece::KA || P == Piece::HI) &&
      All;
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !PROMOTION_PROXY_H_INCLUDED
