#pragma once
#ifndef MOVE_PIECE_VALUE
#define MOVE_PIECE_VALUE

#include <cstdint>

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../utility/configuration.h"
#include "../move.h"

namespace state_action {
namespace move_generator {
#ifdef KEEP_PIECE_IN_GENERATED_MOVE
constexpr uint16_t MoveUpper(const Color us, const Piece piece,
                             const bool promotion) {
  // pieceは既に手番の情報を含んでいるかもしれない
  return (us == Color::WHITE ? Piece::WHITE : Piece::BLACK) |
         (promotion ? Piece::PROMOTION : 0) | piece;
}
constexpr uint16_t DropUpper(const Color us, const Piece piece) {
  return (us == Color::WHITE ? Piece::WHITE : Piece::BLACK) + Piece::DROP +
         piece;
}
#else
constexpr uint16_t MoveUpper(const Color us, const Piece piece,
                             const bool promotion) {
  return 0;
}
constexpr uint16_t DropUpper(const Color us, const Piece piece) { return 0; }
#endif  // KEEP_PIECE_IN_GENERATED_MOVE
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_PIECE_VALUE
