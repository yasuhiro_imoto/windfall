#pragma once
#ifndef MOVE_TARGET_GENERAL_H_INCLUDED
#define MOVE_TARGET_GENERAL_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../position.h"
#include "generator_helper.h"
#include "move_piece_value.h"
#include "move_target.h"
#include "promotion_proxy.h"

namespace state_action {
namespace move_generator {
template <Color::Value Us, bool All>
struct MakeMoveTargetGeneral {
  // これらをすべて同じ名前にできなくはないが、特殊化が現状では面倒なので
  // 頑張らない

  template <Piece::Value P>
  FORCE_INLINE static void GenerateMove(const Position& position,
                                        const Square source,
                                        const Bitboard& target_bb,
                                        ExtendedMoveArray& move_list) noexcept {
    const Bitboard effect = GetEffect<P, Us>(source);
    MakeMoveTarget<P, Us, PromotionProxy<P, All>::value>()(
        position, source, effect & target_bb, move_list);
  }
  template <Piece::Value P>
  FORCE_INLINE static void GenerateLongMove(
      const Position& position, const Square source, const Bitboard& target_bb,
      ExtendedMoveArray& move_list) noexcept {
    const Bitboard& occupied = position.types();
    const Bitboard effect = GetEffect<P, Us>(source, occupied);
    MakeMoveTarget<P, Us, PromotionProxy<P, All>::value>()(
        position, source, effect & target_bb, move_list);
  }
  FORCE_INLINE static void GenerateKyMove(
      const Position& position, const Square source, const Bitboard& target_bb,
      ExtendedMoveArray& move_list) noexcept {
    const Bitboard& occupied = position.types();
    const Bitboard effect = GetEffect<Piece::KY, Us>(source, occupied);
    MakeMoveTarget<Piece::KY, Us, All>()(position, source, effect & target_bb,
                                         move_list);
  }
  FORCE_INLINE static void GenerateOuMove(
      const Position& position, const Square source, const Bitboard& target_bb,
      ExtendedMoveArray& move_list) noexcept {
    const Bitboard effect = GetOuEffect(source);
    MakeMoveTarget<Piece::OU, Us, false>()(position, source, effect & target_bb,
                                           move_list);
  }

  void operator()(const Position& position, const Piece piece,
                  const Square source, const Bitboard& target_bb,
                  ExtendedMoveArray& move_list) const noexcept {
    ASSERT_LV2(piece != Piece::EMPTY);
    switch (piece.type().value) {
      case Piece::FU:
        GenerateMove<Piece::FU>(position, source, target_bb, move_list);
        break;
      case Piece::KY:
        GenerateKyMove(position, source, target_bb, move_list);
        break;
      case Piece::KE:
        GenerateMove<Piece::KE>(position, source, target_bb, move_list);
        break;
      case Piece::GI:
        GenerateMove<Piece::GI>(position, source, target_bb, move_list);
        break;
      case Piece::KA:
        GenerateLongMove<Piece::KA>(position, source, target_bb, move_list);
        break;
      case Piece::HI:
        GenerateLongMove<Piece::HI>(position, source, target_bb, move_list);
        break;
      case Piece::KI:
        GenerateMove<Piece::KI>(position, source, target_bb, move_list);
        break;
      case Piece::OU:
        GenerateOuMove(position, source, target_bb, move_list);
        break;
      case Piece::TO:
        GenerateMove<Piece::TO>(position, source, target_bb, move_list);
        break;
      case Piece::NY:
        GenerateMove<Piece::NY>(position, source, target_bb, move_list);
        break;
      case Piece::NK:
        GenerateMove<Piece::NK>(position, source, target_bb, move_list);
        break;
      case Piece::NG:
        GenerateMove<Piece::NG>(position, source, target_bb, move_list);
        break;
      case Piece::UM:
        GenerateLongMove<Piece::UM>(position, source, target_bb, move_list);
        break;
      case Piece::RY:
        GenerateLongMove<Piece::RY>(position, source, target_bb, move_list);
        break;
      default:
        UNREACHABLE;
        break;
    }
  }
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_TARGET_GENERAL_H_INCLUDED
