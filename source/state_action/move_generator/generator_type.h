#pragma once
#ifndef MOVE_GENERATOR_TYPE_H_INCLUDED
#define MOVE_GENERATOR_TYPE_H_INCLUDED

namespace state_action {
namespace move_generator {
/**
 * @brief 生成する指し手の種類
 *
 * LEGAL/LEGAL_ALL以外は自殺手が含まれることがあるので
 * 事前に確認が必要
 * MOVE_GEN_TYPE
 */
enum class MoveGeneratorType {
  //! 駒を取らない指し手
  NON_CAPTURES,
  //! 駒を取る指し手
  CAPTURES,

  // BonanzaではCAPTURESに銀以外の成りも含まれる
  // Aperyでは歩の成り以外を含まない
  // Aperyの方を採用する

  /**
   * @brief CAPTURES + 歩の成り
   *
   * CAPTURES_PRO_PLUS
   */
  CAPTURES_PLUS_PROMOTIONS,
  /**
   * @brief  NON_CAPTURES - 歩の成り
   *
   * NON_CAPTURES_PRO_MINUS
   */
  NON_CAPTURES_MINUS_PROMOTIONS,

  /**
   * @brief CAPTURES_PLUS_PROMOTIONS + 歩の不成など
   *
   * CAPTURES_PRO_PLUS_ALL
   */
  CAPTURES_PLUS_PROMOTIONS_ALL,
  /**
   * @brief NON_CAPTURES_MINUS_PROMOTIONS + 歩の不成など
   *
   * NON_CAPTURES_PRO_MINUS_ALL
   */
  NON_CAPTURES_MINUS_PROMOTIONS_ALL,

  /**
   * @brief 王手の回避
   *
   * 指し手生成元で王手されている局面であることがわかっているときはこちらを呼び出す
   * EVASIONS
   */
  EVASIONS,
  /**
   * @brief EVASIONS + 歩の不成など
   *
   * EVASIONS_ALL
   */
  EVASIONS_ALL,

  /**
   * @brief 王手の回避ではない手
   *
   * 指し手生成元で王手されていない局面であることがわかっているときのすべての指し手
   * NON_EVASIONS
   */
  NON_EVASIONS,
  /**
   * @brief NON_EVASIONS + 歩の不成など
   *
   * NON_EVASIONS_ALL
   */
  NON_EVASIONS_ALL,

  // 以下の2つは、pos.legalを内部的に呼び出すので生成するのに時間が少しかかる。
  // 棋譜の読み込み時などにしか使わない。

  /**
   * @brief 合法手すべて
   *
   * ただし、2段目の歩・香の不成や角・飛の不成は生成しない
   * LEGAL
   */
  LEGAL,
  /**
   * @brief 合法手すべて
   *
   * LEGAL_ALL
   */
  LEGAL_ALL,

  /**
   * @brief 王手となる指し手
   *
   * 歩の不成などは含まない
   * CHECKS
   */
  CHECKS,
  /**
   * @brief 王手となる指し手
   *
   * 歩の不成などは含む
   * CHECKS
   */
  CHECKS_ALL,

  /**
   * @brief 王手となる指し手
   *
   * 歩の不成などは含まない
   * 駒を捕る手は含まない
   * QUIET_CHECKS
   */
  QUIET_CHECKS,
  /**
   * @brief 王手となる指し手
   *
   * 歩の不成などは含む
   * 駒を捕る手は含まない
   * QUIET_CHECKS_ALL
   */
  QUIET_CHECKS_ALL,

  // 王手となる指し手(歩の不成などは含まない)で、CAPTURES_PRO_PLUSの指し手は含まない指し手
  // QUIET_CHECKS_PRO_MINUS,
  // 王手となる指し手(歩の不成なども含む)で、CAPTURES_PRO_PLUSの指し手は含まない指し手
  // QUIET_CHECKS_PRO_MINUS_ALL,
  // →　これらは実装が難しいので、QUIET_CHECKSで生成してから、歩の成る指し手を除外したほうが良いと思う。

  /**
   * @brief 指定升への移動の指し手のみ
   *
   * 歩の不成などは含まない
   * RECAPTURES
   */
  RECAPTURES,
  /**
   * @brief 指定升への移動の指し手のみ
   *
   * 歩の不成なども含む
   * RECAPTURES_ALL
   */
  RECAPTURES_ALL,

  QUIETS = NON_CAPTURES,  //!< Stockfishとの互換性向上ためのalias
};

template <MoveGeneratorType T>
struct MoveGeneratorAll;

template <>
struct MoveGeneratorAll<MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS> {
  static constexpr MoveGeneratorType value =
      MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS_ALL;
};
template <>
struct MoveGeneratorAll<MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS> {
  static constexpr MoveGeneratorType value =
      MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS_ALL;
};
template <>
struct MoveGeneratorAll<MoveGeneratorType::EVASIONS> {
  static constexpr MoveGeneratorType value = MoveGeneratorType::EVASIONS_ALL;
};
template <>
struct MoveGeneratorAll<MoveGeneratorType::QUIET_CHECKS> {
  static constexpr MoveGeneratorType value =
      MoveGeneratorType::QUIET_CHECKS_ALL;
};

constexpr bool IsAll(MoveGeneratorType type) {
  switch (type) {
    case MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS_ALL:
    case MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS_ALL:
    case MoveGeneratorType::EVASIONS_ALL:
    case MoveGeneratorType::NON_EVASIONS_ALL:
    case MoveGeneratorType::LEGAL_ALL:
    case MoveGeneratorType::CHECKS_ALL:
    case MoveGeneratorType::QUIET_CHECKS_ALL:
    case MoveGeneratorType::RECAPTURES_ALL:
      return true;
    default:
      break;
  }
  return false;
}

constexpr MoveGeneratorType RemoveAll(MoveGeneratorType type) {
  switch (type) {
    case MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS_ALL:
      return MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS;
    case MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS_ALL:
      return MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS;
    case MoveGeneratorType::EVASIONS_ALL:
      return MoveGeneratorType::EVASIONS;
    case MoveGeneratorType::NON_EVASIONS_ALL:
      return MoveGeneratorType::NON_EVASIONS;
    case MoveGeneratorType::LEGAL_ALL:
      return MoveGeneratorType::LEGAL;
    case MoveGeneratorType::CHECKS_ALL:
      return MoveGeneratorType::CHECKS;
    case MoveGeneratorType::QUIET_CHECKS_ALL:
      return MoveGeneratorType::QUIET_CHECKS;
    case MoveGeneratorType::RECAPTURES_ALL:
      return MoveGeneratorType::RECAPTURES;
    default:
      break;
  }
  return type;
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_GENERATOR_TYPE_H_INCLUDED
