#pragma once
#ifndef GENERATOR_EVASION_H_INCLUDED
#define GENERATOR_EVASION_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "../position.h"
#include "generator_drop.h"
#include "generator_helper.h"
#include "generator_type.h"
#include "move_target.h"
#include "piece_iterator.h"

namespace state_action {
namespace move_generator {
template <Color::Value Us, bool All>
void GenerateEvasions(const Position& position, ExtendedMoveArray& move_list) {
  // この関数は王手されている時用
  ASSERT_LV2(position.checked());

  // 王手している駒
  Bitboard checkers = position.checker();
  // 王手している駒の枚数
  int checkers_count = 0;

  // 王が移動した後の利きを考えたいのでその前処理
  const Square ou_square = position.ou_square<Us>();
  const Bitboard occupied = position.types() ^ Bitboard(ou_square);

  // 王手している駒の升
  Square checker_square;
  // 王手している駒の利き
  Bitboard slider_attacks = Bitboard::Zero;
  do {
    ++checkers_count;

    checker_square = checkers.Pop();
    ASSERT_LV3(position[checker_square].color() == ~Us);
    // 王手している駒の利きを記録
    slider_attacks |=
        GetEffect(position[checker_square], checker_square, occupied);
  } while (checkers);

  // 最低限の王の移動先の条件
  // 1. 自分の駒がない
  // 2. 王手をかけている駒の利きがない
  // 条件としては不十分だが、残りは合法手の判定に任せる
  Bitboard bb =
      GetOuEffect(ou_square) & ~(position.color<Us>() | slider_attacks);
  while (bb) {
    const Square target = bb.Pop();
    move_list.push_back(Move(ou_square, target),
                        MoveUpper(Us, Piece::OU, false));
  }

  if (checkers_count > 1) {
    // 両王手なので、王が動く以外の手はない
    return;
  }

  // 合駒
  const Bitboard target_bb1 = GetBetweenBB(checker_square, ou_square);
  // 王手している駒を捕る + 移動合
  const Bitboard target_bb2 = target_bb1 | checker_square;

  Apply<MoveGeneratorType::EVASIONS, Us, All, Piece::FU, Piece::KY, Piece::KE,
        Piece::GI, Piece::GPM_BR, Piece::GPM_GHD>(position, target_bb2,
                                                  move_list);
  GenerateDropMoves<Us>()(position, target_bb1, move_list);
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !GENERATOR_EVASION_H_INCLUDED
