#pragma once
#ifndef MOVE_GENERATOR_TARGET_H_INCLUDED
#define MOVE_GENERATOR_TARGET_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../move_helper.h"
#include "../position.h"
#include "generator_helper.h"
#include "move_piece_value.h"

namespace state_action {
namespace move_generator {
enum class PieceCategory {
  INDIVIDUAL,  //!< 個別に関数を書く駒
  GHDK,        //!< 成れない駒
  BR           //!< 角か飛
};

constexpr bool IsGHDK(const Piece::Value v) noexcept {
  return v >= Piece::KI && v <= Piece::RY || v == Piece::GPM_GHDK;
}
constexpr bool IsBR(const Piece::Value v) noexcept {
  return v == Piece::KA || v == Piece::HI || v == Piece::GPM_BR;
}

constexpr PieceCategory GetCategory(const Piece::Value v) noexcept {
  return IsGHDK(v) ? PieceCategory::GHDK
                   : IsBR(v) ? PieceCategory::BR : PieceCategory::INDIVIDUAL;
}

/**
 * @brief sourceの位置にある駒を移動先を指定して動かす指し手を生成
 *
 * make_move_target
 */
template <Piece::Value P, Color::Value Us, bool All,
          PieceCategory Category = GetCategory(P)>
struct MakeMoveTarget {
  /**
   * @brief
   * @param position
   * @param source
   * @param target_bb sourceに応じた移動先が適切に設定されている必要がある
   * @param move_list
   * @return
   */
  // void operator()(const Position& position, const Square source,
  //                 const Bitboard& target_bb,
  //                 ExtendedMoveArray& move_list) const noexcept;
};

template <Color::Value Us, bool All>
struct MakeMoveTarget<Piece::FU, Us, All, PieceCategory::INDIVIDUAL> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    // if (!target_bb) {
    //  return;
    //}

    const Square target = source + Forward<Us>::value;
    if (target.promotable(Us)) {
      move_list.push_back(Move(source, target, true),
                          MoveUpper(Us, Piece::FU, true));
      if (All && target.rank() != EndRank<Us>::value) {
        move_list.push_back(Move(source, target),
                            MoveUpper(Us, Piece::FU, false));
      }
    } else {
      move_list.push_back(Move(source, target),
                          MoveUpper(Us, Piece::FU, false));
    }
  }
};

template <Color::Value Us, bool All>
struct MakeMoveTarget<Piece::KY, Us, All, PieceCategory::INDIVIDUAL> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    // 成り
    Bitboard target_enemy_field = target_bb & GetEnemyField(Us);
    MakeMove<Piece::KY, Us, true>(target_enemy_field, source, move_list);

    // 不成
    // Allによる不成の対象となるのは2段目だけで3段目は常に生成する
    constexpr Rank rank = All ? EndRank<Us>::value : EndRank<Us>::value2;
    Bitboard bb = target_bb & FORWARD_RANK_BB[~Us][rank];
    MakeMove<Piece::KY, Us, false>(bb, source, move_list);
  }
};

template <Color::Value Us>
struct MakeMoveTarget<Piece::KE, Us, false, PieceCategory::INDIVIDUAL> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    Bitboard bb = target_bb;
    while (bb) {
      const Square target = bb.Pop();
      if (target.promotable(Us)) {
        move_list.push_back(Move(source, target, true),
                            MoveUpper(Us, Piece::KE, true));
      }

      if ((Us == Color::BLACK && target.rank() >= Rank::_3) ||
          (Us == Color::WHITE && target.rank() <= Rank::_7)) {
        move_list.push_back(Move(source, target),
                            MoveUpper(Us, Piece::KE, false));
      }
    }
  }
};

template <Color::Value Us>
struct MakeMoveTarget<Piece::GI, Us, false, PieceCategory::INDIVIDUAL> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    if (GetEnemyField(Us) & source) {
      MakeMove<Piece::GI, Us>(target_bb, source, move_list);
    } else {
      Bitboard enemy_field_bb = target_bb & GetEnemyField(Us);
      MakeMove<Piece::GI, Us>(enemy_field_bb, source, move_list);

      Bitboard bb = target_bb & ~GetEnemyField(Us);
      MakeMove<Piece::GI, Us, false>(bb, source, move_list);
    }
  }
};

/**
 * @brief 成れない駒
 *        GHDK
 */
template <Piece::Value P, Color::Value Us>
struct MakeMoveTarget<P, Us, false, PieceCategory::GHDK> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    Bitboard bb = target_bb;
    MakeMove<P, Us, false>(bb, source, move_list);
  }
};

/**
 * @brief 角か飛
 *        BR
 */
template <Piece::Value P, Color::Value Us, bool All>
struct MakeMoveTarget<P, Us, All, PieceCategory::BR> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    if (source.promotable(Us)) {
      Bitboard bb = target_bb;
      MakeMoveBR<P, Us, All>(bb, source, move_list);
    } else {
      Bitboard enemy_field_bb = target_bb & GetEnemyField(Us);
      MakeMoveBR<P, Us, All>(enemy_field_bb, source, move_list);

      Bitboard bb = target_bb & ~GetEnemyField(Us);
      MakeMove<P, Us, false>(bb, source, move_list);
    }
  }
};

template <Color::Value Us>
struct MakeMoveTarget<Piece::GPM_GHDK, Us, false, PieceCategory::GHDK> {
  void operator()(const Position& position, const Square source,
                  const Bitboard& target_bb,
                  ExtendedMoveArray& move_list) const noexcept {
    Bitboard bb = target_bb;
    MakeMove<Us>(bb, source, position[source], move_list);
  }
};

template <Color::Value Us, bool All>
struct MakeMoveTarget<Piece::GPM_BR, Us, All, PieceCategory::BR> {
  FORCE_INLINE void operator()(const Position& position, const Square source,
                               const Bitboard& target_bb,
                               ExtendedMoveArray& move_list) const noexcept {
    if (source.promotable(Us)) {
      Bitboard bb = target_bb;
      MakeMoveBR<Us, All>(bb, source, position[source], move_list);
    } else {
      Bitboard enemy_field_bb = target_bb & GetEnemyField(Us);
      MakeMoveBR<Us, All>(enemy_field_bb, source, position[source], move_list);

      Bitboard bb = target_bb & ~GetEnemyField(Us);
      MakeMove<Us>(bb, source, position[source], move_list);
    }
  }
};
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_GENERATOR_TARGET_H_INCLUDED
