#pragma once
#ifndef MOVE_CHECK_H_INCLUDED
#define MOVE_CHECK_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../../utility/force_inline.h"
#include "../../utility/level_assert.h"
#include "../bitboard.h"
#include "../effect.h"
#include "../extended_move.h"
#include "move_piece_value.h"
#include "move_target.h"
#include "move_target_promotion.h"

namespace state_action {
namespace move_generator {
enum class MoveCheckCategory { UMPROMOTED, KY, BR, PROMOTED, HD };

template <Piece::Value P, Color::Value Us, bool All, MoveCheckCategory Category>
struct MoveCheckGenerator;

template <Piece::Value P, Color::Value Us, bool All>
struct MoveCheckGenerator<P, Us, All, MoveCheckCategory::UMPROMOTED> {
  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) {
    constexpr Piece::Value Q = static_cast<Piece::Value>(P + Piece::PROMOTION);

    const Bitboard tmp = GetEffect<P, Us>(source) & target_bb;
    if (!tmp) {
      return;
    }

    Bitboard bb = tmp & GetEffect<Q, ~Us>(ou_square);
    if (!(GetEnemyField(Us) & source)) {
      bb &= GetEnemyField(Us);
    }
    MakeMoveTargetPromotion<P, Us, All, true>(source, bb, move_list);

    bb = tmp & GetEffect<P, ~Us>(ou_square);
    MakeMoveTargetPromotion<P, Us, All, false>(source, bb, move_list);
  }
};

template <Color::Value Us, bool All>
struct MoveCheckGenerator<Piece::KY, Us, All, MoveCheckCategory::KY> {
  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) {
    if (abs(source.file() - ou_square.file()) > 1) {
      // 筋が完全に違うので、成っても王手はない
      return;
    }

    const Bitboard& occupied = position.types();
    if (source.file() == ou_square.file() &&
        IsMultiple(GetBetweenBB(source, ou_square) & occupied)) {
      // 間に駒が2枚以上ある
      return;
    }

    const Bitboard tmp = GetEffect<Piece::KY, Us>(source, occupied) & target_bb;
    if (!tmp) {
      return;
    }

    Bitboard bb = tmp & GetEffect<Piece::NY, ~Us>(ou_square);
    if (!(GetEnemyField(Us) & source)) {
      bb &= GetEnemyField(Us);
    }
    MakeMoveTargetPromotion<Piece::KY, Us, All, true>(source, bb, move_list);

    if (source.file() != ou_square.file()) {
      // 筋が違う
      // TODO: 指し手の生成順序を入れ替えて良いか確認
      return;
    }
    bb = tmp & GetEffect<Piece::KY, ~Us>(ou_square, occupied);
    MakeMoveTargetPromotion<Piece::KY, Us, All, false>(source, bb, move_list);
  }
};

template <Piece::Value P, Color::Value Us, bool All>
struct MoveCheckGenerator<P, Us, All, MoveCheckCategory::BR> {
  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) {
    constexpr Piece::Value Q = static_cast<Piece::Value>(P + Piece::PROMOTION);

    const Bitboard& occupied = position.types();
    const Bitboard tmp = GetEffect<P, Us>(source, occupied) & target_bb;
    if (!tmp) {
      return;
    }

    Bitboard bb = tmp & GetEffect<Q, ~Us>(ou_square, occupied);
    if (!(GetEnemyField(Us) & source)) {
      bb &= GetEnemyField(Us);
    }
    MakeMoveTargetPromotion<P, Us, All, true>(source, bb, move_list);

    bb = tmp & GetEffect<P, ~Us>(ou_square, occupied);
    MakeMoveTargetPromotion<P, Us, All, false>(source, bb, move_list);
  }
};

template <Piece::Value P, Color::Value Us, bool All>
struct MoveCheckGenerator<P, Us, All, MoveCheckCategory::PROMOTED> {
  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) {
    const Bitboard tmp = GetEffect<P, Us>(source) & target_bb;
    if (!tmp) {
      return;
    }

    Bitboard bb = tmp & GetEffect<P, ~Us>(ou_square);
    MakeMoveTarget<P, Us, false>()(position, source, bb, move_list);
  }
};

template <Piece::Value P, Color::Value Us, bool All>
struct MoveCheckGenerator<P, Us, All, MoveCheckCategory::HD> {
  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) {
    const Bitboard& occupied = position.types();
    const Bitboard tmp = GetEffect<P, Us>(source, occupied) & target_bb;
    if (!tmp) {
      return;
    }

    Bitboard bb = tmp & GetEffect<P, Us>(ou_square, occupied);
    MakeMoveTarget<P, Us, false>()(position, source, bb, move_list);
  }
};

template <Piece::Value P, Color::Value Us, bool All>
struct MoveCheckProxy {
  static constexpr MoveCheckCategory GetCategory() noexcept {
    if (P >= Piece::UM) {
      return MoveCheckCategory::HD;
    } else if (P >= Piece::KI) {
      return MoveCheckCategory::PROMOTED;
    } else if (P >= Piece::KA) {
      return MoveCheckCategory::BR;
    } else if (P == Piece::KY) {
      return MoveCheckCategory::KY;
    } else {
      return MoveCheckCategory::UMPROMOTED;
    }
  }

  FORCE_INLINE static void Generate(const Position& position,
                                    const Square source, const Square ou_square,
                                    const Bitboard& target_bb,
                                    ExtendedMoveArray& move_list) noexcept {
    MoveCheckGenerator<P, Us, All, GetCategory()>::Generate(
        position, source, ou_square, target_bb, move_list);
  }
};

template <Color::Value Us, bool All>
void MakeMoveCheck(const Position& position, const Piece piece,
                   const Square source, const Square ou_square,
                   const Bitboard& target_bb,
                   ExtendedMoveArray& move_list) noexcept {
  switch (piece.type().value) {
    case Piece::FU:
      MoveCheckProxy<Piece::FU, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::KY:
      MoveCheckProxy<Piece::KY, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::KE:
      MoveCheckProxy<Piece::KE, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::GI:
      MoveCheckProxy<Piece::GI, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::KA:
      MoveCheckProxy<Piece::KA, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::HI:
      MoveCheckProxy<Piece::HI, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::KI:
      MoveCheckProxy<Piece::KI, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
      //
    case Piece::TO:
      MoveCheckProxy<Piece::TO, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::NY:
      MoveCheckProxy<Piece::NY, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::NK:
      MoveCheckProxy<Piece::NK, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::NG:
      MoveCheckProxy<Piece::NG, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::UM:
      MoveCheckProxy<Piece::UM, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    case Piece::RY:
      MoveCheckProxy<Piece::RY, Us, All>::Generate(position, source, ou_square,
                                                   target_bb, move_list);
      break;
    default:
      UNREACHABLE;
      break;
  }
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !MOVE_CHECK_H_INCLUDED
