#pragma once
#ifndef PIECE_ITERATOR_H_INCLUDED
#define PIECE_ITERATOR_H_INCLUDED

#include "../../rule/color.h"
#include "../../rule/piece.h"
#include "../../rule/square.h"
#include "../bitboard.h"
#include "../extended_move.h"
#include "../position.h"
#include "generator_piece.h"
#include "generator_type.h"
#include "move_piece_value.h"

namespace state_action {
namespace move_generator {
template <MoveGeneratorType Type, Color::Value Us, bool All>
void Apply(const Position& position, const Bitboard& target_bb,
           ExtendedMoveArray& move_list) {}

template <MoveGeneratorType Type, Color::Value Us, bool All, Piece::Value Head,
          Piece::Value... Tails>
void Apply(const Position& position, const Bitboard& target_bb,
           ExtendedMoveArray& move_list) {
  GeneratePieceMoves<Type, Head, Us, All>()(position, target_bb, move_list);
  Apply<Type, Us, All, Tails...>(position, target_bb, move_list);
}
}  // namespace move_generator
}  // namespace state_action
#endif  // !PIECE_ITERATOR_H_INCLUDED
