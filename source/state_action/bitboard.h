#pragma once
#ifndef BITBOARD_H_INCLUDED
#define BITBOARD_H_INCLUDED

#include <emmintrin.h>

#include <array>
#include <boost/operators.hpp>
#include <cstdint>
#include <iosfwd>

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/bit_operator.h"
#include "../utility/level_assert.h"
#include "direction.h"

namespace state_action {
// ---------------------------------------------------------------------------
// Bitboard
// ---------------------------------------------------------------------------

//! Bitboard関連のテーブル初期化
void InitializeBitboards();
namespace detail {
//! 角の利きのテーブルを初期化
void InitializeKaEffectTable();
//! 飛車の利きのテーブルを初期化
void InitializeHiEffectTable();

//! 王の利きテーブルを初期化
/*! 角と飛車の利きのテーブルを利用している */
void InitializeOuEffectTable();

//! 他の駒を考慮しない香車の利きテーブルを初期化
/*! 飛車の縦の利きテーブルを利用 */
void InitializeKyNaiveEffectTable();

//! 近接の利きの駒のテーブルを初期化
void InitializeShortEffectTable();

//! 二歩用のテーブルを初期化
void InitializeFuDropMask();

void InitializeBetweenBB();
void InitializeLineBB();

//! 王手となる候補の駒のテーブル初期化
/*! 王手の指し手生成に必要 */
void InitializeCheckCandidateTable();
}  // namespace detail

// Bitboardクラスは、コンストラクタでの初期化が保証できないので
// (オーバーヘッドがあるのでやりたくないので)
// GCC 7.1.0以降で警告が出るのを回避できない。
// ゆえに、このクラスではこの警告を抑制する。
#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif

struct alignas(16) Bitboard : boost::equality_comparable<Bitboard> {
  //! 初期化しない
#pragma warning(push)
#pragma warning(disable : 26495)
  Bitboard() {}
#pragma warning(pop)
  Bitboard(const uint64_t p0, const uint64_t p1) : m(_mm_set_epi64x(p1, p0)) {}
  Bitboard(const Bitboard& bb) { _mm_store_si128(&m, bb.m); }
  //! sqの升が1のBitboardとして初期化
  explicit Bitboard(const Square s);

  Bitboard(const __m128i& v) { _mm_store_si128(&m, v); }
  Bitboard(__m128i&& v) { _mm_store_si128(&m, v); }

  Bitboard& operator=(const Bitboard& rhs) {
    _mm_store_si128(&m, rhs.m);
    return *this;
  }

  /*! set */
  void Set(const uint64_t p0, const uint64_t p1) { m = _mm_set_epi64x(p1, p0); }

  operator bool() const { return !_mm_testz_si128(m, m); }

  //! bit test命令
  /*! if (lhs & rhs)とか(lhs & sq) と書くべきところを
      if (lhs.test(rhs)) とか(lhs.test(ssq))
      書くことでSSE命令を用いて高速化する。
      test */
  bool Test(const Bitboard& rhs) const { return !_mm_testz_si128(m, rhs.m); }
  bool Test(const Square s) const;

  //! p[n]を取り出す
  /*! extract64 */
  template <int N>
  uint64_t Extract() const {
    static_assert(N == 0 || N == 1, "");
    return static_cast<uint64_t>(_mm_extract_epi64(m, N));
  }
  //! p[n]に値を設定
  /*! insert64 */
  template <int N>
  Bitboard& Insert(const uint64_t u) {
    static_assert(N == 0 || N == 1, "");
    m = _mm_insert_epi64(m, u, N);

    return *this;
  }

  //! p[0]とp[1]をbitwise or
  /*! toUに相当
      merge */
  uint64_t Merge() const { return Extract<0>() | Extract<1>(); }
  //! p[0]とp[1]とで bitwise and したときに被覆しているbitがあるか
  /*! merge()したあとにpext()を使うときなどに被覆していないことを
      前提とする場合にそのassertを書くときに使う
      cross_over */
  bool CrossOver() const { return Extract<0>() & Extract<1>(); }

  //! 指定した升(Square)がBitboardのどちらのuint64_t変数の要素に属するか
  /*! part */
  static constexpr int Part(const Square s) {
    return static_cast<int>(s > Square::_79);
  }

  template <typename F>
  FORCE_INLINE void loop(const F& f) const {
    uint64_t q0 = Extract<0>();
    while (q0) {
      const Square s = Square(pop_lsb(q0));
      f(s);
    }
    uint64_t q1 = Extract<1>();
    while (q1) {
      const Square s = Square(pop_lsb(q1) + 63);
      f(s);
    }
  }

  //! 下位bitから1bit拾ってそのbit位置を返す。
  /*! 絶対に1bitはnon zeroと仮定
      pop */
  FORCE_INLINE Square Pop() {
    uint64_t q0 = Extract<0>();
    Square s;
    if (q0 != 0) {
      s = Square(pop_lsb(q0));
      Insert<0>(q0);
    } else {
      uint64_t q1 = Extract<1>();
      s = Square(pop_lsb(q1) + 63);
      Insert<1>(q1);
    }
    return s;
  }

  //! このBitboardの値を変えないpop()
  /*! pop_c */
  FORCE_INLINE Square PopC() const {
    uint64_t q0 = Extract<0>();
    return q0 != 0 ? Square(LSB64(q0)) : Square(LSB64(Extract<1>()) + 63);
  }

  //! pop()をp[0],p[1]に分けて片側ずつ利用
  /*! pop_from_p0 と pop_from_p1 を統合 */
  template <int N>
  FORCE_INLINE Square PopFromPn() {
    uint64_t q = Extract<N>();
    ASSERT_LV3(q != 0);
    Square s(pop_lsb(q) + 63 * N);
    Insert<N>(q);
    return s;
  }

  //! 1のbitを数えて返す
  /*! pop_count */
  int PopCount() const {
    return static_cast<int>(POPCNT64(Extract<0>()) + POPCNT64(Extract<1>()));
  }

  Bitboard& operator|=(const Bitboard& bb) {
    _mm_store_si128(&m, _mm_or_si128(m, bb.m));
    return *this;
  }
  Bitboard& operator&=(const Bitboard& bb) {
    _mm_store_si128(&m, _mm_and_si128(m, bb.m));
    return *this;
  }
  Bitboard& operator^=(const Bitboard& bb) {
    _mm_store_si128(&m, _mm_xor_si128(m, bb.m));
    return *this;
  }
  Bitboard& operator+=(const Bitboard& bb) {
    _mm_store_si128(&m, _mm_add_epi64(m, bb.m));
    return *this;
  }
  Bitboard& operator-=(const Bitboard& bb) {
    _mm_store_si128(&m, _mm_sub_epi64(m, bb.m));
    return *this;
  }
  //! 左シフト(縦型Bitboardでは左1回シフトで1段下の升に移動する)
  Bitboard& operator<<=(const int shift) {
    _mm_store_si128(&m, _mm_slli_epi64(m, shift));
    return *this;
  }
  //! 右シフト(縦型Bitboardでは右1回シフトで1段上の升に移動する)
  Bitboard& operator>>=(const int shift) {
    _mm_store_si128(&m, _mm_srli_epi64(m, shift));
    return *this;
  }

  Bitboard& operator&=(const Square s);
  Bitboard& operator|=(const Square s);
  Bitboard& operator^=(const Square s);

  bool operator==(const Bitboard& rhs) const {
    const __m128i neq = _mm_xor_si128(m, rhs.m);
    return _mm_test_all_zeros(neq, neq) ? true : false;
  }

  Bitboard operator|(const Bitboard& rhs) const {
    return Bitboard(_mm_or_si128(m, rhs.m));
  }
  Bitboard operator&(const Bitboard& rhs) const {
    return Bitboard(_mm_and_si128(m, rhs.m));
  }
  Bitboard operator^(const Bitboard& rhs) const {
    return Bitboard(_mm_xor_si128(m, rhs.m));
  }
  Bitboard operator+(const Bitboard& rhs) const {
    return Bitboard(_mm_add_epi64(m, rhs.m));
  }
  Bitboard operator-(const Bitboard& rhs) const {
    return Bitboard(_mm_sub_epi64(m, rhs.m));
  }
  Bitboard operator<<(const int shift) const {
    return Bitboard(_mm_slli_epi64(m, shift));
  }
  Bitboard operator>>(const int shift) const {
    return Bitboard(_mm_srli_epi64(m, shift));
  }

  static const Bitboard All, Zero;

  //! range-forで回せるようにするためのhack
  /*! 少し遅いので速度が要求されるところでは使わないこと */
  Square operator*() { return Pop(); }
  void operator++() {}

  union {
    uint64_t p[2];
    __m128i m;
  };
};

// 抑制していた警告を元に戻す。
#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic warning "-Wmaybe-uninitialized"
#endif

Bitboard operator|(const Bitboard& bb, const Square s);
Bitboard operator&(const Bitboard& bb, const Square s);
Bitboard operator^(const Bitboard& bb, const Square s);
Bitboard operator~(const Bitboard& bb);

Bitboard begin(const Bitboard& bb);
Bitboard end(const Bitboard&);

//! デバッグ用出力
/*! Bitboardの1の升を'*'、0の升を'.'として表示 */
std::ostream& operator<<(std::ostream& os, const Bitboard& bb);

namespace detail {
extern const std::array<Bitboard, Color::SIZE> enemy_field_table;
}
/**
 * @brief 敵陣を表現するBitboardを取得する
 * @param us 
 * @return 
*/
inline const Bitboard& GetEnemyField(const Color us) noexcept {
  return detail::enemy_field_table[us];
}
template <Color::Value Us>
const Bitboard& GetEnemyField() noexcept {
  return detail::enemy_field_table[Us];
}

/**
 * @brief 各筋を表現するBitboard定数
*/
extern const Bitboard FILE1_BB, FILE2_BB, FILE3_BB, FILE4_BB, FILE5_BB,
    FILE6_BB, FILE7_BB, FILE8_BB, FILE9_BB;
/**
 * @brief 各段を表現するBitboard定数
*/
extern const Bitboard RANK1_BB, RANK2_BB, RANK3_BB, RANK4_BB, RANK5_BB,
    RANK6_BB, RANK7_BB, RANK8_BB, RANK9_BB;
/**
 * @brief 各筋を表現するBitboard配列
*/
extern const std::array<Bitboard, File::SIZE> FILE_BB;
/**
 * @brief 各段を表現するBitboard配列
*/
extern const std::array<Bitboard, Rank::SIZE> RANK_BB;

extern const Bitboard FORWARD_RANK_BB[Color::SIZE][Rank::SIZE];
inline const Bitboard& GetForwardRankBB(const Color us,
  const Rank r) {
  ASSERT_LV2(us.ok());
  return FORWARD_RANK_BB[us][us == Color::BLACK ? r.value + 1 : 7 - r.value];
}

}  // namespace state_action
#endif  // !BITBOARD_H_INCLUDED
