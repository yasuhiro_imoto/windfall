#pragma once
#ifndef MOVE_HELPER_H_INCLUDED
#define MOVE_HELPER_H_INCLUDED

#include "../rule/color.h"
#include "../rule/square.h"

namespace state_action {
/**
 * @brief ぞれぞれの手番にとっての歩の前方
 */
template <Color::Value C>
struct Forward;
template <>
struct Forward<Color::BLACK> {
  static constexpr Square value = Square::U;
};
template <>
struct Forward<Color::WHITE> {
  static constexpr Square value = Square::D;
};

/**
 * @brief 歩、香、桂が絶対に成らなければならない段
 *        また歩、香、桂を打てない段
 */
template <Color::Value C>
struct EndRank;
template <>
struct EndRank<Color::BLACK> {
  static constexpr Rank value = Rank::_1;
  static constexpr Rank value2 = Rank::_2;
  //! 王手の時に桂で合駒ができるかに関係
  static constexpr Rank value3 = Rank::_3;
};
template <>
struct EndRank<Color::WHITE> {
  static constexpr Rank value = Rank::_9;
  static constexpr Rank value2 = Rank::_8;
  //! 王手の時に桂で合駒ができるかに関係
  static constexpr Rank value3 = Rank::_7;
};

}  // namespace state_action
#endif  // !MOVE_HELPER_H_INCLUDED
