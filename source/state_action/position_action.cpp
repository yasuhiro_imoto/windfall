#include "stdafx.h"

#include "position.h"

#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../rule/color.h"
#include "../rule/entering_ou.h"
#include "../rule/square.h"
#include "../search/search_limit.h"
#include "../utility/level_assert.h"
#include "bitboard.h"
#include "direction.h"
#include "effect.h"
#include "piece_location.h"

namespace state_action {
bool Position::IsLegal(const Move move) const {
  if (move.drop()) {
    // 指し手の生成段階で確認済み
    return true;
  }

  const Square source = move.source();
  ASSERT_LV5(location_[source].color() == side_);
  ASSERT_LV5(location_[location_.ou_square(side_)] == Piece(side_, Piece::OU));

  if (location_[source].type() == Piece::OU) {
    // 移動先に相手の利きがないことを確かめる
    return !IsEffected(~side_, move.target(), source);
  }

  // blockerは先後を区別しないが、sourceの駒は自分のものであることは保証される
  return !(blocker(side_) & source) ||
         IsAligned(source, move.target(), location_.ou_square(side_));
}

template bool Position::IsPseudoSublegal<true>(const Move move) const;
template bool Position::IsPseudoSublegal<false>(const Move move) const;

template <bool All>
inline bool Position::IsPseudoSublegal(const Move move) const {
  const Color us = side();
  const Square target = move.target();

  if (move.drop()) {
    // 元々は指し手生成ルーチンで作ったはずなので、持ち駒として妥当な範囲の値のはず
    const Piece piece_raw_type = move.dropped_piece();

#ifdef KEEP_PIECE_IN_GENERATED_MOVE
    if (moved_piece(move) != Piece(us, piece_raw_type).value + Piece::DROP) {
      // 上位16bitと値が一致しない
      return false;
    }
#endif  // KEEP_PIECE_IN_GENERATED_MOVE

    ASSERT_LV3(piece_raw_type >= Piece::FU && piece_raw_type < Piece::OU);

    if (location_[target] != Piece::EMPTY ||
        location_.hand(us).count(piece_raw_type) == 0) {
      // 移動先に駒があるか持ち駒にない
      return false;
    }

    if (checked()) {
      // 王手されているので、合駒のみ有効
      Bitboard b = checker();
      const Square checker_square = b.Pop();

      if (b) {
        // このタイミングで王手している駒がまだあるということは
        // 元々で王手している駒が2枚あるということ
        // 両王手で合駒はできない
        return false;
      }

      if (!(GetBetweenBB(checker_square, location_.ou_square(us)) & target)) {
        // 合駒になっていない
        return false;
      }
    }

    if (piece_raw_type == Piece::FU && !IsLegalFuDropEx(target)) {
      // 二歩か打ち歩詰め
      return false;
    }

    // 歩、香、桂の移動できないところへの指し手は元々生成されないので大丈夫
    // 置換表で先後のハッシュは混ざらないので大丈夫
    // counter moveは手番に関係ないが駒種を保存しているので大丈夫
  } else {
    const Square source = move.source();
    const Piece piece = location_[source];

    if (piece == Piece::EMPTY || piece.color() != us) {
      // 自分の駒でない
      return false;
    }

    if (!(GetEffect(piece, source, location_.get<>()) & target)) {
      // targetに移動できない
      return false;
    }

    if (location_[target] != Piece::EMPTY && location_[target].color() == us) {
      // targetに自分の駒がある
      return false;
    }

    const Piece piece_type = piece.type();
    if (move.promotion()) {
      static_assert(Piece::KI == 7, "");
      if (piece_type >= Piece::KI) {
        // 成れない駒
        return false;
      }

#ifdef KEEP_PIECE_IN_GENERATED_MOVE
      if (moved_piece(move) != piece.value + Piece::PROMOTION) {
        // 上位16bitと一致しない
        return false;
      }
#else
      if (!(GetEnemyField(us) & (Bitboard(source) | Bitboard(target)))) {
        // 移動元か移動先が敵陣でないので成れない
        return false;
      }
#endif  // KEEP_PIECE_IN_GENERATED_MOVE
    } else {
#ifdef KEEP_PIECE_IN_GENERATED_MOVE
      if (moved_piece(move) != piece) {
        // 上位16bitと一致しない
        return false;
      }
#endif  // KEEP_PIECE_IN_GENERATED_MOVE

      if (All) {
        // 一番奥まで不成で進む場合を確認
        // 桂馬は動きが独特で他の駒と間違える可能性はないので確認する必要はない
        if ((piece_type == Piece::FU || piece_type == Piece::KY) &&
            ((us == Color::BLACK && target.rank() == Rank::_1) ||
             (us == Color::WHITE && target.rank() == Rank::_9))) {
          return false;
        }
      } else {
        // 歩、香の2段目、大駒の不成を違法とみなす
        switch (piece_type.value) {
          case Piece::FU:
            if (GetEnemyField(us) & target) {
              return false;
            }
            break;
          case Piece::KY:
            if ((us == Color::BLACK && target.rank() <= Rank::_2) ||
                (us == Color::WHITE && target.rank() >= Rank::_8)) {
              return false;
            }
            break;
          case Piece::KA:
          case Piece::HI:
            if (GetEnemyField(us) & (Bitboard(source) | Bitboard(target))) {
              return false;
            }
            break;
          default:
            break;
        }
      }
    }

    const Bitboard c = checker();
    if (c) {
      // この時の指し手はEVASIONと同等以上でなければならない
      if (piece.type() != Piece::OU) {
        if (IsMultiple(c)) {
          // 両王手なので王を動かすしかない
          return false;
        }

        if (!((GetBetweenBB(checker().Pop(), location_.ou_square(us)) | c) &
              target)) {
          // 中合か王手している駒を捕る必要がある
          // 素抜きに遭う可能性があるがここでの確認は不要
          return false;
        }
      }
    }
    // 王の自殺手はIsLegalの方に含まれる
  }

  return true;
}

bool Position::IsLegalFuDrop(const Square target) const {
  const Color us = side();

  // この関数を呼び出す前提条件
  // 歩の正面に相手の玉がいるはず
  ASSERT_LV3(GetFuEffect(us, target) == Bitboard(location_.ou_square(~us)));

  if (!IsEffected(us, target)) {
    // この歩に自分の駒の利きがないなら詰みはない
    return true;
  }

  // この歩に利きのある相手の駒
  const Bitboard b = attacker_fu(~us, target);
  // 敵玉についてピンされている駒
  const Bitboard pinned = blocker(~us);
  // 下がりながらこの歩を捕るのはピンに関係ない
  if (b & (~pinned | FILE_BB[target.file()])) {
    // ピンされていない駒があるならそれで捕れる
    return true;
  }

  const Square ou_square = location_.ou_square(~us);
#ifdef LONG_EFFECT_LIBRARY
#else
  // 愚直に逃げられる場所を探す

  Bitboard escape_bb = GetOuEffect(ou_square) & ~location_[~us];
  escape_bb ^= target;

  // targetの位置に歩を置く予定なので、その状態で考える
  const Bitboard occupied = location_.get<>() ^ target;

  while (escape_bb) {
    const Square candidate = escape_bb.Pop();
    if (!attacker(us, candidate, occupied)) {
      // 逃げ場所が見つかったので詰みではない
      return true;
    }
  }
#endif  // LONG_EFFECT_LIBRARY

  // 打ち歩詰めだった
  return false;
}

#ifdef USE_SEE
/**
 * @brief targetに移動できる駒のうち、最も価値の低い駒を返す
 *
 * 選択した駒がなくなることで利きが通る場合があるので、
 * occupiedとattackersの更新を行う
 * @param position
 * @param target
 * @param stm_attackers
 * @param occupied
 * @param attackers
 * @return
 * min_atacker
 */
template <Color::Value stm>
Piece GetLeastAttacker(const Position& position, const Square target,
                       const Bitboard stm_attackers, Bitboard& occupied,
                       Bitboard& attackers) {
  // targetに移動できる駒が存在する
  ASSERT_LV5(stm_attackers);

  Bitboard b;
  b = stm_attackers & position.types<Piece::FU>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::KY>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::KE>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::GI>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::GOLDS>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::KA>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::HI>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::UM>();
  if (b) {
    goto found;
  }
  b = stm_attackers & position.types<Piece::RY>();
  if (b) {
    goto found;
  }

  // 利きのある駒が存在するので、王だけ
  // 王が移動することによって新たに利きが発生して王が捕られるということはないので、
  // occupiedなどの更新は不要
  return Piece::OU;

found:;

  // ピンされている駒はこの関数の外側で候補から取り除かれているので、ここでの操作は合法手
  const Square square = b.Pop();
  occupied ^= square;

  // squareにある駒がなくなるので、targetからsquareへの方角にある駒を調べて、追加
  auto direction = effect8::GetDirection(target, square);
  // 桂馬の可能性がある
  if (direction) {
    switch (direction.Pop()) {
      case effect8::Direction::RU:
      case effect8::Direction::LD:
        attackers |= detail::GetKaDiagonalEffect<0>(target, occupied) &
                     position.types<Piece::BISHOP_HORSE>();
        ASSERT_LV3(detail::GetKaNaiveEffect(target) & square);
        break;
      case effect8::Direction::RD:
      case effect8::Direction::LU:
        attackers |= detail::GetKaDiagonalEffect<1>(target, occupied) &
                     position.types<Piece::BISHOP_HORSE>();
        ASSERT_LV3(detail::GetKaNaiveEffect(target) & square);
        break;
      case effect8::Direction::U:
        // 飛と後手の香
        attackers |= GetEffect<Piece::KY, Color::BLACK>(target, occupied) &
                     (position.types<Piece::ROOK_DRAGON>() |
                      position.color<Color::WHITE, Piece::KY>());
        ASSERT_LV3(detail::GetKyNaiveEffect(Color::BLACK, target) & square);
        break;
      case effect8::Direction::D:
        // 飛と先手の香
        attackers |= GetEffect<Piece::KY, Color::WHITE>(target, occupied) &
                     (position.types<Piece::ROOK_DRAGON>() |
                      position.color<Color::BLACK, Piece::KY>());
        ASSERT_LV3(detail::GetKyNaiveEffect(Color::WHITE, target) & square);
        break;
      case effect8::Direction::R:
      case effect8::Direction::L:
        attackers |= detail::GetHiRankEffect(target, occupied) &
                     position.types<Piece::ROOK_DRAGON>();
        ASSERT_LV3(detail::GetHiNaiveEffect(target) & square);
        break;
      default:
        UNREACHABLE;
        break;
    }
  } else {
    //  飛と角の方向ではない
    ASSERT_LV3(!(
        (detail::GetKaNaiveEffect(target) | detail::GetHiNaiveEffect(target)) &
        square));
  }

  attackers &= occupied;
  return position[square].type();
}

bool Position::IsSeeGe(const Move move,
                       const evaluation::Value threshold) const {
  // null windowの時のalpha beta searchに似たアルゴリズムを用いる
  // 最後に成った駒の成りによる価値の上昇分は考慮しない

  const bool drop = move.drop();
  // 駒打ちの場合にtypes() ^ sourceを無効化するハック
  const Square source = drop ? Square::SIZE : move.source();
  const Square target = move.target();

  // 次にtargetのマスで捕られる駒
  // 成りによる上昇分は考慮しない
  Piece next_victim = drop ? move.dropped_piece() : location_[source].type();

  const Color us = moved_piece(move).color();
  ASSERT_LV3(us == side());
  // side to move
  Color stm = ~us;

  // 駒の取り合いによる収支
  evaluation::Value balance =
      static_cast<evaluation::Value>(
          evaluation::captured_value_delta[location_[target]]) -
      threshold;
  if (balance < evaluation::Value::ZERO) {
    // この時点でマイナスなので、見込みなしと判断
    return false;
  }

  // next_victimが王になっている場合もあるが、王が捕られる可能性は考慮しなくてよい
  // そのため、王の場合は収支がプラスといえる
  // captured_value_delta[Piece::OU] == 0なので、次の処理は必ず真になる
  balance -= static_cast<evaluation::Value>(
      evaluation::captured_value_delta[next_victim]);
  if (balance >= evaluation::Value::ZERO) {
    return true;
  }

  // trueなら相手番、falseなら自分
  bool relative_stm = true;

  // 手番側のtargetに利きがある駒
  Bitboard stm_attackers;

  // sourceの駒を取り除く
  Bitboard occupied = types() ^ source ^ target;
  // targetに利きがある駒を列挙
  Bitboard attackers = attacker(target, occupied) & occupied;

  while (true) {
    stm_attackers = attackers & location_.get(stm);
    // TODO: YaneuraOuの条件が間違っているような気がするので修正した
    //       これが正しいのか要確認
    if (rb_->pinners_for_ou[stm] & occupied) {
      // stmの王についてピンを発生させている相手の駒がある（まだ捕り合いの中で消えていない）
      // ピンされている駒がtargetに移動すると空き王手になる
      // ピンされている駒が利きの直線上を移動するパターンが存在する
      //

      // TODO:
      // YaneuraOuではピンされている駒が利きの直線上を移動するパターンを見逃しているはず
      //       修正と確認が必要
      //       王の位置からtargetを通る半直線のマスクで利きの直線上を移動する駒を保護するのがよさそう
      stm_attackers &= ~rb_->blockers_for_ou[us];
    }

    if (!stm_attackers) {
      break;
    }

    next_victim = (stm == Color::BLACK)
                      ? GetLeastAttacker<Color::BLACK>(
                            *this, target, stm_attackers, occupied, attackers)
                      : GetLeastAttacker<Color::WHITE>(
                            *this, target, stm_attackers, occupied, attackers);

    // alpha=balance, beta=balance+1としてnull windows searchしているので、
    // (balance, balance+1) -> (-balance-1, -balance)
    // と変化する
    ASSERT_LV3(balance < evaluation::Value::ZERO);
    balance = -balance - 1 - evaluation::captured_value_delta[next_victim];

    // next_victimを捕られてもbalanceが非負ならthresholdよりも駒得したので、成立する
    if (balance >= evaluation::Value::ZERO) {
      // 王で捕るのは他に駒がない場合
      if (next_victim == Piece::OU && (attackers & location_.get(~stm))) {
        // 王が捕られてしまう
        // 自分の出番でないなら成立
        break;
      }
      stm = ~stm;
      break;
    }
    // 手番を更新
    stm = ~stm;
  }
  return stm != us;
}
#endif  // USE_SEE

bool Position::IsCheckMove(const Move move) const {
  // 前提として指し手が正しいか
  ASSERT_LV3(move.ok());

  const Square target = move.target();

  if (rb_->check_squares[moved_piece(move).type()] & target) {
    // 移動した駒もしくは打った駒で王手
    return true;
  }

  // 空き王手かどうか
  return !move.drop() &&
         ((blocker(~side()) & move.source()) &&
          !IsAligned(move.source(), target, location_.ou_square(~side())));
}

Move Position::Declarate() const {
  const EnteringOuRule rule = search::limit.entering_ou_rule;
  switch (rule) {
    case EnteringOuRule::NONE:
      return Move::NONE;
    case EnteringOuRule::POINTS24:
    case EnteringOuRule::POINTS27: {
      // CSAルールでは27点法
      // 入玉が成立する条件
      // 1. 玉が敵陣の三段目以内に入っている
      // 2. 大駒を5点、小駒を1点として
      //    先手は28点以上、後手は27点以上ある
      //    点数の対象は敵陣の三段目以内の駒と持ち駒
      // 3. 敵陣に王を除いて10枚以上存在する
      // 4. 王に王手がかかっていない

      const Color us = side();
      const Bitboard enemy_field = GetEnemyField(us);

      // 1. 敵陣にいるか
      if (!(enemy_field & location_.ou_square(us))) {
        return Move::NONE;
      }

      // 4. 王手がかかっていないか
      if (checker()) {
        return Move::NONE;
      }

      // 3. 10枚以上あるか
      // 玉も数えているので、その分を補正
      const int p1 = (enemy_field & location_[us]).PopCount() - 1;
      if (p1 < 10) {
        return Move::NONE;
      }

      // 大駒の枚数
      const int p2 =
          (enemy_field &
           location_.get<Piece::BISHOP_HORSE, Piece::ROOK_DRAGON>(us))
              .PopCount();
      const Hand h = location_.hand(us);
      // 大駒は5点なので、敵陣の駒の枚数では1点しか数えていない
      // 残りの4点を追加する
      const int score = p1 + p2 * 4 + h.count(Piece::FU) + h.count(Piece::KY) +
                        h.count(Piece::KE) + h.count(Piece::GI) +
                        (h.count(Piece::KA) + h.count(Piece::HI)) * 5 +
                        h.count(Piece::KI);
      // 27点法では先手28点、後手27点以上で勝利
      // 24点法では31点以上で勝利(30点以下は引き分け)
      if (score < (rule == EnteringOuRule::POINTS27
                       ? (us == Color::BLACK ? 28 : 27)
                       : 31)) {
        return Move::NONE;
      } else {
        return Move::WIN;
      }
    }
    case EnteringOuRule::TRY_RULE: {
      const Color us = side();
      const Square try_square = us == Color::BLACK ? Square::_51 : Square::_59;
      const Bitboard try_bb(try_square);
      const Square ou_square = location_.ou_square(us);

      if (!(GetOuEffect(ou_square) & try_bb)) {
        // トライできる場所まで届かない
        return Move::NONE;
      }

      if (location_[us] & try_bb) {
        // 自分の駒駒があって移動できない
        return Move::NONE;
      }

      if (IsEffected(~us, try_square, ou_square)) {
        // 相手の利きがあってトライできない
        return Move::NONE;
      }

      return Move(ou_square, try_square);
    }
    default:
      UNREACHABLE;
      return Move::NONE;
  }
}
}  // namespace state_action
