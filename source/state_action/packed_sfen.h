#pragma once
#ifndef PACKED_SFEN_H_INCLUDED
#define PACKED_SFEN_H_INCLUDED

#include <array>
#include <cstdint>

namespace state_action {
struct PackedSfen {
  using Array = std::array<uint8_t, 32>;
  Array data;
};
}
#endif // !PACKED_SFEN_H_INCLUDED


