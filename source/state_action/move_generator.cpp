#include "stdafx.h"

#include "move_generator.h"

#include "../rule/color.h"
#include "../rule/square.h"
#include "../utility/force_inline.h"
#include "extended_move.h"
#include "move_generator/generator_check.h"
#include "move_generator/generator_evasion.h"
#include "move_generator/generator_general.h"
#include "position.h"

namespace state_action {
// ---------------------------------------------
// 指し手を生成(テンプレートに振り分ける)
// ---------------------------------------------

template <MoveGeneratorType Type, bool All>
void GenerateMoves(const Position& position, ExtendedMoveArray& move_list,
                   const Square square = Square::SIZE) {
  if (position.side() == Color::BLACK) {
    move_generator::GenerateGeneral<Type, Color::BLACK, All>(position,
                                                             move_list, square);
  } else {
    move_generator::GenerateGeneral<Type, Color::WHITE, All>(position,
                                                             move_list, square);
  }
}

template <bool All>
void GenerateEvasionMoves(const Position& position,
                          ExtendedMoveArray& move_list) {
  if (position.side() == Color::BLACK) {
    move_generator::GenerateEvasions<Color::BLACK, All>(position, move_list);
  } else {
    move_generator::GenerateEvasions<Color::WHITE, All>(position, move_list);
  }
}

template <MoveGeneratorType Type, bool All>
void GenerateCheckMoves(const Position& position,
                        ExtendedMoveArray& move_list) {
  if (position.side() == Color::BLACK) {
    move_generator::GenerateChecks<Type, Color::BLACK, All>(position,
                                                            move_list);
  } else {
    move_generator::GenerateChecks<Type, Color::WHITE, All>(position,
                                                            move_list);
  }
}

/**
 * @brief MoveGeneratorの特殊化のための値
 */
enum class MoveGeneratorCategory { LEGAL, CHECK, EVASION, OTHER };

constexpr MoveGeneratorCategory GetMoveGeneratorCategory(
    const MoveGeneratorType type) {
  switch (type) {
    case MoveGeneratorType::LEGAL:
      return MoveGeneratorCategory::LEGAL;
    case MoveGeneratorType::CHECKS:
    case MoveGeneratorType::QUIET_CHECKS:
      return MoveGeneratorCategory::CHECK;
    case MoveGeneratorType::EVASIONS:
      return MoveGeneratorCategory::EVASION;
    default:
      break;
  }
  return MoveGeneratorCategory::OTHER;
}

template <MoveGeneratorType Type, bool All, MoveGeneratorCategory Category>
struct MoveGenerator {
  FORCE_INLINE static void Call(const Position& position,
                                ExtendedMoveArray& move_list,
                                const Square recapture_square) {
    GenerateMoves<RemoveAll(Type), All>(position, move_list, recapture_square);
  }
};
template <bool All>
struct MoveGenerator<MoveGeneratorType::LEGAL, All,
                     MoveGeneratorCategory::LEGAL> {
  FORCE_INLINE static void Call(const Position& position,
                                ExtendedMoveArray& move_list,
                                const Square recapture_square) {
    // 合法手かどうかのチェックが入っているので遅い
    // 基本的には使うべきではない
    if (position.checked()) {
      GenerateEvasionMoves<All>(position, move_list);
    } else {
      GenerateMoves<MoveGeneratorType::NON_EVASIONS, All>(position, move_list);
    }
    move_list.RemoveIllegalMoves(position);
  }
};
template <MoveGeneratorType Type, bool All>
struct MoveGenerator<Type, All, MoveGeneratorCategory::CHECK> {
  FORCE_INLINE static void Call(const Position& position,
                                ExtendedMoveArray& move_list,
                                const Square recapture_square) {
    GenerateCheckMoves<Type, All>(position, move_list);
    // 王手がかかっている時に呼ばれると王手を放置して駒打ちなどをしている可能性がある
    move_list.RemovePsuedoIllegalMoves(position);
  }
};
template <bool All>
struct MoveGenerator<MoveGeneratorType::EVASIONS, All,
                     MoveGeneratorCategory::EVASION> {
  FORCE_INLINE static void Call(const Position& position,
                                ExtendedMoveArray& move_list,
                                const Square recapture_square) {
    GenerateEvasionMoves<All>(position, move_list);
  }
};

template <MoveGeneratorType Type>
void GenerateMoves(const Position& position, ExtendedMoveArray& move_list,
                   const Square recapture_square) {
  constexpr bool All = IsAll(Type);
  constexpr MoveGeneratorType T = RemoveAll(Type);
  MoveGenerator<T, All, GetMoveGeneratorCategory(T)>::Call(position, move_list,
                                                           recapture_square);
}

template <MoveGeneratorType Type>
void GenerateMoves(const Position& position, ExtendedMoveArray& move_list) {
  static_assert(RemoveAll(Type) != MoveGeneratorType::RECAPTURES,
                "'Recaptures' is not allowed.");
  GenerateMoves<Type>(position, move_list, Square::SIZE);
}

// ----------------------------------------
// テンプレートの実体化
// ----------------------------------------

template void GenerateMoves<MoveGeneratorType::NON_CAPTURES>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::CAPTURES>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS>(
    const Position& position, ExtendedMoveArray& move_list);
template void
GenerateMoves<MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::EVASIONS>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::EVASIONS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::NON_EVASIONS>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::NON_EVASIONS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::LEGAL>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::LEGAL_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::CHECKS>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::CHECKS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::QUIET_CHECKS>(
    const Position& position, ExtendedMoveArray& move_list);
template void GenerateMoves<MoveGeneratorType::QUIET_CHECKS_ALL>(
    const Position& position, ExtendedMoveArray& move_list);

template void GenerateMoves<MoveGeneratorType::RECAPTURES>(
    const Position& position, ExtendedMoveArray& move_list,
    const Square recapture_square);
template void GenerateMoves<MoveGeneratorType::RECAPTURES_ALL>(
    const Position& position, ExtendedMoveArray& move_list,
    const Square recapture_square);
}  // namespace state_action
