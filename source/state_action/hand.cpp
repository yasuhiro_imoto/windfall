#include "stdafx.h"

#include "hand.h"

namespace state_action {
std::ostream& operator<<(std::ostream& os, const Hand hand) {
  os << boost::format(
            "Hand[FU:%1%,KY:%2%,KE:%3%,GI:%4%,KA:%5%,HI:%6%,KI:%7%]") %
            hand.count(Piece::FU) % hand.count(Piece::KY) %
            hand.count(Piece::KE) % hand.count(Piece::GI) %
            hand.count(Piece::KA) % hand.count(Piece::HI) %
            hand.count(Piece::KI);
  return os;
}
}  // namespace state_action
