#pragma once
#ifndef ZOBRIST_HASH_H_INCLUDED
#define ZOBRIST_HASH_H_INCLUDED

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/configuration.h"
#include "../utility/hash_key.h"

namespace state_action {
namespace zobrist {
constexpr HashKey zero = 0;
constexpr HashKey side = 1;
extern HashKey psq[Square::SIZE_PLUS1][Piece::SIZE];
extern HashKey hand[Color::SIZE][Piece::HAND_SIZE];
extern HashKey depth[max_ply];

void Initialize();
} // namespace zobrist
} // namespace state_action
#endif // !ZOBRIST_HASH_H_INCLUDED
