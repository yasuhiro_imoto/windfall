#include "stdafx.h"

#include "position.h"

#include "../rule/color.h"
#include "../rule/square.h"
#include "../utility/level_assert.h"
#include "bitboard.h"
#include "move_generator.h"

namespace state_action {
Bitboard Position::attacker(const Color color, const Square target,
                            const Bitboard& occupied) const {
  ASSERT_LV3(color.ok() && target <= Square::SIZE);

  const Color them = ~color;

  // targetの地点に敵駒ptをおいて、その利きに自駒のptがあればtargetに利いている

  auto effect = GetFuEffect(them, target) & location_.get<Piece::FU>();
  effect |= GetKeEffect(them, target) & location_.get<Piece::KE>();
  // 金と銀の利きで8隣接をカバーしているので、王、龍、馬の8隣接を同時にチェックする
  effect |= GetGiEffect(them, target) & location_.get<Piece::SILVER_HDK>();
  effect |= GetKiEffect(them, target) & location_.get<Piece::GOLDS_HDK>();

  effect |=
      GetKaEffect(target, occupied) & location_.get<Piece::BISHOP_HORSE>();
  // 香も同時にチェック
  effect |=
      GetHiEffect(target, occupied) &
      (location_.get<Piece::ROOK_DRAGON>() |
       (detail::GetKyNaiveEffect(them, target) & location_.get<Piece::KY>()));

  effect &= location_[color];
  return effect;
}

Bitboard Position::attacker(const Square target,
                            const Bitboard& occupied) const {
  ASSERT_LV3(target <= Square::SIZE);
  // squareの地点に敵駒ptをおいて、その利きに自駒のptがあればsquareに利いている

  // 先手の近接の利き
  auto effect_b =
      GetFuEffect(Color::WHITE, target) & location_.get<Piece::FU>();
  effect_b |= GetKeEffect(Color::WHITE, target) & location_.get<Piece::KE>();
  effect_b |=
      GetGiEffect(Color::WHITE, target) & location_.get<Piece::SILVER_HDK>();
  effect_b |=
      GetKiEffect(Color::WHITE, target) & location_.get<Piece::GOLDS_HDK>();
  effect_b &= location_.color<Color::BLACK>();

  // 後手の近接の利き
  auto effect_w =
      GetFuEffect(Color::BLACK, target) & location_.get<Piece::FU>();
  effect_w |= GetKeEffect(Color::BLACK, target) & location_.get<Piece::KE>();
  effect_w |=
      GetGiEffect(Color::BLACK, target) & location_.get<Piece::SILVER_HDK>();
  effect_w |=
      GetKiEffect(Color::BLACK, target) & location_.get<Piece::GOLDS_HDK>();
  effect_w &= location_.color<Color::WHITE>();

  /// 遠隔の利き
  auto effect =
      GetKaEffect(target, occupied) & location_.get<Piece::BISHOP_HORSE>();
  effect |= GetHiEffect(target, occupied) &
            (location_.get<Piece::ROOK_DRAGON>() |
             (detail::GetKyNaiveEffect(Color::WHITE, target) &
              location_.color<Color::BLACK, Piece::KY>()) |
             (detail::GetKyNaiveEffect(Color::BLACK, target) &
              location_.color<Color::WHITE, Piece::KY>()));

  effect |= effect_b | effect_w;
  return effect;
}

Bitboard Position::attacker_fu(const Color color,
                               const Square fu_square) const {
  ASSERT_LV3(color.ok() && fu_square <= Square::SIZE);

  const Color them = ~color;
  const Bitboard& occupied = location_.get<>();
  // TODO: 最後に利きのある駒から王を取り除いた方が処理が少ないと思われる 要確認
  // 馬、龍の近接の利きに利用
  // HDKからKを取り除く
  const Bitboard bb_hd =
      GetOuEffect(fu_square) & location_.get<Piece::UM, Piece::RY>();

  auto effect = GetKeEffect(them, fu_square) & location_.get<Piece::KE>();
  effect |= GetGiEffect(them, fu_square) & (location_.get<Piece::GI>() | bb_hd);
  effect |=
      GetKiEffect(them, fu_square) & (location_.get<Piece::GOLDS>() | bb_hd);
  effect |=
      GetKaEffect(fu_square, occupied) & location_.get<Piece::BISHOP_HORSE>();
  effect |=
      GetHiEffect(fu_square, occupied) & location_.get<Piece::ROOK_DRAGON>();
  effect &= location_[color];
  return effect;
}

Bitboard Position::blocker(const Color color, const Square target,
                           Bitboard& pinner) const {
  Bitboard blocker_bb = Bitboard::Zero;
  pinner = Bitboard::Zero;

  Bitboard candidate = GetPinnerCandidate(color, target);

  while (candidate) {
    const Square source = candidate.Pop();
    const Bitboard b = GetBetweenBB(target, source) & location_.get<>();
    // 王手はないので、間にある駒は1以上
    if (b && !IsMultiple(b)) {
      // 間にある駒は1枚だけ
      blocker_bb |= b;

      if (b & location_[~color]) {
        // 遮っているのは相手方の駒
        pinner |= source;
      }
    }
  }

  return blocker_bb;
}

Bitboard Position::pinned(const Color color, const Square excluded) const {
  const Square ou_sq = location_.ou_square(color);

  // TODO: excludedに駒があることが保証されるならxorが使える
  Bitboard excluded_bb = ~Bitboard(excluded);

  Bitboard pinner = GetPinnerCandidate(~color, ou_sq) & excluded_bb;
  const Bitboard tmp = location_.get<>() & excluded_bb;

  return GetPinned(color, ou_sq, pinner, tmp);
}

Bitboard Position::pinned(const Color color, const Square source,
                          const Square target) const {
  const Square ou_sq = location_.ou_square(color);

  // TODO: excludedに駒があることが保証されるならxorが使える
  Bitboard excluded_bb = ~Bitboard(source);

  Bitboard pinner = GetPinnerCandidate(~color, ou_sq) & excluded_bb;
  // sourceからtargetに駒が移動した
  const Bitboard occupied = (location_.get<>() & excluded_bb) | target;

  return GetPinned(color, ou_sq, pinner, occupied);
}

bool Position::IsMated() const { return LegalMoveList(*this).empty(); }

Bitboard Position::GetPinnerCandidate(const Color color,
                                      const Square target) const {
  return ((location_.get<Piece::ROOK_DRAGON>() &
           detail::GetHiNaiveEffect(target)) |
          (location_.get<Piece::BISHOP_HORSE>() &
           detail::GetKaNaiveEffect(target)) |
          (location_.get<Piece::KY>() &
           detail::GetKyNaiveEffect(~color, target))) &
         location_[color];
}

Bitboard Position::GetPinned(const Color color, const Square target,
                             Bitboard& pinner, const Bitboard& occupied) const {
  Bitboard result = Bitboard::Zero;

  while (pinner) {
    const Bitboard b = GetBetweenBB(target, pinner.Pop()) & occupied;
    // NOTE: 0枚の場合を判定した方が速いかどうかは不明
    if (b && !IsMultiple(b)) {
      // 間に1枚だけある
      result |= b;
    }
  }

  // 守備方の駒だけにする
  result &= location_[color];
  return result;
}

}  // namespace state_action
