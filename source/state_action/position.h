#pragma once
#ifndef POSITION_H_INCLUDED
#define POSITION_H_INCLUDED
#include <array>
#include <cstdint>
#include <iosfwd>
#include <string>

#include "../evaluation/bonanza_piece.h"
#include "../evaluation/evaluation_list.h"
#include "../evaluation/kpp_index.h"
#include "../evaluation/value.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/repetition.h"
#include "../rule/square.h"
#include "../utility/force_inline.h"
#include "../utility/level_assert.h"
#include "effect.h"
#include "hand.h"
#include "move.h"
#include "piece_location.h"
#include "rollback.h"

namespace search {
class Thread;
}

namespace state_action {
#if defined(KEEP_PIECE_IN_GENERATED_MOVE)
//! 現局面で与えられた指し手でこれから移動する駒を返す
/*! 駒は先後の情報を含む */
constexpr Piece GetMovingPiece(const Move::Value m) {
  // 上位16ビットに格納されている値を利用する
  // 成りのビットを解除してやる
  static_assert(Move::PROMOTION == (1 << 15), "");
  static_assert(Piece::PROMOTION == 8, "");
  return static_cast<Piece>((m ^ ((m & Move::PROMOTION) << 4)) >> 16);
}
#endif

struct PackedSfen;

//! 盤面
class Position {
 public:
#pragma warning(push)
#pragma warning(disable : 26495)
  /**
   * @brief 初期化のタイミングの都合で平手で初期化できない
   *
   * 評価値の計算を行おうとするが、
   * 評価関数のテーブルがまだ初期化されていないので計算できない
   */
  Position() = default;
#pragma warning(pop)

  Position(const Position&) = delete;
  Position& operator=(const Position&) = delete;

  /**
   * @brief Zobristテーブルを初期化
   *
   * init
   */
  static void Initialize();

  /**
   * @brief sfen文字列で盤面を設定する
   *
   * 内部でInitializeを呼び出す
   * 局面を遡るために、rootまでの局面の情報が必要であるから、それを引数のrbで与える
   * 遡る必要がない場合は、ダミーの変数を与える
   * 内部でmemsetを使ってrbを初期化している
   * @param sfen
   * @param rb
   * @param thread
   * set
   */
  void Set(const std::string& sfen, Rollback* rb, search::Thread* thread);
  /**
   * @brief 平手の初期盤面を設定
   *
   * @param rb
   * @param thread
   * set_hirate
   */
  void SetHirate(Rollback* rb, search::Thread* thread) {
    Set(sfen_hirate, rb, thread);
  }

  /**
   * @brief PackedSfenから直接に局面を設定する
   * @param sfen 
   * @param rb 
   * @param thread 
   * @param mirror 
   * @param ply sfenに手数が含まれないので、必要なら指定する
   * @return 正常ならば0を返す
   *         局面に問題があってエラーの場合は負の値を返す
  */
  int Set(const PackedSfen& sfen, Rollback* rb, search::Thread* thread,
          const bool mirror = false, const int ply = 0);

  //! 局面をsfen文字列に変換
  /*! デバッグで利用したい
      sfen */
  std::string sfen() const;

  //
  // PROPERTY
  //

  //! 現局面での手番
  /*! side_to_move */
  Color side() const { return side_; }

  //! 初期局面からの手数
  /*! 初期局面の手数は1
      game_ply */
  int game_ply() const { return game_ply_; }

  //! このインスタンスが利用しているスレッドのポインタを返す
  /*! note:
   * 他のメンバ関数の引数と名前がかぶるのが気になったので、名前を少し変えた
     this_thread */
  search::Thread* ptr_thread() const { return ptr_thread_; }

  //! 保持しているデータに矛盾がないか確認
  /*! pos_is_ok */
  bool ok() const;

  //! 千日手かどうか、その種別を判定
  /*!  rep_ply: 遡る手数
      is_repetition */
  RepetitionState IsRepetition(const int rep_ply = 16) const;

  Key key() const { return rb_->key(); }

#if defined(CUCKOO)
  //! この局面から既出の局面に到達する指し手があるか
  /*! plies_from_root: rootからの手数 ss->plyを渡す
      rep_ply: 遡る手数
      has_game_cycle */
  bool HasGameCycle(const int plies_from_root, const int rep_ply = 16) const;
#endif  // defined(CUCKOO)

  Piece operator[](const Square square) const { return location_[square]; }
  Hand operator[](const Color color) const { return location_.hand(color); }

  const Bitboard& types() const { return types<Piece::ALL_PIECES>(); }
  template <Piece::Value P>
  const Bitboard& types() const {
    return location_.get<P>();
  }
  template <Piece::Value Head1, Piece::Value Head2, Piece::Value... Tail>
  Bitboard types() const {
    return location_.template get<Head1>() | location_.template get<Head2, Tail...>();
  }

  template <Color::Value C>
  const Bitboard& color() const {
    return location_.color<C>();
  }
  template <Color::Value C, Piece::Value Head, Piece::Value... Tail>
  Bitboard color() const {
    return location_.color<C>() & types<Head, Tail...>();
  }

  template <Color::Value C>
  Square ou_square() const {
    return location_.ou_square(C);
  }
  Square ou_square(const Color color) const {
    return location_.ou_square(color);
  }

  Bitboard empties() const;

  /**
   * @brief 直前に捕獲された駒
   *        先後の区別あり
   * @return
   * captured_piece
   */
  Piece captured_piece() const { return rb_->captured_piece; }

  //
  // STATE
  //

  //! 現局面で王手している駒
  /*! NOTE: const reference でも大丈夫な気はするが確信できない
      checkers */
  Bitboard checker() const { return rb_->checkers_bb; }

  //! color側の王に対する王手を邪魔している駒
  /*! 先後の区別はない
      blockers_for_king */
  Bitboard blocker(const Color color) const {
    return rb_->blockers_for_ou[color];
  }

  //! 現局面でpiece_typeを動かしたときに王手できる升
  /*! check_squares */
  Bitboard check_square(const Piece piece_type) const {
    ASSERT_LV3(piece_type != Piece::EMPTY && piece_type < Piece::WHITE);
    return rb_->check_squares[piece_type];
  }

  //! マスtargetに利きのあるcolor側の駒を列挙
  /*! colorの指定がなければ先後両方
      指定されたoccupied bitboardの下で利きを計算する
      指定されなければ現局面のoccupied bitboardを利用する
      attackers_to */
  Bitboard attacker(const Color color, const Square target) const {
    return attacker(color, target, location_.get<>());
  }
  Bitboard attacker(const Color color, const Square target,
                    const Bitboard& occupied) const;
  Bitboard attacker(const Square target) const {
    return attacker(target, location_.get<>());
  }
  Bitboard attacker(const Square target, const Bitboard& occupied) const;

  //! 打ち歩の状態でその歩を捕る利きのある駒を列挙する
  /*! 打たれた歩の位置をfu_squareとして、color側(王側)のfu_squareへ
      の利きのある駒を列挙する
      香の利きがないことは自明
      attackers_to_pawn */
  Bitboard attacker_fu(const Color color, const Square fu_square) const;

#ifdef LONG_EFFECT_LIBRARY
#else
  /**
   * @brief targetに利きのあるcolor側の駒が存在するか
   * @param color 
   * @param target 
   * @return 
   * effected_to
  */
  bool IsEffected(const Color color, const Square target) const {
    return attacker(color, target);
  }
  /**
   * @brief targetに利きのあるcolor側の駒が存在するか
   * @param color 
   * @param target 
   * @param ou_square この地点から王を取り除いたうえで利きを判定する
   * @return 
   * effected_to
  */
  bool IsEffected(const Color color, const Square target,
                  const Square ou_square) const {
    return attacker(color, target, location_.get<>() ^ ou_square);
  }
  /**
   * @brief targetに利きのあるcolor側の駒が存在するか
   * @param color 
   * @param target 
   * @param occupied 
   * @return 
  */
  // bool IsEffected(const Color color, const Square target,
  //                const Bitboard& occupied) {
  //  return attacker(color, target, occupied);
  //}
#endif  // LONG_EFFECT_LIBRARY

  //! targetに対してcolor側の長い利きを遮っている駒(先後を区別しない)を返す
  /*! pinnerは利きを遮っている駒を取り除いたときにtargetに利きが
      発生する長い利きの駒
      故に2枚以上の駒が間にある場合は含まない
      pinnerは出力値
      targetにある駒は~color側の王
      slider_blockers */
  Bitboard blocker(const Color color, const Square target,
                   Bitboard& pinner) const;

  //! 現局面で王手がかかっているか
  /*! in_check */
  bool checked() const { return checker(); }

  //! excludedの位置の駒を除外してpinされている駒を返す
  /*! excludedの位置の駒は利きの長い駒
      短い利きの1手詰めの判定に利用
      pinned_pieces */
  Bitboard pinned(const Color color, const Square excluded) const;
  //! sourceからtargetに駒が移動したとしてpinされている駒を返す
  /*! 短い利きの1手詰めの判定に利用
      pinned_pieces */
  Bitboard pinned(const Color color, const Square source,
                  const Square target) const;

  /**
   * @brief 手番側の駒をsourceからtargetに移動させると素抜きに遭うか
   * 
   * double checkとdiscovered checkが両方とも短縮表現がdcになるので、
   * discoveredの代わりにrevealedを使う
   * @param source 
   * @param target 
   * @param ou_square 
   * @param pinned 
   * @return 
   * discovered
  */
  bool revealed(const Square source, const Square target,
                const Square ou_square, const Bitboard& pinned) const {
    // 1. pinされている駒が存在する
    // 2. 移動させた駒がそのpinされた駒
    // 3. pinされた駒を相手の駒の利きから外れる方向に移動した
    // -> 全てを満たすなら素抜き
    return pinned && (pinned & source) && !IsAligned(source, target, ou_square);
  }

  /**
   * @brief 現局面で指し手がないかどうか
   * 
   * 指し手がないならtrue
   * 指し手生成ルーチンを用いるので速くないので、探索中に使ってはいけない
   * @return 
   * is_mated
  */
  bool IsMated() const;

 private:
  //! targetの位置にある敵玉に関してピンを発生させているかもしれない
  //! color側の利きの長い駒を返す
  /*! 敵玉は~color */
  Bitboard GetPinnerCandidate(const Color color, const Square target) const;

  //! ピンを発生させているかもしれない候補pinnerから本当にピンされている駒を計算する
  /*! ピンされているのはcolor側の駒
      targetの位置にcolor側の王がある
      occupiedは駒が移動した後のoccupied bitboard */
  Bitboard GetPinned(const Color color, const Square target, Bitboard& pinner,
                     const Bitboard& occupied) const;

  //! color側の持ち駒piece_raw_typeの最後の1枚のBonaPieceを返す
  /*! bona_piece_of */
  evaluation::BonaPiece GetBonaPiece(const Color color,
                                     const Piece piece_raw_type) const {
    const int n = location_.hand(color).count(piece_raw_type);
    ASSERT_LV3(n > 0);
    return static_cast<evaluation::BonaPiece>(
        evaluation::GetSelfHandIndex(color, piece_raw_type) + n - 1);
  }

  //! color側の手駒piece_raw_typeの最後の1枚のPieceNumberを返す
  /*! piece_no_of */
  PieceNumber GetPieceNumber(const Color color,
                             const Piece piece_raw_type) const {
    return evaluation_list_.GetPieceNumberHand(
        GetBonaPiece(color, piece_raw_type));
  }

  //! squareの升にある駒のPieceNumberを返す
  /*! piece_no_of */
  PieceNumber GetPieceNumber(const Square square) const {
    ASSERT_LV3(location_[square] != Piece::EMPTY);
    const PieceNumber n = evaluation_list_.GetPieceNumberBoard(square);
    ASSERT_LV3(n.ok());
    return n;
  }

 public:
  //
  // ACTION
  //

  //! 現局面で与えられた指し手でこれから移動する駒を返す
  /*! 駒は先後の情報を含む
      後手の駒打ちは後手の駒が返る
      moved_piece_before */
  Piece moving_piece(const Move move) const {
    ASSERT_LV3(move.ok());
#if defined(KEEP_PIECE_IN_GENERATED_MOVE)
    return GetMovingPiece(move.value);
#else
    //! movingとdroppedでタイミングが一致していない。何とかしたい
    return move.drop() ? Piece(side_, move.dropped_piece()) :
                       operator[](move.source());
#endif  // defined(KEEP_PIECE_IN_GENERATED_MOVE)
  }

  //! 現局面で与えられた指し手で移動した後の駒を返す
  /*! USE_DROPBIT_IN_STATSが定義されているときは、
      駒打ちにDrop(==32)を加算して駒の種類を返す
      移動後なので、成りの場合は成った駒
      KEEP_PIECE_IN_GENERATED_MOVESが定義されているときは単にmoveの上位16ビットを返す
      moved_piece_after */
  Piece moved_piece(const Move move) const {
    /* move pickerからmove::noneに対して呼び出されることがある
       そのため、assertは設定できない
       move::noneに対しては[piece::none, piece::size)の範囲の値が返ればよい */
#if defined(KEEP_PIECE_IN_GENERATED_MOVE)
    // そのまま上位16bit
    return Piece(move.value >> 16);
#else
    return GenerateMovedPiece(move);
#endif  // defined(KEEP_PIECE_IN_GENERATED_MOVE)
  }

  //! 置換表から取り出したMoveを32bit化
  /*! move16_to_move */
  Move ConvertMove16ToMove(const Move move) const {
    /* 置換表から取り出した値なので m==0である可能性がある
       その場合、SQ_ZEROの駒が上位16ビットに登録されるが
       指し手はillegalなのでこの指し手が選ばれることはない */
    const uint32_t upper = GenerateMovedPiece(move);
    return Move(move, upper);
  }

 private:
  //! 移動後の駒種を計算
  /*! Moveの上位16bitの情報が使えない場合に
      現局面におけるMoveの下位16bitの情報から移動後を求める */
  Piece GenerateMovedPiece(const Move move) const {
    return move.drop()
               ? Piece((Piece(side_, move.dropped_piece()).value + Piece::DROP))
               : move.promotion()
                     ? Piece(location_[move.source()].value + Piece::PROMOTION)
                     : Piece(location_[move.source()]);
  }

 public:
  //! 指し手が合法か確認する
  /*! 指し手を生成した際に判定されていない項目について確認する
      王手がかかっている局面ではEVASIONで差し手の生成が行われているので、
      王手のかかっていない局面のみを扱う

      判定内容は、
      1. 盤上の駒を移動させたときに素抜きに遭わないか、
      2. 敵の利きがあるところへの王の移動でないか
      ※ 連続王手の千日手は探索の領分なのでここでは扱わない
      ※ 置換表から得た指し手はIsPseudoLegalで予め確認する必要がある
      legal */
  bool IsLegal(const Move move) const;

  //! 指し手が疑似合法であるか確認する
  /*! 自殺手は許容するがそれ以外は合法であるかどうか
      置換表から取り出した指し手はここで予め確認したうえで、IsLegalで確認する
      指し手生成ルーチンのテストに使える
      killer moveがこの局面においても疑似合法であるかを判定する
      pseudo_legal */
  bool IsPseudoLegal(const Move move) const {
    return IsPseudoSublegal<true>(move);
  }

  //! 歩や大駒の不成を違法とする疑似合法の判定
  /*! All==false なら歩や大駒の不成を疑似合法に含まない
      All==true なら歩や大駒の不成も疑似合法に含む */
  template <bool All>
  bool IsPseudoSublegal(const Move move) const;

  //! targetの場所に歩を打てるかどうか
  /*! 打ち歩詰めにならないならtrue
      前提として二歩でないことと歩の正面に敵玉がいる必要がある
      legal_drop */
  bool IsLegalFuDrop(const Square target) const;

  //! targetの場所に歩を打った時に二歩でなく打ち歩詰めでないかどうか
  /*! 二歩でなく打ち歩詰めでないならtrue
      IsLegalDropに二歩の判定を追加したバージョン
      legal_pawn_drop */
  bool IsLegalFuDropEx(const Square target) const;

#ifdef USE_SEE
  /**
   * @brief 指し手moveのSEEにおいてthreshold以上の評価になるか
   *        SEE: static exchange valuation
   *
   * 最初に駒を動かす手番側から見て価値がthresholdを超えるかどうか
   * @param move
   * @param threshold
   * @return
   * see_ge
   */
  bool IsSeeGe(const Move move, const evaluation::Value threshold =
                                    evaluation::Value::ZERO) const;
#endif  // USE_SEE

  //! 王手をかける指し手かどうか
  /*! 前提条件として指し手は疑似合法手の必要がある
      gives_check */
  bool IsCheckMove(const Move move) const;

  //! 駒を捕る指し手か成る指し手
  /*! capture_or_promotion */
  bool IsCaptureOrPromotion(const Move move) const {
    return move.promotion() || IsCapture(move);
  }

  //! 歩が成る指し手かどうか
  /*! pawn_promotion */
  bool IsFuPromotion(const Move move) const {
#ifdef KEEP_PIECE_IN_GENERATED_MOVE
    // 上位16bitに移動する駒が入っている
    return move.promotion() && moved_piece(move).raw_type() == Piece::FU;
#else
    return move.promotion() && operator[](move.source()).type() == Piece::FU;
#endif  // KEEP_PIECE_IN_GENERATED_MOVE
  }

  //! 捕る手か歩が成る手
  /*! capture_or_pawn_promotion */
  bool IsCaptureOrFuPromotion(const Move move) const {
    return IsFuPromotion(move) || IsCapture(move);
  }

  //! 捕る手か価値のある成りの手
  /*! 価値のある成りは歩、角、飛の成りとする
      capture_or_valuable_promotion */
  bool IsCaptureOrValuablePromotion(const Move move) const {
#ifdef KEEP_PIECE_IN_GENERATED_MOVE
    const auto p = moved_piece(move).raw_type();
#else
    const auto p = operator[](move.source()).type();
#endif  // KEEP_PIECE_IN_GENERATED_MOVE
    return (move.promotion() &&
            (p == Piece::FU || p == Piece::KA || p == Piece::HI)) ||
           IsCapture(move);
  }

  //! 捕る手かどうか
  /*! capture */
  bool IsCapture(const Move move) const {
    return !move.drop() && location_[move.target()] != Piece::EMPTY;
  }

#ifdef USE_MATE_1PLY
  //! 1手詰めの指し手を生成する
  /*! 詰みが見つからなければNoneを返す
      判定は完全ではなく漏れがある
      戻り値はMove16
      mate1ply */
  Move Mate1ply() const;

 private:
  //! Mate1plyの先後で別のバージョン
  /*! 内部実装
      mate1ply_impl */
  template <Color::Value Us>
  Move Mate1plyImpl() const;

 public:
  //! 利きのある場所への近接王手からのn手詰め
  /*! ply=1, 3, 5, ...
      weak_mate_n_ply */
  Move MateNplyWeak(const int ply) const;
#endif  // USE_MATE_1PLY

  //! 入玉での勝ちを宣言する
  /*! 入玉のルールに基づいて宣言を行う
      トライルールでの王の移動も含む
      勝ちの条件を満たしていないならばNONEが返る
      Mate1plyから利用する
      トライルールの場合の戻り値はMove16 */
  Move Declarate() const;

  //
  // UPDATE
  //

  //! 指し手で盤面を一つ進める
  /*! 指し手に非合法手が含まれてはならない
      new_rbはこの関数の呼び出し元の責任で確保する必要がある
      is_checkはこの指し手で王手になるかどうか
      予めrb.check_info.Update(position)を呼び出している必要がある
      do_move */
  void StepForward(const Move move, Rollback& new_rb, const bool is_check);
  /*! 指し手で王手になるかわからない場合
      do_move */
  void StepForward(const Move move, Rollback& new_rb) {
    StepForward(move, new_rb, IsCheckMove(move));
  }

  //! 1手局面を戻す
  void StepBackward(const Move move);

  //! null moveで局面を進める
  void StepForwardNull(Rollback& new_rb);
  //! null moveで局面を戻す
  void StepBackwardNull();

 private:
  /*! do_move_impl */
  template <Color::Value Us>
  void StepForwardImpl(const Move move, Rollback& new_rb, const bool is_check);
  /*! undo_move_inpl */
  template <Color::Value Us>
  void StepBackwardImpl(const Move move);

  /**
   * @brief Rollbackの初期化
   *
   * 初期化の時に内部的に利用される
   * TODO: あえて引数としてメンバ変数を与えた方が最適化されやすいのか要確認
   * @param rb
   * set_state
   */
  void InitializeState(Rollback* rb) const;

  /**
   * @brief 王手になるBitboardなどを更新する
   *
   * null moveの場合は利きの更新を省略できるのでフラグを設定する
   * @param rb
   * set_check_info
   */
  template <bool NullMove>
  void UpdateCheckState(Rollback* rb) const;

 public:
  //! 現局面に対応するRollback
  /*! 例えばstate()->captured_pieceには
      直前の指し手で捕獲された駒が格納されている
      state */
  Rollback* state() const { return rb_; }

  //! 評価関数で使う駒番号と位置の対応関係を取得
  /*! eval_list */
  const evaluation::EvaluationList* evaluation_list() const {
    return &evaluation_list_;
  }

 private:
  PieceLocation location_;

  Color side_;

 public:
  //! "lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1"
  static const std::string sfen_hirate;

 private:
   /**
    * @brief 駒に対応するUSIのアルファベットの配列
    * 
    * 主に持ち駒の出力に使う 
    * PieceToCharBW
   */
   static constexpr const char* piece_to_char_bw = " PLNSBRGK        plnsbrgk";

  //! 初期局面からの手数
  /*! 初期局面の手数は1
      gamePly */
  int game_ply_;

  //! このインスタンスに紐づいているスレッド
  /*! thisThread */
  search::Thread* ptr_thread_;

  /**
   * @brief 現局面に対応するデータ
   * 
   * do_moveで進むときは次の局面のRollbackへの参照をdo_moveの引数として渡す
   * このとき、undo_moveで戻れるようにRollback::previousに前のrbを設定する
   * undo_moveで局面を巻き戻すときはRollback::previousで遡る
   * StateInfo* st
  */
  Rollback* rb_;

  //! 評価関数に用いる駒のリスト
  /*! evalList */
  evaluation::EvaluationList evaluation_list_;
};

/**
 * @brief デバッグ用に出力
 * @param os 
 * @param position 
 * @return 
*/
std::ostream& operator<<(std::ostream& os, const Position& position);
}  // namespace state_action
#endif  // !POSITION_H_INCLUDED
