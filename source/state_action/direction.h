#pragma once
#ifndef DIRECTION_H_INCLUDED
#define DIRECTION_H_INCLUDED

#include <array>
#include <boost/operators.hpp>
#include <cstdint>

#include "../rule/color.h"
#include "../rule/square.h"
#include "../utility/bit_operator.h"

namespace state_action {
namespace effect8 {
namespace detail {
/*! DirectToDeltaWW_ */
constexpr std::array<SquareWithWall, 8> direction_to_delta_ww_table = {
    SquareWithWall::RU, SquareWithWall::R, SquareWithWall::RD,
    SquareWithWall::U,  SquareWithWall::D, SquareWithWall::LU,
    SquareWithWall::L,  SquareWithWall::LD};
}  // namespace detail

//! DirectionSetをpopしたもの
/*! 複数の方角を同時に表すことはない
 *  おまけで桂馬の移動も追加
    Direct */
struct Direction {
  enum Value {
    // clang-format off
    RU, R, RD, U, D, LU, L, LD,
    SIZE,
    ZERO = 0,

    RUU = 8, LUU, RDD, LDD,
    SIZE_PLUS4
    // clang-format on
  };
#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Direction(const Value d) : value(d) {}
#pragma warning(pop)
  explicit constexpr Direction(const int d) : value(static_cast<Value>(d)) {}

  //! 180度反転させた方角
  /*! 桂馬の方向には対応していない */
  constexpr Direction operator~() const {
    static_assert(R == 1 && L == 6, "");
    return Direction(7 - value);
  }

  constexpr Direction& operator++() {
    value = static_cast<Value>(value + 1);
    return *this;
  }
  constexpr Direction operator++(int) {
    const Direction tmp(value);
    value = static_cast<Value>(value + 1);
    return tmp;
  }
  constexpr Direction& operator--() {
    value = static_cast<Value>(value - 1);
    return *this;
  }
  constexpr Direction operator--(int) {
    const Direction tmp(value);
    value = static_cast<Value>(value - 1);
    return tmp;
  }

  constexpr operator std::underlying_type_t<Value>() const { return value; }
  /*! DirectToDeltaWW */
  constexpr operator SquareWithWall() const {
    return detail::direction_to_delta_ww_table[value];
  }

  constexpr bool ok() const { return value >= ZERO && value < SIZE_PLUS4; }

  Value value;
};

//! 方角を表す。遠方駒の利きや、玉から見た方角を表すのに用いる。
/* bit0..右上、bit1..右、bit2..右下、bit3..上、
 * bit4..下、bit5..左上、bit6..左、bit7..左下
 * 同時に複数のbitが1であることがありうる。
   Directions */
struct DirectionSet : private boost::operators<DirectionSet> {
  enum Value {
    ZERO = 0,
    RU = 1,
    R = 2,
    RD = 4,
    U = 8,
    D = 16,
    LU = 32,
    L = 64,
    LD = 128,

    HORIZONTAL = R | L,
    VERTICAL = U | D,

    SLASH = RU | LD,
    // 英語版wikiによるとBackslashのことをsloshとも呼ぶらしい
    SLOSH = RD | LU,

    CROSS = HORIZONTAL | VERTICAL,
    DIAGONAL = SLASH | SLOSH,

    ALL = CROSS | DIAGONAL
  };

  DirectionSet() = default;
#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr DirectionSet(const Value d) : value(d) {}
#pragma warning(pop)
  constexpr DirectionSet(const Direction d)
      : value(static_cast<Value>(1 << d.value)) {}

  //! Directionを一つずつ取り出す
  Direction Pop() { return Direction(pop_lsb(value)); }

  /**
   * @brief 逆方向を計算する
   *
   * 指している方向は一つだけの必要がある
   * @return
   */
  constexpr DirectionSet reverse() const noexcept {
    // NOTE: 割り算のコストは高いような気がするが、大丈夫だろうか
    return static_cast<Value>(128 / value);
  }

  constexpr bool operator==(const DirectionSet rhs) const noexcept {
    return value == rhs.value;
  }
  constexpr bool operator==(const Value rhs) const noexcept {
    return value == rhs;
  }
  constexpr bool operator!=(const Value rhs) const noexcept {
    return value != rhs;
  }

  constexpr operator bool() const { return value != ZERO; }

  constexpr DirectionSet& operator|=(const DirectionSet obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) | obj.value);
    return *this;
  }
  constexpr DirectionSet& operator&=(const DirectionSet obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) & obj.value);
    return *this;
  }
  constexpr DirectionSet& operator^=(const DirectionSet obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) ^ obj.value);
    return *this;
  }

  constexpr DirectionSet& operator|=(const Value obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) | obj);
    return *this;
  }
  constexpr DirectionSet& operator&=(const Value obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) & obj);
    return *this;
  }
  constexpr DirectionSet& operator^=(const Value obj) noexcept {
    value = static_cast<Value>(enum_cast<>(value) ^ obj);
    return *this;
  }

  Value value;
};

constexpr DirectionSet operator&(const DirectionSet x,
                                 const DirectionSet::Value y) noexcept {
  return static_cast<DirectionSet::Value>(enum_cast<>(x.value) & y);
}
constexpr DirectionSet operator&(const DirectionSet::Value x,
                                 const DirectionSet y) noexcept {
  return static_cast<DirectionSet::Value>(enum_cast<>(x) & y.value);
}

constexpr DirectionSet::Value operator|(const DirectionSet::Value x,
                                        const DirectionSet::Value y) noexcept {
  return static_cast<DirectionSet::Value>(enum_cast<>(x) | y);
}

constexpr DirectionSet::Value operator&(const DirectionSet::Value x,
                                        const DirectionSet::Value y) noexcept {
  return static_cast<DirectionSet::Value>(enum_cast<>(x) & y);
}

namespace detail {
/**
 * @brief sq1にとってsq2がどのdirectionにあるか
 * 
 * direc_table
*/
extern DirectionSet direction_table[Square::SIZE_PLUS1][Square::SIZE_PLUS1];

//! direction_tableの初期化
void InitializeDirectionTable();
}  // namespace detail

/**
 * @brief sq1にとってsq2がどのdirectionにあるか
 *        DirectionSetが返り値の方が縦横十字方向や、斜め方向の位置関係にある場合
 *        DIRECTIONSET::CROSSやDIRECTIONSET_DIAGONALのような定数が使えて便利
 * @param sq1 
 * @param sq2 
 * @return 
 * directions_of
*/
inline DirectionSet GetDirection(Square sq1, Square sq2) {
  return detail::direction_table[sq1][sq2];
}
}  // namespace effect8

//! 与えられた3点が縦横斜めの一直線にあるか
/*! 駒を移動させたときに開き王手になるかどうかを判定するのに利用
 *  s3は玉で、s1とs2は同じ側にいるものとする
 *  (玉を挟んでの一直線は一直線とはみなさない) */
inline bool IsAligned(const Square s1, const Square s2, const Square s3) {
  const auto d1 = effect8::GetDirection(s1, s3);
  return d1 ? d1 == effect8::GetDirection(s2, s3) : false;
}
}  // namespace state_action
#endif  // !DIRECTION_H_INCLUDED
