#pragma once
#ifndef MOVE_GENERATOR_H_INCLUDED
#define MOVE_GENERATOR_H_INCLUDED

#include <array>
#include <algorithm>

#include "move.h"
#include "move_generator/generator_type.h"
#include "extended_move.h"

namespace state_action {
using MoveGeneratorType = move_generator::MoveGeneratorType;
template <MoveGeneratorType T>
using MoveGeneratorAll = move_generator::MoveGeneratorAll<T>;

class Position;

template <MoveGeneratorType Type>
void GenerateMoves(const Position& position, ExtendedMoveArray& move_list);
template <MoveGeneratorType Type>
void GenerateMoves(const Position& position, ExtendedMoveArray& move_list,
                   const Square recapture_square);

template <MoveGeneratorType Type>
struct MoveList {
  explicit MoveList(const Position& position) {
    GenerateMoves<Type>(position, move_list_);
  }

  using iterator = ExtendedMoveArray::iterator;
  using const_iterator = ExtendedMoveArray::const_iterator;
  using difference_type = ExtendedMoveArray::difference_type;
  using reference = ExtendedMoveArray::reference;
  using const_reference = ExtendedMoveArray::const_reference;

  iterator begin() { return move_list_.begin(); }
  iterator end() { return move_list_.end(); }
  const_iterator begin() const { return move_list_.begin(); }
  const_iterator end() const { return move_list_.end(); }

  difference_type size() const { return move_list_.size(); }
  bool empty() const noexcept { return move_list_.empty(); }

  reference operator[](const size_t i) { return move_list_[i]; }
  const_reference operator[](const size_t i) const { return move_list_[i]; }

  bool IsIn(const Move move) const {
    return std::find(begin(), end(), move) != end();
  }

 private:
  ExtendedMoveArray move_list_;
};

using LegalMoveList = MoveList<MoveGeneratorType::LEGAL>;
}  // namespace state_action
#endif  // !MOVE_GENERATOR_H_INCLUDED
