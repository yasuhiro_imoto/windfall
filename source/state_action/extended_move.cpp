#include "stdafx.h"

#include "extended_move.h"

#include "move.h"
#include "position.h"

namespace state_action {
void ExtendedMoveArray::RemoveIllegalMoves(const Position& position) {
  end_ = std::remove_if(
      begin(), end(), [&](const Move move) { return !position.IsLegal(move); });
}

void ExtendedMoveArray::RemovePsuedoIllegalMoves(const Position& position) {
  end_ = std::remove_if(begin(), end(), [&](const Move move) {
    return !position.IsPseudoLegal(move);
  });
}
}  // namespace state_action
