#pragma once
#ifndef PIECE_LOCATION_H_INCLUDED
#define PIECE_LOCATION_H_INCLUDED

#include <array>

#include "../evaluation/bonanza_piece.h"
#include "../evaluation/kpp_index.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../utility/enum_cast.h"
#include "../utility/force_inline.h"
#include "bitboard.h"
#include "hand.h"

namespace state_action {
#pragma warning(push)
#pragma warning(disable : 26495)
//! 駒の配置の保持とその値の操作を行う
class PieceLocation {
 public:
  //! 先後それぞれの駒があるところが1
  /*! byColorBB */
  std::array<Bitboard, Color::SIZE> color_bb;
  //! 駒の種類ごとにそれぞれの駒があるところが1
  /*! 先後は区別しない
      Pieceで定義されている特殊な定数を指定できる
      byTypeBB */
  std::array<Bitboard, Piece::BB_SIZE> type_bb;

  //! 駒を配置する
  /*! 呼び出した後は、Updateを呼び出して全体の整合性を保つ必要がある
      また、王を配置した場合は別途ou_squareを更新する必要がある
      eval_listも同様に必要ならば別途更新する必要がある
      put_piece */
  void Add(const Square square, const Piece piece) {
    ASSERT_LV2(board_[square] == Piece::EMPTY);
    board_[square] = piece;
    Xor(piece, square);
  }
  //! 駒を盤から取り除く
  /*! 呼び出した後は、Updateを呼び出して全体の整合性を保つ必要がある
      また、王を配置した場合は別途ou_squareを更新する必要がある
      eval_listも同様に必要ならば別途更新する必要がある
      remove_piece */
  void Remove(const Square square) {
    const Piece piece = board_[square];
    ASSERT_LV3(piece != Piece::EMPTY);
    board_[square] = Piece::EMPTY;
    Xor(piece, square);
  }

  //! squareの位置の駒を取り除く/pcを配置する
  /*! 呼び出した後は、Updateを呼び出して全体の整合性を保つ必要がある
      また、王を配置した場合は別途ou_squareを更新する必要がある
      eval_listも同様に必要ならば別途更新する必要がある
      xor_piece */
  void Xor(const Piece piece, const Square square) {
    // 先手/後手の駒のoccupied bitboardを更新
    color_bb[piece.color()] ^= square;
    // 全部の駒のoccupied bitboardを更新
    type_bb[Piece::ALL_PIECES] ^= square;
    // 駒の種類のoccupied bitboardを更新
    type_bb[piece.type()] ^= square;
    // その他はUpdate()で処理される
  }

  void Add(const Color color, const Piece piece_raw_type, const int c = 1) {
    hand_[color].Add(piece_raw_type, c);
  }
  void Sub(const Color color, const Piece piece_raw_type, const int c = 1) {
    hand_[color].Sub(piece_raw_type, c);
  }

  /**
   * @brief Add, Remove, Xorでの操作に対して内部状態を更新する
   *
   * update_bitboards
   */
  void Update();
  /**
   * @brief ou_squareの更新を行う
   *
   * update_kingSquare
   */
  void UpdateOuSquare();

  /**
   * @brief 初期化のための初期状態に設定
   *
   * Position::Setで利用する
   */
  void ResetOuSquare() {
    ou_square_[enum_cast<>(Color::BLACK)] =
        ou_square_[enum_cast<>(Color::WHITE)] = Square::SIZE;
  }

  /**
   * @brief 0で初期化
   *
   * Position::Setで利用する
   */
  void ResetHand() {
    hand_[enum_cast<>(Color::BLACK)].hand =
        hand_[enum_cast<>(Color::WHITE)].hand = 0;
  }

  template <Color::Value Us>
  void UpdateOuSquare(const Square square) {
    ou_square_[Us] = square;
  }

  //! color側の持ち駒piece_typeの最後の1枚のBonaPieceを返す
  /*! bona_piece_of */
  evaluation::BonaPiece GetBonaPiece(const Color color,
                                     const Piece piece_type) const {
    const int count = hand_[color][piece_type.raw_type()];
    ASSERT_LV3(count > 0);
    return static_cast<evaluation::BonaPiece>(
        evaluation::kpp_hand_index[color][piece_type.raw_type()].fb + count -
        1);
  }

  //! 指定された升の駒を返す
  /*! piece_on */
  Piece operator[](const Square square) const {
    ASSERT_LV3(square < Square::SIZE_PLUS1);
    return board_[square];
  }
  /*! hand_of */
  Hand hand(const Color color) const {
    ASSERT_LV3(color.ok());
    return hand_[color];
  }

  /*! king_square */
  FORCE_INLINE Square ou_square(const Color color) const {
    ASSERT_LV3(color.ok());
    return ou_square_[color];
  }

  const Bitboard& operator[](const Piece piece) const {
    ASSERT_LV3(piece < Piece::BB_SIZE);
    return type_bb[piece];
  }
  const Bitboard& operator[](const Color color) const {
    ASSERT_LV3(color.ok());
    return color_bb[color];
  }

  Piece get(const Square square) const {
    ASSERT_LV3(square < Square::SIZE_PLUS1);
    return board_[square];
  }

  //! Colorに依存しない駒の配置を返す
  /*! 駒種を省略した場合は全ての種類の駒になる
      NOTE: 駒種は定数でしか与えられないようなので、テンプレート引数にした */
  template <Piece::Value P = Piece::ALL_PIECES>
  const Bitboard& get() const {
    static_assert(P < Piece::BB_SIZE, "");
    return type_bb[P];
  }
  template <Piece::Value P1, Piece::Value P2>
  Bitboard get() const {
    return get<P1>() | get<P2>();
  }
  template <Piece::Value P1, Piece::Value P2, Piece::Value P3>
  Bitboard get() const {
    return get<P1>() | get<P2>() | get<P3>();
  }
  template <Piece::Value P1, Piece::Value P2, Piece::Value P3, Piece::Value P4>
  Bitboard get() const {
    return get<P1>() | get<P2>() | get<P3>() | get<P4>();
  }
  template <Piece::Value P1, Piece::Value P2, Piece::Value P3, Piece::Value P4,
            Piece::Value P5>
  Bitboard get() const {
    return get<P1>() | get<P2>() | get<P3>() | get<P4>() | get<P5>();
  }

  // コンパイラのバグでオーバロードを解決できないのでその対策
  // NOTE: バグが解消したら名前をgetにしたい
  template <Color::Value C>
  const Bitboard& color() const {
    static_assert(Color(C).ok(), "");
    return color_bb[C];
  }
  template <Color::Value C, Piece::Value P>
  Bitboard color() const {
    return color<C>() & get<P>();
  }

  const Bitboard& get(const Color color) const {
    ASSERT_LV3(color.ok());
    return color_bb[color];
  }
  template <Piece::Value P>
  Bitboard get(const Color color) const {
    return get(color) & get<P>();
  }
  template <Piece::Value P1, Piece::Value P2>
  Bitboard get(const Color color) const {
    return get(color) & get<P1, P2>();
  }

  bool ok() const;

  //! ある駒の存在する升を返す
  /*! Pt==KINGしか渡せない。Stockfishとの互換用 */
  template <Piece::Value Pt>
  Square square(const Color color) const {
    static_assert(Pt == Piece::OU, "Pt must be a KING in Position::square().");
    ASSERT_LV3(color.ok());
    return ou_square(color);
  }

 private:
  //! 盤面
  /*! 王がいない場合に対応するために一つ大きくしてある
      board */
  std::array<Piece, Square::SIZE_PLUS1> board_;
  //! 持ち駒
  /*! hand */
  std::array<Hand, Color::SIZE> hand_;
  //! 王の位置
  /*! kingSquare */
  std::array<Square, Color::SIZE> ou_square_;
};
#pragma warning(pop)

//! デバッグ用出力
std::ostream& operator<<(std::ostream& os, const PieceLocation& board);
}  // namespace state_action
#endif  // !PIECE_LOCATION_H_INCLUDED
