#include "stdafx.h"

#include "bitboard.h"

#include "direction.h"
#include "effect.h"

namespace state_action {
const Bitboard Bitboard::All = Bitboard(0x7FFFFFFFFFFFFFFFull, 0x3FFFFull);
const Bitboard Bitboard::Zero = Bitboard(0, 0);

namespace detail {
constexpr uint64_t p0_rank = 0x40201008040201ull, p1_rank = 0x201ull;

/**
 * @brief 敵陣を表現するBitboard
 * EnemyField
*/
const std::array<Bitboard, Color::SIZE> enemy_field_table = {
    Bitboard(p0_rank * 0x7, p1_rank * 0x7),
    Bitboard(p0_rank * 0x1C0, p1_rank * 0x1C0) };

}

const Bitboard FILE1_BB(0x1FFull << (9 * 0), 0),
    FILE2_BB(0x1FFull << (9 * 1), 0), FILE3_BB(0x1FFull << (9 * 2), 0),
    FILE4_BB(0x1FFull << (9 * 3), 0), FILE5_BB(0x1FFull << (9 * 4), 0),
    FILE6_BB(0x1FFull << (9 * 5), 0), FILE7_BB(0x1FFull << (9 * 6), 0),
    FILE8_BB(0, 0x1FFull << (9 * 0)), FILE9_BB(0, 0x1FFull << (9 * 1));
const Bitboard RANK1_BB(detail::p0_rank << 0, detail::p1_rank << 0),
    RANK2_BB(detail::p0_rank << 1, detail::p1_rank << 1),
    RANK3_BB(detail::p0_rank << 2, detail::p1_rank << 2),
    RANK4_BB(detail::p0_rank << 3, detail::p1_rank << 3),
    RANK5_BB(detail::p0_rank << 4, detail::p1_rank << 4),
    RANK6_BB(detail::p0_rank << 5, detail::p1_rank << 5),
    RANK7_BB(detail::p0_rank << 6, detail::p1_rank << 6),
    RANK8_BB(detail::p0_rank << 7, detail::p1_rank << 7),
    RANK9_BB(detail::p0_rank << 8, detail::p1_rank << 8);
const std::array<Bitboard, File::SIZE> FILE_BB = {FILE1_BB, FILE2_BB, FILE3_BB,
                                                  FILE4_BB, FILE5_BB, FILE6_BB,
                                                  FILE7_BB, FILE8_BB, FILE9_BB};
const std::array<Bitboard, Rank::SIZE> RANK_BB = {RANK1_BB, RANK2_BB, RANK3_BB,
                                                  RANK4_BB, RANK5_BB, RANK6_BB,
                                                  RANK7_BB, RANK8_BB, RANK9_BB};

const Bitboard FORWARD_RANK_BB[Color::SIZE][Rank::SIZE] = {
    {Bitboard(detail::p0_rank * 0x00, detail::p1_rank * 0x00),
     Bitboard(detail::p0_rank * 0x01, detail::p1_rank * 0x01),
     Bitboard(detail::p0_rank * 0x03, detail::p1_rank * 0x03),
     Bitboard(detail::p0_rank * 0x07, detail::p1_rank * 0x07),
     Bitboard(detail::p0_rank * 0x0F, detail::p1_rank * 0x0F),
     Bitboard(detail::p0_rank * 0x1F, detail::p1_rank * 0x1F),
     Bitboard(detail::p0_rank * 0x3F, detail::p1_rank * 0x3F),
     Bitboard(detail::p0_rank * 0x7F, detail::p1_rank * 0x7F),
     Bitboard(detail::p0_rank * 0xFF, detail::p1_rank * 0xFF)},
    {Bitboard(detail::p0_rank * 0x1FE, detail::p1_rank * 0x1FE),
     Bitboard(detail::p0_rank * 0x1FC, detail::p1_rank * 0x1FC),
     Bitboard(detail::p0_rank * 0x1F8, detail::p1_rank * 0x1F8),
     Bitboard(detail::p0_rank * 0x1F0, detail::p1_rank * 0x1F0),
     Bitboard(detail::p0_rank * 0x1E0, detail::p1_rank * 0x1E0),
     Bitboard(detail::p0_rank * 0x1C0, detail::p1_rank * 0x1C0),
     Bitboard(detail::p0_rank * 0x180, detail::p1_rank * 0x180),
     Bitboard(detail::p0_rank * 0x100, detail::p1_rank * 0x100),
     Bitboard(detail::p0_rank * 0x000, detail::p1_rank * 0x000)}};

__m128i MakeSquareBB(const Square s) {
  return _mm_set_epi64x(s >= Square::_81 ? (1ull << (s - 63)) : 0,
                        s >= Square::_81 ? 0 : (1ull << s));
}

Bitboard::Bitboard(const Square s) { _mm_store_si128(&m, MakeSquareBB(s)); }

Bitboard& Bitboard::operator|=(const Square s) {
  _mm_store_si128(&m, _mm_or_si128(m, MakeSquareBB(s)));
  return *this;
}
Bitboard& Bitboard::operator&=(const Square s) {
  _mm_store_si128(&m, _mm_and_si128(m, MakeSquareBB(s)));
  return *this;
}
Bitboard& Bitboard::operator^=(const Square s) {
  _mm_store_si128(&m, _mm_xor_si128(m, MakeSquareBB(s)));
  return *this;
}

Bitboard operator|(const Bitboard& bb, const Square s) {
  return bb | MakeSquareBB(s);
}

Bitboard operator&(const Bitboard& bb, const Square s) {
  return bb & MakeSquareBB(s);
}

Bitboard operator^(const Bitboard& bb, const Square s) {
  return bb ^ MakeSquareBB(s);
}

bool Bitboard::Test(const Square s) const {
  return !_mm_testz_si128(m, MakeSquareBB(s));
}

Bitboard operator~(const Bitboard& bb) { return bb ^ Bitboard::All; }

Bitboard begin(const Bitboard& bb) { return bb; }
Bitboard end(const Bitboard&) { return Bitboard::Zero; }

//! 歩が打てる筋を得るためのBitboard mask
/*! 1～7筋にそれぞれ歩があるかを7桁の2進数で表現して、配列のインデックスとする
    p[0]には1～7筋 、p[1]には8,9筋のときのデータが入っている
    PAWN_DROP_MASK_BB */
std::array<Bitboard, 0x80> fu_drop_mask_bb_table;

namespace detail {
//! 2升に挟まれている升を返すためのテーブル(その2升は含まない)
/*! この配列には直接アクセスせずにbetween_bb()を使うこと。
 *  配列サイズが大きくてcache汚染がひどいのでシュリンクしてある。*/
std::array<Bitboard, 785> between_bb_table;
uint16_t between_index_table[Square::SIZE_PLUS1][Square::SIZE_PLUS1];

//! 2升を通過する直線を返すためのテーブル
/*!
 * 2つ目のindexは[0]:右上から左下、[1]:横方向、[2]:左上から右下、[3]:縦方向の直線
 */
Bitboard line_bb_table[Square::SIZE][4];

std::array<Bitboard, Square::SIZE_PLUS1> ou_effect_bb;
Bitboard ki_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
Bitboard gi_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
Bitboard ke_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
Bitboard fu_effect_bb[Square::SIZE_PLUS1][Color::SIZE];

//! 盤上の駒をないものとして扱う
Bitboard ky_naive_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
//! 盤上の駒をないものとして扱う
std::array<Bitboard, Square::SIZE_PLUS1> ka_naive_effect_bb;
std::array<Bitboard, Square::SIZE_PLUS1> hi_naive_effect_bb;

//! 角の利き
Bitboard ka_effect_bb[2][1856 + 1];
Bitboard ka_effect_mask_bb[2][Square::SIZE_PLUS1];
int ka_effect_index_table[2][Square::SIZE_PLUS1];

//! 飛車の縦の利き
/*! 指定した升sqの属するfileのbitをshiftし、index を求める */
uint64_t hi_file_effect_table[Rank::SIZE + 1][128];
Bitboard hi_rank_effect_table[File::SIZE + 1][128];

//! squareにいる~c側の玉に王手となるc側の駒pieceの候補
/*! 第2添字は(pr-1)を渡して使う
    CheckCandidateBB */
Bitboard check_candidate_bb_table[Square::SIZE_PLUS1][Piece::OU - 1]
                                 [Color::SIZE];
/*! CheckCandidateKingBB */
std::array<Bitboard, Square::SIZE_PLUS1> check_candidate_ou_bb_table;
}  // namespace detail

//! Bitboardを表示する(USI形式ではない) デバッグ用
std::ostream& operator<<(std::ostream& os, const Bitboard& board) {
  for (Rank rank = Rank::_1; rank <= Rank::_9; ++rank) {
    for (File file = File::_9; file >= File::_1; --file) {
      os << ((board & Square(file, rank)) ? " *" : " .");
    }
    os << std::endl;
  }
  return os;
}

//! sに駒pcを置いたときの利き
/*! pcは先後の区別あり
 *  pc==QUEENの場合は馬+龍の利きを返す
    effects_from */
Bitboard GetEffect(const Piece pc, const Square square,
                   const Bitboard& occupied) {
  switch (pc.value) {
      //
      // 先手
      //
    case Piece::BLACK_FU:
      return GetFuEffect(Color::BLACK, square);
    case Piece::BLACK_KY:
      return GetKyEffect(Color::BLACK, square, occupied);
    case Piece::BLACK_KE:
      return GetKeEffect(Color::BLACK, square);
    case Piece::BLACK_GI:
      return GetGiEffect(Color::BLACK, square);
    case Piece::BLACK_KI:
    case Piece::BLACK_TO:
    case Piece::BLACK_NY:
    case Piece::BLACK_NK:
    case Piece::BLACK_NG:
      return GetKiEffect(Color::BLACK, square);

      //
      // 後手
      //
    case Piece::WHITE_FU:
      return GetFuEffect(Color::WHITE, square);
    case Piece::WHITE_KY:
      return GetKyEffect(Color::WHITE, square, occupied);
    case Piece::WHITE_KE:
      return GetKeEffect(Color::WHITE, square);
    case Piece::WHITE_GI:
      return GetGiEffect(Color::WHITE, square);
    case Piece::WHITE_KI:
    case Piece::WHITE_TO:
    case Piece::WHITE_NY:
    case Piece::WHITE_NK:
    case Piece::WHITE_NG:
      return GetKiEffect(Color::WHITE, square);

      //
      // 先後共通
      //
    case Piece::BLACK_KA:
    case Piece::WHITE_KA:
      return GetKaEffect(square, occupied);
    case Piece::BLACK_HI:
    case Piece::WHITE_HI:
      return GetHiEffect(square, occupied);
    case Piece::BLACK_UM:
    case Piece::WHITE_UM:
      return GetUmEffect(square, occupied);
    case Piece::BLACK_RY:
    case Piece::WHITE_RY:
      return GetRyEffect(square, occupied);
    case Piece::BLACK_OU:
    case Piece::WHITE_OU:
      return GetOuEffect(square);
    case Piece::BLACK_QUEEN:
    case Piece::WHITE_QUEEN:
      return GetUmEffect(square, occupied) | GetRyEffect(square, occupied);

      // 初期化の処理を楽にする
    case Piece::BLACK:
    case Piece::WHITE:
      return Bitboard::Zero;

    default:
      UNREACHABLE;
      return Bitboard::All;
  }
}

void InitializeBitboards() {
  // 二つの升の方向のテーブルを初期化
  effect8::detail::InitializeDirectionTable();

  // 角の利きテーブルの初期化
  detail::InitializeKaEffectTable();
  // 飛車の利きテーブルの初期化
  detail::InitializeHiEffectTable();
  // 王の利きテーブルの初期化
  detail::InitializeOuEffectTable();
  // 香の利きテーブルの初期化
  detail::InitializeKyNaiveEffectTable();
  // その他の利きテーブルの初期化
  detail::InitializeShortEffectTable();

  // 二歩用のテーブルを初期化
  detail::InitializeFuDropMask();

  detail::InitializeBetweenBB();
  detail::InitializeLineBB();

  // 王手の候補のテーブルを初期化
  detail::InitializeCheckCandidateTable();
}

namespace detail {
void InitializeKaEffectTable() {
  // 引数indexを2進数として扱う(桁数はbits桁)
  // bits個が1になっているmaskをindexに応じて1の場所を0に変える
  const auto f_index_to_occupied = [](const int index, const int bits,
                                      const Bitboard& mask) {
    auto m = mask;
    Bitboard r = Bitboard::Zero;
    for (int i = 0; i < bits; ++i) {
      const Square square = m.Pop();
      if (index & (1 << i)) {
        r ^= square;
      }
    }
    return r;
  };

  // 斜めの利きの範囲のBitboardを返す
  // occupied: 駒のある升が1
  // n: 0なら右上から左下、1なら左上から右下
  const auto f_effect = [](const Square square, const Bitboard& occupied,
                           const int n) {
    Bitboard r = Bitboard::Zero;
    // 角の利きのrayと飛車の利きのray
    constexpr SquareWithWall delta_array[2][2] = {
        {SquareWithWall::RU, SquareWithWall::LD},
        {SquareWithWall::RD, SquareWithWall::LU}};
    for (const auto delta : delta_array[n]) {
      // 壁に当たるまでsqを利き方向に伸ばしていく
      for (SquareWithWall s = SquareWithWall(square) + delta; s.ok();
           s += delta) {
        // まだ障害物に当っていないのでここまでは利きが到達している
        r ^= static_cast<Square>(s);
        if (occupied & static_cast<Square>(s)) {
          // 障害物があった
          break;
        }
      }
    }
    return r;
  };

  // 角をsquareに置いたときに利きを得るのに関係する升を返す
  const auto f_ka_effect_mask = [](const Square square, const int n) {
    Bitboard result = Bitboard::Zero;
    // 外周は利きに影響しない
    for (Rank r = Rank::_2; r <= Rank::_8; ++r) {
      for (File f = File::_2; f <= File::_8; ++f) {
        const int dr = static_cast<int>(square.rank() - r);
        const int df = static_cast<int>(square.file() - f);
        if (abs(dr) == abs(df) && ((dr == df) == (n == 0))) {
          result ^= Square(f, r);
        }
      }
    }
    // squareの地点は除く
    result &= ~Bitboard(square);

    return result;
  };

  for (const int n : {0, 1}) {
    int index = 0;
    for (const auto sq : SQUARE) {
      // 1段階目のテーブル
      ka_effect_index_table[n][sq] = index;

      auto& mask = ka_effect_mask_bb[n][sq];
      mask = f_ka_effect_mask(sq, n);

      // p[0]とp[1]が被覆していると正しく計算できないのでNG。
      // Bitboardのレイアウト的に、正しく計算できるかのテスト。
      // 縦型Bitboardであるならp[0]のbit63を余らせるようにしておく必要がある。
      ASSERT_LV3(!(mask.CrossOver()));

      // sqの升用に何bit情報を拾ってくるのか
      const int bits = mask.PopCount();
      // ビット数の分の状態数
      const int num = 1 << bits;
      for (int i = 0; i < num; ++i) {
        Bitboard occupied = f_index_to_occupied(i, bits, mask);
        const auto j = ExtractIndexFromOccupied(occupied & mask, mask);
        ka_effect_bb[n][index + j] = f_effect(sq, occupied, n);
      }
      index += num;
    }

    // 盤外(SQ_NB)に駒を配置したときに利きがZERO_BBとなるときのための処理
    ka_effect_index_table[n][Square::SIZE] = index;
  }
}

void InitializeHiEffectTable() {
  // 縦の利き
  for (Rank rank = Rank::_1; rank <= Rank::_9; ++rank) {
    const Square sq(File::_1, rank);

    constexpr int num1s = 7;
    for (int i = 0; i < (1 << num1s); ++i) {
      // iはsqに駒をおいたときに、その筋の2段～8段目の升がemptyかどうかを表現する値なので
      // 1ビットシフトして、1～9段目の升を表現するようにする。
      int j = i << 1;
      Bitboard bb = Bitboard::Zero;
      for (Rank r = sq.rank() - 1; r >= Rank::_1; --r) {
        bb |= Square(sq.file(), r);
        if (j & (1 << r)) {
          break;
        }
      }
      for (Rank r = sq.rank() + 1; r <= Rank::_9; ++r) {
        bb |= Square(sq.file(), r);
        if (j & (1 << r)) {
          break;
        }
      }

      hi_file_effect_table[rank][i] = bb.p[0];
    }
  }

  // 横の利き
  for (File file = File::_1; file <= File::_9; ++file) {
    const Square sq(file, Rank::_1);

    constexpr int num1s = 7;
    for (int i = 0; i < (1 << num1s); ++i) {
      int j = i << 1;
      Bitboard bb = Bitboard::Zero;
      for (File f = sq.file() - 1; f >= File::_1; --f) {
        bb |= Square(f, sq.rank());
        if (j & (1 << f)) {
          break;
        }
      }
      for (File f = sq.file() + 1; f <= File::_9; ++f) {
        bb |= Square(f, sq.rank());
        if (j & (1 << f)) {
          break;
        }
      }
      hi_rank_effect_table[file][i] = bb;
    }
  }
}

void InitializeOuEffectTable() {
  for (const auto square : SQUARE) {
    // 長さ1の角と飛車の利きを合成
    ou_effect_bb[square] =
        GetKaEffect(square, Bitboard::All) | GetHiEffect(square, Bitboard::All);
  }
}

void InitializeKyNaiveEffectTable() {
  for (const auto color : COLOR) {
    for (const auto square : SQUARE) {
      // 障害物がないときの香の利き
      ky_naive_effect_bb[square][color] =
          GetHiFileEffect(square, Bitboard::Zero) &
          FORWARD_RANK_BB[color][square.rank()];
    }
  }
}

void InitializeShortEffectTable() {
  for (const auto color : COLOR) {
    for (const auto square : SQUARE) {
      // 歩
      // 長さ1の香の利きとして定義
      fu_effect_bb[square][color] = GetKyEffect(color, square, Bitboard::All);

      // 桂
      // 歩の利きの地点に長さ1の角の利きを作って、前方のみ残す
      Bitboard tmp = Bitboard::Zero;
      Bitboard fu1 = GetKyEffect(color, square, Bitboard::All);
      if (fu1) {
        Square sq = fu1.Pop();
        Bitboard fu2 = GetKyEffect(color, sq, Bitboard::All);
        if (fu2) {
          // 2個前の地点が存在している
          tmp = GetKaEffect(sq, Bitboard::All) & RANK_BB[fu2.Pop().rank()];
        }
      }
      ke_effect_bb[square][color] = tmp;

      // 銀
      // 長さ1の角の利きと長さ1の香の利きの合成
      gi_effect_bb[square][color] = GetKyEffect(color, square, Bitboard::All) |
                                    GetKaEffect(square, Bitboard::All);

      // 金
      // 長さ1の角と飛車の利き。ただし、角のほうは相手側の歩の行き先の段でmask
      Bitboard e_fu = GetKyEffect(~color, square, Bitboard::All);
      Bitboard mask = Bitboard::Zero;
      if (e_fu) {
        mask = RANK_BB[e_fu.Pop().rank()];
      }
      ki_effect_bb[square][color] =
          (GetKaEffect(square, Bitboard::All) & ~mask) |
          GetHiEffect(square, Bitboard::All);

      // 障害物がないときの角と飛車の利き
      ka_naive_effect_bb[square] = GetKaEffect(square, Bitboard::Zero);
      hi_naive_effect_bb[square] = GetHiEffect(square, Bitboard::Zero);
    }
  }
}

void InitializeFuDropMask() {
  // 1～7筋
  for (int i = 0; i < 0x80; ++i) {
    Bitboard bb = Bitboard::Zero;
    for (int j = 0; j < 7; ++j) {
      if ((i & (1 << j)) == 0) {
        bb |= FILE_BB[j];
      }
    }
    fu_drop_mask_bb_table[i].p[0] = bb.p[0];
  }

  // 8～9筋
  for (int i = 0; i < 0x4; ++i) {
    Bitboard bb = Bitboard::Zero;
    for (int j = 0; j < 2; ++j) {
      if ((i & (1 << j)) == 0) {
        bb |= FILE_BB[j + 7];
      }
    }
    fu_drop_mask_bb_table[i].p[1] = bb.p[1];
  }
}

void InitializeBetweenBB() {
  // index=0はZERO_BBでおいておく
  // 直線の関係にない場合はすべてindex=0を参照する
  uint16_t index = 1;
  for (const auto sq1 : SQUARE) {
    for (const auto sq2 : SQUARE) {
      if (sq1 >= sq2) {
        // 対称な部分は後で設定
        continue;
      }

      if (effect8::GetDirection(sq1, sq2)) {
        Bitboard bb = Bitboard::Zero;
        const int delta = (sq2 - sq1) / distance(sq1, sq2);
        for (int s = sq1 + delta; s != sq2; s += delta) {
          bb |= Square(s);
        }

        if (!bb) {
          // このsq1とsq2の組み合わせのインデックスは0に設定される
          continue;
        }

        between_index_table[sq1][sq2] = index;
        between_bb_table[index++] = bb;
      }
    }
  }

  ASSERT_LV1(index == 785);

  // 対称な部分を設定
  for (const auto sq1 : SQUARE) {
    for (const auto sq2 : SQUARE) {
      if (sq1 > sq2) {
        between_index_table[sq1][sq2] = between_index_table[sq2][sq1];
      }
    }
  }
}

void InitializeLineBB() {
  constexpr std::array<Square, 4> delta_list = {Square::RU, Square::R,
                                                Square::RD, Square::U};
  for (const auto sq1 : SQUARE) {
    for (int d = 0; d < 4; ++d) {
      const Square delta = delta_list[d];
      Bitboard bb(sq1);
      // 壁に当たるまでsq1から-delta方向に延長
      for (int sq2 = sq1; distance(Square(sq2), Square(sq2 - delta)) <= 1;
           sq2 -= delta) {
        bb |= Square(sq2 - delta);
      }
      // 壁に当たるまでsq1から+delta方向に延長
      for (int sq2 = sq1; distance(Square(sq2), Square(sq2 + delta)) <= 1;
           sq2 += delta) {
        bb |= Square(sq2 + delta);
      }

      line_bb_table[sq1][d] = bb;
    }
  }
}

Bitboard GetCandidate(const Square ou_square,
                      const std::function<const Bitboard(const Square)> f1,
                      const std::function<const Bitboard(const Square)> f2) {
  Bitboard target = Bitboard::Zero;
  Bitboard tmp = f1(ou_square);
  for (const auto sq : tmp) {
    target |= f2(sq);
  }
  return target;
}

void InitializeCheckCandidateTable() {
  using namespace std::placeholders;
  using F1 = const Bitboard& (*)(const Color, const Square);
  using F2 = Bitboard (*)(const Square, const Bitboard&);
  using F_OU = const Bitboard& (*)(const Square);

  for (const auto us : COLOR) {
    const Color them = ~us;

    const auto f_ki_enemy = [them, us](const Square sq) {
      return GetKiEffect(them, sq) & GetEnemyField(us);
    };
    const auto f_ki = std::bind(static_cast<F1>(GetKiEffect), them, _1);

    const auto f_ou_enemy = [us](const Square sq) {
      return GetOuEffect(sq) & GetEnemyField(us);
    };
    const auto f_ou = static_cast<F_OU>(GetOuEffect);

    for (const auto ou_square : SQUARE) {
      Bitboard target;

      auto& table = check_candidate_bb_table[ou_square];

      // ou_squareの位置に自分の駒pで王手できるのは、
      // ou_squareに相手の駒pを置いたときに
      // その利きが及ぶ範囲に自分の駒pがある場合

      // 歩
      // 不成と成りの2種類
      const auto f_fu = std::bind(static_cast<F1>(GetFuEffect), them, _1);
      target = GetCandidate(ou_square, f_fu, f_fu);
      target |= GetCandidate(ou_square, f_ki_enemy, f_fu);
      table[Piece::FU - 1][us] = target & ~Bitboard(ou_square);

      // 香
      // 不成でそのまま王手と成りの場合は王の両隣の筋
      target = GetKyNaiveEffect(them, ou_square);
      if (GetEnemyField(us) & Bitboard(ou_square)) {
        if (ou_square.file() != File::_1) {
          target |= GetKyNaiveEffect(them, Square(ou_square + Square::R));
        }
        if (ou_square.file() != File::_9) {
          target |= GetKyNaiveEffect(them, Square(ou_square + Square::L));
        }
      }
      table[Piece::KY - 1][us] = target;

      // 桂馬
      // 不成と成りの2種類
      const auto f_ke = std::bind(static_cast<F1>(GetKeEffect), them, _1);
      target = GetCandidate(ou_square, f_ke, f_ke);
      target |= GetCandidate(ou_square, f_ki_enemy, f_ke);
      table[Piece::KE - 1][us] = target & ~Bitboard(ou_square);

      // 銀
      // 不成と成りの2種類だが敵陣から外に出て成る場合がある
      const auto f_gi = std::bind(static_cast<F1>(GetGiEffect), them, _1);
      target = GetCandidate(ou_square, f_gi, f_gi);
      target |= GetCandidate(ou_square, f_ki_enemy, f_gi);
      target |= GetCandidate(ou_square, f_ki, f_gi) & GetEnemyField(us);
      table[Piece::GI - 1][us] = target & ~Bitboard(ou_square);

      // 金
      target = GetCandidate(ou_square, f_ki, f_ki);
      table[Piece::KI - 1][us] = target & ~Bitboard(ou_square);

      // 角
      const auto f_ka =
          std::bind(static_cast<F2>(GetKaEffect), _1, Bitboard::Zero);
      target = GetCandidate(ou_square, f_ka, f_ka);
      target |= GetCandidate(ou_square, f_ou_enemy, f_ka);
      target |= GetCandidate(ou_square, f_ou, f_ka) & GetEnemyField(us);
      table[Piece::KA - 1][us] = target & ~Bitboard(ou_square);

      // 馬
      // 飛・龍は無条件全域なので、代わりに馬の結果を格納する
      const auto f_um =
          std::bind(static_cast<F2>(GetUmEffect), _1, Bitboard::Zero);
      target = GetCandidate(ou_square, f_um, f_um);
      table[Piece::HI - 1][us] = target & ~Bitboard(ou_square);

      // 王
      target = GetCandidate(ou_square, f_ou, f_ou);
      check_candidate_ou_bb_table[ou_square] = target & ~Bitboard(ou_square);
    }
  }
}
}  // namespace detail
}  // namespace state_action
