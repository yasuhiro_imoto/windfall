#include "stdafx.h"

#include "position.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../rule/color.h"
#include "../rule/square.h"
#include "../search/prefetch.h"
#include "../search/thread.h"
#include "../search/transposition_table.h"
#include "../utility/level_assert.h"
#include "bitboard.h"
#include "direction.h"
#include "piece_location.h"
#include "zobrist_hash.h"

namespace state_action {
void Position::StepForward(const Move move, Rollback& new_rb,
                           const bool is_check) {
  if (side() == Color::BLACK) {
    StepForwardImpl<Color::BLACK>(move, new_rb, is_check);
  } else {
    StepForwardImpl<Color::WHITE>(move, new_rb, is_check);
  }
}

void Position::StepBackward(const Move move) {
  // 1手前の手番の下で処理する
  if (side() == Color::BLACK) {
    // 1手前はWHITE
    StepBackwardImpl<Color::WHITE>(move);
  } else {
    // 1手前はBLACK
    StepBackwardImpl<Color::BLACK>(move);
  }
}

void Position::StepForwardNull(Rollback& new_rb) {
  ASSERT_LV3(!checker());
  ASSERT_LV3(&new_rb != rb_);

  std::memcpy(&new_rb, rb_, sizeof(Rollback));
  new_rb.previous = rb_;
  rb_ = &new_rb;

  rb_->board_key_ ^= zobrist::side;

  const Key key = rb_->key();
  search::Prefetch(search::tt.GetFirstEntry(key));
#ifdef USE_EVAL_HASH
  evaluation::PrefetchEvaluationHash(key);
#endif  // USE_EVAL_HASH
  rb_->accumulator.computed_score = false;

  rb_->plies_from_null = 0;
  side_ = ~side_;

  UpdateCheckState<true>(rb_);
}

void Position::StepBackwardNull() {
  ASSERT_LV3(!checker());

  rb_ = rb_->previous;
  side_ = ~side_;
}

template <Color::Value Us>
void Position::StepForwardImpl(const Move move, Rollback& new_rb,
                               const bool is_check) {
  // NONE, NULL, RESIGNなどは対応できない
  ASSERT_LV3(move.ok());
  ASSERT_LV3(&new_rb != rb_);

  // 探索ノード数を更新
  // 探索ノード数はこの関数の呼び出し回数に大体等しい
  ptr_thread_->nodes.fetch_add(1, std::memory_order_relaxed);

  // hash keyを記録
  auto k = rb_->board_key_ ^ zobrist::side;
  auto h = rb_->hand_key_;

  // 一つ前を保存してからRoolbackを更新
  Rollback* previous;
  new_rb.previous = previous = rb_;
  rb_ = &new_rb;

  ++game_ply_;
  // 遡られる手数を更新
  rb_->plies_from_null = previous->plies_from_null + 1;

  // 評価値の差分計算用の初期化
  rb_->accumulator.computed_accumulation = false;
  rb_->accumulator.computed_score = false;

  const Square target = move.target();
  ASSERT_LV2(target.ok());

  // 駒割りの差分計算用
  int material_delta;

  auto& dp = rb_->dirty_piece;

  if (move.drop()) {
    ASSERT_LV2(location_[target] == Piece::EMPTY);

    // pieceにはcolorが含まれる
    const Piece piece = moved_piece(move);
    const Piece piece_raw_type = piece.raw_type();
    ASSERT_LV2(piece_raw_type >= Piece::HAND_ZERO &&
               piece_raw_type < Piece::HAND_SIZE);

    h -= zobrist::hand[Us][piece_raw_type];
    k += zobrist::psq[target][piece];

    // ここでハッシュが確定
    const Key key = k + h;
    search::Prefetch(search::tt.GetFirstEntry(key));
#ifdef USE_EVAL_HASH
    evaluation::PrefetchEvaluationHash(key);
#endif  // USE_EVAL_HASH

    location_.Add(target, piece);

    const PieceNumber piece_number = GetPieceNumber(Us, piece_raw_type);
    ASSERT_LV3(piece_number.ok());

    // 差分計算のために動いた駒を記録
    dp.dirty_num = 1;
    dp.piece_numbers[0] = piece_number;
    dp.changed_pieces[0].old_piece = evaluation_list_.bona_piece(piece_number);

    evaluation_list_.Update(piece_number, target, piece);
    dp.changed_pieces[0].new_piece = evaluation_list_.bona_piece(piece_number);

    // BonaPieceの計算で駒の枚数を参照した
    location_.Sub(Us, piece_raw_type);

    if (is_check) {
      // 王手している駒のBitboardを更新
      // 駒打ちなので、この駒で王手した
      // 両王手はない
      rb_->checkers_bb = Bitboard(target);
      rb_->continuous_check[Us] = previous->continuous_check[Us] + 2;
    } else {
      rb_->checkers_bb = Bitboard::Zero;
      rb_->continuous_check[Us] = 0;
    }

    // 駒打ちなので捕った駒はない
    rb_->captured_piece = Piece::EMPTY;

    // Addを呼び出したので更新
    location_.Update();

    // 駒打ちなので駒割りの変化はなし
    material_delta = 0;
  } else {
    const Square source = move.source();
    ASSERT_LV2(source.ok());

    const Piece piece_before = location_[source];
    ASSERT_LV2(piece_before != Piece::EMPTY);

    const Piece piece_after = moved_piece(move);

    material_delta =
        move.promotion() ? evaluation::promotion_value_delta[piece_before] : 0;

    const Piece target_piece = location_[target];
    if (target_piece != Piece::EMPTY) {
      // ルール上、ここで王を捕ることはない
      ASSERT_LV1(target_piece.type() != Piece::OU);

      const Piece target_piece_raw = target_piece.raw_type();
      // 捕獲したこの駒についてevaluation_list_を更新
      const PieceNumber piece_number = GetPieceNumber(target);
      ASSERT_LV3(piece_number.ok());
      // 動いた駒と捕った駒
      dp.dirty_num = 2;
      dp.piece_numbers[1] = piece_number;
      dp.changed_pieces[1].old_piece =
          evaluation_list_.bona_piece(piece_number);

      evaluation_list_.Update(piece_number, Us, target_piece_raw,
                              location_.hand(Us).count(target_piece_raw));
      dp.changed_pieces[1].new_piece =
          evaluation_list_.bona_piece(piece_number);

      location_.Add(Us, target_piece_raw);
      location_.Remove(target);

      k -= zobrist::psq[target][target_piece];
      h += zobrist::hand[Us][target_piece_raw];

      // 捕獲した駒を記録
      rb_->captured_piece = target_piece;

      material_delta += evaluation::captured_value_delta[target_piece];
    } else {
      rb_->captured_piece = Piece::EMPTY;

      dp.dirty_num = 1;
    }

    // 移動元のPieceNumber
    const PieceNumber piece_number = GetPieceNumber(source);
    dp.piece_numbers[0] = piece_number;
    dp.changed_pieces[0].old_piece = evaluation_list_.bona_piece(piece_number);

    location_.Remove(source);
    location_.Add(target, piece_after);

    evaluation_list_.Update(piece_number, target, piece_after);
    dp.changed_pieces[0].new_piece = evaluation_list_.bona_piece(piece_number);

    if (piece_before.type() == Piece::OU) {
      // 手動で更新
      location_.UpdateOuSquare<Us>(target);
    }

    k -= zobrist::psq[source][piece_before];
    k += zobrist::psq[target][piece_after];

    // ハッシュ値が確定した
    const Key key = k + h;
    search::Prefetch(search::tt.GetFirstEntry(key));
#ifdef USE_EVAL_HASH
    evaluation::PrefetchEvaluationHash(key);
#endif  // USE_EVAL_HASH

    // 駒を移動したので更新
    location_.Update();

    if (is_check) {
      const Rollback* previous2 = rb_->previous;

      // 移動した駒で王手したか
      rb_->checkers_bb = previous2->check_squares[piece_after.type()] & target;

      // 空き王手になるか
      const Square ou_square = location_.ou_square(~Us);
      if (revealed(source, target, ou_square,
                    previous2->blockers_for_ou[~Us] & source)) {
        effect8::DirectionSet directions =
            effect8::GetDirection(source, ou_square);
        switch (directions.Pop()) {
          case effect8::Direction::U:
          case effect8::Direction::D:
            // 縦方向で空き王手ということは飛か香で王手になる
            // 王手になることはわかっているので香の後ろに王があるといったことはない
            // sourceが王の前方ならば利きが王で遮られるので、王の後ろの香を拾うことはない
            rb_->checkers_bb |=
                detail::GetHiFileEffect(source, location_.get<>()) &
                location_.color<Us>();
            break;
          case effect8::Direction::R:
          case effect8::Direction::L:
            // 横方向で空き王手ということは飛で王手になる
            rb_->checkers_bb |=
                detail::GetHiRankEffect(source, location_.get<>()) &
                location_.color<Us>();
            break;
          case effect8::Direction::RU:
          case effect8::Direction::LD:
            rb_->checkers_bb |=
                detail::GetKaDiagonalEffect<0>(source, location_.get<>()) &
                location_.color<Us>();
            break;
          case effect8::Direction::RD:
          case effect8::Direction::LU:
            rb_->checkers_bb |=
                detail::GetKaDiagonalEffect<1>(source, location_.get<>()) &
                location_.color<Us>();
            break;
          default:
            UNREACHABLE;
            break;
        }
      }
      ASSERT_LV3(rb_->checkers_bb = attacker(Us, ou_square));

      rb_->continuous_check[Us] = previous->continuous_check[Us] + 2;
    } else {
      rb_->checkers_bb = Bitboard::Zero;
      rb_->continuous_check[Us] = 0;
    }
  }

  // 相手番の方は変更がないのでそのまま
  rb_->continuous_check[enum_cast<>(~Us)] = previous->continuous_check[~Us];

  rb_->material_value = static_cast<evaluation::Value>(
      enum_cast<>(rb_->previous->material_value) +
      (Us == Color::BLACK ? +material_delta : -material_delta));

  side_ = ~side_;

  // ハッシュ値を更新
  rb_->board_key_ = k;
  rb_->hand_key_ = h;

  rb_->hand = location_.hand(side_);

  UpdateCheckState<false>(rb_);
}

template <Color::Value Us>
void Position::StepBackwardImpl(const Move move) {
  const Square target = move.target();
  ASSERT_LV2(target.ok());

#ifdef KEEP_PIECE_IN_GENERATED_MOVE
  const Piece piece_after = moved_piece(move);
#else
  // 手番が変わるので、moved_pieceは使えない
  const Piece piece_after = location_[target];
#endif  // KEEP_PIECE_IN_GENERATED_MOVE

  // 今targetにあるPieceNumberは移動元のPieceNumberと同じもの
  const PieceNumber piece_number = GetPieceNumber(target);
  ASSERT_LV3(piece_number.ok());

  // Moveの成りのフラグからPieceの成りのフラグを直接操作する
  static_assert(
      enum_cast<>(Move::PROMOTION) / enum_cast<>(Piece::PROMOTION) == (1 << 12),
      "");
  const Piece piece_before(piece_after.value ^
                           ((move.value & Move::PROMOTION) >> 12));

  if (move.drop()) {
    const Piece piece_raw_type = piece_after.raw_type();

    // NOTE: 手番がテンプレート引数になっているので、
    //       後続の処理もテンプレート引数を利用したい
    evaluation_list_.Update(piece_number, Us, piece_raw_type,
                            location_.hand(Us).count(piece_raw_type));
    location_.Add(Us, piece_raw_type);
    location_.Remove(target);
  } else {
    const Square source = move.source();
    ASSERT_LV2(source.ok());

    location_.Remove(target);
    if (rb_->captured_piece != Piece::EMPTY) {
      const Piece captured_piece = rb_->captured_piece;

      // NOTE: 駒をRemoveしてからcaptured_pieceを復元している
      //       一度の操作で置き換えた方が効率は良いか？
      location_.Add(target, captured_piece);
      location_.Add(source, piece_before);

      const PieceNumber piece_number2 =
          GetPieceNumber(Us, captured_piece.raw_type());
      ASSERT_LV3(piece_number2.ok());

      evaluation_list_.Update(piece_number2, target, captured_piece);
      location_.Sub(Us, captured_piece.raw_type());
      evaluation_list_.Update(piece_number, source, piece_before);
    } else {
      location_.Add(source, piece_before);
      evaluation_list_.Update(piece_number, source, piece_before);
    }

    if (piece_before.type() == Piece::OU) {
      location_.UpdateOuSquare<Us>(source);
    }
  }

  location_.Update();

  // この関数を呼び出すときに手番を入れ替えて呼び出した
  side_ = Us;

  rb_ = rb_->previous;

  --game_ply_;
}

template <bool NullMove>
void Position::UpdateCheckState(Rollback* rb) const {
  if (!NullMove) {
    rb->blockers_for_ou[enum_cast<>(Color::WHITE)] =
        blocker(Color::BLACK, location_.ou_square(Color::WHITE),
                rb->pinners_for_ou[enum_cast<>(Color::WHITE)]);
    rb->blockers_for_ou[enum_cast<>(Color::BLACK)] =
        blocker(Color::WHITE, location_.ou_square(Color::BLACK),
                rb->pinners_for_ou[enum_cast<>(Color::BLACK)]);
  }

  const Square ou_square = location_.ou_square(~side_);

  const Bitboard& occupied = location_.get<>();
  const Color them = ~side_;

  // 相手の玉に王手をかけられる位置を駒種ごとに調べる

  // 二歩のチェックはここではしない
  // 指し手の生成の時に行う
  rb->check_squares[Piece::FU] = GetFuEffect(them, ou_square);
  rb->check_squares[Piece::KE] = GetKeEffect(them, ou_square);
  rb->check_squares[Piece::GI] = GetGiEffect(them, ou_square);
  rb->check_squares[Piece::KA] = GetKaEffect(ou_square, occupied);
  rb->check_squares[Piece::HI] = GetHiEffect(ou_square, occupied);
  rb->check_squares[Piece::KI] = GetKiEffect(them, ou_square);
  // 香は飛をマスクすることで求める
  rb->check_squares[Piece::KY] =
      rb->check_squares[Piece::HI] & detail::GetKyNaiveEffect(them, ou_square);
  // 王で王手はできない
  rb->check_squares[Piece::OU] = Bitboard::Zero;

  // 指し手ごとに王手の判定があるので、ここでの処理のコストは回収できる
  rb->check_squares[Piece::TO] = rb->check_squares[Piece::KI];
  rb->check_squares[Piece::NY] = rb->check_squares[Piece::KI];
  rb->check_squares[Piece::NK] = rb->check_squares[Piece::KI];
  rb->check_squares[Piece::NG] = rb->check_squares[Piece::KI];
  rb->check_squares[Piece::UM] =
      rb->check_squares[Piece::KA] | GetOuEffect(ou_square);
  rb->check_squares[Piece::RY] =
      rb->check_squares[Piece::HI] | GetOuEffect(ou_square);
}

}  // namespace state_action
