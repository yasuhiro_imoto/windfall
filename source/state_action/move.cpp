#include "stdafx.h"

#include "move.h"

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../usi/protocol.h"
#include "bitboard.h"
#include "move_helper.h"
#include "position.h"

namespace std {
bool operator==(
    const std::pair<const std::string, state_action::Move::Value>& x,
    const std::string& y) noexcept {
  return x.first == y;
}
}  // namespace std

namespace state_action {
namespace detail {
using UsiMoveMap = std::unordered_map<std::string, Move::Value>;

const UsiMoveMap special_move = {{"resign", Move::RESIGN},
                                 {"win", Move::WIN},
                                 {"0000", Move::NULL_MOVE},
                                 {"null", Move::NULL_MOVE},
                                 {"pass", Move::NULL_MOVE}};

constexpr Square MakeSquare(const char f, const char r) {
  const File file(f);
  const Rank rank(r);
  return file.ok() && rank.ok() ? Square(file, rank) : Square::SIZE;
}

Move::Value ParseUsiString(const std::string& usi_string) {
  const UsiMoveMap::const_iterator it = boost::find(special_move, usi_string);
  if (it != special_move.end()) {
    return it->second;
  }

  if (usi_string.size() <= 3) {
    // 変な文字列
    return Move::NONE;
  }

  const Square target = MakeSquare(usi_string[2], usi_string[3]);
  if (!target.ok()) {
    return Move::NONE;
  }

  const bool promotion = usi_string.size() == 5 && usi_string[4] == '+';
  const bool drop = usi_string[1] == '*';

  if (drop) {
    for (int i = 1; i <= 7; ++i) {
      if (Piece::usi_piece[2 * i] == usi_string[0]) {
        return Move::CalculateValue(static_cast<Piece::Value>(i), target);
      }
    }
  } else {
    const Square source = MakeSquare(usi_string[0], usi_string[1]);
    if (source.ok()) {
      return Move::CalculateValue(source, target, promotion);
    }
  }
  return Move::NONE;
}
}  // namespace detail

Move::Move(const std::string& usi_string)
    : value(detail::ParseUsiString(usi_string)) {}

Move::Move(const Position& position, const std::string& usi_string)
    : Move(usi_string) {
  // 上位16bitを設定しないと
  *this = position.ConvertMove16ToMove(*this);
  if (!position.IsPseudoLegal(*this) || !position.IsLegal(*this)) {
    value = NONE;
  }
}

 std::ostream& operator<<(std::ostream& os, const Move& m) {
  // デフォルトではUSI形式で出力
  // USIでない形式で出力することも考えられるが必要性が低そうなのでやらない
  os << usi::Move(m);
  return os;
}
}  // namespace state_action
