#pragma once
#ifndef HAND_H_INCLUDED
#define HAND_H_INCLUDED

#include <array>
#include <cstdint>

#include "../rule/piece.h"
#include "../utility/bit_operator.h"

namespace state_action {
namespace detail {
//! 順序は保証されないので、手元でデバッグするときのみ参考程度に利用
struct HandBitField {
  uint8_t fu;
  uint8_t ky : 4;
  uint8_t ke : 4;
  uint8_t gi : 4;
  uint8_t ka : 4;
  uint8_t hi : 4;
  uint8_t ki : 4;
};

//! 手駒のbit位置
constexpr std::array<int, Piece::HAND_SIZE> piece_bits = {
    0,         0 /*歩*/,  8 /*香*/,  12 /*桂*/,
    16 /*銀*/, 20 /*角*/, 24 /*飛*/, 28 /*金*/};
//! Piece(歩,香,桂,銀,金,角,飛)を手駒に変換するテーブル
constexpr std::array<uint32_t, Piece::HAND_SIZE> piece_to_hand = {
    0,
    1 << piece_bits[Piece::FU] /*歩*/,
    1 << piece_bits[Piece::KY] /*香*/,
    1 << piece_bits[Piece::KE] /*桂*/,
    1 << piece_bits[Piece::GI] /*銀*/,
    1 << piece_bits[Piece::KA] /*角*/,
    1 << piece_bits[Piece::HI] /*飛*/,
    1 << piece_bits[Piece::KI] /*金*/
};
//! その持ち駒を表現するのに必要なbit数のmask(例えば3bitなら2の3乗-1で7)
constexpr std::array<int, Piece::HAND_SIZE> piece_bit_mask = {
    0,        31 /*歩は5bit*/, 7 /*香は3bit*/, 7 /*桂*/,
    7 /*銀*/, 3 /*角*/,        3 /*飛*/,       7 /*金*/
};
constexpr std::array<int, Piece::HAND_SIZE> piece_bit_mask2 = {
    0,
    piece_bit_mask[Piece::FU] << piece_bits[Piece::FU],
    piece_bit_mask[Piece::KY] << piece_bits[Piece::KY],
    piece_bit_mask[Piece::KE] << piece_bits[Piece::KE],
    piece_bit_mask[Piece::GI] << piece_bits[Piece::GI],
    piece_bit_mask[Piece::KA] << piece_bits[Piece::KA],
    piece_bit_mask[Piece::HI] << piece_bits[Piece::HI],
    piece_bit_mask[Piece::KI] << piece_bits[Piece::KI]};
//! 駒の枚数が格納されているbitが1となっているMASK。(駒種を得るときに使う)
constexpr int32_t hand_bit_mask =
    piece_bit_mask2[Piece::FU] | piece_bit_mask2[Piece::KY] |
    piece_bit_mask2[Piece::KE] | piece_bit_mask2[Piece::GI] |
    piece_bit_mask2[Piece::KA] | piece_bit_mask2[Piece::HI] |
    piece_bit_mask2[Piece::KI];
// 余らせてあるbitの集合
constexpr int32_t hand_borrow_mask = (hand_bit_mask << 1) & ~hand_bit_mask;
}  // namespace detail

struct Hand {
  union {
    uint32_t hand;
    detail::HandBitField bit;
  };

  constexpr bool operator==(const uint32_t v) const { return hand == v; }
  constexpr bool operator==(const Hand other) const {
    return hand == other.hand;
  }

  //! 手駒の枚数を返す
  constexpr int count(const Piece piece_raw_type) const {
    return (hand >> detail::piece_bits[piece_raw_type]) &
           detail::piece_bit_mask[piece_raw_type];
  }
  constexpr int operator[](const Piece piece_raw_type) const {
    return count(piece_raw_type);
  }

  //! 手駒pcを持っているかどうか
  constexpr bool Exists(const Piece piece_raw_type) const {
    return static_cast<bool>(hand & detail::piece_bit_mask2[piece_raw_type]);
  }

  //! 手駒にpをc枚加える
  constexpr Hand& Add(const Piece piece_raw_type, const int c = 1) {
    hand = hand + detail::piece_to_hand[piece_raw_type] * c;
    return *this;
  }
  //! 手駒からpをc枚減らす
  constexpr Hand& Sub(const Piece piece_raw_type, const int c = 1) {
    hand = hand - detail::piece_to_hand[piece_raw_type] * c;
    return *this;
  }
};

//! 手駒h1のほうがh2より優れているか。(すべての種類の手駒がh2のそれ以上ある)
constexpr bool operator>=(const Hand h1, const Hand h2) {
  return ((h1.hand - h2.hand) & detail::hand_borrow_mask) == 0;
}
constexpr bool operator<=(const Hand h1, const Hand h2) { return h2 >= h1; }

//! デバッグ用出力
std::ostream& operator<<(std::ostream& os, const Hand hand);

//! 駒の有無をビットフラグで表現
struct HandKind {
  enum HandKindValue : uint32_t {
    FU = 1 << (Piece::FU - 1),
    KY = 1 << (Piece::KY - 1),
    KE = 1 << (Piece::KE - 1),
    GI = 1 << (Piece::GI - 1),
    KA = 1 << (Piece::KA - 1),
    HI = 1 << (Piece::HI - 1),
    KI = 1 << (Piece::KI - 1),
    OU = 1 << (Piece::OU - 1),  //!< フラグ用
    ZERO = 0
  };

  HandKind(const Hand hand)
      : value(PEXT32(hand.hand + detail::hand_bit_mask,
                     detail::hand_borrow_mask)) {}

  //! piece_typeの駒を持っているかを判定
  constexpr bool Exists(const Piece piece_type) const {
    return static_cast<bool>(value & (1 << (piece_type.value - 1)));
  }
  //! 歩以外の駒を持っているか判定
  constexpr bool ExistsExceptFu() const {
    return static_cast<bool>(value & ~FU);
  }

 private:
  uint32_t value;
};

//! デバッグ用出力
std::ostream& operator<<(std::ostream& os, const HandKind hand_kind);
}  // namespace state_action
#endif  // !HAND_H_INCLUDED
