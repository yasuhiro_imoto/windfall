#include "stdafx.h"

#include "zobrist_hash.h"

#include "position.h"
#include "../utility/random_generator.h"
#include "../utility/hash_key.h"

namespace state_action {
namespace zobrist {
HashKey psq[Square::SIZE_PLUS1][Piece::SIZE];
HashKey hand[Color::SIZE][Piece::HAND_SIZE];
HashKey depth[max_ply];

/**
 * @brief braced initializer listで評価順序を固定する
*/
struct RandomHash {
  RandomHash(const uint64_t a, const uint64_t b, const uint64_t c,
             const uint64_t d)
      : x(a), y(b), z(c), w(d) {}

  uint64_t x, y, z, w;
};

#if HASH_KEY_BITS <= 64
void Set(HashKey& h, const RandomHash& r) { h = r.x; }
#elif HASH_KEY_BITS <= 128
void Set(HashKey& h, const RandomHash& r) { h.set(r.x, r.y); }
#else
void Set(HashKey& h, const RandomHash& r) { h.set(r.x, r.y, r.z, r.w); }
#endif  // HASH_KEY_BITS<=64

void Initialize() {
  // YaneuraOuと同じシードにする
  utility::PseudoRandom generator(20151225);

  for (const auto piece : PIECE) {
    for (const auto square : SQUARE) {
      // 駒がない場合はハッシュを0にしておく
      // グローバル変数なので、初期値が0なのは保証される
      if (piece != Piece::EMPTY) {
        // 最下位ビットは手番用に常に0にする
        Set(psq[square][piece],
            RandomHash{generator.rand<Key>() & ~1ull, generator.rand<Key>(),
                       generator.rand<Key>(), generator.rand<Key>()});
      }
    }
  }

  for (const auto color : COLOR) {
    for (Piece piece = Piece::HAND_ZERO; piece < Piece::HAND_SIZE; ++piece) {
      Set(hand[color][piece],
          RandomHash{generator.rand<Key>() & ~1ull, generator.rand<Key>(),
                     generator.rand<Key>(), generator.rand<Key>()});
    }
  }

  for (int ply = 0; ply < max_ply; ++ply) {
    Set(depth[ply],
        RandomHash{generator.rand<Key>() & ~1ull, generator.rand<Key>(),
                   generator.rand<Key>(), generator.rand<Key>()});
  }

#ifdef CUCKOO
  // 何かの処理
#endif // CUCKOO

}
}  // namespace zobrist
}  // namespace state_action
