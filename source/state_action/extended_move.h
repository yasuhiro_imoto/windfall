#pragma once
#ifndef EXTENDED_MOVE_H_INCLUDED
#define EXTENDED_MOVE_H_INCLUDED

#include <algorithm>
#include <array>

#include "move.h"

namespace state_action {
#pragma warning(push)
#pragma warning(disable : 26495)
/**
 * @brief オーダーリングのために指し手とスコアを一体にしたデータ型
 *
 * ExtMove
 */
struct ExtendedMove {
  //! 指し手(32bit)
  Move move;
  //! 指し手のオーダーリングのためのスコア
  int32_t value;

  bool operator==(const Move m) const noexcept { return move == m; }

  bool operator<(const ExtendedMove& rhs) const noexcept {
    return value < rhs.value;
  }
  bool operator<=(const ExtendedMove& rhs) const noexcept {
    return value <= rhs.value;
  }
  bool operator>(const ExtendedMove& rhs) const noexcept {
    return value > rhs.value;
  }
  bool operator>=(const ExtendedMove& rhs) const noexcept {
    return value >= rhs.value;
  }

  // 暗黙の変換
  operator Move() const noexcept { return move; }
  // 暗黙の変換
  ExtendedMove& operator=(const Move m) noexcept {
    move = m;
    return *this;
  }

  // 意図しない変換を防ぐための対策
  // cf. Fix involuntary conversions of ExtMove to Move :
  // https://github.com/official-stockfish/Stockfish/commit/d482e3a8905ee194bda3f67a21dda5132c21f30b
  operator float() const = delete;
};
#pragma warning(pop)

/**
 * @brief 一局面あたりの合法手の最大値
 *
 * 正確には593らしいが、念のために少し余裕を持たせる
 * MAX_MOVES
 */
constexpr int max_moves = 600;

// 前方宣言
class Position;

struct ExtendedMoveArray {
 private:
  using Array = std::array<ExtendedMove, max_moves>;

 public:
  ExtendedMoveArray() : end_(list_.begin()) {}

  using iterator = Array::iterator;
  using const_iterator = Array::const_iterator;
  using difference_type = Array::difference_type;
  using value_type = Array::value_type;
  using reference = Array::reference;
  using const_reference = Array::const_reference;
  using pointer = Array::pointer;
  using const_pointer = Array::const_pointer;
  using size_type = Array::size_type;

  void push_back(const Move move, const uint16_t upper) {
    end_++->move = Move(move, upper);
  }
  /**
   * @brief 駒を打つ場合に使う
   * @param move 駒の種類などの打つ座標以外はセットされている
   * @param target 駒を打つ座標
   */
  void push_back(const Move move, const Square target) {
    end_++->move = move + target;
  }

  iterator begin() { return list_.begin(); }
  iterator end() { return end_; }
  const_iterator begin() const { return list_.begin(); }
  const_iterator end() const { return end_; }

  size_type size() const { return std::distance(begin(), end()); }
  bool empty() const noexcept { return begin() == end(); }

  /**
   * @brief 配列の末尾をitに変更する
   * @param it 配列の要素を指す有効なイテレータ
   */
  void resize(const iterator it) { end_ = it; }

  reference operator[](const size_t i) {
    ASSERT_LV3(i < size());
    return list_[i];
  }
  const_reference operator[](const size_t i) const {
    ASSERT_LV3(i < size());
    return list_[i];
  }

  void RemoveIllegalMoves(const Position& position);
  void RemovePsuedoIllegalMoves(const Position& position);

  pointer data() noexcept { return list_.data(); }
  const_pointer data() const noexcept { return list_.data(); }

  // MovePickerの方で継承して使うので、アクセス制限を緩和
 protected:
  Array list_;
  /**
   * @brief 有効な範囲の一つ外側を指している
   *
   * ここにはまだ値が入っていない
   * 要素を追加するときはここに値を書き込む
   */
  iterator end_;
};
}  // namespace state_action
#endif  // !EXTENDED_MOVE_H_INCLUDED
