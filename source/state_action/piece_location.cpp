#include "stdafx.h"

#include "piece_location.h"

#include "../rule/color.h"
#include "../rule/piece.h"
#include "bitboard.h"

namespace state_action {
void PieceLocation::Update() {
  type_bb[Piece::HDK] = get<Piece::OU, Piece::UM, Piece::RY>();
  type_bb[Piece::GOLDS] =
      get<Piece::KI, Piece::TO, Piece::NY, Piece::NK, Piece::NG>();

  type_bb[Piece::BISHOP_HORSE] = get<Piece::KA, Piece::UM>();
  type_bb[Piece::ROOK_DRAGON] = get<Piece::HI, Piece::RY>();
  type_bb[Piece::SILVER_HDK] = get<Piece::GI, Piece::HDK>();
  type_bb[Piece::GOLDS_HDK] = get<Piece::GOLDS, Piece::HDK>();
}

void PieceLocation::UpdateOuSquare() {
  for (const Color color : COLOR) {
    Bitboard b = get<Piece::OU>(color);
    ou_square_[color] = b ? b.Pop() : Square::SIZE;
  }
}
}  // namespace state_action
