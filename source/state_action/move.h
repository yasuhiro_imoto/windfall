#pragma once
#ifndef MOVE_H_INCLUDED
#define MOVE_H_INCLUDED

#include <array>
#include <boost/operators.hpp>
#include <cstdint>
#include <iosfwd>
#include <string>

#pragma warning(push)
#pragma warning(disable : 4566 26812 26451 26439 28020 26495 6387 28182)
#include "spdlog/fmt/ostr.h"
#pragma warning(pop)

#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/level_assert.h"

namespace state_action {
class Position;

struct Move : private boost::operators<Move> {
  enum Value : uint32_t {
    NONE = 0,                  //!< 無効な値
    NULL_MOVE = (1 << 7) + 1,  //!< null move
    RESIGN = (2 << 7) + 2,  //!< operator<<で出力した時にresignと表示する値
    WIN = (3 << 7) + 3,  //! 宣言勝ちの宣言

    DROP_MASK = 1 << 14,  //!< 駒打ちのフラグ
    PROMOTION = 1 << 15,  //!< 成りのフラグ

    ZERO = 0
  };

#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Move() : value(NONE) {}
#pragma warning(pop)
  explicit constexpr Move(const uint16_t move)
      : value(static_cast<Value>(move)) {}

  constexpr Move(const Value value) : value(value) {}
  constexpr Move(const Square src, const Square tgt,
                 const bool promotion = false)
      : value(CalculateValue(src, tgt, promotion)) {}
  constexpr Move(const Piece pt, const Square tgt)
      : value(CalculateValue(pt, tgt)) {}

  //! 16bitに切り詰められたMoveを32bit化
  /*! lowerの上位16ビットは無視される
      upperの下位16bitに復元すべき情報が入っている*/
  constexpr Move(const Move lower, const uint32_t upper)
      : value(static_cast<Value>((lower.value & 0xFFFF) | (upper << 16))) {}

  Move(const std::string& usi_string);
  // 上位16bitを設定するためにPositionが必要
  Move(const Position& position, const std::string& usi_string);

  //! usi形式の文字列から変換するときに使う
  static constexpr Value CalculateValue(const Piece pt,
                                        const Square tgt) noexcept {
    return static_cast<Value>((pt.value << 7) + tgt + DROP_MASK);
  }
  static constexpr Value CalculateValue(const Square src, const Square tgt,
                                        const bool promotion = false) noexcept {
    return static_cast<Value>((src << 7) + tgt +
                              (promotion ? PROMOTION : ZERO));
  }

  constexpr Move& operator=(const Value move) {
    value = move;
    return *this;
  }

  constexpr bool operator==(const Move& other) const {
    return value == other.value;
  }

  constexpr bool operator==(const Value& other) const {
    return value == other;
  }
  constexpr bool operator!=(const Value& other) const {
    return value != other;
  }

  //! 指し手の移動元
  constexpr Square source() const {
    ASSERT_LV3(!(value & DROP_MASK));
    return static_cast<Square>((value >> 7) & 0x7F);
  }
  //! 指し手の移動先
  constexpr Square target() const { return static_cast<Square>(value & 0x7F); }

  //! 駒打ちかどうか
  constexpr bool drop() const { return (value & DROP_MASK) != 0; }
  //! 成りかどうか
  constexpr bool promotion() const { return (value & PROMOTION) != 0; }

  //! 移動元と移動先を直列化
  /*! [0, (81 + 7) * 81 -1]の範囲の値が返る */
  constexpr int Serialize() const {
    return (drop() ? static_cast<int>(Square::SIZE - 1 + dropped_piece().value)
                   : static_cast<int>(source().value)) *
               Square::SIZE +
           target();
  }

  //! 打った駒
  /*! 先後の区別はない　FUからKIまでの範囲 */
  constexpr Piece dropped_piece() const {
    return static_cast<Piece>((value >> 7) & 0x7F);
  }

  //! 指し手が正しいか簡易テスト
  /*! 盤面は考慮していない
      NONEとNULL_MOVEはテストに失敗する WINも失敗する */
  constexpr bool ok() const {
    // 駒を打つ場合もあるので、単純にsourceではなく駒打ちのフラグも含めて比較
    return (value >> 7) != (value & 0x7F);
  }

  //! 見た目に、わかりやすい形式で表示
  std::string prettify() const;
  //! 移動させた駒がわかっているときに見た目に、わかりやすい形式で表示
  std::string prettify(const Piece moved_pt) const;

  constexpr operator bool() const noexcept { return value != Value::NONE; }
  constexpr operator uint16_t() const noexcept { return value & 0xFFFF; }
  constexpr operator uint64_t() const noexcept { return value; }
  // 意図しないキャストを検出するために宣言だけしておく
  template <typename T,
            std::enable_if_t<std::is_integral_v<T>, std::nullptr_t> = nullptr>
  constexpr operator T() const noexcept;

  Value value;

  //! spdlogで正しく出力するために必要
  //! インクルードの循環を防ぐためにusi/protocol.hで定義する
  // template <typename OStream>
  // friend OStream& operator<<(OStream& os, const Move& m);
};

//! usi形式で出力
std::ostream& operator<<(std::ostream& os, const Move& m);

constexpr Move operator+(const Move m, const Square s) {
  return static_cast<Move::Value>(enum_cast<>(m.value) + enum_cast<>(s.value));
}
constexpr Move operator+(const Square s, const Move m) {
  return static_cast<Move::Value>(enum_cast<>(s.value) + enum_cast<>(m.value));
}
}  // namespace state_action

namespace std {
template <>
struct hash<state_action::Move> {
  size_t operator()(const state_action::Move& move) const {
    return enum_cast<>(move.value);
  }
};
}  // namespace std

//! spdlogで正しく指し手が出力されるために必要
//! インクルードの循環を防ぐためにusi/protocol.hで定義する
template <> struct fmt::formatter<state_action::Move>;

#endif  // !MOVE_H_INCLUDED
