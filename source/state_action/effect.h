#pragma once
#ifndef EFFECT_H_INCLUDED
#define EFFECT_H_INCLUDED

#include <array>

#include "../rule/color.h"
#include "../rule/square.h"
#include "../utility/bit_operator.h"
#include "../utility/level_assert.h"
#include "bitboard.h"
#include "direction.h"

namespace state_action {
//! 歩が打てる筋を得るためのBitboard mask
/*! 1～7筋にそれぞれ歩があるかを7桁の2進数で表現して、配列のインデックスとする
    p[0]には1～7筋 、p[1]には8,9筋のときのデータが入っている
    PAWN_DROP_MASK_BB */
extern std::array<Bitboard, 0x80> fu_drop_mask_bb_table;

namespace detail {
//! 2升に挟まれている升を返すためのテーブル(その2升は含まない)
/*! この配列には直接アクセスせずにbetween_bb()を使うこと。
 *  配列サイズが大きくてcache汚染がひどいのでシュリンクしてある。
    BetweenBB */
extern std::array<Bitboard, 785> between_bb_table;
/*! BetweenIndex */
extern uint16_t between_index_table[Square::SIZE_PLUS1][Square::SIZE_PLUS1];
}  // namespace detail
//! 2升に挟まれている升を表すBitboardを返す
/*! sq1とsq2が縦横斜めの関係にないときはZERO_BBが返る
    note: 戻り値が参照でない方が良いかもしれない
    between_bb */
inline const Bitboard& GetBetweenBB(const Square s1, const Square s2) {
  return detail::between_bb_table[detail::between_index_table[s1][s2]];
}

namespace detail {
//! 2升を通過する直線を返すためのテーブル
/*!
 * 2つ目のindexは[0]:右上から左下、[1]:横方向、[2]:左上から右下、[3]:縦方向の直線
   LineBB */
extern Bitboard line_bb_table[Square::SIZE][4];
}  // namespace detail
//! 2升を通過する直線を返すためのBitboardを返す
/*! sq1とsq2が縦横斜めの関係にないときに呼び出してはならない
    note: 戻り値が参照でない方が良いかもしれない
    line_bb*/
inline const Bitboard& GetLineBB(const Square s1, const Square s2) {
  static_assert(effect8::Direction::RU == 0 && effect8::Direction::LD == 7, "");
  effect8::DirectionSet directions = effect8::GetDirection(s1, s2);
  ASSERT_LV3(directions != 0);
  static constexpr std::array<int, 8> a{{0, 1, 2, 3, 3, 2, 1, 0}};
  return detail::line_bb_table[s1][a[directions.Pop()]];
}

namespace detail {
/*! KingEffectBB */
extern std::array<Bitboard, Square::SIZE_PLUS1> ou_effect_bb;
/*! GoldEffectBB */
extern Bitboard ki_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
/*! SilverEffectBB */
extern Bitboard gi_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
/*! KnightEffectBB */
extern Bitboard ke_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
/*! PawnEffectBB */
extern Bitboard fu_effect_bb[Square::SIZE_PLUS1][Color::SIZE];

//! 盤上の駒をないものとして扱う
/*! LanceStepEffectBB */
extern Bitboard ky_naive_effect_bb[Square::SIZE_PLUS1][Color::SIZE];
//! 盤上の駒をないものとして扱う
/*! BishopStepEffectBB */
extern std::array<Bitboard, Square::SIZE_PLUS1> ka_naive_effect_bb;
//! 盤上の駒をないものとして扱う
/*! RookStepEffectBB */
extern std::array<Bitboard, Square::SIZE_PLUS1> hi_naive_effect_bb;

//! 角の利き
/*! BishopEffect */
extern Bitboard ka_effect_bb[2][1856 + 1];
/*! BishopEffectMask */
extern Bitboard ka_effect_mask_bb[2][Square::SIZE_PLUS1];
/*! BishopEffectIndex */
extern int ka_effect_index_table[2][Square::SIZE_PLUS1];

//! 飛車の縦の利き
/*! 指定した升sqの属するfileのbitをshiftし、index を求める
    Slide */
constexpr std::array<uint8_t, Square::SIZE_PLUS1> slide_table = {
    1,  1,  1,  1,  1,  1,  1,  1,  1,   //
    10, 10, 10, 10, 10, 10, 10, 10, 10,  //
    19, 19, 19, 19, 19, 19, 19, 19, 19,  //
    28, 28, 28, 28, 28, 28, 28, 28, 28,  //
    37, 37, 37, 37, 37, 37, 37, 37, 37,  //
    46, 46, 46, 46, 46, 46, 46, 46, 46,  //
    55, 55, 55, 55, 55, 55, 55, 55, 55,  //
    1,  1,  1,  1,  1,  1,  1,  1,  1,   //
    10, 10, 10, 10, 10, 10, 10, 10, 10,  //
    0};
/*! RookFileEffect */
extern uint64_t hi_file_effect_table[Rank::SIZE + 1][128];
/*! RookRankEffect */
extern Bitboard hi_rank_effect_table[File::SIZE + 1][128];

//! squareにいる~c側の玉に王手となるc側の駒pieceの候補
/*! 第2添字は(pr-1)を渡して使う
    CheckCandidateBB */
extern Bitboard check_candidate_bb_table[Square::SIZE_PLUS1][Piece::OU - 1]
                                        [Color::SIZE];
/*! CheckCandidateKingBB */
extern std::array<Bitboard, Square::SIZE_PLUS1> check_candidate_ou_bb_table;
}  // namespace detail

//! 遠方駒の利きを得るための利きテーブルのインデックスを計算
/*! mask: 利きの算出に絡む升が1のbitboard
    occupiedToIndex */
inline uint64_t ExtractIndexFromOccupied(const Bitboard& occupied,
                                         const Bitboard& mask) {
  return PEXT64(occupied.Merge(), mask.Merge());
}

//! squareにいる~c側の玉に王手となるc側の駒pieceの候補
/*! raw_piece == HI は全域で条件が成立して意味がないので、
 *  代わりにUMで王手になる領域を返す
 *  OUでの呼び出しは不正　GetAround24BBを利用する
    check_candidate_bb */
inline const Bitboard& GetCheckCandidateBB(const Color us,
                                           const Piece raw_piece,
                                           const Square sq) {
  ASSERT_LV3(raw_piece >= Piece::FU && raw_piece < Piece::OU &&
             sq < Square::SIZE_PLUS1 && us.ok());
  return detail::check_candidate_bb_table[sq][raw_piece - 1][us];
}

//! ある升の24近傍のBitboard
/*! around24_bb */
inline const Bitboard& GetAround24BB(const Square sq) {
  ASSERT_LV3(sq < Square::SIZE_PLUS1);
  return detail::check_candidate_ou_bb_table[sq];
}

//! 王の利き
/*! kingEffect */
inline const Bitboard& GetOuEffect(const Square s) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  return detail::ou_effect_bb[s];
}

//! 歩の利き
/*! pawnEffect */
inline const Bitboard& GetFuEffect(const Color c, const Square s) {
  ASSERT_LV3(c.ok() && s < Square::SIZE_PLUS1);
  return detail::fu_effect_bb[s][c];
}
//! Bitboardに対する歩の利き
/*! 座標の循環に注意
    pawnEffect */
inline Bitboard GetFuEffect(const Color c, const Bitboard& bb) {
  ASSERT_LV3(c.ok());
  // zero_bbが戻り値になることはないはずなので、
  // キャストのコストは考える必要がない
  return c == Color::BLACK ? (bb >> 1) : (bb << 1);
}

//! 桂の利き
/*! knightEffect */
inline const Bitboard& GetKeEffect(const Color c, const Square s) {
  ASSERT_LV3(c.ok() && s < Square::SIZE_PLUS1);
  return detail::ke_effect_bb[s][c];
}

//! 銀の利き
/*! silverEffect */
inline const Bitboard& GetGiEffect(const Color c, const Square s) {
  ASSERT_LV3(c.ok() && s < Square::SIZE_PLUS1);
  return detail::gi_effect_bb[s][c];
}

//! 金の利き
/*! goldEffect */
inline const Bitboard& GetKiEffect(const Color c, const Square s) {
  ASSERT_LV3(c.ok() && s < Square::SIZE_PLUS1);
  return detail::ki_effect_bb[s][c];
}

namespace detail {
//! 盤上の駒を考慮しない香の利き
/*! lanceStepEffect */
inline const Bitboard& GetKyNaiveEffect(const Color c, const Square s) {
  ASSERT_LV3(c.ok() && s < Square::SIZE_PLUS1);
  return detail::ky_naive_effect_bb[s][c];
}

template <Color::Value C>
inline const Bitboard& GetKyNaiveEffect(const Square s) {
  static_assert(Color(C).ok(), "");
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  return detail::ky_naive_effect_bb[s][C];
}

//! 盤上の駒を考慮しない角の利き
/*! bishopStepEffect */
inline const Bitboard& GetKaNaiveEffect(const Square s) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  return detail::ka_naive_effect_bb[s];
}

//! 盤上の駒を考慮しない飛車の利き
/*! rookStepEffect */
inline const Bitboard& GetHiNaiveEffect(const Square s) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  return detail::hi_naive_effect_bb[s];
}

//! 盤上の駒を無視するQueenの動き
/*! queenStepEffect */
inline Bitboard GetQueenNaiveEffect(const Square s) {
  return GetKaNaiveEffect(s) | GetHiNaiveEffect(s);
}
}  // namespace detail

template<Piece::Value P>
inline const Bitboard& GetNaiveEffect(const Square s);

template<>
inline const Bitboard& GetNaiveEffect<Piece::KA>(const Square s) {
  return detail::GetKaNaiveEffect(s);
}
template<>
inline const Bitboard& GetNaiveEffect<Piece::HI>(const Square s) {
  return detail::GetHiNaiveEffect(s);
}

/**
 * @brief 十字の利き 利き長さ=1升分
 * @param s 
 * @return 
 * cross00StepEffect
*/
inline Bitboard GetCrossEffect(const Square s) {
  return detail::GetHiNaiveEffect(s) & GetOuEffect(s);
}
/**
 * @brief 対角の利き 利き長さ=1升分
 * @param s 
 * @return 
 * cross45StepEffect
*/
inline Bitboard GetDiagonalEffect(const Square s) {
  return detail::GetKaNaiveEffect(s) & GetOuEffect(s);
}

namespace detail {
//! 角の1方向の利き
/*! N=0なら右上と左下方向、N=1なら左上と右下方向
    bishopEffect0 と bishopEffect1 を統合 */
template <int N>
inline Bitboard GetKaDiagonalEffect(const Square s, const Bitboard& occupied) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  const auto& mask = ka_effect_mask_bb[N][s];
  const auto block = occupied & mask;
  const auto index = ExtractIndexFromOccupied(block, mask);
  return ka_effect_bb[N][ka_effect_index_table[N][s] + index];
}

//! 飛車の縦の利き
/*! rookFileEffect */
inline Bitboard GetHiFileEffect(const Square s, const Bitboard& occupied) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  const int index = (occupied.p[Bitboard::Part(s)] >> slide_table[s]) & 0x7F;
  const auto effect = hi_file_effect_table[s.rank()][index];
  return s.file() < File::_8
             ? Bitboard(effect << Square(s.file(), Rank::_1), 0)
             : Bitboard(0, effect << Square(s.file() - File::_8, Rank::_1));
}

//! 飛車の横の利き
/*! rookRankEffect */
inline Bitboard GetHiRankEffect(const Square s, const Bitboard& occupied) {
  ASSERT_LV3(s < Square::SIZE_PLUS1);
  const auto rank = s.rank();
  const uint64_t u =
      (occupied.Extract<1>() << 6 * 9) + (occupied.Extract<0>() >> 9);
  uint64_t index = PEXT64(
      u, 0b1000000001000000001000000001000000001000000001000000001 << rank);
  return hi_rank_effect_table[s.file()][index] << rank;
}
}  // namespace detail

//! 他の駒を考慮した角の利き
/*! bishopEffect */
inline Bitboard GetKaEffect(const Square s, const Bitboard& occupied) {
  return detail::GetKaDiagonalEffect<0>(s, occupied) |
         detail::GetKaDiagonalEffect<1>(s, occupied);
}
//! 他の駒を考慮した馬の利き
/*! horseEffect */
inline Bitboard GetUmEffect(const Square s, const Bitboard& occupied) {
  return GetKaEffect(s, occupied) | GetOuEffect(s);
}

//! 他の駒を考慮した飛の利き
/*! rookEffect */
inline Bitboard GetHiEffect(const Square s, const Bitboard& occupied) {
  return detail::GetHiFileEffect(s, occupied) |
         detail::GetHiRankEffect(s, occupied);
}
//! 他の駒を考慮した龍の利き
/*! dragonEffect */
inline Bitboard GetRyEffect(const Square s, const Bitboard& occupied) {
  return GetHiEffect(s, occupied) | GetOuEffect(s);
}

//! 他の駒を考慮した香の利き
/*! lanceEffect */
inline Bitboard GetKyEffect(const Color c, const Square s,
                            const Bitboard& occupied) {
  return detail::GetHiFileEffect(s, occupied) & detail::GetKyNaiveEffect(c, s);
}

//! sに駒pcを置いたときの利き
/*! pcは先後の区別あり
 *  pc==QUEENの場合は馬+龍の利きを返す
    effects_from */
Bitboard GetEffect(const Piece pc, const Square s, const Bitboard& occupied);

namespace detail {
template <Piece::Value P, Color::Value C>
struct Effect {
  FORCE_INLINE static const Bitboard& get(const Square s) {
    // デフォルトで金の利き
    ASSERT_LV3(Color(C).ok() && s < Square::SIZE_PLUS1);
    return ki_effect_bb[s][C];
  }
};

template <Color::Value C>
struct Effect<Piece::FU, C> {
  FORCE_INLINE static const Bitboard& get(const Square s) {
    ASSERT_LV3(Color(C).ok() && s < Square::SIZE_PLUS1);
    return fu_effect_bb[s][C];
  }
};

template <Color::Value C>
struct Effect<Piece::KY, C> {
  FORCE_INLINE static Bitboard get(const Square s, const Bitboard& occupied) {
    ASSERT_LV3(Color(C).ok() && s < Square::SIZE_PLUS1);
    return detail::GetHiFileEffect(s, occupied) &
           detail::GetKyNaiveEffect<C>(s);
  }
};

template <Color::Value C>
struct Effect<Piece::KE, C> {
  FORCE_INLINE static const Bitboard& get(const Square s) {
    ASSERT_LV3(Color(C).ok() && s < Square::SIZE_PLUS1);
    return ke_effect_bb[s][C];
  }
};

template <Color::Value C>
struct Effect<Piece::GI, C> {
  FORCE_INLINE static const Bitboard& get(const Square s) {
    ASSERT_LV3(Color(C).ok() && s < Square::SIZE_PLUS1);
    return gi_effect_bb[s][C];
  }
};

/**
 * @brief 先後で動きが同じかを判定
 * @param piece
 * @return
 */
constexpr bool IsSymmetry(const Piece piece) {
  return piece == Piece::KA || piece == Piece::HI || piece == Piece::OU ||
         piece == Piece::UM || piece == Piece::RY;
}
/**
 * @brief 先後で動きが同じ駒の特殊化をまとめる
 */
template <Piece::Value P, Color::Value C>
struct ColorProxy {
  static constexpr Color::Value value = IsSymmetry(P) ? Color::SIZE : C;
};

template <>
struct Effect<Piece::OU, Color::SIZE> {
  FORCE_INLINE static const Bitboard& get(const Square s) {
    ASSERT_LV3(s < Square::SIZE_PLUS1);
    return detail::ou_effect_bb[s];
  }
};

template <>
struct Effect<Piece::KA, Color::SIZE> {
  FORCE_INLINE static Bitboard get(const Square s, const Bitboard& occupied) {
    return GetKaDiagonalEffect<0>(s, occupied) |
           GetKaDiagonalEffect<1>(s, occupied);
  }
};
template <>
struct Effect<Piece::UM, Color::SIZE> {
  FORCE_INLINE static Bitboard get(const Square s, const Bitboard& occupied) {
    return Effect<Piece::KA, Color::SIZE>::get(s, occupied) | GetOuEffect(s);
  }
};

template <>
struct Effect<Piece::HI, Color::SIZE> {
  FORCE_INLINE static Bitboard get(const Square s, const Bitboard& occupied) {
    return GetHiFileEffect(s, occupied) | GetHiRankEffect(s, occupied);
  }
};
template <>
struct Effect<Piece::RY, Color::SIZE> {
  FORCE_INLINE static Bitboard get(const Square s, const Bitboard& occupied) {
    return Effect<Piece::HI, Color::SIZE>::get(s, occupied) | GetOuEffect(s);
  }
};
}  // namespace detail

template <Piece::Value P, Color::Value C>
inline const Bitboard& GetEffect(const Square s) {
  return detail::Effect<P, detail::ColorProxy<P, C>::value>::get(s);
}
template <Piece::Value P, Color::Value C>
inline Bitboard GetEffect(const Square s, const Bitboard& occupied) {
  return detail::Effect<P, detail::ColorProxy<P, C>::value>::get(s, occupied);
}

//! 2bit以上あるか
/*! 縦横斜めの直線上に駒が2枚以上あるかを判定する
 *  直線以外での呼び出しは不正
    more_than_one */
inline bool IsMultiple(const Bitboard& bb) {
  ASSERT_LV2(!bb.CrossOver());
  return POPCNT64(bb.Merge()) > 1;
}
}  // namespace state_action
#endif  // !EFFECT_H_INCLUDED
