#include "stdafx.h"

#include "position.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/enum_cast.h"
#include "zobrist_hash.h"

namespace state_action {
void* Rollback::operator new(std::size_t size) {
  return aligned_malloc(size, alignof(Rollback));
}
void Rollback::operator delete(void* p) noexcept { aligned_free(p); }

const std::string Position::sfen_hirate(
    "lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1");

void Position::Initialize() { zobrist::Initialize(); }

constexpr uint8_t char_table[128] = {
    // clang-format off
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 16
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 32
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 48
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 64
 // @, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O,
    0, 0, 5, 0, 0, 0, 0, 7, 0, 0, 0, 8, 2, 0, 3, 0,  // 80
 // P, Q, R, S, T, U, V, W, X, Y, Z, [,  , ], ^, _,
    1, 0, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // 96
 // `, a,  b, c, d, e, f,  g, h, i, j,  k,  l, m,  n, o,
    0, 0, 21, 0, 0, 0, 0, 23, 0, 0, 0, 24, 18, 0, 19, 0,  // 112
 //  p, q,  r,  s, t, u, v, w, x, y, z, {, |, }, ~, 
    17, 0, 22, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 // 128
    // clang-format on
};

void Position::Set(const std::string& sfen, Rollback* rb,
                   search::Thread* thread) {
  std::memset(this, 0, sizeof(Position));
  std::memset(rb, 0, sizeof(Rollback));
#ifndef FOR_TOURNAMENT
  // 駒落ちに対応させるために初期値を変更する
  evaluation_list_.clear();
#endif // !FOR_TOURNAMENT

  rb_ = rb;

  std::istringstream iss(sfen);
  // 盤面と手番の区切り文字として空白を利用するので、スキップしないように設定
  iss >> std::noskipws;

  uint8_t token;
  bool promotion = false;
  int index;

  // それぞれの駒で駒番号をどこまで使ったかを管理する
  std::array<PieceNumber, Piece::OU> pn_count = {
      PieceNumber::ZERO, PieceNumber::FU, PieceNumber::KY, PieceNumber::KE,
      PieceNumber::GI,   PieceNumber::KA, PieceNumber::HI, PieceNumber::KI};
  location_.ResetOuSquare();

  File f = File::_9;
  Rank r = Rank::_1;
  while ((iss >> token) && !std::isspace(token)) {
    if (std::isdigit(token)) {
      // 空白の個数だけ右に移動
      f = f - (token - '0');
    } else if (token == '/') {
      // 次の段へ移動
      f = File::_9;
      ++r;
    } else if (token == '+') {
      // 成りのフラグ
      promotion = true;
    } else if ((index = char_table[token]) != 0) {
      const Square square(f, r);
      const Piece piece(index +
                        (promotion ? enum_cast<>(Piece::PROMOTION) : 0));
      location_.Add(square, piece);

      const Piece tmp(index);
      // clang-format off
      const PieceNumber piece_number = 
        tmp == Piece::BLACK_OU ? PieceNumber::BLACK_OU :
        tmp == Piece::WHITE_OU ? PieceNumber::WHITE_OU : pn_count[tmp.raw_type()]++;
      // clang-format on
      evaluation_list_.Update(piece_number, square, piece);

      // 一つ右へ移動
      --f;

      promotion = false;
    }
  }

  location_.Update();
  location_.UpdateOuSquare();

  // 手番
  iss >> token;
  side_ = token == 'w' ? Color::WHITE : Color::BLACK;

  // 手番と持ち駒の間の空白
  iss >> token;

  // 持ち駒
  location_.ResetHand();
  int count = 0;
  while ((iss >> token) && !std::isspace(token)) {
    if (token == '-') {
      // 持ち駒はない
      break;
    } else if (std::isdigit(token)) {
      // 駒の枚数
      // 歩は2桁の場合がある
      count = (token - '0') + count * 10;
    } else if ((index = char_table[token]) != 0) {
      // 個数が省略されている場合は1
      count = std::max(count, 1);
      const Piece piece(index);
      location_.Add(piece.color(), piece.raw_type(), count);

      for (int i = 0; i < count; ++i) {
        const Piece piece_raw = piece.raw_type();
        const PieceNumber piece_number = pn_count[piece_raw]++;
        ASSERT_LV1(piece_number.ok());
        evaluation_list_.Update(piece_number, piece.color(), piece_raw, i);
      }

      count = 0;
    }
  }

  // 手数
  iss >> std::skipws >> game_ply_;

  InitializeState(rb_);

#ifdef LONG_EFFECT_LIBRARY
  // 長い利きをセット
#endif // LONG_EFFECT_LIBRARY

  rb_->material_value = evaluation::InitializeMaterial(*this);
  evaluation::ComputeEvaluation(*this);

  ptr_thread_ = thread;
}

std::string Position::sfen() const {
  std::ostringstream oss;

  // 盤面
  int empty_count;
  for (Rank r = Rank::_1; r <= Rank::_9; ++r) {
    for (File f = File::_9; f >= File::_1; --f) {
      for (empty_count = 0;
           f >= File::_1 && location_[Square(f, r)] == Piece::EMPTY; --f) {
        ++empty_count;
      }

      // 駒のなかったマスの個数を出力
      if (empty_count) {
        oss << empty_count;
      }

      // 駒があるならそれを出力
      if (f >= File::_1) {
        oss << location_[Square(f, r)];
      }
    }
    // 段ごとの区切り
    if (r < Rank::_9) {
      oss << '/';
    }
  }

  // 手番
  oss << (side() == Color::WHITE ? " w " : " b ");
  // 持ち駒
  bool found = false;
  for (const auto c : COLOR) {
    for (int pn = 0; pn < 7; ++pn) {
      // 駒の順序をYaneuraOuと同じで飛、角、金、銀、桂、香、歩にする

      constexpr Piece usi_hand[7] = {Piece::HI, Piece::KA, Piece::KI, Piece::GI,
                                     Piece::KE, Piece::KY, Piece::FU};
      const auto p = usi_hand[pn];

      const int n = location_.hand(c).count(p);
      if (n) {
        // 持ち駒が一つでもあった
        found = true;

        if (n != 1) {
          // 1枚以上なら枚数を出力
          oss << n;
        }
        oss << piece_to_char_bw[Piece(c, p)];
      }
    }
  }
  if (!found) {
    oss << '-';
  }

  // 手数
  oss << ' ' << game_ply();

  return oss.str();
}

void Position::InitializeState(Rollback* rb) const {
  ASSERT_LV1(rb);

  // 自玉に王手をかけている相手の駒
  rb->checkers_bb = attacker(~side_, location_.ou_square(side_));

  UpdateCheckState<false>(rb);

  rb->board_key_ = side_ == Color::BLACK ? zobrist::zero : zobrist::side;
  rb->hand_key_ = zobrist::zero;
  for (const Square square : SQUARE) {
    const Piece piece = location_[square];
    rb->board_key_ += zobrist::psq[square][piece];
  }
  for (const Color color : COLOR) {
    for (Piece piece = Piece::FU; piece < Piece::HAND_SIZE; ++piece) {
      rb->hand_key_ += zobrist::hand[color][piece] *
                       static_cast<int64_t>(location_.hand(color).count(piece));
    }
  }

  rb->hand = location_.hand(side_);
}

RepetitionState Position::IsRepetition(const int rep_ply) const {
  // rep_ply手遡って現在と同じhashの局面があれば、千日手と判定する
  // rootから遡るなら、root以外に2回同じ局面が出現すると千日手とする
  // rootから遡らないなら、現局面と同じhashの局面が出現した時点で千日手とする
  // cf. Don't score as an immediate draw 2-fold repetitions of the root
  // position
  //   https://github.com/official-stockfish/Stockfish/commit/6d89d0b64a99003576d3e0ed616b43333c9eca01
  // 本当に千日手になっているか4回の出現をチェックする必要はないらしい

  // 初期化が必要
  ASSERT_LV3(rb_->plies_from_null >= 0);

  // 遡られる最大手数
  const int n = std::min(rep_ply, rb_->plies_from_null);
  // どんなに少なくても4手は遡らないと循環は発生しない
  if (n < 4) {
    return RepetitionState::NONE;
  }

  const Rollback* p = rb_->previous->previous;
  for (int i = 4; i <= n; i += 2) {
    p = p->previous->previous;

    if (p->board_key() != rb_->board_key()) {
      continue;
    }

    if (p->hand == rb_->hand) {
      if (i <= rb_->continuous_check[side_]) {
        // 自分が王手をかけている千日手
        return RepetitionState::LOSE;
      } else if (i <= rb_->continuous_check[~side_]) {
        // 相手が王手をかけている千日手
        return RepetitionState::WIN;
      } else {
        return RepetitionState::DRAW;
      }
    } else {
      if (p->hand >= rb_->hand) {
        // 以前の方が持ち駒が多かった
        return RepetitionState::INFERIOR;
      } else if (p->hand <= rb_->hand) {
        // 今の方が持ち駒が多い
        return RepetitionState::SUPERIOR;
      }
    }
  }
  return RepetitionState::NONE;
}

Bitboard Position::empties() const { return types() ^ Bitboard::All; }

bool Position::IsLegalFuDropEx(const Square target) const {
  // 1. 二歩でない
  // 2. 歩の正面に敵玉がない、かつ、打ち歩詰めでない
  return !(location_.get<Piece::FU>(side_) & FILE_BB[target.file()]) &&
         ((GetFuEffect(side_, target) !=
               Bitboard(location_.ou_square(~side_)) ||
           IsLegalFuDrop(target)));
}

std::ostream& operator<<(std::ostream& os, const Position& position) {
  for (Rank r = Rank::_1; r <= Rank::_9; ++r) {
    for (File f = File::_9; f >= File::_1; --f) {
      os << position[Square(f, r)].pretty();
    }
    os << std::endl;
  }
  os << boost::format("Black hand: %1%, White hand: %2%") %
            position[Color::BLACK] % position[Color::WHITE]
     << std::endl;
  os << "turn: " << position.side() << std::endl;

  os << "sfen: " << position.sfen() << std::endl;

  return os;
}
}  // namespace state_action
