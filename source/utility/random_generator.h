#pragma once
#ifndef RANDOM_GENERATOR_H_INCLUDED
#define RANDOM_GENERATOR_H_INCLUDED

#include <chrono>
#include <cstdint>
#include <ctime>
#include <iosfwd>
#include <mutex>

#include "level_assert.h"

namespace utility {
/**
 * @brief 疑似乱数の生成器
 *
 * PRNG
 */
class PseudoRandom {
 public:
  PseudoRandom() : s(Initialize()) {}
  PseudoRandom(const uint64_t seed) : s(seed) { ASSERT_LV1(seed); }

  /**
   * @brief 乱数を一つ取り出す
   * @tparam T
   * @return
   * rand
   */
  template <typename T>
  T rand() {
    return static_cast<T>(Randamize());
  }

  /**
   * @brief 0からn-1までの乱数を取得する
   *
   * 一様分布ではないが、実用的には十分
   * @param n
   * @return
   * rand
   */
  uint64_t rand(const uint64_t n) { return Randamize() & n; }

  /**
   * @brief 現在の内部状態を返す
   * @return
   * get_seed
   */
  uint64_t get_seed() const { return s; }

 private:
  uint64_t s;

  uint64_t Randamize() {
    s ^= s >> 12;
    s ^= s << 25;
    s ^= s >> 27;
    return s * 2685821657736338717LL;
  }

  uint64_t Initialize() noexcept {
    // C++11のrandom_deviceがmsys2のgccで上手く動作しないので、
    // 時間などで初期化する
    return static_cast<uint64_t>(std::time(nullptr)) +
           (reinterpret_cast<int64_t>(this) << 32) +
           static_cast<uint64_t>(std::chrono::high_resolution_clock::now()
                                     .time_since_epoch()
                                     .count());
  }
};

extern PseudoRandom prng;

std::ostream& operator<<(std::ostream& os, const PseudoRandom& generator);

struct AsyncPseudoRandom {
  template <typename T>
  T rand() {
    std::unique_lock<std::mutex> lock(mutex);
    return generator.rand<T>();
  }

  uint64_t rand(const uint64_t n) noexcept {
    std::unique_lock<std::mutex> lock(mutex);
    return generator.rand(n);
  }

  uint64_t get_seed() const noexcept { return generator.get_seed(); }

 protected:
  std::mutex mutex;
  PseudoRandom generator;
};
}  // namespace utility
#endif  // !RANDOM_GENERATOR_H_INCLUDED
