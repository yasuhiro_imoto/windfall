#pragma once
#ifndef TIME_POINT_H_INCLUDED
#define TIME_POINT_H_INCLUDED
#include <chrono>
#include <cstdint>

#include "../rule/color.h"
#include "configuration.h"

namespace search {
struct SearchLimit;
}

namespace utility {
//! ms単位で時間を計測
/*! 時間の計測の単位はこれだけ */
using TimePoint = std::chrono::milliseconds::rep;
static_assert(sizeof(TimePoint) == sizeof(int64_t),
              "The size of TimePoint must be 64bits.");

//! ms単位で現在時刻を返す
inline TimePoint now() {
  return std::chrono::duration_cast<std::chrono::milliseconds>(
             std::chrono::steady_clock::now().time_since_epoch())
      .count();
}

class Timer {
 public:
  /**
   * @brief タイマーを初期化
   *
   * reset
   */
  void Reset() { start_time_ = start_time_ponder_hit_ = now(); }

  /**
   * @brief "ponderhit"からの時刻を計測する用に初期化
   *
   * reset_for_ponderhit
   */
  void ResetPonderHit() { start_time_ponder_hit_ = now(); }

  /**
   * @brief 探索開始からの経過時間(ms単位)
   *
   * ノード数制限で探索を行う場合は、時間の代わりに探索ノード数を返す
   * @return
   * elapsed
   */
  TimePoint elapsed() const;

  TimePoint elapsed_ponder_hit() const;

#ifdef USE_TIME_MANAGEMENT
  /**
   * @brief optimumなどが正しくなるように状態を更新する
   *
   * 毎回の探索のための適正な消費時間を計算する
   * @param limits
   * @param us
   * @param ply 現在のの手数
   *            初期局面なら1
   * init
   */
  void Update(search::SearchLimit& limits, const Color us, const int ply);

  TimePoint minimum() const { return minimum_time_; }
  TimePoint optimum() const { return optimum_time_; }
  TimePoint maximum() const { return maximum_time_; }

  /**
   * @brief 1秒単位に切り上げてからdelayを引く
   *
   * remaining_time_よりは短くなるように制限される
   * @param t
   * @return
   * round_up
   */
  TimePoint RoundUp(const TimePoint t) const;

  /**
   * @brief 探索終了時刻
   *
   * start_time_ + duration >= now()なら停止
   * search_end
   */
  TimePoint duration;
#endif  // USE_TIME_MANAGEMENT
 private:
  /**
   * @brief 探索開始時刻
   * startTime
   */
  TimePoint start_time_;
  /**
   * @brief
   * startTimeFromPonderhit
   */
  TimePoint start_time_ponder_hit_;

#ifdef USE_TIME_MANAGEMENT
  //! minimumTime
  TimePoint minimum_time_;
  //! optimumTime
  TimePoint optimum_time_;
  //! maximumTime
  TimePoint maximum_time_;

  /**
   * @brief options["NetworkDelay"]の値
   * network_delay
   */
  TimePoint network_delay_;
  /**
   * @brief options["MinimalThinkingTime"]の値
   * minimum_thinking_time
   */
  TimePoint minimum_thinking_time_;

  /**
   * @brief 今回の残り時間 - options["NetworkDelay2"]
   * remain_time
   */
  TimePoint remaining_time_;
#endif  // USE_TIME_MANAGEMENT
};

extern Timer timer;
}  // namespace utility
#endif  // !TIME_POINT_H_INCLUDED
