#pragma once

#include <cstdint>

#include "configuration.h"

#include "force_inline.h"
#include "level_assert.h"

#if defined(USE_AVX2)
#include <immintrin.h>
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)

#if defined(USE_AVX2)
#define PEXT32(a, b) \
  _pext_u32(static_cast<uint32_t>(a), static_cast<uint32_t>(b))
#define PEXT64(a, b) _pext_u64(a, b)

#define POPCNT32(a) _mm_popcnt_u32(a)
#define POPCNT64(a) _mm_popcnt_u64(a)
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)

// ----------------------------
//     BSF(bitscan forward)
// ----------------------------
#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#include <intrin.h>

//! 1である最下位のbitのbit位置を得る。0を渡してはならない。
FORCE_INLINE int LSB32(uint32_t v) {
  ASSERT_LV3(v != 0);
  unsigned long index;
  _BitScanForward(&index, v);
  return index;
}
//! 1である最下位のbitのbit位置を得る。0を渡してはならない。
FORCE_INLINE int LSB64(uint64_t v) {
  ASSERT_LV3(v != 0);
  unsigned long index;
  _BitScanForward64(&index, v);
  return index;
}

//! 1である最上位のbitのbit位置を得る。0を渡してはならない。
FORCE_INLINE int MSB32(uint32_t v) {
  ASSERT_LV3(v != 0);
  unsigned long index;
  _BitScanReverse(&index, v);
  return index;
}
//! 1である最上位のbitのbit位置を得る。0を渡してはならない。
FORCE_INLINE int MSB64(uint64_t v) {
  ASSERT_LV3(v != 0);
  unsigned long index;
  _BitScanReverse64(&index, v);
  return index;
}

#elif defined(__GNUC__) && \
    (defined(__i386__) || defined(__x86_64__) || defined(__ANDROID__))

FORCE_INLINE int LSB32(const uint32_t v) {
  ASSERT_LV3(v != 0);
  return __builtin_ctzll(v);
}
FORCE_INLINE int LSB64(const uint64_t v) {
  ASSERT_LV3(v != 0);
  return __builtin_ctzll(v);
}
FORCE_INLINE int MSB32(const uint32_t v) {
  ASSERT_LV3(v != 0);
  return 63 ^ __builtin_clzll(v);
}
FORCE_INLINE int MSB64(const uint64_t v) {
  ASSERT_LV3(v != 0);
  return 63 ^ __builtin_clzll(v);
}

#endif  // defined(_MSC_VER) && !defined(__INTEL_COMPILER)

// ----------------------------
//  ymm(256bit register class)
// ----------------------------

#ifdef USE_AVX2
// Byteboardの直列化で使うAVX2命令
struct alignas(32) ymm {
  union {
    __m256i m;
    uint64_t _u64[4];
    uint32_t _u32[8];
    // typedef名と同じ変数名にするとg++で警告が出るようだ。
  };

  ymm(const __m256i m_) : m(_mm256_loadu_si256((__m256i*)(&m_))) {}
  ymm operator=(const __m256i& m_) {
    this->m = _mm256_loadu_si256((__m256i*)(&m_));
    return *this;
  }

  // アライメント揃っていないところからの読み込みに対応させるためにloadではなくloaduのほうを用いる。
  ymm(const void* p) : m(_mm256_loadu_si256((__m256i*)p)) {}
#pragma warning(push)
#pragma warning(disable : 26495)
  ymm(const uint8_t t) {
    for (int i = 0; i < 32; ++i) (reinterpret_cast<uint8_t*>(this))[i] = t;
  }
  //      /*  m.m256i_u8[i] = t; // これだとg++対応できない。 */

  ymm() {}
#pragma warning(pop)

  // MSBが1なら1にする
  uint32_t to_uint32() const { return _mm256_movemask_epi8(m); }

  ymm& operator|=(const ymm& b1) {
    m = _mm256_or_si256(m, b1.m);
    return *this;
  }
  ymm& operator&=(const ymm& b1) {
    m = _mm256_and_si256(m, b1.m);
    return *this;
  }
  ymm operator&(const ymm& rhs) const { return ymm(*this) &= rhs; }
  ymm operator|(const ymm& rhs) const { return ymm(*this) |= rhs; }

  // packed
  // byte単位で符号つき比較してthisのほうが大きければMSBを1にする。(このあとto_uint32()で直列化するだとか)
  // ※　AVX2には符号つき比較する命令しかない…。
  ymm cmp(const ymm& rhs) const {
    ymm t;
    t.m = _mm256_cmpgt_epi8(m, rhs.m);
    return t;
  }

  // packed
  // byte単位で比較して、等しいなら0xffにする。(このあとto_uint32()で直列化するだとか)
  // ※　AVXにはnot equal命令がない…。
  ymm eq(const ymm& rhs) const {
    ymm t;
    t.m = _mm256_cmpeq_epi8(m, rhs.m);
    return t;
  }
};
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // USE_AVX2

extern ymm ymm_zero;  // all packed bytes are 0.
extern ymm ymm_one;   // all packed bytes are 1.

// ----------------------------
//    custom allocator
// ----------------------------

extern void* aligned_malloc(const size_t size, const size_t align);
static void aligned_free(void* ptr) { _mm_free(ptr); }

// alignasを指定しているのにnewのときに無視される＆STLのコンテナがメモリ確保するときに無視するので、
// そのために用いるカスタムアロケーター。
template <typename T>
class AlignedAllocator {
 public:
  using value_type = T;

  AlignedAllocator() {}
  AlignedAllocator(const AlignedAllocator&) {}
  AlignedAllocator(AlignedAllocator&&) {}

  template <typename U>
  AlignedAllocator(const AlignedAllocator<U>&) {}

  T* allocate(std::size_t n) {
    return (T*)aligned_malloc(n * sizeof(T), alignof(T));
  }
  void deallocate(T* p, std::size_t n) { aligned_free(p); }
};

// ----------------------------
//    BSLR
// ----------------------------

// 最下位bitをresetする命令。

#if (defined(USE_AVX2) && defined(IS_64BIT))
#define BLSR(x) _blsr_u64(x)
#else
#define BLSR(x) (x & (x - 1))
#endif

// ----------------------------
//    pop_lsb
// ----------------------------

// 1である最下位bitを1bit取り出して、そのbit位置を返す。0を渡してはならない。
// sizeof(T)<=4 なら
// LSB32(b)で済むのだが、これをコンパイル時に評価させるの、どう書いていいのかわからん…。
// デフォルトでLSB32()を呼ぶようにしてuint64_tのときだけ64bit版を用意しておく。

template <typename T>
FORCE_INLINE int pop_lsb(T& b) {
  int index = LSB32(b);
  b = T(BLSR(b));
  return index;
}
FORCE_INLINE int pop_lsb(uint64_t& b) {
  int index = LSB64(b);
  b = BLSR(b);
  return index;
}
