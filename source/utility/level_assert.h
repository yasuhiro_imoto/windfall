#pragma once
#ifndef LEVEL_ASSERT_H_INCLUDED
#define LEVEL_ASSERT_H_INCLUDED

#ifdef NDEBUG
// 一旦取り消す
#undef NDEBUG
// 取り消したことを記録
#define NDEBUG_DEFINED
#endif // NDEBUG

#include <cassert>

#include <boost/current_function.hpp>

#pragma warning(push)
#pragma warning(disable : 6387 26439 26451 26495 26812 28182)
#include <spdlog/fmt/fmt.h>
#pragma warning(pop)

#ifndef ASSERT_LV
#define ASSERT_LV 0
#endif // !ASSERT_LV

#define ASSERT_LV_EXP(L, X) ((L <= ASSERT_LV) ? assert(X) : ((void)0))
#define ASSERT_LV1(X) ASSERT_LV_EXP(1, X)
#define ASSERT_LV2(X) ASSERT_LV_EXP(2, X)
#define ASSERT_LV3(X) ASSERT_LV_EXP(3, X)
#define ASSERT_LV4(X) ASSERT_LV_EXP(4, X)
#define ASSERT_LV5(X) ASSERT_LV_EXP(5, X)

namespace boost {
void assertion_failed_msg(char const* expr, char const* msg,
                          char const* function, char const* file,
                          long line);  // user defined
}  // namespace boost

#define ASSERT_MSG_LV_EXP(L, X, FMT, ...)                                    \
  do {                                                                       \
    if constexpr (L > ASSERT_LV) {                                           \
      break;                                                                 \
    }                                                                        \
    if (!(X)) {                                                              \
      const std::string msg = fmt::format(FMT, __VA_ARGS__);                 \
      ::boost::assertion_failed_msg(#X, msg.c_str(), BOOST_CURRENT_FUNCTION, \
                                    __FILE__, __LINE__);                     \
      ::exit(1);                                                             \
    }                                                                        \
  } while (0)
#define ASSERT_MSG_LV1(X, FMT, ...) ASSERT_MSG_LV_EXP(1, X, FMT, __VA_ARGS__)
#define ASSERT_MSG_LV2(X, FMT, ...) ASSERT_MSG_LV_EXP(2, X, FMT, __VA_ARGS__)
#define ASSERT_MSG_LV3(X, FMT, ...) ASSERT_MSG_LV_EXP(3, X, FMT, __VA_ARGS__)
#define ASSERT_MSG_LV4(X, FMT, ...) ASSERT_MSG_LV_EXP(4, X, FMT, __VA_ARGS__)
#define ASSERT_MSG_LV5(X, FMT, ...) ASSERT_MSG_LV_EXP(5, X, FMT, __VA_ARGS__)

#ifdef NDEBUG_DEFINED
#define NDEBUG
// フラグを削除
#undef NDEBUG_DEFINED
#endif // NDEBUG_DEFINED

// --- declaration of unreachablity
// switchにおいてdefaultに到達しないことを明示して高速化させる
// デバッグ時は普通にしとかないと変なアドレスにジャンプして原因究明に時間がかかる。
#if defined(_MSC_VER)
#define UNREACHABLE    \
  do {                 \
    ASSERT_LV3(false); \
    __assume(0);       \
  } while (false)
#elif defined(__GNUC__)
#define UNREACHABLE          \
  do {                       \
    ASSERT_LV3(false);       \
    __builtin_unreachable(); \
  } while (false)
#else
#define UNREACHABLE    \
  do {                 \
    ASSERT_LV3(false); \
  } while (false)
#endif

#endif  // !LEVEL_ASSERT_H_INCLUDED

