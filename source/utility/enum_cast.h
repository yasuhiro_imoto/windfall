#pragma once
#ifndef ENUM_CAST_H_INCLUDED
#define ENUM_CAST_H_INCLUDED

#include <type_traits>

template <typename T>
constexpr std::underlying_type_t<T> enum_cast(const T t) {
  return static_cast<std::underlying_type_t<T>>(t);
}
#endif  // !ENUM_CAST_H_INCLUDED
