#pragma once

#define USE_EVAL_HASH
#define USE_SEE
#define USE_MATE_1PLY
#define USE_ENTERING_KING_WIN
#define USE_TIME_MANAGEMENT
#define KEEP_PIECE_IN_GENERATED_MOVE
#define ONE_PLY_EQ_1

#define HASH_KEY_BITS 64

#define NNUE_VANILLA

//! 通常探索における最大の探索深さ
constexpr int max_ply = 127;

// ターゲットが64bitOSかどうか
#if (defined(_WIN64) && defined(_MSC_VER)) || \
    (defined(__GNUC__) && defined(__x86_64__)) || defined(IS_64BIT)
constexpr bool Is64Bit = true;
#ifndef IS_64BIT
#define IS_64BIT
#endif
#else
constexpr bool Is64Bit = false;
#endif
