#include "stdafx.h"

#include "tool.h"

#include "../search/process_group.h"
#include "../usi/option.h"
#include "logger.h"

namespace utility {
void ClearTable(const char* name, void* table, const size_t size_mb) {
  logger->info("{} Start clearing, Hash size = {} MB.", name,
               size_mb / (1024 * 1024));

  std::vector<std::thread> threads;
  const size_t n = usi::options["Threads"];
  for (size_t i = 0; i < n; ++i) {
    threads.push_back(std::thread([table, size_mb, n, i]() {
      // NUMA環境ではBindThisThreadを呼び出しておいた方が速くなるらしい
      if (usi::options["Threads"] > 8) {
        search::windows::BindThisThread(i);
      }

      const size_t stride = size_mb / n;
      const size_t start = stride * i;
      const size_t len = i != n - 1 ? stride : size_mb - start;
      std::memset(reinterpret_cast<uint8_t*>(table) + start, 0, len);
    }));
  }

  for (std::thread& t : threads) {
    t.join();
  }

  logger->info("{} Finished clearing, Hash size = {} MB.", name,
               size_mb / (1024 * 1024));
}

void Sleep(const int ms) {
  std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void Exit() {
  Sleep(3000);
  exit(EXIT_FAILURE);
}
}  // namespace utility
