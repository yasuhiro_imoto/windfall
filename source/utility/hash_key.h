#pragma once

#include <cstdint>

#if defined(USE_AVX2)
#include <immintrin.h>
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)

#include "configuration.h"

using Key = uint64_t;

// 置換表で用いるためにPositionクラスから得られるhash
// keyを64bit版の他に128,256bitに変更することが出来るのでそのための構造体。

#if HASH_KEY_BITS <= 64
// 64bit
using Key64 = uint64_t;
#elif HASH_KEY_BITS == 128
// 128bit版
struct alignas(16) Key128 {
  union {
    __m128i m;
    uint64_t _u64[2];
  };

  Key128() {}
  Key128(const Key128& bb) { _mm_store_si128(&this->m, bb.m); }
  Key128& operator=(const Key128& rhs) {
    _mm_store_si128(&this->m, rhs.m);
    return *this;
  }

  void set(Key k0, Key k1) {
    _u64[0] = k0;
    _u64[1] = k1;
  }
  operator Key() const { return _u64[0]; }

  uint64_t p(int i) const { return _u64[i]; }

  Key128& operator+=(const Key128& b1) {
    this->m = _mm_add_epi64(m, b1.m);
    return *this;
  }
  Key128& operator-=(const Key128& b1) {
    this->m = _mm_sub_epi64(m, b1.m);
    return *this;
  }
  Key128& operator^=(const Key128& b1) {
    this->m = _mm_xor_si128(m, b1.m);
    return *this;
  }

  Key128& operator*=(int64_t i) {
    _u64[0] *= i;
    _u64[1] *= i;
    return *this;
  }
  bool operator==(const Key128& rhs) const {
    return (_mm_testc_si128(_mm_cmpeq_epi8(this->m, rhs.m),
                            _mm_set1_epi8(static_cast<char>(0xFFu)))
                ? true
                : false);
  }
  bool operator!=(const Key128& rhs) const { return !(*this == rhs); }

  Key128 operator*(const int64_t i) const { return Key128(*this) *= i; }

  Key128 operator+(const Key128& rhs) const { return Key128(*this) += rhs; }
  Key128 operator^(const Key128& rhs) const { return Key128(*this) ^= rhs; }
};
#else
// 256bit版
struct alignas(32) Key256 {
  union {
    __m256i m;
    uint64_t _u64[4];
  };

  Key256() {}
  Key256(const Key256& bb) { _mm256_store_si256(&this->m, bb.m); }
  Key256& operator=(const Key256& rhs) {
    _mm256_store_si256(&this->m, rhs.m);
    return *this;
  }

  void set(Key k0, Key k1, Key k2, Key k3) {
    _u64[0] = k0;
    _u64[1] = k1;
    _u64[2] = k2;
    _u64[3] = k3;
  }
  operator Key() const { return _u64[0]; }

  uint64_t p(int i) const { return _u64[i]; }

  Key256& operator+=(const Key256& b1) {
    this->m = _mm256_add_epi64(m, b1.m);
    return *this;
  }
  Key256& operator-=(const Key256& b1) {
    this->m = _mm256_sub_epi64(m, b1.m);
    return *this;
  }
  Key256& operator^=(const Key256& b1) {
    this->m = _mm256_xor_si256(m, b1.m);
    return *this;
  }

  Key256& operator*=(int64_t i) {
    _u64[0] *= i;
    _u64[1] *= i;
    _u64[2] *= i;
    _u64[3] *= i;
    return *this;
  }
  bool operator==(const Key256& rhs) const {
    return (_mm256_testc_si256(_mm256_cmpeq_epi8(this->m, rhs.m),
                               _mm256_set1_epi8(static_cast<char>(0xFFu)))
                ? true
                : false);
  }
  bool operator!=(const Key256& rhs) const { return !(*this == rhs); }

  Key256 operator*(const int64_t i) const { return Key256(*this) *= i; }

  Key256 operator+(const Key256& rhs) const { return Key256(*this) += rhs; }
  Key256 operator^(const Key256& rhs) const { return Key256(*this) ^= rhs; }
};
#endif  // HASH_KEY_BITS==128

#if HASH_KEY_BITS <= 64
using HashKey = Key64;
#elif HASH_KEY_BITS <= 128
using HashKey = Key128;
#else
using HashKey = Key256;
#endif
