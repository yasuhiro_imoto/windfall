#include "stdafx.h"

#include "time_point.h"

#include "../search/search_limit.h"
#include "../search/thread.h"
#include "../usi/option.h"
#include "logger.h"
#include "random_generator.h"

namespace utility {
//! 標準的な一局の手数
constexpr int move_horizon = 80;

Timer timer;

TimePoint Timer::elapsed() const {
  return TimePoint(search::limit.npm_sec ? search::thread_pool.nodes_searched()
                                         : now() - start_time_);
}

TimePoint Timer::elapsed_ponder_hit() const {
  // npm_secの場合の値が違うが、ponderで利用されることはないので問題ない
  return TimePoint(search::limit.npm_sec ? search::thread_pool.nodes_searched()
                                         : now() - start_time_ponder_hit_);
}

#ifdef USE_TIME_MANAGEMENT
void Timer::Update(search::SearchLimit& limits, const Color us, const int ply) {
  network_delay_ = usi::options["NetworkDelay"];
  // 探索の予定時間を初期化
  duration = 0;

  // 探索時間の上限
  remaining_time_ = search::limit.time[us] + search::limit.byoyomi[us] -
                    static_cast<TimePoint>(usi::options["NetworkDelay2"]);
  // 下限を0にすると時間切れの後に自滅するので、少し余裕を持たせる
  remaining_time_ = std::max<TimePoint>(remaining_time_, 100);

  minimum_thinking_time_ = usi::options["MinimumThinkingTime"];

  if (search::limit.rtime) {
    // 最小思考時間をランダム化
    // 連続自己対局で進行が同じになるのを防ぐ
    // 終盤で時間を変え過ぎると勝率が5割になる

    auto r = search::limit.rtime;
    if (ply) {
      // 指し手が進むごとに減衰させる
      r += static_cast<int>(
          prng.rand(static_cast<int>(std::min(r * 0.5f, r * 10.0f / ply))));
    }
    remaining_time_ = minimum_time_ = optimum_time_ = maximum_time_ = r;
    return;
  }

  // 秒読みで持ち時間が少ないなら持ち時間を使い切った方が得
  // その閾値は秒読みの1.2倍とする
  if (const auto c = us; search::limit.byoyomi[c] &&
                         search::limit.time[c] <
                             static_cast<int>(search::limit.byoyomi[c] * 1.2)) {
    minimum_time_ = optimum_time_ = maximum_time_ =
        search::limit.byoyomi[c] + search::limit.time[c];
  } else {
    // 残りの手数
    // 初期局面はply=1で、max_game_plyの1手前でmtgが1になるように調整
    const int mtg =
        std::min((search::limit.max_game_ply - ply + 2) / 2, move_horizon);

    if (mtg <= 0) {
      // 最大手数を超えていることになるので、ここには到達しないはず
      logger->info("info string max_game_ply is too small.");
      return;
    }
    if (mtg == 1) {
      // この手番で終了なので、時間を使い切る
      minimum_time_ = optimum_time_ = maximum_time_ = remaining_time_;
    }

    minimum_time_ =
        std::max<TimePoint>(minimum_thinking_time_ - network_delay_, 1000);
    // 一旦は上限値を設定
    optimum_time_ = maximum_time_ = remaining_time_;

    // 残りの手数での時間の合計を計算
    // 残り全てで秒読みの場合を想定
    TimePoint remaining_estimated = search::limit.time[c] +
                                    search::limit.inc[c] * mtg +
                                    search::limit.byoyomi[c] * mtg;
    // ただし、1手ごとに最低1秒は消費する
    remaining_estimated -= (mtg + 1) * 1000;

    remaining_estimated = std::max<TimePoint>(remaining_estimated, 0);

    TimePoint t1 = minimum_time_ + remaining_estimated / mtg;
    float max_ratio = 5.0f;
    if (search::limit.inc[c] == 0 && search::limit.byoyomi[c] == 0) {
      // 切れ負けの場合、5分以下なら比率を抑える
      // 3分     : ratio = 3
      // 2分     : ratio = 2
      // 1分以下 : ratio = 1で固定
      max_ratio = std::clamp(
          static_cast<float>(search::limit.time[c]) / (60 * 1000), 1.0f, 5.0f);
    }
    TimePoint t2 =
        minimum_time_ + static_cast<int>(remaining_estimated * max_ratio / mtg);
    // maximum_time_は残り時間の30%未満とする
    // optimum_time_はそれを超えても構わない
    t2 = std::min(t2, static_cast<TimePoint>(remaining_estimated * 0.3));

    const auto slow_mover = static_cast<int>(usi::options["SlowMover"]);

    optimum_time_ = std::min(t1, optimum_time_) * slow_mover / 100;
    maximum_time_ = std::min(t2, maximum_time_);

    // ponder hitがあると時間が余り気味になるので、長めに調整する
    if (usi::options["USI_Ponder"]) {
      optimum_time_ += optimum_time_ / 4;
    }
  }

  // 残り時間を超えないように補正する
  minimum_time_ = std::min(RoundUp(minimum_time_), remaining_time_);
  optimum_time_ = std::min(optimum_time_, remaining_time_);
  maximum_time_ = std::min(RoundUp(maximum_time_), remaining_time_);
}

TimePoint Timer::RoundUp(const TimePoint t) const {
  // 1000単位に繰り上げ
  // ただし、options["MinimalThinkingTime"]が下限
  const auto tmp = std::max((t + 999) / 1000 * 1000, minimum_thinking_time_);
  // 通信ディレイの分を調整
  const auto result = std::min(tmp - network_delay_, remaining_time_);
  return result;
}
#endif  // USE_TIME_MANAGEMENT

}  // namespace utility
