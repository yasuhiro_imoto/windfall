#pragma once
#ifndef TOOL_H_INCLUDED
#define TOOL_H_INCLUDED

namespace utility {
/**
 * @brief 並列処理でテーブルを0で初期化
 *
 * Windows10ではzero clearに時間がかかる
 * また、mallocを呼び出した段階ではまだ実メモリの割り当てがない
 * このタイミングで割り当てさせる
 * @param name
 * @param table
 * @param size_mb 単位はMB
 * memclear
 */
void ClearTable(const char* name, void* table, const size_t size_mb);

/**
 * @brief スリープする
 * @param ms ミリ秒
 */
void Sleep(const int ms);

/**
 * @brief 途中での終了処理のためのwrapper
 *
 * コンソールの出力が完了するのを待ちたいので3秒待ってから::exit(EXIT_FAILURE)する
 * exit
 */
void Exit();
}  // namespace utility
#endif  // !TOOL_H_INCLUDED
