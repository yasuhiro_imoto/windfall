// clang-format off
#include "stdafx.h"
// clang-format on

#include "random_generator.h"

namespace utility {
PseudoRandom prng;

std::ostream& operator<<(std::ostream& os, const PseudoRandom& generator) {
  os << "PseudoRandom::seed = " << std::hex << generator.get_seed() << std::dec;
  return os;
}
}  // namespace utility
