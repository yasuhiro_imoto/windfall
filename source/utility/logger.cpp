#include "stdafx.h"

#ifdef SEARCH_LOG
#pragma warning(push)
#pragma warning(disable : 6387 26439 26451 26495 26812 28182)
#include <spdlog/sinks/basic_file_sink.h>
#pragma warning(pop)
#endif // SEARCH_LOG

#include "logger.h"

namespace utility {
std::shared_ptr<spdlog::logger> logger;

void InitializeLogger()
{
  logger = spdlog::stdout_logger_st("console");
  // 自動でinfo stringを入れてやりたい気もするが、
  // そうでないメッセージもあるので単純にメッセージ本体をそのまま出力する
  logger->set_pattern("%v");
  logger->set_level(spdlog::level::info);

  // 初期化の時に時間のかかる読み込みの進捗の表示やkeep aliveに使う
  auto logger_multi = spdlog::stdout_logger_mt("loader_keep_alive");
  logger_multi->set_pattern("%v");
  logger_multi->set_level(spdlog::level::info);

#ifdef SEARCH_LOG
  search::tree_logger =
      spdlog::basic_logger_st("debug_logger", "search-log.txt");
  search::tree_logger->set_pattern("%v");
  search::tree_logger->set_level(spdlog::level::info);
#endif // SEARCH_LOG
}

void FinalizeLogger() { spdlog::drop_all(); }

void SetLoggerState(const bool state) {}
}  // namespace utility
