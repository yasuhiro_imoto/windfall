// clang-format off
#include "stdafx.h"
// clang-format on

#include "bit_operator.h"

void* aligned_malloc(const size_t size, const size_t align) {
  void* p = _mm_malloc(size, align);
  if (p == nullptr) {
    std::cout << "info string Error: Failed to allocate memory. size = " << size
              << std::endl;
    exit(1);
  }
  return p;
}
