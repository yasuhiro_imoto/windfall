#pragma once
#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#pragma warning(push)
#pragma warning(disable : 6387 26439 26451 26495 26812 28182)
#include <spdlog/spdlog.h>
#pragma warning(pop)

#include <memory>

namespace utility {
/**
 * @brief spdlogのロガー
 *        TODO: 初期化が必要
*/
extern std::shared_ptr<spdlog::logger> logger;

void InitializeLogger();
/**
 * @brief windowsでspdlogを使う時に必要
*/
void FinalizeLogger();

/**
 * @brief Loggerの有効・無効を切り替える
 * @param state trueならcin/coutへの入出力をファイルにリダイレクトを開始する
 *              falseならリダイレクトを終了する
 * start_logger
*/
void SetLoggerState(const bool state);
}  // namespace utility

#ifdef SEARCH_LOG
namespace search {
extern std::shared_ptr<spdlog::logger> tree_logger;
}
#endif  // SEARCH_LOG

#endif  // !LOGGER_H_INCLUDED
