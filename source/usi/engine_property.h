#pragma once
#ifndef ENGINE_PROPERTY_H_INCLUDED
#define ENGINE_PROPERTY_H_INCLUDED

#include <string>

namespace usi {
/**
 * @brief USIプロトコルで要求されるエンジン名と作者名をプロトコル形式で返す
 * 
 * YaneuraOuと同様にテキストファイルから名前を設定できるが、
 * 形式はjsonのmap
 * {"name": "Engine", "author": "Author"}
 * となる
 * @return 
*/
std::string GetEngineProperty();
}
#endif // !ENGINE_PROPERTY_H_INCLUDED

