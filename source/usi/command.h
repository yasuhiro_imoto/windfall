#pragma once
#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

#include <deque>
#include <iosfwd>
#include <memory>

#include "../utility/bit_operator.h"

namespace state_action {
class Position;
class Rollback;
using StateList = std::deque<Rollback, AlignedAllocator<Rollback>>;
using StateListPtr = std::unique_ptr<StateList>;
}  // namespace state_action

namespace usi {
namespace command {
void SetPosition(state_action::Position& position,
                 state_action::StateListPtr& states, std::istringstream& iss);

void Go(const state_action::Position& position,
        state_action::StateListPtr& states, std::istringstream& iss);

void SetOption(std::istringstream& iss);
void GetOption(std::istringstream& iss);

void GetReady(state_action::Position& position,
              state_action::StateListPtr& states);
void GetReady(const bool skip_corruption_check);
}  // namespace command
}  // namespace usi
#endif  // !COMMAND_H_INCLUDED
