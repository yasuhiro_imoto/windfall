#include "stdafx.h"

#ifdef NNUE_LEARN

#include "remote.h"

#include "../search/learner.h"
#include "../evaluation/evaluator.h"

#pragma warning(push)
#pragma warning(disable : 4267 6011 6387 26110 26451 26495 26812)
#include "../remote_usi.grpc.pb.h"
#pragma warning(pop)

namespace usi {
class LearnerServiceImpl final : public remote_usi::Learner::Service {
  grpc::Status GetPV(
      grpc::ServerContext* context,
      grpc::ServerReaderWriter<remote_usi::PV, remote_usi::SearchCondition>*
          stream) override {
    // スレッド数などオプションの設定が必要
    search::learner::MultiSearch ms;
    ms.StartWorker();

    int counter = 0;
    remote_usi::SearchCondition condition;
    while (stream->Read(&condition)) {
      auto input_data = std::make_unique<search::learner::LearningData>();

      const auto& sfen = condition.sfen();
      std::memcpy(input_data->sfen.data.data(), sfen.data(), sfen.size());

      input_data->depth = condition.depth();

      input_data->id = counter++;

      ms.input_queue.push(std::move(input_data));
    }
    ms.input_queue.MarkAsFinished();

    for (int i = 0; i < counter; ++i) {
      auto output_data = ms.output_queue.pop();

      remote_usi::PV pv;
      for (const auto move : output_data->pv) {
        pv.add_move(static_cast<uint32_t>(move.value));
      }
      pv.set_score(output_data->score);

      stream->Write(pv);
    }

    ms.WaitForFinish();

    return grpc::Status::OK;
  }

  grpc::Status ComputeTrainingPair(
      grpc::ServerContext* context,
      grpc::ServerReaderWriter<remote_usi::TrainingPair, remote_usi::TeacherPV>*
          stream) override {
    search::learner::MultiSearchPair ms;
    ms.StartWorker();

    int counter = 0;
    remote_usi::TeacherPV teacher_pv;
    while (stream->Read(&teacher_pv)) {
      auto input_data = std::make_unique<search::learner::TeacherPV>();

      const auto& sfen = teacher_pv.sfen();
      std::memcpy(input_data->sfen.data.data(), sfen.data(), sfen.size());

      const auto& tmp = teacher_pv.pv();
      input_data->pv.reserve(tmp.size());
      for (const auto v : tmp) {
        input_data->pv.emplace_back(static_cast<state_action::Move::Value>(v));
      }
      ms.input_queue.push(std::move(input_data));
      ++counter;
    }
    ms.input_queue.MarkAsFinished();

    for (int i = 0; i < counter; ++i) {
      auto output_data = ms.output_queue.pop();
      if (!output_data) {
        continue;
      }

      remote_usi::TrainingPair training_pair;
      for (const auto index : output_data->teacher_indices[0]) {
        training_pair.mutable_teacher()->add_p_index(index);
      }
      for (const auto index : output_data->teacher_indices[1]) {
        training_pair.mutable_teacher()->add_q_index(index);
      }
      for (const auto index : output_data->student_indices[0]) {
        training_pair.mutable_student()->add_p_index(index);
      }
      for (const auto index : output_data->student_indices[1]) {
        training_pair.mutable_student()->add_q_index(index);
      }
      // note: うっかりdoubleで64bitにしてしまった
      //       そのうち修正するかも
      training_pair.set_teacher_sign(
          static_cast<float>(output_data->teacher_sign));
      training_pair.set_student_sign(
          static_cast<float>(output_data->student_sign));

      stream->Write(training_pair);
    }
    ms.WaitForFinish();

    return grpc::Status::OK;
  }

  grpc::Status UpdateParameter(
      grpc::ServerContext* context,
      const remote_usi::ParamterPath* paramter_path,
      remote_usi::UpdateStatus* update_status) override {
    const std::string& path = paramter_path->path();
    std::cout << path << std::endl;
    const bool result = evaluation::LoadParameter(path);
    update_status->set_update_result(result);
    return grpc::Status::OK;
  }

  // 動作確認用
  grpc::Status SayHello(grpc::ServerContext* context,
                        const remote_usi::HelloRequest* request,
                        remote_usi::HelloReply* reply) override {
    std::string prefix("Hello ");
    reply->set_message(prefix + request->name());
    return grpc::Status::OK;
  }
  grpc::Status Twice(
      grpc::ServerContext* context,
      grpc::ServerReaderWriter<remote_usi::HelloReply,
                               remote_usi::HelloRequest>* stream) override {
    remote_usi::HelloRequest request;
    while (stream->Read(&request)) {
      const auto& name = request.name();

      remote_usi::HelloReply reply;
      reply.set_message(name + name);
      stream->Write(reply);
    }
    return grpc::Status::OK;
  }
};

void RunServer(const std::string& address, const int port) {
  std::string server_address(fmt::format("{}:{}", address, port));
  LearnerServiceImpl service;

  grpc::EnableDefaultHealthCheckService(true);
  grpc::reflection::InitProtoReflectionServerBuilderPlugin();
  grpc::ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an "synchronous" service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
  std::cout << "Sever listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}
}  // namespace usi

#endif  // NNUE_LEARN
