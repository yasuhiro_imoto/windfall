#include "stdafx.h"

#include "global_option.h"

namespace usi {
#ifdef USE_GLOBAL_OPTIONS
GlobalOptions global_options;
#endif  // USE_GLOBAL_OPTIONS
}  // namespace usi
