#pragma once
#ifndef PROTOCOL_H_INCLUDED
#define PROTOCOL_H_INCLUDED

#include <iosfwd>

#pragma warning(push)
#pragma warning(disable : 6387 26439 26451 26495 26812 28182)
// どういう実装かわからないが、includeするとoperator<<を使えるようになる
#include <spdlog/fmt/ostr.h>
#pragma warning(pop)

#include "../search/depth.h"
#include "../state_action/move.h"

namespace state_action {
class Position;
}
namespace evaluation {
enum class Value : int32_t;
}

namespace usi {
/**
 * @brief USI形式でPVを出力するためのラッパー
 */
struct PV {
  PV(const state_action::Position& p, const search::Depth d,
     const evaluation::Value a, const evaluation::Value b)
      : position(p), depth(d), alpha(a), beta(b) {}

  const state_action::Position& position;
  const search::Depth depth;
  const evaluation::Value alpha;
  const evaluation::Value beta;
};
std::ostream& operator<<(std::ostream& os, const PV& pv);

/**
 * @brief evaluation::ValueをUSI形式で出力するためのラッパー
 */
struct Value {
  Value(const evaluation::Value value) : v(value) {}
  evaluation::Value v;
};
std::ostream& operator<<(std::ostream& os, const Value& v);

struct Move {
  Move(const state_action::Move move) : m(move) {}
  state_action::Move m;

  // template <typename OStream>
  // friend OStream& operator<<(OStream& os, const Move& usi_move) {
  //  const auto move = usi_move.m;
  //  if (!move.ok()) {
  //    switch (move.value & 0x7F) {
  //      case 0:
  //        os << "none";
  //        break;
  //      case 1:
  //        os << "null";
  //        break;
  //      case 2:
  //        os << "resign";
  //        break;
  //      case 3:
  //        os << "win";
  //        break;
  //      default:
  //        os << "?";
  //        break;
  //    }
  //  } else if (move.drop()) {
  //    os << move.dropped_piece() << '*' << move.target();
  //  } else {
  //    os << move.source() << move.target();
  //    if (move.promotion()) {
  //      os << '+';
  //    }
  //  }

  //  return os;
  //}
};
std::ostream& operator<<(std::ostream& os, const Move& m);

}  // namespace usi

// namespace state_action {
// template <typename OStream>
// OStream& operator<<(OStream& os, const Move& m) {
//  return os << usi::Move(m);
//}
//}  // namespace state_action

template <>
struct fmt::formatter<state_action::Move> : formatter<string_view> {
  template <typename FormatContext>
  auto format(const state_action::Move& move, FormatContext& ctx) {
    std::ostringstream oss;
    oss << usi::Move(move);

    return formatter<string_view>::format(oss.str(), ctx);
  }
};

#endif  // !PROTOCOL_H_INCLUDED
