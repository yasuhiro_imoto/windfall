#pragma once
#ifndef OPTION_H_INCLUDED
#define OPTION_H_INCLUDED

#include <boost/lexical_cast.hpp>
#include <boost/operators.hpp>
#include <functional>
#include <map>
#include <ostream>
#include <string>
#include <vector>

#include "../utility/level_assert.h"

namespace state_action {
class Position;
}

namespace usi {
class Option;

//! 大文字小文字を区別しない比較
struct CaseInsensitiveLess {
  bool operator()(const std::string& lhs, const std::string& rhs) const;
};

//! USIのoption名と、それに対応する設定内容を保持
using OptionsMap = std::map<std::string, Option, CaseInsensitiveLess>;

//! オプションの種類の名前を定義
struct OptionType {
  static constexpr const char* button = "button";
  static constexpr const char* string = "string";
  static constexpr const char* check = "check";
  static constexpr const char* combo = "combo";
  static constexpr const char* spin = "spin";
};

//! check boxの値
/*! boolはintに変換したとき0,1になるのでOK */
constexpr char check_value[2][6] = {"false", "true"};

//! USIプロトコルで指定されるoptionの内容
class Option : private boost::equality_comparable<Option> {
  // USIプロトコルでsetoptionコマンドが送られてきたときのcallback
  using OnChange = std::function<void(const Option&)>;

  friend std::ostream& operator<<(std::ostream& os, const OptionsMap om);

 public:
  //! (GUI側のエンジン設定画面に出てくる)ボタン
  Option(const OnChange& on_change = nullptr)
      : type_(OptionType::button),
        min_(0),
        max_(0),
        on_change_(on_change),
        index_(0) {}
  //! 文字列
  Option(const char* s, const OnChange& on_change = nullptr)
      : type_(OptionType::string),
        min_(0),
        max_(0),
        on_change_(on_change),
        defalut_value_(s),
        current_value_(s),
        index_(0) {}
  //! (GUI側のエンジン設定画面に出てくる)CheckBox
  /*! bool型のoption デフォルト値がv */
  Option(const bool v, const OnChange& on_change = nullptr)
      : type_(OptionType::check),
        min_(0),
        max_(0),
        on_change_(on_change),
        defalut_value_(check_value[v]),
        current_value_(check_value[v]),
        index_(0) {}
  //! (GUI側のエンジン設定画面に出てくる)SpinBox
  /*! int64_t
      GUIが浮動小数型をサポートしていない可能性や有効桁数の好みからint64_tのみを対象とする
   */
  Option(const int64_t v, const int64_t min_value, const int64_t max_value,
         const OnChange& on_change = nullptr)
      : type_(OptionType::spin),
        min_(min_value),
        max_(max_value),
        on_change_(on_change),
        index_(0) {
    defalut_value_ = current_value_ = std::to_string(v);
  }
  template <typename Array>
  Option(const Array& combo_list, const std::string& v,
         const OnChange& on_change = nullptr)
      : type_(OptionType::combo), on_change_(on_change) {
    list_.reserve(combo_list.size());
    for (const auto& value : combo_list) {
      list_.push_back(value);
    }

    defalut_value_ = current_value_ = v;
  }

  //! USIプロトコル経由で値を設定されたとき、current_valueに反映
  Option& operator=(const std::string& s) {
    ASSERT_LV1(!type_.empty());

    // 値が範囲外の場合は何もしない
    // EvalDirなどは空文字列もあり得るのでチェックはしない
    if (((type_ != OptionType::button && type_ != OptionType::string) &&
         s.empty()) ||
        (type_ == OptionType::check && s != check_value[true] &&
         s != check_value[false]) ||
        (type_ == OptionType::spin &&
         (boost::lexical_cast<int64_t>(s) < min_ ||
          boost::lexical_cast<int64_t>(s) > max_))) {
      return *this;
    }

    // buttonは値ではなくただのトリガー
    // button以外なら値を設定
    if (type_ != OptionType::button) {
      current_value_ = s;
    }
    // 値が変化したのでハンドラを呼び出す
    if (on_change_) {
      on_change_(*this);
    }

    return *this;
  }

  //! 起動時に設定を代入
  void operator<<(const Option& option) {
    static size_t insertion_order = 0;
    *this = option;
    index_ = insertion_order++;
  }

  //! 整数型への暗黙の変換
  operator int64_t() const {
    ASSERT_LV1(type_ == OptionType::check || type_ == OptionType::spin);
    return (type_ == OptionType::spin
                ? boost::lexical_cast<int64_t>(current_value_)
                : current_value_ == check_value[true]);
  }
  //! 文字列型への暗黙の変換
  /*! 内容が文字列でなくても文字列に変換できると便利 */
  operator std::string() const {
    // アサーションはない方が都合がいい場合がある
    return current_value_;
  }

  //! case insensitiveなので等価演算子を独自に定義
  bool operator==(const char* s) const {
    ASSERT_LV1(type_ == OptionType::combo);
    return !CaseInsensitiveLess()(current_value_, s) &&
           !CaseInsensitiveLess()(s, current_value_);
  }

  //! indexの値を変えずにオプションの値を更新
  /*! コマンド文字列からOptionのインスタンスを構築する時にこの機能が必要 */
  void Overwrite(const Option& option) {
    const bool modification = current_value_ != option.current_value_;

    // バックアップ
    const auto f = on_change_;
    const auto i = index_;

    *this = option;

    // 復元
    index_ = i;
    on_change_ = f;

    // 値が書き換わったならコールバックを呼び出す
    if (modification && f) {
      f(*this);
    }
  }

 private:
  std::string defalut_value_, current_value_, type_;

  //! 整数型の範囲の最小と最大
  int64_t min_, max_;

  //! 出力の順序
  /*! この順序に従ってGUIの設定ダイアログに反映される */
  size_t index_;

  //! combo boxの場合の項目リスト
  std::vector<std::string> list_;

  //! 値が変更された時のcallback handler
  OnChange on_change_;
};

//! USIのoption設定
/*! Options */
extern OptionsMap options;

/**
 * @brief 評価関数のパラメータを読み込んだかどうか
 * 
 * EvalDirが変更されるとfalseになる
*/
extern bool parameter_loaded;
/**
 * @brief オプションのインデックスの順に項目をUSI形式で出力する
 * 
 * 最後にも改行が入る
 * @param os 
 * @param om 
 * @return 
*/
std::ostream& operator<<(std::ostream& os, const OptionsMap om);
}  // namespace usi
#endif  // !OPTION_H_INCLUDED
