#include "stdafx.h"

#include "command.h"

#include "../rule/entering_ou.h"
#include "../search/search_limit.h"
#include "../search/thread.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../utility/logger.h"
#include "option.h"
#include "../utility/tool.h"
#include "../evaluation/evaluator.h"
#include "../search/transposition_table.h"
#include "../search/search_initialization.h"

namespace usi {
namespace command {
void SetPosition(state_action::Position& position,
                 state_action::StateListPtr& states, std::istringstream& iss) {
  std::string token, sfen;
  iss >> token;

  if (token == "startpos") {
    // 初期局面
    sfen = state_action::Position::sfen_hirate;

    // もしあるならmovesを消費する
    iss >> token;
  } else {
    // sfenから始まっていなくてもOK
    if (token != "sfen") {
      sfen += token + " ";
    }

    while (iss >> token && token != "moves") {
      sfen += token + " ";
    }
  }

  // 古い局面を捨てて新しく作る
  states = std::make_unique<state_action::StateList>(1);
  position.Set(sfen, &states->back(), search::thread_pool.main_thread());

  state_action::Move move;
  while (iss >> token && (move = state_action::Move(position, token)) !=
                             state_action::Move::NONE) {
    states->emplace_back();
    if (move == state_action::Move::NULL_MOVE) {
      position.StepForwardNull(states->back());
    } else {
      position.StepForward(move, states->back());
    }
  }
}

void Go(const state_action::Position& position,
        state_action::StateListPtr& states, std::istringstream& iss) {
  std::string token;

  search::SearchLimit limits;
  bool ponder_mode = false;

  // 思考時間の起点を更新
  // できるだけ早く行わないとサーバーとのずれが大きくなる
  utility::timer.Reset();

#ifdef USE_ENTERING_KING_WIN
  // 入玉ルールを設定
  limits.entering_ou_rule = ParseEnteringOuRule(options["EnteringKingRule"]);
#endif  // USE_ENTERING_KING_WIN

  // 終局(引き分け)になるまでの手数
  // エンジンによってはこのオプションはないかも
  const int max_game_ply = options.count("MaxMovesToDraw")
                               ? static_cast<int>(options["MaxMovesToDraw"])
                               : 0;
  // 0は制限なしだが、代わりに十分に大きな値を設定する
  limits.max_game_ply = max_game_ply == 0 ? 100000 : max_game_ply;

#if !defined(MATE_ENGINE) && !defined(FOR_TOURNAMENT)
  // 全ての合法手を生成するか
  limits.generate_all_legal_moves = options["GenerateAllLegalMoves"];
#endif  // !defined(MATE_ENGINE) && !defined(FOR_TOURNAMENT)

  if (options["DepthLimit"] >= 0) {
    limits.depth = static_cast<int>(options["DepthLimit"]);
  }
  if (options["NodesLimit"] >= 0) {
    limits.nodes = static_cast<uint64_t>(options["NodesLimit"]);
  }

  while (iss >> token) {
    if (token == "seasrchmoves") {
      // 探索開始局面から指定の指し手だけを探索
      // 残り全部は指し手
      while (iss >> token) {
        limits.search_moves.emplace_back(position, token);
      }
    } else if (token == "wtime") {
      // 後手の残り時間[ms]
      iss >> limits.time[Color::WHITE];
    } else if (token == "btime") {
      // 先手の残り時間[ms]
      iss >> limits.time[Color::BLACK];
    } else if (token == "winc") {
      // 後手のフィッシャーで増える時間[ms]
      iss >> limits.inc[Color::WHITE];
    } else if (token == "binc") {
      // 先手のフィッシャーで増える時間[ms]
      iss >> limits.inc[Color::BLACK];
    } else if (token == "rtime") {
      // "go rtime 100" だと100~300ms思考する
      iss >> limits.rtime;
    } else if (token == "byoyomi") {
      // 秒読み
      utility::TimePoint t;
      iss >> t;
      // 先後で同じ
      limits.byoyomi[Color::BLACK] = limits.byoyomi[Color::WHITE] = t;
    } else if (token == "depth") {
      // 探索を打ち切る深さ
      iss >> limits.depth;
    } else if (token == "nodes") {
      // 探索を打ち切るノード数
      iss >> limits.nodes;
    } else if (token == "mate") {
      iss >> token;
      if (token == "infinite") {
        limits.mate = std::numeric_limits<int32_t>::max();
      } else {
        // USIプロトコルでは、手数ではなく探索に使う時間[ms]を与える
        limits.mate = boost::lexical_cast<int32_t>(token);
      }
    } else if (token == "perft") {
      iss >> limits.perft;
    } else if (token == "infinite") {
      // 思考時間を無制限
      limits.infinite = 1;
    } else if (token == "ponder") {
      ponder_mode = true;
    }
  }

  if (limits.byoyomi[Color::BLACK] == 0 && limits.byoyomi[Color::WHITE] == 0 &&
      limits.time[Color::BLACK] == 0 && limits.time[Color::WHITE] == 0) {
    // デバッグ用に毎回の入力が面倒なので、デフォルト値を設定
    limits.byoyomi[Color::BLACK] = limits.byoyomi[Color::WHITE] = 1000;
  }

  search::thread_pool.StartThinking(position, states, limits, ponder_mode);
}

void SetOption(std::istringstream& iss) {
  std::string token, name, value;
  while ((iss >> token) && token != "value") {
    // "name"は省略可能にする

    if (token != "name") {
      // スペース区切りで長い名前のoputionもある
      name += token + ' ';
    }
  }
  // 最後の空白を取り除く
  boost::trim(name);

  // valueの後にスペース区切りで長い値もあり得る
  while (iss >> token) {
    value += token + ' ';
  }
  boost::trim(value);

  if (options.count(name)) {
    options[name] = value;
  } else {
    utility::logger->info("Effor! : No such option: {}", name);
  }
}

void GetOption(std::istringstream& iss) {
  std::string token, name;
  // 長いオプション名に対応
  while (iss >> token) {
    name += token + ' ';
  }
  boost::trim(name);

  // すべて出力するかどうか
  const bool all = name.empty();
  for (const auto& o : options) {
    if (boost::iequals(o.first, name) || all) {
      utility::logger->info("Options[{1}] = {2}", o.first, o.second);
      if (!all) {
        return;
      }
    }
  }

  if (!all) {
    utility::logger->info("No such option: {}", name);
  }
}

void GetReady(const bool skip_corruption_check) {
  // スレッドが起動したことを知らせるフラグ
  bool thread_started = false;
  // 関数を抜けるときのフラグ
  // スレッドを停止させる役割がある
  bool thread_end = false;

  auto worker = std::thread([&] {
    // スレッドを起動した
    thread_started = true;

    auto logger = spdlog::get("loader_keep_alive");

    int count = 0;
    while (!thread_end) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      if (++count >= 50) {
        count = 0;
        // 改行を送信する
        // 定跡の読み込みが動いているので出力はそれぞれ制御が必要
        logger->info("");
      }
    }
  });
  // clang-format off
  BOOST_SCOPE_EXIT((&thread_end)(&worker)) {
    thread_end = true;
    worker.join();
  } BOOST_SCOPE_EXIT_END;
  // clang-format on

  // workerが起動するのを待つ
  while (!thread_started) {
    utility::Sleep(100);
  }

#ifdef USE_EVAL_HASH
  evaluation::ResizeEvaluationHash(options["EvalHash"]);
  // 初回だけ実行する
  static bool initialized = false;
  if (!initialized) {
    evaluation::ClearEvaluationHash();
  }
#endif // USE_EVAL_HASH

  if (!parameter_loaded) {
    evaluation::LoadParameter();
    // TODO: チェックサムを記録した方が良いのかも
    // TODO: ソフト名を表示した方が良いかも

    parameter_loaded = true;
  } else {
    // パラメータを読み込んだ時に記録したチェックサムを調べて
    // 値が壊れていないか確認する
  }

  search::tt.resize(options["USI_Hash"]);
  search::Clear();

  search::thread_pool.stop = false;
}

void GetReady(state_action::Position& position, state_action::StateListPtr& states)
{
  // 対局ごとにisreadyとusinewgameが送られてくるはずだが、プロトコルの定義があいまい
  // isreadyは毎回来るようなので、評価関数、定数、探索部を初期化する
  GetReady(false);

  // このタイミングで初期局面で初期化する
  states = std::make_unique<state_action::StateList>(1);
  position.SetHirate(&states->back(), search::thread_pool.main_thread());

  utility::logger->info("readyok");
}
}  // namespace command
}  // namespace usi
