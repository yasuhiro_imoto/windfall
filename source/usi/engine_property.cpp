#include "stdafx.h"

#include "engine_property.h"

namespace usi {
std::string GetEngineProperty() {
  std::ostringstream oss;

  // 設定を正しい順序で書かないといけないとか難しすぎ
  // json形式にする
  std::ifstream ifs("engine_name.json");
  if (!ifs.fail()) {
    nlohmann::json json;
    ifs >> json;

    const auto name = json.find("name") != json.end()
                          ? json["name"].get<std::string>()
                          : "default engine";
    oss << boost::format("id name %1%") % name << std::endl;

    const auto author = json.find("author") != json.end()
                            ? json["author"].get<std::string>()
                            : "default author";
    oss << boost::format("id author %1%") << author << std::endl;
  } else {
    // TODO: 64bit対応やtarget cpu, tournament仕様など色々ある
#ifdef _DEBUG
    oss << "id name Fluke(debug) v0.1a" << std::endl;
#else
    oss << "id name Fluke v0.1a" << std::endl;
#endif // _DEBUG


    oss << "id author Yasuhiro Imoto" << std::endl;
  }
  return oss.str();
}
}  // namespace usi
