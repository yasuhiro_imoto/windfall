// clang-format off
#include "stdafx.h"
// clang-format on

#include "extra_option.h"

namespace usi {
void InitializeExtraOption(OptionsMap& om) {
  // todo: 定跡の初期化設定

  // PVの出力の抑制のために前回出力時間からの間隔(ms)
  om["PvInterval"] << Option(300, 0, 100000);

  // 投了スコア
  om["ResignValue"] << Option(99999, 0, 99999);

#ifdef EVAL_LEARN
  // 学習時のパラメータの保存先のルート
  om["EvalSaveDir"] << Option("evalsave");
#endif  // EVAL_LEARN

#ifdef ENABLE_OUTPUT_GAME_RESULT
  // パラメーターのログの保存先パス
  om["PARAMETERS_LOG_FILE_PATH"] << Option("param_log.txt");
#endif  // ENABLE_OUTPUT_GAME_RESULT

  // 検討モードでPVを出力
  om["ConsiderationMode"] << Option(false);

  // fail low/highの時にPVを出力するかどうか
  om["OutputFailLHPV"] << Option(true);
}

}  // namespace usi