#include "stdafx.h"

#include "protocol.h"

#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../search/depth.h"
#include "../search/thread.h"
#include "../search/transposition_table.h"
#include "../state_action/position.h"
#include "../utility/configuration.h"
#include "../utility/level_assert.h"
#include "../utility/time_point.h"
#include "option.h"

namespace usi {
/**
 * @brief 置換表からPVを掻き集めて出力する
 *
 * スタックサイズが大きすぎるとVisual Studioの警告が出るが
 * リンカーの設定でスタックサイズを大きくしてあるので問題ない
 * @param os
 * @param position
 * @param root_move
 */
void OutputPvFromTT(std::ostream& os, const state_action::Position& position,
                    const search::RootMove& root_move) {
  // 置換表からPVを集める
  auto p = const_cast<state_action::Position*>(&position);
  std::array<state_action::Move, max_ply + 1> moves;
  std::array<state_action::Rollback, max_ply> rollbacks;

  int ply = 0;

  while (ply < max_ply) {
    // 千日手が最善手の場合は置換表を更新しないので、
    // 置換表ではMove::NONEが最善手になっている可能性がある
    // 早めに千日手か確認する
    if (const auto repetition = p->IsRepetition();
        repetition != RepetitionState::NONE && ply >= 1) {
      // 千日手の場合は初手のPVだけ出力して終了
      os << " " << repetition;
      break;
    }

    state_action::Move move;
    if (ply == 0) {
      // Multi PVを考慮して初手はroot_movesから取得する
      // 宣言勝ちが含まれている可能性がある
      move = root_move.pv[0];
    } else {
      // 置換表から探す
      bool found;
      auto* tte = search::tt.Probe(p->state()->key(), found);

      if (!found) {
        // なかった
        break;
      }

      move = tte->move();
      if (move != state_action::Move::WIN) {
        // 置換表から取り出したので、合法手か確認する
        move = p->ConvertMove16ToMove(move);
        if (!p->IsPseudoLegal(move) || !p->IsLegal(move)) {
          break;
        }
      }
    }

#ifdef USE_ENTERING_KING_WIN
    if (move == state_action::Move::WIN) {
      // 宣言勝ち
      if (p->Declarate() != state_action::Move::NONE) {
        // 合法手
        os << " " << state_action::Move::WIN;
      }
      break;
    }
#endif  // USE_ENTERING_KING_WIN

    moves[ply] = move;
    os << " " << move;

    p->StepForward(move, rollbacks[ply]);
    ++ply;
  }

  while (ply > 0) {
    p->StepBackward(moves[--ply]);
  }
}

std::ostream& operator<<(std::ostream& os, const PV& pv) {
  utility::TimePoint elapsed = utility::timer.elapsed() + 1;

  const auto& root_moves = pv.position.ptr_thread()->root_moves;
  const size_t pv_index = pv.position.ptr_thread()->pv_index;
  const size_t multi_pv =
      std::min<size_t>(options["MultiPV"], root_moves.size());

  const uint64_t nodes_searched = search::thread_pool.nodes_searched();

  bool first_line = true;
  for (size_t i = 0; i < multi_pv; ++i) {
    // この指し手の更新が終わっているか
    const bool updated =
        i <= pv_index && root_moves[i].score != -evaluation::Value::INFINITE;
    if (pv.depth == search::Depth::ONE && !updated) {
      continue;
    }

    const search::Depth d = updated ? pv.depth : pv.depth - search::Depth::ONE;
    const evaluation::Value v =
        updated ? root_moves[i].score : root_moves[i].previous_score;

    // root_movesの先頭の方だけ評価されて、後ろの方が未評価の場合がある
    // 置換表にヒットしまくることが偏りの原因と予想される
    // 未評価の場合は表示をスキップする
    if (v == -evaluation::Value::INFINITE) {
      continue;
    }

    if (!first_line) {
      // 1行目でないなら改行
      os << std::endl;
    }
    // 改行のためのフラグをセット
    first_line = false;

    os << "info depth " << static_cast<int>(d / search::Depth::ONE)
       << " seldepth " << root_moves[i].selective_depth << " score "
       << Value(v);

    if (i == pv_index) {
      // 現在も探索中の指し手
      // lower boundかupper boundを表示する
      os << (v >= pv.beta ? " lowerbound" : v <= pv.alpha ? " upperbound" : "");
    }
    if (multi_pv > 1) {
      os << " multipv " << (i + 1);
    }

    os << " nodes " << nodes_searched << " nps "
       << nodes_searched * 1000 / elapsed;
    if (elapsed > 1000) {
      // 経過時間が短すぎるときは正確な値にならないので、置換表の使用率を表示しない
      os << " hashfull " << search::tt.Hashfull();
    }

    os << " time " << elapsed << " pv";

    // USIプロトコルで読み筋は最後に出力することになっている
#ifndef USE_TT_PV
    // 検討モードなら置換表からPVを集める
    // (そうしないとMulti PVの時にPVが欠損する場合がある)
    const bool condition = search::limit.consideration_mode;
#else
    // 置換表からPVを出力するモード
    // ただし、probe()するとTTEntryのgenerationが変わるので、探索に影響する
    // benchコマンドの時はこのモードをオフにする
    const bool condition = !search::limit.bench;
#endif  // !USE_TT_PV

    if (condition) {
      // 置換表からPVを集める
      // スタックサイズが大きいので関数にして分離
      OutputPvFromTT(os, pv.position, root_moves[i]);
    } else {
      // PVの配列から出力する
      for (const auto move : root_moves[i].pv) {
        os << " " << move;
      }
    }
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const Value& v) {
  ASSERT_LV3(v.v > -evaluation::Value::INFINITE &&
             v.v < evaluation::Value::INFINITE);

  if (v.v == evaluation::Value::NONE) {
    // 置換表で値が確定していないことがある
    os << "none";
  } else if (abs(v.v) < evaluation::Value::MATE_IN_MAX_PLY) {
    os << "cp "
       << enum_cast<>(v.v) * 100 / enum_cast<>(evaluation::PieceScore::FU);
  } else if (v.v == evaluation::Value::MATE) {
    // 詰みの場合の出力方法が規定されていない
    // ちなみに手数のわからない詰みは"mate -"で表す
    // YaneuraOuと同様の形式で出力する
    os << "make -0";
  } else {
    os << "mate "
       << ((enum_cast<>(v.v) > 0)
               ? enum_cast<>(evaluation::Value::MATE - v.v)
               : enum_cast<>(-evaluation::Value::MATE - v.v));
  }

  return os;
}

std::ostream& operator<<(std::ostream& os, const Move& m) {
  if (!m.m.ok()) {
    switch (m.m.value & 0x7F) {
      case 0:
        os << "none";
        break;
      case 1:
        os << "null";
        break;
      case 2:
        os << "resign";
        break;
      case 3:
        os << "win";
        break;
      default:
        os << "?";
        break;
    }
  } else if (m.m.drop()) {
    os << m.m.dropped_piece() << '*' << m.m.target();
  } else {
    os << m.m.source() << m.m.target();
    if (m.m.promotion()) {
      os << '+';
    }
  }

  return os;
}
}  // namespace usi
