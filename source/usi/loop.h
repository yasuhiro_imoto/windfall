#pragma once
#ifndef LOOP_H_INCLUDED
#define LOOP_H_INCLUDED

namespace usi {
/**
 * @brief USIコマンドに応答する
 * @param argc 
 * @param argv 
 * loop
*/
void Loop(const int argc, const char* const argv[]);
}
#endif // !LOOP_H_INCLUDED

