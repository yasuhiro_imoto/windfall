// clang-format off
#include "stdafx.h"
// clang-format on

#include "option.h"

namespace usi {
OptionsMap options;

bool parameter_loaded = false;

bool CaseInsensitiveLess::operator()(const std::string& lhs,
                                     const std::string& rhs) const {
  // boost::lexicographical_compareでは複数該当する
  return boost::range::lexicographical_compare(
      lhs, rhs, [](const char cl, const char cr) {
        return std::tolower(cl) < std::tolower(cr);
      });
}

std::ostream& operator<<(std::ostream& os, const OptionsMap om) {
  std::vector<std::string> option_list(om.size());
  for (const auto& e : om) {
    std::ostringstream buffer;

    const Option& o = e.second;
    buffer << boost::format("option name %1% type %2%") % e.first % o.type_;
    if (o.type_ == "string" || o.type_ == "check" || o.type_ == "combo") {
      buffer << " default " << o.defalut_value_;
    } else if (o.type_ == "spin") {
      buffer << boost::format(" default %1% min %2% max %3%") %
                    boost::lexical_cast<int64_t>(o.defalut_value_) % o.min_ %
                    o.max_;
    } 
    
    if (!o.list_.empty()) {
      for (const auto& v : o.list_) {
        buffer << " var " << v;
      }
    }

    option_list[o.index_] = std::move(buffer.str());
  }

  // インデックスの順に出力
  for (const std::string& s : option_list) {
    os << s << std::endl;
  }

  return os;
}
}  // namespace usi
