#pragma once
#include <string>

namespace usi {
#ifdef NNUE_LEARN
void RunServer(const std::string& address, const int port);
#endif  // NNUE_LEARN
}  // namespace usi
