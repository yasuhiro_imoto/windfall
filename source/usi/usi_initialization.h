#pragma once
#ifndef USI_INITIALIZATION_H_INCLUDED
#define USI_INITIALIZATION_H_INCLUDED

#include "option.h"

namespace usi {
void Initialization(OptionsMap& om);
}
#endif  // !USI_INITIALIZATION_H_INCLUDED
