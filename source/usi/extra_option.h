#pragma once
#ifndef EXTRA_OPTION_H_INCLUDED
#define EXTRA_OPTION_H_INCLUDED

#include "option.h"

namespace usi {
void InitializeExtraOption(OptionsMap& om);
}
#endif // !EXTRA_OPTION_H_INCLUDED

