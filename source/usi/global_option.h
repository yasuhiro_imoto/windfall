#pragma once
#ifndef GLOBAL_OPTION_H_INCLUDED
#define GLOBAL_OPTION_H_INCLUDED

namespace usi {
#ifdef USE_GLOBAL_OPTIONS

struct GlobalOptions
{
  GlobalOptions() :use_evaluation_hash(true), use_hash_probe(true) {}

  /**
   * @brief evaluation_hashを有効/無効化する
   *
   * use_eval_hash
  */
  bool use_evaluation_hash;

  /**
   * @brief 置換表のprobeを有効/無効化する
   *
   * 無効化するとTranspositionTable.Probeが必ずmiss hitする
   * use_hash_probe
  */
  bool use_hash_probe;
};

extern GlobalOptions global_options;
#endif // USE_GLOBAL_OPTIONS
}
#endif // !GLOBAL_OPTION_H_INCLUDED

