#include "stdafx.h"

#include "loop.h"

#include "../evaluation/evaluator.h"
#include "../search/thread.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../utility/logger.h"
#include "../utility/time_point.h"
#include "command.h"
#include "engine_property.h"
#include "option.h"
#include "remote.h"

namespace usi {
enum class Command;
}

namespace std {
bool operator==(const std::pair<const std::string, usi::Command>& x,
                const std::string& y) noexcept {
  return x.first == y;
}
}  // namespace std

namespace usi {
enum class Command {
  QUIT,
  STOP,
  GAMEOVER,
  PONDER_HIT,
  GO,
  POSITION,

  USI,
  SET_OPTION,
  GET_OPTION,
  IS_READY,
  USI_NEW_GAME,

  DEBUG,
  MATSURI,
  SFEN,
  LOG,

  EVALUATION,
  STATISTICS,

  QUIET_SEARCH,
  SEARCH,

  MOVES,
  SIDE,
  MATED,
  KEY,
  BENCHMARK,

  NONE,

  OUTPUT_PARAMETER,
  SERVER
};

using CommandList = std::unordered_map<std::string, Command>;
const CommandList command_list = {
    {"quit", Command::QUIT},
    {"stop", Command::STOP},
    {"gameover", Command::GAMEOVER},
    {"ponderhit", Command::PONDER_HIT},
    {"go", Command::GO},
    {"position", Command::POSITION},
    //
    {"usi", Command::USI},
    {"setoption", Command::SET_OPTION},
    {"getoption", Command::GET_OPTION},
    {"isready", Command::IS_READY},
    {"usinewgame", Command::USI_NEW_GAME},
    //
    {"d", Command::DEBUG},
    {"matsuri", Command::MATSURI},
    {"sfen", Command::SFEN},
    {"log", Command::LOG},
    //
    {"eval", Command::EVALUATION},
    {"evalstat", Command::STATISTICS},
    //
    {"qsearch", Command::QUIET_SEARCH},
    {"search", Command::SEARCH},
    //
    {"moves", Command::MOVES},
    {"side", Command::SIDE},
    {"mated", Command::KEY},
    {"bench", Command::BENCHMARK},
    // ダミーの値のパラメータのファイルを出力する
    {"output_parameter", Command::OUTPUT_PARAMETER},
    {"server", Command::SERVER}};

void Loop(const int argc, const char* const argv[]) {
  std::string cmd, token;
  // 起動時に入力されたコマンド
  std::queue<std::string> cmds;

  state_action::Position position;
  state_action::StateListPtr states(
      std::make_unique<state_action::StateList>(1));

  if (argc >= 3 && std::string(argv[1]) == "file") {
    std::ifstream ifs(argv[2]);
    std::string buf;
    if (ifs.fail()) {
      utility::logger->error("info File is not found. ({})", argv[2]);
      exit(-1);
    }
    while (std::getline(ifs, buf)) {
      boost::algorithm::trim(buf);
      cmds.push(buf);
    }
  } else {
    for (int i = 1; i < argc; ++i) {
      std::string s(argv[i]);
      boost::algorithm::trim(s);

      if (s != ",") {
        cmd += s + " ";
      } else {
        // 末尾の余計な空白を取り除く
        boost::algorithm::trim(cmd);
        cmds.push(cmd);

        cmd.clear();
      }
    }
    if (!cmd.empty()) {
      // 末尾の余計な空白を取り除く
      boost::algorithm::trim(cmd);
      cmds.push(cmd);
    }
  }

  do {
    if (cmds.empty()) {
      // 入力待ち
      if (!std::getline(std::cin, cmd)) {
        cmd = "quit";
      }
    } else {
      // 入力済みのコマンドを実行する
      cmd = std::move(cmds.front());
      cmds.pop();
    }

    std::istringstream iss(cmd);
    token.clear();

    iss >> std::skipws >> token;
    const CommandList::const_iterator it = boost::find(command_list, token);
    const auto command = it != command_list.end() ? it->second : Command::NONE;
    switch (command) {
      case Command::QUIT:
      case Command::STOP:
      case Command::GAMEOVER:
        // TODO: game overの場合のコールバックを設定

        search::thread_pool.stop = true;
        break;
      case Command::PONDER_HIT:
        // 経過時間の起点を更新
        utility::timer.ResetPonderHit();
        // 通常探索に切り替える
        search::thread_pool.main_thread()->ponder = false;
        break;
      case Command::GO:
        command::Go(position, states, iss);
        break;
      case Command::POSITION:
        command::SetPosition(position, states, iss);
        break;
      case Command::USI:
        // それぞれの文字列に改行が含まれいる
        utility::logger->info("{}{}usiok", GetEngineProperty(),
                              boost::str(boost::format("%1%") % options));
        break;
      case Command::SET_OPTION:
        command::SetOption(iss);
        break;
      case Command::GET_OPTION:
        command::GetOption(iss);
        break;
      case Command::IS_READY:
        command::GetReady(position, states);
        break;
      case Command::DEBUG:
        std::cout << position << std::endl;
        break;
      case Command::EVALUATION:
        std::cout << "eval = "
                  << static_cast<int>(evaluation::Evaluate(position))
                  << std::endl;
        break;
      case Command::NONE:
        if (!token.empty()) {
          // setoption name Threads value 1
          // などの省略系として
          // threads 1
          // のような形式をサポートする

          std::string value;
          iss >> value;

          bool flag = false;
          for (auto& o : options) {
            // 大文字/小文字を無視して比較
            // localeに依存するので、期待通りの結果にならないかも
            // 明示的にlocaleを適切に指定すれば良いと思うが、それが何かはわからない
            if (boost::iequals(o.first, token)) {
              options[o.first] = value;
              utility::logger->info("Options[{}] = {}", o.first, value);

              flag = true;
              break;
            }
          }
          if (!flag) {
            utility::logger->info("No such option: {}", token);
          }
        }
        break;
      case Command::OUTPUT_PARAMETER:
        // 独自に追加
        // 評価関数のパラメータの形式を確認するために初期化されていない値で保存する
        evaluation::SaveParameter();
        break;
#ifdef NNUE_LEARN
      case Command::SERVER: {
        // server learn [address [port]]
        // のようにして呼び出す

        std::string value;
        iss >> std::noskipws >> value;
        std::vector<std::string> result;
        boost::algorithm::split(result, value, boost::is_any_of(" "));

        std::string mode = result[0], address = "0.0.0.0";
        int port = 50051;
        if (result.size() >= 2) {
          address = result[1];
        }
        if (result.size() >= 3) {
          port = boost::lexical_cast<int>(result[2]);
        }
        RunServer(address, port);
      } break;
#endif // NNUE_LEARN
      default:
        break;
    }
  } while (token != "quit");
  search::thread_pool.main_thread()->WaitForSearchFinished();
}
}  // namespace usi
