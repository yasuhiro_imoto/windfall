#include "stdafx.h"

#include "../search/thread.h"
#include "option.h"
#include "../search/transposition_table.h"
#include "../evaluation/evaluator.h"
#include "../utility/logger.h"
#include "../rule/entering_ou.h"
#include "extra_option.h"
#include "../utility/configuration.h"

namespace usi {
//! 入玉ルールのUSI文字列での表記
const std::array<std::string, 4> ekr_rules = {"NoEnteringKing", "CSARule24",
                                              "CSARule27", "TryRule"};

//! 前回のOptions["EvalDir"]
std::string last_eval_dir;
//! 評価関数を読み込んだかのフラグ
/*! これはevaldirの変更にともなってfalseにする
    load_eval_finished */
bool is_loaded_evaluation_parameter = false;

void Initialization(OptionsMap& om) {
  // Hash上限
  // 64bitなら1024GB
  constexpr int max_hash_mb = 1024 * 1024;

  // 並列探索のスレッド数
  om["Threads"] << Option(4, 1, 512,
                          [](const Option& o) { search::thread_pool.Set(o); });

#if !defined(MATE_ENGINE)
  // 置換表のサイズ(MB単位)
  om["USI_Hash"] << Option(16, 1, max_hash_mb,
                           [](const Option& o) { search::tt.resize(o); });

#if defined(USE_EVAL_HASH)
  // 評価値用のcacheサイズ(MB単位)
#if defined(FOR_TOURNAMENT)
  // トーナメント用は少し大きなサイズ
  om["EvalHash"] << Option(1024, 1, max_hash_mb, [](const Option& o) {
    evaluation::ResizeEvaluationHash(o);
  });
#else
  om["EvalHash"] << Option(128, 1, max_hash_mb, [](const Option& o) {
    evaluation::ResizeEvaluationHash(o);
  });
#endif  // defined(FOR_TOURNAMENT)
#endif  // defined(USE_EVAL_HASH)
  om["USI_Ponder"] << Option(false);
  // その局面での上位N個の候補手を調べる機能
  om["MultiPV"] << Option(1, 1, 600);
  // 弱くするための調整
  // 20なら手加減なし、0が最弱
  om["SkillLevel"] << Option(20, 0, 20);
#else
  om["USI_Hash"] << Option(4096, 1, max_hash_mb);
#endif  // !defined(MATE_ENGINE)

  // cin/coutの入出力をファイルにリダイレクトする
  om["WriteDebugLog"] << Option(
      false, [](const Option& o) { utility::SetLoggerState(o); });

  // ネットワークの平均遅延時間[ms]
  // この時間だけ早めに指せばだいたい間に合う。
  // 切れ負けの瞬間は、NetworkDelayのほうなので大丈夫。
  om["NetworkDelay"] << Option(120, 0, 10000);
  // ネットワークの最大遅延時間[ms]
  // 切れ負けの瞬間だけはこの時間だけ早めに指す。
  // 1.2秒ほど早く指さないとfloodgateで切れ負けしかねない。
  om["NetworkDelay2"] << Option(1120, 0, 10000);

  // 最小思考時間[ms]
  om["MinimumThinkingTime"] << Option(2000, 1000, 100000);
  // 切れ負けのときの思考時間を調整する。序盤重視率。百分率になっている。
  // 例えば200を指定すると本来の最適時間の200%(2倍)思考するようになる。
  // 対人のときに短めに設定して強制的に早指しにすることが出来る。
  om["SlowMover"] << Option(100, 1, 1000);

  // 引き分けまでの最大手数。256手ルールのときに256を設定すると良い。0なら無制限。
  om["MaxMovesToDraw"] << Option(0, 0, 100000);

  // 探索深さ制限。0なら無制限。
  om["DepthLimit"] << Option(0, 0, std::numeric_limits<int32_t>::max());

  // 探索ノード制限。0なら無制限。
  om["NodesLimit"] << Option(0, 0, std::numeric_limits<int64_t>::max());

  // 引き分けを受け入れるスコア
  // 歩を100とする。例えば、この値を100にすると引き分けの局面は評価値が
  // -100とみなされる。
  // 千日手での引き分けを回避しやすくなるように、デフォルト値を2に変更した。[2017/06/03]
  // ちなみに、2にしてあるのは、
  //  int contempt = Options["Contempt"] * PawnValue / 100;
  //  でPawnValueが100より小さいので
  // 1だと切り捨てられてしまうからである。
  om["Contempt"] << Option(2, -30000, 30000);

  // Contemptの設定値を先手番から見た値とするオプション。Stockfishからの独自拡張。
  // 先手のときは千日手を狙いたくなくて、後手のときは千日手を狙いたいような場合、
  // このオプションをオンにすれば、Contemptをそういう解釈にしてくれる。
  // この値がtrueのときは、Contemptを常に先手から見たスコアだとみなす。
  om["ContemptFromBlack"] << Option(false);

#if defined(USE_ENTERING_KING_WIN)
  // 入玉ルール
  om["EnteringKingRule"] << Option(
      ekr_rules, ekr_rules[enum_cast<>(EnteringOuRule::POINTS27)]);
#endif  // defined(USE_ENTERING_KING_WIN)

  // 評価関数フォルダ
  // これを変更したとき、評価関数を次のisreadyタイミングで読み直す必要がある。
  last_eval_dir = "eval";
  om["EvalDir"] << Option("eval", [](const Option& o) {
    if (last_eval_dir != std::string(o)) {
      last_eval_dir = std::string(o);
      // ファイル名が変更されたので、読み込みフラグをクリア
      is_loaded_evaluation_parameter = false;
    }
  });

#if defined(EVAL_LEARN)
  // isreadyタイミングで評価関数を読み込まれると、新しい評価関数の変換のために
  // test evalconvertコマンドを叩きたいのに、その新しい評価関数がないがために
  // このコマンドの実行前に異常終了してしまう。
  // そこでこの隠しオプションでisready時の評価関数の読み込みを抑制して、
  // test evalconvertコマンドを叩く。
  om["SkipLoadingEval"] << Option(false);
#endif

#if !defined(MATE_ENGINE) && !defined(FOR_TOURNAMENT)
  // 読みの各局面ですべての合法手を生成する
  // (普通、歩の2段目での不成などは指し手自体を生成しないのですが、これのせいで不成が必要な詰みが絡む問題が解けないことが
  // あるので、このオプションを用意しました。トーナメントモードではこのオプションは無効化されます。)
  om["GenerateAllLegalMoves"] << Option(false);
#endif

  // 探索周辺・定跡の設定
  InitializeExtraOption(om);

  // TODO: engine_options.txtの対応
}
}  // namespace usi
