#include "stdafx.h"

#include "transposition_table.h"

#include "../state_action/move.h"
#include "../usi/global_option.h"
#include "../utility/level_assert.h"
#include "../utility/tool.h"
#include "bound.h"
#include "depth.h"
#include "thread.h"

namespace search {
TranspositionTable tt;

void TTEntry::Save(const Key k, const evaluation::Value v, const bool pv,
                   const Bound bound, const Depth d, const state_action::Move m,
                   evaluation::Value ev) {
  // 置換表にValue::INFINITE以上の値を書き込むのは本来はおかしいが、
  // 置換表の衝突によってValue::INFINITE以上の値が発生してしまう
  // N手詰めの局面で置換表の衝突によりそれより短い手数の詰みのスコアが返ってきた場合に起こる
  // そのため置換表からValue::INFINITE以上の値が出てくる可能性がある
  // これはmate distance pruningで補正されるので問題ない

  if (m || (k >> 48) != key_) {
    // case 1. 有効な指し手で、keyが一致している場合
    //         今回の探索の方が置換表の値よりも良いはずなので指し手を登録
    // case 2. 指し手はどちらでもよい、keyが一致しない場合
    //         古いエントリーを上書きする
    move_ = static_cast<uint16_t>(m);
  }

  if ((k >> 48) != key_ || static_cast<int>(d - Depth::OFFSET) > depth_ - 4 ||
      bound == Bound::EXACT) {
    // 1. keyが一致しない
    //    TranspositionTable.Probeでここに上書きすると決めた
    // 2. keyが一致するが、探索が深くなった
    //    新しい情報にも価値があるので少しくらい浅くても許容する
    // 3. Bound::EXACTなのでとても重要

    key_ = static_cast<uint16_t>(k >> 48);
    value_ = static_cast<int16_t>(v);
    evaluation_value_ = static_cast<int16_t>(ev);
    gpb_ = static_cast<uint8_t>(tt.generation_) |
           (static_cast<uint8_t>(pv) << 2) | static_cast<uint8_t>(bound);
    ASSERT_LV3((d - Depth::OFFSET) / Depth::ONE >= Depth::ZERO);
    // Depth::NONEより浅ければOKということがわかる
    static_assert((Depth::NONE - Depth::OFFSET) / Depth::ONE >= Depth::ZERO,
                  "");
    depth_ = static_cast<uint8_t>(d - Depth::OFFSET);
  }
}

TTEntry* TranspositionTable::Probe(const Key key, bool& found) const {
  ASSERT_LV3(cluster_count_ != 0);

#ifdef USE_GLOBAL_OPTIONS
  if (!usi::global_options.use_hash_probe) {
    // 置換表を利用しないモード
    // 見つからなかったことにして、常に先頭アドレスを返す
    found = false;
    return GetFirstEntry(0);
  }
#endif  // USE_GLOBAL_OPTIONS

  // 最初のTTEntryの要素のアドレス
  TTEntry* const tte = GetFirstEntry(key);

  // 上位16bitを取得する
  // 下位はエントリーを取得するために利用してある程度は一致している
  const uint16_t key16 = key >> 48;

  for (int i = 0; i < cluster_size; ++i) {
    if (!tte[i].key_ || tte[i].key_ == key16) {
      // 空のエントリーを見つけたかハッシュの上位16bitが一致した

      // 空のエントリーを見つけた場合はこの後のSaveで値が上書きされるので必要ない処理
      // 世代を更新
      tte[i].gpb_ = static_cast<uint8_t>(generation_ | (tte[i].gpb_ & 0x7));

      found = static_cast<bool>(tte[i].key_);
      return &tte[i];
    }
  }

  // 空のエントリーもハッシュが一致するエントリーも見つからなかった
  found = false;

  // どれか一つのエントリーをつぶす必要がある
  TTEntry* replace = tte;
  // 深い探索であるほど価値が高い(価値の重み:1.0)
  // 世代が今の探索の世代に近いほど価値が高い(価値の重み:2.0)
  // これによってスコアを付ける
  //
  // generation_はuint8_tなので、オーバーフローを上手く扱って引き算を行う
  // a,bが8bitであるとき、(256 + a - b) & 0xFFで引き算ができる
  // a := generation_(下位3bitは使っていないので常に0)
  // b := gpb_(下位3bitにはBoundが入っている)
  // (256 + (a + 0x7) - b) & 0xF8で下位3bitを無視しながら引き算ができる
  auto score = replace->depth_ - ((263 + generation_ - replace->gpb_) & 0xF8);
  for (int i = 1; i < cluster_size; ++i) {
    if (const auto tmp =
            tte[i].depth_ - ((263 + generation_ - tte[i].gpb_) & 0xF8);
        score > tmp) {
      replace = &tte[i];
      score = tmp;
    }
  }

  return replace;
}

int TranspositionTable::Hashfull() const {
  // 全部を調べるのは時間がかかるので、先頭から1000エントリーだけ調べる
  int count = 0;
  for (int i = 0; i < 1000 / cluster_size; ++i) {
    for (int j = 0; j < cluster_size; ++j) {
      count += (table_[i].entry[j].gpb_ & 0xF8) == generation_;
    }
  }

  return count * 1000 / (cluster_size * (1000 / cluster_size));
}

void TranspositionTable::resize(const size_t size_mb) {
#ifdef MATE_ENGINE
  // MateEngineは置換表を利用しない
  return;
#else
  if (thread_pool.empty()) {
    // スレッドの初期か前に呼び出された
    // Optionのオーバーライドで呼ばれる
    // これは無視する
    return;
  }

  // 探索が終わってからサイズを変更する
  thread_pool.main_thread()->WaitForSearchFinished();

  const size_t new_cluster_count = size_mb * 1024 * 1024 / sizeof(Cluster);
  // サイズは1MBの倍数でなくてもよいが、new_countは偶数の必要がある
  // 1024 * 1024 / sizeof(Cluster)の部分で偶数は保証されているはず
  ASSERT_LV3(new_cluster_count % 2 == 0);

  if (new_cluster_count == cluster_count_) {
    // サイズが同じなら処理は不要
    return;
  }

  cluster_count_ = new_cluster_count;

  free(memory_);
  // alignされた位置に配置するためにcache_line_size-1だけ余計に確保する
  // callocは自動で0クリアされる
  // mallocはされない
  // 自前でクリアすることで、速度低下を抑える
  // cf. Explicitly zero TT upon resize. :
  // https://github.com/official-stockfish/Stockfish/commit/2ba47416cbdd5db2c7c79257072cd8675b61721f
  memory_ = malloc(cluster_count_ * sizeof(Cluster) + cache_line_size - 1);
  if (!memory_) {
    std::cout << "info string Error : Failed to allocate " << size_mb
              << "MB for transposition table. ClusterCount = "
              << new_cluster_count << std::endl;
    exit(1);
  }

  table_ = reinterpret_cast<Cluster*>(
      (reinterpret_cast<uintptr_t>(memory_) + cache_line_size - 1) &
      ~(cache_line_size - 1));
#endif  // MATE_ENGINE
}

void TranspositionTable::clear() {
#ifdef MATE_ENGINE
  // mate engineは置換表を利用しないので、クリアの必要もない
  return;
#endif  // MATE_ENGINE

  const auto size = cluster_count_ * sizeof(Cluster);
  utility::ClearTable("Hash", table_, size);
}
}  // namespace search
