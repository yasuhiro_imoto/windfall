#pragma once
#ifndef MOVE_PICKER_H_INCLUDED
#define MOVE_PICKER_H_INCLUDED

#include <array>

#include "../evaluation/value.h"
#include "../rule/piece.h"
#include "../state_action/move.h"
#include "../state_action/move_generator.h"
#include "depth.h"
#include "history.h"

namespace state_action {
class Position;
}

namespace search {
enum class State : int;

/**
 * @brief 通常探索時のMovePickerのための指し手の配列
 */
class ExtendedMoveRange : public state_action::ExtendedMoveArray {
 public:
  static constexpr size_t refutation_size = 3;

  ExtendedMoveRange(
      const std::array<state_action::Move, refutation_size - 1>& killers,
      const state_action::Move counter)
      : ExtendedMoveArray(),
        bad_capture_end_(begin()),
        refutation_end_(begin()) {
    // 先頭にrefutationsを記録
    for (const auto move : killers) {
      *end_++ = state_action::ExtendedMove{move, 0};
    }
    *end_++ = state_action::ExtendedMove{counter, 0};
    // 先頭の3個まで要素が入った

    ASSERT_LV1(refutation_end_ == end_);
    ASSERT_LV1(bad_capture_end_ == end_);
  }

  using iterator = state_action::ExtendedMoveArray::iterator;
  using const_iterator = state_action::ExtendedMoveArray::const_iterator;

  /**
   * @brief refutationsを含まない位置を先頭とする
   * @return
   */
  iterator begin() noexcept { return list_.begin() + refutation_size; }
  // 終端はそのまま
  using state_action::ExtendedMoveArray::end;

  iterator refutation_begin() noexcept { return list_.begin(); }
  iterator refutation_end() noexcept { return refutation_end_; }

  iterator bad_capture_begin() noexcept { return begin(); }
  iterator bad_capture_end() noexcept { return bad_capture_end_; }

  void push_back_bad_capture(const state_action::Move move) {
    // 先頭の方を上書きする形でbad capturesを集める
    *bad_capture_end_++ = move;
  }

  /**
   * @brief refutationsに重複があれば取り除く
   * @return
   */
  void RemoveDuplicatedRefutation() noexcept {
    if (list_[0].move == list_[2].move || list_[1].move == list_[2].move) {
      --refutation_end_;
    }
  }

 private:
  /**
   * @brief refutationsの終端
   *        内容が重複しているかもしれないので、終端は変化する
   *        先頭は配列の先頭で固定
   */
  iterator refutation_end_;

  /**
   * @brief BAD_CAPTURESの終端
   *        先頭はbegin()で得られる位置で固定
   */
  iterator bad_capture_end_;
};

class MovePickerBase {
 public:
#pragma warning(push)
#pragma warning(disable : 26495)
  MovePickerBase(const state_action::Position& position,
                 const state_action::Move tt_move,
                 const CapturePieceTargetHistory* cph)
      : position_(position), tt_move_(tt_move), capture_history_(cph) {}
#pragma warning(pop)

  MovePickerBase(const MovePickerBase&) = delete;

  MovePickerBase& operator=(const MovePickerBase&) = delete;

 protected:
  enum class PickType {
    NEXT,  //!< 生成順に次の一手
    BEST   //!< オーダリングをして最善手
  };

  /**
   * @brief predがtrueになる指し手を返す
   *
   * 置換表の指し手は返さない
   * [cursor_, last)の範囲を処理する
   * @tparam Predicate
   * @param last
   * @param pred
   * @return
   */
  template <PickType Type, typename Predicate>
  state_action::Move Select(state_action::ExtendedMoveArray::iterator last,
                            Predicate pred);

  /**
   * @brief 駒を捕る指し手のオーダリングのための点数を設定
   * score
   */
  template <typename Array>
  void ComputeScore(Array& move_list);
  /**
   * @brief 駒を捕らない指し手のオーダリングのための点数を設定
   * score
   */
  template <typename Array>
  void ComputeScore(Array& move_list, const ButterflyHistory* main_history,
                    const PieceTargetHistory** continuation_history);
  /**
   * @brief 王手を回避する指し手のオーダリングのための点数を設定
   * score
   */
  template <typename Array>
  void ComputeScore(Array& move_list, const ButterflyHistory* main_history);

  const state_action::Position& position_;

  const CapturePieceTargetHistory* capture_history_;

  const state_action::Move tt_move_;

  /**
   * @brief 次に返す指し手
   *
   * cur
   */
  state_action::ExtendedMoveArray::iterator cursor_;

  /**
   * @brief 返す指し手の種類を決める内部状態
   */
  State state_;
};

/**
 * @brief 通常探索用の指し手オーダリング器
 */
class MovePickerNormal : public MovePickerBase {
 public:
  /**
   * @brief
   * @param position
   * @param tt_move
   * @param depth
   * @param mh
   * @param cph
   * @param ch
   * @param cm counter move(渡された直前の指し手に対する応手)
   * @param killer killer move
   */
  MovePickerNormal(const state_action::Position& position,
                   const state_action::Move tt_move, const Depth depth,
                   const ButterflyHistory* mh,
                   const CapturePieceTargetHistory* cph,
                   const PieceTargetHistory** ch, const state_action::Move cm,
                   const std::array<state_action::Move, 2>& killer);

  /**
   * @brief 一つpseudo legalな指し手を返す
   *
   * 指し手をすべて返した後はMove::NONEを返す
   * 置換表の指し手を優先して返す
   * @param skip_quiets
   * @return
   * next_move
   */
  state_action::Move Next(const bool skip_quiets = false);

 private:
  ExtendedMoveRange move_range_;

  const ButterflyHistory* main_history_;
  const PieceTargetHistory** continuation_history_;

  const Depth depth_;
};

/**
 * @brief 静止探索用の指し手オーダリング器
 */
class MovePickerQuiet : public MovePickerBase {
 public:
  /**
   * @brief
   * @param position
   * @param tt_move
   * @param depth
   * @param mh
   * @param cph
   * @param recapture_square 直前に動かした駒の移動先
   */
  MovePickerQuiet(const state_action::Position& position,
                  const state_action::Move tt_move, const Depth depth,
                  const ButterflyHistory* mh,
                  const CapturePieceTargetHistory* cph,
                  const Square recapture_square);

  /**
   * @brief 一つpseudo legalな指し手を返す
   *
   * 指し手をすべて返した後はMove::NONEを返す
   * 置換表の指し手を優先して返す
   * @return
   * next_move
   */
  state_action::Move Next();

 private:
  state_action::ExtendedMoveArray move_list_;

  const ButterflyHistory* main_history_;

  const Depth depth_;

  const Square recapture_square_;
};

/**
 * @brief 通常探索のProb Cutの処理から利用される専用の指し手オーダリング器
 */
class MovePickerProbCut : public MovePickerBase {
 public:
  /**
   * @brief
   * @param position
   * @param tt_move
   * @param threshold 直前に取られた駒の価値
   *                  これより価値の低い駒を捕獲する指し手は生成しない
   * @param cph
   */
  MovePickerProbCut(const state_action::Position& position,
                    const state_action::Move tt_move,
                    const evaluation::Value threshold,
                    const CapturePieceTargetHistory* cph);

  /**
   * @brief 一つpseudo legalな指し手を返す
   *
   * 指し手をすべて返した後はMove::NONEを返す
   * 置換表の指し手を優先して返す
   * @return
   */
  state_action::Move Next();

 private:
  state_action::ExtendedMoveArray move_list_;

  /**
   * @brief Prob Cutで用いる
   *        直前の指し手で捕獲された駒の価値
   */
  evaluation::Value threshold_;
};
}  // namespace search
#endif  // !MOVE_PICKER_H_INCLUDED
