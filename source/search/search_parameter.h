#pragma once
#ifndef SEARCH_PARAMETER_H_INCLUDED
#define SEARCH_PARAMETER_H_INCLUDED

namespace search {
namespace parameter {
/**
 * @brief aspiration searchの窓サイズの変化量
 *
 * PARAM_ASPIRATION_SEARCH_DELTA
 */
constexpr int aspiration_search_delta = 16;

/**
 * @brief quietな指し手を保存する個数
 *        保存した指し手はhistoryの増減させるために使われる
 *
 * PARAM_QUIET_SEARCH_COUNT
 */
constexpr int quiet_search_count = 64;

/**
 * @brief 深さに比例したfutility pruningの係数
 *
 * PARAM_FUTILITY_MARGIN_ALPHA1
 */
constexpr int futility_margin_alpha1 = 172;
/**
 * @brief
 *
 * PARAM_FUTILITY_MARGIN_ALPHA2
 */
constexpr int futility_margin_alpha2 = 50;

/**
 * @brief
 *
 * PARAM_FUTILITY_MARGIN_BETA
 */
constexpr int futility_margin_beta = 195;

/**
 * @brief 静止探索でのfutility pruning
 *
 * PARAM_FUTILITY_MARGIN_QUIET
*/
constexpr int futility_margin_quiet = 145;

/**
 * @brief futility pruningを適用する深さ
 *
 * PARAM_FUTILITY_RETURN_DEPTH
 */
constexpr int futility_return_depth = 9;

/**
 * @brief 親ノードの時点でfutility pruningを適用する深さ
 *
 * PARAM_FUTILITY_AT_PARENT_NODE_DEPTH
 */
constexpr int futility_at_parent_node_depth = 12;

/**
 * @brief
 *
 * PARAM_FUTILITY_AT_PARENT_NODE_MARGIN1
 */
constexpr int futility_at_parent_node_margin1 = 256;
/**
 * @brief
 *
 * PARAM_FUTILITY_AT_PARENT_NODE_MARGIN2
 */
constexpr int futility_at_parent_node_margin2 = 248;

/**
 * @brief
 *
 * PARAM_FUTILITY_AT_PARENT_NODE_GAMMA1
 */
constexpr int futility_at_parent_node_gamma1 = 40;
/**
 * @brief
 *
 * PARAM_FUTILITY_AT_PARENT_NODE_GAMMA2
 */
constexpr int futility_at_parent_node_gamma2 = 51;

/**
 * @brief
 *
 * PARAM_NULL_MOVE_MARGIN
 */
constexpr int null_move_margin = 31;

/**
 * @brief
 *
 * PARAM_NULL_MOVE_DYNAMIC_ALPHA
 */
constexpr int null_move_dynamic_alpha = 818;
/**
 * @brief
 *
 * PARAM_NULL_MOVE_DYNAMIC_BETA
 */
constexpr int null_move_dynamic_beta = 67;

/**
 * @brief null moveの時の評価値がbetaを上回った際に
 *        残りの探索深さがこの値以下なら終了する閾値
 *
 * PARAM_NULL_MOVE_RETURN_DEPTH
 */
constexpr int null_move_return_depth = 14;

/**
 * @brief probcutを行う時の深さの下限
 *
 * PARAM_PROBCUT_DEPTH
 */
constexpr int prob_cut_depth = 5;

/**
 * @brief
 *
 * PARAM_PROBCUT_MARGIN1
 */
constexpr int prob_cut_margin1 = 194 + 16 / 2;
/**
 * @brief
 *
 * PARAM_PROBCUT_MARGIN2
 */
constexpr int prob_cut_margin2 = 48 / 2;

/**
 * @brief move countによる枝刈りをする深さの閾値
 *
 * PARAM_PRUNING_BY_MOVE_COUNT_DEPTH
 */
constexpr int pruning_by_move_count_depth = 16;

/**
 * @brief historyによる枝刈りを行う深さの閾値
 *
 * PARAM_PRUNING_BY_HISTORY_DEPTH
 */
constexpr int pruning_by_history_depth = 9;

/**
 * @brief historyの値を調整する
 *
 * PARAM_REDUCTION_BY_HISTORY
 */
constexpr int reduction_by_history = 4000;

/**
 * @brief 使わない
 *        razoringはdepth < Depth::ONEでな実施しないため
 *
 * PARAM_RAZORING_MARGIN1
*/
constexpr int razoring_margin1 = 0;
/**
 * @brief 
 *
 * PARAM_RAZORING_MARGIN2
*/
constexpr int razoring_margin2 = 590;
/**
 * @brief 
 *
 * PARAM_RAZORING_MARGIN3
*/
constexpr int razoring_margin3 = 604;
/**
 * @brief 
 *
 * PARAM_REDUCTION_ALPHA
*/
constexpr int reduction_alpha = 135;

/**
 * @brief singularの場合の延長を行うかの探索深さ
 *
 * PARAM_SINGULAR_EXTENSION_DEPTH
 */
constexpr int singular_extension_depth = 7;

/**
 * @brief singularのmarginを計算するときの係数
 *
 * PARAM_SINGULAR_MARGIN
 */
constexpr int singular_margin = 194;

/**
 * @brief singular延長で浅い探索をするときの深さに関する係数
 */
constexpr int singular_search_depth_alpha = 20;

/**
 * @brief 静止探索での1手詰めを使うかのフラグ
 *
 * PARAM_QSEARCH_MATE1
 */
constexpr int quiet_search_mate1 = 1;

/**
 * @brief 通常探索での1手詰め
 *
 * PARAM_SEARCH_MATE1
*/
constexpr int search_mate1 = 1;

/**
 * @brief 1手詰めではなくN手詰めを使う
 *        そのN
 *
 * PARAM_WEAK_MATE_PLY
*/
constexpr int weak_mate_ply = 1;
}  // namespace parameter
}  // namespace search
#endif  // !SEARCH_PARAMETER_H_INCLUDED
