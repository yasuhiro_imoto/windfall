#include "stdafx.h"

#include "search_initialization.h"

#include "../utility/enum_cast.h"
#include "depth.h"
#include "engine.h"
#include "engine_component.h"
#include "search_parameter.h"
#include "thread.h"
#include "transposition_table.h"

namespace search {
void Initialize() {
  // 特にやることはない
}

namespace detail {
void InitializeParameter() {
  // 探索の閾値を動的に設定する場合はこの関数で読み込みなどを行う
}
}  // namespace detail

void Clear() {
  // 一つ前の探索が完了するのを待つ
  thread_pool.main_thread()->WaitForSearchFinished();

  // --------------------
  // 探索パラメータの設定
  // --------------------

  // 連続対局の中で設定を変更するためにここで行う
  detail::InitializeParameter();

  // --------------------
  // テーブルの初期化
  // --------------------

  // LMRで使うreduction tableの初期化
  // 探索パラメータの初期化の後に行う必要がある
  //
  // pvとnon pvのreductionの定数
  // 0.05とか変更するだけで勝率えらく変わるらしい

  // 念のために0で初期化
  std::memset(&reduction_table, 0, sizeof(reduction_table));

  for (int improving = 0; improving <= 1; ++improving) {
    for (int d = 1; d < 64; ++d) {
      for (int mc = 1; mc < 64; ++mc) {
        const double r =
            std::log(d) * std::log(mc) * parameter::reduction_alpha / 256;

        constexpr auto non_pv = enum_cast<>(NodeType::NonPV);
        constexpr auto pv = enum_cast<>(NodeType::PV);
        reduction_table[non_pv][improving][d][mc] =
            static_cast<int>(std::round(r)) * enum_cast<>(Depth::ONE);
        reduction_table[pv][improving][d][mc] = std::max(
            reduction_table[non_pv][improving][d][mc] - enum_cast<>(Depth::ONE),
            0);

        // non pvでimprovingでない時はreductionの量を増やす
        // TODO: 要調整
        if (!improving && reduction_table[non_pv][improving][d][mc] >=
                              2 * enum_cast<>(Depth::ONE)) {
          ++reduction_table[non_pv][improving][d][mc];
        }
      }
    }
  }

  // razor marginの初期化
  razor_margin[0] = parameter::razoring_margin1;
  razor_margin[1] = parameter::razoring_margin2;
  razor_margin[2] = parameter::razoring_margin3;

  // --------------------
  // 定跡の読み込み
  // --------------------

  // --------------------
  // 置換表のクリア
  // --------------------

  tt.clear();
  thread_pool.Clear();
}
}  // namespace search
