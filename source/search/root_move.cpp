#include "stdafx.h"

#include "root_move.h"

#include "../state_action/move.h"
#include "../state_action/move_generator.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "transposition_table.h"

namespace search {
bool RootMove::ExtractPonderFromTT(state_action::Position& position,
                                   const state_action::Move ponder_candidate) {
  if (!pv[0].ok()) {
    // pv[0] == Move::RESIGNがあり得る
    return false;
  }

  state_action::Rollback rb;
  bool tt_hit;

  position.StepForward(pv[0], rb);
  TTEntry* tte = tt.Probe(position.state()->key(), tt_hit);

  state_action::Move move;

  // TODO: なぜか全ての合法手を列挙した上でそれに含まれるかを調べている
  //       単純に合法手であるかをその手について調べればいいはず
  //       処理を効率化できるはずなので確認
  constexpr auto legal_all = state_action::MoveGeneratorType::LEGAL_ALL;
  const auto legal_list = state_action::MoveList<legal_all>(position);
  if (tt_hit) {
    // SMP safeにするためにコピー
    move = tte->move();
    if (legal_list.IsIn(move)) {
      goto FOUND;
    }
  }
  // 置換表になかったので、以前のiterationのpv[1]を試す
  move = ponder_candidate;
  if (legal_list.IsIn(move)) {
    goto FOUND;
  }

  position.StepBackward(pv[0]);
  return false;
FOUND:;
  position.StepBackward(pv[0]);
  pv.push_back(move);

  return true;
}
}  // namespace search
