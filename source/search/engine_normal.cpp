#include "stdafx.h"

#include "engine.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../utility/enum_cast.h"
#include "../utility/hash_key.h"
#include "../utility/level_assert.h"
#include "bound.h"
#include "depth.h"
#include "engine_component.h"
#include "engine_debug.h"
#include "history.h"
#include "move_picker.h"
#include "search_parameter.h"
#include "search_stack.h"
#include "thread.h"
#include "transposition_table.h"

#ifdef SEARCH_LOG
#include "../utility/logger.h"
#endif // SEARCH_LOG


namespace search {
#ifdef SEARCH_LOG
std::shared_ptr<spdlog::logger> tree_logger;

namespace detail {
std::string GetSpaces(const int count) {
  std::stringstream ss;
  for (int i = 0; i < count; ++i) {
    ss << '\t';
  }
  return ss.str();
}
}  // namespace detail

void OutputLog(const evaluation::Value value, const state_action::Move move,
               const int ply, const int line) {
  tree_logger->info(
    "{}value={}, move={}, ply={}, line={}",
    detail::GetSpaces(ply), static_cast<int>(value),
    detail::Move(move), ply, line);
}
void OutputLogIn(const state_action::Move move, const int ply, const int line) {
  tree_logger->info("{}>>move={}, ply={}, line={}", detail::GetSpaces(ply),
                    detail::Move(move), ply, line);
}
void OutputLogOut(const state_action::Move move, const int ply,
                  const int line) {
  tree_logger->info("{}<<move={}, ply={}, line={}", detail::GetSpaces(ply),
                    detail::Move(move), ply, line);
}

#define LogValue(value, move) OutputLog(value, move, ss->ply, __LINE__)
#define LogIn(move) OutputLogIn(move, ss->ply, __LINE__)
#define LogOut(move) OutputLogOut(move, ss->ply, __LINE__)
#else
#define LogValue(value, move) ((void)0)
#define LogIn(move) ((void)0)
#define LogOut(move) ((void)0)
#endif // SEARCH_LOG

template <NodeType Type>
evaluation::Value Search(state_action::Position& position, Stack* const ss,
                         evaluation::Value alpha, evaluation::Value beta,
                         const Depth depth, const bool cut_node,
                         const bool skip_early_pruning) {
  constexpr bool pv_node = Type == NodeType::PV;
  const bool root_node = pv_node && ss->ply == 0;

#ifdef CUCKOO
  // この局面から既出の局面に到達できるなら千日手が成立する
  // 千日手のスコアを早期に返すことで枝刈りできる
  evaluation::Value draw_value =
      evaluation::GetDrawValue(RepetitionState::DRAW, position.size());
  if (alpha < draw_value && !root_node /* && 局面が循環できるかの判定 */) {
#error Not Implemented to detect a cyclic path
    alpha = draw_value;
    if (alpha >= beta) {
      return alpha;
    }
  }
#endif  // CUCKOO

  if (depth < Depth::ONE) {
#ifdef SEARCH_LOG
    const auto tmp = QuietSearch<Type>(position, ss, alpha, beta);
    LogValue(tmp, state_action::Move::NONE);
    return tmp;
#else
    return QuietSearch<Type>(position, ss, alpha, beta);
#endif  // SEARCH_LOG
  }

  ASSERT_MSG_LV3(-evaluation::Value::INFINITE <= alpha && alpha < beta &&
                     beta <= evaluation::Value::INFINITE,
                 "alpha={}, beta={}", enum_cast<>(alpha), enum_cast<>(beta));
  ASSERT_MSG_LV3(pv_node || (alpha == beta - 1),
                 "pv_node={}, alpha={}, beta={}", pv_node, enum_cast<>(alpha),
                 enum_cast<>(beta));
  ASSERT_MSG_LV3(Depth::ZERO < depth && depth < Depth::MAX, "depth={}",
                 enum_cast<>(depth.value));
  // PV NodeではCut Nodeを呼び出さない
  ASSERT_MSG_LV3(!(pv_node && cut_node), "pv_node={}, cut_node={}", pv_node,
                 cut_node);
  // depthがDepth::Oneの倍数であるか確認
  ASSERT_MSG_LV3(Depth(static_cast<int>(depth)) == depth, "depth={}",
                 enum_cast<>(depth.value));

  // このノードからの読み筋
  // pv
  std::array<state_action::Move, max_ply + 1> pv;
  // 駒を捕獲する指し手 + 歩の成り
  // capturesSearched
  std::array<state_action::Move, 32> capture_searched;
#if defined(USE_AUTO_TUNE_PARAMETERS) || defined(USE_RANDOM_PARAMETERS)
  // 駒を捕獲しない指し手 - 歩の成り
  // 変数では配列を確保できないので大きめの値を設定
  std::array<state_action::Move, 128> quiet_searched;
#else
  // 駒を捕獲しない指し手 - 歩の成り
  // quietsSearched
  std::array<state_action::Move, parameter::quiet_search_count> quiet_searched;
#endif  // defined(USE_AUTO_TUNE_PARAMETERS) || defined(USE_RANDOM_PARAMETERS)

  // st
  state_action::Rollback rollback;
  // TT::Probeの戻り値
  // tte
  TTEntry* tte;
  // このノードのhash
  // posKey
  Key hash_key;

  // 置換表の指し手
  // ttMove
  state_action::Move tt_move;
  // MovePickerから受け取る一時変数
  // move
  state_action::Move move;
  // singular extensionの時に除外する指し手
  // excludedMove
  state_action::Move excluded_move;
  // このノードの最善手
  // bestMove
  state_action::Move best_move;

  // 延長する深さ
  // extension
  Depth extension;
  // 新しい残り探索深さ
  // newDepth
  Depth new_depth;

  // best_moveでの探索スコア
  // bestValue
  evaluation::Value best_value;
  // 一時変数
  // value
  evaluation::Value value;
  // 置換表でのスコア
  // ttValue
  evaluation::Value tt_value;
  // このノードの静的評価値の見積もり
  // eval
  evaluation::Value static_value;

  // 置換表にヒットしたか
  // ttHit
  bool tt_hit;
  // このノードで王手がかかっているか
  // inCheck
  bool checked;
  // 次の指し手で王手するか
  // givesCheck
  bool checking;
  // 評価値が直前のノードより上昇しているか
  //   このフラグを各種枝刈りのmarginの決定に用いる
  //   cf. Tweak probcut margin with 'improving' flag :
  //   https://github.com/official-stockfish/Stockfish/commit/c5f6bd517c68e16c3ead7892e1d83a6b1bb89b69
  //   cf. Use evaluation trend to adjust futility margin :
  //   https://github.com/official-stockfish/Stockfish/commit/65c3bb8586eba11277f8297ef0f55c121772d82c
  // improving
  bool improving;

  // captureOrPawnPromotion
  bool capture_or_fu_promotion;
  // LMRの時にfail
  // highが発生するなどしたので、元の残りの深さで探索を要求するフラグ
  // doFullDepthSearch
  bool full_depth_search_required;
  // move_countによって枝刈りするか
  // quietな指し手を生成しない
  // moveCountPruning
  bool move_count_pruning;
  // QUIETの指し手をスキップするか
  // skipQuiets
  bool skip_quiets;
  // 置換表の指し手が駒を捕る指し手であるか
  // ttCapture
  bool tt_capture;
  // PV Nodeで置換表にヒットして、かつBound::EXACT
  // pvExact
  bool pv_exact;

  // 指し手で移動した駒
  // movedPiece
  Piece moved_piece;

  // 調べた合法手の数
  // moveCount
  int move_count;
  // move_countのうちで駒を捕る指し手の数
  // capture_searched用のカウンタ
  // captureCount
  int capture_count;
  // move_countのうちで駒を捕らない指し手の数
  // quiet_searched用のカウンタ
  // quietCount
  int quiet_count;

  // ------------------------------
  // 1. 初期化
  // ------------------------------

  // thisThead
  Thread* const ptr_thread = position.ptr_thread();
  checked = position.checker();
  const Color us = position.side();
  move_count = capture_count = quiet_count = ss->move_count = 0;
  best_value = -evaluation::Value::INFINITE;

  if (ptr_thread == thread_pool.main_thread()) {
    // タイマーを確認
    static_cast<MainThread*>(ptr_thread)->CheckTime();
  }

  if (pv_node && ptr_thread->selective_depth < ss->ply + 1) {
    // selective depthをGUIに出力するために、PV Nodeなら深さを更新
    // selective depthは1から数える
    // plyは0から数える
    ptr_thread->selective_depth = ss->ply + 1;
  }

  if (!root_node) {
    // ------------------------------
    // 2. 探索終了と引き分けの確認
    // ------------------------------

    // 千日手、連続王手の千日手、優等・劣等局面の処理

    if (const auto draw_type = position.IsRepetition();
        draw_type != RepetitionState::NONE) {
      // 連続王手の千日手に対してdraw valueは詰みのスコアになるので、
      // root nodeからの手数に応じたスコアに変換する必要がある
#ifdef SEARCH_LOG
      const auto v = evaluation::ConvertValuueFromTT(
          evaluation::GetDrawValue(draw_type, position.side()), ss->ply);
      LogValue(v, state_action::Move::NONE);
      return v;
#else
      return evaluation::ConvertValuueFromTT(
          evaluation::GetDrawValue(draw_type, position.side()), ss->ply);
#endif  // SEARCH_LOG
    }

    if (thread_pool.stop.load(std::memory_order_relaxed) ||
        ss->ply >= max_ply || position.game_ply() > limit.max_game_ply) {
      // 停止命令が来た、最大手数を超えた
#ifdef SEARCH_LOG
      const auto v =
          evaluation::GetDrawValue(RepetitionState::DRAW, position.side());
      LogValue(v, state_action::Move::NONE);
      return v;
#else
      return evaluation::GetDrawValue(RepetitionState::DRAW, position.side());
#endif  // SEARCH_LOG
    }

    // ------------------------------
    // 3. 詰みまでの手数で枝刈り
    // ------------------------------

    // rootから5手目のノードとして、
    // このノードのスコアが5手以内で詰ますスコアを上回ることはない
    // 逆に5手以内で詰まされるスコアを下回ることもない
    // それらの値は窓の上限と下限になる
    alpha = std::max(evaluation::GetLoseValue(ss->ply), alpha);
    beta = std::min(evaluation::GetWinValue(ss->ply + 1), beta);
    if (alpha >= beta) {
      // beta cut
#ifdef SEARCH_LOG
      LogValue(alpha, state_action::Move::NONE);
#else
      return alpha;
#endif // SEARCH_LOG
    }
  }

  // ------------------------------
  // Stackの初期化
  // ------------------------------

  // rootからの手数
  ASSERT_MSG_LV3(0 <= ss->ply && ss->ply < max_ply, "ply={}", ss->ply);
  (ss + 1)->ply = ss->ply + 1;

  ss->current_move = (ss + 1)->excluded_move = best_move =
      state_action::Move::NONE;
  ss->continuation_history =
      &ptr_thread->continuation_history[Square::ZERO][Piece::EMPTY];

  (ss + 2)->killlers[0] = (ss + 2)->killlers[1] = state_action::Move::NONE;

  // 前の指し手で移動させた先のマス
  const Square previous_square = (ss - 1)->current_move.target();

  // statistics scoreを現在のノードの孫ノードのために0で初期化
  // statistics scoreは孫ノードに伝搬するので、最初の孫をの値だけを初期化する
  // これらの親ノードにおける値は、LMRでのreduction rulesに影響する
  (ss + 2)->score = 0;

  // ------------------------------
  // 4. 置換表を参照
  // ------------------------------

  // 前回の全探索の置換表の値を上書きする部分探索のスコアは不要なので、
  // excluded moveがある場合には異なるhash keyを用いる

  // このノードで探索から除外する
  excluded_move = ss->excluded_move;
  // 良い感じのhashを生成する
  // 動かす駒の値がhash keyで使われるようにビット位置を調整する
  hash_key = position.key() ^
             static_cast<Key>(static_cast<uint64_t>(excluded_move) << 16);

  tte = tt.Probe(hash_key, tt_hit);
  // 置換表でのスコア
  // singular searchとIIDとのスレッド競合を考慮して、tt_value,
  // tt_moveの順で取り出す必要があるらしい cf. More robust interaction of
  // singular search and iid :
  // https://github.com/official-stockfish/Stockfish/commit/16b31bb249ccb9f4f625001f9772799d286e2f04
  tt_value = tt_hit ? evaluation::ConvertValuueFromTT(tte->value(), ss->ply)
                    : evaluation::Value::NONE;
  // 置換表での指し手
  // root nodeであれば、multi pvでも現在の考えている指し手がbest
  // moveとして扱えるので、 それが置換表にあったものとして探索する
  tt_move = root_node ? ptr_thread->root_moves[ptr_thread->pv_index].pv[0]
                      : tt_hit ? tte->move() : state_action::Move::NONE;
  // 置換表からは普通の指し手、NONE、WINのいずれかが出てくる
  if (tt_move.ok()) {
    tt_move = position.ConvertMove16ToMove(tt_move);
    if (!position.IsPseudoLegal(tt_move)) {
      // 置換表の衝突で現局面では不正な指し手を取得している可能性がある
      // この指し手がcounter moveとして登録される可能性もあるので、
      // 影響範囲は見た目より広い
      //
      // 枝刈りの度に合法手かチェックすることになるので、このタイミングでチェックする
      //
      // NOTE: 置換表が十分に大きいなら衝突はかなり少ないはずなので、
      //       ここで判定せずに変な指し手が来ても大丈夫なようにした方が良いか
      tt_move = state_action::Move::NONE;
      // 指し手がillegalなら評価値も不適切なはず
      tt_value = evaluation::Value::NONE;

      tt_hit = false;
    }
  } else {
    ASSERT_MSG_LV1(tt_move == state_action::Move::NONE ||
                       tt_move == state_action::Move::WIN,
                   "tt_move={}", detail::Move(tt_move));
  }

  // PV nodeは極僅かしかないので、置換表の指し手では枝刈りしない
  // 置換表の方が深く探索しているなら枝刈りをする
  // ただし、値がNoneの場合は読み出し前に壊れた可能性がある
  if (!pv_node && tt_hit && tte->depth() >= depth &&
      tt_value != evaluation::Value::NONE &&
      (tt_value >= beta ? (tte->bound() & Bound::LOWER)
                        : (tte->bound() & Bound::UPPER)) != Bound::NONE) {
    // 置換表の値が下界(真の評価値はこれより大きい)で、
    // betaより大きいなら beta cut
    // 値が上界(真の評価値はこれより小さい）で、
    // 置換表の方が深く探索しているということは、
    // 置換表の方が甘い枝刈りで多く探索したはず
    // 置換表の値は信用できる

    if (tt_move.ok()) {
      // NONEおよびWINではない
      // 通常の指し手

      // beta cutになったのならば、この指し手をkillerに登録する
      // ただし、捕獲する指し手や成る指し手は(captureで生成する指し手なので)
      // killerに登録する価値はない
      if (tt_value >= beta) {
        if (!position.IsCapture(tt_move)) {
          ASSERT_MSG_LV1(position.IsPseudoLegal(tt_move), "tt_move={}, sfen={}",
                         detail::Move(tt_move), position.sfen());

          UpdateQuietStatistics(position, ss, tt_move, nullptr, 0,
                                GetStatisticsBonus(depth));
        }

        // 1手前はMove::NONEではない
        if ((ss - 1)->move_count == 1 &&
            !position.IsCaptureOrFuPromotion((ss - 1)->current_move)) {
          // 反駁された1手前の置換表の駒を捕らない指し手に対するペナルティを追加
          UpdateContinuationHistory(ss - 1, position[previous_square],
                                    previous_square,
                                    -GetStatisticsBonus(depth + Depth::ONE));
        }
      } else if (!position.IsCaptureOrPromotion(tt_move)) {
        // fails lowの時のquiet tt_moveに対するペナルティ
        const int penalty = -GetStatisticsBonus(depth);
        ptr_thread->main_history[tt_move.Serialize()][us] <<= penalty;
        UpdateContinuationHistory(ss, position.moved_piece(tt_move),
                                  tt_move.target(), penalty);
      }
    }

    LogValue(tt_value, tt_move);
    return tt_value;
  }

  // ------------------------------
  // 5. 宣言勝ち
  // ------------------------------

  if (tt_move == state_action::Move::WIN && !pv_node) {
    // 宣言勝ちが置換表に登録されていることがあるが、PV Nodeでは信用しない
#ifdef SEARCH_LOG
    const auto v = evaluation::GetWinValue(ss->ply + 1);
    LogValue(v, tt_move);
    return v;
#else
    return evaluation::GetWinValue(ss->ply + 1);
#endif  // SEARCH_LOG
  }
  if (!tt_move || pv_node) {
    // 置換表にないということは宣言勝ちの判定をしていないので、判定する
    // また、PV Nodeでは置換表の指し手を信用しないので、毎回判定する
    const state_action::Move m = position.Declarate();
    if (m) {
      // ここでは王手がかかっていても問題ない

      // 1手詰めと同等
      best_value = evaluation::GetWinValue(ss->ply + 1);
      tte->Save(hash_key, evaluation::ConvertValueForTT(best_value, ss->ply),
                false, Bound::EXACT, Depth::MAX, m, ss->static_evaluation);

      LogValue(best_value, tt_move);
      return best_value;
    }
  }

  ASSERT_MSG_LV1(
      tt_move == state_action::Move::NONE || position.IsPseudoLegal(tt_move),
      "tt_move={}, sfen={}", detail::Move(tt_move), position.sfen());

  // ------------------------------
  // 1手詰め
  // ------------------------------

  if (parameter::search_mate1 && !root_node && !tt_hit && !checked) {
    ASSERT_MSG_LV3(!position.checked(), "sfen={}", position.sfen());

    // root nodeでは指し手の入れ替えが発生するので、
    // 1手詰めの判定はややこしくなるので行わない
    //
    // 置換表にヒットしたときも既に判定を行っているはずなので、省略
    //
    // 残りの探索深さが少ないとわざわざ判定を行っている見返りが少ない
    // この後の処理でどうせ発見される
    // ただし、静止探索で行っているので、Depth::ONEでも判定した方がよさそう

    if (parameter::weak_mate_ply == 1) {
      move = position.Mate1ply();
      if (move) {
        static_assert(Depth::MAX >= Depth::NONE, "");

        // 1手詰めなので、確実にalpha以上のはず
        // 次の手で詰む
        best_value = evaluation::GetWinValue(ss->ply + 1);
        tte->Save(hash_key, evaluation::ConvertValueForTT(best_value, ss->ply),
                  false, Bound::EXACT, Depth::MAX, move, best_value);
        LogValue(best_value, move);
        return best_value;
      }
    } else {
      // N手詰めの判定
    }
  }

  // ------------------------------
  // 6. 静的な局面の評価
  // ------------------------------

  // 評価関数をいつ呼び出すかはいくつか考えられるが、
  // かなりの確率でどうせ呼び出すので、今のタイミングで呼び出す
  ss->static_evaluation = static_value = evaluation::Evaluate(position);
  if (checked) {
    // 反復深化でこのノードを再訪したときも、このノードの評価値を利用しないので、
    // 置換表に入れる意味がない

    ss->static_evaluation = static_value = evaluation::Value::NONE;
    improving = false;
    // 王手の時はearly pruningをしない
    goto moves_loop;
  } else if (tt_hit) {
    // 置換表にヒットしたならば、評価値が記録されているはずなので、それを取り出す
    // あとで置換表に書き込むときに使える
    // また枝刈りにこの値を使う

    // tt_valueがこの局面の評価値の見積もりとして適切かを判定する
    if (tt_value != evaluation::Value::NONE &&
        (tte->bound() &
         (tt_value > static_value ? Bound::LOWER : Bound::UPPER)) !=
            Bound::NONE) {
      // tt_valueの方が大きくBound::LOWERなら真の値はこれより大きいはずだから、
      // tt_valueは見積もりとして適切
      //
      // tt_valueの方が小さくBound::UPPERなら真の値はこれより小さいはずだから、
      //tt_valueは見積もりとして適切
      static_value = tt_value;
    }
  } else {
    // 置換表になかったので、せっかく呼び出した評価値を保存しておく
    tte->Save(hash_key, evaluation::Value::NONE, false, Bound::NONE,
              Depth::NONE, state_action::Move::NONE, ss->static_evaluation);
    // ノードごとに評価関数を呼び出すので、評価値そのものにあまり意味はないが、
    // Mate1Plyを呼び出した証という意味はある
  }

  // 評価値の改善のフラグ
  // 改善しているなら枝刈りを甘くする
  // Value::NONEの場合は王手がかかっているということなので、枝刈りを甘くする必要がある
  // ※ Value::NONE == 32000なので、これより大きな評価値はない
  improving = ss->static_evaluation >= (ss - 2)->static_evaluation ||
              (ss - 2)->static_evaluation == evaluation::Value::NONE;

  if (skip_early_pruning) {
    goto moves_loop;
  }

  // ------------------------------
  // 評価値ベースの枝刈り
  // ------------------------------

  // 王手がかかっているときはここはスキップされる

  ASSERT_MSG_LV3(evaluation::IsFinite(static_value), "static_value={}",
                 enum_cast<>(static_value));

  // ------------------------------
  // 7. Razoring
  // ------------------------------

  // 残りの探索深さが少ないときに、その手数でalphaを上回りそうにない時用の枝刈り
  if (!pv_node && depth < razoring_depth &&
      static_value <= alpha - razor_margin[depth]) {
    // 残りの探索深さが1, 2の場合のみここに到達
    // null windowでのQuietSearchの結果がalpha - razor_marginを上回るか調べる

    constexpr auto threshold = Depth(razoring_depth - 1);

    const evaluation::Value ra =
        alpha - (depth >= threshold) * razor_margin[depth];
    const evaluation::Value v =
        QuietSearch<NodeType::NonPV>(position, ss, ra, ra + 1);
    if (depth < threshold || v <= ra) {
      LogValue(v, state_action::Move::NONE);
      return v;
    }
  }

  // ------------------------------
  // 8. Futility Pruning
  // ------------------------------

  // 残りの探索深さで評価値が変動する幅をfutility marginとする
  // 静的評価値からこの値を引いてもbetaより大きいなら、beta cutできる
  if (!root_node && depth < parameter::futility_return_depth &&
      static_value - GetFutilityMargin(depth, improving) >= beta &&
      static_value < evaluation::Value::KNOWN_WIN) {
    // 詰みの絡みはmate distance
    // pruningで枝刈りされるはずなので、ここには含まない

    // static_value - GetFutilityMargin(depth, improving)
    // より単にstatic_valueを返した方が良いらしい cf. Simplify futility pruning
    // return value :
    // https://github.com/official-stockfish/Stockfish/commit/f799610d4bb48bc280ea7f58cd5f78ab21028bf5

    LogValue(static_value, state_action::Move::NONE);
    return static_value;
  }

  // ------------------------------
  // 9. 検証用の探索つきのnull move探索
  // ------------------------------

  if (!pv_node && static_value >= beta &&
      (ss->static_evaluation >=
           beta - parameter::null_move_margin * (static_cast<int>(depth) - 6) ||
       depth >= 13)) {
    // null moveをしてもbetaを超えそう

    ASSERT_MSG_LV3(static_value - beta >= evaluation::Value::ZERO,
                   "static_value={}, beta={}", enum_cast<>(static_value),
                   enum_cast<>(beta));

    // 残り探索深さと評価値によるnull moveの深さを動的に減らす
    const auto term1 =
        (parameter::null_move_dynamic_alpha +
         static_cast<int>(parameter::null_move_dynamic_beta * depth)) /
        256;
    const auto term2 =
        std::min<int>(enum_cast<>(static_value - beta) /
                          enum_cast<>(evaluation::PieceScore::FU),
                      3);
    const Depth d = Depth(term1 + term2);

    ss->current_move = state_action::Move::NONE;
    ss->continuation_history =
        &ptr_thread->continuation_history[Square::ZERO][Piece::EMPTY];

    ASSERT_MSG_LV3(!position.checked(), "sfen={}", position.sfen());

    position.StepForwardNull(rollback);
    LogIn(state_action::Move::NONE);

    evaluation::Value null_score = -Search<NodeType::NonPV>(
        position, ss + 1, -beta, -beta + 1, depth - d, !cut_node, true);

    LogOut(state_action::Move::NONE);
    position.StepBackwardNull();

    if (null_score >= beta) {
      // null moveでも評価値が高くなりそうなのはわかったので、より詳しく調べる

      if (null_score >= evaluation::Value::WIN_IN_MAX_PLY) {
        // 証明されていないmate scoreは返さない
        null_score = beta;
      }
      if (abs(beta) < evaluation::Value::KNOWN_WIN &&
          depth < parameter::null_move_return_depth) {
        LogValue(null_score, state_action::Move::NONE);
        return null_score;
      }

      // null moveを使わずに、現在のノードを同じ手番、同じ深さで
      // 探索して本当にbetaを超えるか確かめる
      const evaluation::Value v = Search<NodeType::NonPV>(
          position, ss, beta - 1, beta, depth - d, false, true);
      if (v >= beta) {
        LogValue(null_score, state_action::Move::NONE);
        return null_score;
      }
    }
  }

  // ------------------------------
  // 10. Prob Cut
  // ------------------------------

  // このノードで非常に良い駒を捕る指し手があり、探索深さを減らしてみても
  // betaを十分に上回るなら、このノードをほぼ安全に枝刈りできる
  if (!pv_node && depth >= parameter::prob_cut_depth &&
      abs(beta) < evaluation::Value::WIN_IN_MAX_PLY) {
    // 各探索のハイパーパラメータや置換表の衝突具合によっては
    // 深く探索しているときのnull move searchの次の深さでここに到達できると思われる
    // null move searchの次の深さでprob cutを行いたくない場合は明示的に
    // 条件をしている素必要がある
    // BOOST_ASSERT_MSG((ss - 1)->current_move.ok(),
    //                 boost::str(boost::format("current_move=0x%1$X") %
    //                            static_cast<uint64_t>(ss[-1].current_move))
    //                     .c_str());
    // ASSERT_LV3((ss - 1)->current_move.ok());

    const evaluation::Value rb =
        std::min(beta + parameter::prob_cut_margin1 -
                     parameter::prob_cut_margin2 * improving,
                 evaluation::Value::INFINITE);

    // rb - ss->static_evaluationを超える駒を捕る指し手のみを生成する
    MovePickerProbCut mp(position, tt_move, rb - ss->static_evaluation,
                         &ptr_thread->capture_history);

    int prob_cut_count = 0;
    // 3回までトライする
    // それ以上は無駄と判断
    // cf. Do move-count pruning in probcut :
    // https://github.com/official-stockfish/Stockfish/commit/b87308692a434d6725da72bbbb38a38d3cac1d5f
    while ((move = mp.Next()) != state_action::Move::NONE &&
           prob_cut_count < 3) {


      ASSERT_MSG_LV3(position.IsPseudoLegal(move), "move={}, sfen={}",
                     detail::Move(move), position.sfen());
      ASSERT_LV3(position[position.ou_square(position.side())] ==
                 Piece(position.side(), Piece::OU));

      if (!position.IsLegal(move)) {
        continue;
      }

      ASSERT_MSG_LV3(position.IsPseudoLegal(move) && position.IsLegal(move),
                     "move={}, sfen={}", detail::Move(move), position.sfen());


      ++prob_cut_count;

      ss->current_move = move;
      ss->continuation_history =
          &ptr_thread->continuation_history[move.target()]
                                           [position.moved_piece(move)];

      // NOTE: パラメータを調整するときはstatic_assert自体が成立しない
      static_assert(parameter::prob_cut_depth == 5, "");
      ASSERT_MSG_LV3(depth >= parameter::prob_cut_depth, "depth={}",
                     enum_cast<>(depth.value));

      position.StepForward(move, rollback);
      LogIn(move);

      // 移動後は王手されていないはず
      ASSERT_MSG_LV3(!position.IsEffected(position.side(),
                                          position.ou_square(~position.side())),
                     "move={}, sfen={}", detail::Move(move), position.sfen());

      // この手がある程度は良いことを確認する予備探索
      value = -QuietSearch<NodeType::NonPV>(position, ss + 1, -rb, -rb + 1);
      if (value >= rb) {
        value = -Search<NodeType::NonPV>(
            position, ss + 1, -rb, -rb + 1,
            depth - (parameter::prob_cut_depth - 1), !cut_node, false);
      }
      LogOut(move);
      position.StepBackward(move);

      if (value >= rb) {
        LogValue(value, move);
        return value;
      }
    }
  }

  // ------------------------------
  // 11. 多重反復深化
  // ------------------------------

  // 王手の時はスキップする

  // 残りの探索深さがある程度あるのに置換表に指し手が登録されていないとき、
  // たぶん、置換表のエントリーを上書きされた、
  // 浅い探索をしてその指し手を置換表の指し手として代用する
  // 置換表のサイズが十分にある時はほとんど効果はないはず

  if (depth >= 8 && !tt_move) {
    const Depth d = 3 * depth / 4 - 2;
    Search<Type>(position, ss, alpha, beta, d, cut_node, true);

    tte = tt.Probe(hash_key, tt_hit);
    tt_value = tt_hit ? evaluation::ConvertValuueFromTT(tte->value(), ss->ply)
                      : evaluation::Value::NONE;
    tt_move = tt_hit ? position.ConvertMove16ToMove(tte->move())
                     : state_action::Move::NONE;
    // 宣言勝ちは上でチェックしたので、Move::WINはここではないはず
    if (!position.IsPseudoLegal(tt_move)) {
      // 最初に置換表から取得したときと同じ処理をする
      tt_move = state_action::Move::NONE;
      // 指し手がillegalなら評価値も不適切なはず
      tt_value = evaluation::Value::NONE;

      tt_hit = false;
    }
  }

moves_loop:;
  // 王手がかかっている局面ではここから探索

  // 0: counter move   : ある指し手に対する応手
  // 1: follow up move : 2手前の自分の指し手の継続手
  // 2: なし
  // 3: follow up move : 4手前の自分の指し手の継続手
  const PieceTargetHistory* continuation_history[4] = {
      (ss - 1)->continuation_history, (ss - 2)->continuation_history, nullptr,
      (ss - 4)->continuation_history};

  const Piece previous_piece = position[previous_square];
  // 直前の指し手が存在しない場合、counter moveも存在しない
  // 探索の初回とnull moveの次が考えられる
  // NOTE: counter_movesの戻り値がMoveではなくキャストが発生している
  // TODO: CounterMoveHistoryをただの2次元配列にした方がシンプル
  const state_action::Move counter_move =
      ss[-1].current_move &&
              position.IsPseudoLegal(
                  ptr_thread->counter_moves[previous_square][previous_piece])
          ? ptr_thread->counter_moves[previous_square][previous_piece]
          : state_action::Move(state_action::Move::NONE);
  // 初めての探索で一つ前の指し手が存在しない場合は、
  // おかしな値を使うことになるので、このアサーションは成立しない
  // ASSERT_LV1(previous_piece == Piece::EMPTY ||
  //           previous_piece.color() != position.side());
  ASSERT_MSG_LV1(counter_move == state_action::Move::NONE ||
                     position.IsPseudoLegal(counter_move),
                 "counter_move={}, sfen={}", detail::Move(counter_move),
                 position.sfen());

  MovePickerNormal mp(position, tt_move, depth, &ptr_thread->main_history,
                      &ptr_thread->capture_history, continuation_history,
                      counter_move, ss->killlers);

#ifdef __GNUC__
  // gccでコンパイルした場合に変数が未初期化という警告を回避
  value = best_value;
#endif  // __GNUC__

  // 指し手生成の時に駒を捕らない指し手をスキップするかどうか
  skip_quiets = false;

  // 置換表の指し手がcapture_or_promotionなら高確率でこの指し手が最善なので、
  // 他の指し手をあまり読まなくても大丈夫
  // このノードの全ての指し手のreductionを増やす
  tt_capture = false;
  // 読み筋としていい状態なので、できるだけ深く読んだ方が良いはず
  // reductionを減らす
  pv_exact = pv_node && tt_hit && tte->bound() == Bound::EXACT;

  // ------------------------------
  // 12. 一つずつ指し手を調べる
  // ------------------------------

  // 指し手がなくなるか、beta cutが発生するまで全ての疑似合法手を調べる

  while ((move = mp.Next(skip_quiets)) != state_action::Move::NONE) {
    ASSERT_MSG_LV3(position.IsPseudoLegal(move), "move={}, sfen={}",
                   detail::Move(move), position.sfen());

    if (move == excluded_move) {
      continue;
    }

    if (root_node &&
        std::find(ptr_thread->root_moves.begin() + ptr_thread->pv_index,
                  ptr_thread->root_moves.end(),
                  move) == ptr_thread->root_moves.end()) {
      // Root Nodeではroot_movesに含まれない指し手はスキップする
      continue;
    }

    // StepForwardの回数を増やす
    // 枝刈りのためにSearchを呼び出したりするので、このタイミングで増やす
    // 合法手でなかった場合は増やした分を戻す
    ss->move_count = ++move_count;

    if (pv_node) {
      // 次の局面のために初期化
      (ss + 1)->pv = nullptr;
    }

    // ------------------------------
    // 探索深さの延長
    // ------------------------------

    extension = Depth::ZERO;

    capture_or_fu_promotion = position.IsCaptureOrFuPromotion(move);
    moved_piece = position.moved_piece(move);

    checking = position.IsCheckMove(move);

    // move countベースの枝刈りをするかどうかのフラグ
    move_count_pruning =
        depth < parameter::pruning_by_move_count_depth &&
        move_count >= GetFutilityMoveCount(improving, depth);

    // ------------------------------
    // 13. singular延長と王手延長
    // ------------------------------

    // sをマージンとして探索窓(alpha - s, beta - s)で1手以外は全てfail
    // lowであり、 この1手が(探索窓(alpha, beta)でfail
    // highしたなら、指し手はsingularで延長されるべき
    // これを調べるためにtt_move以外の探索を減らして探索する
    // その結果がtt_value - s以下ならtt_moveの指し手を延長する
    //
    // 将棋だと1手以外は全て悪い場合が良くあるので、
    // 1割くらいの指し手がsingularになるようにする
    //
    // 一つの指し手だけが他より特に良いなら相手も同じ手を読んでいる可能性が高い
    // 同じ読み筋ならより深く読んだ方が有利なので、延長すると強くなる

    if (depth >= parameter::singular_extension_depth &&
        move == tt_move && !root_node &&
        !excluded_move &&  // 再帰的にはsingular延長はしない
        tt_value != evaluation::Value::NONE &&
        (tte->bound() & Bound::LOWER) != Bound::NONE &&
        tte->depth() >= depth - 3 &&
        position.IsLegal(move)) {
      // 詰みのスコアでもsingular延長はした方が良いらしい

      // tt_move != Move::NONEなので、この局面についてある程度は調べている
      // さもないとshingularの指し手以外に有望な指し手がないか調べるために
      // 大きなコストを伴いかねない

      // 割る数はDepthの調整が入る
      const evaluation::Value rb = std::max(
          tt_value - static_cast<evaluation::Value>(static_cast<int>(
                         parameter::singular_margin * depth / Depth(64))),
          -evaluation::Value::MATE);
      // 割る数はDepthの調整が入る
      const Depth d =
          depth * parameter::singular_search_depth_alpha / Depth(32);

      // 再帰的な探索のために除外
      ss->excluded_move = move;
      // 浅い探索
      value =
          Search<NodeType::NonPV>(position, ss, rb - 1, rb, d, cut_node, true);
      // 探索が終わったので除外設定を解除
      ss->excluded_move = state_action::Move::NONE;

      if (value < rb) {
        // 置換表の指し手以外は全てfail low
        // singular延長
        extension = Depth::ONE;
        // 延長した回数の統計を録った方が良い？
      }
    } else if (checking && !move_count_pruning && position.IsSeeGe(move)) {
      // 王手と成る指し手でSEE >= 0なら探索を延長
      extension = Depth::ONE;
    }

    // ------------------------------
    // 1手進める前の枝刈り
    // ------------------------------

    // 再帰的に探索するときの残り深さ
    new_depth = depth - Depth::ONE + extension;

    // ------------------------------
    // 14. 浅い深さでの枝刈り
    // ------------------------------

    // historyに使う
    const Square moved_square = move.target();
    if (!root_node && best_value > evaluation::Value::LOSE_IN_MAX_PLY) {
      if (!capture_or_fu_promotion && !checking) {
        // move countに基づいた枝刈り
        if (move_count_pruning) {
          skip_quiets = true;
          continue;
        }

        // 次のLMR探索のための軽減された深さ
        const int lmr_depth = static_cast<int>(std::max<Depth>(
            new_depth -
                GetReductionDepth<pv_node>(improving, depth, move_count),
            Depth::ZERO));
        // counter movesに基づいた枝刈り
        // historyの値が悪いものはスキップ
        if (lmr_depth < parameter::pruning_by_history_depth &&
            (*continuation_history[0])[moved_square][moved_piece] <
                counter_move_prune_threshold &&
            (*continuation_history[1])[moved_square][moved_piece] <
                counter_move_prune_threshold) {
          continue;
        }

        // 子ノードを展開する前にfutility
        // pruningの対象になりそうなら親ノード時点で枝刈り
        if (lmr_depth < parameter::futility_at_parent_node_depth && !checked &&
            enum_cast<>(ss->static_evaluation) +
                    parameter::futility_at_parent_node_margin1 +
                    parameter::futility_margin_beta * lmr_depth <=
                enum_cast<>(alpha)) {
          continue;
        }
        // SEEで値が低い指し手を枝刈り
        if (!position.IsSeeGe(
                move,
                evaluation::Value(-parameter::futility_at_parent_node_gamma1 *
                                  lmr_depth * lmr_depth))) {
          continue;
        }
      } else if (extension == Depth::ZERO &&
                 !position.IsSeeGe(
                     move, evaluation::Value(
                               -static_cast<int>(
                                   parameter::futility_at_parent_node_gamma2 *
                                   depth * depth) /
                               enum_cast<>(Depth::ONE)))) {
        // ↑Depthの2乗なので、キャストだけではスケールが1回分不足する
        continue;
      }
    }

    // ------------------------------
    // 1手進める
    // ------------------------------

    // root nodeでは合法手であることはわかっているので、確認不要
    // 非合法手はほとんど含まれていないので、局面を進める直前まで遅延させた方が効率的
    if (!root_node && !position.IsLegal(move)) {
      // 足してしまったカウントを戻す
      ss->move_count = --move_count;
      continue;
    }

    // root moveは作成時に合法手であることを確認している
    ASSERT_MSG_LV3(position.IsPseudoLegal(move) && position.IsLegal(move),
                   "move={}, sfen={}", detail::Move(move), position.sfen());

    if (move == tt_move && capture_or_fu_promotion) {
      tt_capture = true;
    }

    // 現在のスレッドで探索している指し手を保存
    ss->current_move = move;
    ss->continuation_history =
        &ptr_thread->continuation_history[moved_square][moved_piece];

    // ------------------------------
    // 15. 指し進める
    // ------------------------------

    position.StepForward(move, rollback, checking);
    LogIn(move);

    // 移動後は王手されていないはず
    ASSERT_MSG_LV3(!position.IsEffected(position.side(),
                                        position.ou_square(~position.side())),
                   "move={}, sfen={}", detail::Move(move), position.sfen());

    // ------------------------------
    // 16. depthを減らして探索
    // ------------------------------

    // LMR: Late Move Reduction

    // move countが多きいものは探索深さを減らしてざっくりと調べる
    // alphaを更新しそう(fail highが起きたら)full depthで調べ直す
    if (depth >= 3 && move_count > 1 &&
        (!capture_or_fu_promotion || move_count_pruning)) {
      Depth reduction =
          GetReductionDepth<pv_node>(improving, depth, move_count);
      if (capture_or_fu_promotion) {
        reduction -= reduction != Depth::ZERO ? Depth::ONE : Depth::ZERO;
      } else {
        if (tt_capture) {
          // 捕る指し手ならそんなに読まなくても大丈夫
          reduction += Depth::ONE;
        }

        if (cut_node) {
          // cut_nodeはpv_nodeではないという条件を含む

          // historyの値が悪いので、読む量を減らす
          reduction += Depth(2);
        }

        ss->score =
            ptr_thread->main_history[move.Serialize()][~position.side()] +
            (*continuation_history[0])[moved_square][moved_piece] +
            (*continuation_history[1])[moved_square][moved_piece] +
            (*continuation_history[3])[moved_square][moved_piece] -
            parameter::reduction_by_history;

        if (ss->score >= 0 && (ss - 1)->score < 0) {
          reduction -= Depth::ONE;
        } else if ((ss - 1)->score >= 0 && ss->score < 0) {
          reduction += Depth::ONE;
        }

        reduction = std::max<Depth>(
            Depth::ZERO,
            Depth(static_cast<int>(reduction) - ss->score / 20000));
      }

      // depth >= 3なので、QuietSearchは呼ばれない
      // move_count > 1なので、このノードは2手目以降
      // よって、NonPVの方を呼び出すべき
      const Depth d = std::max<Depth>(new_depth - reduction, Depth::ONE);
      value = -Search<NodeType::NonPV>(position, ss + 1, -(alpha + 1), -alpha,
                                       d, true, false);

      // alphaを更新しそうなので、調べ直す
      full_depth_search_required = (value > alpha) && (d != new_depth);
    } else {
      full_depth_search_required = !pv_node || move_count > 1;
    }

    // ------------------------------
    // 17. LMRがスキップされたかfail highならfull depth search
    // ------------------------------

    if (full_depth_search_required) {
      value = -Search<NodeType::NonPV>(position, ss + 1, -(alpha + 1), -alpha,
                                       new_depth, !cut_node, false);
    }

    if (pv_node &&
        (move_count == 1 || (value > alpha && (root_node || value < beta)))) {
      // pv nodeで上のfull depth searchがfail highになったならpv
      // nodeとして探索し直す value >=
      // betaなら、正確な値を求めることに意味はないので、そのままbeta cutする

      (ss + 1)->pv = pv.data();
      (ss + 1)->pv[0] = state_action::Move::NONE;
      // full depth searchの場合はcut nodeであってはいけない
      value = -Search<NodeType::PV>(position, ss + 1, -beta, -alpha, new_depth,
                                    false, false);
    }

    // ------------------------------
    // 18. 局面を戻す
    // ------------------------------

    LogOut(move);
    position.StepBackward(move);

    ASSERT_MSG_LV3(evaluation::IsFinite(value), "value={}", enum_cast<>(value));

    // ------------------------------
    // 19. 探索終了を確認
    // ------------------------------

    // 停止シグナルが来たときの探索結果は信頼できないので、
    // best moveを更新しない
    // PVや置換表を汚さずに終了する
    if (thread_pool.stop.load(std::memory_order_relaxed)) {
      return evaluation::Value::ZERO;
    }

    // ------------------------------
    // root node用の特別な処理
    // ------------------------------

    if (root_node) {
      RootMove& rm = *boost::range::find(ptr_thread->root_moves, move);

      if (move_count == 1 || value > alpha) {
        // PVの指し手か新しいbest move
        // スコアを記録しておく
        // iterationごとにソートするので、指し手が入れ替わる

        rm.score = value;
        rm.selective_depth = ptr_thread->selective_depth;
        // PVは変化するはずなので、一旦はリセット
        rm.pv.resize(1);

        // 1手は進めたので、何らかのPVはあるはず
        ASSERT_LV3((ss + 1)->pv);
        // PVをコピー
        for (state_action::Move* m = (ss + 1)->pv;
             *m != state_action::Move::NONE; ++m) {
          rm.pv.push_back(*m);
        }

        if (move_count > 1 && ptr_thread == thread_pool.main_thread()) {
          ++static_cast<MainThread*>(ptr_thread)->best_move_changes;
        }
      } else {
        // ソートで指し手の順序が入れ替わるのを防ぐために定数をセットする
        // オーダリングのコストを最小限にする
        rm.score = -evaluation::Value::INFINITE;
      }
    }

    // ------------------------------
    // alphaの更新
    // ------------------------------

    if (value > best_value) {
      best_value = value;
      if (value > alpha) {
        ASSERT_MSG_LV3(position.IsPseudoLegal(move) && position.IsLegal(move),
                       "move={}, sfen={}", detail::Move(move), position.sfen());

        best_move = move;

        // fail highの場合もPVを更新する
        if (pv_node && !root_node) {
          UpdatePV(ss->pv, move, (ss + 1)->pv);
        }

        if (pv_node && value < beta) {
          alpha = value;

          // 相手からの詰みがあるかを調べるならここに書く
        } else {
          // fail high

          // non pvでは探索窓の幅が0なので、alphaを更新した時点でvalue >=
          // betaが成立する beta cut
          BOOST_ASSERT_MSG(value >= beta,
                           boost::str(boost::format("value=%1%, beta=%2%") %
                                      enum_cast<>(value) % enum_cast<>(beta))
                               .c_str());
          ASSERT_LV3(value >= beta);
          // fail
          // highのときには、負のstatScoreをリセットしたほうが良いらしい。
          // cf. Reset negative statScore on fail high :
          // https://github.com/official-stockfish/Stockfish/commit/b88374b14a7baa2f8e4c37b16a2e653e7472adcc
          ss->score = std::max(ss->score, 0);

          break;
        }
      }
    }

    if (move != best_move) {
      if (capture_or_fu_promotion &&
          capture_count < static_cast<int>(capture_searched.size())) {
        capture_searched[capture_count++] = move;
      }
      if (!capture_or_fu_promotion &&
          quiet_count < static_cast<int>(quiet_searched.size())) {
        quiet_searched[quiet_count++] = move;
      }
    }
  }

  // ------------------------------
  // 20. 詰みとstalemateを確認
  // ------------------------------

  using LegalList =
      state_action::MoveList<state_action::MoveGeneratorType::LEGAL>;
  ASSERT_MSG_LV5(
      move_count || !checked || excluded_move || LegalList(position).empty(),
      "sfen={}", position.sfen());

  if (!move_count) {
    // 詰みか唯一の指し手を除外している
    // -> excluded_moveの場合はsingular extension中
    //    tt_moveが除外されている
    best_value = excluded_move ? alpha : evaluation::GetLoseValue(ss->ply);
  } else if (best_move) {
    ASSERT_MSG_LV3(
        position.IsPseudoLegal(best_move) && position.IsLegal(best_move),
        "move={}, sfen={}", detail::Move(best_move), position.sfen());

    if (position.IsCaptureOrFuPromotion(best_move)) {
      UpdateCaptureStatistics(position, best_move, capture_searched.data(),
                              capture_count, GetStatisticsBonus(depth));
    } else {
      // 駒を捕獲しない指し手なので、killer move, history, counter movesを更新
      UpdateQuietStatistics(position, ss, best_move, quiet_searched.data(),
                            quiet_count, GetStatisticsBonus(depth));
    }

    if ((ss - 1)->move_count == 1 && !position.captured_piece()) {
      // 反駁された1手前の置換表の駒を捕らない指し手に対するペナルティを追加する
      // 1手前は置換表の指し手なので、null moveではない
      UpdateContinuationHistory(ss - 1, position[previous_square],
                                previous_square,
                                -GetStatisticsBonus(depth + Depth::ONE));
    }
  } else if (depth >= 3 && position.captured_piece() != Piece::EMPTY &&
             (ss - 1)->current_move.ok()) {
    UpdateContinuationHistory(ss - 1, position[previous_square],
                              previous_square, GetStatisticsBonus(depth));
  }

  // ------------------------------
  // 置換表に保存
  // ------------------------------

  if (!excluded_move) {
    ASSERT_MSG_LV1(depth >= Depth::NONE, "tt_depth={}",
                   enum_cast<>(depth.value));

    // beta以上ならbeta cutされる
    // この時、残りの指し手を調べていないから真の評価値はこれよりも大きいと考えられる
    // よって、この値は下界
    // pv nodeなら枝刈りをしていないので、正確な値のはず
    // それ以外の条件ならば、pv
    // nodeではなく、探索窓の幅を0にして枝刈りをしているので、上界
    // ただし、詰みの場合は違う手数の詰みがあってスコアが変動するかもしれないので上界と同様に扱う
    tte->Save(
        hash_key, evaluation::ConvertValueForTT(best_value, ss->ply), false,
        best_value >= beta ? Bound::LOWER
                           : pv_node && best_move ? Bound::EXACT : Bound::UPPER,
        depth, best_move, ss->static_evaluation);
  }

  ASSERT_MSG_LV3(evaluation::IsFinite(best_value), "best_value={}",
                 enum_cast<>(best_value));

  LogValue(best_value, best_move);
  return best_value;
}

template evaluation::Value Search<NodeType::PV>(
    state_action::Position& position, Stack* const ss, evaluation::Value alpha,
    evaluation::Value beta, const Depth depth, const bool cut_node,
    const bool skip_early_pruning);
template evaluation::Value Search<NodeType::NonPV>(
    state_action::Position& position, Stack* const ss, evaluation::Value alpha,
    evaluation::Value beta, const Depth depth, const bool cut_node,
    const bool skip_early_pruning);
}  // namespace search
