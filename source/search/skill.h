#pragma once
#ifndef SKILL_H_INCLUDED
#define SKILL_H_INCLUDED

#include "../state_action/move.h"
#include "depth.h"

namespace search {
/**
 * @brief 手加減の度合い
 */
struct Skill {
  /**
   * @brief
   * @param lv 手加減のレベル
   *           0が最弱、20が手加減なし
   * @return
   */
  explicit Skill(const int lv) : level(lv) {}

  /**
   * @brief 手加減が有効かどうか
   * @return
   */
  bool enabled() const { return level < 20; }

  /**
   * @brief depthがSkill Levelに到達したかを判定する
   *
   * depthをSkill Levelと同じくらいにしたいので疎の判定
   * @param depth
   * @return
   */
  bool IsTimeToPick(const Depth depth) const {
    return static_cast<int>(depth) == 1 + level;
  }

  /**
   * @brief 指し手を選ぶ
   *
   * 手加減が有効な時はmulti_pv=4で探索
   * best moveをlevelに依存する統計ルールに基づいたRootMovesから選ぶ
   * Heinz van Saanenのアイデア
   * @param multi_pv
   * @return
   * pick_best
   */
  state_action::Move PickBest(const size_t multi_pv);

  int level;
  state_action::Move best = state_action::Move::NONE;
};
}  // namespace search
#endif  // !SKILL_H_INCLUDED
