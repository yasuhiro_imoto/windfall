#include "stdafx.h"

#include "move_picker.h"

#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../rule/square.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "../utility/level_assert.h"
#include "depth.h"
#include "history.h"
#include "pick_state.h"
#include "search_limit.h"

namespace search {
namespace {
/**
 * @brief 駒損が小さいようにLVAを優先的に選択するためのテーブル
 *        LVA: least valuable aggressor
 * LVATable
 */
constexpr std::array<int, Piece::WHITE> lva_table = {
    0,
    1,      // FU
    2,      // KY
    3,      // KE
    4,      // GI
    7,      // KA
    8,      // HI
    6,      // KI
    10000,  // OU
    5,      // TO
    5,      // NY
    5,      // NK
    5,      // NG
    9,      // UM
    10,     // RY
    11      // QUEEN
};

/**
 * @brief 評価値がlimitまでの指し手を降順にソートする
 *        limit以下の指し手の順序は不定
 * @param first
 * @param last
 * @param limit ソートする範囲の閾値
 * partial_insertion_sort
 */
void PartialInsertionSort(const state_action::ExtendedMoveArray::iterator first,
                          const state_action::ExtendedMoveArray::iterator last,
                          const int limit) {
  using iterator = state_action::ExtendedMoveArray::iterator;
  for (iterator sorted_end = first, p = first + 1; p != last; ++p) {
    if (p->value >= limit) {
      state_action::ExtendedMove tmp = *p;
      iterator q;
      *p = *++sorted_end;
      for (q = sorted_end; q != first && *(q - 1) < tmp; --q) {
        *q = *(q - 1);
      }
      *q = tmp;
    }
  }
}

/**
 * @brief 歩の不成を生成するかどうかを切り替えるためのラッパー
 * @param position
 * @param tt_move
 * @return
 * pseudo_legal
 */
bool IsPseudoLegal(const state_action::Position& position,
                   const state_action::Move tt_move) {
#ifdef FOR_TOURNAMENT
  return position.IsPseudoSublegal<false>(tt_move);
#else
  return limit.generate_all_legal_moves
             ? position.IsPseudoSublegal<true>(tt_move)
             : position.IsPseudoSublegal<false>(tt_move);
#endif  // FOR_TOURNAMENT
}

template <state_action::MoveGeneratorType Type>
void GenerateMoves(const state_action::Position& position,
                   state_action::ExtendedMoveArray& move_list) {
#ifdef FOR_TOURNAMENT
  state_action::GenerateMoves<Type>(position, move_list);
#else
  if (limit.generate_all_legal_moves) {
    state_action::GenerateMoves<state_action::MoveGeneratorAll<Type>::value>(
        position, move_list);
  } else {
    state_action::GenerateMoves<Type>(position, move_list);
  }
#endif  // FOR_TOURNAMENT
}
}  // namespace

template <MovePickerBase::PickType Type, typename Predicate>
inline state_action::Move MovePickerBase::Select(
    state_action::ExtendedMoveArray::iterator last, Predicate pred) {
  while (cursor_ != last) {
    if (Type == PickType::BEST) {
      // 最善手を探して、入れ替える
      // 最善手がtt_move_ならもう一周することになる
      std::swap(*cursor_, *std::max_element(cursor_, last));
    }

    state_action::Move tmp = *cursor_++;
    if (tmp != tt_move_ && pred(tmp)) {
      return tmp;
    }
  }
  return state_action::Move::NONE;
}

  template <typename Array>
void MovePickerBase::ComputeScore(Array& move_list) {
  for (auto& e : move_list) {
    const auto target = e.move.target();
    const auto target_piece = position_[target];

    // SEEを行うと遅いので、単純に駒の価値で代用する
    // 歩が成る指し手もあるので、ある程度は優先されないといけない
    // 駒を捕る指し手なので、打つ指し手はない

    // MVV-LVAという概念がチェスではあるらしい

    e.value = evaluation::captured_value_delta[target_piece] +
              (*capture_history_)[target][position_.moved_piece(e.move)]
                                 [target_piece.type()] /
                  16;
  }
}

template <typename Array>
void MovePickerBase::ComputeScore(
    Array& move_list, const ButterflyHistory* main_history,
    const PieceTargetHistory** continuation_history) {
  for (auto& e : move_list) {
    const auto piece = position_.moved_piece(e.move);
    const auto target = e.move.target();
    e.value = (*main_history)[e.move.Serialize()][position_.side()] +
              (*continuation_history[0])[target][piece] +
              (*continuation_history[1])[target][piece] +
              (*continuation_history[3])[target][piece];
  }
}

template <typename Array>
void MovePickerBase::ComputeScore(Array& move_list,
                                  const ButterflyHistory* main_history) {
  for (auto& e : move_list) {
    if (position_.IsCapture(e.move)) {
      // 安い駒のスコアが高くなる
      // 駒の価値を移動前(成る前)と移動後(成った後)では移動前の方が強くなるらしい
      // 移動後の価値は駒が捕られない前提なので、前提が違うと思われる

      // 駒の価値 + MVV/LVA
      e.value = evaluation::captured_value_delta[position_[e.move.target()]] -
                lva_table[position_.moving_piece(e.move).type()];
    } else {
      // historyの値の順番
      e.value =
          (*main_history)[e.move.Serialize()][position_.side()] - (1 << 28);
    }
  }
}

FORCE_INLINE bool IsLegalTTMove(
    const state_action::Move tt_move,
    const state_action::Position& position) noexcept {
  return tt_move && IsPseudoLegal(position, tt_move);
}

FORCE_INLINE constexpr state_action::Move GetValidTTMove(
    const state_action::Move tt_move, const bool condition) noexcept {
  return condition ? tt_move : state_action::Move::NONE;
}

FORCE_INLINE state_action::Move GetTTMoveNormal(
    const state_action::Move tt_move,
    const state_action::Position& position) noexcept {
  return GetValidTTMove(tt_move, IsLegalTTMove(tt_move, position));
}

MovePickerNormal::MovePickerNormal(
    const state_action::Position& position, const state_action::Move tt_move,
    const Depth depth, const ButterflyHistory* mh,
    const CapturePieceTargetHistory* cph, const PieceTargetHistory** ch,
    const state_action::Move cm,
    const std::array<state_action::Move, 2>& killer)
    : MovePickerBase(position, GetTTMoveNormal(tt_move, position), cph),
      move_range_(killer, cm),
      depth_(depth),
      main_history_(mh),
      continuation_history_(ch) {
  // 通常探索から呼び出されているので、残りの探索深さは0より大きい
  ASSERT_LV3(depth > Depth::ZERO);

  state_ = position.checked() ? State::EVASTION_TT : State::MAIN_TT;
  state_ += tt_move_ == state_action::Move::NONE;
}

state_action::Move MovePickerNormal::Next(const bool skip_quiets) {
  switch (state_) {
    case State::MAIN_TT:
    case State::EVASTION_TT:
      // 置換表の指し手を返す

      ++state_;
      return tt_move_;

    case State::CAPTURE_INITIAL:
      // 置換表の指し手を返した後
      cursor_ = move_range_.begin();

      // 名前空間を明示的に指定しないとstate_actionの方もマッチする
      search::GenerateMoves<
          state_action::MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS>(
          position_, move_range_);

      // 駒を捕る指し手に対してオーダリングのためのスコアを付ける
      ComputeScore(move_range_);

      ++state_;

      [[fallthrough]];

    case search::State::GOOD_CAPTURE:
      // 置換表の指し手を返した後、killer moveの前
      // SSEの値が悪いものはBAD_CAPTURE
      if (const state_action::Move move = Select<PickType::BEST>(
              move_range_.end(),
              [&](const state_action::Move m) {
                const bool flag = position_.IsSeeGe(
                    m, evaluation::Value(-55 * (cursor_ - 1)->value / 1024));
                if (!flag) {
                  // 指し手の配列から取り出されたが、SEEが低いのでBAD_CAPTUREに戻す
                  move_range_.push_back_bad_capture(m);
                }
                return flag;
              });
          move != state_action::Move::NONE) {
        return move;
      }

      cursor_ = move_range_.refutation_begin();
      move_range_.RemoveDuplicatedRefutation();

      ++state_;

      [[fallthrough]];

    case search::State::REFUTATION:
      // killer move, counter moveを返す

      // 既に返した置換表の指し手、GOOD_CAPTURE、これから返すBAD_CAPTUREは
      // ここでは除外する
      if (const state_action::Move move = Select<PickType::NEXT>(
              move_range_.refutation_end(),
              [&](const state_action::Move m) {
                return m != state_action::Move::NONE && m != tt_move_ &&
                       !position_.IsCaptureOrFuPromotion(m) &&
                       IsPseudoLegal(position_, m);
              });
          move != state_action::Move::NONE) {
        return move;
      }

      ++state_;

      [[fallthrough]];

    case search::State::QUIET_INITIAL:
      // 駒を捕らない指し手を生成してオーダリング

      if (!skip_quiets) {
        // BAD_CAPTUREの終端に駒を捕らない指し手を追加する
        cursor_ = move_range_.bad_capture_end();
        move_range_.resize(cursor_);

        search::GenerateMoves<
            state_action::MoveGeneratorType::NON_CAPTURES_MINUS_PROMOTIONS>(
            position_, move_range_);
        ComputeScore(move_range_, main_history_, continuation_history_);
        // 深さに依存する閾値で部分的にソート
        PartialInsertionSort(cursor_, move_range_.end(),
                             -4000 * static_cast<int>(depth_));
      }

      ++state_;

      [[fallthrough]];

    case search::State::QUIET:
      // 駒を捕らない指し手

      if (!skip_quiets) {
        // 置換表の指し手とkiller moveは返した後なので、
        // それらは取り除く必要がある
        if (const state_action::Move move = Select<PickType::NEXT>(
                move_range_.end(),
                [&](const state_action::Move m) {
                  return m != tt_move_ && m != move_range_[0].move &&
                         m != move_range_[1].move && m != move_range_[2].move;
                });
            move != state_action::Move::NONE) {
          return move;
        }
      }

      cursor_ = move_range_.bad_capture_begin();

      ++state_;

      [[fallthrough]];

    case search::State::BAD_CAPTURE:
      // SEEが低い指し手

      return Select<PickType::NEXT>(
          move_range_.bad_capture_end(),
          [](const state_action::Move m) { return true; });

    case search::State::EVASION_INITIAL:
      cursor_ = move_range_.begin();
      search::GenerateMoves<state_action::MoveGeneratorType::EVASIONS>(
          position_, move_range_);

      ComputeScore(move_range_, main_history_);

      ++state_;

      [[fallthrough]];

    case search::State::EVASION:
      // 王手を回避する指し手を返す

      // 上で指し手の生成とスコア付けをしてある
      //
      // 数は多くないはずなので、常に最善のものを返す
      return Select<PickType::BEST>(
          move_range_.end(), [](const state_action::Move m) { return true; });
    default:
      UNREACHABLE;
      break;
  }
  ASSERT_LV1(false);
  return state_action::Move::NONE;
}

FORCE_INLINE state_action::Move GetTTMoveQuiet(
    const state_action::Move tt_move, const state_action::Position& position,
    const Depth depth, const Square recapture_square) noexcept {
  return GetValidTTMove(tt_move, IsLegalTTMove(tt_move, position) &&
                                     (depth > Depth::QS_RECAPTURES ||
                                      tt_move.target() == recapture_square));
}

MovePickerQuiet::MovePickerQuiet(const state_action::Position& position,
                                 const state_action::Move tt_move,
                                 const Depth depth, const ButterflyHistory* mh,
                                 const CapturePieceTargetHistory* cph,
                                 const Square recapture_square)
    : MovePickerBase(position,
                     GetTTMoveQuiet(tt_move, position, depth, recapture_square),
                     cph),
      move_list_(),
      depth_(depth),
      main_history_(mh),
      recapture_square_(recapture_square) {
  // 静止探索から呼び出されているので、残りの探索深さは0以下
  ASSERT_LV3(depth <= Depth::ZERO);

  state_ = position.checked() ? State::EVASTION_TT : State::QUIET_SEARCH_TT;
  state_ += tt_move_ == state_action::Move::NONE;
}

state_action::Move MovePickerQuiet::Next() {
  switch (state_) {
    case State::EVASTION_TT:
    case State::QUIET_SEARCH_TT:
      // 置換表の指し手を返す

      ++state_;
      return tt_move_;
    case search::State::EVASION_INITIAL:
      cursor_ = move_list_.begin();
      search::GenerateMoves<state_action::MoveGeneratorType::EVASIONS>(
          position_, move_list_);

      ComputeScore(move_list_, main_history_);

      ++state_;

      [[fallthrough]];

    case search::State::EVASION:
      // 王手を回避する指し手を返す

      // 上で指し手の生成とスコア付けをしてある
      //
      // 数は多くないはずなので、常に最善のものを返す
      return Select<PickType::BEST>(
          move_list_.end(), [](const state_action::Move m) { return true; });

    case State::QUIET_CAPTURE_INITIAL:
      cursor_ = move_list_.begin();

      // TODO: depth_ <= Depth::QS_RECAPTURES
      //       ならrecaptureの指し手のみを生成した方が効率的

      // 名前空間を明示的に指定しないとstate_actionの方もマッチする
      search::GenerateMoves<
          state_action::MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS>(
          position_, move_list_);

      // 駒を捕る指し手に対してオーダリングのためのスコアを付ける
      ComputeScore(move_list_);

      ++state_;

      [[fallthrough]];

    case search::State::QUIET_CAPTURE:
      // 静止探索用の指し手を返す

      // 上で指し手の生成とスコア付けをしてある
      //
      // depth_がDepth::QS_RECAPTUREより深いなら、
      // recaptureのマスに移動する指し手のみを生成
      if (const state_action::Move move =
              Select<PickType::BEST>(move_list_.end(),
                                     [&](const state_action::Move m) {
                                       return depth_ > Depth::QS_RECAPTURES ||
                                              m.target() == recapture_square_;
                                     });
          move != state_action::Move::NONE) {
        return move;
      }

      // 指し手がなくてdepthがDepth::QS_CHECKSより深いなら、終了
      // depth_が0の時は特別に王手になる指し手も試す
      if (depth_ != Depth::QS_CHECKS) {
        return state_action::Move::NONE;
      }

      ++state_;

      [[fallthrough]];

    case search::State::QUIET_CHECK_INITIAL:
      // 王手になる指し手を生成する

      // CAPTURES_PLUS_PROMOTIONSで生成したので、歩の成る指し手の除外が必要
      // 歩の成る指し手は王手かどうかに関係なく全て生成して、指し手として選択された後
      // QUIET_CHECKS_MINUS_PROMOTIONSがあれば良いのだが、実装が難しいので、
      // QUIET_CHECKSで生成して、歩の成る指し手を取り除く

      cursor_ = move_list_.begin();
      search::GenerateMoves<state_action::MoveGeneratorType::QUIET_CHECKS>(
          position_, move_list_);

      ++state_;

      [[fallthrough]];

    case search::State::QUIET_CHECK:
      // 王手になる指し手を返す

      return Select<PickType::NEXT>(move_list_.end(),
                                    [&](const state_action::Move m) {
                                      return !position_.IsFuPromotion(m);
                                    });

    default:
      UNREACHABLE;
      break;
  }
  ASSERT_LV1(false);
  return state_action::Move::NONE;
}

FORCE_INLINE state_action::Move GetTTMoveProbCut(
    const state_action::Move tt_move, const state_action::Position& position,
    const evaluation::Value threshold) noexcept {
  return GetValidTTMove(tt_move, tt_move && position.IsCapture(tt_move) &&
                                     IsPseudoLegal(position, tt_move) &&
                                     position.IsSeeGe(tt_move, threshold));
}

MovePickerProbCut::MovePickerProbCut(const state_action::Position& position,
                                     const state_action::Move tt_move,
                                     const evaluation::Value threshold,
                                     const CapturePieceTargetHistory* cph)
    : MovePickerBase(position, GetTTMoveProbCut(tt_move, position, threshold),
                     cph),
      threshold_(threshold) {
  ASSERT_LV3(!position.checked());

  state_ = State::PROBCUT_TT;
  state_ += tt_move_ == state_action::Move::NONE;
}

state_action::Move MovePickerProbCut::Next() {
  switch (state_) {
    case State::PROBCUT_TT:
      ++state_;
      return tt_move_;
    case State::PROBCUT_INITIAL:
      cursor_ = move_list_.begin();

      // 名前空間を明示的に指定しないとstate_actionの方もマッチする
      search::GenerateMoves<
          state_action::MoveGeneratorType::CAPTURES_PLUS_PROMOTIONS>(
          position_, move_list_);

      // 駒を捕る指し手に対してオーダリングのためのスコアを付ける
      ComputeScore(move_list_);

      ++state_;

      [[fallthrough]];

    case State::PROBCUT:
      // 上で指し手の生成とスコア付けをしてある
      return Select<PickType::BEST>(move_list_.end(),
                                    [&](const state_action::Move m) {
                                      return position_.IsSeeGe(m, threshold_);
                                    });

    default:
      UNREACHABLE;
      break;
  }
  ASSERT_LV1(false);
  return state_action::Move::NONE;
}

}  // namespace search
