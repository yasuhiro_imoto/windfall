#pragma once
#ifndef SEARCH_STACK_H_INCLUDED
#define SEARCH_STACK_H_INCLUDED

#include <array>

#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "history.h"

namespace search {
#pragma warning(push)
#pragma warning(disable : 26495)
struct Stack {
  /**
   * @brief RootMovesのpvを指すポインタ
   *
   * pv
   */
  state_action::Move* pv;
  /**
   * @brief counter moveに関する履歴へのポインタ
   *
   * 実体はThreadが持っている
   * continuationHistory
   */
  PieceTargetHistory* continuation_history;
  /**
   * @brief rootからの探索深さ
   *
   * rootなら0
   * ply
   */
  int ply;
  /**
   * @brief スレッドが探索しているこの局面について選択されている指し手
   *
   * currentMove
   */
  state_action::Move current_move;
  /**
   * @brief 探索から除外したい指し手
   *
   * singular extension判定の場合に置換表の指し手からその手を除外する
   * excludedMove
   */
  state_action::Move excluded_move;
  /**
   * @brief killer move
   *
   * killers
   */
  std::array<state_action::Move, 2> killlers;
  /**
   * @brief 評価関数でのスコア
   *
   * null moveの場合に親ノードの評価値を使うのでそのために保存
   * staticEval
   */
  evaluation::Value static_evaluation;
  /**
   * @brief 計算したhistoryの合計値
   *
   * statScore
   */
  int score;
  /**
   * @brief この局面で生成した手のうち何番目の指し手か
   *
   * 1ならおそらく置換表の指し手
   * moveCount
   */
  int move_count;
};
#pragma warning(pop)
}  // namespace search
#endif  // !SEARCH_STACK_H_INCLUDED
