#pragma once
#ifndef TRANSPOSITION_TABLE_H_INCLUDED
#define TRANSPOSITION_TABLE_H_INCLUDED

// cf.【決定版】コンピュータ将棋のHASHの概念について詳しく :
// http://yaneuraou.yaneu.com/2018/11/18/%E3%80%90%E6%B1%BA%E5%AE%9A%E7%89%88%E3%80%91%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF%E5%B0%86%E6%A3%8B%E3%81%AEhash%E3%81%AE%E6%A6%82%E5%BF%B5%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6/

#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "../utility/hash_key.h"
#include "../utility/level_assert.h"
#include "bound.h"
#include "depth.h"

namespace search {
//! 置換表エントリー
/*! 10 bytesなので32 bytesのchache lineに3個入る
    key               16bit: hash keyの上位16bit
    move              16bit: 最善手
    value             16bit: Searchの結果
    evaluation value  16bit: Evaluateの結果
    generation         5bit: 世代カウンター
    pv node            1bit: PV nodeで調べた結果かのフラグ
    bound              2bit: Bound
    depth              8bit: valueの探索深さ

    TTEntry */
class TTEntry {
  friend struct TranspositionTable;

 public:
  state_action::Move move() const { return state_action::Move(move_); }
  //! Searchの結果
  evaluation::Value value() const {
    return static_cast<evaluation::Value>(value_);
  }
  //! Evaluationの結果
  evaluation::Value evaluation_value() const {
    return static_cast<evaluation::Value>(evaluation_value_);
  }
  Depth depth() const {
    return Depth(depth_) + Depth::OFFSET;
  }
  bool pv() const { return static_cast<bool>(gpb_ & 0x4); }
  Bound bound() const { return static_cast<Bound>(gpb_ & 0x3); }

  /**
   * @brief 置換表のエントリーに対して与えられたデータを上書き保存
   * @param k 
   * @param v 探索のスコア
   * @param pv PV nodeであるか
   * @param bound 
   * @param d その時の探索深さ
   * @param m ベストな指し手
   * @param ev 評価関数 or 静止探索の値
   * save
  */
  void Save(const Key k, const evaluation::Value v, const bool pv,
            const Bound bound, const Depth d, const state_action::Move m,
            evaluation::Value ev);

 private:
  //! hash keyの上位16bit
  /*! hashがそこそこ一致しているという前提
      key16 */
  uint16_t key_;
  /*! 指し手の下位16bit
      move16 */
  uint16_t move_;
  //! Searchの結果
  /*! value16 */
  int16_t value_;
  //! Evaluationの結果
  /*! eval16 */
  int16_t evaluation_value_;
  //! generationの上位5bit + PVであるか + Bound
  /*! generationはentryの世代
      TranspositionTableで新しい探索ごとに+8される
      genBound8 */
  uint8_t gpb_;
  //! 探索深さ
  /*! これが大きいものほど価値がある
      1バイトに収めるために、DepthをONE_PLYで割ったものを格納する
      Depth::NONEが-6なのでこの分だけ下駄履きさせてある
      depth8 */
  uint8_t depth_;
};

//! 置換表本体
/*! TTEntryをClusterSize個並べるてクラスターを構成する
    TranspositionTable */
struct TranspositionTable {
  //! TTEntryはこのサイズでalignされたメモリに配置
  /*! CacheLineSize */
  static constexpr int cache_line_size = 64;
  //! 1クラスターにおけるTTEntryの数
  /*! ClusterSize */
  static constexpr int cluster_size = 3;

  struct Cluster {
    TTEntry entry[cluster_size];
    //! 全体を32bytesにするためのpadding
    uint8_t padding[2];
  };

  static_assert(cache_line_size % sizeof(Cluster) == 0,
                "The size of Cluster is invalid.");
  static_assert(sizeof(cache_line_size) > 0 &&
                    sizeof(cache_line_size) <= cache_line_size,
                "The size of Cluster is invalid.");

#pragma warning(push)
#pragma warning(disable : 26495)
  TranspositionTable() : memory_(nullptr), cluster_count_(0) {}
#pragma warning(pop)
  ~TranspositionTable() { free(memory_); }

  //! generationを更新
  /*! USE_GLOBAL_OPTIONSが有効のときは、このタイミングで
      Options["Threads"]の値をキャプチャして、
      探索スレッドごとの置換表と世代カウンターを用意する

      下位3bitはPV nodeかどうかのフラグとBoundに用いている
      new_search */
  void UpdateGeneration() { generation_ += 8; }

  //! 置換表のなかから与えられたkeyに対応するentryを探す
  /*! 見つかったならfound == trueにしてそのTT_ENTRY*を返す
      見つからなかったらfound == falseで、
      このとき置換表に書き戻すときに使うと良いTT_ENTRY*を返す
      probe */
  TTEntry* Probe(const Key key, bool& found) const;

  //! 置換表の利用率(千分率)
  /*! USIプロトコルで統計情報として出力
      hashfull */
  int Hashfull() const;

  //! 置換表のサイズを変更
  /*! 確保するメモリサイズ(MB単位)
      resize */
  void resize(const size_t size_mb);

  //! 置換表のエントリーの全クリア
  /*! clear */
  void clear();

  //! keyの下位bitをClusterのindexにしてその最初のTTEntry*を返す
  TTEntry* GetFirstEntry(const Key key) const {
    /* 下位32bit × cluster_count_ / 2^32 ->  [0, cluster_count_ - 1]
       掛け算が必要にはなるが、cluster_count_を2^Nで確保しないといけないという制約が外れる
       cf. Allow for general transposition table sizes.
       https://github.com/official-stockfish/Stockfish/commit/2198cd0524574f0d9df8c0ec9aaf14ad8c94402b

       return &table_[(static_cast<uint32_t>(key) *
       static_cast<uint64_t>(cluster_count_)) >> 32].entry[0]; →　(key &
       1)が先後フラグなので重要 奇数の場合は(cluster_count & ~1) | (key &
       1)が配列の外側になる cluster_countは偶数の必要がある */
    ASSERT_LV3(cluster_count_ % 2 == 0);

    // cf. 置換表の128GB制限を取っ払う冴えない方法
    // http://yaneuraou.yaneu.com/2018/05/03/%E7%BD%AE%E6%8F%9B%E8%A1%A8%E3%81%AE128gb%E5%88%B6%E9%99%90%E3%82%92%E5%8F%96%E3%81%A3%E6%89%95%E3%81%86%E5%86%B4%E3%81%88%E3%81%AA%E3%81%84%E6%96%B9%E6%B3%95/
#if defined(IS_64BIT) && defined(USE_SSE2) && defined(USE_HUGE_HASH)
#error Not Implemented.
#else
    // static_cast<uint32_t>(key) * static_cast<uint64_t>(cluster_count)では
    // keyのbit0を使ってしまうので、1bitシフトする
    const auto h = (static_cast<uint32_t>(key >> 1) *
                    static_cast<uint64_t>(cluster_count_)) >>
                   32;
    return &table_[(h & ~1) | (key & 1)].entry[0];
#endif  // defined(IS_64BIT) && defined(USE_SSE2) && defined(USE_HUGE_HASH)
  }

 private:
  friend class TTEntry;

  //! この置換表が保持しているクラスター数
  /*! clusterCount */
  size_t cluster_count_;
  //! alignedなクラスターの先頭アドレス
  /*! table */
  Cluster* table_;
  //! 確保されたalignedでないメモリの先頭
  /*! mem */
  void* memory_;
  //! 世代カウンター
  /*! 新しい探索ごとに8ずつ加算
      TTEntry::Save()で用いる
      generation8 */
  uint8_t generation_;
};

/*! TT */
extern TranspositionTable tt;
}  // namespace search
#endif  // !TRANSPOSITION_TABLE_H_INCLUDED
