#pragma once
#ifndef BOUND_H_INCLUDED
#define BOUND_H_INCLUDED

#include "../utility/enum_cast.h"

namespace search {
//! 評価値の性質
/*! 探索窓の範囲外の値が返ってきた場合、
    high fail時はこの値は上界(真の値はこれより小さい)、
    low fail時はこの値は下界(真の値はこれより大きい) */
enum class Bound {
  /*! 探索していない(DEPTH::NONE)ときに、
      最善手か、静的評価スコアだけを置換表に格納したいとき */
  NONE,
  /*! 上界(真の評価値はこれより小さい) =
      詰みのスコアや、nonPVで評価値があまり信用ならない状態 */
  UPPER,
  /*! 下界(真の評価値はこれより大きい) */
  LOWER,
  /*! 真の評価値と一致
      PV nodeでかつ詰みのスコアでないことを表現 */
  EXACT = UPPER | LOWER
};

constexpr Bound operator&(const Bound x, const Bound y) {
  return static_cast<Bound>(enum_cast<>(x) & enum_cast<>(y));
}
}  // namespace search
#endif  // !BOUND_H_INCLUDED
