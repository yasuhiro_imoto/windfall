#pragma once
#ifndef PROCESS_GROUP_H_INCLUDED
#define PROCESS_GROUP_H_INCLUDED

namespace search {
namespace windows {
//! windows環境において全コアを使い切るための処理
/*! bindThisThread */
void BindThisThread(const size_t index);
}  // namespace windows
}
#endif // !PROCESS_GROUP_H_INCLUDED
