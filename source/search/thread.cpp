#include "stdafx.h"

#include "thread.h"

#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "../state_action/move_generator.h"
#include "../state_action/position.h"
#include "../usi/option.h"
#include "../utility/bit_operator.h"
#include "../utility/configuration.h"
#include "../utility/level_assert.h"
#include "process_group.h"
#include "search_limit.h"

namespace search {
ThreadPool thread_pool;
SearchLimit limit;

void* Thread::operator new(const size_t s) {
  return aligned_malloc(s, alignof(Thread));
}
void Thread::operator delete(void* p) { aligned_free(p); }

Thread::Thread(const size_t id) : id_(id), thread_(&Thread::IdleLoop, this) {
  // スレッドはsearching_flag ==
  // trueで開始するので、このままworkerのほう待機状態にさせておく
  WaitForSearchFinished();
}

Thread::~Thread() {
  // 探索中はスレッドを破棄しない
  ASSERT_LV3(!searhing_flag);

  // 探索は終わっているのでそのまま終了するはず
  exit_flag = true;
  StartSearch();
  thread_.join();
}

void Thread::Clear() {
  counter_moves.fill(state_action::Move::NONE);
  main_history.fill(0);
  capture_history.fill(0);

  // 未初期化のときに[SQ_ZERO][NO_PIECE]を指すので、ここを-1で初期化しておけば、
  // history > 0 を条件にすれば自ずと未初期化のときは除外される
  for (auto& tgt : continuation_history) {
    for (auto& h : tgt) {
      h->fill(0);
    }
  }

  continuation_history[Square::ZERO][Piece::EMPTY]->fill(
      counter_move_prune_threshold - 1);
}

void Thread::StartSearch() {
  std::lock_guard<std::mutex> lock(mutex_);
  searhing_flag = true;
  // アイドル中のスレッドを起こす
  cv_.notify_one();
}

void Thread::WaitForSearchFinished() {
  std::unique_lock<std::mutex> lock(mutex_);
  cv_.wait(lock, [&] { return !searhing_flag; });
}

void Thread::IdleLoop() {
  // NUMA環境では、8スレッド未満だと異なるNUMAに割り振られることがあり、
  // パフォーマンス激減するのでその回避策
  // ・8スレッド未満のときはOSに任せる
  // ・8スレッド以上のときは、自前でBindThisThreadを行なう
  // cf. Upon changing the number of threads, make sure all threads are bound
  // https://github.com/official-stockfish/Stockfish/commit/1c50d8cbf554733c0db6ab423b413d75cc0c1928

  if (usi::options["Threads"] >= 8) {
    // Windows環境での固有の処理
    windows::BindThisThread(id_);
  }

  while (true) {
    std::unique_lock<std::mutex> lock(mutex_);
    searhing_flag = false;

    cv_.notify_one();
    cv_.wait(lock, [&] { return searhing_flag; });

    if (exit_flag) {
      return;
    }

    // exit_flagがfalseということは、search_flagがtrueなので探索する
    Search();
  }
}

void MainThread::CheckTime() {
  // master threadからしか呼び出されないので、シンプルな実装で大丈夫

  if (--call_counter > 0) {
    // 1024回に1回の割合で確認する程度で大丈夫
    return;
  }
  // ノード数制限の場合は制限数の場合の0.1%程度にする
  // ある程度は小さくしておかないと通信遅延のせいで1秒を超えて
  // 消費時間が2秒になって損をしてしまうかもしれない
  // cf. Check the clock every 1024 nodes :
  // https://github.com/official-stockfish/Stockfish/commit/8db75dd9ec05410136898aa2f8c6dc720b755eb8
  call_counter =
      limit.nodes ? std::min(1024, static_cast<int>(limit.nodes / 1024)) : 1024;

  static utility::TimePoint last_info_time = utility::now();
  utility::TimePoint tick = utility::now();
  if (tick - last_info_time >= 1000) {
    last_info_time = tick;
    // TODO: debug用のprint
  }

  if (ponder) {
    // ponder中はGUIの方からstop, ponderhitが来るまで止まるべきではない
    return;
  }

  // ponderhitからの経過時間
  // ponderが外れた場合はgoからの経過時間を返すので結局は一緒
  utility::TimePoint elapsed = utility::timer.elapsed_ponder_hit();

  if ((limit.enable_time_management() &&
       (elapsed > utility::timer.maximum() ||
        (utility::timer.duration > 0 && elapsed > utility::timer.duration))) ||
      (limit.move_time && elapsed >= limit.move_time) ||
      (limit.nodes &&
       thread_pool.nodes_searched() >= static_cast<uint64_t>(limit.nodes))) {
    thread_pool.stop = true;
  }
}

void ThreadPool::Set(const size_t requested) {
  if (!threads_.empty()) {
    // 一旦はすべてのスレッドを解体(NUMA対策)
    main_thread()->WaitForSearchFinished();

    while (!threads_.empty()) {
      delete threads_.back();
      threads_.pop_back();
    }
  }
  ASSERT_LV3(threads_.empty());

  if (requested > 0) {
    // 要求数だけスレッドを生成
    threads_.push_back(new MainThread(0));

    for (size_t i = 1; i < requested; ++i) {
      threads_.push_back(new SubordinateThread(i));
    }
  }
}

void ThreadPool::Clear() {
  for (Thread* thread : threads_) {
    thread->Clear();
  }

  main_thread()->call_counter = 0;
  main_thread()->previous_score = evaluation::Value::INFINITE;
  main_thread()->previous_time_reduction = 1.0;
}

template <state_action::MoveGeneratorType Type>
void AddRootMoves(const state_action::Position& position,
                  const SearchLimit& limits, RootMoves& root_moves) {
  for (auto m : state_action::MoveList<Type>(position)) {
    if (limits.search_moves.empty() ||
        boost::range::find(limits.search_moves, m) !=
            limits.search_moves.end()) {
      root_moves.emplace_back(m);
    }
  }
}

void ThreadPool::StartThinking(const state_action::Position& position,
                               state_action::StateListPtr& states,
                               const SearchLimit& limits,
                               const bool ponder_mode) {
  // 思考中ならば停止を待つ
  main_thread()->WaitForSearchFinished();

  stop = false;
  main_thread()->ponder = ponder_mode;
  limit = limits;

  RootMoves root_moves;
  // ルート局面における合法手をすべて生成する
  // goコマンドで差し手の指定があるならそれを使う

#ifdef USE_ENTERING_KING_WIN
  if (position.Declarate() == state_action::Move::WIN) {
    root_moves.emplace_back(state_action::Move::WIN);
  }
#endif  // USE_ENTERING_KING_WIN

#if !defined(MATE_ENGINE) && !defined(FOR_TOURNAMENT)
  // 全合法手を生成するオプションが有効な場合
  if (limits.generate_all_legal_moves) {
    AddRootMoves<state_action::MoveGeneratorType::LEGAL_ALL>(position, limits,
                                                             root_moves);
  } else {
    AddRootMoves<state_action::MoveGeneratorType::LEGAL>(position, limits,
                                                         root_moves);
  }
#else
  // トーナメント用ならば歩の不成は生成しない
  AddRootMoves<state_action::MoveGeneratorType::LEGAL>(position, limits,
                                                       root_moves);
#endif  // !defined(MATE_ENGINE) && !defined(FOR_TOURNAMENT)

  ASSERT_LV3(states || setup_states_);
  // positionコマンドが呼ばれてから初めてのgoコマンドならstatesをスレッドの方に渡す
  if (states) {
    setup_states_ = std::move(states);
  }

  // 各スレッドでpositionを構築するときにrb->previousがクリアされてしまうので、
  // バックアップする
  state_action::Rollback tmp;
  std::memcpy(&tmp, &setup_states_->back(), sizeof(tmp));

  const auto sfen = position.sfen();
  for (Thread* thread : threads_) {
    thread->nodes = thread->nmp_min_ply = 0;
    thread->root_depth = thread->completed_depth = Depth::ZERO;
    thread->root_moves = root_moves;

    thread->root_position.Set(sfen, &setup_states_->back(), thread);
  }

  // 復元
  std::memcpy(&setup_states_->back(), &tmp, sizeof(tmp));

  main_thread()->StartSearch();
}
}  // namespace search
