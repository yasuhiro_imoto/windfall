#include "stdafx.h"

#include "learner.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../rule/color.h"
#include "../rule/entering_ou.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../state_action/move_generator.h"
#include "../state_action/packed_sfen.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../usi/command.h"
#include "../usi/option.h"
#include "../utility/level_assert.h"
#include "../utility/tool.h"
#include "depth.h"
#include "engine.h"
#include "process_group.h"
#include "search_limit.h"
#include "search_parameter.h"
#include "search_stack.h"
#include "thread.h"


#ifdef NNUE_LEARN
namespace search {
namespace learner {
void Initialize(state_action::Position& position, Stack* ss) {
  std::memset(ss - 4, 0, 7 * sizeof(Stack));

  // time managementを無効化する
  // go infiniteに相当
  limit.infinite = true;
  // PVを表示させない
  limit.silent_mode = true;
  // 各ノードの累積と比較することになるので使わない
  limit.nodes = 0;
  // 引数で与えたものを使う
  limit.depth = 0;
  // max moveによる引き分けを避ける
  limit.max_game_ply = 1 << 16;
  // 入玉ルールがないとなかなか決着がつかない
  limit.entering_ou_rule = EnteringOuRule::POINTS27;

  // スレッドごとに独立していない
  evaluation::SetDrawValue(Color::BLACK, enum_cast<>(evaluation::Value::ZERO));
  evaluation::SetDrawValue(Color::WHITE, enum_cast<>(evaluation::Value::ZERO));

  auto thread = position.ptr_thread();
  thread->completed_depth = Depth::ZERO;
  thread->selective_depth = 0;
  thread->root_depth = Depth::ZERO;
  thread->nodes = 0;

  for (int i = 4; i > 0; --i) {
    ss[-i].continuation_history =
        &thread->continuation_history[Square::ZERO][Piece::EMPTY];
  }

  thread->root_moves.clear();
  for (const auto m : state_action::LegalMoveList(position)) {
    thread->root_moves.emplace_back(m);
  }
  ASSERT_LV3(!thread->root_moves.empty());
}

ValueAndPV QuietSearch(state_action::Position& position) {
  std::array<Stack, max_ply + 7> stack;
  Stack* ss = stack.data() + 4;
  std::array<state_action::Move, max_ply + 1> pv;
  std::vector<state_action::Move> pvs;

  Initialize(position, ss);
  // ダミーのバッファを設定
  ss->pv = pv.data();

  if (position.IsMated()) {
    pvs.push_back(state_action::Move::RESIGN);
    return ValueAndPV(evaluation::GetLoseValue(1), pvs);
  }

  const auto best_value = search::QuietSearch<NodeType::PV>(
      position, ss, -evaluation::Value::INFINITE, evaluation::Value::INFINITE,
      Depth::ZERO);
  for (state_action::Move* m = &ss->pv[0];
       *m == state_action::Move::WIN || m->ok(); ++m) {
    pvs.push_back(*m);
  }

  return ValueAndPV(best_value, pvs);
}

ValueAndPV Search(state_action::Position& position, const int depth,
                  const size_t multi_pv, const uint64_t nodes_limit) {
  std::vector<state_action::Move> pvs;

  const Depth d(depth);
  if (d < Depth::ZERO) {
    return std::make_pair(evaluation::Evaluate(position), std::move(pvs));
  } else if (d == Depth::ZERO) {
    return QuietSearch(position);
  }

  std::array<Stack, max_ply + 7> stack;
  Stack* ss = stack.data() + 4;
  std::array<state_action::Move, max_ply + 1> pv;

  Initialize(position, ss);
  // ダミーのバッファを設定
  ss->pv = pv.data();

  const auto thread = position.ptr_thread();
  auto& root_depth = thread->root_depth;
  auto& pv_index = thread->pv_index;
  auto& root_moves = thread->root_moves;
  auto& completed_depth = thread->completed_depth;
  auto& selective_depth = thread->selective_depth;

  // 現局面の指しての数を超えるわけにはいかない
  const auto m_pv = std::min(multi_pv, root_moves.size());
  // 探索深さを固定でMulti PVが有効な場合に
  // 一つの候補手をnodes_limitだけ探索するように調整
  const auto limit = nodes_limit * m_pv;

  evaluation::Value alpha = -evaluation::Value::INFINITE;
  evaluation::Value beta = evaluation::Value::INFINITE;
  evaluation::Value delta = -evaluation::Value::INFINITE;
  evaluation::Value best_value = -evaluation::Value::INFINITE;
  while ((root_depth += Depth::ONE) <= d &&
         !(limit && thread->nodes.load(std::memory_order_relaxed) >= limit)) {
    for (auto& rm : root_moves) {
      rm.previous_score = rm.score;
    }

    for (pv_index = 0; pv_index < m_pv && !thread_pool.stop; ++pv_index) {
      selective_depth = 0;

      if (root_depth >= 5) {
        delta =
            static_cast<evaluation::Value>(parameter::aspiration_search_delta);
        const auto v = root_moves[pv_index].previous_score;
        alpha = std::max(v - delta, -evaluation::Value::INFINITE);
        beta = std::max(v + delta, evaluation::Value::INFINITE);
      }

      int failed_high_count = 0;
      while (true) {
        Depth adjusted =
            std::max<Depth>(Depth::ONE, root_depth - failed_high_count);
        best_value = search::Search<NodeType::PV>(position, ss, alpha, beta,
                                                  adjusted, false, false);

        std::stable_sort(root_moves.begin() + pv_index, root_moves.end());

        if (best_value <= alpha) {
          beta = (alpha + beta) / 2;
          alpha = std::max(best_value - delta, -evaluation::Value::INFINITE);

          failed_high_count = 0;
        } else if (best_value >= beta) {
          beta = std::min(best_value + delta, evaluation::Value::INFINITE);
          ++failed_high_count;
        } else {
          break;
        }

        delta += delta / 4 + 5;
        ASSERT_LV3(-evaluation::Value::INFINITE <= alpha && alpha < beta &&
                   beta <= evaluation::Value::INFINITE);
      }
      std::stable_sort(root_moves.begin(), root_moves.begin() + pv_index + 1);
    }
    completed_depth = root_depth;
  }

  // 途中にnull moveがあるかもしれないので確認する
  for (const auto m : root_moves[0].pv) {
    if (m != state_action::Move::NULL_MOVE && !m.ok()) {
      break;
    }
    pvs.push_back(m);
  }

  // multi pvを考慮してpv 0のスコアをbest valueとする
  best_value = root_moves[0].score;

  return ValueAndPV(best_value, pvs);
}

void MultiThink::Think() {
  // 現在の値をバックアップ
  auto old_options = usi::options;

  // 読み込みに時間がかかるうえにスレッドセーフでないので、
  // 定跡は全てメモリに入っているものとする
  usi::options["BookOnTheFly"] = std::string("false");

  // 評価関数のパラメータの読み込みなど
  // 値の整合性のチェックはスキップ
  usi::command::GetReady(true);

  // 派生クラス独自の初期化
  Initialize();

  // ループの上限は別途設定しているとする
  loop_count_ = done_count_ = 0;

  std::vector<std::thread> threads;
  const auto n_threads = static_cast<size_t>(usi::options["Threads"]);

  thread_finished_.resize(n_threads);
  for (size_t i = 0; i < n_threads; ++i) {
    thread_finished_[i] = 0;
    threads.emplace_back([i, this] {
      // プロセッサの全てのスレッドを使い切る
      windows::BindThisThread(i);
      // 派生クラスで設定されている学習局面の生成などを実行
      this->Run(i);
      this->thread_finished_[i] = 1;
    });
  }

  // スレッドの終了待ち
  // joinを使うと他の処理ができなくなるので、保存ができない
  // 自前の終了フラグを確認する
  const auto threads_done = [&]() {
    // 一つでも終了していないものがあればfalse
    for (auto& f : thread_finished_) {
      if (f == 0) {
        return false;
      }
    }
    return true;
  };

  const auto run_callback = [&]() {
    // コールバックが設定されているなら実行する
    if (callback) {
      callback();
    }
  };

  {
    uint64_t i = 0;
    while (true) {
      if (threads_done()) {
        // 全てのスレッドが終了した
        break;
      }

      utility::Sleep(1000);

      if (++i == callback_interval) {
        run_callback();
        i = 0;
      }
    }
  }

  std::cout << std::endl << "finalize...";
  // 処理は終わったが、スレッドの終了処理の途中の可能性もあるので、
  // きっちりとjoinで待つ必要がある
  for (auto& th : threads) {
    th.join();
  }
  std::cout << "all threads are joined." << std::endl;

  // 書き換えたオプションを復元
  // ハンドラを起動させるために一つずつ代入する
  for (const auto& s : old_options) {
    usi::options[s.first] = std::string(s.second);
  }
}

void MultiThink::StartWorker() {
  // 現在の値をバックアップ
  backup_options_ = usi::options;

  // 読み込みに時間がかかるうえにスレッドセーフでないので、
  // 定跡は全てメモリに入っているものとする
  usi::options["BookOnTheFly"] = std::string("false");

  // 評価関数のパラメータの読み込みなど
  // 値の整合性のチェックはスキップ
  usi::command::GetReady(true);

  // 派生クラス独自の初期化
  Initialize();

  // ループの上限は別途設定しているとする
  loop_count_ = done_count_ = 0;

  const auto n_threads = static_cast<size_t>(usi::options["Threads"]);

  thread_finished_.resize(n_threads);
  for (size_t i = 0; i < n_threads; ++i) {
    thread_finished_[i] = 0;
    threads_.emplace_back([i, this] {
      // プロセッサの全てのスレッドを使い切る
      windows::BindThisThread(i);
      // 派生クラスで設定されている学習局面の生成などを実行
      this->Run(i);
      this->thread_finished_[i] = 1;
    });
  }
}

void MultiThink::WaitForFinish() {
  for (auto& th : threads_) {
    th.join();
  }

  // 書き換えたオプションを復元
  // ハンドラを起動させるために一つずつ代入する
  for (const auto& s : backup_options_) {
    usi::options[s.first] = std::string(s.second);
  }
}

void MultiSearch::Run(const size_t thread_id) {
  auto& thread = search::thread_pool[thread_id];
  auto& position = thread.root_position;
  state_action::Rollback rollback;

  while (true) {
    const auto input_data = input_queue.pop();
    if (!input_data) {
      // 最後まで処理した
      break;
    }

    position.Set(input_data->sfen, &rollback, &thread);

    ValueAndPV result = Search(position, input_data->depth);

    auto output_data = std::make_shared<SearchResult>();
    output_data->id = input_data->id;
    output_data->pv = std::move(result.second);
    output_data->score = static_cast<int32_t>(result.first);

    output_queue.push(std::move(output_data));
  }
}

bool GetTrainingPair(state_action::Position& position,
                     std::vector<state_action::Move>::const_iterator it,
                     const std::vector<state_action::Move>::const_iterator end,
                     TrainingPair& training_pair) {
  // とりあえずPVに従って局面を進める
  state_action::Rollback rollback;
  position.StepForward(*it, rollback);
#ifdef _DEBUG
  training_pair.teacher_pv.push_back(*it);
  training_pair.student_pv.push_back(*it);
#endif // _DEBUG
  ++it;

  if (it == end) {
    // PVの終端に到達した
    // 教師局面の特徴量
    evaluation::nn::RawFeatures::AppendActiveIndices(
        position, evaluation::nn::refresh_triggers[0],
        training_pair.teacher_indices);

    // 一つ前の局面に戻す
#ifdef _DEBUG
    training_pair.student_pv.pop_back();
#endif // _DEBUG
    --it;
    position.StepBackward(*it);

    return false;
  }

  // 再帰的に進める
  const bool result = GetTrainingPair(position, it, end, training_pair);
  if (result) {
    // 学習データの作成が完了した
    return true;
  }

  const ValueAndPV v =
      Search(position, static_cast<int>(std::distance(it, end)));
  if (v.second.front() == *it) {
    // この部分での読み筋は一致
    // 局面を戻す
#ifdef _DEBUG
    training_pair.student_pv.pop_back();
#endif // _DEBUG
    --it;
    position.StepBackward(*it);
    return false;
  }

  // 読み筋が不一致
  std::vector<state_action::Rollback> rb_list(v.second.size());
  for (size_t i = 0; i < rb_list.size(); ++i) {
    position.StepForward(v.second[i], rb_list[i]);
#ifdef _DEBUG
    training_pair.student_pv.push_back(v.second[i]);
#endif // _DEBUG
  }
  // 不一致の読み筋の終端の特徴量
  evaluation::nn::RawFeatures::AppendActiveIndices(
    position, evaluation::nn::refresh_triggers[0],
    training_pair.student_indices);

  // 奇数なら相手の手番になるので、評価が反転する
  training_pair.teacher_sign = std::distance(it, end) % 2 == 1 ? -1 : 1;
  training_pair.student_sign = v.second.size() % 2 == 1 ? -1 : 1;

  // 学習データの作成が完了した
  return true;
}

void MultiSearchPair::Run(const size_t thread_id) {
  auto& thread = search::thread_pool[thread_id];
  auto& position = thread.root_position;
  state_action::Rollback rollback;

  while (true) {
    const auto input_data = input_queue.pop();
    if (!input_data) {
      // 最後まで処理した
      break;
    }

    position.Set(input_data->sfen, &rollback, &thread);
    auto training_pair = std::make_unique<TrainingPair>();
    const bool result = GetTrainingPair(position, input_data->pv.begin(),
                                        input_data->pv.end(), *training_pair);
    if (result) {
      if (input_data->pv.size() % 2 == 1) {
        // 相手番なので、評価値を反転
        training_pair->teacher_score = -input_data->score;
      } else {
        // 自分の手番の局面なので、評価値はそのまま
        training_pair->teacher_score = input_data->score;
      }

      output_queue.push(std::move(training_pair));
    } else {
      // 数合わせのために無効なデータをキューに入れる
      output_queue.push(std::unique_ptr<TrainingPair>());
    }
  }
}
}  // namespace learner
}  // namespace search
#endif  // NNUE_LEARN
