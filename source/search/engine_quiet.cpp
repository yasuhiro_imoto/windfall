#include "stdafx.h"

#include "engine.h"

#include "../evaluation/evaluator.h"
#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../utility/enum_cast.h"
#include "../utility/hash_key.h"
#include "../utility/level_assert.h"
#include "bound.h"
#include "depth.h"
#include "engine_component.h"
#include "engine_debug.h"
#include "move_picker.h"
#include "search_limit.h"
#include "search_parameter.h"
#include "search_stack.h"
#include "thread.h"
#include "transposition_table.h"

namespace search {
template <NodeType Type>
evaluation::Value QuietSearch(state_action::Position& position, Stack* const ss,
                              evaluation::Value alpha,
                              const evaluation::Value beta, const Depth depth) {
  constexpr bool pv_node = Type == NodeType::PV;

  ASSERT_MSG_LV3(-evaluation::Value::INFINITE <= alpha && alpha < beta &&
                     beta <= evaluation::Value::INFINITE,
                 "alpha={}, beta={}", enum_cast<>(alpha), enum_cast<>(beta));
  ASSERT_MSG_LV3(pv_node || (alpha == beta - 1),
                 "pv_node={}, alpha={}, beta={}", pv_node, enum_cast<>(alpha),
                 enum_cast<>(beta));
  ASSERT_MSG_LV3(depth < Depth::ONE, "depth={}", enum_cast<>(depth.value));
  ASSERT_MSG_LV3(Depth(static_cast<int>(depth)) == depth, "depth={}",
                 enum_cast<>(depth.value));

  // PVを求めるbuffer
  // NonPVの場合は利用しない
  // pv
  std::array<state_action::Move, max_ply + 1> pv;

  state_action::Rollback rb;

  // 置換表にヒットしたときのエントリーへのポインタ
  // tte
  TTEntry* tte;

  // この局面のhash key
  // posKey
  Key hash_key;

  // 置換表の指し手
  // ttMove
  state_action::Move tt_move;
  // MovePickerからもらう現在の指し手
  // move
  state_action::Move move;
  // 現局面の最善手
  // bestMove
  state_action::Move best_move;

  // 置換表に登録するときの残り探索深さ
  // ttDepth
  Depth tt_depth;

  // best_moveに対応するスコア
  // bestValue
  evaluation::Value best_value;
  // 現在のmoveに対応するスコア
  // value
  evaluation::Value value;
  // 置換表に登録されているスコア
  // ttValue
  evaluation::Value tt_value;
  // futility pruningに用いるスコア
  // futilityValue
  evaluation::Value futility_value;
  // futility pruningの基準となるスコア
  // futilityBase
  evaluation::Value futility_base;
  // この関数が呼び出された時点のalpha
  // oldAlpha
  evaluation::Value old_alpha;

  // 置換表にヒットしたか
  // hhHit
  bool tt_hit;
  // 現局面で王手がかかっているか
  // inCheck
  bool checked;
  // 王手をかける指し手か
  // givesCheck
  bool checking;
  // 枝刈りの候補となる回避手であるか
  // evasionPrunable
  bool evasion_prunable;

  // この局面での何手目の指し手であるか
  // moveCount
  int move_count;

  // ------------------------------
  // 初期化
  // ------------------------------

  if (pv_node) {
    // alphaを上回る指し手が存在した場合には、
    // (そこそこ探索したので)置換表にはBound::EXACTで登録したい
    // 上回る指し手がなかった場合は、探索が足りないかもしれないので、
    // Bound::UPPERとしてbest_valueが登録される
    old_alpha = alpha;

    (ss + 1)->pv = pv.data();
    ss->pv[0] = state_action::Move::NONE;
  }

  // rootからの手数
  (ss + 1)->ply = ss->ply + 1;

  ss->current_move = best_move = state_action::Move::NONE;
  checked = position.checked();
  move_count = 0;

  // ------------------------------
  // 最大手数に到達したか
  // ------------------------------

  if (ss->ply >= max_ply || position.game_ply() > limit.max_game_ply) {
    return evaluation::GetDrawValue(RepetitionState::DRAW, position.side());
  }

  ASSERT_MSG_LV3(ss->ply >= 0 && ss->ply < max_ply, "ply={}", ss->ply);

  // ------------------------------
  // 置換表のprobe
  // ------------------------------

  // 置換表に登録する深さを補正する
  tt_depth = checked || depth >= Depth::QS_CHECKS ? Depth::QS_CHECKS
                                                  : Depth::QS_NO_CHECKS;

  hash_key = position.key();
  tte = tt.Probe(hash_key, tt_hit);
  tt_move = tt_hit ? position.ConvertMove16ToMove(tte->move())
                   : state_action::Move::NONE;
  tt_value = tt_hit ? evaluation::ConvertValuueFromTT(tte->value(), ss->ply)
                    : evaluation::Value::NONE;
  // 宣言勝ちの可能性もあるが、かなりレアなので、考えない
  if (!position.IsPseudoLegal(tt_move)) {
    // MovePickerに到達するまでは指し手を使わないが、
    // 評価値を信用できないと思うので、このタイミングでチェックする

    tt_move = state_action::Move::NONE;
    // 指し手がillegalなら評価値も不適切なはず
    tt_value = evaluation::Value::NONE;

    tt_hit = false;
  }

  if (!pv_node && tt_hit && tte->depth() >= tt_depth &&
      tt_value != evaluation::Value::NONE &&  // 置換表の値が壊れたかもしれない
      (tt_value >= beta ? (tte->bound() & Bound::LOWER)
                        : (tte->bound() & Bound::UPPER)) != Bound::NONE) {
    // non pvでは置換の指し手で枝刈りをする
    // tt_valueが下界(真の評価値はこれよりも大きい)もしくはexactで、
    // tt_value >= betaならbeta cutされる
    // tt_valueが上界(真の評価値はこれよりも小さい)が、tte->depth()の方が深いなら
    // 今回よりも多く探索したはずで、枝刈りも甘いはずだからその値を信頼する
    return tt_value;
  }

  // ------------------------------
  // 評価関数の呼び出し
  // ------------------------------

  if (checked) {
    ss->static_evaluation = evaluation::Value::NONE;

    // 王手がかかっている時は、-Value::INFINITEを初期値として
    // 全ての指し手を対象にこれを上回る手を探すので
    // best_valueはalphaと区別されなければならない
    best_value = futility_base = -evaluation::Value::INFINITE;
  } else {
    // ------------------------------
    // 1手詰めの判定
    // ------------------------------

    ASSERT_MSG_LV3(!position.checked(), "sfen={}", position.sfen());

    if (parameter::quiet_search_mate1 && !tt_hit) {
      // 置換表にヒットした場合は既に1手詰めを調べたはずなので、
      // ヒットしなかった場合のみ調べる
      if (parameter::weak_mate_ply == 1) {
        if (position.Mate1ply() != state_action::Move::NONE) {
          return evaluation::GetWinValue(ss->ply + 1);
        }
      } else {
        // TODO: N手詰めの場合
      }
      // このノードに再訪する可能性は低いはずなので、置換表には登録しない
    }

    if (tt_hit) {
      // 置換表の指し手を持ってくる
      // 置換表に評価値が格納されているとは限らないので、その場合は評価関数の呼び出しが必要
      if ((best_value = tte->evaluation_value()) == evaluation::Value::NONE) {
        best_value = evaluation::Evaluate(position);
      }
      ss->static_evaluation = best_value;

      if (tt_value != evaluation::Value::NONE &&
          (tte->bound() &
           (tt_value > best_value ? Bound::LOWER : Bound::UPPER)) !=
              Bound::NONE) {
        // 置換表のスコアが、この局面で今回探索するものと同等か少し劣るくらいの精度で
        // 探索されたものならば、置換表のスコアをbest_valueの初期値とする
        best_value = tt_value;
      }
    } else {
      ss->static_evaluation = best_value = evaluation::Evaluate(position);
    }

    // stand pat.

    if (best_value >= beta) {
      if (!tt_hit) {
        tte->Save(hash_key, evaluation::ConvertValueForTT(best_value, ss->ply),
                  false, Bound::LOWER, Depth::NONE, state_action::Move::NONE,
                  ss->static_evaluation);
      }
      return best_value;
    }

    if (pv_node && best_value > alpha) {
      // alphaの初期値
      alpha = best_value;
    }

    // futilityの基準となる値
    // これを下回るようなら枝刈り
    futility_base = best_value + parameter::futility_margin_quiet;
  }

  // ------------------------------
  // 1手ずつ調べる
  // ------------------------------

  // 駒を捕り合う指し手のみを生成する
  // Searchから呼ばれた場合、直前の指し手がMove::NULLである可能性があるが、
  // 静止探索の最初の指し手ではrecaptureを生成しないので大丈夫

  MovePickerQuiet mp(
      position, tt_move, depth, &position.ptr_thread()->main_history,
      &position.ptr_thread()->capture_history, (ss - 1)->current_move.target());
  // この後でノードを展開するにあたって評価値を差分計算できないと
  // 速度面で良くないので、差分計算できるようにする
  evaluation::Update(position);

  while ((move = mp.Next()) != state_action::Move::NONE) {
    ASSERT_MSG_LV3(position.IsPseudoLegal(move), "move={}, sfen={}",
                   detail::Move(move), position.sfen());

    // ------------------------------
    // 局面を進める前の枝刈り
    // ------------------------------

    checking = position.IsCheckMove(move);
    ++move_count;

    // futility pruning
    // 自玉に王手がかかっていなくて、敵玉に王手する指し手でもない時
    // 今回で捕獲される駒による評価値の上昇分を加味しても
    // alphaを超えそうにないなら枝刈り

    if (!checked && !checking &&
        futility_base > -evaluation::Value::KNOWN_WIN) {
      futility_value =
          futility_base +
          evaluation::captured_value_delta[position[move.target()]] +
          (move.promotion()
               ? static_cast<evaluation::Value>(
                     evaluation::promotion_value_delta[position[move.source()]])
               : evaluation::Value::ZERO);

      if (futility_value <= alpha) {
        // 捕獲する駒と成りの分を加えてもalphaを超えない酷い指し手
        // 枝刈りをする
        best_value = std::max(best_value, futility_value);
        continue;
      }

      if (futility_base <= alpha &&
          !position.IsSeeGe(move, evaluation::Value::ZERO + 1)) {
        // この局面の評価値にmarginを加算しているがalphaを超えない
        // さらに、SEEがプラスではないので悪い手だろう
        // 枝刈りをする
        best_value = std::max(best_value, futility_base);
        continue;
      }
    }

    // 駒を捕らない王手を回避する指し手を枝刈り
    // 成りでなくSEEが負の指し手も王手回避でなくとも良くない指し手の可能性が高いので枝刈り
    // ただし、王手されている場合に有効な回避手を見つけていないのに枝刈りしてしまうと
    // 回避手がないように錯覚してしまうので、現状のスコアが詰みでない必要がある

    // 枝刈りの候補
    evasion_prunable = checked && (depth != Depth::ZERO || move_count > 2) &&
                       best_value > evaluation::Value::LOSE_IN_MAX_PLY &&
                       !position.IsCapture(move);
    if ((!checked || evasion_prunable) &&
        !position.IsFuPromotion(move) &&  // 歩が成る指し手を除外
        !position.IsSeeGe(move)) {
      continue;
    }

    // ------------------------------
    // 局面を進める
    // ------------------------------

    // 非合法手である可能性はかなり低いので、判定をできるだけ遅延
    if (!position.IsLegal(move)) {
      --move_count;
      continue;
    }
    ASSERT_MSG_LV3(position.IsPseudoLegal(move) && position.IsLegal(move),
                   "move={}, sfen={}", detail::Move(move), position.sfen());

    ss->current_move = move;

    position.StepForward(move, rb, checking);

    // 移動後は王手されていないはず
    ASSERT_MSG_LV3(!position.IsEffected(position.side(),
                                        position.ou_square(~position.side())),
                   "move={}, sfen={}", detail::Move(move), position.sfen());

    value =
        -QuietSearch<Type>(position, ss + 1, -beta, -alpha, depth - Depth::ONE);
    position.StepBackward(move);

    ASSERT_MSG_LV3(evaluation::IsFinite(value), "value={}", enum_cast<>(value));

    // best_value, alphaを更新するか
    if (value > best_value) {
      best_value = value;
      if (value > alpha) {
        if (pv_node) {
          // fail highでも更新してOK
          UpdatePV(ss->pv, move, (ss + 1)->pv);
        }

        if (pv_node && value < beta) {
          // alphaの更新はこのタイミングでOK
          alpha = value;
          best_move = move;
        } else {
          ASSERT_MSG_LV1(tt_depth >= Depth::NONE, "tt_depth={}",
                         enum_cast<>(tt_depth.value));

          tte->Save(hash_key, evaluation::ConvertValueForTT(value, ss->ply),
                    false, Bound::LOWER, tt_depth, move, ss->static_evaluation);
          return value;
        }
      }
    }
  }

  // ------------------------------
  // 指し手を調べ終えた
  // ------------------------------

  if (checked && best_value == -evaluation::Value::INFINITE) {
    // 全ての指し手を調べたが、有効なスコアがついていない
    // 詰みである
    // このノードを再訪する可能性は低いので置換表に登録しない
    best_value = evaluation::GetLoseValue(ss->ply);
  } else {
    ASSERT_MSG_LV1(tt_depth >= Depth::NONE, "tt_depth={}",
                   enum_cast<>(tt_depth.value));

    tte->Save(hash_key, evaluation::ConvertValueForTT(best_value, ss->ply),
              false,
              (pv_node && best_value > old_alpha) ? Bound::EXACT : Bound::UPPER,
              tt_depth, best_move, ss->static_evaluation);
  }

  // 置換表にabs(value) < Value::INFINITEの値しか登録しない
  // また、この関数もその範囲の値しか返さない
  // しかし、置換表の衝突があった場合はその限りではない
  // このノードはrootからss->ply手進めた局面なので、
  // ss->ply手より短い詰みがあるのはおかしいが、
  // 置換表の衝突があった場合はそんな値を作り出してしまう
  // ただし、通常探索のmate distance pruningで枝刈りされるので問題ない
  ASSERT_MSG_LV3(evaluation::IsFinite(best_value), "best_value={}",
                 enum_cast<>(best_value));

  return best_value;
}

template evaluation::Value QuietSearch<NodeType::PV>(
    state_action::Position& position, Stack* const ss, evaluation::Value alpha,
    const evaluation::Value beta, const Depth depth);
template evaluation::Value QuietSearch<NodeType::NonPV>(
    state_action::Position& position, Stack* const ss, evaluation::Value alpha,
    const evaluation::Value beta, const Depth depth);
}  // namespace search
