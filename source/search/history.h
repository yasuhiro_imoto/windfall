#pragma once
#ifndef HISTORY_H_INCLUDED
#define HISTORY_H_INCLUDED

#include <array>
#include <type_traits>

#include "../rule/piece.h"
#include "../rule/square.h"
#include "../state_action/move.h"
#include "../utility/level_assert.h"

namespace search {
//! statistics tableの値を保存する
/*! 値はだいたいは数値だが、指し手や入れ子になったhistoryもある
    多次元配列であるかのように振る舞う
    生の値を用いる代わりに、history
   updateを行なうentry上でoperator<<()を呼び出す Bは要素の値の絶対値の上限
    StatsEntry */
template <typename T, int B>
class StatisticsEntry {
  //! operator()の戻り値の型
  using TT = typename std::conditional_t<std::is_integral_v<T>, int, T>;

 public:
  StatisticsEntry() : entry_(TT()) {}

  StatisticsEntry& operator=(const T& v) {
    entry_ = v;
    return *this;
  }
  T* operator&() { return &entry_; }
  T* operator->() { return &entry_; }
  operator const T&() const { return entry_; }

  //! bonusを加算
  /*! 値が範囲内に収まるように制限 */
  StatisticsEntry& operator<<=(const int bonus) {
    ASSERT_LV3(abs(bonus) <= B);
    static_assert(B <= std::numeric_limits<T>::max(), "B overflows T");

    entry_ += bonus - entry_ * abs(bonus) / B;
    ASSERT_LV3(abs(entry_) <= B);

    return *this;
  }

 private:
  T entry_;
};

//! 様々な統計情報を格納する汎用N次元配列
/*! T: entryの型
    B: entryの値の範囲 [-B, B]
    Size, Sizes: 配列の次元数
    Stats */
template <typename T, int B, int Size, int... Sizes>
struct Statistics : private std::array<Statistics<T, B, Sizes...>, Size> {
 private:
  using Array = std::array<Statistics<T, B, Sizes...>, Size>;

 public:
  T* get() { return this->at(0).get(); }

  void fill(const T& v) {
    T* p = get();
    std::fill(p, p + sizeof(*this) / sizeof(*p), v);
  }

  using Array::begin;
  using Array::end;
  using Array::operator[];
};

template <typename T, int B, int Size>
struct Statistics<T, B, Size>
    : private std::array<StatisticsEntry<T, B>, Size> {
 private:
  using Array = std::array<StatisticsEntry<T, B>, Size>;

 public:
  T* get() { return &this->at(0); }

  using Array::begin;
  using Array::end;
  using Array::operator[];
};

enum StatisticsParameters {
  NOT_USED = 0  //!< statistics tableにおいてBを0にした場合、このtemplate
                //!< parameterは用いないという意味
};

//! 現在の探索中にquietな指し手がどれくらい成功/失敗したか
/*! reductionと指し手オーダリングの決定のために用いられる
    cf. http://chessprogramming.wikispaces.com/Butterfly+Boards
    簡単に言うと、sourceの駒をtargetに移動させることに対するhistory
    やねうら王では、ここで用いられるsourceは、駒打ちのときに特殊な値になっていて、盤上のsourceとは区別される
    そのため、(SQ_SIZE + 7)まで移動元がある
    ButterflyHistory */
using ButterflyHistory =
    Statistics<int16_t, 10368, (Square::SIZE + 7) * Square::SIZE, Color::SIZE>;

//! 直前の指し手の[target][piece]によってindexされるcounter moves(応手)を格納
/*! cf. http://chessprogramming.wikispaces.com/Countermove+Heuristic
    CounterMoveHistroy */
using CounterMoveHistory =
    Statistics<state_action::Move, NOT_USED, Square::SIZE, Piece::SIZE>;

//! 指し手の[target][piece][captured piece type]で示される
/*! CapturePieceToHistory */
using CapturePieceTargetHistory =
    Statistics<int16_t, 10368, Square::SIZE, Piece::SIZE, Piece::TYPE_SIZE>;

//! ButterflyHistoryに似たものだが、指し手の[target][piece]で示される
/*! PieceToHistory */
using PieceTargetHistory =
    Statistics<int16_t, 29952, Square::SIZE, Piece::SIZE>;

//! 与えられた2つの指し手のhistoryを組み合わせたもので、普通、1手前によって与えられる現在の指し手(によるcombined
//! history)
/*! このnested history
   tableは、ButterflyBoardsの代わりに、PieceTargetHistoryをベースとしている
    ContinuationHistory */
using ContinuationHistory =
    Statistics<PieceTargetHistory, NOT_USED, Square::SIZE, Piece::SIZE>;
}  // namespace search
#endif  // !HISTORY_H_INCLUDED
