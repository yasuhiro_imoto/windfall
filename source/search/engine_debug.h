#pragma once
#ifndef ENGINE_DEBUG_H_INCLUDED
#define ENGINE_DEBUG_H_INCLUDED

#pragma warning(push)
#pragma warning(disable : 6387 26439 26451 26495 26812 28182)
#include <spdlog/fmt/fmt.h>
#pragma warning(pop)

#include "../state_action/move.h"

namespace search {
namespace detail {
struct Move {
  Move() = default;
  Move(const state_action::Move move) : m(move) {}
  state_action::Move m;
};
}  // namespace detail
}  // namespace search

template <>
struct fmt::formatter<search::detail::Move> : formatter<string_view> {
  template <typename FormatContext>
  auto format(const search::detail::Move& move, FormatContext& ctx) {
    std::ostringstream oss;
    if (move.m.drop()) {
      oss << fmt::format("[piece={},target={}]",
                         static_cast<int>(move.m.dropped_piece()),
                         static_cast<int>(move.m.target()));
    } else {
      oss << fmt::format("[source={},target={},promotion={}]",
                         static_cast<int>(move.m.source()),
                         static_cast<int>(move.m.target()), move.m.promotion());
    }

    return formatter<string_view>::format(oss.str(), ctx);
  }
};

#endif  // !ENGINE_DEBUG_H_INCLUDED
