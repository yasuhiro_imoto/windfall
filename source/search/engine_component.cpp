#include "stdafx.h"

#include "engine_component.h"

#include "../rule/piece.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "history.h"
#include "search_stack.h"
#include "thread.h"

#define BOOST_ENABLE_ASSERT_HANDLER
#if ASSERT_LV > 0
// release modeでも有効なアサーション

#ifdef NDEBUG
// 一旦取り消す
#undef NDEBUG
// 取り消したことを記録
#define NDEBUG_DEFINED
#endif  // NDEBUG

#include <boost/assert.hpp>

#ifdef NDEBUG_DEFINED
#define NDEBUG
// フラグを削除
#undef NDEBUG_DEFINED
#endif  // NDEBUG_DEFINED

#else
// 普通のrelease modeでは消えるアサーション
#include <boost/assert.hpp>
#endif  // ASSERT_LV > 0

namespace search {
std::array<int, razoring_depth> razor_margin;
int reduction_table[2][2][64][64];

void UpdatePV(state_action::Move* pv, const state_action::Move move,
              const state_action::Move* child_pv) {
  *pv++ = move;
  while (child_pv && *child_pv != state_action::Move::NONE) {
    *pv++ = *child_pv++;
  }
  *pv = state_action::Move::NONE;
}

void UpdateContinuationHistory(Stack* const ss, const Piece piece,
                               const Square target, const int bonus) {
  for (const int i : {1, 2, 4}) {
    if ((ss - i)->current_move.ok()) {
      (*(ss - i)->continuation_history)[target][piece] <<= bonus;
    }
  }
}

void UpdateCaptureStatistics(const state_action::Position& position,
                             const state_action::Move move,
                             const state_action::Move* const captures,
                             const int capture_count, const int bonus) {
  CapturePieceTargetHistory& capture_history =
      position.ptr_thread()->capture_history;
  Piece moved_piece = position.moved_piece(move);
  Piece captured_piece = position[move.target()].type();

  capture_history[move.target()][moved_piece][captured_piece] <<= bonus;
  for (int i = 0; i < capture_count; ++i) {
    moved_piece = position.moved_piece(captures[i]);
    captured_piece = position[captures[i].target()].type();
    capture_history[captures[i].target()][moved_piece][captured_piece] <<=
        -bonus;
  }
}

void UpdateQuietStatistics(const state_action::Position& position,
                           Stack* const ss, const state_action::Move move,
                           const state_action::Move* const quiets,
                           const int quiet_count, const int bonus) {
  BOOST_ASSERT_MSG(position.IsPseudoLegal(move),
                   boost::str(boost::format("tt_move=0x%1$X, sfen=%2%") %
                              static_cast<uint64_t>(move) % position.sfen())
                       .c_str());
  ASSERT_LV1(position.IsPseudoLegal(move));

  // killerの更新
  if (ss->killlers[0] != move) {
    // 2個しか場所がないので、[0]を降格させて新しい値を設定
    ss->killlers[1] = ss->killlers[0];
    ss->killlers[0] = move;
  }

  // historyの更新

  const Color us = position.side();
  Thread* const ptr_thread = position.ptr_thread();
  ptr_thread->main_history[move.Serialize()][us] <<= bonus;
  UpdateContinuationHistory(ss, position.moved_piece(move), move.target(),
                            bonus);

  if ((ss - 1)->current_move.ok()) {
    // 直前に移動させたマス
    // 今回の指し手は駒を捕る指し手でないはずなので、そのマスに駒がある
    const Square previous = (ss - 1)->current_move.target();
    ptr_thread->counter_moves[previous][position[previous]] = move;
  }
  for (int i = 0; i < quiet_count; ++i) {
    ptr_thread->main_history[quiets[i].Serialize()][us] <<= -bonus;
    UpdateContinuationHistory(ss, position.moved_piece(quiets[i]),
                              quiets[i].target(), -bonus);
  }
}
}  // namespace search
