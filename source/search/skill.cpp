#include "stdafx.h"

#include "skill.h"

#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../utility/enum_cast.h"
#include "../utility/random_generator.h"
#include "../utility/time_point.h"
#include "root_move.h"
#include "thread.h"

namespace search {
state_action::Move Skill::PickBest(const size_t multi_pv) {
  const RootMoves& root_moves = thread_pool.main_thread()->root_moves;
  static utility::PseudoRandom generator(utility::now());

  // 降順でソートされている
  const evaluation::Value top_score = root_moves[0].score;
  const int delta = std::min<int>(
      static_cast<int>(top_score - root_moves[multi_pv - 1].score),
      evaluation::PieceScore::FU);
  const int weakness = 120 - 2 * level;
  evaluation::Value max_score = -evaluation::Value::INFINITE;

  for (size_t i = 0; i < multi_pv; ++i) {
    // 魔法の公式
    // 第1項はレベルが低いと大きな値になる
    // 第2項はランダムな要素
    const evaluation::Value offset = static_cast<evaluation::Value>(
        (weakness * static_cast<int>(top_score - root_moves[i].score) +
         delta * (generator.rand<unsigned>() % weakness)) /
        128);

    if (root_moves[i].score + offset >= max_score) {
      max_score = root_moves[i].score + offset;
      best = root_moves[i].pv[0];
    }
  }

  return best;
}
}  // namespace search
