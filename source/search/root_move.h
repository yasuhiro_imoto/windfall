#pragma once
#ifndef ROOT_MOVE_H_INCLUDED
#define ROOT_MOVE_H_INCLUDED
#include <vector>

#include "../evaluation/value.h"
#include "../state_action/move.h"

namespace state_action {
class Position;
}

namespace search {
//! countermoves based pruningで使う閾値
constexpr int counter_move_prune_threshold = 0;

//! rootでの指し手
/*! それぞれの指し手に対してscoreとpvを持っている
    fail lowの場合はその値は信用できない */
struct RootMove {
  // pv[0]は引数で渡された指し手を設定する
  explicit RootMove(const state_action::Move move) : pv(1, move) {}

  bool operator==(const state_action::Move move) const noexcept {
    return pv.front() == move;
  }
  bool operator!=(const state_action::Move move) const noexcept {
    return pv.front() != move;
  }

  //! ソートでデフォルトで降順にしたいので、不等号を反転
  /*! スコアが同じ場合は、前回のスコアで比較 */
  bool operator<(const RootMove& m) const {
    return score != m.score ? m.score < score
                            : m.previous_score < previous_score;
  }

  //! ponderの指し手がないときにponderの指し手を置換表からひねり出す。pv[1]に格納する。
  /*! ponder_candidateが2手目の局面で合法手なら、それをpv[1]に格納する。
      それすらなかった場合はfalseを返す。
      extract_ponder_from_tt */
  bool ExtractPonderFromTT(state_action::Position& position,
                           const state_action::Move ponder_candidate);

  //! 今回の探索でのスコア
  /*! score */
  evaluation::Value score = -evaluation::Value::INFINITE;
  //! 前回の探索でのスコア
  /*! 次の探索の探索窓の範囲の決定に使う
      previousScore */
  evaluation::Value previous_score = -evaluation::Value::INFINITE;

  //! rootから最大で何手目まで探索したか(選択深さ)
  /*! selDepth */
  int selective_depth = 0;

  //! この差し手で進めた時のpv
  std::vector<state_action::Move> pv;
};

using RootMoves = std::vector<RootMove>;
}  // namespace search
#endif  // !ROOT_MOVE_H_INCLUDED
