#pragma once
#include <array>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <numeric>
#include <queue>
#include <utility>
#include <vector>

#include "../utility/configuration.h"

#include "../evaluation/nn/feature/index_list.h"
#include "../evaluation/value.h"
#include "../state_action/move.h"
#include "../state_action/packed_sfen.h"
#include "../usi/option.h"
#include "../utility/random_generator.h"

namespace state_action {
class Position;
}

#ifdef NNUE_LEARN
namespace search {
namespace learner {
using ValueAndPV =
    std::pair<evaluation::Value, std::vector<state_action::Move>>;

/**
 * @brief 学習用の静止探索
 *
 * positionの探索スレッドが設定されている必要がある
 * thread_pool.stopがtrueになると探索を中断してしまうので、その時のPVは正しくない
 * 呼び出し後にthread_pool.stopを確認する必要がある
 *
 * @param position
 * @return PV。詰みの場合はMove::RESIGNが返る
 */
ValueAndPV QuietSearch(state_action::Position& position);

/**
 * @brief 学習用の通常探索
 *
 * Multi PVが有効な場合は、探索スレッドの方から結果を取得する
 * ルート局面での宣言勝ちの判定は行わないので、呼び出し側で処理する必要がある
 * positionの探索スレッドが設定されている必要がある
 * thread_pool.stopがtrueになると探索を中断してしまうので、その時のPVは正しくない
 * 呼び出し後にthread_pool.stopを確認する必要がある
 *
 * @param position
 * @param depth
 * @param multi_pv
 * @param nodes_limit
 * @return
 */
ValueAndPV Search(state_action::Position& position, const int depth,
                  const size_t multi_pv = 1, const uint64_t nodes_limit = 0);

struct MultiThink {
  MultiThink() : loop_count_(0) {}

  /**
   * @brief
   * メインスレッドからこの関数を呼び出すと、スレッドがそれぞれで処理を行う
   *
   * ・各スレッドが安全にSearch, QuietSearchを呼び出せるように
   * 　置換表をスレッドごとに分離する(終了後に戻す)
   * ・定跡は一時的にオフにする
   * go_think
   */
  void Think();

  /**
   * @brief gRPCで呼ばれる
   *        スレッドを生成して与えられたデータを処理する
   */
  void StartWorker();
  /**
   * @brief スレッドの終了を待つ
   */
  void WaitForFinish();

  /**
   * @brief タスク固有な初期化処理
   *
   * Think内の初期化の後に呼び出される
   * init
   */
  virtual void Initialize() {}

  /**
   * @brief スレッドを生成して処理を行う
   * @param thread_id
   */
  virtual void Run(const size_t thread_id) = 0;

  /**
   * @brief Thinkの際に一定間隔ごとに呼び出される処理
   *
   * callback_func
   */
  std::function<void()> callback;
  /**
   * @brief callbackを呼び出す間隔
   *        単位は秒
   *
   * callback_seconds
   */
  static constexpr uint32_t callback_interval = 600;

  void SetLoopMax(const uint64_t max) noexcept { loop_max_ = max; }
  uint64_t loop_max() const noexcept { return loop_max_; }

  /**
   * @brief ループ回数を取り出して、更新する
   *
   * ループ回数が上限に達していたらuint64_tの最大値を返す
   * @return
   */
  uint64_t GetNextLoopCount() {
    std::unique_lock<std::mutex> lock(mutex_);
    if (loop_count_ >= loop_max_) {
      return std::numeric_limits<uint64_t>::max();
    }
    return loop_count_++;
  }

  uint64_t done_count() {
    std::unique_lock<std::mutex> lock(mutex_);
    return ++done_count_;
  }

  /**
   * @brief I/Oにアクセスするときに使う
   *
   * io_mutex
   */
  std::mutex io_mutex;

 protected:
  utility::AsyncPseudoRandom generator_;

 private:
  std::mutex mutex_;

  /**
   * @brief workerが処理する回数
   */
  std::atomic<uint64_t> loop_max_;
  /**
   * @brief workerが処理した回数
   */
  std::atomic<uint64_t> loop_count_;
  /**
   * @brief 処理した回数を返す用
   */
  std::atomic<uint64_t> done_count_;

  /**
   * @brief スレッドが終了したかどうかのフラグ
   *
   * bool型だとマルチスレッドでアクセスしたときにおかしくなるはず
   */
  std::vector<uint8_t> thread_finished_;

  std::vector<std::thread> threads_;
  usi::OptionsMap backup_options_;
};

#pragma warning(push)
#pragma warning(disable : 26495)
struct LearningData {
  /**
   * @brief packed sfen
   */
  state_action::PackedSfen sfen;
  /**
   * @brief 読み筋を求める深さ
   */
  int32_t depth;
  /**
   * @brief 出力の順序を入力と同じにするための順番
   */
  int32_t id;
};

struct SearchResult {
  /**
   * @brief 入力局面のid
   */
  int32_t id;
  /**
   * @brief 探索した読み筋
   *
   * protocol bufferに16bit整数型はないので、32bitのままにする
   */
  std::vector<state_action::Move> pv;

  int32_t score;
};

/**
 * @brief 学習データの起点となる読み筋
 */
struct TeacherPV {
  /**
   * @brief packed sfen
   */
  state_action::PackedSfen sfen;
  /**
   * @brief 探索した読み筋
   *
   * protocol bufferに16bit整数型はないので、32bitのままにする
   */
  std::vector<state_action::Move> pv;
  /**
   * @brief 局面の信頼できる評価値
  */
  int32_t score;
};

struct TrainingPair {
  std::array<evaluation::nn::feature::IndexList, 2> teacher_indices;
  std::array<evaluation::nn::feature::IndexList, 2> student_indices;
  double teacher_sign, student_sign;
  /**
   * @brief 教師局面の評価値
  */
  int32_t teacher_score;

#ifdef _DEBUG
  // 必要ないがデバッグのために読み筋を記録する
  std::vector<state_action::Move> teacher_pv, student_pv;
#endif // _DEBUG
};
#pragma warning(pop)

/**
 * @brief 昇順でソートするためのオペレータ
 *
 * 普通に定義する
 * @param x
 * @param y
 * @return
 */
inline bool operator>(const std::shared_ptr<SearchResult>& x,
                      const std::shared_ptr<SearchResult>& y) {
  return x->id > y->id;
}

template <typename T>
class InputQueue {
 public:
  InputQueue() : completed(false) {}

  std::unique_ptr<T> pop() {
    std::unique_lock<std::mutex> lk(mutex_);
    cv_.wait(lk, [&] { return !queue_.empty() || completed; });

    if (queue_.empty()) {
      // 全てのデータを処理した
      // ワーカースレッドに終了を通知する

      // nullptrなら終了
      return std::unique_ptr<T>();
    } else {
      auto tmp = std::move(queue_.front());
      queue_.pop();
      return tmp;
    }
  }

  /**
   * @brief
   * @param data gRPCのデータ型の宣言が難しいので関数の外でデータを作成する
   */
  void push(std::unique_ptr<T>&& data) {
    std::lock_guard<std::mutex> lk(mutex_);
    const bool signal = queue_.empty();
    queue_.emplace(std::move(data));
    if (signal) {
      cv_.notify_one();
    }
  }

  void MarkAsFinished() {
    completed = true;
    cv_.notify_all();
  }

 private:
  /**
   * @brief 全てのデータを読み込むとtrue
   */
  std::atomic_bool completed;

  std::mutex mutex_;
  std::condition_variable cv_;

  /**
   * @brief 学習データを入れる
   *
   * サイズの上限はない
   */
  std::queue<std::unique_ptr<T>> queue_;
};

class OutputQueue {
 public:
  OutputQueue() : completed(false), count_(0) {}

  std::shared_ptr<SearchResult> pop() {
    std::unique_lock<std::mutex> lk(mutex_);
    cv_.wait(lk, [&] {
      return (!queue_.empty() && queue_.top()->id == count_) || completed;
    });

    if (completed) {
      // 全部のデータを出力した
      return std::shared_ptr<SearchResult>();
    } else {
      auto tmp = queue_.top();
      queue_.pop();

      ++count_;

      return tmp;
    }
  }

  void push(std::shared_ptr<SearchResult>&& data) {
    bool signal;
    {
      std::lock_guard<std::mutex> lk(mutex_);
      // 次に出力したいデータが来たかどうか
      signal = data->id == count_;

      queue_.emplace(std::move(data));
    }
    if (signal) {
      cv_.notify_one();
    }
  }

  /**
   * @brief 全てのデータを書き出すとtrue
   */
  std::atomic_bool completed;

 private:
  std::mutex mutex_;
  std::condition_variable cv_;

  int count_;

  /**
   * @brief 結果を書きだす前のバッファ
   *
   * 全部の結果がここに貯められる可能性がある
   *
   * 昇順にソートして取り出す
   * わかりにくいと思うので、不等号を逆にしたオペレータを定義することはしない
   *
   * 先頭の要素はconst referenceなので、unique_ptrではなくshared_ptrになった
   */
  std::priority_queue<std::shared_ptr<SearchResult>,
                      std::vector<std::shared_ptr<SearchResult>>,
                      std::greater<std::shared_ptr<SearchResult>>>
      queue_;
};

struct MultiSearch : public MultiThink {
  void Run(const size_t thread_id) override;

  InputQueue<LearningData> input_queue;
  OutputQueue output_queue;
};

struct MultiSearchPair : public MultiThink {
  void Run(const size_t thread_id) override;

  InputQueue<TeacherPV> input_queue;
  InputQueue<TrainingPair> output_queue;
};

#ifdef _DEBUG
/**
 * @brief 教師局面の特徴インデックスと生徒の読み筋の局面の特徴インデックスを作る
 * 
 * まず教師PVに従って終端まで移動して教師局面の特徴インデックスを作る
 * PVが一致しなくなるまでPVを遡りながら探索の深さを一つずつ増やす
 * 一致しなくなった時の読み筋の終端が生徒局面になる
 * @param position 
 * @param it 
 * @param end 
 * @param training_pair
 * @return 
*/
bool GetTrainingPair(state_action::Position& position,
                     std::vector<state_action::Move>::const_iterator it,
                     const std::vector<state_action::Move>::const_iterator end,
                     TrainingPair& training_pair);
#endif  // _DEBUG

}  // namespace learner
}  // namespace search
#endif  // NNUE_LEARN
