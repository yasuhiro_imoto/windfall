#pragma once
#ifndef THREAD_H_INCLUDED
#define THREAD_H_INCLUDED
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

#include "../evaluation/value.h"
#include "../rule/color.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../utility/time_point.h"
#include "depth.h"
#include "history.h"
#include "root_move.h"
#include "search_limit.h"

namespace search {
//! 探索用スレッド
/*! メインスレッドはこれの派生クラスを使う */
class Thread {
 public:
  //! ThreadPoolで何番目か
  /*! 要するにスレッドIDを引数として渡す */
  explicit Thread(const std::size_t id);
  virtual ~Thread();

  /*! workerはmain threadから
      for(auto&th:Thread) {th->StartSearch();}
      のようにするとこの関数が呼ばれる
      MainThread::SearchはThinkが呼び出される
      search */
  virtual void Search() = 0;

protected:
  /**
   * @brief 探索を行う実体
  */
  void SearchTree();
public:
  //! 保持している探索で必要なテーブル(historyなど)をクリア
  /*! clear */
  void Clear();

  //! スレッド起動後、この関数が呼び出される
  /*! idle_loop */
  void IdleLoop();

  //! Searchを開始する
  /*! start_searching */
  void StartSearch();
  //! 探索が終わるのを待機する。(searchingフラグがfalseになるのを待つ)
  /*! wait_for_search_finished */
  void WaitForSearchFinished();

  //! thread id
  /*! スレッドごとにメモリ領域を割り当てたいときなどに必要 */
  std::size_t id() const { return id_; }

  //! multi pvの場合にroot_movesの何番目のpvの指し手か
  /*! インデックスは0から数える
      multi pvでない場合はこの値は常に0
      pvIdx */
  size_t pv_index;

  //! rootから最大何手まで探索したか
  /*! 選択深さの最大値
      selDepth */
  int selective_depth;

  //! null moveを前回適用したply
  /*! nmpMinPly */
  int nmp_min_ply;

  //! null moveを前回適用したcolor
  /*! nmpColor */
  Color nmp_color;

  //! このスレッドが探索したノード数
  /*! ほぼPosition::DoMoveを呼び出した回数に等しい
      nodes */
  std::atomic<uint64_t> nodes;

  //! 反復深化においてbest_moveが変わった回数
  /*! 評価の安定性の指標として使う
      全スレッドで集計して使う
      bestMoveChanges */
  std::atomic<uint64_t> best_move_changes;

  //! 探索開始局面
  /*! rootPos */
  state_action::Position root_position;

 private:
  //! exitフラグやsearchingフラグの状態を変更に使う
  /*! mutex */
  std::mutex mutex_;
  //! idle_loopで待機している時の待つ対象
  /*! cv */
  std::condition_variable cv_;

  //! mainスレッドは0 workerスレッドは1から順に番号を割り当てる
  /*! idx */
  std::size_t id_;

  //! このフラグが立ったら終了する
  /*! exit */
  bool exit_flag = false;
  //! 探索中であるかを表すフラグ
  /*! searching */
  bool searhing_flag = true;

  /*! stdThread */
  std::thread thread_;

public:
  //! 探索開始局面で探索対象とする指し手の集合
  /*! 明示的に指定されていなければ合法手全てが対象
      ただし、歩の不成などは除く
      rootMoves */
  RootMoves root_moves;

  //! 反復深化の深さ
  /*! lazy smpなのでスレッドごとに記録している
      rootDepth */
  Depth root_depth;
  //! このスレッドで完了した反復深化の深さ
  /*! completedDepth */
  Depth completed_depth;

  //! 近代的なMovePickerによるオーダリングのためのhistory
  /*! counterMoves */
  CounterMoveHistory counter_moves;
  //! 近代的なMovePickerによるオーダリングのためのhistory
  /*! mainHistory */
  ButterflyHistory main_history;
  //! 近代的なMovePickerによるオーダリングのためのhistory
  /*! captureHistory */
  CapturePieceTargetHistory capture_history;
  //! 近代的なMovePickerによるオーダリングのためのhistory
  /*! コア数が多いか、長い持ち時間においては、
      ContinuationHistoryもスレッドごとに確保したほうが良いらしい
      cf. https://github.com/official-stockfish/Stockfish/commit/
          5c58d1f5cb4871595c07e6c2f6931780b5ac05b5
      continuationHistory */
  ContinuationHistory continuation_history;

  //!  PositionクラスのEvalationListのalignasのため
  void* operator new(const size_t s);
  void operator delete(void* p);

  //! スレッドIDを取得
  /*! スレッドごとにメモリを割り当てたいときに使う
      main_threadならid=0, workerならid=1, 2, ...
      thread_id */
  size_t thread_id() const { return id_; }
};

class SubordinateThread :public Thread {
public:
  //! constructorはそのまま
  using Thread::Thread;

  //! 探索を開始する
  /*! search */
  void Search() override;

};

//! 探索のmain thread
class MainThread : public Thread {
public:
  //! constructorはそのまま
  using Thread::Thread;

  //! 探索を開始する
  /*! search */
  void Search() override;

  //! 思考時間の終わりが来たかを確かめる
  /*! check_time */
  void CheckTime();

  //! 反復深化の前回のiteration時のtime_reductionの値
  /*! previousTimeReduction */
  double previous_time_reduction;

  //! 前回の探索時のスコア
  /*! 次の探索で使えるかも
      previousScore */
  evaluation::Value previous_score;

  //! CheckTimeで用いるカウンター
  /*! デクリメントしていきこれが0になるごとに思考をストップするのか判定
      callsCnt */
  int call_counter;

  //! "go ponder" コマンドでの探索中であるかを示すフラグ
  /*! ponder */
  std::atomic_bool ponder;

  //! 将棋所に出力する時間間隔を管理
  /*!
     将棋所のコンソールが詰まるので一定時間経過するごとに出力するという方式を採る
      lastPvInfoTime */
  utility::TimePoint last_pv_time;

  //! Ponder用の指し手
  /*! Stockfishは置換表からponder moveをひねり出すコードになっているが、
      前回iteration時のPVの2手目の指し手で良いのではなかろうか…
      ponder_candidate */
  state_action::Move ponder_candidate;
};

//! スレッドオブジェクトの配列
/*! global空間に配置されるのでconstructor, destoructorはない
    スレッド数に1以上を指定することで生成
    0を指定することで破壊になる */
struct ThreadPool {
  //! main threadに思考させる
  /*! start_thinking */
  void StartThinking(const state_action::Position& position,
                     state_action::StateListPtr& states,
                     const SearchLimit& limit, const bool ponder_mode = false);

  //! 生成したスレッドの初期化
  /*! clear */
  void Clear();
  //! スレッド数を変更
  /*! すべてのスレッドの終了を待つ必要があるため
      終了時は明示的にSet(0)として呼び出すこと
      set */
  void Set(const size_t requested);

  //! main threadを取得
  /*! main */
  MainThread* main_thread() { return static_cast<MainThread*>(threads_[0]); }

  //! 今回のgoコマンド以降に探索したノード数
  /*! nodes_searched */
  uint64_t nodes_searched() const { return Accumulate(&Thread::nodes); }

  //! 探索中にこれがtrueになったら探索を即座に終了する
  /*! stop */
  std::atomic_bool stop;

  size_t size() const { return threads_.size(); }
  bool empty() const { return threads_.empty(); }

  using iterator = std::vector<Thread*>::iterator;
  using const_iterator = std::vector<Thread*>::const_iterator;

  iterator begin() { return threads_.begin(); }
  iterator end() { return threads_.end(); }
  const_iterator begin() const { return threads_.begin(); }
  const_iterator end() const { return threads_.end(); }

  Thread& operator[](const size_t i) { return *threads_[i]; }

 private:
  //! 現局面までのリスト
  state_action::StateListPtr setup_states_;

  uint64_t Accumulate(std::atomic<uint64_t> Thread::*member) const {
    uint64_t sum = 0;
    for (Thread* th : threads_) {
      sum += (th->*member).load(std::memory_order_relaxed);
    }
    return sum;
  }

  std::vector<Thread*> threads_;
};

/*! Threads */
extern ThreadPool thread_pool;
}  // namespace search
#endif  // !THREAD_H_INCLUDED
