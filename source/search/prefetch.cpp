#include "stdafx.h"

#include <immintrin.h>

#include "../utility/level_assert.h"
#include "prefetch.h"

namespace search {
void Prefetch(void* addr) {
  // 変なアドレスはalignされていないので、バグの可能性が高い
  ASSERT_LV3((reinterpret_cast<int64_t>(addr) & 0x1F) == 0);

#ifdef __INTEL_COMPILER
  // 最適化でprefetch命令を削除されるのを防ぐhack
  __asm__("");
#endif  // __INTEL_COMPILER

  // 1 cache lineの読み込み
  // 多分32 bytes(Ryzenは32 btyes)
#if defined(__INTEL_COMPILER) || defined(_MSC_VER)
  _mm_prefetch(reinterpret_cast<const char*>(addr), _MM_HINT_T0);
#else
  __builtin_prefetch(addr);
#endif  // defined(__INTEL_COMPILER) || defined(_MSC_VER)
}

void Prefetch128(void* addr) {
  // NOTE: stockfishではこうらしい
  //
  Prefetch(addr);
  Prefetch(reinterpret_cast<uint8_t*>(addr) + 64);
}
}  // namespace search
