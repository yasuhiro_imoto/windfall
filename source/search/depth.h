#pragma once
#ifndef DEPTH_H_INCLUDED
#define DEPTH_H_INCLUDED

#include <cstdint>
#include <type_traits>

#include <boost/operators.hpp>

#include "../utility/configuration.h"
#include "../utility/enum_cast.h"

namespace search {
struct Depth : private boost::equality_comparable<Depth>,
               private boost::less_than_comparable<Depth> {
  enum Value : int32_t {
  /**
   * @brief depthの1手をスケール
   * ONE_PLY
   */
#if defined(ONE_PLY_EQ_1)
    ONE = 1,
#else
    ONE = 2,
#endif  // defined(ONE_PLY_EQ_1)
    /**
     * @brief 深さ0
     * DEPTH_ZERO
     */
    ZERO = 0,
    /**
     * @brief 静止探索で王手がかかっているときに
     *        これより少ない探索深さでの結果が置換表にあっても信用しない
     * DEPTH_QS_CHECKS
     */
    QS_CHECKS = 0,
    /**
     * @brief 静止探索で王手がかかっていないとき
     * DEPTH_QS_NO_CHECKS
     */
    QS_NO_CHECKS = -1 * ONE,
    /**
     * @brief 静止探索でこれより深い(残り探索深さが少ない)ところでは
     *        RECAPTURESしか生成しない
     * DEPTH_QS_RECAPTURES
     */
    QS_RECAPTURES = -5 * ONE,
    /**
     * @brief 探索せずに値を求めたいときの設定
     * DEPTH_NONE
     */
    NONE = -6 * ONE,
    /**
     * @brief TTのオフセット
     * DEPTH_OFFSET
     */
    OFFSET = NONE,
    /**
     * @brief 最大の深さ
     * DEPTH_MAX
     */
    MAX = max_ply * ONE
  };
  // Depth::ONEは2の冪乗でなければならない
  static_assert(!(static_cast<int32_t>(ONE) & (static_cast<int32_t>(ONE) - 1)),
                "Depth::ONE must be the nth power of 2.");

#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr Depth() : value(ZERO) {}
#pragma warning(pop)
  explicit constexpr Depth(const int d)
      : value(static_cast<Value>(d * enum_cast<>(ONE))) {}
  constexpr Depth(const Value d) : value(d) {}

  template <typename T, typename std::enable_if_t<std::is_integral_v<T>,
                                                  std::nullptr_t> = nullptr>
  constexpr operator T() const noexcept {
    return static_cast<T>(value) / static_cast<T>(ONE);
  }

  constexpr bool operator==(const Depth d) const noexcept {
    return value == d.value;
  }
  constexpr bool operator<(const Depth d) const noexcept {
    return value < d.value;
  }

  constexpr bool operator==(const Value v) const noexcept { return value == v; }
  constexpr bool operator!=(const Value v) const noexcept { return value != v; }

  constexpr bool operator<(const Value v) const noexcept { return value < v; }
  constexpr bool operator<=(const Value v) const noexcept { return value <= v; }
  constexpr bool operator>(const Value v) const noexcept { return value > v; }
  constexpr bool operator>=(const Value v) const noexcept { return value >= v; }

  constexpr bool operator<(const int v) const noexcept {
    return value < static_cast<Value>(v * Depth::ONE);
  }
  constexpr bool operator<=(const int v) const noexcept {
    return value <= static_cast<Value>(v * Depth::ONE);
  }
  constexpr bool operator>(const int v) const noexcept {
    return value > static_cast<Value>(v * Depth::ONE);
  }
  constexpr bool operator>=(const int v) const noexcept {
    return value >= static_cast<Value>(v * Depth::ONE);
  }

  constexpr Depth operator-() const noexcept {
    return Depth(static_cast<Value>(-value));
  }

  //! TTEntryで利用
  constexpr Depth operator+(const Depth d) const noexcept {
    return Depth(static_cast<Value>(value + d.value));
  }
  //! TTEntryで利用
  constexpr Depth operator*(const Depth d) const noexcept {
    return Depth(static_cast<Value>(value * d.value));
  }
  constexpr Depth operator-(const Depth d) const noexcept {
    return Depth(static_cast<Value>(value - d.value));
  }
  constexpr Depth operator/(const Depth d) const noexcept {
    return Depth(static_cast<Value>(value / d.value));
  }

  constexpr Depth operator+(const Value v) const noexcept {
    return Depth(static_cast<Value>(value + v));
  }
  constexpr Depth operator-(const Value v) const noexcept {
    return Depth(static_cast<Value>(value - v));
  }

  constexpr Depth operator+(const int v) const noexcept {
    return Depth(static_cast<Value>(value + v * enum_cast<>(ONE)));
  }
  constexpr Depth operator-(const int v) const noexcept {
    return Depth(static_cast<Value>(value - v * enum_cast<>(ONE)));
  }
  constexpr Depth operator*(const int v) const noexcept {
    return Depth(static_cast<Value>(value * v));
  }
  constexpr Depth operator/(const int v) const noexcept {
    return Depth(static_cast<Value>(value / v));
  }

  // Thread::SearchTreeで利用
  constexpr Depth& operator+=(const Depth d) noexcept {
    value = static_cast<Value>(value + d.value);
    return *this;
  }
  constexpr Depth& operator-=(const Depth d) noexcept {
    value = static_cast<Value>(value - d.value);
    return *this;
  }

  Value value;
};

constexpr Depth operator*(const int v, const Depth d) noexcept { return d * v; }

constexpr bool operator<(const Depth::Value x, const Depth y) noexcept {
  return x < y.value;
}
constexpr bool operator<=(const Depth::Value x, const Depth y) noexcept {
  return x <= y.value;
}
constexpr bool operator>(const Depth::Value x, const Depth y) noexcept {
  return x > y.value;
}
constexpr bool operator>=(const Depth::Value x, const Depth y) noexcept {
  return x >= y.value;
}

}  // namespace search
#endif  // !DEPTH_H_INCLUDED
