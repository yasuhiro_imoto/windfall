#pragma once
#ifndef PREFETCH_H_INCLUDED
#define PREFETCH_H_INCLUDED

namespace search {
/**
 * @brief 与えられたアドレスのデータをL1/L2キャッシュに事前に取り込む
 *
 * ノンブロッキング処理
 * @param addr 
 * prefetch
 */
void Prefetch(void* addr);

/**
 * @brief 128バイトの領域ををL1/L2キャッシュに事前に取り込む
 * @param addr 
 * prefetch2
*/
void Prefetch128(void* addr);
}
#endif // !PREFETCH_H_INCLUDED

