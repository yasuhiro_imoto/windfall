#pragma once
#ifndef SEARCH_INITIALIZATION_H_INCLUDED
#define SEARCH_INITIALIZATION_H_INCLUDED

namespace search {
/**
 * @brief 探索部を初期化
 *
 * init
*/
void Initialize();

/**
 * @brief 置換表のクリアを行う
 *
 * 時間のかかる処理を行う
 * isreadyに対する処理として呼び出される
 * clear
*/
void Clear();
}
#endif // !SEARCH_INITIALIZATION_H_INCLUDED

