#include "stdafx.h"

#include "thread.h"

#include "../evaluation/material.h"
#include "../evaluation/value.h"
#include "../rule/color.h"
#include "../state_action/move.h"
#include "../state_action/position.h"
#include "../usi/option.h"
#include "../usi/protocol.h"
#include "../utility/force_inline.h"
#include "../utility/logger.h"
#include "../utility/time_point.h"
#include "../utility/tool.h"
#include "engine.h"
#include "move_picker.h"
#include "search_limit.h"
#include "search_parameter.h"
#include "search_stack.h"
#include "skill.h"
#include "transposition_table.h"

namespace search {
/**
 * @brief 前回の出力からオプションの値より時間が経過しているならtrue
 *        rootでの残り探索深さが少ないなら経過時間によらず常にtrue
 * @param main_thread
 * @param root_depth
 * @return
 */
FORCE_INLINE bool CheckOutputInterval(const MainThread* const main_thread,
                                      const Depth root_depth) {
  return root_depth < 3 || main_thread->last_pv_time + limit.pv_interval <=
                               utility::timer.elapsed();
}

void Thread::SearchTree() {
  // (ss - 4)から(ss + 2)までアクセスするために余計に確保
  std::array<Stack, max_ply + 7> stack;
  Stack* const ss = stack.data() + 4;

  std::array<state_action::Move, max_ply + 1> pv;

  // このノードでbest moveを選択したときの評価値
  evaluation::Value best_value;
  // aspiration searchの窓の範囲
  evaluation::Value alpha, beta;
  // aspiration searchで窓を動かす量
  evaluation::Value delta;

  // 前回のiterationの記録
  // lastBestMove
  state_action::Move last_best_move = state_action::Move::NONE;
  // lastBestMoveDepth
  Depth last_best_move_depth = Depth::ZERO;

  // mainThread
  MainThread* main_thread =
      (this == thread_pool.main_thread() ? thread_pool.main_thread() : nullptr);

  // 読み筋が安定しているときに時間を短縮するときの係数
  // timeReduction
  double time_reduction = 1.0;
  // 直近でbest moveが変化した回数
  // 読み筋の安定度の指標とする
  // totBestMoveChanges
  double total_best_move_changes = 0;

  const Color Us = root_position.side();

  // 先頭の7個だけで十分
  // 残りは利用する際に初期化する
  std::memset(stack.data(), 0, 7 * sizeof(Stack));

  for (int i = 0; i < 4; ++i) {
    // 番兵を設定
    stack[i].continuation_history =
        &this->continuation_history[Square::ZERO][Piece::EMPTY];
  }
  ss->pv = pv.data();

  // 反復深化のiterationが浅いうちはaspiration searchをしない
  best_value = alpha = delta = -evaluation::Value::INFINITE;
  beta = evaluation::Value::INFINITE;

  size_t multi_pv = usi::options["MultiPV"];

  Skill skill(static_cast<int>(usi::options["SkillLevel"]));
  if (skill.enabled()) {
    multi_pv = std::max<size_t>(multi_pv, 4);
  }
  // 現局面の指し手より数が多くなるわけにはいかない
  multi_pv = std::min(multi_pv, root_moves.size());

  // 一つ目のroot_depthは、このスレッドの反復深化での深さ
  // 二つ目のroot_depthは、マスタースレッドでのみ有効で深さ制限がある場合の終了条件
  while ((root_depth += Depth::ONE) < Depth::MAX && !thread_pool.stop &&
         !(limit.depth && main_thread && root_depth > limit.depth)) {
    // -------------------------
    // Lazy SMPのための初期化
    // -------------------------

    if (main_thread) {
      // 古い世代の情報の重みを下げる
      total_best_move_changes /= 2;
    }

    for (RootMove& rm : root_moves) {
      rm.previous_score = rm.score;
    }

    for (pv_index = 0; pv_index < multi_pv && !thread_pool.stop; ++pv_index) {
      selective_depth = 0;

      // -------------------------
      // Aspiration window search
      // -------------------------

      // 探索が浅い時は窓のサイズを最大にして実質的にaspiration
      // windowsを無効にする
      // ある程度まで深くなると前回の探索の最小値と最大値より
      // それぞれ少し広げたくらいに窓を設定する
      if (root_depth >= 5) {
        // NOTE: winodwが有効になる閾値は要検討

        evaluation::Value previous_score = root_moves[pv_index].previous_score;

        delta =
            static_cast<evaluation::Value>(parameter::aspiration_search_delta);
        alpha = std::max(previous_score - delta, -evaluation::Value::INFINITE);
        beta = std::min(previous_score + delta, evaluation::Value::INFINITE);
      }
      // 小さな窓から始めてfail high/lowが起きないように窓を広げていく

      // fail highの回数
      // fail highの回数だけ探索depthを下げた方が強いらしい
      int failed_high_count = 0;
      while (true) {
        // fail highごとにdepthを下げる
        const Depth adjusted = std::max<Depth>(
            Depth::ONE,
            root_depth - failed_high_count);
        best_value = search::Search<NodeType::PV>(root_position, ss, alpha,
                                                  beta, adjusted, false, false);

        // 各指し手についてのスコアリングが終わったので並べ替える
        // 一つ目の指し手以外はValue::INFINITEが返る仕様なので、安定ソートでなければならない
        std::stable_sort(root_moves.begin() + pv_index, root_moves.end());

        if (thread_pool.stop) {
          break;
        }

        // fail low/highの場合に読み筋を出力
        if (main_thread && multi_pv == 1 && !limit.silent_mode &&
            limit.outout_fail_lh_pv &&
            (best_value <= alpha || best_value >= beta) &&
            utility::timer.elapsed() > 3000 &&
            CheckOutputInterval(main_thread, root_depth)) {
          // 1. 出力するのはmain threadのみ
          // 2. multi pvの場合は、常に別の指し手の場合の読み筋が
          //    表示されているので、fail low/highはあまり役に立たないと思われる
          // 3. オプションで出力するか設定
          // 4. 同上
          // 5. fail low/highになった
          // 6. 探索開始直後は読み筋が安定しないので最初の3秒は表示しない
          // 7. 将棋所のコンソールが詰まるのを防ぐために出力を抑制する
          //    一定時間が経過していたら出力する

          // 最後に出力した時刻を記録
          main_thread->last_pv_time = utility::timer.elapsed();

          utility::logger->info(
              "{}", usi::PV(root_position, root_depth, alpha, beta));
        }

        // aspiration windowの範囲外
        if (best_value <= alpha) {
          // ----------
          // fail low
          // ----------

          // betaを動かし過ぎると今度はfail highになるかもしれないので、
          // 少しalphaの方に近づける
          beta = (alpha + beta) / static_cast<evaluation::Value>(2);
          alpha = std::max(best_value - delta, -evaluation::Value::INFINITE);

          failed_high_count = 0;
          // fail lowなので、探索を終了するのは良くない
        } else if (best_value >= beta) {
          // ----------
          // fail high
          // ----------

          // alphaは動かさない方が良いらしい
          // cf. Simplify aspiration window :
          // https://github.com/official-stockfish/Stockfish/commit/a6ae2d3a31e93000e65bdfd8f0b6d9a3e6b8ce1b
          beta = std::min(best_value + delta, evaluation::Value::INFINITE);

          ++failed_high_count;
        } else {
          // 正確な値が得られたので、aspiration window searchを終了
          break;
        }

        delta += delta / static_cast<evaluation::Value>(4) +
                 static_cast<evaluation::Value>(5);

        ASSERT_LV3(alpha >= -evaluation::Value::INFINITE &&
                   beta <= evaluation::Value::INFINITE);
      }

      // multi pvの候補手を再度並び替える
      std::stable_sort(root_moves.begin(), root_moves.begin() + pv_index + 1);

      if (main_thread && !limit.silent_mode) {
        // 停止する場合でもPVを出力
        // 最低でもノード数は出力すべき

        // 反復深化のiterationを途中で打ち切る場合は、PVが中途半端になってしまう
        // stopの時に出力しないとpv_intervalが長い時に何も表示されないケースが発生する
        // 検討モードの場合のみstopの時にPVを出力しないことにする

        if (thread_pool.stop ||
            ((pv_index + 1 == multi_pv || utility::timer.elapsed() > 3000) &&
             CheckOutputInterval(main_thread, root_depth))) {
          // multi pvの指し手を全部探索したらその都度出力する
          if (!(thread_pool.stop && limit.consideration_mode)) {
            // 検討モードではstopの時にPVを出力しない
            main_thread->last_pv_time = utility::timer.elapsed();

            utility::logger->info(
                "{}", usi::PV(root_position, root_depth, alpha, beta));
          }
        }
      }
    }

    // 反復深化の1回分が終了したので、completed_depthを更新
    if (!thread_pool.stop) {
      completed_depth = root_depth;
    }

    if (root_moves[0].pv[0] != last_best_move) {
      last_best_move = root_moves[0].pv[0];
      last_best_move_depth = root_depth;
    }

    if (!main_thread) {
      continue;
    }

    // 詰みを見つけた時のいろいろ
    // multi pvの場合は詰みを見つけても探索を終了しない
    if (multi_pv == 1) {
      if (limit.mate) {
        // go mateで詰みを探索
        if (best_value >= evaluation::Value::WIN_IN_MAX_PLY &&
            enum_cast<>(evaluation::Value::MATE - best_value) <= limit.mate) {
          // 設定された手数以下の詰みを見つけた場合は探索を終了
          thread_pool.stop = true;
        }
      } else {
        // 将棋所の処理待ちで詰みを読み切っているのに時間切れになるケースがある
        // 読み切った詰みの手数の倍以上の深さを探索しても仕方がないので、探索を終了する
        if (best_value >= evaluation::Value::WIN_IN_MAX_PLY &&
            static_cast<int>(root_depth) >
                enum_cast<>(evaluation::Value::MATE - best_value) * 2) {
          // 詰ます場合
          break;
        } else if (best_value <= evaluation::Value::LOSE_IN_MAX_PLY &&
                   static_cast<int>(root_depth) >
                       enum_cast<>(evaluation::Value::MATE + best_value) * 2) {
          // 詰まされる場合
          break;
        }
      }
    }

    // ponder用に2手目を保存する
    // これが良いかはわからないが、ないよりはまし
    if (main_thread->root_moves[0].pv.size() >= 2) {
      main_thread->ponder_candidate = main_thread->root_moves[0].pv[1];
    }

    if (skill.enabled() && skill.IsTimeToPick(root_depth)) {
      skill.PickBest(multi_pv);
    }

    if (limit.enable_time_management()) {
      if (!thread_pool.stop && utility::timer.duration == 0) {
        // まだ停止条件を満たしていない

        const double falling_evaluation = std::clamp(
            (314.0 +
             9 * enum_cast<>(main_thread->previous_score - best_value)) /
                581.0,
            0.5, 1.5);

        // best moveが何回かのiterationを通じて変化していないなら多めに減らす
        time_reduction =
            last_best_move_depth + 10 < completed_depth ? 1.95 : 1.0;
        const double reduction =
            std::pow(main_thread->previous_time_reduction, 0.528) /
            time_reduction;

        for (Thread* thread : thread_pool) {
          total_best_move_changes += thread->best_move_changes;
          thread->best_move_changes = 0;
        }
        const double best_move_instability =
            1 + total_best_move_changes / thread_pool.size();

        // best_moveが何度も変わっているとunstable pv factorが大きくなる
        // fail
        // lowが起きていなかったり一つ前の反復深化から良くなっていたりすると
        // improving factorが小さくなる

        if (root_moves.size() == 1 ||
            utility::timer.elapsed() > utility::timer.optimum() *
                                           falling_evaluation * reduction *
                                           best_move_instability) {
          // 停止条件を満たした

          // 最小思考時間分は探索した方が得なので、
          // 探索は継続してキリの良い時間に成ったらCheckTimeで停止する

          if (main_thread->ponder) {
            utility::timer.duration = utility::timer.minimum();
          } else {
            // ponder hitなら、その時点からの経過時間
            // ponder hitしていないなら、やっぱりその時点からの経過時間
            // なので、いずれにしてもTimer.elapsed_from_ponder_hitでいい
            utility::timer.duration = std::max(
                utility::timer.RoundUp(utility::timer.elapsed_ponder_hit()),
                utility::timer.minimum());
          }
        }
      }
    }
  }

  if (!main_thread) {
    return;
  }

  main_thread->previous_time_reduction = time_reduction;

  if (skill.enabled()) {
    // 最善手とSkillで選ばれた指し手を入れ替える
    const auto tmp = skill.best != state_action::Move::NONE
                         ? skill.best
                         : skill.PickBest(multi_pv);
    // boost::findでは複数該当する
    std::swap(root_moves[0], *boost::range::find(root_moves, tmp));
  }
}

void SubordinateThread::Search() { SearchTree(); }

void MainThread::Search() {
  // 通常探索をしたかどうかのフラグ
  // trueなら今回の探索はスキップした
  bool search_skipped = true;

  // root nodeにおける自分の手番
  const Color us = root_position.side();

  // 検討モード用にPVを出力するか
  limit.consideration_mode = usi::options["ConsiderationMode"];
  // fail low/highの時にPVを出力するか
  limit.outout_fail_lh_pv = usi::options["OutputFailLHPV"];

  // 前回のPVを出力した時刻
  last_pv_time = 0;
  // PVの出力間隔(ms単位)
  // go infiniteでは毎回出力しないとShogiGUIなどで読み筋が表示されない
  limit.pv_interval = (limit.infinite || limit.consideration_mode)
                          ? 0
                          : static_cast<int>(usi::options["PvInterval"]);

  // ponder用の指し手を初期化
  // ponderの指し手がない時は、一つ前のiterationのbest moveで代用する
  ponder_candidate = state_action::Move::NONE;

  // 引き分けの値の設定
  const int contempt = static_cast<int>(usi::options["Contempt"] *
                                        evaluation::PieceScore::FU / 100);
  if (usi::options["ContemptFromBlack"]) {
    // 先手から見た値と解釈する
    evaluation::SetDrawValue(Color::BLACK, contempt);
  } else {
    // 現在の手番側から見た値とする
    evaluation::SetDrawValue(us, contempt);
  }

  // --------------------
  // performance teset
  // --------------------
  // そのうち対応するかも

  if (root_moves.empty()) {
    // 合法手がないので詰み
    root_moves.push_back(RootMove(state_action::Move::RESIGN));
    root_moves[0].score = evaluation::GetLoseValue(0);

    if (!limit.silent_mode) {
      utility::logger->info(
          "{}", usi::PV(root_position, Depth::ONE, -evaluation::Value::INFINITE,
                        evaluation::Value::INFINITE));
    }

    goto SKIP_SEARCH;
  }

  // --------------------
  // 定跡を検索
  // --------------------
  // TODO: 探索をスキップ

  // 宣言勝ちを判定
  //
  // 王手されていてもトライしながら王手を回避する場合もあるので、ここで判定する
  // Multi PVの時に1手詰めを見つけた後も探索を続けたいので、
  // 1手詰めはここでは扱わない
  if (auto best_move = root_position.Declarate();
      best_move != state_action::Move::NONE) {
    // root_movesにこの手が含まれているはず
    // 宣言勝ちが見つかった場合はMulti PVが機能しないが、実装難易度から諦める

    // トライルールの場合はroot_movesに含まれることを確認しないといけない
    // goコマンドで渡された指し手にない時はその手を指せない

    // 宣言勝ちの場合はgoコマンドを処理した後で、root_movesにMove::WINを追加したはず

    if (best_move != state_action::Move::WIN) {
      // トライルールの場合はMoveを32bitに変換する必要がある
      best_move = root_position.ConvertMove16ToMove(best_move);
    }

    // boost::findでは複数該当する
    auto it = boost::range::find(root_moves, best_move);
    if (it != root_moves.end()) {
      std::swap(root_moves[0], *it);
      // 1手詰めのスコア
      root_moves[0].score = evaluation::GetWinValue(2);

      if (!limit.silent_mode) {
        // rootでも宣言勝ちの場合はそのPVを出力
        utility::logger->info("{}", usi::PV(root_position, Depth::ONE,
                                            -evaluation::Value::INFINITE,
                                            evaluation::Value::INFINITE));
      }

      goto SKIP_SEARCH;
    }
  }

  // 探索時間の設定を更新
  utility::timer.Update(search::limit, us, root_position.game_ply());

  // 置換表の世代を更新
  // slaveが動き出す前に更新する必要がある
  // cf. Call TT.new_search() earlier.  :
  // https://github.com/official-stockfish/Stockfish/commit/ebc563059c5fc103ca6d79edb04bb6d5f182eaf5
  tt.UpdateGeneration();

  for (Thread* thread : thread_pool) {
    thread->best_move_changes = 0;
    if (thread != this) {
      // slaveを起動
      thread->StartSearch();
    }
  }

  // 自分自身も探索に入る
  SearchTree();
  search_skipped = false;

  // TODO: gotoの代わりに関数のreturnで対応したい
SKIP_SEARCH:;
  while (!thread_pool.stop && (ponder || limit.infinite)) {
    // ponderやgo
    // infiniteの場合に深さなどの探索制限に到達した場合にここが実行される

    // 計算は終了しているので、細かく待機する
    utility::Sleep(1);
  }
  thread_pool.stop = true;

  // 全てのスレッドが終了するのを待つ
  for (Thread* thread : thread_pool) {
    if (thread != this) {
      thread->WaitForSearchFinished();
    }
  }

  // --------------------
  // Lazy SMPの結果を取り出す
  // --------------------

  // pointerが指している先のデータを書き換えないconst
  Thread* best_thread = this;

  // 詰みの場合にroot_moves[0].pv[0]がMove::RESIGNの可能性がある
  // この時、master thread以外は指し手がなくて
  // root_moves[0]へのアクセスは違反になる
  // search_skippedの場合はmasterのroot_moves[0].pv[0]とpv[1]の指し手を
  // 出力すればいい
  if (usi::options["MultiPV"] == 1 && !limit.depth &&
      !Skill(static_cast<int>(usi::options["SkillLevel"])).enabled() &&
      !search_skipped) {
    // 深くまで探索できていてかつ評価値の方が優れているならば、
    // そのスレッドの差し手を採用する
    // 単純に深い探索のスレッドを採用してもいい気はするが、
    // 評価値が良い方が良い指し手を発見できた可能性があって、
    // 楽観合議の効果があるみたい

    std::unordered_map<state_action::Move, int64_t> votes;
    evaluation::Value min_score = root_moves[0].score;
    for (const Thread* thread : thread_pool) {
      min_score = std::min(min_score, thread->root_moves[0].score);
    }

    int64_t best_vote = 0;
    for (Thread* thread : thread_pool) {
      // slaveの中で最小となったスコアからの増分
      votes[thread->root_moves[0].pv[0]] +=
          (static_cast<int64_t>(thread->root_moves[0].score - min_score) + 14) *
          static_cast<int64_t>(thread->completed_depth.value);
      if (votes[thread->root_moves[0].pv[0]] > best_vote) {
        best_vote = votes[thread->root_moves[0].pv[0]];
        best_thread = thread;
      }
    }
  }

  // 次の探索のために値を保存
  previous_score = best_thread->root_moves[0].score;

  if (best_thread != this && !limit.silent_mode && !limit.consideration_mode) {
    // best_threadがmasterでないなら読み筋は出力されていない可能性が高い
    // ただし、iterationの途中で中途半端なPVの可能性があるので、検討モードでは出力しない
    utility::logger->info(
        "{}",
        usi::PV(best_thread->root_position, best_thread->completed_depth,
                -evaluation::Value::INFINITE, evaluation::Value::INFINITE));
  }

  // --------------------
  // GUIに指し手を返す
  // --------------------

  const auto resign_value = static_cast<int>(usi::options["ResignValue"]);
  if (const auto score = best_thread->root_moves[0].score;
      score != -evaluation::Value::INFINITE &&
      enum_cast<>(score) * 100 / evaluation::PieceScore::FU <= -resign_value) {
    best_thread->root_moves[0].pv[0] = state_action::Move::RESIGN;
  }

  // サイレントモードでないなら指し手を出力
  if (!limit.silent_mode) {
    // もしかすると他のスレッドが割り込むかも
    // ログの出力中の割り込みではなく、出力したい値が書き換わるかもしれない
    // 他の出力部分も同じ懸念がある

    if (auto& m = best_thread->root_moves[0];
        m.pv.size() > 1 ||
        m.ExtractPonderFromTT(root_position, ponder_candidate)) {
      utility::logger->info("bestmove {} ponder {}", m.pv[0], m.pv[1]);
    } else {
      utility::logger->info("bestmove {}", best_thread->root_moves[0].pv[0]);
    }
  }
}
}  // namespace search
