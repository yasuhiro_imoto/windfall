#pragma once
#ifndef PICK_STATE_H_INCLUDED
#define PICK_STATE_H_INCLUDED

namespace search {
enum class State : int {
  // ------------------------------
  // 王手がかかっていない通常探索
  // ------------------------------

  MAIN_TT,          //!< 置換表の指し手を返す
  CAPTURE_INITIAL,  //!< CAPTURESの指し手を生成する
  GOOD_CAPTURE,     //!< CAPTURES_PLUS_PROMOTIONSを生成して返す
  REFUTATION,       //!< killer move, counter moveを返す
  QUIET_INITIAL,    //!< QUIETの指し手を生成する
  QUIET,  //!< CAPTURES_PLUS_PROMOTIONSで生成しなかった指し手を生成して返す
          //!< SEEの低い値の指し手は後回し
  BAD_CAPTURE,  //!< SEE<0の捕獲する指し手

  // ------------------------------
  // 王手がかかっている静止探索/通常探索
  // ------------------------------

  EVASTION_TT,      //!< 置換表の指し手を返す
  EVASION_INITIAL,  //!< EVASTIONの指し手を生成する
  EVASION,          //!< EVASTIONの指し手を生成して返す

  // ------------------------------
  // 通常探索のProb Cut
  // ------------------------------

  PROBCUT_TT,       //!< 置換表の指し手を返す
  PROBCUT_INITIAL,  //!< PROBCUTの指し手を生成する
  PROBCUT,  //!< 直前の指し手の駒の価値を上回る駒を捕る指し手のみを生成して返す

  // ------------------------------
  // 静止探索
  // ------------------------------

  QUIET_SEARCH_TT,        //!< 置換表の指し手を返す
  QUIET_CAPTURE_INITIAL,  //!< QUIET_CAPTURESの指し手を生成する
  QUIET_CAPTURE,  //!< 捕獲する指し手と歩が成る指し手を返す
  QUIET_CHECK_INITIAL,  //!< 王手になる指し手を生成する
  QUIET_CHECK  //!< 王手になる指し手と歩が成る指し手を返す
};

//! 状態の更新を行う
constexpr State& operator+=(State& x, const bool y) {
  if (!y) {
    return x;
  }
  x = static_cast<State>(enum_cast<>(x) + 1);
  return x;
}

constexpr State& operator++(State& x) {
  x = static_cast<State>(enum_cast<>(x) + 1);
  return x;
}
}  // namespace search
#endif  // !PICK_STATE_H_INCLUDED
