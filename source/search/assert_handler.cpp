#include "stdafx.h"

#ifdef SEARCH_LOG
#include "../utility/logger.h"
#endif // SEARCH_LOG

namespace boost {
void assertion_failed_msg(const char* expr, const char* msg,
                          const char* function, const char* file, long line) {
  // アサーションに引っ掛かる頻度はかなり低いので、スレッドの競合は考えない
  // 必要ならば、spdlogの利用も考える
  std::ofstream ofs("log.txt", std::ios::app);
  ofs << "Expression : " << expr << '\n'
      << "Message : " << msg << '\n'
      << "Function : " << function << '\n'
      << "File : " << file << '\n'
      << "Line : " << line << std::endl;
  // abortはlevel assertの方に任せる

#ifdef SEARCH_LOG
  search::tree_logger->flush();
#endif // SEARCH_LOG
}
}  // namespace boost
