#pragma once
#ifndef ENGINE_QUIET_H_INCLUDED
#define ENGINE_QUIET_H_INCLUDED

#include "../evaluation/value.h"
#include "depth.h"

namespace state_action {
class Position;
}

namespace search {
struct Stack;

/**
 * @brief 探索しているノードの種類
 */
enum class NodeType { NonPV, PV };

/**
 * @brief 残りの探索深さがDepth::ONE未満になった時に利用する
 * @param position
 * @param ss
 * @param alpha
 * @param beta
 * @param depth
 * @return
 */
template <NodeType Type>
evaluation::Value QuietSearch(state_action::Position& position, Stack* const ss,
                              evaluation::Value alpha,
                              const evaluation::Value beta,
                              const Depth depth = Depth::ZERO);

template <NodeType Type>
evaluation::Value Search(state_action::Position& position, Stack* const ss,
                         evaluation::Value alpha,
                         evaluation::Value beta, const Depth depth,
                         const bool cut_node, const bool skip_early_pruning);
}  // namespace search
#endif  // !ENGINE_QUIET_H_INCLUDED
