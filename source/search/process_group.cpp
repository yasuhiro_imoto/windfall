// clang-format off
#include "stdafx.h"
// clang-format on

#include "process_group.h"
#include "../utility/level_assert.h"

// Windows環境下でのプロセッサグループの割当関係
#ifdef _WIN32
#if _WIN32_WINNT < 0x0601
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0601  // Force to include needed API prototypes
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>
// The needed Windows API for processor groups could be missed from old Windows
// versions, so instead of calling them directly (forcing the linker to resolve
// the calls at compile time), try to load them at runtime. To do this we need
// first to define the corresponding function pointers.
extern "C" {
typedef bool (*fun1_t)(LOGICAL_PROCESSOR_RELATIONSHIP,
                       PSYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX, PDWORD);
typedef bool (*fun2_t)(USHORT, PGROUP_AFFINITY);
typedef bool (*fun3_t)(HANDLE, CONST GROUP_AFFINITY *, PGROUP_AFFINITY);
}
#endif  // _WIN32

namespace search {
namespace windows {
/// best_group() retrieves logical processor information using Windows specific
/// API and returns the best group id for the thread with index idx. Original
/// code from Texel by Peter Österlund.
#ifdef _WIN32
int best_group(size_t idx) {
  int threads = 0;
  int nodes = 0;
  int cores = 0;
  DWORD returnLength = 0;
  DWORD byteOffset = 0;

  // Early exit if the needed API is not available at runtime
  HMODULE k32 = GetModuleHandle(L"Kernel32.dll");
  auto fun1 = (fun1_t)(void (*)())GetProcAddress(
      k32, "GetLogicalProcessorInformationEx");
  if (!fun1) return -1;

  // First call to get returnLength. We expect it to fail due to null buffer
  if (fun1(RelationAll, nullptr, &returnLength)) return -1;

  // Once we know returnLength, allocate the buffer
  SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX *buffer, *ptr;
  ptr = buffer =
      (SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX *)malloc(returnLength);

  // Second call, now we expect to succeed
  if (!fun1(RelationAll, buffer, &returnLength)) {
    free(buffer);
    return -1;
  }

  while (byteOffset < returnLength) {
    if (ptr->Relationship == RelationNumaNode)
      nodes++;

    else if (ptr->Relationship == RelationProcessorCore) {
      cores++;
      threads += (ptr->Processor.Flags == LTP_PC_SMT) ? 2 : 1;
    }

    ASSERT_LV3(ptr->Size);
    byteOffset += ptr->Size;
    ptr =
        (SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX *)(((char *)ptr) + ptr->Size);
  }

  free(buffer);

  std::vector<int> groups;

  // Run as many threads as possible on the same node until core limit is
  // reached, then move on filling the next node.
  for (int n = 0; n < nodes; n++)
    for (int i = 0; i < cores / nodes; i++) groups.push_back(n);

  // In case a core has more than one logical processor (we assume 2) and we
  // have still threads to allocate, then spread them evenly across available
  // nodes.
  for (int t = 0; t < threads - cores; t++) groups.push_back(t % nodes);

  // If we still have more threads than the total number of logical processors
  // then return -1 and let the OS to decide what to do.
  return idx < groups.size() ? groups[idx] : -1;
}
#endif	// _WIN32

void BindThisThread(const size_t index) {
#ifdef _WIN32
  // Use only local variables to be thread-safe
  int group = best_group(index);

  if (group == -1) return;

  // Early exit if the needed API are not available at runtime
  HMODULE k32 = GetModuleHandle(L"Kernel32.dll");
  auto fun2 =
      (fun2_t)(void (*)())GetProcAddress(k32, "GetNumaNodeProcessorMaskEx");
  auto fun3 = (fun3_t)(void (*)())GetProcAddress(k32, "SetThreadGroupAffinity");

  if (!fun2 || !fun3) return;

  GROUP_AFFINITY affinity;
  if (fun2(group, &affinity)) fun3(GetCurrentThread(), &affinity, nullptr);
#endif  // _WIN32
}
}  // namespace windows
}  // namespace search
