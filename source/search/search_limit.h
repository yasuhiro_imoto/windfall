#pragma once
#ifndef SEARCH_LIMIT_H_INCLUDED
#define SEARCH_LIMIT_H_INCLUDED

#include <array>
#include <cstdint>
#include <vector>

#include "../rule/color.h"
#include "../rule/entering_ou.h"
#include "../state_action/move.h"
#include "../utility/configuration.h"
#include "../utility/time_point.h"

namespace search {
//! goコマンドでの探索時に用いる持ち時間設定など
/*! "ponder"のフラグはここに含まれず、Threads.ponderにある
    LimitsType */
struct SearchLimit {
  SearchLimit()
      : time{utility::TimePoint(0), utility::TimePoint(0)},
        inc{utility::TimePoint(0), utility::TimePoint(0)},
        npm_sec(utility::TimePoint(0)),
        move_time(utility::TimePoint(0)),
        depth(0),
        mate(0),
        perft(0),
        infinite(0),
        nodes(0),
        byoyomi{utility::TimePoint(0), utility::TimePoint(0)},
        max_game_ply(100000),
        rtime(0),
        entering_ou_rule(EnteringOuRule::NONE),
        silent_mode(false),
        bench_mode(false),
        consideration_mode(false),
        outout_fail_lh_pv(false),
        pv_interval(0) {
#ifndef FOR_TOURNAMENT
    generate_all_legal_moves = false;
#endif  // !FOR_TOURNAMENT
  }

  /**
   * @brief 時間制御を行うか
   * @return
   * use_time_management
   */
  bool enable_time_management() const {
    return !(mate | move_time | depth | nodes | perft | infinite);
  }

  //! root(探索開始局面)で、探索する指し手集合
  /*! 特定の指し手を除外したいときにここから省く
      searchmoves */
  std::vector<state_action::Move> search_moves;

  /**
   * @brief 残り時間
   *
   * ms単位
   * time
   */
  std::array<utility::TimePoint, Color::SIZE> time;
  /**
   * @brief 1手ごとに増加する時間
   *
   * ms単位
   * inc
   */
  std::array<utility::TimePoint, Color::SIZE> inc;
  /**
   * @brief 探索ノード数を時間の代わりに使うかどうかのフラグ
   *
   * 将棋と相性が良くないようなので、基本的には使わない
   * npmsec
   */
  utility::TimePoint npm_sec;
  /**
   * @brief 0以外が指定してあるなら思考時間を固定
   *
   * ms単位
   * movetime
   */
  utility::TimePoint move_time;

  //! 0より大きいなら探索深さ固定
  /*! depth */
  int depth;
  //! 詰み専用探索(USIの'go mate'コマンドを使ったとき)
  /*! 詰み探索モードのときは、ここに詰みの手数を指定
      その手数以内の詰みが見つかったら探索を終了
      USIプロトコルでは、この値に詰将棋探索に使う時間[ms]を指定することになっている
      時間制限なしであれば、INT32_MAXが入っている
      mate */
  int mate;
  //! perft(performance test)中であるかのフラグ
  /*! 0より大きいなら探索深さ
      perft */
  int perft;
  //! 思考時間無制限かどうかのフラグ
  /*! non zeroなら無制限
      infinite */
  int infinite;

  //! 今回のgoコマンドでの探索ノード数
  /*! nodes */
  int64_t nodes;

  //! 秒読み(ms換算で)
  /*! byoyomi */
  std::array<utility::TimePoint, Color::SIZE> byoyomi;

  //! 引き分けとなる手数
  /*! 256なら256手目を指したあとに引き分け
      USIのoption["MaxMovesToDraw"]の値。引き分けなしなら100000
      note:
      残り手数を計算する時に桁あふれすると良くないのでINT_MAXにはしていない
      max_game_ply */
  int max_game_ply;

  //! "go rtime 100"とすると100～300msぐらい考える
  utility::TimePoint rtime;

  //! 画面に出力しないサイレントモード
  /*! PVを出力しない
      プロセス内での連続自己対戦のとき用
      siltent */
  bool silent_mode;

  //! 入玉ルール
  /*! enteringKingRule */
  EnteringOuRule entering_ou_rule;

  //! 検討モード用のPVを出力するか
  /*! consideration_mode */
  bool consideration_mode;

  //! fail low/highのときのPVを出力するか
  /*! outout_fail_lh_pv */
  bool outout_fail_lh_pv;

  //! ベンチマークモード
  /*! PVの出力時に置換表にアクセスしなくなる
      bench */
  bool bench_mode;

  //! PVの出力間隔
  /*! 探索のときにMainThread::search()内で初期化する
      pv_interval */
  utility::TimePoint pv_interval;

#if !defined(FOR_TOURNAMENT)
  //! 全合法手を生成するか
  /*! generate_all_legal_moves */
  bool generate_all_legal_moves;
#endif
};

extern SearchLimit limit;
}  // namespace search
#endif  // !SEARCH_LIMIT_H_INCLUDED
