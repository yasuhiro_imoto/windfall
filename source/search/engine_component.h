#pragma once
#ifndef ENGINE_COMPONENT_H_INCLUDED
#define ENGINE_COMPONENT_H_INCLUDED

#include <array>

#include "../evaluation/value.h"
#include "../rule/square.h"
#include "../state_action/move.h"
#include "depth.h"
#include "search_parameter.h"

namespace state_action {
class Position;
}

namespace search {
struct Stack;

/**
 * @brief PV Lineをコピーする
 *
 * 番兵として最後はMove::NONEが保証される
 * また、コピー元の最後もMove::NONE
 * @param pv
 * @param move 次の1手
 * @param child_pv 次の1手以降のPV
 */
void UpdatePV(state_action::Move* pv, const state_action::Move move,
              const state_action::Move* child_pv);

/**
 * @brief 1, 2, 4手前の指し手と現在の指し手の組に応じて更新する
 *
 * 1手前に対する現在の指し手: counter move
 * 2手前に対する現在の指し手: follow up move
 * 4手前に対する現在の指し手: follow up move
 * @param ss
 * @param piece
 * @param target
 * @param bonus
 * update_continuation_histories
 */
void UpdateContinuationHistory(Stack* const ss, const Piece piece,
                               const Square target, const int bonus);

/**
 * @brief 新しく駒を捕る指し手の最善手が見つかったときにmove sorting
 * heuristicsを更新する
 *
 * @param position
 * @param move
 * @param captures
 * @param capture_count
 * @param bonus
 * update_capture_stats
 */
void UpdateCaptureStatistics(const state_action::Position& position,
                             const state_action::Move move,
                             const state_action::Move* const captures,
                             const int capture_count, const int bonus);

/**
 * @brief 新しい最善手が見つかったときにmove sorting heuristicsを更新する
 *
 * 駒を捕らない指し手のstatistics, killerなどを更新する
 * @param position
 * @param ss
 * @param move 新しく見つかった良かった指し手
 * @param quiets 悪かった指し手
 * @param quiet_count 悪かった指し手の個数
 * @param bonus
 */
void UpdateQuietStatistics(const state_action::Position& position,
                           Stack* const ss, const state_action::Move move,
                           const state_action::Move* const quiets,
                           const int quiet_count, const int bonus);

//! razoringを行う深さの閾値
constexpr int razoring_depth = 3;
/**
 * @brief Razoringのdepthに応じたマージン
 *
 * 探索中はdepth >= Depth::ONEなのでインデックス0は使われない
 */
extern std::array<int, razoring_depth> razor_margin;

/**
 * @brief 残りの探索深さに応じたfutility margin
 * @param depth
 * @param improving
 * @return
 */
constexpr evaluation::Value GetFutilityMargin(const Depth depth,
                                              const bool improving) {
  return static_cast<evaluation::Value>(
      (parameter::futility_margin_alpha1 -
       parameter::futility_margin_alpha2 * improving) *
      static_cast<int>(depth));
}

/**
 * @brief 探索深さを減らすためのテーブル
 *
 * [pv_node][improving][残りの深さ][move_count]
 */
extern int reduction_table[2][2][64][64];

/**
 * @brief 残りの探索深さを減少させる量を返す
 *
 * @param improving
 * @param depth
 * @param move_count
 * @return
 * reduction
 */
template <bool PvNode>
Depth GetReductionDepth(const bool improving, const Depth depth,
                        const int move_count) {
  const auto d = std::min(static_cast<int>(depth), 63);
  const auto n = std::min(move_count, 63);
  return Depth(reduction_table[PvNode][improving][d][n]);
}

/**
 * @brief move countベースのfutility pruningで使う閾値
 *
 * この値よりmove_countが大きければ、futility pruningが実施される
 * 残りの探索深さが少なくて、王手がかかっていなくて、
 * 王手にもならないような指し手を枝刈りするために使う
 * @param improving 1手前の局面から評価値が上昇しているか
 * @param depth 残り探索depth
 * @return
 * futility_move_count
 */
constexpr int GetFutilityMoveCount(const bool improving,
                                   const Depth depth) noexcept {
  // Depthの2乗なので、スケールが1回分不足する
  return (5 + static_cast<int>(depth * depth) / enum_cast<>(Depth::ONE)) *
         (1 + improving) / 2;
}

/**
 * @brief 深さに依存したhistoryとstatisticsの更新のボーナス
 * @param depth
 * @return
 */
constexpr int GetStatisticsBonus(const Depth depth) noexcept {
  const int d = static_cast<int>(depth);
  // 32 * d * d + 64 * d - 64 = 32 * (d * d + 2 * d - 2)
  //                          = 32 * ((d + 1) * (d + 1) - 3)
  return d > 17 ? 0 : 32 * ((d + 1) * (d + 1) - 3);
}
}  // namespace search
#endif  // !ENGINE_COMPONENT_H_INCLUDED
