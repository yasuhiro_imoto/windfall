#include "stdafx.h"

#include "evaluation.h"
#include "search.h"
#include "state_action.h"
#include "usi.h"
#include "utility.h"

int main(const int argc, const char* argv[]) {
#if ASSERT_LV > 0
  FILE* stream;
  if (freopen_s(&stream, "error_log.txt", "w", stderr) != 0) {
    std::cout << "info string ERROR: something is worng.";
  }
#endif // ASSERT_LV > 0


  utility::InitializeLogger();
  usi::Initialization(usi::options);
  state_action::InitializeBitboards();
  state_action::Position::Initialize();
  search::Initialize();
  search::thread_pool.Set(usi::options["Threads"]);
  evaluation::Initialize();

  usi::Loop(argc, argv);

  utility::FinalizeLogger();

#if ASSERT_LV > 0
  fclose(stderr);
#endif // ASSERT_LV > 0
  return 0;
}
