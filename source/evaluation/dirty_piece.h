#pragma once
#ifndef DIRTY_PIECE_H_INCLUDED
#define DIRTY_PIECE_H_INCLUDED
#include <array>

#include "../rule/piece.h"
#include "bonanza_piece.h"

namespace evaluation {
#pragma warning(push)
#pragma warning(disable : 26495)
/**
 * @brief 評価値の差分計算で利用
 * 
 * 前の局面から変化した駒を管理
*/
struct DirtyPiece {
  //! その駒番号の駒が何から何に変わったのか
  std::array<ChangedBonaPiece, 2> changed_pieces;
  //! 変化した駒の番号
  std::array<PieceNumber, 2> piece_numbers;
  //! 変化した駒の枚数
  /*! null moveの場合は0になる
      最大は駒を捕る場合で2になる */
  int dirty_num;
};
#pragma warning(pop)
}  // namespace evaluation
#endif  // !DIRTY_PIECE_H_INCLUDED
