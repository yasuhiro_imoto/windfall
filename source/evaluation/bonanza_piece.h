#pragma once
#ifndef BONANZA_PIECE_H_INCLUDED
#define BONANZA_PIECE_H_INCLUDED
#include <cstdint>
#include <ostream>

namespace evaluation {
#ifdef NNUE_LITE
/**
 * @brief 駒の位置に対応したインデックス
 * 
 * 上位16bitは駒の種類(持ち駒と同じ、成りは下位16bitに含む)
 * 下位16bitは駒の位置、持ち駒の枚数、成り、自分の駒か相手の駒か
*/
enum BonaPiece : int32_t {
  //! 未初期化
  BONA_PIECE_UNINITIALIZED = -1,
  //! 無効な駒、駒落ちなどの不要な駒
  BONA_PIECE_ZERO = 0,

  F_FU = (1 << 16) + 1,
  F_TO = F_FU + 81,
  E_FU = F_TO + 81,
  E_TO = E_FU + 81,
  F_HAND_FU = E_TO + 81,
  E_HAND_FU = F_HAND_FU + 18,

  F_KY = (2 << 16) + 1,
  F_NY = F_KY + 81,
  E_KY = F_NY + 81,
  E_NY = E_KY + 81,
  F_HAND_KY = E_NY + 81,
  E_HAND_KY = F_HAND_KY + 18,

  F_KE = (3 << 16) + 1,
  F_NK = F_KE + 81,
  E_KE = F_NK + 81,
  E_NK = E_KE + 81,
  F_HAND_KE = E_NK + 81,
  E_HAND_KE = F_HAND_KE + 18,

  F_GI = (4 << 16) + 1,
  F_NG = F_GI + 81,
  E_GI = F_NG + 81,
  E_NG = E_GI + 81,
  F_HAND_GI = E_NG + 81,
  E_HAND_GI = F_HAND_GI + 18,

  F_KA = (5 << 16) + 1,
  F_UM = F_KA + 81,
  E_KA = F_UM + 81,
  E_UM = E_KA + 81,
  F_HAND_KA = E_UM + 81,
  E_HAND_KA = F_HAND_KA + 18,

  F_HI = (6 << 16) + 1,
  F_RY = F_HI + 81,
  E_HI = F_RY + 81,
  E_RY = E_HI + 81,
  F_HAND_HI = E_RY + 81,
  E_HAND_HI = F_HAND_HI + 18,

  F_KI = (7 << 16) + 1,
  E_KI = F_KI + 81 * 2,
  F_HAND_KI = E_KI + 81 * 2,
  E_HAND_KI = F_HAND_KI + 18,

  F_OU = (8 << 16) + 1,
  E_OU = F_OU + 81 * 2,

  // 持ち駒の先手側から見たBonaPieceをPieceNumberに変換するためのテーブルサイズ
  // 先頭から順に
  // +FU1, ..., +FU18, -FU1, ..., -FU18,
  // +KY1, ..., +KY4, +KE1, ..., +KE4, +GI1, ..., +GI4, +KA1, +KA2, +HI1, +HI2,
  // padding, padding,
  // -KY1, ..., -KY4, -KE1, ..., -KE4, -GI1, ..., -GI4, -KA1, -KA2, -HI1, -HI2,
  // padding, padding,
  // +KI1, ..., +KI4, padding, ..., padding, -KI1, ..., -KI4
  // で割り当てる
  // 香の後ろの空きスペースに他の駒を割り当ててスペースを節約する
  FE_HAND_END = 18 * 2 * 2 + 18 + 4,

  FE_END = 1 + 81 * 4 + 18 * 2
};
#else
/**
 * @brief 駒の種類と場所の組み合わせに対して一意なID
 * 
 * 駒の順序はYaneuraOuに準拠
 * 評価関数の互換性を保つためにはこの値を同じにしておくのが最も簡単
*/
enum BonaPiece : int32_t {
  //! 未初期化
  BONA_PIECE_UNINITIALIZED = -1,
  //! 無効な駒、駒落ちなどの不要な駒
  BONA_PIECE_ZERO = 0,

  // 持ち駒

  F_HAND_FU = BONA_PIECE_ZERO + 1,
  E_HAND_FU = F_HAND_FU + 19,

  F_HAND_KY = E_HAND_FU + 19,
  E_HAND_KY = F_HAND_KY + 5,

  F_HAND_KE = E_HAND_KY + 5,
  E_HAND_KE = F_HAND_KE + 5,

  F_HAND_GI = E_HAND_KE + 5,
  E_HAND_GI = F_HAND_GI + 5,

  F_HAND_KI = E_HAND_GI + 5,
  E_HAND_KI = F_HAND_KI + 5,

  F_HAND_KA = E_HAND_KI + 5,
  E_HAND_KA = F_HAND_KA + 3,

  F_HAND_HI = E_HAND_KA + 3,
  E_HAND_HI = F_HAND_HI + 3,

  FE_HAND_END = E_HAND_HI + 2,  // handで使う最後の値

  // 盤上の駒
  // 存在しない部分の値を詰めない
  // 縦型bitboardなので計算が難しい

  // handの最後の値と重なっているが、歩は11には存在しないので実用上は問題ない
  F_FU = FE_HAND_END,  
  E_FU = F_FU + 81,
  F_KY = E_FU + 81,
  E_KY = F_KY + 81,
  F_KE = E_KY + 81,
  E_KE = F_KE + 81,
  F_GI = E_KE + 81,
  E_GI = F_GI + 81,
  F_KI = E_GI + 81,
  E_KI = F_KI + 81,
  F_KA = E_KI + 81,
  E_KA = F_KA + 81,
  F_UM = E_KA + 81,
  E_UM = F_UM + 81,
  F_HI = E_UM + 81,
  E_HI = F_HI + 81,
  F_RY = E_HI + 81,
  E_RY = F_RY + 81,
  FE_END = E_RY + 81,
  F_OU = FE_END,
  E_OU = F_OU + 81,
  FE_END2 = E_OU + 81
};
#endif // NNUE_LITE

std::ostream& operator<<(std::ostream& os, const BonaPiece bp);

// BonaPieceを後手から見たとき(先手の39の歩を後手から見ると後手の71の歩)の番号のペア
union ExtendedBonaPiece {
  struct {
    BonaPiece fb;  // black
    BonaPiece fw;  // white
  };
  BonaPiece from[2];

  ExtendedBonaPiece() = default;
#pragma warning(push)
#pragma warning(disable : 26812)
  constexpr ExtendedBonaPiece(const BonaPiece b, const BonaPiece w)
      : fb(b), fw(w) {}
#pragma warning(pop)
};

inline std::ostream& operator<<(std::ostream& os, const ExtendedBonaPiece ebp) {
  // 片方だけを表示
  os << ebp.fb;
  return os;
}

//! 駒がどこからどこへ移動したかの情報
struct ChangedBonaPiece {
  ExtendedBonaPiece old_piece, new_piece;
};
}  // namespace evaluation

#endif  // !BONANZA_PIECE_H_INCLUDED
