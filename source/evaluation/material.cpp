// clang-format off
#include "stdafx.h"
// clang-format on

#include <array>

#include "material.h"

#include "../rule/piece.h"
#include "../state_action/position.h"
#include "../rule/color.h"
#include "../utility/enum_cast.h"

namespace evaluation {
std::array<int, Piece::SIZE> piece_value;
std::array<int, Piece::SIZE> captured_value_delta;
std::array<int, Piece::SIZE> promotion_value_delta;

Value InitializeMaterial(const state_action::Position& position) {
  int v = enum_cast<>(Value::ZERO);

  for (const auto square : SQUARE) {
    v += piece_value[position[square]];
  }

  for (const auto color : COLOR) {
    int tmp = 0;
    for (Piece piece = Piece::FU; piece < Piece::HAND_SIZE; ++piece) {
      tmp += position[color].count(piece) * piece_value[piece];
    }
    v += color == Color::BLACK ? tmp : -tmp;
  }

  return static_cast<Value>(v);
}

Value draw_value_table[enum_cast<>(RepetitionState::SIZE)][Color::SIZE] = {
    {Value::ZERO, Value::ZERO},           // RepetitionState::NONE
    {Value::MATE, Value::MATE},           // RepetitionState::WIN
    {-Value::MATE, -Value::MATE},         // RepetitionState::LOSE
    {Value::ZERO, Value::ZERO},           // RepetitionState::DRAW
    {Value::SUPERIOR, Value::SUPERIOR},   // RepetitionState::SUPERIOR
    {-Value::SUPERIOR, -Value::SUPERIOR}  // RepetitionState::INFERIOR
};

}  // namespace evaluation
