#pragma once
#ifndef VALUE_H_INCLUDED
#define VALUE_H_INCLUDED

#include <cstdint>
#include <type_traits>
#include <limits>

#include "../utility/enum_cast.h"
#include "../utility/configuration.h"
#include "../utility/level_assert.h"

// spdlogがどこかでwinbase.hを使っているようで、INFINITEが衝突する
#undef INFINITE

namespace evaluation {
/**
 * @brief 評価値
 *
 * 置換表に格納する都合で値は16bitに収める
*/
enum class Value : int32_t {
  ZERO = 0,
  //! rootが詰みの状態のスコア(0手詰め)
  /*! 例えば、3手詰めならこの値より3小さい */
  MATE = 32000,
  //! 取り得る最大値
  /*! 取り得る最小値は符号を反転させる */
  INFINITE = MATE + 1,
  //! 無効値
  NONE = INFINITE + 1,
  //! max_plyで詰むときのスコア
  MATE_IN_MAX_PLY = int(MATE) - max_ply,
  //! max_plyで詰まされる時のスコア
  MATED_IN_MAX_PLY = -int(MATE_IN_MAX_PLY),
  //! 何らかの方法によって勝ちが担保されているときのスコアの下限
  KNOWN_WIN = int(MATE_IN_MAX_PLY) - 1000,
  /**
   * @brief 巡回して盤上の駒は同じ配置になって 持ち駒は増えているときの点数
   *
   * ある程度は他の値と離しておく必要がある
  */
  SUPERIOR = 28000,
  //! 評価関数が返す値の最大値
  MAX = 27000,
  /**
   * @brief まだ評価関数が呼び出されていないときの特殊な値
  */
  NOT_EVALUATED = std::numeric_limits<int32_t>::max(),

  // ややこしいのでエイリアスを設定

  WIN_IN_MAX_PLY=MATE_IN_MAX_PLY,
  LOSE_IN_MAX_PLY=MATED_IN_MAX_PLY
};

constexpr Value operator-(const Value v) {
  return static_cast<Value>(-std::underlying_type_t<Value>(v));
}

constexpr Value operator+(const Value x, const Value y) {
  return static_cast<Value>(enum_cast<>(x) + enum_cast<>(y));
}
constexpr Value operator-(const Value x, const Value y) {
  return static_cast<Value>(enum_cast<>(x) - enum_cast<>(y));
}
constexpr Value operator*(const Value x, const Value y) {
  return static_cast<Value>(enum_cast<>(x) * enum_cast<>(y));
}
constexpr Value operator/(const Value x, const Value y) {
  return static_cast<Value>(enum_cast<>(x) / enum_cast<>(y));
}

constexpr Value abs(const Value v) {
  return v > Value::ZERO ? v : -v;
}

constexpr Value operator+(const Value x, const int y) noexcept {
  return x + static_cast<Value>(y);
}
constexpr Value operator-(const Value x, const int y) noexcept {
  return x - static_cast<Value>(y);
}
constexpr Value operator*(const Value x, const int y) noexcept {
  return x * static_cast<Value>(y);
}
constexpr Value operator/(const Value x, const int y) noexcept {
  return x / static_cast<Value>(y);
}

constexpr Value& operator+=(Value& lhs, const Value rhs) {
  lhs = lhs + rhs;
  return lhs;
}
constexpr Value& operator-=(Value& lhs, const Value rhs) {
  lhs = lhs - rhs;
  return lhs;
}

constexpr bool IsFinite(const Value v) noexcept {
  return v > -Value::INFINITE && v < Value::INFINITE;
}

/**
 * @brief 勝つ場合のスコア
 * @param ply ply手で詰ませる
 * @return 
 * mate_in
*/
constexpr Value GetWinValue(const int ply) {
  return Value::MATE - static_cast<Value>(ply);
}

/**
 * @brief 負ける場合のスコア
 * @param ply ply手で詰まされる
 * @return 
 * mated_in
*/
constexpr Value GetLoseValue(const int ply) {
  return -Value::MATE + static_cast<Value>(ply);
}

/**
 * @brief 詰みのスコアをあと何手で詰むかという形式に変換する
 *
 * 置換表に入れるスコアはこの形式に変換する必要がある
 * 詰みでない場合はそのままの値
 * @param v 
 * @param ply root nodeからの手数
 * @return 
*/
constexpr Value ConvertValueForTT(const Value v, const int ply) {
  ASSERT_LV3(-Value::INFINITE < v && v < Value::INFINITE);
  return v >= Value::WIN_IN_MAX_PLY ? v + ply
                                    : v <= Value::LOSE_IN_MAX_PLY ? v - ply : v;
}

/**
 * @brief 置換表から取り出した値を詰みのスコアに変換する
 *
 * 詰みでない場合はそのままの値
 * @param v
 * @param ply root nodeからの手数
 * @return
 */
constexpr Value ConvertValuueFromTT(const Value v, const int ply) {
  return v == Value::NONE ? Value::NONE
                          : v >= Value::WIN_IN_MAX_PLY
                                ? v - ply
                                : v <= Value::LOSE_IN_MAX_PLY ? v + ply : v;
}
}  // namespace evaluation
#endif  // !VALUE_H_INCLUDED

