#pragma once
#ifndef EVALUATOR_H_INCLUDED
#define EVALUATOR_H_INCLUDED

#include <cstdint>

#include "../utility/hash_key.h"
#include "../utility/configuration.h"

namespace state_action {
class Rollback;
class Position;
}

namespace evaluation {
enum class Value :int32_t;

/**
 * @brief 評価関数の軽量な初期化のみを行う
 *
 * 評価関数のパラメータの読み込みはisreadyに対する応答として行う
 * さもないとタイムアウトになる
 * init
*/
void Initialize();

/**
 * @brief 評価関数のパラメータを読み込む
 * 
 * 学習の時に利用する
 * @param path 
*/

/**
 * @brief 評価関数のパラメータを読み込む
 * 
 * 学習の時に利用する
 * @param path 
 * @return 
*/
bool LoadParameter(const std::string& path);

/**
 * @brief 評価関数のパラメータを読み込む
 * 
 * isreadyが来たら読み込む
 * load_eval
*/
void LoadParameter();

/**
 * @brief 評価関数のパラメータを保存する
*/
void SaveParameter();

/**
 * @brief 駒割りを計算する
 *
 * 先手から見た評価値
 * @param position 
 * @return 
 * material
*/
Value ComputeMaterial(const state_action::Position& position);

/**
 * @brief 評価関数の本体
 * @param position 
 * @return 
 * evaluate
*/
Value Evaluate(const state_action::Position& position);

/**
 * @brief 差分計算ができるように内部状態を更新する
 * @param position 
 * evaluate_with_no_return
*/
void Update(const state_action::Position& position);

/**
 * @brief 駒割り以外の全ての部分の合計を計算する
 * 
 * Position::Setから呼び出される
 * 差分計算ができない場合も呼び出される
 * @param position 
 * @return 
 * compute_eval
*/
Value ComputeEvaluation(const state_action::Position& position);

/**
 * @brief 評価値を計算する
 * 
 * 関数が多段になっているのはNNUE以外の評価関数を利用するための名残
 * @param position 
 * @param refresh 
 * @return 
 * ComputeScore
*/
Value ComputeScore(const state_action::Position& position, const bool refresh = false);

#if defined(USE_EVAL_HASH)
//!  EvalationHashのリサイズ
/*! EvalHash_Resize */
void ResizeEvaluationHash(const size_t size_mb);

//! EvalationHashをクリア
void ClearEvaluationHash();

/*! pretech_evalhash */
void PrefetchEvaluationHash(const Key key);
#endif // defined(USE_EVAL_HASH)

}
#endif // !EVALUATOR_H_INCLUDED

