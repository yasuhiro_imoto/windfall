#pragma once
#ifndef SCORE_HASH_H_INCLUDED
#define SCORE_HASH_H_INCLUDED

#include <immintrin.h>

#include "../utility/bit_operator.h"
#include "../utility/hash_key.h"
#include "../utility/tool.h"

namespace evaluation {
template <typename T>
struct HashTable {
  /**
   * @brief 大きさを変更
   *        MB単位で2の累乗になるよう切り捨てられる
   * @param size_mb 単位はMB
   */
  void resize(const size_t size_mb) {
    const size_t tmp = size_mb * 1024 * 1024 / sizeof(T);
    const size_t new_cluster_count = static_cast<size_t>(1) << MSB64(tmp);
    if (new_cluster_count != size_) {
      release();
      size_ = new_cluster_count;
      entries_ =
          reinterpret_cast<T*>(aligned_malloc(size_ * sizeof(T), alignof(T)));
    }
  }

  void release() {
    if (entries_) {
      aligned_free(entries_);
      entries_ = nullptr;
    }
  }
  ~HashTable() { release(); }

  T* operator[](const Key key) {
    return entries_ + (static_cast<size_t>(key) & (size_ - 1));
  }
  void clear() {
    utility::ClearTable("evaluation hash", entries_, size_ * sizeof(T));
  }

 private:
  size_t size_ = 0;
  T* entries_ = nullptr;
};

struct alignas(16) ScoreKeyValue {
  ScoreKeyValue() = default;
  ScoreKeyValue(const ScoreKeyValue& other) {
    static_assert(sizeof(ScoreKeyValue) == sizeof(__m128i), "");
    // 同じインスタンスは渡さない前提で、アドレスのチェックはしない

    _mm_store_si128(&score_key, other.score_key);
  }

  ScoreKeyValue& operator=(const ScoreKeyValue& other) {
    // 同じインスタンスは渡さない前提で、アドレスのチェックはしない

    _mm_store_si128(&score_key, other.score_key);
    return *this;
  }

  void encode() {
    // keyとscoreがatomicな操作でコピーされるので、keyが合っていればscoreも合っている
  }
  void decode() {
    // 逆変換だが同じこと
    encode();
  }

  union {
    struct {
      uint64_t key;
      uint64_t score;
    };
    __m128i score_key;
  };
};

using EvaluationHashTable = HashTable<ScoreKeyValue>;
extern EvaluationHashTable hash_table;
}  // namespace evaluation
#endif  // !SCORE_HASH_H_INCLUDED
