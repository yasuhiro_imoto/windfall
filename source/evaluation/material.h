#pragma once
#ifndef MATERIAL_H_INCLUDED
#define MATERIAL_H_INCLUDED

#include <array>
#include <cstdint>

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/repetition.h"
#include "../utility/enum_cast.h"
#include "../utility/level_assert.h"
#include "value.h"

namespace state_action {
class Position;
}

namespace evaluation {
/**
 * @brief 駒の価値
 *        先手はプラス、後手はマイナス
 */
extern std::array<int, Piece::SIZE> piece_value;

/**
 * @brief 駒を捕ったときの価値の変動量
 *
 * 盤面にいた時の価値と持ち駒になった価値の合計
 * MovePickerとSEEで使う
 * CapturePieceValue
 */
extern std::array<int, Piece::SIZE> captured_value_delta;

/**
 * @brief 駒が成ったときに増加する価値
 *
 * SEEで使う
 * 例えばPiece::FUとPiece::TOの両方にと金-歩の値が入っている
 * ProDiffPieceValue
 */
extern std::array<int, Piece::SIZE> promotion_value_delta;

/**
 * @brief 駒割りを計算
 *
 * 先手から見た評価値
 * 局面の初期化時に呼び出される
 * @param position
 */
Value InitializeMaterial(const state_action::Position& position);

/**
 * @brief 駒割り
 *        Apery(WCSC26)の値
 */
struct PieceScore {
  enum AperyWCSC26 {
    FU = 90,
    KY = 315,
    KE = 405,
    GI = 495,
    KI = 540,
    KA = 855,
    HI = 990,
    TO = 540,
    NY = 540,
    NK = 540,
    NG = 540,
    UM = 945,
    RY = 1395,
    OU = 15000
  };
};

/**
 * @brief 引き分けのスコア
 */
extern Value draw_value_table[enum_cast<>(RepetitionState::SIZE)][Color::SIZE];
inline Value GetDrawValue(const RepetitionState state, const Color color) {
  return draw_value_table[enum_cast<>(state)][color];
}

inline void SetDrawValue(const Color color, const int contempt) {
  ASSERT_LV3(color == Color::BLACK || color == Color::WHITE);

  const auto value_m = Value::ZERO - static_cast<Value>(contempt);
  const auto value_p = Value::ZERO + static_cast<Value>(contempt);

  static_assert(static_cast<int>(RepetitionState::DRAW) ==
                    enum_cast<>(RepetitionState::DRAW),
                "");
  // NOTE: 以前は問題なかったが、急になぜかenum_cast<>を使うと
  //       Releaseモードでのみで不正なアドレスにアクセスして
  //       クラッシュするようになった
  //       MSVCのバグと思われるが、良くわからない
  draw_value_table[static_cast<int>(RepetitionState::DRAW)][color] = value_m;
  draw_value_table[static_cast<int>(RepetitionState::DRAW)][~color] = value_p;
}

}  // namespace evaluation
#endif  // !MATERIAL_H_INCLUDED
