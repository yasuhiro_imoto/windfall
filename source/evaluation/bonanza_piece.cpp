#include "stdafx.h"

#include "bonanza_piece.h"

namespace evaluation {
std::ostream& operator<<(std::ostream& os, const BonaPiece bp) {
#ifdef NNUE_LITE
  const auto piece_type = bp >> 16;
  const auto n = ((bp - 1) & 0xFFFF) / 81;
  const auto m = ((bp - 1) & 0xFFFF) % 81;

  os << fmt::format("BonaPiece({},{},{})", piece_type, n, m);
#else
  os << fmt::format("BonaPiece({})", static_cast<int>(bp));
#endif  // NNUE_LITE

  return os;
}
}  // namespace evaluation