#pragma once
#ifndef EVALUATION_LIST_H_INCLUDED
#define EVALUATION_LIST_H_INCLUDED

#include <boost/range/algorithm/fill.hpp>

#include "../rule/piece.h"
#include "../rule/square.h"
#include "../utility/level_assert.h"
#include "bonanza_piece.h"
#include "kpp_index.h"

namespace state_action {
class Position;
}

namespace evaluation {
#pragma warning(push)
#pragma warning(disable : 26495)
/**
 * @brief どの駒(PieceNumber)がどこにあるのか(BonaPiece)を保持
 * 
 * 評価関数で用いる駒リスト
 * EvalList
*/
struct EvaluationList {
 public:
  //! 評価関数(FV38型)で用いる駒番号のリスト
  BonaPiece* piece_list_fb() const {
    return const_cast<BonaPiece*>(piece_list_fb_);
  }
  //! 評価関数(FV38型)で用いる駒番号のリスト
  BonaPiece* piece_list_fw() const {
    return const_cast<BonaPiece*>(piece_list_fw_);
  }

  //! 指定されたpiece_numberの駒をExtBonaPiece型に変換して返す
  ExtendedBonaPiece bona_piece(const PieceNumber piece_number) const {
    ExtendedBonaPiece bp;
    bp.fb = piece_list_fb_[piece_number];
    bp.fw = piece_list_fw_[piece_number];
    return bp;
  }

  /**
   * @brief 盤上のsqの升にpiece_noのpcの駒を配置する
   * @param piece_number 
   * @param square 
   * @param piece 
   * put_piece
  */
  void Update(const PieceNumber piece_number, const Square square,
              const Piece piece) {
    UpdateBoard(piece_number, BonaPiece(kpp_board_index[piece].fb + square),
                BonaPiece(kpp_board_index[piece].fw + square.inverse()),
                square);
  }
  /**
   * @brief color側の手駒piece_typeのi+1枚目の駒のPieceNumberを設定する
   * 
   * 1枚目の駒のPieceNumberを設定したいならi==0にして呼び出す
   * @param piece_number 
   * @param color 
   * @param piece_type 
   * @param i 
   * put_piece
  */
  void Update(const PieceNumber piece_number, const Color color,
              const Piece piece_type, const int i) {
    UpdateHand(piece_number,
               BonaPiece(kpp_hand_index[color][piece_type].fb + i),
               BonaPiece(kpp_hand_index[color][piece_type].fw + i));
  }

  /**
   * @brief BonaPieceに対応するPieceNumberを返す
   * @param bp 先手の側から見たBonaPiece
   * @return 
   * piece_no_of_hand
  */
  PieceNumber GetPieceNumberHand(const BonaPiece bp) const {
    return piece_number_list_hand[ConvertIndex(bp)];
  }
  /**
   * @brief 盤上の升sqに対応するPieceNumberを返す
   * @param square 
   * @return 
   * piece_no_of_board
  */
  PieceNumber GetPieceNumberBoard(const Square square) const {
    return piece_number_list_board[square];
  }

  /**
   * @brief piece_listを初期化する
   * 
   * 駒落ちに対応させる時のために、未使用の駒の値はBONA_PIECE_ZEROにしておく
   * 通常の評価関数を駒落ちの評価関数として流用できる
   * piece_no_listのほうはデバッグが捗るようにPIECE_NUMBER_NBで初期化
   * clear
  */
  void clear() {
#if defined(FOR_TOURNAMENT) || !defined(NNUE_LITE)
    // 駒落ちはないので、全て適切な値が設定される
    boost::fill(piece_list_fb_, BONA_PIECE_ZERO);
    boost::fill(piece_list_fw_, BONA_PIECE_ZERO);
#else
    // 駒落ちに対応
    for (int i = 0; i < PieceNumber::SIZE; ++i) {
      if (i < PieceNumber::KY) {
        // 歩
        piece_list_fb_[i] = static_cast<BonaPiece>(1 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(1 << 16);
      } else if (i < PieceNumber::KE) {
        // 香
        piece_list_fb_[i] = static_cast<BonaPiece>(2 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(2 << 16);
      } else if (i < PieceNumber::GI) {
        // 桂
        piece_list_fb_[i] = static_cast<BonaPiece>(3 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(3 << 16);
      } else if (i < PieceNumber::KA) {
        // 銀
        piece_list_fb_[i] = static_cast<BonaPiece>(4 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(4 << 16);
      } else if (i < PieceNumber::HI) {
        // 角
        piece_list_fb_[i] = static_cast<BonaPiece>(5 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(5 << 16);
      } else if (i < PieceNumber::KI) {
        // 飛
        piece_list_fb_[i] = static_cast<BonaPiece>(6 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(6 << 16);
      } else if (i < PieceNumber::OU) {
        // 金
        piece_list_fb_[i] = static_cast<BonaPiece>(7 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(7 << 16);
      } else {
        // 王
        piece_list_fb_[i] = static_cast<BonaPiece>(8 << 16);
        piece_list_fw_[i] = static_cast<BonaPiece>(8 << 16);
      }
    }
#endif // defined(FOR_TOURNAMENT) || !defined(NNUE_LITE)


    boost::fill(piece_number_list_board, PieceNumber::SIZE);
    boost::fill(piece_number_list_hand, PieceNumber::SIZE);
  }

  /**
   * @brief 内部で保持しているpieceListFb[]が正しいBonaPieceであるかを検査する
   * 
   * 注 : デバッグ用。遅い
   * @param position 
   * @return 
   * is_valid
  */
  bool IsValid(const state_action::Position& position) const;

 private:
  /**
   * @brief 盤上squareにあるpiece_numberの駒のBonaPieceがfb,fwであることを設定する
   * @param piece_number 
   * @param fb 
   * @param fw 
   * @param square 
   * set_piece_on_board
  */
  inline void UpdateBoard(const PieceNumber piece_number, const BonaPiece fb,
                          const BonaPiece fw, const Square square) {
    ASSERT_LV3(piece_number.ok());
    piece_list_fb_[piece_number] = fb;
    piece_list_fw_[piece_number] = fw;
    piece_number_list_board[square] = piece_number;
  }
  /**
   * @brief 手駒であるpiece_numberの駒のBonaPieceがfb,fwであることを設定する
   * @param piece_number 
   * @param fb 
   * @param fw 
   * set_piece_on_hand
  */
  inline void UpdateHand(const PieceNumber piece_number, const BonaPiece fb,
                         const BonaPiece fw) {
    ASSERT_LV3(piece_number.ok());
    piece_list_fb_[piece_number] = fb;
    piece_list_fw_[piece_number] = fw;
    piece_number_list_hand[ConvertIndex(fb)] = piece_number;
  }

 public:
  // 駒リスト
  // 駒番号(PieceNumber)いくつの駒がどこにあるのか(BonaPiece)を示す。
  // FV38などで用いる

  //! 駒リストの長さ
  int length() const { return PieceNumber::OU; }

  /**
   * @brief 駒リストの長さ
   * 
   * VPGATERDDを使うために4の倍数である必要がある
   * KPPT型評価関数などは、39,40番目の要素がゼロであることを
   * 前提としたアクセスをしている部分がある
   * MAX_LENGTH
  */
  static constexpr int max_length = 40;

 private:
#if defined(USE_AVX2)
  /*! pieceListFb */
  alignas(32) BonaPiece piece_list_fb_[max_length];
  /*! pieceListFw */
  alignas(32) BonaPiece piece_list_fw_[max_length];
#else
  BonaPiece piece_list_fb_[max_length];
  BonaPiece piece_list_fw_[max_length];
#endif  //  defined(USE_AVX2)

  /**
   * @brief 手駒である、任意のBonaPieceに対して、
   *        その駒番号(PieceNumber)を保持している配列
   * 
   * piece_no_list_hand
  */
  PieceNumber piece_number_list_hand[FE_HAND_END];
  /**
   * @brief 盤上の駒に対して、その駒番号(PieceNumber)を保持している配列
   * 
   * 玉がSQ_NBに移動しているとき用に+1まで保持しておくが、
   * SQ_NBの玉を移動させないので、この値を使うことはないはず
   * piece_no_list_board
  */
  PieceNumber piece_number_list_board[Square::SIZE_PLUS1];

#ifdef NNUE_LITE
  /**
   * @brief BonaPieceからPieceNumberを管理するための変換テーブル
   */
  static constexpr std::array<int, Piece::HAND_SIZE> offset = {
      0,
      81 * 4 + 1,                // FU
      81 * 4 + 1 - 18 * 2,       // KY
      81 * 4 + 1 - 18 * 2 - 4,   // KE
      81 * 4 + 1 - 18 * 2 - 8,   // GI
      81 * 4 + 1 - 18 * 2 - 12,  // KA
      81 * 4 + 1 - 18 * 2 - 14,  // HI
      81 * 4 + 1 - 18 * 2 * 2    // KI
  };

  /**
   * @brief 持ち駒のBonaPieceをテーブルのインデックスに変換する
   * @param bp 先手側から見たBonaPiece
   * @return
   */
  static constexpr int ConvertIndex(const BonaPiece bp) noexcept {
    return (bp & 0xFFFF) - offset[bp >> 16];
  }
#else
  static constexpr int ConvertIndex(const BonaPiece bp) { return bp; }
#endif  // NNUE_LITE
};
#pragma warning(pop)
}  // namespace evaluation
#endif  // !EVALUATION_LIST_H_INCLUDED
