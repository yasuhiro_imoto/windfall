#pragma once
#ifndef KPP_INDEX_HINCLUDED
#define KPP_INDEX_HINCLUDED

#include <array>

#include "../rule/color.h"
#include "../rule/piece.h"
#include "bonanza_piece.h"

namespace evaluation {
#ifdef NNUE_LITE
/*! kpp_board_index */
constexpr ExtendedBonaPiece kpp_board_index[Piece::SIZE] = {
  // 先手から見た場合
  {BONA_PIECE_ZERO, BONA_PIECE_ZERO},
  {F_FU, E_FU},
  {F_KY, E_KY},
  {F_KE, E_KE},
  {F_GI, E_GI},
  {F_KA, E_KA},
  {F_HI, E_HI},
  {F_KI, E_KI},
  {F_OU, E_OU},
  {F_TO, E_TO},  // 成歩
  {F_NY, E_NY},  // 成香
  {F_NK, E_NK},  // 成桂
  {F_NG, E_NG},  // 成銀
  {F_UM, E_UM},
  {F_RY, E_RY},
  {BONA_PIECE_ZERO, BONA_PIECE_ZERO},  // padding
  // 後手から見た場合
  {BONA_PIECE_ZERO, BONA_PIECE_ZERO},
  {E_FU, F_FU},
  {E_KY, F_KY},
  {E_KE, F_KE},
  {E_GI, F_GI},
  {E_KA, F_KA},
  {E_HI, F_HI},
  {E_KI, F_KI},
  {E_OU, F_OU},
  {E_TO, F_TO},  // 成歩
  {E_NY, F_NY},  // 成香
  {E_NK, F_NK},  // 成桂
  {E_NG, F_NG},  // 成銀
  {E_UM, F_UM},
  {E_RY, F_RY},
  {BONA_PIECE_ZERO, BONA_PIECE_ZERO}  // padding
};
#else
/*! kpp_board_index */
constexpr ExtendedBonaPiece kpp_board_index[Piece::SIZE] = {
    // 先手から見た場合
    {BONA_PIECE_ZERO, BONA_PIECE_ZERO},
    {F_FU, E_FU},
    {F_KY, E_KY},
    {F_KE, E_KE},
    {F_GI, E_GI},
    {F_KA, E_KA},
    {F_HI, E_HI},
    {F_KI, E_KI},
    {F_OU, E_OU},
    {F_KI, E_KI},  // 成歩
    {F_KI, E_KI},  // 成香
    {F_KI, E_KI},  // 成桂
    {F_KI, E_KI},  // 成銀
    {F_UM, E_UM},
    {F_RY, E_RY},
    {BONA_PIECE_ZERO, BONA_PIECE_ZERO},  // padding
    // 後手から見た場合
    {BONA_PIECE_ZERO, BONA_PIECE_ZERO},
    {E_FU, F_FU},
    {E_KY, F_KY},
    {E_KE, F_KE},
    {E_GI, F_GI},
    {E_KA, F_KA},
    {E_HI, F_HI},
    {E_KI, F_KI},
    {E_OU, F_OU},
    {E_KI, F_KI},  // 成歩
    {E_KI, F_KI},  // 成香
    {E_KI, F_KI},  // 成桂
    {E_KI, F_KI},  // 成銀
    {E_UM, F_UM},
    {E_RY, F_RY},
    {BONA_PIECE_ZERO, BONA_PIECE_ZERO}  // padding
};
#endif // NNUE_LITE

/*! kpp_hand_index */
constexpr ExtendedBonaPiece kpp_hand_index[Color::SIZE][Piece::OU] = {
    {{BONA_PIECE_ZERO, BONA_PIECE_ZERO},
     {F_HAND_FU, E_HAND_FU},
     {F_HAND_KY, E_HAND_KY},
     {F_HAND_KE, E_HAND_KE},
     {F_HAND_GI, E_HAND_GI},
     {F_HAND_KA, E_HAND_KA},
     {F_HAND_HI, E_HAND_HI},
     {F_HAND_KI, E_HAND_KI}},
    {{BONA_PIECE_ZERO, BONA_PIECE_ZERO},
     {E_HAND_FU, F_HAND_FU},
     {E_HAND_KY, F_HAND_KY},
     {E_HAND_KE, F_HAND_KE},
     {E_HAND_GI, F_HAND_GI},
     {E_HAND_KA, F_HAND_KA},
     {E_HAND_HI, F_HAND_HI},
     {E_HAND_KI, F_HAND_KI}}};

//! color側の持ち駒piece_raw_typeのインデックスを返す
constexpr BonaPiece GetSelfHandIndex(const Color color,
                                     const Piece piece_raw_type) {
  return kpp_hand_index[color][piece_raw_type].fb;
}
}  // namespace evaluation
#endif  // !KPP_INDEX_HINCLUDED
