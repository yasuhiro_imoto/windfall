#include "stdafx.h"

#include "evaluator.h"

#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "value.h"
#include "nn/accumulator.h"
#include "nn/nnue_common.h"
#include "nn/feature_embedding.h"
#include "nn/network.h"
#include "score_hash.h"
#include "../search/prefetch.h"
#include "../usi/global_option.h"
#include "../usi/option.h"
#include "../utility/logger.h"
#include "../utility/tool.h"

namespace evaluation {
EvaluationHashTable hash_table;

void Initialize() {
  // 評価関数のメモリを確保する
  // もしかするとUSIのやり取りがタイムアウトになるかも
  nn::Initialize();
}

namespace detail {
bool Load(const std::filesystem::path& path) {
  std::ifstream ifs(path, std::ios::binary);
  std::istreambuf_iterator<char> first(ifs), last;
  const std::string buffer(first, last);

  bool result = false;
  try {
    std::size_t offset = 0;
    msgpack::object_handle handle0 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);
    msgpack::object_handle handle1 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);
    msgpack::object_handle handle2 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);

    std::unordered_map<std::string, std::string> header;
    handle0->convert(header);
    // headerの内容を表示
    for (const auto& e : header) {
      utility::logger->info("info string {}: {}", e.first, e.second);
    }

    handle1->convert(*nn::feature_embedding);
    handle2->convert(*nn::network);

    result = true;
  } catch (msgpack::unpack_error&) {
    utility::logger->info("info string unpack failed");
  } catch (msgpack::type_error&) {
    utility::logger->info("info string type error");
  }

  return result;
}
}  // namespace detail

bool LoadParameter(const std::string& path) {
  const bool result = detail::Load(path);
  if (!result) {
    utility::logger->info("Error!: failed to read {}", path);
  }

  return result;
}

void LoadParameter() {
#if defined(EVAL_LEARN)
  if (usi::options["SkipLoadingEval"]) {
    return;
  }
#endif

  const auto full_dir = std::filesystem::current_path() /
                        static_cast<std::string>(usi::options["EvalDir"]);
  utility::logger->info("info string EvalDirectory = {}", full_dir.string());

  const auto path = full_dir / "nn.mpac";

  const bool result = detail::Load(path);
  if (!result) {
    utility::logger->info("Error!: failed to read {}", path.string());
    // 終了する
    utility::Exit();
  }
}

void SaveParameter() {
  const auto full_dir = std::filesystem::current_path() /
                        static_cast<std::string>(usi::options["EvalDir"]);
  if (!std::filesystem::exists(full_dir)) {
    // 出力ディレクトリを作成
    std::filesystem::create_directories(full_dir);
  }

  const auto path = full_dir / "nn.mpac";
  std::ofstream ofs(path, std::ios::binary);
  // 内容は自由
  const std::unordered_map<std::string, std::string> header = {
      {"author", "Yasuhiro Imoto"},
      {"version", "0.1a"},
      {"comment", "This is a dummy file."}};
  msgpack::pack(&ofs, header);

  msgpack::pack(&ofs, *nn::feature_embedding);
  msgpack::pack(&ofs, *nn::network);
}

Value Evaluate(const state_action::Position& position) {
  auto& accumulator = position.state()->accumulator;
  if (accumulator.computed_score) {
    return accumulator.score;
  }

#ifdef USE_GLOBAL_OPTIONS
  if (usi::global_options.use_evaluation_hash) {
    ASSERT_LV5(position.state()->material_value == ComputeMaterial(position));
    return ComputeScore(position);
  }
#endif  // USE_GLOBAL_OPTIONS

#ifdef USE_EVAL_HASH
  const Key key = position.state()->key();
  ScoreKeyValue entry = *hash_table[key];
  entry.decode();
  if (entry.key == key) {
    return static_cast<Value>(entry.score);
  }
#endif  // USE_EVAL_HASH

  const Value score = ComputeScore(position);
#ifdef USE_EVAL_HASH
  entry.key = key;
  entry.score = enum_cast<>(score);
  entry.encode();
  *hash_table[key] = entry;
#endif  // USE_EVAL_HASH

  return score;
}

void Update(const state_action::Position& position) {
  nn::feature_embedding->UpdateAccumulatorIfPossible(position);
}

Value ComputeEvaluation(const state_action::Position& position) {
  return ComputeScore(position, true);
}

Value ComputeScore(const state_action::Position& position, const bool refresh) {
  auto& accumulator = position.state()->accumulator;
  if (!refresh && accumulator.computed_score) {
    return accumulator.score;
  }

  alignas(nn::cache_line_bytes)
      nn::FeatureType embedded_features[nn::FeatureEmbedding::BufferSize];
#ifdef NNUE_VANILLA
  nn::feature_embedding->Transform(position, embedded_features, refresh);
#else
  alignas(nn::cache_line_bytes)
      uint32_t scale_buffer[nn::Network::scale_index + 1];
  nn::feature_embedding->Transform(position, embedded_features, scale_buffer,
                                   refresh);
#endif  // NNUE_VANILLA

  alignas(nn::cache_line_bytes) char output_buffer[nn::Network::buffer_size];

#ifdef NNUE_VANILLA
  const auto output = (*nn::network)(embedded_features, output_buffer);
  const auto score = std::clamp(static_cast<Value>(output[0] / nn::fv_scale),
                                -Value::MAX, Value::MAX);
#else
  const auto output =
    (*nn::network)(embedded_features, output_buffer, scale_buffer);

  // Value::MAXより小さくしておかないとaspiration searchでfail highになって
  // 探索が終わらなくなる
  // 対局時は秒が設定されているので、探索が打ち切られても一つ前のiterationの最善手が
  // 返るので見かけ上は問題がない
  // Value::MAXを超えるような値は終盤なので、勝敗には影響しない
  // ただし、教師データの作成時はスレッドを無駄にしてしまう
  const auto score = std::clamp(
    static_cast<Value>(output[0] / (1 << nn::Network::shift_scale_bits)),
    -Value::MAX, Value::MAX);
#endif // NNUE_VANILLA

  accumulator.score = score;
  accumulator.computed_score = true;
  return score;
}

#if defined(USE_EVAL_HASH)
void ResizeEvaluationHash(const size_t size_mb) { hash_table.resize(size_mb); }

void ClearEvaluationHash() { hash_table.clear(); }

void PrefetchEvaluationHash(const Key key) {
  constexpr uint64_t mask = ~(static_cast<uint64_t>(0x1F));
  // operator[]はアドレスを返す
  search::Prefetch(reinterpret_cast<void*>(
      reinterpret_cast<uint64_t>(hash_table[key]) & mask));
}
#endif  // defined(USE_EVAL_HASH)

}  // namespace evaluation
