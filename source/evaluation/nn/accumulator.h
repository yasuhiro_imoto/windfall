#pragma once
#ifndef ACCUMULATOR_H_INCLUDED
#define ACCUMULATOR_H_INCLUDED
#include <cstdint>

#include "../value.h"
#include "network_architecture.h"

namespace evaluation {
namespace nn {
#pragma warning(push)
#pragma warning(disable : 26495)
struct alignas(32) Accumulator {
  int16_t accumulation[2][refresh_triggers.size()][EmbeddingDimensions];
  Value score = Value::ZERO;
  bool computed_accumulation = false;
  bool computed_score = false;
};
#pragma warning(pop)
}  // namespace nn
}  // namespace evaluation
#endif  // !ACCUMULATOR_H_INCLUDED
