#pragma once
#ifndef NNUE_COMMON_H_INCLUDED
#define NNUE_COMMON_H_INCLUDED

#include <cstdint>
#include <type_traits>

namespace evaluation {
namespace nn {
//! 評価値の倍率
/*! todo: 後で調整する
    FV_SCALE */
constexpr int fv_scale = 16;
//! 活性化関数で桁数を調整する量
/*! todo: 後で調整する
    kWeightScaleBits */
constexpr int weight_scale_bits = 6;

//! embedding層でのパラメータの倍率
/*! 各層で倍率が累積するので定期的に打ち消す必要がある */
constexpr int embedding_scale_bits = 11;

// キャッシュラインの大きさ
constexpr size_t cache_line_bytes = 64;

// note: そのうちAVX512が来る
#if defined(USE_AVX2)
//! SIMD幅
/*! kSimdWidth */
constexpr size_t simd_bytes = 32;
#else
constexpr size_t simd_bytes = 16;
#endif  // defined(USE_AVX2)
/*! kMaxSimdWidth */
constexpr size_t max_simd_bytes = 32;

//! embedding層の出力の型
/*! TransformerFeatureType */
using FeatureType = uint8_t;
//! インデックスの型
/*! IndexType */
using IndexType = uint32_t;

//! n以上の最小のbaseの倍数
/*! CeilToMultiple */
template <typename T>
constexpr T CeilToMultiple(const T n, const T base) {
  static_assert(std::is_integral_v<T>, "");
  return (n + base - 1) / base * base;
}

//! Nが2の何乗かを計算
template<IndexType N>
struct Log2;

template <>
struct Log2<8> {
  static constexpr IndexType value = 3;
};
template <>
struct Log2<16> {
  static constexpr IndexType value = 4;
};
template <>
struct Log2<32> {
  static constexpr IndexType value = 5;
};
template <>
struct Log2<64> {
  static constexpr IndexType value = 6;
};
template <>
struct Log2<128> {
  static constexpr IndexType value = 7;
};
template <>
struct Log2<256> {
  static constexpr IndexType value = 8;
};
template <>
struct Log2<512> {
  static constexpr IndexType value = 9;
};
}  // namespace nn
}  // namespace evaluation
#endif  // !NNUE_COMMON_H_INCLUDED
