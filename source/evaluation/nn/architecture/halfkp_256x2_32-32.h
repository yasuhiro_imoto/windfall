#pragma once
#ifndef HALFKP_256x2_32_32_H_INCLUDED
#define HALFKP_256x2_32_32_H_INCLUDED

#include "../../utility/configuration.h"
#ifdef NNUE_VANILLA

#include "../feature/feature_set.h"
#include "../feature/half_kp.h"
#include "../layer/dense.h"
#include "../layer/input_slice.h"
#include "../layer/relu.h"
#include "../nnue_common.h"

namespace evaluation {
namespace nn {
//! 評価関数に用いる入力特徴量
using RawFeatures = feature::FeatureSet<feature::HalfKP<feature::Side::Friend>>;

//! embedding層の出力の次元数
constexpr IndexType EmbeddingDimensions = 256;

namespace layer {
using InputLayer = InputSliice<EmbeddingDimensions * 2>;
using HiddenLayer1 = ReLU<Dense<InputLayer, 32>>;
using HiddenLayer2 = ReLU<Dense<HiddenLayer1, 32>>;
using OutputLayer = Dense<HiddenLayer2, 1>;
}  // namespace layer

using Network = layer::OutputLayer;
}  // namespace nn
}  // namespace evaluation

#endif  // NNUE_VANILLA
#endif  // !HALFKP_256x2_32_32_H_INCLUDED
