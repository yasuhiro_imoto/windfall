#pragma once
#ifndef HALFKP_256x2_128_128_H_INCLUDED
#define HALFKP_256x2_128_128_H_INCLUDED

#include "../../utility/configuration.h"
#ifndef NNUE_VANILLA

#include "../feature/feature_set.h"
#include "../feature/half_kp.h"
#include "../layer/binarization.h"
#include "../layer/binary_dense.h"
#include "../layer/binary_inner_product.h"
#include "../layer/input_slice.h"
#include "../layer/leaky_relu.h"
#include "../nnue_common.h"

namespace evaluation {
namespace nn {
//! 評価関数に用いる入力特徴量
using RawFeatures = feature::FeatureSet<feature::HalfKP<feature::Side::Friend>>;

//! embedding層の出力の次元数
constexpr IndexType EmbeddingDimensions = 256;

namespace layer {
using InputLayer = InputSliice<EmbeddingDimensions * 2>;
using HiddenLayer1 = Binarization<BinaryDense<InputLayer, 128>>;
using HiddenLayer2 = LeakyReLU<BinaryDense<HiddenLayer1, 128>>;
using OutputLayer = BinaryInnerProduct<HiddenLayer2>;
}  // namespace layer

using Network = layer::OutputLayer;
}  // namespace nn
}  // namespace evaluation
#endif  // NNUE_VANILLA
#endif  // !HALFKP_256x2_128_128_H_INCLUDED
