#pragma once
#ifndef FEATURE_EMBEDDING_H_INCLUDED
#define FEATURE_EMBEDDING_H_INCLUDED

#include <array>
#include <cstring>
#include <type_traits>

#include <immintrin.h>

#pragma warning(push)
#pragma warning(disable : 26439 26451 26495 26812 28182)
#include <msgpack.hpp>
#pragma warning(pop)

#include "../../rule/color.h"
#include "../../state_action/position.h"
#include "../../utility/configuration.h"
#include "accumulator.h"
#include "feature/index_list.h"
#include "network_architecture.h"
#include "nnue_common.h"

namespace evaluation {
namespace nn {
#ifdef NNUE_VANILLA
/**
 * @brief 入力の特徴量を計算
 *
 * FeatureTransformer
 */
class FeatureEmbedding {
 private:
  /**
   * @brief 片方のプレイヤーの分の出力の次元数
   *
   * kHalfDimensions
   */
  static constexpr IndexType HalfDimensions = EmbeddingDimensions;

 public:
  /**
   * @brief 出力の型
   *
   * OutputType
   */
  using OutputType = FeatureType;

  /**
   * @brief 入力の次元数
   *
   * kInputDimensions
   */
  static constexpr IndexType InputDimensions = RawFeatures::dimensions;
  /**
   * @brief 出力の次元数
   *
   * kOutputDimensions
   */
  static constexpr IndexType OutputDimensions = HalfDimensions * 2;

  /**
   * @brief 順伝播用のバッファサイズ
   *
   * kBufferSize
   */
  static constexpr size_t BufferSize = OutputDimensions * sizeof(OutputType);

  /**
   * @brief 入力特徴量を変換する
   * @param position 
   * @param output 
   * @param refresh 
   * @return 
   * Transform
  */
  void Transform(const state_action::Position& position, OutputType* output,
                 const bool refresh) const noexcept {
    if (refresh || !UpdateAccumulatorIfPossible(position)) {
      RefreshAccumulator(position);
    }

    const auto& accumulation = position.state()->accumulator.accumulation;
    constexpr IndexType num_chunks = HalfDimensions / simd_bytes;
    constexpr int32_t control = _MM_SHUFFLE(3, 1, 2, 0);
    const __m256i zeros = _mm256_setzero_si256();

#ifdef NNUE_LITE
    // 駒の枚数のスケールをそろえる
    // 歩は18で割るのは難しいので、代わりに16で割る
    constexpr std::array<int, 8> shift = {4, 2, 2, 2, 1, 1, 2, 0};
#endif  // NNUE_LITE

    const std::array<Color, 2> perspectives = {position.side(),
                                               ~position.side()};
    for (IndexType p = 0; p < perspectives.size(); ++p) {
      const IndexType offset = HalfDimensions * p;
      auto out = reinterpret_cast<__m256i*>(&output[offset]);

      const auto data = accumulation[perspectives[p]];
      for (IndexType j = 0; j < num_chunks; ++j) {
        const auto p0 = reinterpret_cast<const __m256i*>(data[0]);
        __m256i sum0 = _mm256_load_si256(&p0[j * 2 + 0]);
        __m256i sum1 = _mm256_load_si256(&p0[j * 2 + 1]);
        for (IndexType i = 1; i < refresh_triggers.size(); ++i) {
          const auto pi = reinterpret_cast<const __m256i*>(data[i]);
          sum0 = _mm256_add_epi16(sum0, pi[j * 2 + 0]);
          sum1 = _mm256_add_epi16(sum1, pi[j * 2 + 1]);
        }
#ifdef NNUE_LITE
        // 駒の枚数のスケールをそろえる
        sum0 = _mm256_srai_epi16(sum0, shift[j]);
        sum1 = _mm256_srai_epi16(sum1, shift[j]);
#endif  // NNUE_LITE

        _mm256_store_si256(
            &out[j], _mm256_permute4x64_epi64(
                         _mm256_max_epi8(_mm256_packs_epi16(sum0, sum1), zeros),
                         control));
      }
    }
  }

  /**
   * @brief 可能なら差分計算を進める
   * @param position
   * @return
   * UpdateAccumulatorIfPossible
   */
  bool UpdateAccumulatorIfPossible(
      const state_action::Position& position) const noexcept {
    const auto current = position.state();
    if (current->accumulator.computed_accumulation) {
      return true;
    }

    const auto previous = current->previous;
    if (previous && previous->accumulator.computed_accumulation) {
      UpdateAccumulator(position);
      return true;
    }
    return false;
  }

 private:
  /**
   * @brief 差分計算を用いずに累積値を計算する
   * @param position
   * @return
   * RefreshAccumulator
   */
  void RefreshAccumulator(
      const state_action::Position& position) const noexcept {
    auto& accumulator = position.state()->accumulator;
    for (IndexType i = 0; i < refresh_triggers.size(); ++i) {
      std::array<feature::IndexList, 2> active_indices;
#pragma warning(push)
#pragma warning(disable : 28020)
      RawFeatures::AppendActiveIndices(position, refresh_triggers[i],
                                       active_indices);
#pragma warning(pop)

      for (const auto perspective : COLOR) {
        auto data = accumulator.accumulation[perspective][i];
        constexpr auto bias_size = HalfDimensions * sizeof(BiasType);
        if (i == 0) {
          std::memcpy(data, biases_, bias_size);
        } else {
          std::memset(data, 0, bias_size);
        }

        for (const auto index : active_indices[perspective]) {
#ifdef NNUE_LITE
          const auto piece_type = index >> 16;

          // 8は駒の種類
          constexpr int sections = HalfDimensions / 8;
          static_assert(sections * 8 == HalfDimensions, "");
          const IndexType offset = sections * (index & 0xFFFF);

          auto accumulation = reinterpret_cast<__m256i*>(data);
          auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);

          constexpr IndexType num_chunks =
              sections / (simd_bytes / sizeof(KernelType));
          for (IndexType j = 0; j < num_chunks; ++j) {
            const auto k = (piece_type - 1) * num_chunks + j;
            accumulation[k] = _mm256_add_epi16(accumulation[k], column[j]);
          }
#else
          const IndexType offset = HalfDimensions * index;
          auto accumulation = reinterpret_cast<__m256i*>(data);
          auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);
          constexpr IndexType num_chunks =
              HalfDimensions / (simd_bytes / sizeof(KernelType));
          for (IndexType j = 0; j < num_chunks; ++j) {
            accumulation[j] = _mm256_add_epi16(accumulation[j], column[j]);
          }
#endif  // NNUE_LITE
        }
      }
    }

    accumulator.computed_accumulation = true;
    accumulator.computed_score = false;
  }

  /**
   * @brief 差分計算を用いて累積値を計算する
   * @param position
   * @return
   * UpdateAccumulator
   */
  void UpdateAccumulator(
      const state_action::Position& position) const noexcept {
    const auto& previous = position.state()->previous->accumulator;
    auto& current = position.state()->accumulator;

    for (IndexType i = 0; i < refresh_triggers.size(); ++i) {
      std::array<feature::IndexList, 2> removed_indices, added_indices;
      std::array<bool, 2> reset;
#pragma warning(push)
#pragma warning(disable : 28020)
      RawFeatures::AppendChangedIndices(position, refresh_triggers[i],
                                        removed_indices, added_indices, reset);
#pragma warning(pop)
      for (const auto perspective : COLOR) {
#ifdef NNUE_LITE
        // 8は駒の種類
        constexpr int sections = HalfDimensions / 8;
        static_assert(sections * 8 == HalfDimensions, "");

        constexpr IndexType num_chunks =
            sections / (simd_bytes / sizeof(KernelType));
        auto accumulation = reinterpret_cast<__m256i*>(
            &current.accumulation[perspective][i][0]);

        constexpr auto bias_size = HalfDimensions * sizeof(BiasType);
        if (reset[perspective]) {
          if (i == 0) {
            std::memcpy(current.accumulation[perspective][i], biases_,
                        bias_size);
          } else {
            std::memset(current.accumulation[perspective][i], 0, bias_size);
          }
        } else {
          // 1から0に変化した特徴量に関する差分計算
          std::memcpy(current.accumulation[perspective][i],
                      previous.accumulation[perspective][i], bias_size);
          for (const auto index : removed_indices[perspective]) {
            const auto piece_type = index >> 16;

            const IndexType offset = sections * (index & 0xFFFF);
            auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);
            for (IndexType j = 0; j < num_chunks; ++j) {
              const auto k = (piece_type - 1) * num_chunks + j;
              accumulation[k] = _mm256_sub_epi16(accumulation[k], column[j]);
            }
          }
        }

        // 0から1に変化した特徴量に関する差分計算
        for (const auto index : added_indices[perspective]) {
          const auto piece_type = index >> 16;

          const IndexType offset = sections * (index & 0xFFFF);
          auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);
          for (IndexType j = 0; j < num_chunks; ++j) {
            const auto k = (piece_type - 1) * num_chunks + j;
            accumulation[k] = _mm256_add_epi16(accumulation[k], column[j]);
          }
        }
#else
        constexpr IndexType num_chunks =
            HalfDimensions / (simd_bytes / sizeof(KernelType));
        auto accumulation = reinterpret_cast<__m256i*>(
            &current.accumulation[perspective][i][0]);

        constexpr auto bias_size = HalfDimensions * sizeof(BiasType);
        if (reset[perspective]) {
          if (i == 0) {
            std::memcpy(current.accumulation[perspective][i], biases_,
                        bias_size);
          } else {
            std::memset(current.accumulation[perspective][i], 0, bias_size);
          }
        } else {
          // 1から0に変化した特徴量に関する差分計算
          std::memcpy(current.accumulation[perspective][i],
                      previous.accumulation[perspective][i], bias_size);
          for (const auto index : removed_indices[perspective]) {
            const IndexType offset = HalfDimensions * index;
            auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);
            for (IndexType j = 0; j < num_chunks; ++j) {
              accumulation[j] = _mm256_sub_epi16(accumulation[j], column[j]);
            }
          }
        }

        // 0から1に変化した特徴量に関する差分計算
        for (const auto index : added_indices[perspective]) {
          const IndexType offset = HalfDimensions * index;
          auto column = reinterpret_cast<const __m256i*>(&kernels_[offset]);
          for (IndexType j = 0; j < num_chunks; ++j) {
            accumulation[j] = _mm256_add_epi16(accumulation[j], column[j]);
          }
        }
#endif  // NNUE_LITE
      }
    }

    current.computed_accumulation = true;
    current.computed_score = false;
  }

  using BiasType = int16_t;
  using KernelType = int16_t;

  union {
    alignas(cache_line_bytes) BiasType biases_[HalfDimensions];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(cache_line_bytes) char biases_msgpack_[HalfDimensions *
                                                   sizeof(BiasType)];
  };
  union {
#ifdef NNUE_LITE
    alignas(cache_line_bytes)
        KernelType kernels_[HalfDimensions * InputDimensions / 8];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(cache_line_bytes) char kernels_msgpack_[HalfDimensions *
                                                    InputDimensions *
                                                    sizeof(KernelType) / 8];
#else
    alignas(cache_line_bytes)
        KernelType kernels_[HalfDimensions * InputDimensions];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(cache_line_bytes) char kernels_msgpack_[HalfDimensions *
                                                    InputDimensions *
                                                    sizeof(KernelType)];
#endif  // NNUE_LITE
  };

 public:
  MSGPACK_DEFINE_MAP(MSGPACK_NVP("bias", biases_msgpack_),
                     MSGPACK_NVP("kernel", kernels_msgpack_));
};
#else
//! 入力の特徴量を計算
/*! FeatureTransformer */
class FeatureEmbedding {
 private:
  //! 片方のプレイヤーの分の出力の次元数
  /*! kHalfDimensions */
  static constexpr IndexType HalfDimensions = EmbeddingDimensions;

 public:
  //! 出力の型
  /*! データは2値化するので型は実質的にない
      バイト数がわかりやすいように8bit型
      OutputType */
  using OutputType = uint8_t;
  //! スケールの型
  using ScaleType = uint32_t;

  //! 入力の次元数
  /*! kInputDimensions */
  static constexpr IndexType InputDimensions = RawFeatures::dimensions;
  //! 出力の次元数
  /*! kOutputDimensions */
  static constexpr IndexType OutputDimensions = HalfDimensions * 2;

  //! 順伝播用のバッファサイズ
  /*! データは2値化
      kBufferSize */
  static constexpr size_t BufferSize = OutputDimensions / 8;

  //! スケール用のバッファのインデックス
  /*! スケール用のバッファの割り当てに使う
      0から始まるインデックス */
  static constexpr IndexType scale_index = 0;

  //! 可能なら差分計算を進める
  /*! UpdateAccumulatorIfPossible */
  bool UpdateAccumulatorIfPossible(
      const state_action::Position& position) const {
    const auto current = position.state();
    if (current->accumulator.computed_accumulation) {
      return true;
    }

    const auto previous = current->previous;
    if (previous && previous->accumulator.computed_accumulation) {
      UpdateAccumulator(position);
      return true;
    }
    return false;
  }

  //! 入力特徴量を変換
  /*! Transform */
  void Transform(const state_action::Position& position,
                 FeatureType* output_buffer, ScaleType* scale_buffer,
                 bool refresh) const {
    if (refresh || !UpdateAccumulatorIfPossible(position)) {
      RefreshAccumulator(position);
    }

    constexpr IndexType chunks = HalfDimensions / simd_bytes;

    // スケール用の合計
    // 32bitのうちの下位11bit
    __m256i sum_lo = _mm256_setzero_si256();
    // スケール用の合計
    // sum_loの担当より上のbit
    __m256i sum_hi = _mm256_setzero_si256();
    // 上位5bitのマスク
    constexpr uint16_t mask_value = 0xF800;
    constexpr IndexType mask_size = embedding_scale_bits;
    static_assert((mask_value & ((1 << mask_size) - 1)) == 0, "");
    static_assert(mask_value == static_cast<uint16_t>(~((1 << mask_size) - 1)),
                  "");
    const __m256i sum_mask = _mm256_set1_epi16(mask_value);

    // 値をクリップ
    const __m256i max_value = _mm256_set1_epi16(1 << mask_size);

    const auto& accumulatiion = position.state()->accumulator.accumulation;
    const std::array<Color, 2> perspectives = {position.side(),
                                               ~position.side()};
    for (IndexType i = 0; i < 2; ++i) {
      const IndexType offset = HalfDimensions * i / 8;
#if defined(USE_AVX2)
      auto out = reinterpret_cast<int32_t*>(&output_buffer[offset]);

      const auto ptr = accumulatiion[perspectives[i]];
      __m256i mask = _mm256_set1_epi16(1);
      for (IndexType j = 0; j < chunks; ++j) {
        const auto tmp = reinterpret_cast<const __m256i*>(ptr[0]);
        __m256i v1 = _mm256_load_si256(&tmp[2 * j + 0]);
        __m256i v2 = _mm256_load_si256(&tmp[2 * j + 1]);
        for (IndexType k = 1; k < refresh_triggers.size(); ++k) {
          const auto tmp2 = reinterpret_cast<const __m256i*>(ptr[k]);
          v1 = _mm256_add_epi16(v1, _mm256_load_si256(&tmp2[2 * j + 0]));
          v2 = _mm256_add_epi16(v2, _mm256_load_si256(&tmp2[2 * j + 1]));
        }
        //
        // 2値化
        //
        // 64bitずつ v1の前半, v2の前半, v1の後半, v2の後半となる
        const __m256i packed = _mm256_packs_epi16(v1, v2);
        // 各int8_tの最上位ビットを集める
        // そのため正なら0、負なら1で2値化される
        out[i * chunks + j] = _mm256_movemask_epi8(packed);

        //
        // 入力ベクトルのスケールを計算
        //
        // 絶対値の合計
        const auto clipped1 = _mm256_min_epu16(_mm256_abs_epi16(v1), max_value);
        const auto clipped2 = _mm256_min_epu16(_mm256_abs_epi16(v2), max_value);
        sum_lo = _mm256_adds_epi16(sum_lo, clipped1);
        sum_lo = _mm256_adds_epi16(sum_lo, clipped2);
      }
      // オーバーフローしない
      static_assert((1 << mask_size) * chunks * 2 + ((1 << mask_size) - 1) <
                        std::numeric_limits<uint16_t>::max(),
                    "");
      sum_hi = _mm256_adds_epi16(sum_hi, _mm256_srai_epi16(sum_lo, mask_size));
      sum_lo = _mm256_andnot_si256(sum_mask, sum_lo);
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
    }
    //
    // 水平方向に合計
    //
    // loの前半とhiの前半
    const __m256i a = _mm256_permute2x128_si256(sum_lo, sum_hi, 0x20);
    // loの後半とhiの後半
    const __m256i b = _mm256_permute2x128_si256(sum_lo, sum_hi, 0x31);
    const __m256i c = _mm256_adds_epi16(a, b);  // 16bitの値が16個

    // clang-format off
    const __m256i order =
      _mm256_set_epi8(0xF, 0x7, 0xD, 0x5, 0xB, 0x3, 0x9, 0x1,
        0xE, 0x6, 0xC, 0x4, 0xA, 0x2, 0x8, 0x0,
        0xF, 0x7, 0xD, 0x5, 0xB, 0x3, 0x9, 0x1,
        0xE, 0x6, 0xC, 0x4, 0xA, 0x2, 0x8, 0x0);
    // clang-format on
    // 64bitずつでloの下位8bit、loの上位8bit、hiの下位8bit、hiの上位8bitに並び替えた
    const __m256i shuffled1 = _mm256_shuffle_epi8(c, order);

    // 64bitの範囲ずつ水平方向に合計
    const __m256i zeros = _mm256_setzero_si256();
    const __m256i s = _mm256_sad_epu8(shuffled1, zeros);
    // 32bitの範囲で正しい位置にそれぞれシフト
    // loの11bitより上をhiに含めた
    const __m256i shift = _mm256_set_epi64x(mask_size + 8, mask_size, 8, 0);
    // t0, _, t2, _, t4, _ t6, _
    const __m256i t = _mm256_sllv_epi64(s, shift);

    // t2, _, t0, _, t6, _, t4, _
    const __m256i shuffled2 = _mm256_shuffle_epi32(t, _MM_SHUFFLE(1, 0, 3, 2));
    // t0+t2, _, _, _, t4+t6, _, _, _
    const __m256i u = _mm256_add_epi64(shuffled2, t);

    const __m128i hi = _mm256_extractf128_si256(u, 1);
    const __m128i lo = _mm256_extractf128_si256(u, 0);

    // それぞれから下位32bitを取り出す
    scale_buffer[scale_index] = _mm_cvtsi128_si32(lo) + _mm_cvtsi128_si32(hi);
  }

 private:
  //! 差分計算を用いずに累積値を計算する
  /*! RefreshAccumulator */
  void RefreshAccumulator(const state_action::Position& position) const {
    auto& accumulator = position.state()->accumulator;
    for (IndexType i = 0; i < refresh_triggers.size(); ++i) {
      feature::IndexList active_indices[2];
      RawFeatures::AppendActiveIndices(position, refresh_triggers[i],
                                       active_indices);
      for (const auto perspective : COLOR) {
        if (i == 0) {
          std::memcpy(accumulator.accumulation[perspective][i], biases_,
                      HalfDimensions * sizeof(BiasType));
        } else {
          std::memset(accumulator.accumulation[perspective][i], 0,
                      HalfDimensions * sizeof(BiasType));
        }

        for (const auto index : active_indices[perspective]) {
          const IndexType offset = HalfDimensions * index;
#if defined(USE_AVX2)
          auto accumulation = reinterpret_cast<__m256i*>(
              &accumulator.accumulation[perspective][i][0]);
          const auto column =
              reinterpret_cast<const __m256i*>(&kernels_[offset]);
          constexpr IndexType chunks =
              HalfDimensions / (simd_bytes / sizeof(FeatureType));
          for (IndexType j = 0; j < chunks; ++j) {
            accumulation[j] = _mm256_add_epi16(accumulation[j], column[j]);
          }
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
        }
      }
    }
    accumulator.computed_accumulation = true;
    accumulator.computed_score = false;
  }

  //! 差分計算を用いて累積値を計算
  /*! UpdateAccumulator */
  void UpdateAccumulator(const state_action::Position& position) const {
    const auto previous_accumulator = position.state()->previous->accumulator;
    auto& accumulator = position.state()->accumulator;

    for (IndexType i = 0; i < refresh_triggers.size(); ++i) {
      feature::IndexList removed_indices[2], added_indices[2];
      bool reset[2];
      RawFeatures::AppendChangedIndices(position, refresh_triggers[i],
                                        removed_indices, added_indices, reset);

      for (const auto perspective : COLOR) {
#if defined(USE_AVX2)
        constexpr IndexType chuncks =
            HalfDimensions / (simd_bytes / sizeof(FeatureType));
        auto accumulation = reinterpret_cast<__m256i*>(
            &accumulator.accumulation[perspective][i][0]);
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
        if (reset[perspective]) {
          // todo: 個々の処理はintelのどれかのライブラリが使えそう
          if (i == 0) {
            std::memcpy(accumulator.accumulation[perspective][i], biases_,
                        HalfDimensions * sizeof(BiasType));
          } else {
            std::memset(accumulator.accumulation[perspective][i], 0,
                        HalfDimensions * sizeof(BiasType));
          }
        } else {
          // 1から0に変化した特徴量について差分計算
          std::memcpy(accumulator.accumulation[perspective][i],
                      previous_accumulator.accumulation[perspective][i],
                      HalfDimensions * sizeof(BiasType));
          for (const auto index : removed_indices[perspective]) {
            const IndexType offset = HalfDimensions * index;
#if defined(USE_AVX2)
            const auto column =
                reinterpret_cast<const __m256i*>(&kernels_[offset]);
            for (IndexType j = 0; j < chuncks; ++j) {
              accumulation[j] = _mm256_sub_epi16(accumulation[j], column[j]);
            }
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
          }
        }
        // 0から1に変化した特徴量について差分計算
        for (const auto index : added_indices[perspective]) {
          const IndexType offset = HalfDimensions * index;
#if defined(USE_AVX2)
          const auto column =
              reinterpret_cast<const __m256i*>(&kernels_[offset]);
          for (IndexType j = 0; j < chuncks; ++j) {
            accumulation[j] = _mm256_add_epi16(accumulation[j], column[j]);
          }
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
        }
      }
    }

    accumulator.computed_accumulation = true;
    accumulator.computed_score = false;
  }

  using BiasType = int16_t;
  using KernelType = int16_t;
  // using ScaleType = int16_t;

  alignas(cache_line_bytes) BiasType biases_[HalfDimensions];
  alignas(cache_line_bytes)
      KernelType kernels_[HalfDimensions * InputDimensions];
};
#endif  // NNUE_VANILLA
}  // namespace nn
}  // namespace evaluation
#endif  // !FEATURE_EMBEDDING_H_INCLUDED
