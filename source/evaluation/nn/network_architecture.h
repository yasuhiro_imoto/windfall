#pragma once
#ifndef NETWORK_ARCHITECTURE_H_INCLUDED
#define NETWORK_ARCHITECTURE_H_INCLUDED

#include <type_traits>

#include "../../utility/configuration.h"
#ifdef NNUE_VANILLA
#include "architecture/halfkp_256x2_32-32.h"
#else
#include "architecture/halfkp_256x2-128-128.h"
#endif // NNUE_VANILLA
#include "nnue_common.h"

namespace evaluation {
namespace nn {
static_assert(EmbeddingDimensions % max_simd_bytes == 0, "");
static_assert(Network::OutputDimensions == 1, "");
static_assert(std::is_same_v<Network::OutputType, int32_t>, "");

/**
 * @brief 差分計算の代わりに全計算を行うタイミングのリスト
 *
 * kRefreshTriggers
 */
constexpr auto refresh_triggers = RawFeatures::refresh_triggers;
}  // namespace nn
}  // namespace evaluation
#endif  // !NETWORK_ARCHITECTURE_H_INCLUDED
