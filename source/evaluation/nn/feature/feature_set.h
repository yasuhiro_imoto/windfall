#pragma once
#ifndef FEATURE_SET_H_INCLUDED
#define FEATURE_SET_H_INCLUDED

#include <array>
#include <cstdint>

#include "../../../rule/color.h"
#include "../../../rule/piece.h"
#include "../../../utility/enum_cast.h"
#include "feature_common.h"
#include "half_kp.h"

namespace state_action {
class Position;
}

namespace evaluation {
namespace nn {
namespace feature {
//! 値のリストクラス
template <typename T, T... Values>
struct CompileTimeList;
template <typename T, T First, T... Remaining>
struct CompileTimeList<T, First, Remaining...> {
  static constexpr bool Contains(const T value) {
    return value == First || CompileTimeList<T, Remaining...>::Contains(value);
  }
  static constexpr std::array<T, sizeof...(Remaining) + 1> values = {
      {First, Remaining...}};
};

template <typename T, T First, T... Remaining>
constexpr std::array<T, sizeof...(Remaining) + 1>
    CompileTimeList<T, First, Remaining...>::values;

template <typename T>
struct CompileTimeList<T> {
  static constexpr bool Contains(const T /*value*/) { return false; }
  static constexpr std::array<T, 0> values = {{}};
};

//! リストの先頭への追加を行う
template <typename T, typename ListType, T Value>
struct AppendToList;
template <typename T, T... Values, T AnotherValue>
struct AppendToList<T, CompileTimeList<T, Values...>, AnotherValue> {
  using Result = CompileTimeList<T, AnotherValue, Values...>;
};

//! ソートされた重複のないリストへの追加を行う
template <typename T, typename ListType, T Value>
struct InsertToSet;
template <typename T, T First, T... Remaining, T AnotherValue>
struct InsertToSet<T, CompileTimeList<T, First, Remaining...>, AnotherValue> {
  using Result = std::conditional_t<
      CompileTimeList<T, First, Remaining...>::Contains(AnotherValue),
      CompileTimeList<T, First, Remaining...>,
      std::conditional_t<
          (AnotherValue < First),
          CompileTimeList<T, AnotherValue, First, Remaining...>,
          typename AppendToList<
              T,
              typename InsertToSet<T, CompileTimeList<T, Remaining...>,
                                   AnotherValue>::Result,
              First>::Result>>;
};
template <typename T, T Value>
struct InsertToSet<T, CompileTimeList<T>, Value> {
  using Result = CompileTimeList<T, Value>;
};

//! 特徴量セットの基底クラス
template <typename Derived>
class FeatureSetBase {
 public:
  //! 特徴量のうち、値が1であるインデックスのリストを取得する
  template <typename IndexListType>
  static void AppendActiveIndices(const state_action::Position& position,
                                  const TriggerEvent trigger,
                                  std::array<IndexListType, 2>& active) {
    for (const auto perspective : COLOR) {
      Derived::CollectActiveIndices(position, trigger, perspective,
                                    &active[perspective]);
    }
  }

  //! 特徴量のうち、一手前から値が変化したインデックスのリストを取得する
  template <typename PositionType, typename IndexListType>
  static void AppendChangedIndices(const PositionType& position,
                                   const TriggerEvent trigger,
                                   std::array<IndexListType, 2>& removed,
                                   std::array<IndexListType, 2>& added,
                                   std::array<bool, 2>& reset) {
    const auto& dp = position.state()->dirty_piece;
    if (dp.dirty_num == 0) {
      return;
    }

    for (const auto perspective : COLOR) {
      reset[perspective] = false;
      switch (trigger) {
        case TriggerEvent::None:
          break;
        case TriggerEvent::FriendOuMoved:
          reset[perspective] =
              dp.piece_numbers[0] == PieceNumber::OU + perspective;
          break;
        case TriggerEvent::EnemyOuMoved:
          reset[perspective] =
              dp.piece_numbers[0] == PieceNumber::OU + ~perspective;
          break;
        case TriggerEvent::AnyOuMoved:
          reset[perspective] =
              dp.piece_numbers[0] >= PieceNumber::OU;
          break;
        case TriggerEvent::AnyPieceMoved:
          reset[perspective] = true;
          break;
        default:
          ASSERT_LV5(false);
          break;
      }
      if (reset[perspective]) {
        Derived::CollectActiveIndices(position, trigger, perspective,
                                      &added[perspective]);
      } else {
        Derived::CollectChangedIndices(position, trigger, perspective,
                                       &removed[perspective],
                                       &added[perspective]);
      }
    }
  }
};

//! 特徴量セットを表す
/*! 実行時の計算量を線形にするために、内部の処理はテンプレート引数の逆順に行う
 */
 template <typename FirstFeatureType, typename... RemainingFeatureTypes>
 class FeatureSet<FirstFeatureType, RemainingFeatureTypes...>
    : public FeatureSetBase<
          FeatureSet<FirstFeatureType, RemainingFeatureTypes...>> {
 private:
  using Head = FirstFeatureType;
  using Tail = FeatureSet<RemainingFeatureTypes...>;

 public:
  // 評価関数ファイルに埋め込むハッシュ値
  static constexpr uint32_t hash_value =
      Head::hash_value ^ (Tail::hash_value << 1) ^ (Tail::hash_value >> 31);
  // 特徴量の次元数
  static constexpr IndexType dimensions = Head::dimensions + Tail::dimensions;
  // 特徴量のうち、同時に値が1となるインデックスの数の最大値
  static constexpr IndexType max_active_dimensions =
      Head::max_active_dimensions + Tail::max_active_dimensions;
  // 差分計算の代わりに全計算を行うタイミングのリスト
  using SortedTriggerSet =
      typename InsertToSet<TriggerEvent, typename Tail::SortedTriggerSet,
                           Head::refresh_trigger>::Result;
  static constexpr auto refresh_triggers = SortedTriggerSet::values;

  // 特徴量名を取得する
  static std::string GetName() {
    return std::string(Head::name) + "+" + Tail::GetName();
  }

 private:
  // 特徴量のうち、値が1であるインデックスのリストを取得する
  template <typename IndexListType>
  static void CollectActiveIndices(const state_action::Position& position,
                                   const TriggerEvent trigger,
                                   const Color perspective,
                                   IndexListType* const active) {
    Tail::CollectActiveIndices(position, trigger, perspective, active);
    if (Head::refresh_trigger == trigger) {
      const auto start = active->size();
      Head::AppendActiveIndices(position, perspective, active);
      for (auto i = start; i < active->size(); ++i) {
        (*active)[i] += Tail::dimensions;
      }
    }
  }

  // 特徴量のうち、一手前から値が変化したインデックスのリストを取得する
  template <typename IndexListType>
  static void CollectChangedIndices(const state_action::Position& position,
                                    const TriggerEvent trigger,
                                    const Color perspective,
                                    IndexListType* const removed,
                                    IndexListType* const added) {
    Tail::CollectChangedIndices(position, trigger, perspective, removed, added);
    if (Head::refresh_trigger == trigger) {
      const auto start_removed = removed->size();
      const auto start_added = added->size();
      Head::AppendChangedIndices(position, perspective, removed, added);
      for (auto i = start_removed; i < removed->size(); ++i) {
        (*removed)[i] += Tail::dimensions;
      }
      for (auto i = start_added; i < added->size(); ++i) {
        (*added)[i] += Tail::dimensions;
      }
    }
  }

  // 基底クラスと、自身を再帰的に利用するクラステンプレートをfriendにする
  friend class FeatureSetBase<FeatureSet>;
  template <typename... FeatureTypes>
  friend class FeatureSet;
};

//! 特徴量セットを表す
/*! テンプレート引数が1つの場合の特殊化 */
template <typename FeatureType>
class FeatureSet<FeatureType> : public FeatureSetBase<FeatureSet<FeatureType>> {
 public:
  // 評価関数ファイルに埋め込むハッシュ値
  static constexpr uint32_t hash_value = FeatureType::hash_value;
  // 特徴量の次元数
  static constexpr IndexType dimensions = FeatureType::dimensions;
  // 特徴量のうち、同時に値が1となるインデックスの数の最大値
  static constexpr IndexType max_active_dimensions =
      FeatureType::max_active_dimensions;
  // 差分計算の代わりに全計算を行うタイミングのリスト
  using SortedTriggerSet =
      CompileTimeList<TriggerEvent, FeatureType::refresh_trigger>;
  static constexpr auto refresh_triggers = SortedTriggerSet::values;

  // 特徴量名を取得する
  static std::string GetName() { return FeatureType::name; }

 private:
  // 特徴量のうち、値が1であるインデックスのリストを取得する
  static void CollectActiveIndices(const state_action::Position& position,
                                   const TriggerEvent trigger,
                                   const Color perspective,
                                   IndexList* const active) {
    if (FeatureType::refresh_trigger == trigger) {
      FeatureType::AppendActiveIndices(position, perspective, active);
    }
  }

  // 特徴量のうち、一手前から値が変化したインデックスのリストを取得する
  static void CollectChangedIndices(const state_action::Position& position,
                                    const TriggerEvent trigger,
                                    const Color perspective,
                                    IndexList* const removed,
                                    IndexList* const added) {
    if (FeatureType::refresh_trigger == trigger) {
      FeatureType::AppendChangedIndices(position, perspective, removed, added);
    }
  }

  // 基底クラスと、自身を再帰的に利用するクラステンプレートをfriendにする
  friend class FeatureSetBase<FeatureSet>;
   template <typename... FeatureTypes>
   friend class FeatureSet;
};

}  // namespace feature
}  // namespace nn
}  // namespace evaluation
#endif  // !FEATURE_SET_H_INCLUDED
