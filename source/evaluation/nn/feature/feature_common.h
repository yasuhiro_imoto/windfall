#pragma once
#ifndef FEATURE_COMMON_H_INCLUDED
#define FEATURE_COMMON_H_INCLUDED

namespace evaluation {
namespace nn {
namespace feature {
//! インデックスの配列
class IndexList;

//! 特徴量セット
template <typename... FeatureTypes>
class FeatureSet;

//! 差分計算のではなく、全計算を行うタイミングの種類
enum class TriggerEvent {
  None,           //!< 可能な場合は常に差分計算
  FriendOuMoved,  //!< 自玉が移動した場合に全計算
  EnemyOuMoved,   //!< 敵玉が移動した場合に全計算
  AnyOuMoved,     //!< どちらかの王が移動した場合に全計算
  AnyPieceMoved   //!< 常に全計算
};

enum class Side {
  Friend,  //!< 手番側
  Enemy    //!< 相手側
};
}  // namespace feature
}  // namespace nn
}  // namespace evaluation
#endif  // !FEATURE_COMMON_H_INCLUDED
