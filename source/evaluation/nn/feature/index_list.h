#pragma once
#ifndef INDEX_LIST_H_INCLUDED
#define INDEX_LIST_H_INCLUDED

#include <algorithm>

#include "../network_architecture.h"

namespace evaluation {
namespace nn {
namespace feature {
namespace detail {
#pragma warning(push)
#pragma warning(disable : 26495)
//! 特徴量のインデックスの配列
template <typename T, size_t MaxSize>
class ValueList {
 public:
  size_t size() const { return size_; }
  void resize(const size_t size) { size_ = size; }
  void push_back(const T& value) { values_[size_++] = value; }

  T& operator[](const size_t index) { return values_[index]; }
  T* begin() { return values_; }
  T* end() { return values_ + size_; }
  const T& operator[](const size_t index) const { return values_[index]; }
  const T* begin() const { return values_; }
  const T* end() const { return values_ + size_; }

  void swap(ValueList& other) {
    const auto max_size = std::max(size_, other.size_);
    // 配列の要素を交換
    std::swap_ranges(begin(), begin() + max_size, other.begin());
    // 要素数を交換
    std::swap(size_, other.size_);
  }

 private:
  T values_[MaxSize];
  size_t size_ = 0;
};
#pragma warning(pop)
}  // namespace detail

//! 特徴量のインデックスリスト
class IndexList
    : public detail::ValueList<IndexType, RawFeatures::max_active_dimensions> {
};
}  // namespace feature
}  // namespace nn
}  // namespace evaluation
#endif  // !INDEX_LIST_H_INCLUDED
