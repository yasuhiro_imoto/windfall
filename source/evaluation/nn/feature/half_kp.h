#pragma once
#ifndef HALF_KP_H_INCLUDED
#define HALF_KP_H_INCLUDED

#include <cstdint>

#include "../../../rule/color.h"
#include "../../../rule/piece.h"
#include "../../../rule/square.h"
#include "../../bonanza_piece.h"
#include "../nnue_common.h"
#include "feature_common.h"

namespace state_action {
class Position;
}

namespace evaluation {
namespace nn {
namespace feature {
//! 特徴量 half KP：自玉または敵玉の位置と、玉以外の駒の位置の組み合わせ
template <Side AssociatedOu>
class HalfKP {
 public:
  //! 特徴量の名前
  static constexpr const char* name =
      AssociatedOu == Side::Friend ? "HalfKP(Friend)" : "HalfKP(Enemy)";
  //! 評価関数ファイルに埋め込むハッシュ値
  static constexpr uint32_t hash_value =
      0x5D69D5B9u ^ (AssociatedOu == Side::Friend);

  //! 特徴量の次元数
  static constexpr IndexType dimensions =
      static_cast<IndexType>(Square::SIZE) * static_cast<IndexType>(FE_END);
#ifdef NNUE_LITE
  //! 特徴量の次元数のうち、non zeroのインデックスの最大数
  static constexpr IndexType max_active_dimensions = PieceNumber::OU + 1;
#else
  //! 特徴量の次元数のうち、non zeroのインデックスの最大数
  static constexpr IndexType max_active_dimensions = PieceNumber::OU;
#endif // NNUE_LITE

  //! 差分計算の代わりに全計算を行うタイミング
  static constexpr TriggerEvent refresh_trigger =
      AssociatedOu == Side::Friend ? TriggerEvent::FriendOuMoved
                                   : TriggerEvent::EnemyOuMoved;

  //! 特徴量のうち、non zeroのインデックスリストを配列に追加
  static void AppendActiveIndices(const state_action::Position& position,
                                  const Color perspective, IndexList* active);
  //! 特徴量のうち、一手前から値が変化したインデックスリストを配列に追加
  static void AppendChangedIndices(const state_action::Position& position,
                                   const Color perspective, IndexList* removed,
                                   IndexList* added);

  //! 王の位置とBonaPieceから特徴量のインデックスを計算
  static constexpr IndexType MakeIndex(const Square sq_k,
                                       const BonaPiece p) noexcept {
#ifdef NNUE_LITE
    // 下位16bitだけで値を表現できることを確認
    static_assert(static_cast<IndexType>(FE_END) * 81 < (1 << 16), "");
#endif  // NNUE_LITE

    return static_cast<IndexType>(FE_END) * static_cast<IndexType>(sq_k) + p;
  }

 private:
  //! 駒の情報を取得
  static void GetPieces(const state_action::Position& position,
                        const Color perspective, BonaPiece** pieces,
                        Square* sq_target_k);
};
}  // namespace feature
}  // namespace nn
}  // namespace evaluation
#endif  // !HALF_KP_H_INCLUDED
