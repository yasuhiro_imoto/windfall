#include "stdafx.h"

#include "half_kp.h"

#include "../../../rule/color.h"
#include "../../../rule/square.h"
#include "../../../state_action/position.h"
#include "../../../utility/enum_cast.h"
#include "feature_common.h"
#include "index_list.h"

namespace evaluation {
namespace nn {
namespace feature {
template <Side AssociatedOu>
constexpr PieceNumber GetOuPieceNumber(const Color perspective) noexcept {
  return PieceNumber::OU +
         (AssociatedOu == Side::Friend ? perspective : ~perspective);
}

//!  駒の情報を取得
template <Side AssociatedOu>
inline void HalfKP<AssociatedOu>::GetPieces(
    const state_action::Position& position, const Color perspective,
    BonaPiece** pieces, Square* sq_target_k) {
  *pieces = perspective == Color::BLACK
                ? position.evaluation_list()->piece_list_fb()
                : position.evaluation_list()->piece_list_fw();
  const PieceNumber target = GetOuPieceNumber<AssociatedOu>(perspective);
  *sq_target_k = static_cast<Square>(((*pieces)[target] - F_OU) % Square::SIZE);
}

//! 特徴ベクトルのnon zero要素のインデックスを取得
template <Side AssociatedOu>
void HalfKP<AssociatedOu>::AppendActiveIndices(
    const state_action::Position& position, const Color perspective,
    IndexList* active) {
  // コンパイラの警告を回避
  // 配列サイズが小さい場合は何もしない
  if (RawFeatures::max_active_dimensions < max_active_dimensions) {
    return;
  }

  // note: 必要だと思うけど定かではない
  ASSERT_LV3(active->size() == 0);

  BonaPiece* pieces;
  Square sq_target_k;
  GetPieces(position, perspective, &pieces, &sq_target_k);
  for (int i = 0; i < static_cast<int>(PieceNumber::OU); ++i) {
    active->push_back(MakeIndex(sq_target_k, pieces[i]));
  }

#ifdef NNUE_LITE
  // perspectiveじゃない方の王を取得
  const auto opponent_ou = GetOuPieceNumber<AssociatedOu>(~perspective);
  active->push_back(MakeIndex(sq_target_k, pieces[opponent_ou]));
#endif  // NNUE_LITE
}

//! 特徴ベクトルのうち、一手前から変化したインデックスのリストを取得
template <Side AssociatedOu>
void HalfKP<AssociatedOu>::AppendChangedIndices(
    const state_action::Position& position, const Color perspective,
    IndexList* removed, IndexList* added) {
  BonaPiece* pieces;
  Square sq_target_k;
  GetPieces(position, perspective, &pieces, &sq_target_k);
  const auto& dp = position.state()->dirty_piece;
  for (int i = 0; i < dp.dirty_num; ++i) {
#ifdef NNUE_LITE
    if (dp.piece_numbers[i] == GetOuPieceNumber<AssociatedOu>(perspective)) {
      // 自分の王の場合
      continue;
    }
#else
    if (dp.piece_numbers[i] >= PieceNumber::OU) {
      continue;
    }
#endif  // NNUE_LITE

    const auto old_p = static_cast<BonaPiece>(
        dp.changed_pieces[i].old_piece.from[perspective]);
    removed->push_back(MakeIndex(sq_target_k, old_p));

    const auto new_p = static_cast<BonaPiece>(
        dp.changed_pieces[i].new_piece.from[perspective]);
    added->push_back(MakeIndex(sq_target_k, new_p));
  }
}

template class HalfKP<Side::Friend>;
template class HalfKP<Side::Enemy>;
}  // namespace feature
}  // namespace nn
}  // namespace evaluation
