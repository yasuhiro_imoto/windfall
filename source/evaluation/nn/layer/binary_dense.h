#pragma once
#ifndef BINARY_DENSE_H_INCLUDED
#define BINARY_DENSE_H_INCLUDED

#include <immintrin.h>

#include <algorithm>
#include <array>
#include <type_traits>

#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <typename Previous, IndexType D>
class BinaryDense {
 public:
  //! スケールのデータ型
  using InputType = typename Previous::OutputType;
  using OutputType = int32_t;
  static_assert(sizeof(InputType) == 1, "");
  //! スケールの型
  using ScaleType = uint32_t;

  /*! kInputDimensions */
  static constexpr IndexType InputDimensions = Previous::OutputDimensions;
  /*! kOutputDimensions */
  static constexpr IndexType OutputDimensions = D;
  /*! kPaddedInputDimensions */
  // static constexpr IndexType PaddedInputDimensions =
  //  CeilToMultiple<IndexType>(InputDimensiions, max_simd_bytes);

  // 水平方向に合計できる範囲
  using BlockType = uint64_t;
  // 64bitの倍数
  static_assert(InputDimensions % (sizeof(BlockType) * 8) == 0, "");

 private:
  //! 出力のバッファサイズ(byte単位)
  /*! このレイヤーのバッファサイズ
      kSelfBufferSize */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);

 public:
  //! 入力からここまでの累積の出力のバッファサイズ(byte単位)
  /*! kBufferSize */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! スケール用バッファのインデックス
  /*! 値を読み取る位置 */
  static constexpr IndexType scale_index = Previous::scale_index;

 private:
  //! この層でのパラメータの倍率
  /*! 学習結果に基づいてビット数は決定された */
  static constexpr IndexType local_shift_scale_bits =
      InputDimensions == 512 ? 9 : 12;

 public:
  //! 累積のパラメータの倍率
  /*! 各層での倍率も累積するので、適当なタイミングで打ち消す必要がある */
  static constexpr IndexType shift_scale_bits =
      Previous::shift_scale_bits + local_shift_scale_bits - 1;

  const OutputType* operator()(const FeatureType* features,
                               FeatureType* output_buffer,
                               ScaleType* scale_buffer) const {
    const auto inputs = reinterpret_cast<const std::int64_t*>(previous_layer_(
        features, output_buffer + local_buffer_size, scale_buffer));
    // この層の出力(これから計算する)
    const auto outputs = reinterpret_cast<__m256i*>(output_buffer);

    // 64bitずつを同時にいくつ処理できるか
    constexpr IndexType blocks = simd_bytes / sizeof(BlockType);
    static_assert(blocks == 4, "");
    // 入力ベクトルを何回に分けて積を計算するか
    // maddで隣接部分とmergeするので前半と後半に分けたそれぞれが何回か
    // 512bitなら4
    constexpr IndexType input_chunks =
        InputDimensions / 2 / sizeof(BlockType) / 8;
    // 出力ベクトルを何回に分けて計算するか
    // 1回の処理で8個計算できる
    // 256次元なら32
    constexpr IndexType output_chunks =
        OutputDimensions / (simd_bytes / sizeof(OutputType));

    // 1blockあたりに16bitの要素がいくつ含まれるか
    constexpr IndexType elements = sizeof(BlockType) / sizeof(int16_t);
    static_assert(elements == 4, "");
    // 配列の外側にアクセスするのを防ぐ
    static_assert(OutputDimensions % (blocks * elements) == 0, "");

    const __m256i vmask = _mm256_set1_epi8(0xF);
    // clang-format off
    const __m256i vpop1 =
        _mm256_setr_epi8(0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
                         0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4);
    const __m256i vpop2 =
        _mm256_setr_epi8(8, 7, 7, 6, 7, 6, 6, 5, 7, 6, 6, 5, 6, 5, 5, 4,
                         8, 7, 7, 6, 7, 6, 6, 5, 7, 6, 6, 5, 6, 5, 5, 4);
    // clang-format on

    // ±1の掛け算の結果は0の個数Nを使って
    // N * 2 - InputDimensions
    // で与えられる
    // ただし2ブロックを使って計算するので1ブロック当たりではInputDimensions / 2
    // さらに、計算結果は必ず偶数なので1bitシフトして最終的にInputDimensions / 4
    const __m256i count_offset = _mm256_set1_epi16(InputDimensions / 4);

    __m256i input_list[2][input_chunks];
    for (IndexType i = 0; i < input_chunks; ++i) {
      input_list[0][i] = _mm256_set1_epi64x(inputs[i + 0 * input_chunks]);
      input_list[1][i] = _mm256_set1_epi64x(inputs[i + 1 * input_chunks]);
    }

    const auto kernels = reinterpret_cast<const __m256i*>(kernels_);
    const auto kernel_scales = reinterpret_cast<const __m256i*>(scales_);
    const auto biases = reinterpret_cast<const __m256i*>(biases_);
    // 入力の平均
    // 合計を平均にすることで16bitに収まる
    const auto input_scale =
        _mm256_set1_epi16(scale_buffer[scale_index] / InputDimensions);

    for (IndexType i = 0; i < output_chunks; ++i) {
      __m256i result = _mm256_setzero_si256();
      for (IndexType j = 0; j < elements; ++j) {
        __m256i sum_a = _mm256_setzero_si256();
        __m256i sum_b = _mm256_setzero_si256();

        const IndexType offset = i * elements + j;
        // 偶数ならresultの隣接する16bitの組の下位の方の値を計算
        // 奇数なら上位の方
        const IndexType flag = j & 0x1;
        for (IndexType k = 0; k < input_chunks; ++k) {
          const __m256i& in = input_list[flag][k];
          // maddの時に良い感じに隣接している必要がある
          // それぞれ64bit
          // w0,             w2,             w4,             w6,
          //     w0,             w2,             w4,             w6,
          //         w1,             w3,             w5,             w7,
          //             w1,             w3,             w5,             w7,
          // こんな感じに並べると良い感じに0の個数の合計がそれぞれ16bitで並ぶ
          const auto kernel =
              _mm256_load_si256(&kernels[offset * input_chunks + k]);

          // xor
          // 本当はxnorを計算したいが命令がないので、0の個数を求める
          const __m256i xor_value = _mm256_xor_si256(in, kernel);

          // AVX2を利用したpopcnt
          // https://qiita.com/Seizh/items/26eef63af739ba48e36b
          __m256i a = _mm256_and_si256(xor_value, vmask);
          __m256i b = _mm256_and_si256(_mm256_srli_epi64(xor_value, 4), vmask);

          a = _mm256_shuffle_epi8(vpop1, a);
          b = _mm256_shuffle_epi8(vpop2, b);

          sum_a = _mm256_add_epi8(sum_a, a);
          sum_b = _mm256_add_epi8(sum_b, b);
        }
        // 総ビット数から1の個数を引いた
        // すなわち0の個数
        const __m256i sum = _mm256_sad_epu8(sum_a, sum_b);
        // 64bitのうちの下位16bitに値が入っている
        // 16bitずつずらして値を格納
        result = _mm256_or_si256(result, _mm256_slli_epi64(sum, j * 16));
      }
      // 0の個数を±1の掛け算の結果に変換する
      // 必ず偶数なので1bit右にシフトしてある
      result = _mm256_sub_epi16(result, count_offset);
      // 16bitで収まるようにkernelのスケールは調整してある
      result = _mm256_mullo_epi16(result, _mm256_load_si256(&kernel_scales[i]));
      // 32bit
      result = _mm256_madd_epi16(result, input_scale);
      // バイアス項を追加
      _mm256_store_si256(
          &outputs[i], _mm256_add_epi32(result, _mm256_load_si256(&biases[i])));
    }

    return reinterpret_cast<OutputType*>(output_buffer);
  }

 private:
  using BiasType = OutputType;
  using KernelType = InputType;
  // using ScaleType = OutputType;

  //! 一つ前の層
  Previous previous_layer_;

  //! kernelのスケール
  /*! 実質的にuint16_tのデータだが、
      2回ずつ繰り返しになっているので大きさとしてはuint32_t */
  alignas(cache_line_bytes) ScaleType scales_[OutputDimensions];
  alignas(cache_line_bytes) BiasType biases_[OutputDimensions];
  alignas(cache_line_bytes)
      KernelType kernels_[OutputDimensions * InputDimensions / 8];
};

}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !BINARY_DENSE_H_INCLUDED
