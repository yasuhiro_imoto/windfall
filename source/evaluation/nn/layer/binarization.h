#pragma once
#ifndef BINARIZATION_H_INCLUDED
#define BINARIZATION_H_INCLUDED

#include <immintrin.h>

#include <cstdint>
#include <limits>
#include <type_traits>

#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <typename Previous>
class Binarization {
 public:
  // 入出力の型
  using InputType = typename Previous::OutputType;
  //static_assert(std::is_same_v<IndexType, int32_t>, "");
  using OutputType = uint8_t;
  //! スケールの型
  using ScaleType = uint32_t;

  // 入出力の次元数
  static constexpr IndexType InputDimensions = Previous::OutputDimensions;
  static constexpr IndexType OutputDimensions = InputDimensions;

  //! 出力のバッファサイズ(byte単位)
  /*! このレイヤーのバッファサイズ
      kSelfBufferSize */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(static_cast<size_t>(OutputDimensions / 8), cache_line_bytes);
  //! 入力からここまでの累積の出力のバッファサイズ(byte単位)
  /*! kBufferSize */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! スケール用バッファのインデックス
  /*! 値を書き込む位置 */
  static constexpr IndexType scale_index = Previous::scale_index + 1;

 private:
  //! この層で打ち消すパラメータの倍率
  /*! ビット数は学習結果に基づいて良い感じに決定された */
  static constexpr IndexType local_shift_scale_bits = embedding_scale_bits - 1;

 public:
  //! 累積のパラメータの倍率
  /*! ここでembedding層の分を打ち消す */
  static constexpr IndexType shift_scale_bits =
      Previous::shift_scale_bits - local_shift_scale_bits;

  const OutputType* operator()(const FeatureType* features,
                               FeatureType* output_buffer,
                               ScaleType* scale_buffer) const {
    const auto inputs = previous_layer_(
        features, output_buffer + local_buffer_size, scale_buffer);
    const auto outpus = reinterpret_cast<int32_t*>(output_buffer);

    constexpr IndexType input_chunks =
        InputDimensions / simd_bytes * sizeof(InputType);
    constexpr IndexType input_size = sizeof(InputType);
    const auto in_list = reinterpret_cast<const __m256i*>(inputs);

    // 値をクリップ
    // 学習の時は[-1, 1]でクリップした
    // shift_scale_bits桁になっているので、クリップの範囲も拡大される
    const __m256i max_value = _mm256_set1_epi16(1 << shift_scale_bits);

    __m256i sum = _mm256_setzero_si256();
    for (IndexType i = 0; i < input_chunks / input_size; ++i) {
      const IndexType offset = i * input_size;
      // embedding層の分のスケールを打ち消す
      const __m256i in0 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&in_list[offset + 0]), embedding_scale_bits);
      const __m256i in1 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&in_list[offset + 1]), embedding_scale_bits);
      const __m256i in2 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&in_list[offset + 2]), embedding_scale_bits);
      const __m256i in3 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&in_list[offset + 3]), embedding_scale_bits);

      //
      // 2値化
      //
      // 64bitずつin0の前半、in1の前半、in0の後半、in1の後半
      const __m256i a = _mm256_packs_epi32(in0, in1);
      // 同様に64bitずつin2の前半、in3の前半、in2の後半、in3の後半
      const __m256i b = _mm256_packs_epi32(in2, in3);
      // 32bitずつin0の前半、in1の前半、in2の前半、in3の前半、
      //          in0の後半、in1の後半、in2の後半、in3の後半
      const __m256i c = _mm256_packs_epi16(a, b);
      // 各int8_tの最上位ビットを集める
      // そのため正なら0、負なら1で2値化される
      outpus[i] = _mm256_movemask_epi8(c);

      //
      // 絶対値の合計
      //
      const __m256i clipped_a =
          _mm256_min_epu16(_mm256_abs_epi16(a), max_value);
      const __m256i clipped_b =
          _mm256_min_epu16(_mm256_abs_epi16(b), max_value);
      // そのまま足しても桁あふれはない
      static_assert(InputDimensions / 16 * (1 << shift_scale_bits) <
                        std::numeric_limits<uint16_t>::max(),
                    "");
      sum = _mm256_add_epi16(sum, _mm256_add_epi16(clipped_a, clipped_a));
    }

    const __m256i mask = _mm256_set1_epi32(0x0000FFFF);
    // 隣接する16bitの組を足して32bitにする
    const __m256i sum32 =
        _mm256_add_epi32(_mm256_and_si256(sum, mask),
                         _mm256_and_si256(_mm256_srai_epi32(sum, 16), mask));

    // sum2, sum3, sum0, sum1, sum6, sum7 sum4, sum5
    const __m256i shuffle1 =
        _mm256_shuffle_epi32(sum32, _MM_SHUFFLE(1, 0, 3, 2));
    // sum0+sum2, sum1+sum3, _, _, sum4+sum6, sum5+sum7, _, _
    const __m256i total1 = _mm256_add_epi32(sum32, shuffle1);

    // sum1+sum3, sum0+sum2, _, _,  sum5+sum7, sum4+sum6, _, _
    const __m256i shuffle2 =
        _mm256_shuffle_epi32(total1, _MM_SHUFFLE(2, 3, 0, 1));
    // sum0+sum1+sum2+sum3, _, _, _, sum4+sum5+sum6+sum7, _, _, _
    const __m256i total2 = _mm256_add_epi32(total1, shuffle2);

    const __m128i hi = _mm256_extractf128_si256(total2, 1);
    const __m128i lo = _mm256_extractf128_si256(total2, 0);

    const int32_t v = _mm_cvtsi128_si32(lo) + _mm_cvtsi128_si32(hi);
    scale_buffer[scale_index] = v;

    return reinterpret_cast<OutputType*>(output_buffer);
  }

 private:
  //! この層の直前の層
  Previous previous_layer_;
};
}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !BINARIZATION_H_INCLUDED
