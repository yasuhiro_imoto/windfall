#pragma once
#ifndef DENSE_H_INCLUDED
#define DENSE_H_INCLUDED

#include <cstdint>
#include <string>
#include <type_traits>

#include <immintrin.h>

#pragma warning(push)
#pragma warning(disable : 26439 26451 26495 26812 28182)
#include <msgpack.hpp>
#pragma warning(pop)

#include "../../utility/configuration.h"
#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
#ifdef NNUE_VANILLA
template<typename Previous, IndexType D>
class Dense {
public:
  using InputType = typename Previous::OutputType;
  using OutputType = int32_t;

  /*! kInputDimensions */
  static constexpr IndexType InputDimensiions = Previous::OutputDimensions;
  /*! kOutputDimensions */
  static constexpr IndexType OutputDimensions = D;
  /*! KPaddedInputDimensions */
  static constexpr IndexType PaddedInputDimensions =
    CeilToMultiple<IndexType>(InputDimensiions, max_simd_bytes);

  /**
   * @brief このレイヤーで使うのバッファサイズ
   *
   * kSelfBufferSize
  */
  static constexpr size_t local_buffer_size =
    CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);  
  /**
   * @brief  入力からここまでの累積の出力のバッファサイズ
   *
   * kBufferSize
  */
  static constexpr size_t buffer_size =
    Previous::buffer_size + local_buffer_size;

  /**
   * @brief 順伝播
   * @param features 
   * @param buffer 
   * @return 
   * Propagate
  */
  const OutputType* operator()(const FeatureType* features,
                               char* buffer) const noexcept {
    const auto input = previous_layer_(features, buffer + local_buffer_size);
    const auto output = reinterpret_cast<OutputType*>(buffer);

    constexpr IndexType num_chunks = PaddedInputDimensions / simd_bytes;
    const __m256i ones = _mm256_set1_epi16(1);
    const auto input_vector = reinterpret_cast<const __m256i*>(input);
    for (IndexType i = 0; i < OutputDimensions; ++i) {
      const IndexType offset = PaddedInputDimensions * i;
      __m256i sum = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 0, biases_[i]);
      const auto row = reinterpret_cast<const __m256i*>(&kernels_[offset]);
      for (IndexType j = 0; j < num_chunks; ++j) {
        __m256i product = _mm256_maddubs_epi16(
            _mm256_load_si256(&input_vector[j]), _mm256_load_si256(&row[j]));
        product = _mm256_madd_epi16(product, ones);
        sum = _mm256_add_epi32(sum, product);
      }
      sum = _mm256_hadd_epi32(sum, sum);
      sum = _mm256_hadd_epi32(sum, sum);
      const __m128i lo = _mm256_extracti128_si256(sum, 0);
      const __m128i hi = _mm256_extracti128_si256(sum, 1);
      output[i] = _mm_cvtsi128_si32(lo) + _mm_cvtsi128_si32(hi);
    }
    return output;
  }

 private:
  using BiasType = OutputType;
  using KernelType = int8_t;

  //! 一つ前の層
  Previous previous_layer_;

  union {
    alignas(cache_line_bytes) BiasType biases_[OutputDimensions];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(cache_line_bytes) char biases_msgpack_[OutputDimensions *
                                                   sizeof(BiasType)];
  };
  union {
    alignas(cache_line_bytes)
        KernelType kernels_[OutputDimensions * PaddedInputDimensions];
    alignas(cache_line_bytes) char kernels_msgpack_[OutputDimensions *
                                                    PaddedInputDimensions *
                                                    sizeof(KernelType)];
  };

public:
  MSGPACK_DEFINE_MAP(MSGPACK_NVP("bias", biases_msgpack_),
                     MSGPACK_NVP("kernel", kernels_msgpack_),
                     MSGPACK_NVP("previous", previous_layer_));
};
#else
//! 普通の全結合層
template <typename Previous, IndexType D>
class Dense {
 public:
  using InputType = typename Previous::OutputType;
  using OutputType = int32_t;
  static_assert(std::is_same_v<InputType, uint8_t>, "");

  /*! kInputDimensions */
  static constexpr IndexType InputDimensiions = Previous::OutputDimensions;
  /*! kOutputDimensions */
  static constexpr IndexType OutputDimensions = D;
  /*! KPaddedInputDimensions */
  static constexpr IndexType PaddedInputDimensions =
      CeilToMultiple<IndexType>(InputDimensiions, max_simd_bytes);

  //! 出力のバッファサイズ
  /*! このレイヤーのバッファサイズ
      kSelfBufferSize */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);
  //! 入力からここまでの累積の出力のバッファサイズ
  /*! kBufferSize */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! 評価パラメータのファイルに埋め込むハッシュ値
  /*! GetHashValue */
  static constexpr uint32_t hash() {
    uint32_t h = 0xCC03DAE4u;
    h += OutputDimensions;
    h ^= Previous::hash() >> 1;
    h ^= Previous::hash() << 31;
    return h;
  }

  //! 順伝播
  /*! Propagate */
  const OutputType* operator()(const EmbeddingType* embeddings, char* buffer) const {
    // この層に対する入力
    const auto inputs = previous_layer_(embeddings, buffer + local_buffer_size);
    // この層の出力(これから計算する)
    const auto outputs = reinterpret_cast<OutputType*>(buffer);

    constexpr IndexType chunks = PaddedInputDimensions / simd_bytes;
#if defined(USE_AVX2)
    const __m256i ones = _mm256_set1_epi16(1);
    const auto input_vector = reinterpret_cast<const __m256i*>(inputs);
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif // defined(USE_AVX2)

    for (IndexType i = 0; i < OutputDimensions; ++i) {
      const IndexType offset = i * PaddedInputDimensions;
#if defined(USE_AVX2)
      __m256i sum = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 0, biases_[i]);
      const auto row = reinterpret_cast<const __m256i*>(&kernels_[offset]);
      for (IndexType j = 0; j < chunks; ++j) {
        __m256i product = _mm256_maddubs_epi16(_mm256_load_si256(&input_vector[j]), _mm256_load_si256(&row[j]));
        product = _mm256_madd_epi16(product, ones);
        sum = _mm256_add_epi32(sum, product);
      }
      sum = _mm256_hadd_epi32(sum, sum);
      sum = _mm256_hadd_epi32(sum, sum);
      const __m128i lo = _mm256_extracti128_si256(sum, 0);
      const __m128i hi = _mm256_extracti128_si256(sum, 1);
      outputs[i] = _mm_cvtsi128_si32(lo) + _mm_cvtsi128_si32(hi);
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif // defined(USE_AVX2)
    }
    return outputs;
  }

 private:
  using BiasType = OutputType;
  using KernelType = int8_t;

  //! 一つ前の層
  Previous previous_layer_;

  alignas(cache_line_bytes) BiasType biases_[OutputDimensions];
  alignas(cache_line_bytes)
      KernelType kernels_[OutputDimensions * PaddedInputDimensions];
};
#endif // NNUE_VANILLA

}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !DENSE_H_INCLUDED
