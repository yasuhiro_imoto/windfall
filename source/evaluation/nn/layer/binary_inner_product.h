#pragma once
#ifndef BINARY_INNER_PRODUCT_H_INCLUDED
#define BINARY_INNER_PRODUCT_H_INCLUDED

#include <immintrin.h>

#include <algorithm>
#include <array>
#include <type_traits>

#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <typename Previous>
class BinaryInnerProduct {
 public:
  //! スケールのデータ型
  using InputType = typename Previous::OutputType;
  using OutputType = int32_t;
  static_assert(std::is_same_v<InputType, int16_t>, "");
  //! スケールの型
  using ScaleType = uint32_t;

  /*! kInputDimensions */
  static constexpr IndexType InputDimensions = Previous::OutputDimensions;
  /*! kOutputDimensions */
  static constexpr IndexType OutputDimensions = 1;
  /*! kPaddedInputDimensions */
  // static constexpr IndexType PaddedInputDimensions =
  //  CeilToMultiple<IndexType>(InputDimensiions, max_simd_bytes);

  //! 出力のバッファサイズ(byte単位)
  /*! このレイヤーのバッファサイズ
      kSelfBufferSize */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);
  //! 入力からここまでの累積の出力のバッファサイズ(byte単位)
  /*! kBufferSize */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! スケール用バッファのインデックス
  /*! 使わない */
  static constexpr IndexType scale_index = Previous::scale_index;

 private:
  //! この層でのパラメータの倍率
  /*! 学習結果に基づいてビット数は決定された */
  static constexpr IndexType local_shift_scale_bits = 8;

 public:
  //! 累積のパラメータの倍率
  /*! 各層での倍率も累積するので、適当なタイミングで打ち消す必要がある */
  static constexpr IndexType shift_scale_bits =
      Previous::shift_scale_bits + local_shift_scale_bits;

  const OutputType* operator()(const FeatureType* features,
                               FeatureType* output_buffer,
                               ScaleType* scale_buffer) const {
    const auto inputs = reinterpret_cast<const __m256i*>(previous_layer_(
        features, output_buffer + local_buffer_size, scale_buffer));
    const auto outputs = reinterpret_cast<OutputType*>(output_buffer);

    constexpr IndexType chunks =
        InputDimensions / simd_bytes * sizeof(InputType);
    __m256i sum = _mm256_setzero_si256();
    for (IndexType i = 0; i < chunks; ++i) {
      const auto x = _mm256_stream_load_si256(&inputs[i]);

      const IndexType index = simd_bytes / sizeof(KernelType) * i;
      const auto w =
          _mm256_load_si256(reinterpret_cast<const __m256i*>(&kernels_[index]));

      // xとwを要素ごとに掛け算
      // 補数で1を足す処理はバイアス項にまとめた
      const auto y = _mm256_xor_si256(x, w);

      sum = _mm256_add_epi16(sum, y);
    }
    // 水平方向に合計

    const __m256i mask = _mm256_set1_epi32(0x0000FFFF);
    // 隣接する16bitの組を足して32bitにする
    const __m256i sum32 =
        _mm256_add_epi32(_mm256_and_si256(sum, mask),
                         _mm256_and_si256(_mm256_srai_epi32(sum, 16), mask));

    // sum2, sum3, sum0, sum1, sum6, sum7 sum4, sum5
    const __m256i shuffle1 =
        _mm256_shuffle_epi32(sum32, _MM_SHUFFLE(1, 0, 3, 2));
    // sum0+sum2, sum1+sum3, _, _, sum4+sum6, sum5+sum7, _, _
    const __m256i total1 = _mm256_add_epi32(sum32, shuffle1);

    // sum1+sum3, sum0+sum2, _, _,  sum5+sum7, sum4+sum6, _, _
    const __m256i shuffle2 =
        _mm256_shuffle_epi32(total1, _MM_SHUFFLE(2, 3, 0, 1));
    // sum0+sum1+sum2+sum3, _, _, _, sum4+sum5+sum6+sum7, _, _, _
    const __m256i total2 = _mm256_add_epi32(total1, shuffle2);

    const __m128i hi = _mm256_extractf128_si256(total2, 1);
    const __m128i lo = _mm256_extractf128_si256(total2, 0);

    const int32_t v = _mm_cvtsi128_si32(lo) + _mm_cvtsi128_si32(hi);

    // まだこの層での重みについての平均が残っている
    // この後でFV_SCALEで割る処理があるので、そこで一緒に行う
    outputs[0] = v * scale_ + bias_;

    return outputs;
  }

 private:
  // using ScaleType = OutputType;
  using BiasType = OutputType;
  using KernelType = InputType;

  //! 一つ前の層
  Previous previous_layer_;

  ScaleType scale_;
  //! バイアス項
  /*! 2の補数で1を加える分はここに含まれている */
  BiasType bias_;
  //! +1か-1
  /*! +1なら全部0、-1なら全部1 */
  alignas(cache_line_bytes) KernelType kernels_[InputDimensions];
};
}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !BINARY_INNER_PRODUCT_H_INCLUDED
