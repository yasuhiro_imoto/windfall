#pragma once
#ifndef RELU_H_INCLUDED
#define RELU_H_INCLUDED

#include <algorithm>
#include <type_traits>

#include <immintrin.h>

#pragma warning(push)
#pragma warning(disable : 26439 26451 26495 26812 28182)
#include <msgpack.hpp>
#pragma warning(pop)

#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <typename Previous>
class ReLU {
 public:
  using InputType = typename Previous::OutputType;
  using OutputType = uint8_t;
  static_assert(std::is_same_v<InputType, int32_t>, "");

  static constexpr IndexType InputDimensions = Previous::OutputDimensions;
  static constexpr IndexType OutputDimensions = InputDimensions;

  /**
   * @brief このレイヤーで使用するバッファサイズ
   *
   * kSelfBufferSize
   */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);
  /**
   * @brief 入力からここまでの累積の出力のバッファサイズ
   *
   * kBufferSize
   */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! 順伝播
  const OutputType* operator()(const FeatureType* features,
                               char* buffer) const noexcept {
    // この層に対する入力
    const auto input = previous_layer_(features, buffer + local_buffer_size);
    // この層の出力(これから計算する)
    const auto output = reinterpret_cast<OutputType*>(buffer);

    constexpr IndexType num_chunks = InputDimensions / simd_bytes;
#if defined(USE_AVX2)
    const __m256i zeros = _mm256_setzero_si256();
    const __m256i offsets = _mm256_set_epi32(7, 3, 6, 2, 5, 1, 4, 0);

    const auto in = reinterpret_cast<const __m256i*>(input);
    const auto out = reinterpret_cast<__m256i*>(output);

    for (IndexType i = 0; i < num_chunks; ++i) {
      const __m256i words0 = _mm256_srai_epi16(
          _mm256_packs_epi32(_mm256_load_si256(&in[i * 4 + 0]),
                             _mm256_load_si256(&in[i * 4 + 1])),
          weight_scale_bits);
      const __m256i words1 = _mm256_srai_epi16(
          _mm256_packs_epi32(_mm256_load_si256(&in[i * 4 + 2]),
                             _mm256_load_si256(&in[i * 4 + 3])),
          weight_scale_bits);
      _mm256_store_si256(
          &out[i],
          _mm256_permutevar8x32_epi32(
              _mm256_max_epi8(_mm256_packs_epi16(words0, words1), zeros),
              offsets));
    }

    constexpr IndexType start = num_chunks * simd_bytes;
#else
#error Not Implemented. Please define symbol USE_AVX2.
#endif  // defined(USE_AVX2)
    if constexpr (start < InputDimensions) {
      for (IndexType i = start; i < InputDimensions; ++i) {
        output[i] = static_cast<OutputType>(
            std::clamp(input[i] >> weight_scale_bits, 0, 127));
      }
    }
    return output;
  }

 private:
  Previous previous_layer_;

 public:
  MSGPACK_DEFINE_MAP(MSGPACK_NVP("previous", previous_layer_));
};
}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !RELU_H_INCLUDED
