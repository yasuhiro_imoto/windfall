#pragma once
#ifndef LEAKY_RELU_H_INCLUDED
#define LEAKY_RELU_H_INCLUDED

#include <immintrin.h>

#include <cstdint>
#include <limits>
#include <type_traits>

#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <typename Previous>
class LeakyReLU {
 public:
  // 入出力の型
  using InputType = typename Previous::OutputType;
  //static_assert(std::is_same_v<IndexType, int32_t>, "");
  using OutputType = int16_t;

  // 入出力の次元数
  static constexpr IndexType InputDimensions = Previous::OutputDimensions;
  static constexpr IndexType OutputDimensions = InputDimensions;

  //! 出力のバッファサイズ(byte単位)
  /*! このレイヤーのバッファサイズ
      kSelfBufferSize */
  static constexpr size_t local_buffer_size =
      CeilToMultiple(OutputDimensions * sizeof(OutputType), cache_line_bytes);
  //! 入力からここまでの累積の出力のバッファサイズ(byte単位)
  /*! kBufferSize */
  static constexpr size_t buffer_size =
      Previous::buffer_size + local_buffer_size;

  //! スケール用バッファのインデックス
  /*! 使わない */
  static constexpr IndexType scale_index = Previous::scale_index;

 private:
  //! この層での打ち消すパラメータの倍率
  /*! 次の層でオーバーフローを発生させなければよい */
  static constexpr IndexType local_shift_scale_bits = 16;

 public:
  //! 累積のパラメータの倍率
  /*! 各層での倍率も累積するので、適当なタイミングで打ち消す必要がある */
  static constexpr IndexType shift_scale_bits =
      Previous::shift_scale_bits - local_shift_scale_bits;
  static_assert(shift_scale_bits > 0, "");
  // 次でオーバーフローしないかどうか
  static_assert(((1 << (shift_scale_bits + 1)) - 1) * InputDimensions /
                        (simd_bytes / sizeof(OutputType)) <
                    std::numeric_limits<int16_t>::max(),
                "");

  const OutputType* operator()(const FeatureType* features,
                               FeatureType* output_buffer,
                               uint32_t* scale_buffer) const {
    const auto inputs = reinterpret_cast<const __m256i*>(previous_layer_(
        features, output_buffer + local_buffer_size, scale_buffer));
    const auto outpus = reinterpret_cast<__m256i*>(output_buffer);

    // ほとんどの場合で次の層でオーバーフローしない桁数
    // 最悪ケースでのみオーバーフローする
    constexpr IndexType max_bits =
        15 - Log2<InputDimensions / (simd_bytes / sizeof(OutputType))>::value;

    const __m256i upper = _mm256_set1_epi16(1 << max_bits);
    const __m256i lower = _mm256_set1_epi16(-(1 << (max_bits - 1)));

    constexpr IndexType chunks =
        InputDimensions / simd_bytes * sizeof(InputType) / 2;
    for (IndexType i = 0; i < chunks; ++i) {
      const __m256i in1 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&inputs[i * 2 + 0]), local_shift_scale_bits);
      const __m256i in2 = _mm256_srai_epi32(
          _mm256_stream_load_si256(&inputs[i * 2 + 1]), local_shift_scale_bits);

      const __m256i x = _mm256_packs_epi32(in1, in2);
      // 正の値を1.5倍、負の値を0.5倍する
      // 頑張れば正の値をそのままで負の値を2^p倍(pは負の整数)にできるが、
      // 計算コストをかけるメリットがあるのかは不明
      const __m256i y = _mm256_srai_epi16(_mm256_abs_epi16(x), 1);
      __m256i z = _mm256_add_epi16(x, y);

      // 値を制限
      z = _mm256_min_epi16(z, upper);
      z = _mm256_max_epi16(z, lower);

      _mm256_store_si256(&outpus[i], z);
    }

    return reinterpret_cast<OutputType*>(output_buffer);
  }

 private:
  //! この層の直前の層
  Previous previous_layer_;
};
}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !LEAKY_RELU_H_INCLUDED
