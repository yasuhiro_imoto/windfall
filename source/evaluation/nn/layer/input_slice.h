#pragma once
#ifndef INPUT_SLICE_H_INCLUDED
#define INPUT_SLICE_H_INCLUDED

#include <cstdint>

#pragma warning(push)
#pragma warning(disable : 26439 26451 26495 26812 28182)
#include <msgpack.hpp>
#pragma warning(pop)

#include "../../utility/configuration.h"
#include "../nnue_common.h"

namespace evaluation {
namespace nn {
namespace layer {
template <IndexType D, IndexType Offset = 0>
class InputSliice {
  // アライメントを維持する必要がある
  static_assert(Offset % max_simd_bytes == 0, "");

 public:
  using OutputType = FeatureType;

  static constexpr IndexType OutputDimensions = D;

  //! これより前の層で利用するバッファサイズ
  static constexpr size_t buffer_size = 0;

#ifdef NNUE_VANILLA
  const OutputType* operator()(const FeatureType* features,
                               char* /*buffer*/) const noexcept {
    return features + Offset;
  }
#else
  //! スケール用バッファのインデックス
  /*! ここでは利用せず上の層に値を伝えるだけ
      値を読み取る位置 */
  static constexpr IndexType scale_index = 0;

  //! embedding層でのパラメータの倍率
  /*! 各層での倍率も累積するので、適当なタイミングで打ち消す必要がある */
  static constexpr IndexType shift_scale_bits = embedding_scale_bits;

  /**
   * @brief 順伝播
   * @param features
   * @param
   * @param
   * @return
   * Propagate
   */
  const OutputType* operator()(const FeatureType* features,
                               FeatureType* /* output_buffer */,
                               uint32_t* /* scale_buffer */) const {
    return features;
  }
#endif  // !NNUE_VANILLA

  MSGPACK_DEFINE();
};
}  // namespace layer
}  // namespace nn
}  // namespace evaluation
#endif  // !INPUT_SLICE_H_INCLUDED
