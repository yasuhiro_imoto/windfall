#include "stdafx.h"

#include "network.h"

namespace evaluation {
namespace nn {
AlignedPtr<FeatureEmbedding> feature_embedding;
AlignedPtr<Network> network;

namespace detail {
template <typename T>
void Initialize(AlignedPtr<T>& pointer) {
  // make_uniqueで初期化したい気もするが、alignが必要なので、こうなる
  pointer.reset(reinterpret_cast<T*>(aligned_malloc(sizeof(T), alignof(T))));
}
}  // namespace detail

void Initialize() {
  if (!feature_embedding) {
    detail::Initialize(feature_embedding);
  }
  if (!network) {
    detail::Initialize(network);
  }
}
}  // namespace nn
}  // namespace evaluation
