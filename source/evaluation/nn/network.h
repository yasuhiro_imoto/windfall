#pragma once
#ifndef NETWORK_H_INCLUDED
#define NETWORK_H_INCLUDED

#include <memory>

#include "../../utility/bit_operator.h"
#include "feature_embedding.h"
#include "network_architecture.h"

namespace evaluation {
namespace nn {
/**
 * @brief alignedなメモリを自動で開放するためのデリータ
 * @tparam T
 */
template <typename T>
struct AlignedDeleter {
  void operator()(T* ptr) const {
    ptr->~T();
    aligned_free(ptr);
  }
};

template <typename T>
using AlignedPtr = std::unique_ptr<T, AlignedDeleter<T>>;

extern AlignedPtr<FeatureEmbedding> feature_embedding;
extern AlignedPtr<Network> network;

void Initialize();
}  // namespace nn
}  // namespace evaluation
#endif  // !NETWORK_H_INCLUDED
