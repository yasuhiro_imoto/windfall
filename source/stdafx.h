#pragma once
#ifndef STDAFX_H_INCLUDED
#define STDAFX_H_INCLUDED

#ifdef _MSC_VER
#define NOMINMAX
#endif  // _MSC_VER

#include <immintrin.h>

#include <algorithm>
#include <array>
#include <chrono>
#include <cstdint>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <queue>
#include <sstream>
#include <string>
#include <thread>
#include <type_traits>
#include <unordered_map>
#include <vector>

#pragma warning(push)
#pragma warning(disable : 26451 26495 26812 26819)
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/operators.hpp>
#include <boost/range/algorithm/find.hpp>
#include <boost/range/algorithm/lexicographical_compare.hpp>
#include <boost/range/algorithm/remove_if.hpp>
#include <boost/range/algorithm/stable_sort.hpp>
#include <boost/scope_exit.hpp>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable : 4566 26812 26451 26439 28020 26495 6387 28182)
#define SPDLOG_EOL "\n"
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>
#include <msgpack.hpp>
#include <nlohmann/json.hpp>
#pragma warning(pop)

#ifdef NNUE_LEARN
#pragma warning(push)
#pragma warning(disable : 4267 4996 6011 6387 26110 26451 26495 26812)
#include <grpcpp/ext/proto_server_reflection_plugin.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#pragma warning(pop)
#endif // NNUE_LEARN

#endif  // !STDAFX_H_INCLUDED
