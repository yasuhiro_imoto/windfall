#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from multiprocessing import Pool
from pathlib import Path

import click
import cshogi
import numpy as np
import tensorflow as tf
from tqdm import tqdm

from make_record1 import BonaPiece

__author__ = 'Yasuhiro'
__date__ = '2019/09/22'


# kingに対応するインデックスの99は使わない
# 配列をオーバーランする値に設定
PIECE_OFFSET = [0, 2, 4, 6, 8, 12, 16, 10, 99, 10, 10, 10, 10, 14, 18]


def fv40(board):
    p_index, q_index = [], []
    for square in range(81):
        piece = board.piece(square)
        if piece == 0:
            continue

        piece_type = piece & 0xF
        if piece_type != 8:
            piece_color = piece >> 4
            p_value, q_value = 12 + piece_color, 12 + 1 - piece_color
            offset = PIECE_OFFSET[piece_type]
            p_offset, q_offset = p_value + offset, q_value + offset

            p_index.append(square + BonaPiece[p_offset])
            q_index.append(80 - square + BonaPiece[q_offset])
        else:
            if piece == 8:
                p = square
            else:
                q = 80 - square

    pieces_in_hand = board.pieces_in_hand
    for color in range(2):
        for piece_type in range(7):
            p_hand = BonaPiece[color + piece_type * 2]
            q_hand = BonaPiece[1 - color + piece_type * 2]

            piece_count = pieces_in_hand[color][piece_type]
            for i in range(piece_count):
                p_index.append(p_hand + i)
                q_index.append(q_hand + i)

    # noinspection PyUnresolvedReferences
    if board.turn == cshogi.BLACK:
        # noinspection PyUnboundLocalVariable
        return p, q, p_index, q_index
    else:
        # noinspection PyUnboundLocalVariable
        return q, p, q_index, p_index


def convert_data(data):
    # noinspection PyUnresolvedReferences
    board = cshogi.Board()
    board.set_psfen(data['sfen'])
    # noinspection PyUnresolvedReferences
    board.push_psv(data['move'])

    p, q, fv38p, fv38q = fv40(board=board)

    size = 1548
    p_index = [p * size + i for i in fv38p]
    q_index = [q * size + i for i in fv38q]

    example = tf.train.Example(features=tf.train.Features(feature={
        'p': tf.train.Feature(int64_list=tf.train.Int64List(value=p_index)),
        'q': tf.train.Feature(int64_list=tf.train.Int64List(value=q_index)),
        'score': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[-int(data['score'])]
        )),
        'ply': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[int(data['gamePly'])]
        )),
        'result': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[-int(data['game_result'])]
        ))
    }))
    return example.SerializeToString()


@click.command()
@click.option('--input-path', type=click.Path(exists=True, dir_okay=False))
@click.option('--output-path', type=click.Path())
def cmd(input_path, output_path):
    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    options = tf.io.TFRecordOptions(compression_type="GZIP")
    with Pool(20) as p:
        with tf.io.TFRecordWriter(str(output_path), options=options) as writer:
            # noinspection PyUnresolvedReferences
            dataset = np.fromfile(input_path, dtype=cshogi.PackedSfenValue)
            for record in tqdm(p.imap_unordered(convert_data, dataset),
                               total=len(dataset)):
                writer.write(record)


def main():
    cmd()


if __name__ == '__main__':
    main()
