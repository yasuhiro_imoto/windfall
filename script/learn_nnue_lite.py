#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
学習用に指定局面での読み筋を求める
"""

from dataclasses import dataclass
from multiprocessing import Pool
from pathlib import Path

import cshogi
import grpc
import joblib
import msgpack
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
from absl import flags, app
from adabelief_tf import AdaBeliefOptimizer
from scipy.special import expit
from tqdm import tqdm

try:
    import remote_usi_pb2
    import remote_usi_pb2_grpc
except ImportError as e:
    from grpc_tools import protoc
    protoc.main(('', '--proto_path=../source', '--python_out=.',
                 '--grpc_python_out=.', 'remote_usi.proto'))

    import remote_usi_pb2
    import remote_usi_pb2_grpc

from nnue.nnue import NNUE
from nnue.nnue_lite import NNUELite
from feature_lite import get_feature_index
from convert_bin_to_mpac import load_parameter
from make_record2 import fv40

__author__ = 'Yasuhiro'
__date__ = '2020/10/10'

FLAGS = flags.FLAGS
flags.DEFINE_string('sfen_sample_path', default='', help='')
flags.DEFINE_integer('n_samples', default=10000, help='')
flags.DEFINE_integer('sample_offset', default=0, help='')
flags.DEFINE_integer('teacher_depth', default=8, help='教師データの探索深さ')
flags.DEFINE_integer('student_depth', default=4, help='')
flags.DEFINE_string('teacher_pv', default='', help='教師データの読み筋のパス')
flags.DEFINE_string('model_dir', default='', help='')
flags.DEFINE_integer('batch_size', default=1000, help='')
flags.DEFINE_integer('threads', default=4, help='')
flags.DEFINE_integer('repeat', default=5, help='初期値を設定する学習のエポック数')
flags.DEFINE_enum('mode', default='learn',
                  enum_values=['learn', 'teacher_pv', 'initialize',
                               'nnue_test', 'distillate', 'make_record',
                               'learn_rank', 'make_rank_record'],
                  help='')
flags.DEFINE_string(
    'nnue_parameter',
    default='/home/yasuhiro/fluke/build/source/水匠2/eval/nn.bin',
    help=''
)
flags.DEFINE_string('data_path', default='', help='')
flags.DEFINE_string('record_path', default='', help='変換したファイルパス')
flags.DEFINE_string('record_dir', default='../data', help='')
flags.DEFINE_integer('epochs', default=1, help='')
flags.DEFINE_float('kappa', default=1.0, help='目的関数の重み')
flags.DEFINE_bool('multi', default=False, help='multi GPU')


def load_data(data_path):
    """
    やねうら王の教師データのバイナリを読み込む
    水匠の評価関数で読み筋を求める局目集として使う
    :param data_path:
    :return:
    """
    # noinspection PyStatementEffect
    psfens = np.fromfile(data_path, dtype=cshogi.PackedSfenValue,
                         count=FLAGS.n_samples, offset=FLAGS.sample_offset)
    return psfens


@dataclass
class PvData:
    sfen: np.ndarray
    pv: np.ndarray
    score: int


@dataclass
class FeatureIndex:
    p_index: np.ndarray
    q_index: np.ndarray


@dataclass
class TrainingPair:
    teacher: FeatureIndex
    teacher_sign: float
    student: FeatureIndex
    student_sign: float
    teacher_score: int


@dataclass
class TrainingData:
    teacher: FeatureIndex
    teacher_sign: np.ndarray
    student: FeatureIndex
    student_sign: np.ndarray
    teacher_score: np.ndarray


def make_teacher_pv(output_path):
    """
    学習データ生成の第一ステップ
    局面集に対して教師データとなる評価関数で読み筋を求める
    :param output_path:
    :return:
    """
    psfens = load_data(data_path=FLAGS.sfen_sample_path)

    messages = [remote_usi_pb2.SearchCondition(sfen=data['sfen'].tobytes(),
                                               depth=FLAGS.teacher_depth)
                for data in psfens]

    training_data = []
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = remote_usi_pb2_grpc.LearnerStub(channel=channel)
        results = stub.GetPV(iter(messages))
        for r, sfen in tqdm(zip(results, psfens['sfen']), total=len(messages)):
            pv_data = PvData(sfen=sfen,
                             pv=np.asarray(r.move, dtype=np.int32),
                             score=r.score)
            training_data.append(pv_data)

    joblib.dump(training_data, output_path)


def get_training_features(pv_data_list, depth):
    messages = [remote_usi_pb2.TeacherPV(sfen=data.sfen.tobytes(),
                                         pv=data.pv[:depth], score=data.score)
                for data in pv_data_list]

    training_data = []
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = remote_usi_pb2_grpc.LearnerStub(channel=channel)
        results = stub.ComputeTrainingPair(iter(messages))
        for r in tqdm(results, total=len(messages), leave=False):
            teacher = FeatureIndex(
                p_index=np.asarray(r.teacher.p_index, dtype=np.int32) & 0xFFFF,
                q_index=np.asarray(r.teacher.q_index, dtype=np.int32) & 0xFFFF
            )
            student = FeatureIndex(
                p_index=np.asarray(r.student.p_index, dtype=np.int32) & 0xFFFF,
                q_index=np.asarray(r.student.q_index, dtype=np.int32) & 0xFFFF
            )
            training_pair = TrainingPair(
                teacher=teacher, teacher_sign=r.teacher_sign,
                student=student, student_sign=r.student_sign,
                teacher_score=r.teacher_score
            )
            training_data.append(training_pair)

            # assert np.all(teacher.p_index < 29241)
            # assert np.all(teacher.q_index < 29241)
            # assert np.all(student.p_index < 29241)
            # assert np.all(student.q_index < 29241)
    return training_data


def convert_training_pair(training_pair):
    teacher_p = np.empty((len(training_pair), 39), dtype=np.int32)
    teacher_q = np.empty_like(teacher_p)
    student_p = np.empty_like(teacher_p)
    student_q = np.empty_like(teacher_p)

    teacher_sign = np.empty((len(training_pair),), dtype=np.float32)
    student_sign = np.empty_like(teacher_sign)

    teacher_score = np.empty_like(teacher_sign)

    for i, data in enumerate(training_pair):
        teacher_p[i] = data.teacher.p_index
        teacher_q[i] = data.teacher.q_index
        student_p[i] = data.student.p_index
        student_q[i] = data.student.q_index

        teacher_sign[i] = data.teacher_sign
        student_sign[i] = data.student_sign

        teacher_score[i] = data.teacher_score
    assert np.all(teacher_p < 29241)
    assert np.all(teacher_q < 29241)
    assert np.all(student_p < 29241)
    assert np.all(student_q < 29241)

    teacher_score = expit(
        np.clip(teacher_score / 600.0, a_min=-3, a_max=3)
    ).astype(np.float32)
    teacher_score = np.reshape(teacher_score, (-1, 1))

    training_data = TrainingData(
        teacher=FeatureIndex(p_index=teacher_p, q_index=teacher_q),
        student=FeatureIndex(p_index=student_p, q_index=student_q),
        teacher_sign=teacher_sign, student_sign=student_sign,
        teacher_score=teacher_score
    )

    return training_data


def convert_feature_index(feature_index):
    d = {'fu': feature_index[:, 0:18], 'ky': feature_index[:, 18:22],
         'ke': feature_index[:, 22:26], 'gi': feature_index[:, 26:30],
         'ka': feature_index[:, 30:32], 'hi': feature_index[:, 32:34],
         'ki': feature_index[:, 34:38], 'ou': feature_index[:, 38:39]}
    return d


def make_dataset(training_data, batch_size):
    teacher_p_index = convert_feature_index(training_data.teacher.p_index)
    teacher_q_index = convert_feature_index(training_data.teacher.q_index)
    student_p_index = convert_feature_index(training_data.student.p_index)
    student_q_index = convert_feature_index(training_data.student.q_index)

    dataset = tf.data.Dataset.from_tensor_slices({
        'teacher_p_index': teacher_p_index, 'teacher_q_index': teacher_q_index,
        'student_p_index': student_p_index, 'student_q_index': student_q_index,
        'teacher_sign': training_data.teacher_sign,
        'student_sign': training_data.student_sign,
        'teacher_score': training_data.teacher_score
    }).shuffle(10 * batch_size).batch(batch_size)
    return dataset


def initialize():
    """
    学習をこれから始める
    いきなり読み筋を比較しても効率が悪いと思うので、読み筋の評価値でパラメータの初期化を行う
    :return:
    """
    teacher_pv = joblib.load(FLAGS.teacher_pv)
    dataset = make_score_dataset(teacher_pv=teacher_pv)

    model_dir = Path(FLAGS.model_dir)

    model = NNUELite()
    optimizer = AdaBeliefOptimizer(learning_rate=1e-3)
    metrics = tf.keras.metrics.Mean('loss')

    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=model, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)

    log_dir = str(model_dir / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    @tf.function
    def train_step(x, y):
        with tf.GradientTape() as tape:
            prediction = model(x)

            loss = tf.math.reduce_mean(
                tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=y, logits=prediction
                )
            )
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        metrics.update_state(loss)

        return loss

    total = len(teacher_pv) * FLAGS.repeat * 2 // FLAGS.batch_size
    for i, data in tqdm(enumerate(dataset), total=total):
        train_step(data[0], data[1])

        if (i + 1) % 200 == 0:
            with summary_writer.as_default():
                tf.summary.scalar('kl_divergence', metrics.result(),
                                  step=int(ckpt.step))
            metrics.reset_states()

            ckpt.step.assign_add(1)
            manager.save()

    export_parameter(model=model, output_path=model_dir / 'nn.mpac')


def make_score_dataset(teacher_pv):
    """
    局面と目標となる評価値のデータセットを作る
    モデルのパラメータの初期化としての学習に使う
    :param teacher_pv:
    :return:
    """
    board = cshogi.Board()
    # feature_size = 361

    p_index = np.empty((len(teacher_pv) * 2, 39), dtype=np.int32)
    q_index = np.empty_like(p_index)
    scores = np.empty((len(teacher_pv) * 2,), dtype=np.int32)
    for i, data in enumerate(tqdm(teacher_pv)):
        board.set_psfen(data.sfen)

        p, q = get_feature_index(board=board)
        copy_index(source=p, target=p_index[i * 2])
        copy_index(source=q, target=q_index[i * 2])
        scores[i * 2] = data.score

        board.push_psv(np.uint16(data.pv[0] & 0xFFFF))
        p, q = get_feature_index(board=board)
        copy_index(source=p, target=p_index[i * 2 + 1])
        copy_index(source=q, target=q_index[i * 2 + 1])
        scores[i * 2 + 1] = -data.score

    p_index = convert_feature_index(feature_index=p_index)
    q_index = convert_feature_index(feature_index=q_index)
    scores = np.reshape(scores, [-1, 1])

    def convert(x, y):
        y = tf.cast(y, dtype=tf.float32) / 600.0
        y = tf.clip_by_value(y, clip_value_min=-3, clip_value_max=3)
        y = tf.math.sigmoid(y)
        return x, y
    dataset = tf.data.Dataset.from_tensor_slices(
        ((p_index, q_index), scores)
    ).repeat(FLAGS.repeat).shuffle(FLAGS.batch_size * 10).map(convert).batch(
        FLAGS.batch_size)

    return dataset


def copy_index(source, target):
    target[0:18] = source.fu
    target[18:22] = source.ky
    target[22:26] = source.ke
    target[26:30] = source.gi
    target[30:32] = source.ka
    target[32:34] = source.hi
    target[34:38] = source.ki
    target[38] = source.ou

    target += source.ou_square * 361


def export_parameter(model, output_path):
    embedding_kernel = tf.cast(tf.math.round(model.feature.kernel * 127.0),
                               dtype=tf.int16).numpy()
    embedding_bias = tf.cast(tf.math.round(model.feature.bias * 127.0),
                             dtype=tf.int16).numpy()
    embedding_kernel = embedding_kernel.astype(np.int16)
    embedding_bias = embedding_bias.astype(np.int16)
    print('embedding:', embedding_kernel.shape)

    hidden1_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[1].kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden1_bias = tf.cast(tf.round(model.net.layers[1].bias * 127 * 64),
                           dtype=tf.int32).numpy()
    hidden1_kernel = hidden1_kernel.astype(np.int8)
    hidden1_bias = hidden1_bias.astype(np.int32)
    print('dense1:', hidden1_kernel.shape)

    hidden2_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[3].kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden2_bias = tf.cast(tf.round(model.net.layers[3].bias * 127 * 64),
                           dtype=tf.int32).numpy()
    hidden2_kernel = hidden2_kernel.astype(np.int8)
    hidden2_bias = hidden2_bias.astype(np.int32)
    print('dense2:', hidden2_kernel.shape)

    hidden3_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[5].kernel, perm=[1, 0]) *
                600 * 16 / 127,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden3_bias = tf.cast(tf.round(model.net.layers[5].bias * 600 * 16),
                           dtype=tf.int32).numpy()
    hidden3_kernel = hidden3_kernel.astype(np.int8)
    hidden3_bias = hidden3_bias.astype(np.int32)
    print('dense3:', hidden3_kernel.shape)

    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    with output_path.open('wb') as f:
        header = {b'author': b'Yasuhiro Imoto',
                  b'version': 'Fluke v0.2'.encode('utf-8'),
                  b'comment': b''}
        tmp = msgpack.packb(header)
        f.write(tmp)

        feature = {b'kernel': embedding_kernel.tobytes(),
                   b'bias': embedding_bias.tobytes()}
        tmp = msgpack.packb(feature, use_bin_type=False)
        f.write(tmp)

        hidden1 = {b'kernel': hidden1_kernel.tobytes(),
                   b'bias': hidden1_bias.tobytes(),
                   b'previous': []}
        hidden2 = {b'kernel': hidden2_kernel.tobytes(),
                   b'bias': hidden2_bias.tobytes(),
                   b'previous': {b'previous': hidden1}}
        hidden3 = {b'kernel': hidden3_kernel.tobytes(),
                   b'bias': hidden3_bias.tobytes(),
                   b'previous': {b'previous': hidden2}}
        tmp = msgpack.packb(hidden3, use_bin_type=False)
        f.write(tmp)


def get_test_sfen():
    # shogi-evalと同じ局面
    sfen_list = [
        # 40 orqha
        # 15 illqha2.1
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
        # -873 orqha
        # -875 illqha2.1
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w RGgsn5p 1',
        # 1952 orqha
        # 2096 illqha2.1
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b RGgsn5p 1'
    ]
    return sfen_list


def make_test_dataset():
    sfen_list = get_test_sfen()
    p_index = np.empty((len(sfen_list), 39), dtype=np.int32)
    q_index = np.empty_like(p_index)

    board = cshogi.Board()
    for i, sfen in enumerate(sfen_list):
        board.set_sfen(sfen)
        p, q = get_feature_index(board=board)
        copy_index(source=p, target=p_index[i])
        copy_index(source=q, target=q_index[i])

    dataset = (convert_feature_index(p_index), convert_feature_index(q_index))
    return dataset, len(sfen_list)


def make_nnue_model(parameter_path):
    print(parameter_path)
    d = load_parameter(path=Path(parameter_path))
    d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256])
    d['hidden1_kernel'] = np.reshape(d['hidden1_kernel'], [32, 512]).T
    d['hidden2_kernel'] = np.reshape(d['hidden2_kernel'], [32, 32]).T
    d['hidden3_kernel'] = np.reshape(d['hidden3_kernel'], [1, 32]).T

    nnue = NNUE(d)
    return nnue


def make_distillation_dataset(data_path):
    data = np.fromfile(data_path, dtype=cshogi.PackedSfenValue)
    board = cshogi.Board()

    def generator(n):
        for d in data[n::FLAGS.threads]:
            board.set_psfen(d['sfen'])
            # vanilla
            vanilla_p, vanilla_q = get_vanilla_index(board=board)
            # lite
            lite_p, lite_q = get_lite_index(board=board)

            yield (vanilla_p, vanilla_q), (lite_p, lite_q)

            # 折角なので、最善手と思われる指し手で進めた局面も学習する
            board.move_from_psv(d['move'])
            # vanilla
            vanilla_p, vanilla_q = get_vanilla_index(board=board)
            # lite
            lite_p, lite_q = get_lite_index(board=board)

            yield (vanilla_p, vanilla_q), (lite_p, lite_q)

    def _make_dataset(n):
        lite_type = {'fu': tf.int32, 'ky': tf.int32, 'ke': tf.int32,
                     'gi': tf.int32, 'ka': tf.int32, 'hi': tf.int32,
                     'ki': tf.int32, 'ou': tf.int32}
        tmp = tf.TensorShape((4,))
        lite_shape = {
            'fu': tf.TensorShape((18,)), 'ky': tmp, 'ke': tmp, 'gi': tmp,
            'ka': tf.TensorShape((2,)), 'hi': tf.TensorShape((2,)), 'ki': tmp,
            'ou': tf.TensorShape((1,))
        }
        dataset = tf.data.Dataset.from_generator(
            generator=generator,
            output_types=((tf.int32, tf.int32), (lite_type, lite_type)),
            output_shapes=((tf.TensorShape((38,)), tf.TensorShape((38,))),
                           (lite_shape, lite_shape)),
            args=(n,)
        )
        return dataset

    ds = (tf.data.Dataset.range(FLAGS.threads)
          .interleave(_make_dataset, cycle_length=FLAGS.threads,
                      num_parallel_calls=tf.data.experimental.AUTOTUNE)
          .batch(FLAGS.batch_size)
          .prefetch(tf.data.experimental.AUTOTUNE))

    return ds, len(data)


def get_vanilla_index(board):
    p, q, fv38p, fv38q = fv40(board=board)
    fv38p = np.asarray(fv38p, dtype=np.int32)
    fv38q = np.asarray(fv38q, dtype=np.int32)

    size = 1548
    p_index = fv38p + p * size
    q_index = fv38q + q * size
    return p_index, q_index


def get_lite_index(board):
    p_feature, q_feature = get_feature_index(board=board)

    size = 361
    data_list = []
    for feature in (p_feature, q_feature):
        data = np.empty((39,), dtype=np.int32)
        data[0:18] = feature.fu
        data[18:22] = feature.ky
        data[22:26] = feature.ke
        data[26:30] = feature.gi
        data[30:32] = feature.ka
        data[32:34] = feature.hi
        data[34:38] = feature.ki
        data[38] = feature.ou
        data += feature.ou_square * size
        d = {'fu': data[0:18], 'ky': data[18:22], 'ke': data[22:26],
             'gi': data[26:30], 'ka': data[30:32], 'hi': data[32:34],
             'ki': data[34:38], 'ou': data[38:39]}
        data_list.append(d)

    return data_list


def load_dataset(record_dir):
    if FLAGS.multi:
        global_batch_size = FLAGS.batch_size * 2
    else:
        global_batch_size = FLAGS.batch_size

    files = tf.io.gfile.glob('{}/*.tfrecord'.format(record_dir))
    dataset = tf.data.TFRecordDataset(
        filenames=files, compression_type='GZIP', num_parallel_reads=10
    ).shuffle(10 * FLAGS.batch_size).map(
        parse, num_parallel_calls=FLAGS.threads
    ).batch(global_batch_size).prefetch(tf.data.experimental.AUTOTUNE)

    size = 100000000 * 2 * len(files)
    return dataset, size


def parse(serialized):
    features = {
        'teacher_p': tf.io.FixedLenFeature([38], tf.int64),
        'teacher_q': tf.io.FixedLenFeature([38], tf.int64),

        'student_p/fu': tf.io.FixedLenFeature([18], tf.int64),
        'student_p/ky': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ke': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/gi': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ka': tf.io.FixedLenFeature([2], tf.int64),
        'student_p/hi': tf.io.FixedLenFeature([2], tf.int64),
        'student_p/ki': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ou': tf.io.FixedLenFeature([1], tf.int64),

        'student_q/fu': tf.io.FixedLenFeature([18], tf.int64),
        'student_q/ky': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ke': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/gi': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ka': tf.io.FixedLenFeature([2], tf.int64),
        'student_q/hi': tf.io.FixedLenFeature([2], tf.int64),
        'student_q/ki': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ou': tf.io.FixedLenFeature([1], tf.int64)
    }
    example = tf.io.parse_single_example(serialized, features)

    vanilla_p, vanilla_q = example['teacher_p'], example['teacher_q']
    lite_p = {key[-2:]: value for key, value in example.items()
              if 'student_p' in key}
    lite_q = {key[-2:]: value for key, value in example.items()
              if 'student_q' in key}

    return (vanilla_p, vanilla_q), (lite_p, lite_q)


def distillate():
    # dataset, data_size = make_distillation_dataset(data_path=FLAGS.data_path)
    dataset, data_size = load_dataset(record_dir=FLAGS.record_dir)
    test_dataset, test_size = make_test_dataset()

    model_dir = Path(FLAGS.model_dir)

    if FLAGS.multi:
        train_multi(model_dir=model_dir, train_dataset=dataset,
                    train_size=data_size, test_dataset=test_dataset,
                    test_size=test_size)
    else:
        train_single(model_dir=model_dir, train_dataset=dataset,
                     train_size=data_size, test_dataset=test_dataset,
                     test_size=test_size)


def train_single(model_dir, train_dataset, train_size, test_dataset,
                 test_size):
    nnue_lite = NNUELite()
    nnue_vanilla = make_nnue_model(parameter_path=FLAGS.nnue_parameter)
    optimizer = AdaBeliefOptimizer(learning_rate=5e-7)
    # optimizer = tf.keras.optimizers.SGD(learning_rate=1e-3)

    # 単純にabsolute errorでもいい気はする
    criterion = tf.keras.losses.LogCosh()
    metrics = tf.keras.metrics.LogCoshError()

    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=nnue_lite, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    @tf.function
    def train_step(data):
        with tf.GradientTape() as tape:
            teacher = tf.stop_gradient(nnue_vanilla(data[0]))
            student = nnue_lite(data[1]) * 600.0
            # スケールを調整した方が良い？
            loss = criterion(y_true=teacher, y_pred=student)
        gradients = tape.gradient(loss, nnue_lite.trainable_variables)
        optimizer.apply_gradients(
            zip(gradients, nnue_lite.trainable_variables))

        metrics.update_state(y_true=teacher, y_pred=student)

    log_dir = str(model_dir / 'learn/logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    n = (train_size + FLAGS.batch_size - 1) // FLAGS.batch_size
    saved = False
    for _ in range(FLAGS.epochs):
        for i, d in enumerate(tqdm(train_dataset, total=n)):
            train_step(d)

            saved = False
            if (i + 1) % 100 == 0:
                test_score = tf.squeeze(
                    nnue_lite(test_dataset, training=False) * 600
                )

                with summary_writer.as_default():
                    tf.summary.scalar('loss', metrics.result(),
                                      step=int(ckpt.step))
                    for j in range(test_size):
                        tf.summary.scalar('test/score{}'.format(j),
                                          test_score[j], step=int(ckpt.step))
                metrics.reset_states()

                ckpt.step.assign_add(1)
                manager.save()
                saved = True
    if not saved:
        manager.save()
    export_parameter(model=nnue_lite, output_path=model_dir / 'nn.mpac')


def train_multi(model_dir, train_dataset, train_size, test_dataset, test_size):
    mirrored_strategy = tf.distribute.MirroredStrategy()
    with mirrored_strategy.scope():
        nnue_lite = NNUELite()
        nnue_vanilla = make_nnue_model(parameter_path=FLAGS.nnue_parameter)
        optimizer = AdaBeliefOptimizer(learning_rate=1e-6)
        # optimizer = tf.keras.optimizers.SGD(learning_rate=1e-3)

        # 単純にabsolute errorでもいい気はする
        criterion = tf.keras.losses.LogCosh(
            reduction=tf.keras.losses.Reduction.NONE
        )
        metrics = tf.keras.metrics.LogCoshError()

        # noinspection PyTypeChecker
        ckpt = tf.train.Checkpoint(
            step=tf.Variable(1), network=nnue_lite, optimizer=optimizer
        )
        manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                             max_to_keep=2)
        ckpt.restore(manager.latest_checkpoint).expect_partial()

    global_batch_size = FLAGS.batch_size * 2
    dist_dataset = mirrored_strategy.experimental_distribute_dataset(
        train_dataset
    )

    with mirrored_strategy.scope():
        def step_train(dataset_inputs):
            with tf.GradientTape() as tape:
                teacher = tf.stop_gradient(nnue_vanilla(dataset_inputs[0]))
                student = nnue_lite(dataset_inputs[1]) * 600.0
                # スケールを調整した方が良い？
                loss = tf.nn.compute_average_loss(
                    criterion(y_true=teacher, y_pred=student),
                    global_batch_size=global_batch_size
                )
            gradients = tape.gradient(loss, nnue_lite.trainable_variables)
            optimizer.apply_gradients(zip(
                gradients, nnue_lite.trainable_variables
            ))

            metrics.update_state(y_true=teacher, y_pred=student)
            return loss

        @tf.function
        def distributed_train_step(dataset_inputs):
            per_replica_losses = mirrored_strategy.experimental_run_v2(
                step_train, args=(dataset_inputs,)
            )
            return mirrored_strategy.reduce(
                tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None
            )

    log_dir = str(model_dir / 'learn/logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    n = (train_size + global_batch_size - 1) // global_batch_size
    saved = False
    with mirrored_strategy.scope():
        for _ in range(FLAGS.epochs):
            for i, d in enumerate(tqdm(dist_dataset, total=n)):
                distributed_train_step(d)

                saved = False
                if (i + 1) % 100 == 0:
                    test_score = tf.squeeze(
                        nnue_lite(test_dataset, training=False) * 600
                    )

                    step = int(ckpt.step.values[0])
                    with summary_writer.as_default():
                        tf.summary.scalar('loss', metrics.result(), step=step)
                        for j in range(test_size):
                            tf.summary.scalar('test/score{}'.format(j),
                                              test_score[j], step=step)
                    metrics.reset_states()

                    ckpt.step.assign_add(1)
                    manager.save()
                    saved = True

    if not saved:
        manager.save()

    # 変数の形が違うので、モデルを作り直す
    nnue_lite = NNUELite()
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=nnue_lite)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()
    export_parameter(model=nnue_lite, output_path=model_dir / 'nn.mpac')


def make_tf_record(input_path, output_path):
    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    options = tf.io.TFRecordOptions(compression_type="GZIP")
    with Pool(20) as p:
        with tf.io.TFRecordWriter(str(output_path), options=options) as writer:
            # noinspection PyUnresolvedReferences
            dataset = np.fromfile(input_path, dtype=cshogi.PackedSfenValue)
            args = zip(dataset['sfen'], dataset['move'])
            for record in tqdm(p.imap_unordered(convert_data_wrapper, args),
                               total=len(dataset)):
                writer.write(record[0])
                writer.write(record[1])


def convert_data_wrapper(args):
    return convert_data(*args)


def convert_data(sfen, move):
    board = cshogi.Board()
    board.set_psfen(sfen)

    def make_example(board_):
        # vanilla
        vanilla_p, vanilla_q = get_vanilla_index(board=board_)
        # lite
        lite_p, lite_q = get_lite_index(board=board_)

        feature = {
            'teacher_p': tf.train.Feature(int64_list=tf.train.Int64List(
                value=vanilla_p
            )),
            'teacher_q': tf.train.Feature(int64_list=tf.train.Int64List(
                value=vanilla_q
            ))
        }
        for key, value in lite_p.items():
            feature['student_p/{}'.format(key)] = tf.train.Feature(
                int64_list=tf.train.Int64List(value=value)
            )
        for key, value in lite_q.items():
            feature['student_q/{}'.format(key)] = tf.train.Feature(
                int64_list=tf.train.Int64List(value=value)
            )
        example = tf.train.Example(
            features=tf.train.Features(feature=feature)
        )

        return example.SerializeToString()

    s = make_example(board)
    board.push_psv(move)
    t = make_example(board)

    return s, t


def nnue_test():
    """
    NNUEのパラメータの初期化が正しく行えているか確認
    :return:
    """
    nnue = make_nnue_model(parameter_path=FLAGS.nnue_parameter)

    sfen_list = get_test_sfen()
    p_index = np.empty((len(sfen_list), 38), dtype=np.int32)
    q_index = np.empty_like(p_index)

    for i, sfen in enumerate(sfen_list):
        board = cshogi.Board(sfen)
        p, q = get_vanilla_index(board=board)
        p_index[i] = p
        q_index[i] = q
    test_dataset = p_index, q_index

    test_score = tf.squeeze(nnue(test_dataset, training=False))
    print(test_score.numpy())


def load_rank_dataset(record_dir):
    files = tf.io.gfile.glob('{}/*.tfrecord'.format(record_dir))
    dataset = tf.data.TFRecordDataset(
        filenames=files, compression_type='GZIP', num_parallel_reads=10
    ).shuffle(100 * FLAGS.batch_size).map(
        parse_rank, num_parallel_calls=FLAGS.threads
    ).batch(FLAGS.batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def parse_rank(serialized):
    features = {
        'teacher_p/fu': tf.io.FixedLenFeature([18], tf.int64),
        'teacher_p/ky': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_p/ke': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_p/gi': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_p/ka': tf.io.FixedLenFeature([2], tf.int64),
        'teacher_p/hi': tf.io.FixedLenFeature([2], tf.int64),
        'teacher_p/ki': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_p/ou': tf.io.FixedLenFeature([1], tf.int64),

        'teacher_q/fu': tf.io.FixedLenFeature([18], tf.int64),
        'teacher_q/ky': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_q/ke': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_q/gi': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_q/ka': tf.io.FixedLenFeature([2], tf.int64),
        'teacher_q/hi': tf.io.FixedLenFeature([2], tf.int64),
        'teacher_q/ki': tf.io.FixedLenFeature([4], tf.int64),
        'teacher_q/ou': tf.io.FixedLenFeature([1], tf.int64),

        'student_p/fu': tf.io.FixedLenFeature([18], tf.int64),
        'student_p/ky': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ke': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/gi': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ka': tf.io.FixedLenFeature([2], tf.int64),
        'student_p/hi': tf.io.FixedLenFeature([2], tf.int64),
        'student_p/ki': tf.io.FixedLenFeature([4], tf.int64),
        'student_p/ou': tf.io.FixedLenFeature([1], tf.int64),

        'student_q/fu': tf.io.FixedLenFeature([18], tf.int64),
        'student_q/ky': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ke': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/gi': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ka': tf.io.FixedLenFeature([2], tf.int64),
        'student_q/hi': tf.io.FixedLenFeature([2], tf.int64),
        'student_q/ki': tf.io.FixedLenFeature([4], tf.int64),
        'student_q/ou': tf.io.FixedLenFeature([1], tf.int64),

        'score': tf.io.FixedLenFeature([1], tf.float32)
    }
    example = tf.io.parse_single_example(serialized, features)

    teacher_p = {key[-2:]: value for key, value in example.items()
                 if 'teacher_p' in key}
    teacher_q = {key[-2:]: value for key, value in example.items()
                 if 'teacher_q' in key}
    student_p = {key[-2:]: value for key, value in example.items()
                 if 'student_p' in key}
    student_q = {key[-2:]: value for key, value in example.items()
                 if 'student_q' in key}

    return (teacher_p, teacher_q), (student_p, student_q), example['score']


def make_rank_dataset(data_path):
    data = load_data(data_path=data_path)

    def generator():
        with Pool(20) as p:
            for teacher, students, score in p.imap_unordered(
                    convert_rank_pair, data):
                for s in students:
                    yield teacher, s, score

    types = {'fu': tf.int32, 'ky': tf.int32, 'ke': tf.int32, 'gi': tf.int32,
             'ka': tf.int32, 'hi': tf.int32, 'ki': tf.int32, 'ou': tf.int32}
    shapes = {'fu': tf.TensorShape((18,)), 'ky': tf.TensorShape((4,)),
              'ke': tf.TensorShape((4,)), 'gi': tf.TensorShape((4,)),
              'ka': tf.TensorShape((2,)), 'hi': tf.TensorShape((2,)),
              'ki': tf.TensorShape((4,)), 'ou': tf.TensorShape((1,))}
    dataset = tf.data.Dataset.from_generator(
        generator=generator, output_types=(types, types, tf.float32),
        output_shapes=(shapes, shapes, tf.TensorShape([]))
    ).shuffle(100 * FLAGS.batch_size).batch(FLAGS.batch).prefetch(
        tf.data.experimental.AUTOTUNE
    )

    return dataset


def make_rank_record(input_path, output_path):
    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    n = 1000000
    options = tf.io.TFRecordOptions(compression_type="GZIP")
    with Pool(20) as p:
        with tf.io.TFRecordWriter(str(output_path), options=options) as writer:
            # noinspection PyUnresolvedReferences
            dataset = np.fromfile(input_path, dtype=cshogi.PackedSfenValue,
                                  count=n, offset=40 * n * 9)
            for records in tqdm(p.imap_unordered(convert_rank_pair_wrapper,
                                                 dataset),
                                total=len(dataset)):
                for r in records:
                    writer.write(r)


def convert_rank_pair(data):
    board = cshogi.Board()
    board.set_psfen(data['sfen'])

    best_move = board.push_psv(data['move'])
    teacher_index = get_lite_index(board=board)
    board.pop(best_move)

    data_list = []
    for m in board.legal_moves:
        if m == best_move:
            continue
        board.push(m)
        student_index = get_lite_index(board=board)
        board.pop(m)

        data_list.append(student_index)

    return teacher_index, data_list, float(data['score'])


def convert_rank_pair_wrapper(data):
    teacher, students, score = convert_rank_pair(data)

    def make_example(teacher_, student_, score_):
        # tf.train.Featuresを入れ子にすれば構造をそのまま保てそうだが、
        # 読み込みもあるのでよくわからない

        feature = {}
        for tag, indices in zip(('p', 'q'), teacher_):
            for key, value in indices.items():
                feature['teacher_{}/{}'.format(tag, key)] = tf.train.Feature(
                    int64_list=tf.train.Int64List(value=value)
                )
        for tag, indices in zip(('p', 'q'), student_):
            for key, value in indices.items():
                feature['student_{}/{}'.format(tag, key)] = tf.train.Feature(
                    int64_list=tf.train.Int64List(value=value)
                )
        feature['score'] = tf.train.Feature(
            float_list=tf.train.FloatList(value=[score_])
        )

        example = tf.train.Example(features=tf.train.Features(feature=feature))
        return example.SerializeToString()

    examples = [make_example(teacher, s, score) for s in students]
    return examples


def learn_rank():
    model_dir = Path(FLAGS.model_dir)

    nnue_lite = NNUELite()
    # optimizer = AdaBeliefOptimizer(learning_rate=1e-8)
    optimizer = tf.keras.optimizers.SGD(learning_rate=1e-6)

    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=nnue_lite, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    metrics = tf.metrics.Mean('loss_total')
    metrics_ce = tf.metrics.Mean('loss_ce')
    metrics_rank = tf.metrics.Mean('loss_rank')

    margin = tf.constant(0.01, dtype=tf.float32)

    @tf.function
    def train_step(data):
        score = data[-1] / 600
        p = tf.math.sigmoid(score)

        with tf.GradientTape() as tape:
            teacher = -nnue_lite(data[0])
            student = -nnue_lite(data[1])

            ce = tf.nn.sigmoid_cross_entropy_with_logits(
                labels=p, logits=teacher
            )
            rank = tf.nn.relu(student - score + margin)

            loss = (FLAGS.kappa * tf.math.reduce_mean(ce) +
                    tf.math.reduce_mean(rank))
        gradients = tape.gradient(loss, nnue_lite.trainable_variables)
        optimizer.apply_gradients(
            zip(gradients, nnue_lite.trainable_variables))

        metrics.update_state(loss)
        metrics_ce.update_state(ce)
        metrics_rank.update_state(rank)

    log_dir = str(model_dir / 'learn/logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    # train_dataset = make_rank_dataset(data_path=FLAGS.data_path)
    train_dataset = load_rank_dataset(record_dir=FLAGS.record_dir)
    test_dataset, test_size = make_test_dataset()

    n = (10000000 * 80 + FLAGS.batch_size - 1) // FLAGS.batch_size
    saved = False
    for _ in range(FLAGS.epochs):
        for i, d in enumerate(tqdm(train_dataset, total=n)):
            train_step(d)

            saved = False
            if (i + 1) % 1000 == 0:
                test_score = tf.squeeze(
                    nnue_lite(test_dataset, training=False) * 600
                )

                with summary_writer.as_default():
                    tf.summary.scalar('loss', metrics.result(),
                                      step=int(ckpt.step))
                    tf.summary.scalar('loss/cross_entropy',
                                      metrics_ce.result(), step=int(ckpt.step))
                    tf.summary.scalar('loss/rank', metrics_rank.result(),
                                      step=int(ckpt.step))
                    for j in range(test_size):
                        tf.summary.scalar('test/score{}'.format(j),
                                          test_score[j], step=int(ckpt.step))
                metrics.reset_states()
                metrics_ce.reset_states()
                metrics_rank.reset_states()

                ckpt.step.assign_add(1)
                manager.save()
                saved = True
    if not saved:
        manager.save()
    export_parameter(model=nnue_lite, output_path=model_dir / 'nn.mpac')


def main(_):
    if FLAGS.mode == 'teacher_pv':
        make_teacher_pv(FLAGS.teacher_pv)
        return
    elif FLAGS.mode == 'initialize':
        initialize()
        return
    elif FLAGS.mode == 'nnue_test':
        nnue_test()
        return
    elif FLAGS.mode == 'distillate':
        distillate()
        return
    elif FLAGS.mode == 'make_record':
        make_tf_record(input_path=FLAGS.data_path,
                       output_path=FLAGS.record_path)
        return
    elif FLAGS.mode == 'learn_rank':
        learn_rank()
        return
    elif FLAGS.mode == 'make_rank_record':
        make_rank_record(input_path=FLAGS.data_path,
                         output_path=FLAGS.record_path)
        return

    model_dir = Path(FLAGS.model_dir)

    teacher_pv = joblib.load(FLAGS.teacher_pv)

    model = NNUELite()
    # optimizer = AdaBeliefOptimizer(learning_rate=1e-3)
    optimizer = tf.keras.optimizers.SGD(learning_rate=1e-3)
    # optimizer = tfa.optimizers.Yogi(learning_rate=1e-3)
    metrics = tf.keras.metrics.Mean('loss')
    metrics_ce = tf.keras.metrics.Mean('loss_ce')
    metrics_rank = tf.keras.metrics.Mean('loss_rank')

    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=model, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    test_dataset, test_size = make_test_dataset()

    @tf.function
    def train_step(dataset_inputs):
        with tf.GradientTape() as tape:
            teacher = model((dataset_inputs['teacher_p_index'],
                             dataset_inputs['teacher_q_index']))
            # 教師局面について教師あり学習
            loss_bce = tf.nn.sigmoid_cross_entropy_with_logits(
                labels=dataset_inputs['teacher_score'], logits=teacher
            )
            teacher = tf.squeeze(teacher * dataset_inputs['teacher_sign'])

            # 読み筋が一致するようにランク学習
            student = model((dataset_inputs['student_p_index'],
                             dataset_inputs['student_q_index']))
            student = tf.squeeze(student * dataset_inputs['student_sign'])

            # loss_rank = tf.nn.elu(student - teacher)
            # loss_rank = (
            #     tf.math.exp(student - tf.stop_gradient(teacher)) +
            #     tf.math.exp(tf.stop_gradient(student) - teacher)
            # ) * 0.5
            loss_rank = tf.math.exp(student - tf.stop_gradient(teacher))
            loss = (tf.math.reduce_mean(loss_bce) +
                    FLAGS.kappa * tf.math.reduce_mean(loss_rank))
            mean_loss = tf.math.reduce_mean(loss)
        gradients = tape.gradient(mean_loss, model.trainable_variables)
        optimizer.apply_gradients(zip(
            gradients, model.trainable_variables
        ))

        metrics.update_state(mean_loss)
        metrics_ce.update_state(loss_bce)
        metrics_rank.update_state(loss_rank)
        return mean_loss

    log_dir = str(model_dir / 'learn/logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    data_path = model_dir.parent / 'tmp_data.pickle'
    for _ in tqdm(range(FLAGS.repeat)):
        if data_path.exists():
            training_data = joblib.load(data_path)
        else:
            training_data = get_training_features(pv_data_list=teacher_pv,
                                                  depth=FLAGS.student_depth)
            training_data = convert_training_pair(training_pair=training_data)
            joblib.dump(training_data, data_path)

        dataset = make_dataset(training_data=training_data,
                               batch_size=FLAGS.batch_size)

        for _ in range(5):
            for i, inputs in enumerate(dataset):
                train_step(inputs)

            test_score = tf.squeeze(model(test_dataset, training=False) * 600)

            with summary_writer.as_default():
                tf.summary.scalar('loss', metrics.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('loss/cross_entropy', metrics_ce.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('loss/rank', metrics_rank.result(),
                                  step=int(ckpt.step))
                for i in range(test_size):
                    tf.summary.scalar('test/score{}'.format(i), test_score[i],
                                      step=int(ckpt.step))
            metrics.reset_states()
            metrics_ce.reset_states()
            metrics_rank.reset_states()

            ckpt.step.assign_add(1)
            manager.save()

        name = model_dir / 'nn{}.mpac'.format(int(ckpt.step))
        export_parameter(model=model, output_path=name)

        # with grpc.insecure_channel('localhost:50051') as channel:
        #     stub = remote_usi_pb2_grpc.LearnerStub(channel=channel)
        #     result = stub.UpdateParameter(
        #         remote_usi_pb2.ParamterPath(path=str(name.absolute()))
        #     )
        #     if not result.update_result:
        #         print('something strange')
        #         break


if __name__ == '__main__':
    app.run(main)
