#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import cshogi
import joblib
import numpy as np
import tensorflow as tf
from scipy.special import logit
from tqdm import tqdm

from training_in_gym1 import fv40

__author__ = 'Yasuhiro'
__date__ = '2019/12/15'

MAX_MOVE_FLAG = 100


class Tree(object):
    dtype = np.dtype([('p', np.int32, (38,)), ('q', np.int32, (38,)),
                      ('state', np.uint8, (32,)), ('done', np.bool),
                      ('result', np.int8), ('info', np.int8),
                      ('progress', np.float32),
                      ('score', np.float32), ('score_flag', np.bool)])

    def __init__(self, capacity):
        self._capacity = capacity  # 最大の容量
        self._size = 0  # 現在のサンプル数
        self.priority = np.zeros(2 * capacity - 1)

        # 引き分けの場合、infoが0~3はcshogiの定数と同じ
        # 100はmax moveとする
        self.data = np.empty(capacity, dtype=self.dtype)

    def _shrink(self, threshold):
        """
        閾値以下のデータを捨てる
        :param threshold:
        :return:
        """
        base = self._capacity - 1
        priority = self.priority[base:]
        flag = priority > threshold

        new_data = self.data[flag]
        new_priority = priority[flag].copy()

        n = len(new_data)
        self.data[:n] = new_data

        # 全体を初期化
        self.priority[:] = 0
        self.priority[base:base + n] = new_priority

        for i in range(base, base + n):
            self._update(i)

        self._size = n

    def shrink(self):
        base = self._capacity - 1
        while True:
            threshold = np.mean(self.priority[base:base + self._size])
            self._shrink(threshold)
            if self._size < self._capacity // 2:
                break

    def _update(self, index):
        while index != 0:
            # 奇数ならそのまま、偶数なら1を引く
            i = index - (1 - (index & 0x1))
            # 親のインデックス
            index = (index - 1) // 2

            self.priority[index] = np.sum(self.priority[i:i + 2])

    def add_batch(self, buffer, size):
        self.data[self._size:self._size + size] = buffer[:size]

        base = self._capacity - 1
        begin = base + self._size
        end = begin + size
        if self._size == 0:
            v = 100  # 適当な大きい値
        else:
            # 最大値を探す
            v = np.max(self.priority[base:begin])
        self.priority[begin:end] = v
        for i in range(begin, end, 2):
            # 更新はleftかrightのどちらか一つで十分
            self._update(index=i)
        if end & 0x1 == 0:
            # endが偶数ということは範囲の最後は奇数
            # 奇数ということはノードはleft
            # さっきのループでは更新できていない
            self._update(index=end - 1)

        self._size += size

    def update(self, index, priority):
        self.priority[index] = priority
        self._update(index)

    def reset_score_flag(self):
        self.data['score_flag'] = False

    @property
    def size(self):
        return self._size

    @property
    def capacity(self):
        return self._capacity


class Memory(object):
    def __init__(self, capacity, target_worker, ponanza_constant, elmo_lambda):
        self.tree = Tree(capacity)
        # 目標値の計算をするためのオブジェクト
        self.target_worker = target_worker

        self.board = cshogi.Board()
        # noinspection PyTypeChecker
        self.result_value = (
            0, logit(0.99) * ponanza_constant, logit(0.01) * ponanza_constant
        )

        self.elmo_lambda = elmo_lambda

    def load(self, path):
        self.tree = joblib.load(path)

    def dump(self, path):
        joblib.dump(self.tree, path)

    def add_batch(self, buffer, size):
        self.tree.add_batch(buffer=buffer, size=size)

    def sample(self, n):
        """
        n個のサンプルを取り出す
        目標値の計算をした上でサンプルを返す

        :param n:
        :return:
        """
        indices = self._sample_indices(n)
        (p_list, q_list), target_list = self._make_dataset(indices)

        return indices, ((p_list, q_list), target_list)

    def _make_dataset(self, indices):
        n = len(indices)

        p_list = np.empty([n, 38], dtype=np.int32)
        q_list = np.empty_like(p_list)
        target_list = np.empty(n, dtype=np.float32)

        for i, j in enumerate(indices):
            data = self.tree.data[j - (self.tree.capacity - 1)]

            # 自分の局面から言って進んだ状態なので、局面は相手の手番
            p_list[i] = data['p']
            q_list[i] = data['q']
            target_list[i] = self._make_next_value(data=data)

        return (p_list, q_list), target_list

    def _make_next_value(self, data):
        if data['score_flag']:
            return data['score']

        data['score_flag'] = True
        result_value = (data['result'] + 1) * 0.5
        if data['done']:
            if data['info'] != MAX_MOVE_FLAG:
                data['score'] = result_value
                return result_value

        v = tf.math.sigmoid(self._compute_next_value(state=data['state']))
        target = (self.elmo_lambda * result_value * data['progress'] +
                  (1 - self.elmo_lambda) * v)
        data['score'] = target

        return target

    def compute_cost(self, behavior_model):
        @tf.function
        def _evaluate(x, y):
            behavior, _ = behavior_model(x)

            p = tf.reshape(y, [-1, 1])
            y_pred = tf.clip_by_value(p, clip_value_min=0.01,
                                      clip_value_max=0.99)

            cross_entropy = tf.squeeze(
                tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=p, logits=behavior
                )
            )
            entropy = tf.keras.losses.binary_crossentropy(
                y_true=p, y_pred=y_pred, from_logits=False
            )
            loss = cross_entropy - entropy
            return loss

        data = self.tree.data[:self.tree.size]
        for d in tqdm(data, desc='computing target values'):
            self._make_next_value(data=d)

        batch_size = 1000
        index_list = np.arange(self.tree.size) + self.tree.capacity - 1
        dataset = tf.data.Dataset.from_tensor_slices((
            index_list, (data['p'], data['q']), data['score']
        )).batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
        n = (self.tree.size + batch_size - 1) // batch_size
        for indices, inputs, targets in tqdm(dataset, total=n,
                                             desc='updating priority'):
            costs = _evaluate(inputs, targets)
            self.update(indices=indices.numpy(), costs=costs.numpy())

    def _sample_indices(self, n):
        """
        インデックスを選び出す
        :param n:
        :return:
        """
        indices = np.empty(n, dtype=np.int32)
        for i in range(n):
            parent = 0
            v = np.random.uniform(0, self.tree.priority[0])
            while True:
                left = parent * 2 + 1
                if v > self.tree.priority[left]:
                    # 右側
                    right = left + 1
                    if left >= self.tree.capacity - 1:
                        indices[i] = right
                        break
                    v -= self.tree.priority[left]
                    parent = right
                else:
                    # 左側
                    if left >= self.tree.capacity - 1:
                        indices[i] = left
                        break
                    parent = left
        # インデックスが範囲内に収まっているか確認
        # 演算誤差で範囲外になる場合があるみたい
        begin = self.tree.capacity - 1
        end = begin + self.tree.size
        indices = np.clip(indices, a_min=begin, a_max=end - 1)

        return indices

    def _compute_next_value(self, state):
        # stateは自分の局面
        self.board.set_psfen(state)

        legal_moves = self.board.legal_moves
        n = len(legal_moves)

        p_list = np.empty([n, 38], dtype=np.int32)
        q_list = np.empty_like(p_list)
        for i, m in enumerate(legal_moves):
            self.board.push(m)
            p, q, p_index, q_index = fv40(board=self.board)
            self.board.pop(m)

            p_list[i] = p_index
            p_list[i] += p
            q_list[i] = q_index
            q_list[i] += q

        target_value = compute_target_value(
            tf.convert_to_tensor(p_list), tf.convert_to_tensor(q_list),
            self.target_worker
        )
        return target_value

    def update(self, indices, costs):
        """
        サンプルの誤差を更新

        :param indices:
        :param costs:
        :return:
        """
        for i, c in zip(indices, costs):
            self.tree.update(i, c)

    def shrink(self):
        return self.tree.shrink()

    def reset_score_flag(self):
        self.tree.reset_score_flag()

    @property
    def size(self):
        return self.tree.size

    @property
    def capacity(self):
        return self.tree.capacity


@tf.function(experimental_relax_shapes=True)
def compute_target_value(p_list, q_list, worker):
    scores = worker(p_list, q_list)
    # 自分は相手の評価値が最小になる局面に誘導
    target_value = tf.reduce_min(scores)
    return target_value
