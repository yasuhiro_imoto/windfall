#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
テスト用にランダムなsfenを生成する
"""

import random
import json
from pathlib import Path

import cshogi
from absl import flags, app
from tqdm import trange

__author__ = 'Yasuhiro'
__date__ = '2020/05/28'

FLAGS = flags.FLAGS
flags.DEFINE_string('output_path', default=None, help='')
flags.DEFINE_integer('n_samples', default=100, help='')


def reorder_hand(hand):
    """
    持ち駒の金の順序を最後にする
    :param hand:
    :return:
    """
    tmp = hand[4]
    hand[4:6] = hand[5:7]
    hand[6] = tmp
    return hand


def generate_random_position():
    """
    ランダムに局面だけを生成する
    :return:
    """

    data_list = []
    for _ in trange(FLAGS.n_samples):
        board = cshogi.Board()
        # ランダムに動かす手数
        n = random.randint(1, 512)
        for _ in range(n):
            moves = list(board.legal_moves)
            if len(moves) == 0:
                break
            m = random.choice(moves)
            board.push(m)

        black_hand, white_hand = board.pieces_in_hand
        # 金の順序を最後に移動する
        black_hand = reorder_hand(black_hand)
        white_hand = reorder_hand(white_hand)

        # 入力とその時の駒の並び
        d = {'sfen': board.sfen(),
             'board': board.pieces, 'hand': (black_hand, white_hand)}
        data_list.append(d)

    path = Path(FLAGS.output_path)
    if not path.parent.exists():
        path.parent.mkdir(parents=True)
    with path.open('w') as f:
        json.dump(data_list, f, indent=4)


def main(_):
    generate_random_position()


if __name__ == '__main__':
    app.run(main)
