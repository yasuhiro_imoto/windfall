#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import struct
from pathlib import Path

import msgpack
import numpy as np
from absl import app, flags

__author__ = 'Yasuhiro'
__date__ = '2020/11/14'

FLAGS = flags.FLAGS
flags.DEFINE_string('mpac_path', default='', help='')
flags.DEFINE_string('output_path', default='', help='')
flags.DEFINE_string('sample_path', default='',
                    help='ハッシュ値などを正しく設定するための見本')


def load_parameter(path):
    with path.open('rb') as f:
        msg_list = []
        for msg in msgpack.Unpacker(f, raw=True):
            msg_list.append(msg)
    return msg_list


def convert_parameter(sample_path, output_path, parameters):
    """
    パラメータをnn.binの形式で保存する
    ハッシュ値などは予め用意したダミーのnn.binから取得する
    :param sample_path:
    :param output_path:
    :param parameters:
    :return:
    """
    with sample_path.open('rb') as f:
        with output_path.open('wb') as g:
            #
            # header
            #
            version = struct.unpack('<I', f.read(4))[0]
            hash_value1 = struct.unpack('<I', f.read(4))[0]

            size = struct.unpack('<I', f.read(4))[0]
            model_architecture = struct.unpack(
                '<{}s'.format(size), f.read(size)
            )[0]

            g.write(struct.pack('<I', version))
            g.write(struct.pack('<I', hash_value1))

            g.write(struct.pack('<I', size))
            g.write(struct.pack('{}s'.format(size), model_architecture))

            #
            # embedded feature
            #
            hash_value2 = struct.unpack('<I', f.read(4))[0]

            g.write(struct.pack('<I', hash_value2))

            # パラメータをスキップ
            f.read(256 * 2)
            f.read(32 * 81 * 361 * 2)

            g.write(parameters['feature_bias'])
            g.write(parameters['feature_kernel'])

            #
            # dense nets
            #
            hash_value3 = struct.unpack('<I', f.read(4))[0]

            g.write(struct.pack('<I', hash_value3))

            # パラメータをスキップ
            f.read(512 * 4)
            f.read(512 * 32 * 1)
            f.read(32 * 4)
            f.read(32 * 32 * 1)
            f.read(1 * 4)
            f.read(32 * 1 * 1)

            for i in range(1, 4):
                g.write(parameters['hidden{}_bias'.format(i)])
                g.write(parameters['hidden{}_kernel'.format(i)])


def main(_):
    d = load_parameter(path=Path(FLAGS.mpac_path))
    hidden3 = d[2]
    # activation layerの分も遡らなくてはいけない
    hidden2 = hidden3[b'previous'][b'previous']
    hidden1 = hidden2[b'previous'][b'previous']
    parameters = {
        'feature_bias': d[1][b'bias'], 'feature_kernel': d[1][b'kernel'],
        'hidden1_bias': hidden1[b'bias'], 'hidden1_kernel': hidden1[b'kernel'],
        'hidden2_bias': hidden2[b'bias'], 'hidden2_kernel': hidden2[b'kernel'],
        'hidden3_bias': hidden3[b'bias'], 'hidden3_kernel': hidden3[b'kernel']
    }

    convert_parameter(sample_path=Path(FLAGS.sample_path),
                      output_path=Path(FLAGS.output_path),
                      parameters=parameters)


if __name__ == '__main__':
    app.run(main)
