#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import unittest

import cshogi
import numpy as np

from feature_lite import get_feature_index

__author__ = 'Yasuhiro'
__date__ = '2020/10/17'


class TestFeatureList(unittest.TestCase):
    @staticmethod
    def decode_index(b_index, w_index):
        p_board = np.zeros(81, dtype=np.int32)
        p_hand = np.zeros((2, 7), dtype=np.int32)

        q_board = np.zeros_like(p_board)
        q_hand = np.zeros_like(p_hand)

        p_board[b_index.ou_square] = cshogi.BKING
        for i in range(1, 8):
            for index in b_index[i]:
                j, square = divmod(index - 1, 81)
                if j >= 4:
                    p_hand[int(square >= 18)][i - 1] += 1
                else:
                    p_board[square] = i + 8 * j
        p_board[(b_index.ou - 1) % 81] = cshogi.WKING

        q_board[80 - w_index.ou_square] = cshogi.WKING
        for i in range(1, 8):
            for index in w_index[i]:
                j, square = divmod(index - 1, 81)
                if j >= 4:
                    q_hand[int(square < 18)][i - 1] += 1
                else:
                    # blackとwhiteを反転
                    q_board[80 - square] = (i + 8 * j) ^ 0x10
        q_board[80 - (w_index.ou - 1) % 81] = cshogi.BKING

        return p_board, p_hand, q_board, q_hand

    def test_index(self):
        board = cshogi.Board()
        p_index, q_index = get_feature_index(board=board)

        if board.turn == cshogi.BLACK:
            b_index, w_index = p_index, q_index
        else:
            b_index, w_index = q_index, p_index
        b_board, b_hand, w_board, w_hand = self.decode_index(b_index=b_index,
                                                             w_index=w_index)

        for square in range(81):
            with self.subTest(square=square):
                self.assertEqual(b_board[square], board.piece(square))
                self.assertEqual(w_board[square], board.piece(square))
        hand = board.pieces_in_hand
        # 駒の順序が違うので、入れ替え
        order = (0, 1, 2, 3, 6, 4, 5)
        for color in range(2):
            for i in range(7):
                self.assertEqual(b_hand[color][order[i]], hand[color][i])
                self.assertEqual(w_hand[color][order[i]], hand[color][i])

    def test_random(self):
        with open('../data/test_sample_sfen.json') as f:
            sfen_list = json.load(f)

        for count, sfen in enumerate(sfen_list):
            with self.subTest(count=count, sfen=sfen):
                board = cshogi.Board(sfen)

                p_index, q_index = get_feature_index(board=board)
                if board.turn == cshogi.BLACK:
                    b_index, w_index = p_index, q_index
                else:
                    b_index, w_index = q_index, p_index

                b_board, b_hand, w_board, w_hand = self.decode_index(
                    b_index=b_index, w_index=w_index
                )

                for square in range(81):
                    with self.subTest(square=square):
                        self.assertEqual(b_board[square], board.piece(square))
                        self.assertEqual(w_board[square], board.piece(square))
                hand = board.pieces_in_hand
                # 駒の順序が違うので、入れ替え
                order = (0, 1, 2, 3, 6, 4, 5)
                for color in range(2):
                    for i in range(7):
                        self.assertEqual(b_hand[color][order[i]],
                                         hand[color][i])
                        self.assertEqual(w_hand[color][order[i]],
                                         hand[color][i])

    def test_random_all(self):
        data = np.fromfile('../data/x036', dtype=cshogi.PackedSfenValue,
                           count=100000)
        board = cshogi.Board()
        for count, d in enumerate(data):
            if count != 1:
                continue
            result = board.set_psfen(d['sfen'])
            if not result:
                continue
            with self.subTest(count=count):
                p_index, q_index = get_feature_index(board=board)
                if board.turn == cshogi.BLACK:
                    b_index, w_index = p_index, q_index
                else:
                    b_index, w_index = q_index, p_index

                self.assertEqual(len(b_index.fu), 18)
                self.assertEqual(len(b_index.ky), 4)
                self.assertEqual(len(b_index.ke), 4)
                self.assertEqual(len(b_index.gi), 4)
                self.assertEqual(len(b_index.ka), 2)
                self.assertEqual(len(b_index.hi), 2)
                self.assertEqual(len(b_index.ki), 4)

                self.assertEqual(len(w_index.fu), 18)
                self.assertEqual(len(w_index.ky), 4)
                self.assertEqual(len(w_index.ke), 4)
                self.assertEqual(len(w_index.gi), 4)
                self.assertEqual(len(w_index.ka), 2)
                self.assertEqual(len(w_index.hi), 2)
                self.assertEqual(len(w_index.ki), 4)

                self.assertLess(np.max(b_index.fu), 361)
                self.assertLess(np.max(b_index.ky), 361)
                self.assertLess(np.max(b_index.ke), 361)
                self.assertLess(np.max(b_index.gi), 361)
                self.assertLess(np.max(b_index.ka), 361)
                self.assertLess(np.max(b_index.hi), 361)
                self.assertLess(np.max(b_index.ki), 361)
                self.assertLess(np.max(b_index.ou), 361)

                self.assertLess(np.max(w_index.fu), 361)
                self.assertLess(np.max(w_index.ky), 361)
                self.assertLess(np.max(w_index.ke), 361)
                self.assertLess(np.max(w_index.gi), 361)
                self.assertLess(np.max(w_index.ka), 361)
                self.assertLess(np.max(w_index.hi), 361)
                self.assertLess(np.max(w_index.ki), 361)
                self.assertLess(np.max(w_index.ou), 361)

                b_board, b_hand, w_board, w_hand = self.decode_index(
                    b_index=b_index, w_index=w_index
                )

                for square in range(81):
                    self.assertEqual(b_board[square], board.piece(square))
                    self.assertEqual(w_board[square], board.piece(square))
                hand = board.pieces_in_hand
                # 駒の順序が違うので、入れ替え
                order = (0, 1, 2, 3, 6, 4, 5)
                for color in range(2):
                    for i in range(7):
                        self.assertEqual(b_hand[color][order[i]],
                                         hand[color][i])
                        self.assertEqual(w_hand[color][order[i]],
                                         hand[color][i])


if __name__ == '__main__':
    unittest.main()
