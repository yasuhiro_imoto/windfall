#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np

__author__ = 'Yasuhiro'
__date__ = '2020/04/19'


class MyModel(tf.keras.Model):
    def __init__(self):
        super().__init__()

        self.l1 = tf.keras.layers.Dense(units=3)
        self.l2 = tf.keras.layers.Dense(units=1)

    def call(self, inputs, training=None, mask=None):
        h = self.l1(inputs)
        outputs = self.l2(h)
        return outputs


class MyMetrics(tf.keras.metrics.Metric):
    def __init__(self, name='my_max', **kwargs):
        super(MyMetrics, self).__init__(name=name, **kwargs)
        self.m = self.add_weight(
            name='max', initializer=tf.keras.initializers.constant(-1e3)
        )

    def update_state(self, value):
        self.m.assign(tf.math.maximum(self.m, tf.math.reduce_max(value)))

    def result(self):
        return self.m


def main():
    inputs = tf.keras.Input(shape=(5,))
    outputs = MyModel()(inputs)
    # outputs = tf.keras.Sequential([
    #     tf.keras.layers.Dense(units=3),
    #     tf.keras.layers.Dense(units=1)
    # ])(inputs)
    # h = tf.keras.layers.Dense(units=3)(inputs)
    # outputs = tf.keras.layers.Dense(units=1)(h)
    model = tf.keras.Model(inputs, outputs)
    optimizer = tf.keras.optimizers.Adam()

    # functional api
    # v = tf.keras.Model(inputs, [model.layers[1].output, outputs])
    # Sequentialでもできるが、見たい部分の階層が深くなる
    # v = tf.keras.Model(inputs, [model.layers[1].layers[0].output, outputs])
    v = tf.keras.Model(inputs, [model.layers[1].l1.output, outputs])

    my_metrics = MyMetrics()

    @tf.function
    def train_step(x, y):
        with tf.GradientTape() as tape:
            p, q = v(x)
            loss = tf.reduce_mean((y - p) ** 2)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        my_metrics.update_state(q)
        return p, q

    train_x = np.random.randn(10, 5).astype(np.float32)
    train_y = np.random.randn(10, 1).astype(np.float32)

    print(train_step(train_x, train_y))
    print(my_metrics.result())
    print(train_step(train_x, train_y))
    print(my_metrics.result())

    # print(model.summary())
    # print('---')


if __name__ == '__main__':
    main()
