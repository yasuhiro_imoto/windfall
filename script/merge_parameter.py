#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
random_mutation.pyで作り出した二つの重みを混ぜ合わせる
"""

import json
from pathlib import Path

from absl import app, flags
import numpy as np
from tqdm import tqdm

import nn_parameter

__author__ = 'Yasuhiro'
__date__ = '2021/01/10'

FLAGS = flags.FLAGS
flags.DEFINE_string('input_bin1', default='', help='合成のベースのnn.bin')
flags.DEFINE_string('input_bin2', default='', help='追加するnn.bin')
flags.DEFINE_string('original_bin', default='', help='mutationの元')
flags.DEFINE_enum('merge_mode', default='add',
                  enum_values=['add', 'sub', 'min', 'max'],
                  help='合成する時の演算')
flags.DEFINE_string('output_dir', default='', help='')


def get_delta():
    original_parameters = nn_parameter.read(FLAGS.original_bin)
    input2_parameters = nn_parameter.read(FLAGS.input_bin2)

    delta = []
    for original, input2 in zip(original_parameters, input2_parameters):
        d = nn_parameter.Parameter(
            kernel=input2.kernel - original.kernel,
            bias=input2.bias - original.bias
        )
        delta.append(d)

    return delta


def merge(input1, input2, min_value=-128, max_value=127):
    dtype = input1.dtype
    input1 = input1.astype(np.int32)
    if FLAGS.merge_mode == 'add':
        v = input1 + input2
    elif FLAGS.merge_mode == 'sub':
        v = input1 - input2
    else:
        raise NotImplementedError(FLAGS.merge_mode)

    v = np.clip(v, a_min=min_value, a_max=max_value).astype(dtype)
    return v


def merge_parameters(base, delta):
    new_parameters = [
        nn_parameter.Parameter(
            kernel=merge(base[0].kernel, delta[0].kernel,
                         min_value=-2 ** 15, max_value=2 ** 15 - 1),
            bias=merge(base[0].bias, delta[0].bias,
                       min_value=-2 ** 15, max_value=2 ** 15 - 1)
        )
    ]
    for b, d in zip(base[1:], delta[1:]):
        p = nn_parameter.Parameter(
            kernel=merge(b.kernel, d.kernel),
            bias=merge(b.bias, d.bias,
                       min_value=-2 ** 31, max_value=2 ** 31 - 1)
        )
        new_parameters.append(p)

    return new_parameters


def main(_):
    input1_parameters = nn_parameter.read(FLAGS.input_bin1)
    delta = get_delta()

    new_parameters = merge_parameters(base=input1_parameters, delta=delta)

    output_dir = Path(FLAGS.output_dir)
    if not output_dir.exists():
        output_dir.mkdir(parents=True)

    d = {
        'input1': FLAGS.input_bin1, 'input2': FLAGS.input_bin2,
        'original': FLAGS.original_bin, 'merge_mode': FLAGS.merge_mode
    }
    config_path = output_dir / 'config.json'
    with config_path.open('w') as f:
        json.dump(d, f)

    nn_parameter.write(output_dir / 'nn.bin', new_parameters)


if __name__ == '__main__':
    app.run(main)
