#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path

import click
import numpy as np
import gym
import tensorflow as tf
from tqdm import trange, tqdm
from scipy.special import logit
import joblib
import toml
import cshogi

try:
    import sys
    sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane

from wf_gym import make_usi
from nnue import NNUESigmoid
from self_training2 import BonusNetwork
from make_record2 import fv40

__author__ = 'Yasuhiro'
__date__ = '2019/11月/25'


def get_states(board):
    actions = list(board.legal_moves)
    if len(actions) == 0:
        return (None, None), None
    p_index_list = np.empty([len(actions), 38], dtype=np.int32)
    q_index_list = np.empty_like(p_index_list)
    for i, action in enumerate(actions):
        board.push(action)
        p, q, p_index, q_index = fv40(board=board)
        board.pop(action)

        p_index_list[i] = p_index
        p_index_list[i] += p
        q_index_list[i] = q_index
        q_index_list[i] += q
    return (p_index_list, q_index_list), actions


def make_target_dataset(netx_states_list):
    index_list = []
    max_size = 0
    next_p, next_q = [], []
    # next_statesの要素数の累積和
    # 一つ先のインデックスを操作するので、一つ大きいサイズで配列を確保
    counter = np.empty(len(netx_states_list) + 1, dtype=np.int32)
    counter[0] = 0
    for i, next_states in enumerate(netx_states_list):
        n = len(next_states[0])
        counter[i + 1] = n + counter[i]

        index_list.extend((i, j) for j in range(n))
        max_size = max(max_size, n)
        next_p.extend(next_states[0])
        next_q.extend(next_states[1])
    dataset = tf.data.Dataset.from_tensor_slices((next_p, next_q))
    dataset = dataset.batch(1000).prefetch(tf.data.experimental.AUTOTUNE)

    d = {'counter': counter, 'index_list': index_list, 'max_size': max_size}
    return dataset, d


def evaluate_next_states(behavior_network, target_network, bonus_network,
                         next_states_list, config):
    pc = tf.constant(float(config['ponanza_constant']))
    bs = tf.constant(float(config['bonus']))

    # noinspection PyShadowingNames
    @tf.function
    def _evaluate(inputs):
        scores, _ = behavior_network(inputs)
        scores = tf.squeeze(-scores)

        bonuses = bonus_network(inputs)
        values = scores / pc + bs * bonuses
        return values, bonuses

    states_list = next_states_list[1:]
    dataset, tmp = make_target_dataset(netx_states_list=states_list)

    value_list = []
    bonus_list = []
    for inputs in dataset:
        values, bonuses = _evaluate(inputs=inputs)
        value_list.append(values)
        bonus_list.append(bonuses)
    sparse_values = tf.sparse.SparseTensor(
        indices=tmp['index_list'], values=tf.concat(value_list, axis=0),
        dense_shape=(len(states_list), tmp['max_size'])
    )
    dense_values = tf.sparse.to_dense(sparse_values, default_value=1e-6)
    best_indices = tf.argmax(dense_values, axis=1)

    bonus_list = tf.concat(bonus_list, axis=0)

    best_states_p = [
        p[i] for (p, _), i in zip(states_list, best_indices)
    ]
    best_states_q = [
        q[i] for (_, q), i in zip(states_list, best_indices)
    ]
    bonuses = [bonus_list[n + i] for n, i in zip(tmp['counter'], best_indices)]

    p = tf.convert_to_tensor(best_states_p)
    q = tf.convert_to_tensor(best_states_q)
    bonuses = tf.convert_to_tensor(bonuses)
    target_values, _ = target_network([p, q])
    target_values = tf.squeeze(-target_values)

    values = target_values / pc + bs * bonuses

    return values


class SingleEpisode(object):
    # noinspection PyTypeChecker
    result_value = (0, logit(0.99) * 600, logit(0.01) * 600)

    def __init__(self):
        self.p, self.q = [], []
        self.next_state = []

        self.result = None
        self.record = None
        self.draw = None

    def add(self, index, state_list):
        self.p.append(state_list[0][index])
        self.q.append(state_list[1][index])
        self.next_state.append(state_list)

    def make_pair(self, behavior_network, target_network, bonus_network,
                  config):
        target_values = evaluate_next_states(
            behavior_network=behavior_network, target_network=target_network,
            bonus_network=bonus_network, next_states_list=self.next_state,
            config=config
        )
        if self.result == 0:
            if self.draw:
                # 千日手で引き分け
                t = tf.concat((target_values, [self.result_value[0]]),
                              axis=0)
                return (self.p, self.q), t
            else:
                # max moveに到達した
                # 最後の指し手の次の局面がないので、最後の差し手を削る
                return (self.p[:-1], self.q[:-1]), target_values
        else:
            r = self.result_value[self.result]
            t = tf.concat((target_values, [r]), axis=0)
            return (self.p, self.q), t


class EpisodeList(object):
    def __init__(self):
        self.episodes = []

    def add(self, episode):
        self.episodes.append(episode)

    def make_dataset(self, behavior_network, target_network, bonus_network,
                     config, batch_size, shuffle):
        p_list, q_list, t_list = [], [], []
        for episode in tqdm(self.episodes):
            (p, q), t = episode.make_pair(
                behavior_network=behavior_network,
                target_network=target_network, bonus_network=bonus_network,
                config=config
            )
            p_list.append(p)
            q_list.append(q)
            t_list.append(t)
        p_list = np.vstack(p_list)
        q_list = np.vstack(q_list)
        t_list = tf.concat(t_list, axis=0)

        dataset = tf.data.Dataset.from_tensor_slices((
            (p_list, q_list), t_list
        ))
        if shuffle:
            dataset = dataset.shuffle(batch_size * 100)
        dataset = dataset.batch(batch_size)
        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        return dataset


def sample_episodes(num_episodes, env, behavior_network, bonus_network,
                    config):
    pc = tf.constant(float(config['ponanza_constant']))
    bs = tf.constant(float(config['bonus']))

    if bonus_network is None:
        @tf.function(input_signature=[
            tf.TensorSpec(shape=[2, None, 38], dtype=tf.int32)])
        def _evaluate(x):
            scores, _ = behavior_network(x)
            scores = tf.squeeze(-scores)

            y = scores / pc
            return y
    else:
        @tf.function(input_signature=[
            tf.TensorSpec(shape=[2, None, 38], dtype=tf.int32)])
        def _evaluate(x):
            scores, _ = behavior_network(x)
            scores = tf.squeeze(-scores)

            bonuses = bonus_network(x)
            y = scores / pc + bs * bonuses
            return y

    episode_list = EpisodeList()

    for _ in trange(num_episodes):
        env.reset()
        episode = SingleEpisode()
        while True:
            inputs, actions = get_states(board=env.board)
            if actions is None:
                # 詰みの状態
                episode.result = -1
                break

            tensor = tf.convert_to_tensor(inputs)
            values = _evaluate(x=tensor)
            values = tf.reshape(values, shape=[1, -1]) * config['softmax']
            index = tf.random.categorical(logits=values, num_samples=1)
            index = int(index)
            selected_action = actions[index]

            # 状態を記録
            episode.add(index=index, state_list=inputs)

            m = cshogi.move_to_usi(selected_action)
            reward, done, draw = env.step(m)
            if done:
                episode.result = reward
                episode.draw = draw
                break
        episode.record = env.actions
        episode_list.add(episode=episode)

    return episode_list


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def learn(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    usi = make_usi(config=config['engine'])
    env = gym.make(config['gym']['name'], usi=usi,
                   random_rate=config['gym']['random_rate'])

    behavior_network = NNUESigmoid()
    target_network = NNUESigmoid()
    bonus_network = BonusNetwork()

    nnue_optimizer = tf.keras.optimizers.Adam()
    bonus_optimizer = tf.keras.optimizers.Adam()

    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), behavior_network=behavior_network,
        target_network=target_network, bonus_network=bonus_network,
        behavior_optimizer=nnue_optimizer, bonus_optimizer=bonus_optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)
    if not manager.latest_checkpoint:
        half_kp_size = 125388
        tmp = np.random.randint(half_kp_size, size=[2, 38])
        behavior_network([tmp, tmp])
        target_network([tmp, tmp])
        # 値を同期
        for b, t in zip(behavior_network.variables, target_network.variables):
            t.assign(b)

    test_data_path = model_dir / 'test_dataset.pickle'
    if test_data_path.exists():
        print('load test data from {}'.format(test_data_path))
        test_episode_list = joblib.load(test_data_path)
    else:
        print('sampling test data')
        test_episode_list = sample_episodes(
            num_episodes=100, env=env, behavior_network=behavior_network,
            bonus_network=None, config=config['scale']
        )
        joblib.dump(test_episode_list, test_data_path)
    print('constructing test dataset')
    test_dataset = test_episode_list.make_dataset(
        behavior_network=behavior_network, target_network=target_network,
        bonus_network=bonus_network, config=config['scale'],
        batch_size=config['batch_size'], shuffle=False
    )

    print('sampling train data')
    episode_list = sample_episodes(
        num_episodes=100, env=env, behavior_network=behavior_network,
        bonus_network=bonus_network, config=config['scale']
    )
    print('constructing train dataset')
    train_dataset = episode_list.make_dataset(
        behavior_network=behavior_network, target_network=target_network,
        bonus_network=bonus_network, config=config['scale'],
        batch_size=config['batch_size'], shuffle=True
    )

    criterion = tf.keras.losses.KLDivergence()

    train_nnue_loss = tf.keras.metrics.KLDivergence('train_nnue_loss')
    train_bonus_loss = tf.keras.metrics.Mean('train_bonus_loss')
    test_nnue_loss = tf.keras.metrics.KLDivergence('test_nnue_loss')
    test_bonus_loss = tf.keras.metrics.Mean('test_bonus_loss')
    train_nnue_loss.reset_states()
    train_bonus_loss.reset_states()
    test_nnue_loss.reset_states()
    test_bonus_loss.reset_states()

    train_log_dir = str(config_path.parent / 'logs/gradient_tape/train')
    test_log_dir = str(config_path.parent / 'logs/gradient_tape/test')
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)

    pc = tf.constant(float(config['scale']['ponanza_constant']))

    @tf.function
    def train_step(x, y):
        with tf.GradientTape() as tape:
            behavior, histogram = behavior_network(x)
            values = tf.squeeze(-behavior) / pc
            q = tf.math.sigmoid(values)
            prediction = tf.stack([1 - q, q], axis=1)

            p = tf.math.sigmoid(y)
            target = tf.stack([1 - p, p], axis=1)

            loss = criterion(y_true=target, y_pred=prediction)
        gradients = tape.gradient(
            loss, behavior_network.trainable_variables
        )
        nnue_optimizer.apply_gradients(zip(
            gradients, behavior_network.trainable_variables
        ))

        train_nnue_loss.update_state(y_true=target, y_pred=prediction)

        with tf.GradientTape() as tape:
            loss = bonus_network(x)
        gradients = tape.gradient(loss, bonus_network.trainable_variables)
        bonus_optimizer.apply_gradients(zip(
            gradients, bonus_network.trainable_variables
        ))

        train_bonus_loss.update_state(loss)
        return histogram

    @tf.function
    def test_step(x, y):
        behavior, histogram = behavior_network(x)
        values = tf.squeeze(-behavior) / pc
        q = tf.math.sigmoid(values)
        prediction = tf.stack([1 - q, q], axis=1)

        p = tf.math.sigmoid(y)
        target = tf.stack([1 - p, p], axis=1)

        test_nnue_loss.update_state(y_true=target, y_pred=prediction)

        bonus = bonus_network(x)
        test_bonus_loss.update_state(bonus)

        return histogram

    for epoch in trange(config['epochs']):
        histograms = None
        for train_x, train_y in train_dataset:
            histograms = train_step(train_x, train_y)
        with train_summary_writer.as_default():
            tf.summary.scalar('train/nnue/loss', train_nnue_loss.result(),
                              step=int(ckpt.step))
            tf.summary.scalar('train/bonus/loss', train_bonus_loss.result(),
                              step=int(ckpt.step))
            if int(ckpt.step) % config['frequency']['histogram'] == 0:
                for key, value in histograms.items():
                    tf.summary.histogram('train/{}'.format(key), value,
                                         step=int(ckpt.step))
        train_nnue_loss.reset_states()
        train_bonus_loss.reset_states()

        if int(ckpt.step) % config['frequency']['test'] == 0:
            for test_x, test_y in test_dataset:
                histograms = test_step(test_x, test_y)
            with test_summary_writer.as_default():
                tf.summary.scalar('test/nnue/loss', test_nnue_loss.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('test/bonus/loss', test_bonus_loss.result(),
                                  step=int(ckpt.step))
                for key, value in histograms.items():
                    tf.summary.histogram('test/{}'.format(key), value,
                                         step=int(ckpt.step))
            test_nnue_loss.reset_states()
            test_bonus_loss.reset_states()

            if epoch != config['epochs'] - 1:
                # まだ学習が続くなら訓練データを更新
                print('sampling train data')
                episode_list = sample_episodes(
                    num_episodes=100, env=env,
                    behavior_network=behavior_network,
                    bonus_network=bonus_network, config=config['scale']
                )
                print('constructing train dataset')
                train_dataset = episode_list.make_dataset(
                    behavior_network=behavior_network,
                    target_network=target_network,
                    bonus_network=bonus_network, config=config['scale'],
                    batch_size=config['batch_size'], shuffle=True
                )

        if int(ckpt.step) % config['frequency']['synchronize'] == 0:
            # target networkを更新
            for t, b in zip(target_network.variables,
                            behavior_network.variables):
                t.assign(b)
        ckpt.step.assign_add(1)
        manager.save()

    usi.disconnect()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
@click.option('--data-type', type=click.Choice(['train', 'test']))
def view(config_path, data_type):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    behavior_network = NNUESigmoid()
    target_network = NNUESigmoid()
    bonus_network = BonusNetwork()

    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), behavior_network=behavior_network,
        target_network=target_network, bonus_network=bonus_network
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    if data_type == 'test':
        data_path = model_dir / 'test_dataset.pickle'
        print('load test data from {}'.format(data_path))
        episode_list = joblib.load(data_path)
        # 100個は多いので減らす
        episode_list.episodes = episode_list.episodes[:10]
    else:
        usi = make_usi(config=config['engine'])
        env = gym.make(config['gym']['name'], usi=usi,
                       random_rate=config['gym']['random_rate'])

        print('sampling train data')
        episode_list = sample_episodes(
            num_episodes=10, env=env, behavior_network=behavior_network,
            bonus_network=bonus_network, config=config['scale']
        )

    dataset = episode_list.make_dataset(
        behavior_network=behavior_network, target_network=target_network,
        bonus_network=bonus_network, config=config['scale'],
        batch_size=config['batch_size'], shuffle=False
    )

    pc = tf.constant(float(config['scale']['ponanza_constant']))

    prediction_list, target_list = [], []
    p_list, q_list = [], []
    for x, y in dataset:
        behavior, _ = behavior_network(x)
        values = tf.squeeze(-behavior) / pc

        prediction_list.append(values)
        target_list.append(y)

        p_list.append(x[0])
        q_list.append(x[1])
    prediction_list = tf.concat(prediction_list, axis=0)
    target_list = tf.concat(target_list, axis=0)
    p_list = tf.concat(p_list, axis=0)
    q_list = tf.concat(q_list, axis=0)

    values = tf.stack([prediction_list, target_list], axis=1)
    name = str(config_path.with_name('{}_prediction.csv'.format(data_type)))
    np.savetxt(name, values.numpy())

    joblib.dump(episode_list,
                config_path.with_name('{}_sample.pickle'.format(data_type)))
    np.savez_compressed(
        str(config_path.with_name('{}_inputs.npz'.format(data_type))),
        p=p_list.numpy(), q=q_list.numpy()
    )


def main():
    cmd()


if __name__ == '__main__':
    main()
