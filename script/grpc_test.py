#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
gRPCでYaneuraOuの関数を呼び出せるか確認する
"""

from pathlib import Path

import grpc

try:
    import remote_usi_pb2
    import remote_usi_pb2_grpc
except ImportError as e:
    from grpc_tools import protoc
    protoc.main(('', '--proto_path=../source', '--python_out=.',
                 '--grpc_python_out=.', 'remote_usi.proto'))

    import remote_usi_pb2
    import remote_usi_pb2_grpc

__author__ = 'Yasuhiro'
__date__ = '2020/10/10'


def main():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = remote_usi_pb2_grpc.LearnerStub(channel=channel)
        name = 'foo'
        response = stub.SayHello(remote_usi_pb2.HelloRequest(name=name))
        print(response.message)

        messages = [remote_usi_pb2.HelloRequest(name='a1'),
                    remote_usi_pb2.HelloRequest(name='b2')]
        response = stub.Twice(iter(messages))
        for r in response:
            print(r.message)


if __name__ == '__main__':
    main()
