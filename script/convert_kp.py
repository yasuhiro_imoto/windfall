#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cshogi

from make_record1 import BonaPiece

__author__ = 'Yasuhiro'
__date__ = '2020/1月/27'


def convert_kp(p_list, q_list):
    size = 1548
    p_ou = p_list[0] // size
    q_ou = 80 - q_list[0] // size

    indices = p_list % size

    # [1, 20): 手番側の持ち駒の歩
    # flag = tf.logical_and(tf.greater_equal(indices, 1), tf.less(indices, 20))
    # count = tf.math.count_nonzero(flag, axis=1)

    p_hand = np.array([
        np.count_nonzero(np.logical_and(indices >= i, indices < j))
        for i, j in zip(BonaPiece[0:14:2], BonaPiece[1:15:2])
    ])
    q_hand = np.array([
        np.count_nonzero(np.logical_and(indices >= i, indices < j))
        for i, j in zip(BonaPiece[1:15:2], BonaPiece[2:16:2])
    ])
    # 金の枚数の位置を修正
    tmp = p_hand[-3]
    p_hand[-3:-1] = p_hand[-2:]
    p_hand[-1] = tmp
    tmp = q_hand[-3]
    q_hand[-3:-1] = q_hand[-2:]
    q_hand[-1] = tmp

    piece_table = (
        cshogi.BPAWN, cshogi.WPAWN, cshogi.BLANCE, cshogi.WLANCE,
        cshogi.BKNIGHT, cshogi.WKNIGHT, cshogi.BSILVER, cshogi.WSILVER,
        cshogi.BGOLD, cshogi.WGOLD, cshogi.BBISHOP, cshogi.WBISHOP,
        cshogi.BPROM_BISHOP, cshogi.WPROM_BISHOP,
        cshogi.BROOK, cshogi.WROOK, cshogi.BPROM_ROOK, cshogi.WPROM_ROOK
    )

    board = np.zeros(81, dtype=np.int32)
    board[p_ou] = cshogi.BKING
    board[q_ou] = cshogi.WKING
    for v in indices:
        if v < BonaPiece[14]:
            continue
        piece_type, square = divmod(v - BonaPiece[14], 81)
        board[square] = piece_table[piece_type]

    return board, p_hand, q_hand


def make_state(p_list, q_list):
    board, p_hand, q_hand = convert_kp(p_list=p_list, q_list=q_list)

    sfen = ''
    for i in range(9):
        c = 0
        for j in range(9):
            p = board[i + (8 - j) * 9]
            if p == cshogi.NONE:
                c += 1
            else:
                if c != 0:
                    sfen += str(c)
                    c = 0

                color = p >> 4
                if color == cshogi.BLACK:
                    # noinspection PyUnresolvedReferences
                    sfen += cshogi.PIECE_SYMBOLS[p & 0xF].upper()
                else:
                    # noinspection PyUnresolvedReferences
                    sfen += cshogi.PIECE_SYMBOLS[p & 0xF].lower()
        if c != 0:
            sfen += str(c)
        sfen += '/'
    sfen += ' b '
    if np.all(p_hand == 0) and np.all(q_hand == 0):
        sfen += '-'
    else:
        for i, c in enumerate(p_hand, start=1):
            if c == 0:
                continue
            elif c > 1:
                sfen += str(c)
            # noinspection PyUnresolvedReferences
            sfen += cshogi.PIECE_SYMBOLS[i].upper()
        for i, c in enumerate(q_hand, start=1):
            if c == 0:
                continue
            elif c > 1:
                sfen += str(c)
            # noinspection PyUnresolvedReferences
            sfen += cshogi.PIECE_SYMBOLS[i].lower()
    sfen += ' 1'

    b = cshogi.Board(sfen)
    return b


def main():
    from make_record2 import fv40

    board = cshogi.Board()
    p, q, p_list, q_list = fv40(board=board)
    size = 1548
    p_list = np.asarray(p_list) + p * size
    q_list = np.asarray(q_list) + q * size

    state = make_state(p_list=p_list, q_list=q_list)

    print(state)


if __name__ == '__main__':
    main()
