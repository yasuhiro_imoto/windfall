#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
指し手で進める場合と戻す場合のテスト用データを生成する
"""

import random
import json
from pathlib import Path

import cshogi
from absl import flags, app

__author__ = 'Yasuhiro'
__date__ = '2020/08/12'

FLAGS = flags.FLAGS
flags.DEFINE_integer('n_samples', default=100, help='')
flags.DEFINE_string('output_path', default=None, help='')


def generate_sequence():
    """
    平手の初期配置からランダムな指し手と動いた後の局面のsfenを作る
    :return:
    """
    board = cshogi.Board()

    data = []
    while True:
        legal_moves = list(board.legal_moves)
        if len(legal_moves) == 0:
            break
        else:
            if len(legal_moves) == 1:
                i = 0
            else:
                i = random.randrange(1, len(legal_moves))

            move = cshogi.move_to_usi(legal_moves[i])
            board.push(legal_moves[i])

            data.append({'move': move, 'sfen': board.sfen()})

    return data


def main(_):
    data_list = [generate_sequence() for _ in range(FLAGS.n_samples)]

    output_path = Path(FLAGS.output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    with output_path.open('w') as f:
        json.dump(data_list, f, indent=4)


if __name__ == '__main__':
    app.run(main)
