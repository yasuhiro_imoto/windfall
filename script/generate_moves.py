#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import json
from pathlib import Path

import cshogi
from absl import flags, app

__author__ = 'Yasuhiro'
__date__ = '2020/07/18'

FLAGS = flags.FLAGS
flags.DEFINE_string('output_path', default=None, help='')
flags.DEFINE_integer('n_samples', default=100, help='')


def generate():
    board = cshogi.Board()
    count = 0
    sfen_list = []

    while True:
        legal_moves = list(board.legal_moves)
        if len(legal_moves) == 0:
            # reset
            board = cshogi.Board()
            continue

        if len(legal_moves) == 1:
            i = 0
        else:
            i = random.randrange(1, len(legal_moves))
        board.push(legal_moves[i])

        count += 1
        if count >= 10:
            sfen_list.append(board.sfen())

            if len(sfen_list) == FLAGS.n_samples:
                break
            count = 0
    return sfen_list


def main(_):
    sfen_list = generate()

    data = []
    for sfen in sfen_list:
        board = cshogi.Board(sfen)
        moves = [cshogi.move_to_usi(m) for m in board.legal_moves]

        data.append({'sfen': sfen, 'moves': moves})

    path = Path(FLAGS.output_path)
    if not path.parent.exists():
        path.parent.mkdir(parents=True)
    with path.open('w') as f:
        json.dump(data, f, indent=4)


if __name__ == '__main__':
    app.run(main)
