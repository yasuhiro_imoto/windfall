#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

import cshogi
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
import click
import ray
from ray.rllib import Policy
from ray.rllib.policy.policy import LEARNER_STATS_KEY
from ray.rllib.models.modelv2 import ModelV2
from ray.rllib.utils.annotations import PublicAPI
from ray.rllib.agents.dqn.dqn import DQNTrainer, DEFAULT_CONFIG as DQN_CONFIG
from ray.rllib.optimizers import AsyncReplayOptimizer
from ray.rllib.utils import merge_dicts
from ray import tune
from ray.tune.registry import register_env
from ray.rllib.utils.annotations import override

from wf_gym import make_usi
from wf_gym.envs import SuishoRandomized
from make_record2 import fv40
from nnue import NNUESigmoid

tfd = tfp.distributions

__author__ = 'Yasuhiro'
__date__ = '2019/12月/17'


def env_creator(env_config):
    usi = make_usi(config=env_config['engine'])
    return SuishoRandomized(
        usi=usi, random_rate=env_config['random_rate']
    )  # return an env instance


class CustomPolicy(Policy):
    """Example of a custom policy written from scratch.

    You might find it more convenient to use the `build_tf_policy` and
    `build_torch_policy` helpers instead for a real policy, which are
    described in the next sections.
    """

    def __init__(self, observation_space, action_space, config):
        super().__init__(observation_space, action_space, config)
        # example parameter
        self.w = 1.0

        self.behavior_model = CustomModel(
            obs_space=observation_space, action_space=action_space,
            num_outputs=1, model_config=config, name='behavior_model'
        )
        self.target_model = CustomModel(
            obs_space=observation_space, action_space=action_space,
            num_outputs=1, model_config=config, name='target_model'
        )
        self.optimizer_affine = tf.keras.optimizers.Adam()
        self.optimizer_embedding = tf.keras.optimizers.SGD()

        # ExplorationState
        self._epsilon = tf.Variable(1.0, trainable=False, dtype=tf.float32,
                                    name='eps')

        from scipy.special import logit

        scale = 512.0
        # noinspection PyTypeChecker
        self.result_value = (0, logit(0.99) * scale, logit(0.01) * scale)
        # 少し小さめの値を設定
        self.ponanza_constant = tf.constant(scale, dtype=tf.float32)

    # noinspection PyUnusedLocal
    def compute_actions(self,
                        obs_batch,
                        state_batches,
                        prev_action_batch=None,
                        prev_reward_batch=None,
                        info_batch=None,
                        episodes=None,
                        **kwargs):
        action_batch = []
        for obs, state in zip(obs_batch, state_batches):
            action, _, _ = self.compute_single_action(obs=obs, state=state)
            action_batch.append(action)
        # return action batch, RNN states, extra values to include in batch
        return action_batch, [], {}

    # noinspection PyUnusedLocal
    def compute_single_action(self,
                              obs,
                              state,
                              prev_action=None,
                              prev_reward=None,
                              info=None,
                              episode=None,
                              clip_actions=False,
                              **kwargs):
        # obsはpacked sfen
        board = cshogi.Board()
        board.set_psfen(obs)

        actions = list(board.legal_moves)
        if len(actions) == 0:
            # resign
            return 0, [], {}

        p_index_list = np.empty([len(actions), 38], dtype=np.int32)
        q_index_list = np.empty_like(p_index_list)
        for i, action in enumerate(actions):
            obs.push(action)
            p, q, p_index, q_index = fv40(board=obs)
            obs.pop(action)

            p_index_list[i] = p_index
            p_index_list[i] += p
            q_index_list[i] = q_index
            q_index_list[i] += q

        scores = self.behavior_model.evaluate((p_index_list, q_index_list))
        # 手番が進んでいるので評価を反転
        scores = tf.squeeze(-scores)

        # noinspection PyTypeChecker
        t = self._epsilon - 1 / len(actions)
        r = tf.random.uniform([], minval=0, maxval=1)
        if r < t:
            # greedy
            index = tf.argmax(scores)
            return actions[int(index)], [], {}
        else:
            index = np.random.randint(len(actions))
            return actions[index], [], {}

    # noinspection PyUnusedLocal
    def learn_on_batch(self, samples):
        board = cshogi.Board()

        policy_id = 0
        data = samples[policy_id]
        target_values = self._compute_target(data=data, board=board)

        def _generator():
            obs = data['obs']
            actions = data['actions']

            for o, a, t in zip(obs, actions, target_values):
                board.set_sfen(o)
                board.push(a)

                p, q, p_index, q_index = fv40(board=board)
                p_index = np.asarray(p_index, dtype=np.int32) + p
                q_index = np.asarray(q_index, dtype=np.int32) + q

                yield (p_index, q_index), t

        dataset = tf.data.Dataset.from_generator(
            generator=_generator,
            output_types=((tf.int32, tf.int32), tf.float32),
            output_shapes=((tf.TensorShape([38]), tf.TensorShape([38])),
                           tf.TensorShape([]))
        ).batch(500).prefetch(tf.data.experimental.AUTOTUNE)

        spec_x = tf.TensorSpec([None, 38], dtype=tf.int32)
        spec_y = tf.TensorSpec([None], dtype=tf.float32)

        @tf.function(input_signature=[(spec_x, spec_x), spec_y])
        def step(x, y):
            with tf.GradientTape() as tape:
                behavior = self.behavior_model.evaluate(x[0], x[1])
                logits_q = behavior / self.ponanza_constant

                logits_p = y / self.ponanza_constant
                p = tf.math.sigmoid(logits_p)

                # kl divergence
                loss = tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=p, logits=logits_q
                ) - tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=p, logits=logits_p
                )
            # embeddingの変数が最後になるように手動で取得
            model = self.behavior_model.model
            variables = model.affine1.trainable_variables
            variables += model.affine2.trainable_variables
            variables += model.affine3.trainable_variables
            variables += model.transformer.bias.trainable_variables
            variables += model.transformer.kernel.trainable_variables
            variables += model.transformer.weight_base.trainable_variables

            gradients = tape.gradient(loss, variables)
            self.optimizer_affine.apply_gradients(zip(
                gradients[:-2], variables[:-2]
            ))
            self.optimizer_embedding.apply_gradients(zip(
                gradients[-2:], variables[-2:]
            ))

        for batch_x, batch_y in dataset:
            step(batch_x, batch_y)

        return {LEARNER_STATS_KEY: {}}  # return stats

    def get_weights(self):
        return {"w": self.w}

    def set_weights(self, weights):
        self.w = weights["w"]

    def add_parameter_noise(self):
        # ExplorationState
        if self.config["parameter_noise"]:
            self.sess.run(self.add_noise_op)

    def set_epsilon(self, epsilon):
        # ExplorationState
        self._epsilon.assign(epsilon)

    @override(Policy)
    def get_state(self):
        # ExplorationState
        # get_stateはget_weightsを呼び出している
        # noinspection PyTypeChecker
        return [super().get_state(self), float(self._epsilon)]

    @override(Policy)
    def set_state(self, state):
        # ExplorationState
        # set_stateはset_weightを呼び出している
        super().set_state(self, state[0])
        self.set_epsilon(state[1])

    def _compute_target(self, data, board):
        def _generator():
            rewards = data['rewards']
            new_obs = data['new_obs']
            dones = data['dones']
            for i, (r, no, d) in enumerate(zip(rewards, new_obs, dones)):
                # rewardが±1なら勝ちか負けなので、普通
                # rewardが-2ならmax moveに到達したとする
                # rewardが0なら千日手の引き分け
                if d and r != -2:
                    # max moveならnext stateが目標値になる
                    # それ以外はresult_valueに従う
                    p_list = np.empty((1, 38), dtype=np.int32)
                    q_list = np.empty_like(p_list)
                    yield (p_list, q_list), True, self.result_value[r]

                board.set_psfen(no)
                legal_moves = list(board.legal_moves)
                size = len(legal_moves)

                p_list = np.empty((size, 38), dtype=np.int32)
                q_list = np.empty_like(p_list)
                for j, m in enumerate(legal_moves):
                    board.push(m)
                    p, q, p_index, q_index = fv40(board=board)
                    board.pop(m)

                    p_list[j] = p_index
                    p_list[j] += p
                    q_list[j] = q_index
                    q_list[j] += q
                yield (p_list, q_list), False, 0.0

        index_shape = tf.TensorShape([None, 38])
        dataset = tf.data.Dataset.from_generator(
            generator=_generator,
            output_types=((tf.int32, tf.int32), tf.bool, tf.float32),
            output_shapes=((index_shape, index_shape),
                           tf.TensorShape([]), tf.TensorShape([]))
        ).prefetch(tf.data.experimental.AUTOTUNE)

        spec = tf.TensorSpec(shape=[None, 38], dtype=tf.int32)

        # そのうちDouble DQNにする
        @tf.function(input_signature=[spec, spec])
        def step(p, q):
            scores, _ = self.target_model.evaluate(p, q)
            target_value = -tf.reduce_min(scores)
            return target_value

        n = len(data['obs'])
        target_list = np.empty(n, dtype=np.float32)
        for j, ((p_values, q_values), flag, value) in enumerate(dataset):
            if flag:
                target_list[j] = value.numpy()
            else:
                target_list[j] = step(p_values, q_values).numpy()

        return target_list


@PublicAPI
class CustomModel(ModelV2):
    def __init__(self, obs_space, action_space, num_outputs, model_config,
                 name):
        super().__init__(obs_space, action_space, num_outputs, model_config,
                         name, framework='tf')
        self.model = NNUESigmoid()

    def evaluate(self, p, q):
        scores, _ = self.model([p, q])
        return scores


APEX_DEFAULT_CONFIG = merge_dicts(
    DQN_CONFIG,  # see also the options in dqn.py, which are also supported
    {
        # "optimizer": merge_dicts(
        #     DQN_CONFIG["optimizer"], {
        #         # "max_weight_sync_delay": 400,
        #         # "num_replay_buffer_shards": 4,
        #         "debug": False
        #     }),
        "n_step": 3,
        "num_gpus": 1,
        "num_workers": 8,
        "buffer_size": 2000000,
        "learning_starts": 50000,
        "train_batch_size": 512,
        "sample_batch_size": 50,
        "target_network_update_freq": 500000,
        "timesteps_per_iteration": 25000,
        "per_worker_exploration": True,
        "worker_side_prioritization": True,
        "min_iter_time_s": 30,
    },
)


@click.command()
def cmd():
    ray.init()

    path = Path(__file__).absolute()

    register_env("my_env", env_creator)
    config = {
        'env': 'my_env',
        'env_config': {
            'engine': {
                'path': str(path.with_name('YaneuraOu-by-gcc')),
                'hash_size': 1024,
                'threads': 1,
                'depth_limit': 1,
                'resign_value': 99999,
                'eval_dir': str(path.with_name('eval'))
            },
            'random_rate': 9.95
        },
        'log_level': 'DEBUG'
    }
    trainer = DQNTrainer.with_updates(
        name='APEX', default_policy=CustomPolicy,
        default_config=APEX_DEFAULT_CONFIG,
    )
    tune.run(trainer, config=config)


def main():
    cmd()


if __name__ == '__main__':
    main()
