#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

__author__ = 'Yasuhiro'
__date__ = '2019/10/19'


def my_floor(x):
    tmp = x - tf.math.floor(x)
    return x - tf.stop_gradient(tmp)


class Shift(tf.keras.layers.Layer):
    def __init__(self, scale):
        super().__init__()
        self.scale = scale

    def call(self, inputs, training=None, mask=None):
        return my_floor(inputs / self.scale)


class ClippedReLU(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()

    # noinspection PyMethodOverriding
    def call(self, inputs):
        return tf.clip_by_value(inputs, clip_value_min=0, clip_value_max=127)


class ScalingSigmoid(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()

    def call(self, inputs, training=None, mask=None):
        # sigmoidの範囲はだいたい[-4096, 4096]を想定
        # それより外側は飽和しているとみなして問題ない
        # clipped reluでは64で割るので、64*128=8192
        # 同じくらいの範囲を対象にする
        return 127 * tf.keras.activations.sigmoid(inputs / 512)


class SigmoidReLU1(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.sigmoid = tf.keras.layers.Activation('sigmoid')
        
        self.relu1 = tf.keras.layers.ReLU(max_value=1)
        self.scale = tf.constant(0.17706499785191226, dtype=tf.float32)

    def call(self, inputs, training=None, mask=None):
        if mask is None:
            return self.sigmoid(inputs)
        return (mask * self.sigmoid(inputs) +
                (1 - mask) * self.relu1(self.scale * inputs + 0.5))


class BackwardReLU1(tf.keras.Model):
    def __init__(self):
        super().__init__()

        self.relu1 = tf.keras.layers.ReLU(max_value=1)
        self.sigmoid = tf.keras.layers.Activation('sigmoid')

    def call(self, inputs, training=None, mask=None):
        relu1 = self.relu1(inputs)
        sigmoid = self.sigmoid(inputs - 0.5)
        return tf.stop_gradient(relu1 - sigmoid) + sigmoid


class KernelRegularizer(tf.keras.regularizers.Regularizer):
    def __init__(self, is_last_layer):
        super().__init__()

        self.is_last_layer = is_last_layer
        if is_last_layer:
            tmp = 127 / (600 * 16)
        else:
            a = 0.17706499785191226
            tmp = 64 * a
        self.threshold_upper = 127 / tmp
        self.threshold_lower = -128 / tmp

    def __call__(self, x):
        upper = tf.math.reduce_sum(tf.nn.relu(x - self.threshold_upper))
        lower = tf.math.reduce_sum(tf.nn.relu(self.threshold_lower - x))
        return upper + lower

    def get_config(self):
        return {'scale': float(self.scale),
                'is_last_layer': self.is_last_layer}
