#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from .nnue import NNUE

__author__ = 'Yasuhiro'
__date__ = '2019/10/19'

__all__ = ['NNUE']
