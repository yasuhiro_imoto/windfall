#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

__author__ = 'Yasuhiro'
__date__ = '2019/10/19'


class AffineTransformer(tf.keras.layers.Layer):
    def __init__(self, units):
        super().__init__()
        self.units = units
        if units == 1:
            self.scale = 600 * 16 / 127.0
        else:
            self.scale = 64.0

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel', shape=[input_shape[1], self.units], dtype=tf.float32,
            trainable=True
        )
        if self.units == 1:
            initializer = tf.keras.initializers.zeros()
        else:
            initializer = tf.keras.initializers.constant(0.5)
        self.bias = self.add_weight(
            'bias', shape=[self.units], dtype=tf.float32, trainable=True,
            initializer=initializer
        )

        self.built = True

    # noinspection PyMethodOverriding
    def call(self, inputs):
        w = tf.quantization.fake_quant_with_min_max_args(
            self.kernel * self.scale, min=-128, max=127, num_bits=8
        ) / self.scale

        outputs = tf.matmul(inputs, w) + self.bias
        return outputs
