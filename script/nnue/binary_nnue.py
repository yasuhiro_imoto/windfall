#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf
import tensorflow_probability as tfp

from .feature_transformer import (FeatureTransformerReal,
                                  SeparatedFeatureTransformer)

tfd = tfp.distributions

__author__ = 'Yasuhiro'
__date__ = '2020/3月/18'


class BinaryNNUE(tf.keras.Model):
    def __init__(self, embedding_size=256, units1=32, units2=32,
                 separated=False):
        super().__init__()
        self.embedding_size = embedding_size
        self.units1 = units1
        self.units2 = units2

        self.separated = separated

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        if self.separated:
            self.feature = SeparatedFeatureTransformer(
                embedding_size=self.embedding_size
            )
        else:
            self.feature = FeatureTransformerReal(
                embedding_size=self.embedding_size
            )
        self.concat = tf.keras.layers.Concatenate(axis=1)
        self.activation0 = Clip()

        self.affine1 = BinaryAffineLayer(units=self.units1, name='affine1')
        self.activation1 = Clip()

        self.affine2 = BinaryAffineLayer(units=self.units2, name='affine2')
        self.activation2 = tf.keras.Sequential([Clip(), LeakyReLU()])
        self.affine3 = BinaryInnerProduct(name='affine3')

        self.built = True

    def call(self, inputs, training=None, mask=None):
        p, q = inputs
        h_p = self.feature(p)
        h_q = self.feature(q)

        h = self.concat([h_p, h_q])
        h = self.activation0(h)

        h = self.affine1(h)
        h = self.activation1(h)

        h = self.affine2(h)
        h = self.activation2(h)
        outputs = self.affine3(h)
        return outputs


class Clip(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.relu = tf.keras.layers.ReLU(max_value=2)
        self.tanh = tf.keras.layers.Activation('tanh')

    def call(self, inputs, **kwargs):
        relu = self.relu(inputs + 1) - 1
        tanh = self.tanh(inputs)
        return tf.stop_gradient(relu - tanh) + tanh


class BinaryAffineLayer(tf.keras.layers.Layer):
    def __init__(self, units, name=None):
        super().__init__(name=name)
        self.units = units

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel', shape=[input_shape[-1], self.units], dtype=tf.float32,
            initializer=tf.keras.initializers.lecun_normal(),
            trainable=True
        )
        self.bias = self.add_weight(
            'bias', shape=[self.units], dtype=tf.float32,
            initializer=tf.keras.initializers.zeros(), trainable=True
        )
        # 入力の次元数
        self.d = input_shape[-1]

        self.built = True

    def call(self, inputs, **kwargs):
        ib = my_sign(inputs)
        alpha = tf.reduce_mean(tf.abs(inputs), axis=1, keepdims=True)

        wb = my_sign(self.kernel)
        beta = tf.reduce_mean(tf.abs(self.kernel), axis=0, keepdims=True)

        outputs = tf.matmul(ib, wb) * alpha * beta + self.bias
        return outputs


class BinaryInnerProduct(tf.keras.layers.Layer):
    def __init__(self, name):
        super().__init__(name=name)
        self.units = 1

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel', shape=[input_shape[-1], self.units], dtype=tf.float32,
            initializer=tf.keras.initializers.lecun_normal(),
            trainable=True
        )
        self.bias = self.add_weight(
            'bias', shape=[self.units], dtype=tf.float32,
            initializer=tf.keras.initializers.zeros(), trainable=True
        )

        self.built = True

    def call(self, inputs, **kwargs):
        wb = my_sign(self.kernel)
        beta = tf.reduce_mean(tf.abs(self.kernel), axis=0, keepdims=True)

        outputs = tf.matmul(inputs, wb) * beta + self.bias
        return outputs


class LeakyReLU(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()

    def call(self, inputs, **kwargs):
        v = tf.abs(inputs) * 0.5
        outputs = inputs + v
        return outputs


@tf.custom_gradient
def my_sign(x):
    y = tf.sign(x)

    def grad(dy):
        return dy * tf.cast(tf.abs(x) <= 1, dtype=tf.float32)
    return y, grad
