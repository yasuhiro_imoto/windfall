#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

__author__ = 'Yasuhiro'
__date__ = '2020/10/18'


class EmbeddingLite(tf.keras.layers.Layer):
    def __init__(self, name=None):
        super(EmbeddingLite, self).__init__(name=name)

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.net = tf.keras.layers.Embedding(
            input_dim=361 * 81, output_dim=32
        )

        scales = []
        initial_values = []
        for s in (16, 4, 4, 4, 2, 2, 4, 1):
            scales.extend([1.0 / s for _ in range(32)])
            # スケールの後で0.5になるように調整
            initial_values.extend([0.5 * s for _ in range(32)])

        self.bias = self.add_weight(
            'bias', shape=[256],
            initializer=tf.keras.initializers.constant(initial_values)
        )

        self.scales = tf.constant(scales, dtype=tf.float32)

        self.built = True

    def call(self, inputs, **kwargs):
        features = [
            self.compute(inputs[key])
            for key in ('fu', 'ky', 'ke', 'gi', 'ka', 'hi', 'ki', 'ou')
        ]
        h = tf.concat(features, axis=-1)

        outputs = (h + self.bias) * self.scales
        return outputs

    def compute(self, index):
        x = self.net(index)
        # indexの軸で合計
        y = tf.math.reduce_sum(x, axis=1)
        return y


class QuantizedEmbeddingLite(tf.keras.layers.Layer):
    def __init__(self, name=None):
        super().__init__(name=name)

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        half_kp_size = 361 * 81
        self.kernel = self.add_weight(
            'kernel', shape=[half_kp_size, 32], dtype=tf.float32,
            trainable=True
        )

        scales = []
        initial_values = []
        for s in (16, 4, 4, 4, 2, 2, 4, 1):
            scales.extend([1.0 / s for _ in range(32)])
            # スケールの後で0.5になるように調整
            initial_values.extend([0.5 * s for _ in range(32)])

        self.bias = self.add_weight(
            'bias', shape=[256],
            initializer=tf.keras.initializers.constant(initial_values)
        )
        self.scales = tf.constant(scales, dtype=tf.float32)

        self.built = True

    def call(self, inputs, **kwargs):
        kernel = tf.quantization.fake_quant_with_min_max_args(
            self.kernel, min=-2 ** 8, max=2 ** 8 - 1, num_bits=16
        )
        features = [
            self.compute(kernel, inputs[key])
            for key in ('fu', 'ky', 'ke', 'gi', 'ka', 'hi', 'ki', 'ou')
        ]
        h = tf.concat(features, axis=-1)

        bias = tf.quantization.fake_quant_with_min_max_args(
            self.bias, min=-2 ** 8, max=2 ** 8 - 1, num_bits=16
        )

        outputs = (h + bias) * self.scales
        return outputs

    @staticmethod
    def compute(kernel, index):
        x = tf.nn.embedding_lookup(kernel, index)
        # indexの軸で合計
        y = tf.math.reduce_sum(x, axis=1)
        return y
