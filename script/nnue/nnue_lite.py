#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

from .embedding_lite import EmbeddingLite, QuantizedEmbeddingLite

__author__ = 'Yasuhiro'
__date__ = '2020/10/18'


class NNUELite(tf.keras.Model):
    def __init__(self, name=None):
        super(NNUELite, self).__init__(name=name)

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.feature = QuantizedEmbeddingLite()
        self.concat = tf.keras.layers.Concatenate(axis=-1)

        self.net = tf.keras.Sequential([
            QuantizedClippedReLU(input_shape=(512,)),
            QuantizedDense(units=32),
            QuantizedClippedReLU(),
            QuantizedDense(units=32),
            QuantizedClippedReLU(),
            QuantizedDense(units=1)
        ])

        self.built = True

    def call(self, inputs, training=None, mask=None):
        p = self.feature(inputs[0], training=training)
        q = self.feature(inputs[1], training=training)
        h = self.concat([p, q])

        outputs = self.net(h, training=training)
        return outputs


class QuantizedClippedReLU(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(QuantizedClippedReLU, self).__init__(**kwargs)

    def call(self, inputs, **kwargs):
        outputs = tf.quantization.fake_quant_with_min_max_args(
            inputs, min=0, max=1, num_bits=7
        )
        return outputs


class QuantizedDense(tf.keras.layers.Layer):
    def __init__(self, units):
        super(QuantizedDense, self).__init__()
        self.units = units
        if units == 1:
            self.scale = 600 * 16 / 127.0
        else:
            self.scale = 64.0

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel', shape=[input_shape[-1], self.units], dtype=tf.float32
        )

        if self.units == 1:
            initializer = tf.keras.initializers.zeros()
        else:
            initializer = tf.keras.initializers.constant(0.5)
        self.bias = self.add_weight(
            'bias', shape=[self.units], dtype=tf.float32,
            initializer=initializer
        )

        self.built = True

    def call(self, inputs, **kwargs):
        kernel = tf.quantization.fake_quant_with_min_max_args(
            self.kernel * self.scale, min=-128, max=127, num_bits=8
        ) / self.scale
        outputs = tf.linalg.matmul(inputs, kernel) + self.bias
        return outputs
