#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
KPで評価値をどの程度は学習できるかを確認する
"""

import tensorflow as tf

__author__ = 'Yasuhiro'
__date__ = '2020/02/25'


class KP(tf.keras.Model):
    def __init__(self, output_dim, return_preactivation=False, use_bias=True):
        super().__init__()
        self.return_preactivation = return_preactivation

        self.unit = KPUnit(output_dim=output_dim, use_bias=use_bias)

    def call(self, inputs, training=None, mask=None):
        tmp = self.unit(inputs=inputs, training=training)
        score = tf.reduce_sum(tmp['score'], axis=1, keepdims=True)

        positive_score = tmp['positive_score']
        negative_score = tmp['negative_score']
        self.add_loss(tf.math.reduce_sum(
            tf.abs(positive_score) + tf.abs(negative_score),
            axis=1, keepdims=True
        ))
        if self.return_preactivation:
            return score, tmp['preactivation']
        else:
            return score


class BoostingKP(tf.keras.Model):
    def __init__(self, output_dim, return_preactivation=False, use_bias=True):
        super().__init__()
        self.output_dim = output_dim
        self.return_preactivation = return_preactivation

        self.unit = KPUnit(output_dim=output_dim, use_bias=use_bias)

    def call(self, inputs, training=None, mask=None):
        h, phase = inputs

        tmp = self.unit(inputs=h, training=training)
        score = tmp['score']

        positive, negative = tmp['positive_score'], tmp['negative_score']
        if phase == 0:
            splits = [1, self.output_dim - 1]
            v1, _ = tf.split(score, num_or_size_splits=splits, axis=1)
            outputs = v1

            p, _ = tf.split(positive, num_or_size_splits=splits, axis=1)
            n, _ = tf.split(negative, num_or_size_splits=splits, axis=1)
        elif phase == self.output_dim - 1:
            splits = [self.output_dim - 1, 1]
            v1, v2 = tf.split(score, num_or_size_splits=splits, axis=1)
            offset = tf.stop_gradient(tf.reduce_sum(v1, axis=1, keepdims=True))
            outputs = offset + v2

            _, p = tf.split(positive, num_or_size_splits=splits, axis=1)
            _, n = tf.split(negative, num_or_size_splits=splits, axis=1)
        else:
            splits = [phase, 1, self.output_dim - 1 - phase]
            v1, v2, _ = tf.split(score, num_or_size_splits=splits, axis=1)
            offset = tf.stop_gradient(tf.reduce_sum(v1, axis=1, keepdims=True))
            outputs = offset + v2

            _, p, _ = tf.split(positive, num_or_size_splits=splits, axis=1)
            _, n, _ = tf.split(negative, num_or_size_splits=splits, axis=1)
        self.add_loss(tf.abs(p) + tf.abs(n))

        if self.return_preactivation:
            positive, negative = tmp['preactivation']
            positive, negative = positive[:, :phase], negative[:, :phase]

            return outputs, (positive, negative)
        else:
            return outputs


class KPUnit(tf.keras.Model):
    def __init__(self, output_dim, use_bias=True, name=None):
        super().__init__(name=name)

        self.positive_basis = FeatureBasis(
            name='positive', output_dim=output_dim, use_bias=use_bias
        )
        self.negative_basis = FeatureBasis(
            name='negative', output_dim=output_dim, use_bias=use_bias
        )

    def call(self, inputs, training=None, mask=None):
        positive = self.positive_basis(inputs)
        negative = self.negative_basis(inputs)

        # 両方とも0以上で処理できるようにする
        positive_score = mish(positive)
        negative_score = mish(negative)
        # 二つの差を取るように変更
        score = positive_score - negative_score

        # 直感的に扱えるようにnegativeの方の符号を反転
        outputs = {'score': score, 'preactivation': (positive, -negative),
                   'positive_score': positive_score,
                   'negative_score': negative_score}
        return outputs


class FeatureBasis(tf.keras.layers.Layer):
    def __init__(self, output_dim, name, use_bias=True):
        super().__init__(name=name)

        size = (1548 + 81) * 81
        self.embedding = tf.keras.layers.Embedding(
            input_dim=size, input_length=40, output_dim=output_dim
        )
        if use_bias:
            self.bias = self.add_weight(
                'bias', shape=[output_dim], dtype=tf.float32, trainable=True
            )
        self.use_bias = use_bias

    def call(self, inputs, **kwargs):
        h = self.embedding(inputs)
        outputs = tf.reduce_sum(h, axis=1)
        if self.use_bias:
            outputs += self.bias
        return outputs


def mish(x):
    return x * tf.math.tanh(tf.nn.softplus(x))
