#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

__author__ = 'Yasuhiro'
__date__ = '2019/10/19'


class FeatureTransformer(tf.keras.layers.Layer):
    def __init__(self):
        super(FeatureTransformer, self).__init__()

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel', shape=(125388, 256),
            initializer=tf.keras.initializers.lecun_normal()
        )
        self.bias = self.add_weight(
            'bias', shape=(256,),
            initializer=tf.keras.initializers.constant(0.5)
        )

        self.built = True

    def call(self, inputs, **kwargs):
        embedding = tf.gather(self.kernel, inputs)
        h = tf.quantization.fake_quant_with_min_max_args(
            embedding * 127, min=-2 ** 15, max=2 ** 15 - 1, num_bits=16
        ) / 127
        h = tf.math.reduce_sum(h, axis=1)

        b = tf.quantization.fake_quant_with_min_max_args(
            self.bias * 127, min=-2 ** 15, max=2 ** 15 - 1, num_bits=16
        ) / 127

        outputs = h + b
        return outputs
