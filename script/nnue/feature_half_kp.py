#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import cshogi

__author__ = 'Yasuhiro'
__date__ = '2020/12/24'


BONA_PIECE_ZERO = 0
BonaPiece = [
    BONA_PIECE_ZERO + 1,  # //0//0+1
    20,  # //f_hand_pawn + 19,//19+1
    39,  # //e_hand_pawn + 19,//38+1
    44,  # //f_hand_lance + 5,//43+1
    49,  # //e_hand_lance + 5,//48+1
    54,  # //f_hand_knight + 5,//53+1
    59,  # //e_hand_knight + 5,//58+1
    64,  # //f_hand_silver + 5,//63+1
    69,  # //e_hand_silver + 5,//68+1
    74,  # //f_hand_gold + 5,//73+1
    79,  # //e_hand_gold + 5,//78+1
    82,  # //f_hand_bishop + 3,//81+1
    85,  # //e_hand_bishop + 3,//84+1
    88,  # //f_hand_rook + 3,//87+1
    90,  # //e_hand_rook + 3,//90
    # f_pawn = fe_hand_end,
    90 + 81,  # e_pawn = f_pawn + 81,
    90 + 81 * 2,  # f_lance = e_pawn + 81,
    90 + 81 * 3,  # e_lance = f_lance + 81,
    90 + 81 * 4,  # f_knight = e_lance + 81,
    90 + 81 * 5,  # e_knight = f_knight + 81,
    90 + 81 * 6,  # f_silver = e_knight + 81,
    90 + 81 * 7,  # e_silver = f_silver + 81,
    90 + 81 * 8,  # f_gold = e_silver + 81,
    90 + 81 * 9,  # e_gold = f_gold + 81,
    90 + 81 * 10,  # f_bishop = e_gold + 81,
    90 + 81 * 11,  # e_bishop = f_bishop + 81,
    90 + 81 * 12,  # f_horse = e_bishop + 81,
    90 + 81 * 13,  # e_horse = f_horse + 81,
    90 + 81 * 14,  # f_rook = e_horse + 81,
    90 + 81 * 15,  # e_rook = f_rook + 81,
    90 + 81 * 16,  # f_dragon = e_rook + 81,
    90 + 81 * 17,  # e_dragon = f_dragon + 81,
    90 + 81 * 18,  # fe_old_end = e_dragon + 81,
]


# kingに対応するインデックスの99は使わない
# 配列をオーバーランする値に設定
PIECE_OFFSET = [0, 2, 4, 6, 8, 12, 16, 10, 99, 10, 10, 10, 10, 14, 18]


def fv38(board):
    b_index, w_index = [], []
    for square in range(81):
        piece = board.piece(square)
        if piece == 0:
            continue

        piece_type = piece & 0xF
        if piece_type != 8:
            piece_color = piece >> 4
            p_value, q_value = 12 + piece_color, 12 + 1 - piece_color
            offset = PIECE_OFFSET[piece_type]
            p_offset, q_offset = p_value + offset, q_value + offset

            b_index.append(square + BonaPiece[p_offset])
            w_index.append(80 - square + BonaPiece[q_offset])
        else:
            if piece == 8:
                b = square
            else:
                w = 80 - square

    pieces_in_hand = board.pieces_in_hand
    for color in range(2):
        for piece_type in range(7):
            b_hand = BonaPiece[color + piece_type * 2]
            w_hand = BonaPiece[1 - color + piece_type * 2]

            piece_count = pieces_in_hand[color][piece_type]
            for i in range(piece_count):
                b_index.append(b_hand + i)
                w_index.append(w_hand + i)

    # noinspection PyUnresolvedReferences
    if board.turn == cshogi.BLACK:
        # noinspection PyUnboundLocalVariable
        return b, w, b_index, w_index
    else:
        # noinspection PyUnboundLocalVariable
        return w, b, w_index, b_index
