#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

from .feature_transformer import FeatureTransformer
from .affine_transformer import AffineTransformer
from .nnue_lite import QuantizedClippedReLU as ClippedReLU

__author__ = 'Yasuhiro'
__date__ = '2019/10/19'


class QuantizedClippedReLU(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super(QuantizedClippedReLU, self).__init__(**kwargs)
        # 切り捨てになるように調整
        self.offset = tf.constant(1.0 / 256, dtype=tf.float32)

    def call(self, inputs, **kwargs):
        outputs = tf.quantization.fake_quant_with_min_max_args(
            inputs - self.offset, min=0, max=1, num_bits=7
        )
        return outputs


class NNUE(tf.keras.Model):
    def __init__(self):
        super(NNUE, self).__init__()

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.embedding = FeatureTransformer()

        self.relu1 = QuantizedClippedReLU()
        self.affine1 = AffineTransformer(units=32)
        self.relu2 = QuantizedClippedReLU()
        self.affine2 = AffineTransformer(units=32)
        self.relu3 = QuantizedClippedReLU()
        self.affine3 = AffineTransformer(units=1)

        self.built = True

    def call(self, inputs, training=None, mask=None):
        p = self.embedding(inputs[0], training=training)
        q = self.embedding(inputs[1], training=training)
        h = tf.keras.layers.concatenate([p, q], axis=-1)

        h = self.relu1(h, training=training)
        h = self.affine1(h, training=training)

        h = self.relu2(h, training=training)
        h = self.affine2(h, training=training)

        h = self.relu3(h, training=training)
        outputs = self.affine3(h, training=training)

        return outputs

    def get_config(self):
        return super(NNUE, self).get_config()
