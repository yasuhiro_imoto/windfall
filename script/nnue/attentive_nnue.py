#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import tensorflow as tf

from .feature_transformer import FeatureTransformerReal
from .misc import BackwardReLU1

__author__ = 'Yasuhiro'
__date__ = '2020/03/07'


class AttentiveNNUE(tf.keras.Model):
    def __init__(self, value_size, query_size, n_layers, skip_value_network):
        super().__init__()

        self.feature = FeatureTransformerReal()
        self.attention_like = AttentionLikeTransformation(
            value_size=value_size, query_size=query_size,
            skip_value_network=skip_value_network
        )

        self.n_layers = n_layers
        if n_layers >= 3:
            self.activation1 = BackwardReLU1()
            self.affine1 = tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.lecun_normal(),
                bias_initializer=tf.keras.initializers.constant(0.5),
                name='affine1'
            )
            self.affine1(tf.zeros([1, 32]))
            tmp = tf.reduce_sum(self.affine1.kernel, axis=0)
            self.affine1.bias.assign(0.5 - 0.5 * tmp)

        self.activation2 = BackwardReLU1()
        self.affine2 = tf.keras.layers.Dense(
            units=32, kernel_initializer=tf.keras.initializers.lecun_normal(),
            bias_initializer=tf.keras.initializers.constant(0.5),
            name='affine2'
        )
        self.affine2(tf.zeros([1, 32]))
        tmp = tf.reduce_sum(self.affine2.kernel, axis=0)
        self.affine2.bias.assign(0.5 - 0.5 * tmp)

        self.activation3 = BackwardReLU1()
        self.affine3 = tf.keras.layers.Dense(
            units=1, kernel_initializer=tf.keras.initializers.zeros(),
            name='affine3'
        )

    def call(self, inputs, training=None, mask=None):
        p, q = inputs
        h_p = self.feature(p)
        h_q = self.feature(q)

        h0pre = self.attention_like((h_p, h_q))
        r_upper = tf.math.reduce_mean(tf.math.square(tf.maximum(h0pre - 1, 0)))
        r_lower = tf.math.reduce_mean(tf.math.square(tf.minimum(h0pre, 0)))
        self.add_loss(r_upper + r_lower)

        if self.n_layers >= 3:
            h0post = self.activation1(h0pre)
            h1pre = self.affine1(h0post)

            d = {'pre/activation1': h0pre[0], 'post/activation1': h0post[0]}
        else:
            h1pre = h0pre
            d = {}

        h1post = self.activation2(h1pre)
        h2pre = self.affine2(h1post)

        h2post = self.activation3(h2pre)
        h = self.affine3(h2post)

        outputs = h

        d.update({'pre/activation2': h1pre[0], 'post/activation2': h1post[0],
                  'pre/activation3': h2pre[0], 'post/activation3': h2post[0]})
        return outputs, d


class AttentionLikeTransformation(tf.keras.layers.Layer):
    def __init__(self, value_size, query_size, skip_value_network,
                 name='attention_like'):
        super().__init__(name=name)
        self.value_size = value_size
        self.query_size = query_size
        self.skip_value_network = skip_value_network

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        if self.skip_value_network:
            self.value_net = Identity()
        else:
            self.value_net = LocalAffine(output_dim=self.value_size,
                                         name='value_network')
        self.query_net = LocalAffine(output_dim=self.query_size,
                                     name='query_network')

        self.bias = self.add_weight(
            'bias', shape=[16 * self.query_size], dtype=tf.float32,
            initializer=tf.keras.initializers.constant(0.5), trainable=True
        )

        self.built = True

    def call(self, inputs, **kwargs):
        h_p, h_q = inputs

        h_p = tf.reshape(h_p, [-1, 16, 16])
        h_q = tf.reshape(h_q, [-1, 16, 16])

        # [batch, 16, 16]
        value = self.value_net(h_p)
        # [batch, 16, 2]
        query = self.query_net(h_q)
        # [batch, 16, 2]
        h = tf.linalg.matmul(value, query)
        h = tf.reshape(h, [-1, 32])

        outputs = h + self.bias
        return outputs


class LocalAffine(tf.keras.layers.Layer):
    def __init__(self, output_dim, input_dim=16, element_size=16,
                 name='local_affine'):
        super().__init__(name=name)
        self.output_dim = output_dim
        self.input_dim = input_dim
        self.element_size = element_size

    # noinspection PyAttributeOutsideInit
    def build(self, input_shape):
        self.kernel = self.add_weight(
            'kernel',
            shape=[self.element_size, self.input_dim, self.output_dim],
            dtype=tf.float32, trainable=True,
            initializer=tf.keras.initializers.lecun_normal()
        )
        self.bias = self.add_weight(
            'bias',
            shape=[self.element_size, self.output_dim], dtype=tf.float32,
            trainable=True, initializer=tf.keras.initializers.zeros()
        )

        self.built = True

    def call(self, inputs, **kwargs):
        # A[batch, i, j], B[i, j, k] -> C[batch, i, k]
        # iごとにjで内積、すなわち、A[batch, i][j]*B[i][j, k]をしたい(ベクトル×行列)
        # A[batch, i, :, j], B[:, i, j, k]にするとmatmulでbroadcastが適応される
        a = tf.expand_dims(inputs, axis=2)
        b = tf.expand_dims(self.kernel, axis=0)
        c = tf.linalg.matmul(a, b)

        # 多くなった軸を削除
        outputs = tf.squeeze(c, axis=2) + self.bias
        return outputs


class Identity(tf.keras.layers.Layer):
    def __init__(self):
        super().__init__()

    def call(self, inputs, **kwargs):
        return inputs
