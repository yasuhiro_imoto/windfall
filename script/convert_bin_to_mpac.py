#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
nnue 256x2, 32, 32, 1のパラメータをMessagePackに変換する
パラメータの要素数は固定なので、ネットワーク構造を変更したときはそれに合わせて変更する必要がある
"""

import json
import struct
from pathlib import Path

import click
import msgpack
import numpy as np

__author__ = 'Yasuhiro'
__date__ = '2020/08/09'


def load_parameter(path):
    with path.open('rb') as f:
        version = struct.unpack('<I', f.read(4))[0]
        hash_value1 = struct.unpack('<I', f.read(4))[0]

        size = struct.unpack('<I', f.read(4))[0]
        model_architecture = struct.unpack(
            '<{}s'.format(size), f.read(size)
        )[0]

        hash_value2 = struct.unpack('<I', f.read(4))[0]

        feature_bias = np.asarray(struct.unpack('<256h', f.read(256 * 2)),
                                  dtype=np.int16)
        n = 256 * 125388
        feature_kernel = np.asarray(
            struct.unpack('<{}h'.format(n), f.read(n * 2)), dtype=np.int16
        )

        hash_value3 = struct.unpack('<I', f.read(4))[0]

        hidden1_bias = np.asarray(struct.unpack('<32i', f.read(32 * 4)),
                                  dtype=np.int32)
        n = 32 * 512
        hidden1_kernel = np.asarray(
            struct.unpack('<{}b'.format(n), f.read(n)), dtype=np.int8
        )

        hidden2_bias = np.asarray(struct.unpack('<32i', f.read(32 * 4)),
                                  dtype=np.int32)
        hidden2_kernel = np.asarray(struct.unpack('<1024b', f.read(1024)),
                                    dtype=np.int8)

        hidden3_bias = np.asarray(struct.unpack('<1i', f.read(4)),
                                  dtype=np.int32)
        hidden3_kernel = np.asarray(struct.unpack('<32b', f.read(32)),
                                    dtype=np.int8)

    d = {'version': version, 'hash1': hash_value1, 'hash2': hash_value2,
         'model_architecture': model_architecture,
         'feature_bias': feature_bias, 'feature_kernel': feature_kernel,
         'hash3': hash_value3,
         'hidden1_bias': hidden1_bias, 'hidden1_kernel': hidden1_kernel,
         'hidden2_bias': hidden2_bias, 'hidden2_kernel': hidden2_kernel,
         'hidden3_bias': hidden3_bias, 'hidden3_kernel': hidden3_kernel}
    return d


@click.command()
@click.option('--input-path', type=click.Path(exists=True))
@click.option('--output-path', type=click.Path())
def cmd(input_path, output_path):
    d = load_parameter(Path(input_path))

    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    with output_path.open('wb') as f:
        header = {b'author': b'',
                  b'version': str(d['version']).encode('utf-8'),
                  b'comment': d['model_architecture']}
        tmp = msgpack.packb(header)
        f.write(tmp)

        feature = {b'kernel': d['feature_kernel'].tobytes(),
                   b'bias': d['feature_bias'].tobytes()}
        tmp = msgpack.packb(feature, use_bin_type=False)
        f.write(tmp)

        hidden1 = {b'kernel': d['hidden1_kernel'].tobytes(),
                   b'bias': d['hidden1_bias'].tobytes(),
                   b'previous': []}
        hidden2 = {b'kernel': d['hidden2_kernel'].tobytes(),
                   b'bias': d['hidden2_bias'].tobytes(),
                   b'previous': {b'previous': hidden1}}
        hidden3 = {b'kernel': d['hidden3_kernel'].tobytes(),
                   b'bias': d['hidden3_bias'].tobytes(),
                   b'previous': {b'previous': hidden2}}
        tmp = msgpack.packb(hidden3, use_bin_type=False)
        f.write(tmp)


def main():
    cmd()


if __name__ == '__main__':
    main()
