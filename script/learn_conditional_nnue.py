#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path

import click
import cshogi
import joblib
import numpy as np
import pandas as pd
import tensorflow as tf
import toml
from tqdm import trange

from make_record1 import BonaPiece
from make_record2 import fv40
from nnue import ConditionalNNUE

__author__ = 'Yasuhiro'
__date__ = '2020/02/22'


def make_dataset(record_path, batch_size):
    files = tf.io.gfile.glob(record_path)
    dataset = tf.data.TFRecordDataset(
        filenames=files, compression_type='GZIP', num_parallel_reads=10
    )
    dataset = dataset.shuffle(batch_size * 10)
    dataset = dataset.map(parse, num_parallel_calls=10).batch(batch_size)
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def parse(serialized):
    features = {
        'p': tf.io.FixedLenFeature([38], tf.int64),
        'q': tf.io.FixedLenFeature([38], tf.int64),
        'score': tf.io.FixedLenFeature([], tf.int64),
        'ply': tf.io.FixedLenFeature([], tf.int64),
        'result': tf.io.FixedLenFeature([], tf.int64)
    }
    example = tf.io.parse_single_example(serialized, features)

    score = tf.cast(example['score'], tf.float32)
    result = tf.cast(example['result'], tf.float32)
    elmo_lambda = 0.9
    ponanza_constant = 600.0
    target = ((1 - elmo_lambda) * (result + 1) * 0.5 +
              elmo_lambda * tf.math.sigmoid(score / ponanza_constant))
    target = tf.clip_by_value(target, clip_value_max=0.99, clip_value_min=0.01)

    v = tf.reshape(target, [-1])
    offset_upper = tf.keras.losses.binary_crossentropy(
        v, tf.math.minimum(v + 0.1, 0.95), from_logits=False
    )
    offset_lower = tf.keras.losses.binary_crossentropy(
        v, tf.math.maximum(v - 0.1, 0.05), from_logits=False
    )
    # クロスエントロピーの計算のために変更したshapeをもとに戻す
    offset = tf.squeeze(tf.math.maximum(offset_upper, offset_lower))

    size = tf.constant(1548, dtype=tf.int64)
    p_ou = tf.math.divide(example['p'][0], size)
    q_ou = tf.math.divide(example['q'][0], size)
    index = tf.reshape(p_ou * 81 + q_ou, [-1, 1])

    data = {
        'p': tf.cast(example['p'], tf.int32),
        'q': tf.cast(example['q'], tf.int32),
        'index': tf.cast(index, tf.int32),
        'target': target, 'offset': offset
    }
    return data


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def initialize_multi(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    mirrored_strategy = tf.distribute.MirroredStrategy()
    with mirrored_strategy.scope():
        network = ConditionalNNUE()
        optimizer = tf.keras.optimizers.SGD(
            learning_rate=config['learning_rate'], momentum=0.9
        )
        metrics_ce = tf.keras.metrics.BinaryCrossentropy(
            'cross_entropy', from_logits=True
        )
        metrics_entropy = tf.keras.metrics.BinaryCrossentropy(
            'entropy', from_logits=False
        )
        metrics_regularizer = tf.keras.metrics.Mean('regularizer')
        metrics_loss = tf.keras.metrics.Mean('loss')

        ckpt = tf.train.Checkpoint(
            step=tf.Variable(1), network=network, optimizer=optimizer
        )
        manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                             max_to_keep=2)
        ckpt.restore(manager.latest_checkpoint)

    global_batch_size = config['batch_size']
    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=global_batch_size)
    dist_dataset = mirrored_strategy.experimental_distribute_dataset(dataset)

    weight = tf.constant(1.0 / global_batch_size)
    lambda_ = tf.constant(config['lambda'])
    
    with mirrored_strategy.scope():
        def step(dataset_inputs):
            x = (dataset_inputs['index'],
                 dataset_inputs['p'], dataset_inputs['q'])
            y = dataset_inputs['target']
            o = dataset_inputs['offset']

            for _ in range(10):
                with tf.GradientTape() as tape:
                    scores = network(x)
                    scores = tf.squeeze(scores)

                    cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(
                        labels=y, logits=scores
                    )
                    tmp = tf.reduce_sum(tf.maximum(cross_entropy - o, 0))
                    regularizer = tf.reduce_sum(network.losses)
                    loss = tmp * weight + lambda_ * regularizer
                gradients = tape.gradient(loss, network.trainable_variables)
                optimizer.apply_gradients(zip(
                    gradients, network.trainable_variables
                ))

                metrics_ce.update_state(y_true=y, y_pred=scores)
                metrics_regularizer.update_state(regularizer)
                metrics_loss.update_state(loss)
            # 同じ値なのでループの外側で対応
            metrics_entropy.update_state(y_true=y, y_pred=y)
            return loss

        @tf.function
        def distributed_step(dataset_inputs):
            per_replica_losses = mirrored_strategy.experimental_run_v2(
                step, args=(dataset_inputs,))
            return mirrored_strategy.reduce(
                tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None
            )

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    with mirrored_strategy.scope():
        for i, inputs in zip(trange(config['iterations']), dist_dataset):
            distributed_step(inputs)

            if (i + 1) % 1000 == 0:
                with summary_writer.as_default():
                    tf.summary.scalar(
                        'metrics/cross_entropy', metrics_ce.result(),
                        step=int(ckpt.step.values[0])
                    )
                    tf.summary.scalar(
                        'metrics/kl',
                        metrics_ce.result() - metrics_entropy.result(),
                        step=int(ckpt.step.values[0])
                    )
                    tf.summary.scalar(
                        'loss/regularization', metrics_regularizer.result(),
                        step=int(ckpt.step.values[0])
                    )
                    tf.summary.scalar(
                        'loss/total', metrics_loss.result(),
                        step=int(ckpt.step.values[0])
                    )

                metrics_ce.reset_states()
                metrics_entropy.reset_states()
                metrics_regularizer.reset_states()
                metrics_loss.reset_states()

                ckpt.step.assign_add(1)
                manager.save()


def main():
    cmd()


if __name__ == '__main__':
    main()
