#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import random

import shogi
import cshogi

import make_record1 as make_record1
import make_record2

__author__ = 'Yasuhiro'
__date__ = '2019/9/22'


class MyTestCase(unittest.TestCase):
    def _check(self, board1, board2):
        val1 = make_record1.fv40(board1)
        val2 = make_record2.fv40(board2)

        self.assertEqual(val1[0], val2[0])
        self.assertEqual(val1[1], val2[1])
        self.assertSetEqual(set(val1[2]), set(val2[2]))
        self.assertSetEqual(set(val1[3]), set(val2[3]))

    def test_hirate(self):
        board1 = shogi.Board()
        board2 = cshogi.Board()

        self._check(board1, board2)

    def test_cshogi_sample(self):
        # https://github.com/TadaoYamaoka/cshogi/blob/master/README.rst
        sfen = ('ln4skl/3r1g3/1p2pgnp1/p1ppsbp1p/5p3/2PPP1P1P/PPBSSG1P1'
                '/2R3GK1/LN5NL b P 43')

        board1 = shogi.Board(sfen)
        board2 = cshogi.Board(sfen)

        self._check(board1, board2)

    def test_evaltest_nnue_sample1(self):
        # https://github.com/bleu48/shogi-eval/blob/master/evaltest_nnue.py
        sfen = ('lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL '
                'b - 1')

        board1 = shogi.Board(sfen)
        board2 = cshogi.Board(sfen)

        self._check(board1, board2)

    def test_evaltest_nnue_sample2(self):
        # https://github.com/bleu48/shogi-eval/blob/master/evaltest_nnue.py
        sfen = ('lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL '
                'w - 1')

        board1 = shogi.Board(sfen)
        board2 = cshogi.Board(sfen)

        self._check(board1, board2)

    def test_evaltest_nnue_sample3(self):
        # https://github.com/bleu48/shogi-eval/blob/master/evaltest_nnue.py
        sfen = ('l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL '
                'w RGgsn5p 1')

        board1 = shogi.Board(sfen)
        board2 = cshogi.Board(sfen)

        self._check(board1, board2)

    def test_evaltest_nnue_sample4(self):
        # https://github.com/bleu48/shogi-eval/blob/master/evaltest_nnue.py
        sfen = ('l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL '
                'b RGgsn5p 1')

        board1 = shogi.Board(sfen)
        board2 = cshogi.Board(sfen)

        self._check(board1, board2)

    def test_random1(self):
        random.seed(1)

        board1 = shogi.Board()
        board2 = cshogi.Board()
        for i in range(512):
            legal_moves = list(board2.legal_moves)
            if len(legal_moves) == 0:
                break
            move = random.choice(legal_moves)
            move = cshogi.move_to_usi(move)

            board1.push_usi(move)
            board2.push_usi(move)

            with self.subTest(i=i, board1=board1, board2=board2):
                self._check(board1, board2)

    def test_random2(self):
        random.seed(2)

        board1 = shogi.Board()
        board2 = cshogi.Board()
        for i in range(512):
            legal_moves = list(board2.legal_moves)
            if len(legal_moves) == 0:
                break
            move = random.choice(legal_moves)
            move = cshogi.move_to_usi(move)

            board1.push_usi(move)
            board2.push_usi(move)

            with self.subTest(i=i, board1=board1, board2=board2):
                self._check(board1, board2)

    def test_random3(self):
        random.seed(3)

        board1 = shogi.Board()
        board2 = cshogi.Board()
        for i in range(512):
            legal_moves = list(board2.legal_moves)
            if len(legal_moves) == 0:
                break
            move = random.choice(legal_moves)
            move = cshogi.move_to_usi(move)

            board1.push_usi(move)
            board2.push_usi(move)

            with self.subTest(i=i, board1=board1, board2=board2):
                self._check(board1, board2)


if __name__ == '__main__':
    unittest.main()
