#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from gym.envs.registration import register

try:
    import sys
    from pathlib import Path
    path = Path(__file__).absolute()
    sys.path.append(str(path.parents[2] / 'thirdparty/Ayane/source/shogi'))
    # sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane

__author__ = 'Yasuhiro'
__date__ = '2019/11月/21'

register(id='suisho-v0', entry_point='wf_gym.envs:SuishoRandomized')


def make_usi(config):
    options = {
        'Hash': str(config['hash_size']),
        'Threads': str(config['threads']),
        'NetworkDelay': '0',
        'NetworkDelay2': '0',
        'DepthLimit': str(config['depth_limit']),
        'ResignValue': str(config['resign_value']),
        # 'BookMoves': '0',
        'EvalDir': config['eval_dir']
    }

    usi = ayane.UsiEngine()
    usi.set_engine_options(options)
    usi.connect(config['path'])

    return usi
