#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from collections import defaultdict

import cshogi
import gym
import numpy as np
from gym import spaces
# SVGをPNGに変換してrgb_arrayサポート
# from cairosvg import svg2png

try:
    import sys
    sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane

__author__ = 'Yasuhiro'
__date__ = '2019/11月/21'


class SuishoRandomized(gym.Env):
    metadata = {'render.modes': ['human', 'svg', 'ansi', 'sfen']}

    def __init__(self, usi, random_rate):
        super().__init__()
        self.usi = usi
        self.random_rate = random_rate

        self.board = cshogi.Board()
        self.sfen = "startpos moves"
        # learning agentがどちらの手番(先手=0 , 後手=1)を持っているか
        # ステップ数を0から数えるとcounter%2==turnの時が自分の手番
        self.turn = np.random.randint(2)
        # 手数、512手が最大
        self.counter = 0
        self.last_action = None
        self.actions = []

        self.repetition_hash = defaultdict(int)

        self.observation_space = spaces.Box(0, len(cshogi.PIECES) - 1, (9, 9),
                                            dtype=np.uint8)
        # actionはmoveを直接受け付ける
        # sample()は非合法手も含む
        self.action_space = gym.spaces.Discrete(16777216)

    def __del__(self):
        self.usi.disconnect()

    def reset(self):
        self.turn = np.random.randint(2)

        self._initialize()

        return np.asarray(self.board.pieces, dtype=np.uint8).reshape((9, 9))

    def render(self, mode='human'):
        if mode == 'svg':
            return self.board.to_svg(lastmove=self.last_action)
        elif mode == 'ansi':
            print(self.board)
        elif mode == 'sfen':
            print(self.board.sfen())
        elif mode == 'rgb_array':
            pass
        else:
            return self.board

    def step(self, move):
        # agentの行動を処理
        reward, done, info = self._step_agent(move)
        if done:
            observation = np.empty(32, dtype=np.uint8)
            return observation, reward, done, info

        # 環境の行動を処理
        observation, reward, done, info = self._step_env()
        return observation, reward, done, info

    def _step_agent(self, action):
        # 千日手の関係の情報
        info = {'draw': cshogi.NOT_REPETITION}

        # agentの行動を処理
        if action == 0:
            # 投了
            reward = -1
            done = True
            return reward, done, info

        assert self.board.is_legal(action), (cshogi.move_to_usi(action),
                                             action)
        usi = cshogi.move_to_usi(action)
        self.sfen += ' ' + usi

        self._update_record(action)
        key = self._update_board(action)

        reward, done, is_draw = self._get_reward(key=key)
        if done:
            info['draw'] = is_draw
        # エピソードの最後の行動に対する目標値の設定が面倒になるので、最大手数での終端にしない
        # elif self.counter >= 512:
        #     # 最大手数に到達
        #     reward = 0
        #     done = True
        return reward, done, info

    def _step_env(self):
        # エピソード中はboardを書き換えるので、状態をコピーする
        # packed sfen形式
        observation = np.empty(32, dtype=np.uint8)
        # 千日手の関係の情報
        info = {'draw': cshogi.NOT_REPETITION}

        # 一手詰めをチェック
        # 次の行動が投了になると対応する局面に困るので、ここで打ち切る
        if not self.board.is_check():
            move = self.board.mate_move_in_1ply()
            if move != 0:
                # 詰みがあった
                reward = -1
                done = True
                # observationは使わない
                return observation, reward, done, info

        move = self._select_move(sfen=self.sfen)
        if move == 'resign':
            # 投了
            reward = 1
            done = True
            # observationは使わない
            return observation, reward, done, info
        elif move == 'win':
            reward = -1
            done = True
            # observationは使わない
            return observation, reward, done, info

        self.sfen += ' ' + move

        action = self.board.move_from_usi(move)
        assert self.board.is_legal(action), (move, action)
        self._update_record(action)
        key = self._update_board(action)

        reward, done, is_draw = self._get_reward(key=key)
        reward = -reward
        if is_draw is not None:
            info['draw'] = self._flip_repetition(is_draw)

        self.board.to_psfen(observation)

        if not done and self.counter >= 512:
            # 最大手数に到達
            reward = 0
            done = True
            info['draw'] = 100  # max moveに到達したことを表す
        return observation, reward, done, info

    def _update_record(self, action):
        # 手数を更新
        self.counter += 1

        self.last_action = action
        # 下位16bitのみを記録
        self.actions.append(action & 0xFFFF)

    def _update_board(self, action):
        self.board.push(action)
        key = self.board.zobrist_hash()
        self.repetition_hash[key] += 1
        return key

    @staticmethod
    def _flip_repetition(is_draw):
        if is_draw == cshogi.REPETITION_WIN:
            is_draw = cshogi.REPETITION_LOSE
        else:
            is_draw = cshogi.REPETITION_WIN
        return is_draw

    def _get_reward(self, key):
        if self.repetition_hash[key] == 4:
            # 千日手
            done = True
            # 連続王手
            is_draw = self.board.is_draw()
            if is_draw == cshogi.REPETITION_WIN:
                # 相手の手番なので報酬は反対になる
                reward = -1
            elif is_draw == cshogi.REPETITION_LOSE:
                reward = 1
            else:
                reward = 0
        else:
            done = self.board.is_game_over()
            if done:
                reward = 1
            else:
                reward = 0
            is_draw = cshogi.NOT_REPETITION
        return reward, done, is_draw

    def get_moves(self):
        return self.usi.get_moves()

    def _evaluate(self, sfen):
        self.usi.usi_position(sfen)
        self.usi.usi_go_and_wait_bestmove("time 0 byoyomi 1000")
        return self.usi.think_result

    def _select_move(self, sfen):
        self.usi.usi_position(sfen)

        r = np.random.rand()
        if r < self.random_rate:
            # ランダムに動く
            legal_moves = self.usi.get_moves().split()
            if len(legal_moves) == 0:
                return 'resign'
            move = random.choice(legal_moves)
            # print('random:', move)
            return move
        else:
            # 最善手を選ぶ
            self.usi.usi_go_and_wait_bestmove("time 0 byoyomi 1000")
            bestmove = self.usi.think_result.bestmove
            # print('best:', bestmove)
            return bestmove

    def _initialize(self):
        # 局面を進める手数
        n = np.random.randint(16) * 2 + (1 - self.turn)

        self.counter = 0
        self.sfen = "startpos moves"
        self.actions = []

        self.board.reset()
        self.repetition_hash.clear()
        key = self.board.zobrist_hash()
        self.repetition_hash[key] += 1

        for _ in range(n):
            move = self._select_move(sfen=self.sfen)

            self.sfen += ' ' + move
            action = self.board.move_from_usi(move)
            self._update_record(action)
            self._update_board(action)
