#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .suisho import SuishoRandomized

__author__ = 'Yasuhiro'
__date__ = '2019/11月/21'

__all__ = ['SuishoRandomized']
