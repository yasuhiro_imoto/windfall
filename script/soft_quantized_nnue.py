#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path

import numpy as np
import sonnet as snt
import tensorflow as tf
import click
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
# noinspection PyProtectedMember
from mlflow import log_params
from scipy.special import logit

from learn_nnue import (ClippedReLU, make_clip_metrics, make_cost,
                        make_dataset, get_optimizer, floor)

__author__ = 'Yasuhiro'
__date__ = '2019/08/24'


class SoftQuantization(snt.AbstractModule):
    def __init__(self, lower, upper, name='soft_quantization'):
        super().__init__(name=name)
        self.lower, self.upper = lower, upper

    def _build(self, inputs):
        inputs = tf.clip_by_value(inputs, clip_value_min=self.lower,
                                  clip_value_max=self.upper)

        logit_alpha = tf.get_variable(
            'logit_alpha', shape=[], dtype=tf.float32,
            initializer=tf.initializers.constant(logit(0.2)), trainable=True
        )
        alpha = tf.math.sigmoid(logit_alpha)
        log_alpha = tf.math.log_sigmoid(logit_alpha)
        self.alpha = alpha

        # k = tf.math.log(2 / alpha - 1)
        k = tf.math.log(2 - alpha) - log_alpha

        v = tf.math.floor(inputs)
        m = v + 0.5

        s = 1 / tf.math.tanh(0.5 * k)
        phi = s * tf.math.tanh(k * (inputs - m))

        tmp = tf.math.sign(phi) - phi
        outputs = (phi + tf.stop_gradient(tmp) + 1) * 0.5 + v

        return outputs


class FeatureTransformer(snt.AbstractModule):
    """
    Half-KPの特徴ベクトルを256次元のベクトルに変換する
    """
    def __init__(self, name='feature_transformer'):
        super().__init__(name=name)

    def _build(self, inputs):
        """
        Half-KPについてのスパースな変換
        :param inputs:
        :type inputs: tf.SparseTensor
        :return:
        """
        # Half-KPのベクトルサイズはこれでいいはず
        half_kp_size = 125388
        self.weight = tf.get_variable(
            'weight', shape=[half_kp_size, 256], dtype=tf.float32,
            initializer=tf.initializers.random_normal(stddev=5e2),
            trainable=True
        )
        self.bias = tf.get_variable(
            'bias', shape=[256], dtype=tf.float32,
            initializer=tf.initializers.random_normal(stddev=1e1),
            trainable=True
        )

        self.sq_w = SoftQuantization(lower=-2 ** 15, upper=2 ** 15 - 1)
        self.sq_b = SoftQuantization(lower=-2 ** 15, upper=2 ** 15 - 1)

        self.w = self.sq_w(self.weight)
        self.b = self.sq_b(self.bias)

        # テーブルの各要素に制限は与えない
        h = tf.nn.embedding_lookup_sparse(
            params=self.w, sp_ids=inputs, sp_weights=None,
            combiner='sum', max_norm=None
        )

        outputs = h + self.b
        return outputs


class Affine(snt.AbstractModule):
    def __init__(self, shape, name='affine'):
        super().__init__(name=name)
        self.shape = shape

    def _build(self, inputs):
        self.weight = tf.get_variable(
            'weight', shape=self.shape, dtype=tf.float32,
            initializer=tf.initializers.random_normal(stddev=1e1),
            trainable=True
        )
        self.bias = tf.get_variable(
            'bias', shape=self.shape[1], dtype=tf.float32,
            initializer=tf.initializers.zeros(), trainable=True
        )

        self.sq_w = SoftQuantization(lower=-2 ** 7, upper=2 ** 7 - 1)
        self.sq_b = SoftQuantization(lower=-2 ** 31, upper=2 ** 31 - 1)

        self.w = self.sq_w(self.weight)
        self.b = self.sq_b(self.bias)

        outputs = tf.nn.xw_plus_b(x=inputs, weights=self.w, biases=self.b)
        return outputs


class Sigmoid(snt.AbstractModule):
    def __init__(self, name='sigmoid'):
        super().__init__(name=name)

    def _build(self, inputs):
        # [-512, 512)を90で割るといい感じ
        h = tf.math.sigmoid(inputs / 90.0) * 127.0

        self.sq = SoftQuantization(lower=0, upper=127)
        outputs = self.sq(h)

        self.alpha = self.sq.alpha
        return outputs


class Network(snt.AbstractModule):
    def __init__(self, name='network'):
        super().__init__(name=name)
        with self._enter_variable_scope():
            self.transformer = FeatureTransformer()
            self.affine32x512 = Affine([512, 32], name='affine32x512')
            self.affine32x32 = Affine([32, 32], name='affine32x32')
            self.affine1x32 = Affine([32, 1], name='affine1x32')
            self.sigmoid1 = Sigmoid()
            self.sigmoid2 = Sigmoid()

    def _build(self, inputs, is_training):
        p = self.transformer(inputs['p'])
        q = self.transformer(inputs['q'])
        h = tf.concat([p, q], axis=1)
        h = floor(h / 8.0)
        h = tf.identity(h, name='transformed')
        h = ClippedReLU()(h, 0.1)

        h = self.affine32x512(h)
        h = h / 256.0
        h = tf.identity(h, name='affine1')
        h = self.sigmoid1(h)

        h = self.affine32x32(h)
        h = h / 64.0
        h = tf.identity(h, name='affine2')
        h = self.sigmoid2(h)

        h = self.affine1x32(h)
        outputs = h / 16.0

        return outputs


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--record-dir', type=click.Path(exists=True, file_okay=False))
@click.option('--batch-size', type=int, default=1000)
@click.option('--model-dir', type=click.Path())
@click.option('--epochs', type=int, default=10)
@click.option('--n-iterations-per-epoch', type=int, default=1000)
@click.option('--elmo-lambda', type=float, default=1.0)
@click.option('--ponanza-constant', type=float, default=600)
@click.option('--optimizer', type=str)
@click.option('--lr', type=float)
@click.option('--final-lr', type=float, default=0.1)
@click.option('--beta1', type=float, default=0.9)
@click.option('--beta2', type=float, default=0.999)
@click.option('--gamma', type=float, default=1e-3)
@click.option('--epsilon', type=float, default=1e-8, help='rmsprop:1e-10')
@click.option('--rho', type=float, default=0.95)
@click.option('--decay', type=float, default=0.9)
@click.option('--momentum', type=float, default=0.0)
@click.option('--centered', is_flag=True, default=False)
@click.option('--use_nesterov', is_flag=True, default=False)
@click.option('--score-limit', type=float, default=5.0)
def learn(record_dir, batch_size, model_dir, epochs, n_iterations_per_epoch,
          elmo_lambda, ponanza_constant, optimizer, lr, final_lr, beta1, beta2,
          gamma, epsilon, rho, decay, momentum, centered, use_nesterov,
          score_limit):
    iterator, next_element = make_dataset(record_dir=Path(record_dir),
                                          batch_size=batch_size)
    model = Network()

    prediction = model(next_element, True)
    kl = make_cost(prediction=prediction, next_element=next_element,
                   elmo_lambda=elmo_lambda, ponanza_constant=ponanza_constant,
                   score_limit=score_limit)
    alpha_list1 = [
        model.affine32x512.sq_w.alpha, model.affine32x512.sq_b.alpha,
        model.affine32x32.sq_w.alpha, model.affine32x32.sq_b.alpha,
        model.affine1x32.sq_w.alpha, model.affine1x32.sq_b.alpha
    ]
    alpha_list2 = [
        model.transformer.sq_w.alpha, model.transformer.sq_b.alpha
    ]

    optimizer_name = optimizer
    global_step = tf.train.create_global_step()
    optimizer = get_optimizer(
        name=optimizer, lr=lr, final_lr=final_lr, beta1=beta1, beta2=beta2,
        gamma=gamma, epsilon=epsilon, rho=rho, decay=decay, momentum=momentum,
        centered=centered, use_nesterov=use_nesterov
    )
    total_cost = (tf.reduce_mean(kl) + tf.add_n(alpha_list1) * 1e-4 +
                  tf.add_n(alpha_list2))
    grads_and_vars = optimizer.compute_gradients(total_cost)
    opt_op = optimizer.apply_gradients(grads_and_vars=grads_and_vars,
                                       global_step=global_step)

    graph = tf.get_default_graph()
    with tf.variable_scope('metrics') as vs:
        mean1 = tf.metrics.mean(kl)
        mean2 = tf.metrics.mean(tf.square(kl))

        total1 = tf.metrics.mean(total_cost)
        total2 = tf.metrics.mean(tf.square(total_cost))

        transformed = graph.get_tensor_by_name('network_1/transformed:0')
        t0, t1 = tf.split(transformed, num_or_size_splits=2, axis=1)
        transformed = tf.concat([t0, t1], axis=0)

        mean_transformed = make_clip_metrics(
            transformed, upper_bound=127, lower_bound=0
        )

        local_variables = tf.local_variables(scope=vs.name)
        reset_op = tf.variables_initializer(local_variables)
    summary_list = []
    histogram_list = []
    for g, v in grads_and_vars:
        if g is None:
            continue
        histogram_list.append(
            tf.summary.histogram('gradient/{}'.format(v.name[:-2]), g)
        )
    histogram_list.extend([
        tf.summary.histogram('embedding/black', t0),
        tf.summary.histogram('embedding/white', t1),
        tf.summary.histogram('embedding/affine1',
                             graph.get_tensor_by_name('network_1/affine1:0')),
        tf.summary.histogram('embedding/affine2',
                             graph.get_tensor_by_name('network_1/affine2:0'))
    ])
    for name, ops in (('transformed', mean_transformed),):
        for field in ('upper', 'lower', 'all'):
            summary_list.extend([
                tf.summary.scalar(
                    'clip_{name}/{field}_max'.format(name=name, field=field),
                    tf.reduce_max(getattr(ops, field))
                ),
                tf.summary.scalar(
                    'clip_{name}/{field}_min'.format(name=name, field=field),
                    tf.reduce_min(getattr(ops, field))
                ),
                tf.summary.scalar(
                    'clip_{name}/{field}_mean'.format(name=name, field=field),
                    tf.reduce_mean(getattr(ops, field))
                )
            ])
            histogram_list.append(
                tf.summary.histogram(
                    'clip_{field}/{name}'.format(field=field, name=name),
                    getattr(ops, field)
                )
            )
    summary_op = tf.summary.merge([
        tf.summary.scalar('cost/mean', mean1[0]),
        tf.summary.scalar('cost/variance', mean2[0] - tf.square(mean1[0])),
        tf.summary.scalar('cost/total/mean', total1[0]),
        tf.summary.scalar('cost/total/variance',
                          total2[0] - tf.square(total1[0])),
        tf.summary.scalar('alpha/sigmoid1', model.sigmoid1.alpha),
        tf.summary.scalar('alpha/sigmoid2', model.sigmoid2.alpha),
        tf.summary.scalar('alpha/transformer/weight',
                          model.transformer.sq_w.alpha),
        tf.summary.scalar('alpha/transformer/bias',
                          model.transformer.sq_b.alpha),
        tf.summary.scalar('alpha/affine1/weight',
                          model.affine32x512.sq_w.alpha),
        tf.summary.scalar('alpha/affine1/bias',
                          model.affine32x512.sq_b.alpha),
        tf.summary.scalar('alpha/affine2/weight',
                          model.affine32x32.sq_w.alpha),
        tf.summary.scalar('alpha/affine2/bias',
                          model.affine32x32.sq_b.alpha),
    ] + summary_list)
    update_list = [mean1[1], mean2[1], total1[1], total2[1]]
    update_op = tf.group(*update_list, *mean_transformed.update)
    histogram_op = tf.summary.merge([
        tf.summary.histogram('transformer/weight', model.transformer.w),
        tf.summary.histogram('transformer/bias', model.transformer.b),
        tf.summary.histogram('affine32x512/weight', model.affine32x512.w),
        tf.summary.histogram('affine32x512/bias', model.affine32x512.b),
        tf.summary.histogram('affine32x32/weight', model.affine32x32.w),
        tf.summary.histogram('affine32x32/bias', model.affine32x32.b),
        tf.summary.histogram('affine1x32/weight', model.affine1x32.w),
        tf.summary.histogram('affine1x32/bias', model.affine1x32.b)
    ] + histogram_list)

    model_dir = Path(model_dir)
    if not model_dir.exists():
        model_dir.mkdir(parents=True)

    saver = tf.train.Saver()
    writer = tf.summary.FileWriter(model_dir)

    parameters = dict(
        record_dir=record_dir, batch_size=batch_size,
        model_dir=Path(model_dir), elmo_lambda=elmo_lambda,
        ponanza_constant=ponanza_constant, optimizer=optimizer_name,
        lr=lr, final_lr=final_lr, beta1=beta1, beta2=beta2, gamma=gamma,
        epsilon=epsilon, rho=rho, decay=decay, momentum=momentum,
        centered=centered, use_nesterov=use_nesterov, score_limit=score_limit
    )
    log_params(params=parameters)

    config = tf.ConfigProto(allow_soft_placement=True,
                            gpu_options=tf.GPUOptions(allow_growth=True))
    with tf.Session(config=config) as sess:
        checkpoint = tf.train.get_checkpoint_state(str(model_dir))
        if checkpoint:
            sess.run(tf.local_variables_initializer())

            path = checkpoint.model_checkpoint_path
            saver.restore(sess, path)

            # 出力の符号を反転させるために重みを反転
            # v = tf.trainable_variables(scope='network/affine1x32')
            # flip_ops = tf.group([tf.assign(u, -u) for u in v])
            # sess.run(flip_ops)
        else:
            sess.run(tf.group(tf.global_variables_initializer(),
                              tf.local_variables_initializer()))

        sess.run(iterator.initializer)

        progress = tqdm(total=n_iterations_per_epoch * epochs, mininterval=10)
        for _ in range(epochs):
            try:
                for _ in range(n_iterations_per_epoch):
                    sess.run([opt_op, update_op])
                    progress.update()
            except tf.errors.OutOfRangeError:
                break
            finally:
                summary, step = sess.run([summary_op, global_step])
                writer.add_summary(summary=summary, global_step=step)

                if step % (n_iterations_per_epoch * 25) == 0:
                    summary = sess.run(histogram_op)
                    writer.add_summary(summary=summary, global_step=step)

                sess.run(reset_op)

                saver.save(sess=sess, save_path=str(model_dir / 'model'),
                           global_step=global_step)


@cmd.command()
def check_soft_quantization():
    x = np.linspace(-10, 10, dtype=np.float32, num=201)
    y = SoftQuantization(-100, 100)(x)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        v = sess.run(y)

    fig, ax = plt.subplots()
    ax.plot(x, v, label='soft quantization')
    ax.plot(x, x, label='raw')
    ax.grid()
    ax.legend(loc='best')
    fig.savefig('soft_quantization.png', bbox_inches='tight')


def main():
    cmd()


if __name__ == '__main__':
    main()
