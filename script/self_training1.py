#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from datetime import datetime
from itertools import cycle
from multiprocessing import Pool
from pathlib import Path

try:
    import sys
    sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane

import numpy as np
from scipy.special import softmax
import cshogi
import shogi
import click
import pandas as pd
import tensorflow as tf
from filehash import FileHash
# noinspection PyProtectedMember
from mlflow import log_params, set_experiment
from tqdm import tqdm, trange

from make_record1 import fv40
from learn_nnue import Network, make_cost, make_metrics, get_optimizer

__author__ = 'Yasuhiro'
__date__ = '2019/09/07'


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--log-dir', type=click.Path())
@click.option('--engine-path', type=click.Path(exists=True))
@click.option('--eval-dir', type=click.Path(exists=True))
@click.option('--hash-size', type=int, default=4096)
@click.option('--threads', type=int, default=4)
@click.option('--depth-limit', type=int, default=2)
@click.option('--multi-pv', type=int, default=16)
@click.option('--resign-value', type=int, default=99999)
@click.option('--n-episodes', type=int, default=1)
@click.option('--softmax-scale', type=float, default=600.0,
              help='行動をsoftmax policyで選ぶ際の逆温度')
def collect(log_dir, engine_path, eval_dir, hash_size, threads, depth_limit,
            multi_pv, resign_value, n_episodes, softmax_scale):
    log_dir = Path(log_dir)
    if not log_dir.exists():
        log_dir.mkdir(parents=True)

    usi_list = []
    # 第1エンジンを深さ１、第２エンジンを指定した深さで作成
    for i in range(2):
        # エンジンとやりとりするクラス
        usi = ayane.UsiEngine()
        usi.set_engine_options({
            'Hash': str(hash_size),
            'Threads': str(threads),
            'NetworkDelay': '0',
            'NetworkDelay2': '0',
            'DepthLimit': 1 if i == 0 else str(depth_limit),
            'ResignValue': str(resign_value),
            # 'BookMoves': '0',
            'EvalDir': eval_dir,
            'MultiPV': multi_pv if i == 0 else 1
        })

        # エンジンに接続
        # 通常の思考エンジンであるものとする。
        usi.connect(engine_path)
        usi_list.append(usi)

    md5hasher = FileHash('md5')
    play_setting = {
        'initial state': 'startpos moves',
        'softmax_scale': softmax_scale,
        'engine1': {'path': engine_path,
                    'engine_hash': md5hasher.hash_file(engine_path),
                    'evaldir': eval_dir,
                    'bin_hash': md5hasher.hash_file(eval_dir + '/nn.bin'),
                    'hash_size': hash_size,
                    'threads': threads,
                    'depth_limit': 1,
                    'resign_value': resign_value,
                    'multi_pv': multi_pv},
        'engine2': {'path': engine_path,
                    'engine_hash': md5hasher.hash_file(engine_path),
                    'evaldir': eval_dir,
                    'bin_hash': md5hasher.hash_file(eval_dir + '/nn.bin'),
                    'hash_size': hash_size,
                    'threads': threads,
                    'depth_limit': depth_limit,
                    'resign_value': resign_value,
                    'multi_pv': 1}
    }

    for episode in trange(n_episodes):
        # エピソードごとに先後を入れ替える
        usis = (usi_list[episode & 1], usi_list[(episode + 1) & 1])

        # noinspection PyUnresolvedReferences
        board = cshogi.Board()
        # 棋譜
        sfen = "startpos moves"
        # 手数
        game_ply = 1
        # 手番(先手=0 , 後手=1)
        turn = 0

        t = datetime.now()
        name = 'windfall-windfall-{}.json'.format(
            t.strftime('%Y%m%d-%H%M%S.%f')
        )
        log_path = log_dir / name

        play_data = {'start_time': str(t), 'setting': play_setting,
                     'black': 'engine1' if episode % 2 == 0 else 'engine2',
                     'white': 'engine2' if episode % 2 == 0 else 'engine1'}
        records = []
        while True:
            usi = usis[turn]

            usi.usi_position(sfen)
            usi.usi_go_and_wait_bestmove("time 0 byoyomi 1000")

            if episode % 2 == turn % 2:
                if usi.think_result.pvs[0].eval is None:
                    # 定跡による指し手
                    bestmove = usi.think_result.bestmove
                    score = usi.think_result.pvs[0].eval
                    records.append((bestmove, score))
                else:
                    # Q-learningのエージェント
                    move_list, eval_list = [], []
                    for pv in usi.think_result.pvs:
                        move = pv.pv.split(' ')[0]
                        move_list.append(move)

                        eval_list.append(pv.eval)

                    tmp = np.asarray(eval_list) / softmax_scale
                    p = softmax(tmp)
                    best_index = np.random.choice(len(move_list), p=p)

                    bestmove = move_list[best_index]
                    probability = p[best_index]

                    score = eval_list[best_index]
                    records.append((bestmove, score, probability))
            else:
                bestmove = usi.think_result.bestmove
                score = usi.think_result.pvs[0].eval

                records.append((bestmove, score))

            # 投了 or 宣言勝ち
            if bestmove == "resign":
                if turn == 0:
                    result = -1  # 後手の勝ち
                else:
                    result = 1  # 先手の勝ち
                break
            elif bestmove == "win":
                if turn == 0:
                    result = 1  # 先手の勝ち
                else:
                    result = -1     # 後手の勝ち
                break
            board.push_usi(bestmove)
            # 千日手
            # noinspection PyUnresolvedReferences
            if board.is_draw() == cshogi.REPETITION_DRAW:
                result = 0
                break

            # 棋譜にこのbestmoveを連結
            sfen += " " + bestmove

            # 手番反転
            turn ^= 1
            game_ply += 1

        play_data['end_time'] = str(datetime.now())
        play_data['result'] = result
        play_data['records'] = records
        with log_path.open('w') as f:
            json.dump(play_data, f)

    for usi in usi_list:
        # エンジンを切断
        usi.disconnect()


@cmd.command()
@click.option('--log-dir', type=click.Path(exists=True))
@click.option('--threads', type=int, default=20)
@click.option('--record-dir', type=click.Path())
def convert(log_dir, threads, record_dir):
    files = list(Path(log_dir).glob('*.json'))

    record_dir = Path(record_dir)

    options = tf.python_io.TFRecordOptions(
        tf.python_io.TFRecordCompressionType.GZIP
    )
    with Pool(threads) as pool:
        for i in range(10):
            name = record_dir / '{}.tfrecord'.format(i)
            if not name.parent.exists():
                name.parent.mkdir(parents=True)
            name = str(name)

            with tf.python_io.TFRecordWriter(name, options=options) as writer:
                m = pool.imap_unordered(make_example, files[i::10])
                for records in tqdm(m, total=len(files) // 10):
                    for record in records:
                        writer.write(record)


def make_example(path):
    data = convert_data(path=path)
    examples = [
       tf.train.Example(features=tf.train.Features(feature={
           'p': tf.train.Feature(int64_list=tf.train.Int64List(value=s[0])),
           'q': tf.train.Feature(int64_list=tf.train.Int64List(value=s[1])),
           'score': tf.train.Feature(int64_list=tf.train.Int64List(value=[v])),
           'result': tf.train.Feature(int64_list=tf.train.Int64List(value=[r]))
       })).SerializeToString() for s, v, r in data
    ]
    return examples


@cmd.command()
@click.option('--record-dir', type=click.Path(exists=True))
@click.option('--threads', type=int, default=20)
@click.option('--batch-size', type=int, default=1000)
@click.option('--alpha', type=float, default=0.1)
@click.option('--elmo-lambda', type=float, default=1.0)
@click.option('--ponanza-constant', type=float, default=600)
@click.option('--score-limit', type=float, default=5.0)
@click.option('--optimizer', type=str)
@click.option('--lr', type=float)
@click.option('--final-lr', type=float, default=0.1)
@click.option('--beta1', type=float, default=0.9)
@click.option('--beta2', type=float, default=0.999)
@click.option('--gamma', type=float, default=1e-3)
@click.option('--epsilon', type=float, default=1e-8, help='rmsprop:1e-10')
@click.option('--rho', type=float, default=0.95)
@click.option('--decay', type=float, default=0.9)
@click.option('--momentum', type=float, default=0.0)
@click.option('--centered', is_flag=True, default=False)
@click.option('--use_nesterov', is_flag=True, default=False)
@click.option('--model-dir', type=click.Path())
@click.option('--previous-model-dir', type=click.Path(exists=True))
@click.option('--epochs', type=int, default=10)
@click.option('--n-iterations-per-epoch', type=int, default=1000)
def learn(record_dir, threads, batch_size, alpha, elmo_lambda,
          ponanza_constant, score_limit, optimizer, lr, final_lr, beta1, beta2,
          gamma, epsilon, rho, decay, momentum, centered, use_nesterov,
          model_dir, previous_model_dir, epochs, n_iterations_per_epoch):
    iterator, next_element = make_dataset(
        record_dir=record_dir, batch_size=batch_size, threads=threads
    )
    model = Network(is_offset=False)
    prediction = model(next_element, alpha, True)
    kl = make_cost(
        prediction=prediction, next_element=next_element,
        elmo_lambda=elmo_lambda, ponanza_constant=ponanza_constant,
        score_limit=score_limit
    )

    optimizer_name = optimizer
    global_step = tf.train.create_global_step()
    optimizer = get_optimizer(
        name=optimizer, lr=lr, final_lr=final_lr, beta1=beta1, beta2=beta2,
        gamma=gamma, epsilon=epsilon, rho=rho, decay=decay, momentum=momentum,
        centered=centered, use_nesterov=use_nesterov
    )
    grads_and_vars = optimizer.compute_gradients(tf.reduce_mean(kl))
    opt_op = optimizer.apply_gradients(grads_and_vars=grads_and_vars,
                                       global_step=global_step)

    graph = tf.get_default_graph()
    update_op, summary_op, histogram_op, reset_op = make_metrics(
        graph=graph, model=model, kl=kl, grads_and_vars=grads_and_vars,
        offset=False
    )

    model_dir = Path(model_dir)
    if not model_dir.exists():
        model_dir.mkdir(parents=True)

    saver = tf.train.Saver()
    writer = tf.summary.FileWriter(model_dir)

    parameters = dict(
        record_dir=record_dir, batch_size=batch_size,
        model_dir=Path(model_dir), elmo_lambda=elmo_lambda,
        ponanza_constant=ponanza_constant, optimizer=optimizer_name,
        lr=lr, final_lr=final_lr, beta1=beta1, beta2=beta2, gamma=gamma,
        epsilon=epsilon, rho=rho, decay=decay, momentum=momentum,
        centered=centered, use_nesterov=use_nesterov, alpha=alpha,
        previous_model_dir=previous_model_dir
    )
    set_experiment('self_training')
    log_params(params=parameters)

    config = tf.ConfigProto(allow_soft_placement=True,
                            gpu_options=tf.GPUOptions(allow_growth=True))
    with tf.Session(config=config) as sess:
        if previous_model_dir is not None:
            checkpoint = tf.train.get_checkpoint_state(previous_model_dir)
            sess.run(tf.local_variables_initializer())

            path = checkpoint.model_checkpoint_path
            saver.restore(sess, path)

            sess.run(tf.variables_initializer([global_step]))
        else:
            checkpoint = tf.train.get_checkpoint_state(model_dir)
            if checkpoint:
                sess.run(tf.local_variables_initializer())

                path = checkpoint.model_checkpoint_path
                saver.restore(sess, path)
            else:
                sess.run(tf.group(tf.global_variables_initializer(),
                                  tf.local_variables_initializer()))

        sess.run(iterator.initializer)
        progress = tqdm(total=n_iterations_per_epoch * epochs, mininterval=10)
        for _ in range(epochs):
            try:
                for _ in range(n_iterations_per_epoch):
                    sess.run([opt_op, update_op])
                    progress.update()
            except tf.errors.OutOfRangeError:
                break
            finally:
                summary, step = sess.run([summary_op, global_step])
                writer.add_summary(summary=summary, global_step=step)

                if step % (n_iterations_per_epoch * 25) == 0:
                    summary = sess.run(histogram_op)
                    writer.add_summary(summary=summary, global_step=step)

                sess.run(reset_op)

                saver.save(sess=sess, save_path=str(model_dir / 'model'),
                           global_step=global_step)


def make_dataset(record_dir, batch_size, threads):
    files = tf.gfile.Glob(str(Path(record_dir) / '*.tfrecord'))

    dataset = tf.data.TFRecordDataset(filenames=files, compression_type='GZIP',
                                      num_parallel_reads=threads)
    dataset = dataset.repeat(-1)
    dataset = dataset.shuffle(100000)
    dataset = dataset.map(parse, num_parallel_calls=threads)
    dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(4)

    iterator = dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    return iterator, next_element


def parse(serialized):
    features = {
        'p': tf.FixedLenFeature([38], tf.int64),
        'q': tf.FixedLenFeature([38], tf.int64),
        'score': tf.FixedLenFeature([], tf.int64),
        'result': tf.FixedLenFeature([], tf.int64)
    }
    example = tf.parse_single_example(serialized, features)

    indices = tf.reshape(tf.range(38, dtype=tf.int64), [-1, 1])
    p = tf.SparseTensorValue(
        indices=indices, values=example['p'], dense_shape=[125388]
    )
    q = tf.SparseTensorValue(
        indices=indices, values=example['q'], dense_shape=[125388]
    )

    data = {
        'p': p, 'q': q, 'score': tf.cast(example['score'], tf.float32),
        'result': tf.cast(example['result'], tf.float32)
    }
    return data


def convert_data(path):
    records, result = get_record(path)
    data = convert_record(records=records, result=result)
    return data


def get_record(path):
    with path.open('r') as f:
        tmp = json.load(f)
    return tmp['records'], tmp['result']


def convert_record(records, result):
    df = pd.DataFrame(records, columns=['move', 'score'])

    # noinspection PyUnresolvedReferences
    board = shogi.Board()
    states = [fv40(board)]  # 初期値をセット
    for move in df['move']:
        if move == 'resign' or move == 'win':
            break
        board.push_usi(move)
        states.append(fv40(board))
    states = [convert_state(*s) for s in states]

    # 一手進んだ局面とスコアの組を学習
    samples = [
        (s, -v, r) for s, v, r in
        zip(states[1:], df['score'].values, cycle([result, -result]))
        if abs(v) < 10000
    ]
    return samples


def convert_state(p, q, fv38p, fv38q):
    size = 1548
    p_offset, q_offset = p * size, q * size
    p_index = [p_offset + i for i in fv38p]
    q_index = [q_offset + i for i in fv38q]

    return p_index, q_index


def main():
    cmd()


if __name__ == '__main__':
    main()
