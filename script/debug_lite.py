#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
from absl import app, flags

from nnue.nnue_lite import NNUELite, EmbeddingLite
from learn_nnue_lite import make_test_dataset

__author__ = 'Yasuhiro'
__date__ = '2020/11/14'

FLAGS = flags.FLAGS
flags.DEFINE_string('model_dir', default='', help='')


def extract_parameters(model):
    embedding_kernel = tf.cast(tf.math.round(model.feature.kernel * 127.0),
                               dtype=tf.int16).numpy()
    # embedding_kernel = (model.feature.net.embeddings * 127.0).numpy()
    embedding_bias = tf.cast(tf.math.round(model.feature.bias * 127.0),
                             dtype=tf.int16).numpy()
    # embedding_bias = (model.feature.bias * 127.0).numpy()
    embedding_kernel = embedding_kernel.astype(np.int16)
    embedding_bias = embedding_bias.astype(np.int16)

    hidden1_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[1].kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    # hidden1_kernel = (
    #     tf.transpose(model.net.layers[1].kernel, perm=[1, 0]) * 64
    # ).numpy()
    hidden1_bias = tf.cast(tf.round(model.net.layers[1].bias * 127 * 64),
                           dtype=tf.int32).numpy()
    # hidden1_bias = (model.net.layers[1].bias * 127 * 64).numpy()
    hidden1_kernel = hidden1_kernel.astype(np.int8)
    hidden1_bias = hidden1_bias.astype(np.int32)

    # hidden2_kernelの量子化誤差の影響が大きい
    hidden2_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[3].kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    # hidden2_kernel = (
    #     tf.transpose(model.net.layers[3].kernel, perm=[1, 0]) * 64
    # ).numpy()
    hidden2_bias = tf.cast(tf.round(model.net.layers[3].bias * 127 * 64),
                           dtype=tf.int32).numpy()
    # hidden2_bias = (model.net.layers[3].bias * 127 * 64).numpy()
    hidden2_kernel = hidden2_kernel.astype(np.int8)
    hidden2_bias = hidden2_bias.astype(np.int32)

    hidden3_kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(model.net.layers[5].kernel, perm=[1, 0]) *
                600 * 16 / 127,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    # hidden3_kernel = (
    #     tf.transpose(model.net.layers[5].kernel, perm=[1, 0]) *
    #     600 * 16 / 127.0
    # ).numpy()
    hidden3_bias = tf.cast(tf.round(model.net.layers[5].bias * 600 * 16),
                           dtype=tf.int32).numpy()
    # hidden3_bias = (model.net.layers[5].bias * 600 * 16).numpy()
    hidden3_kernel = hidden3_kernel.astype(np.int8)
    hidden3_bias = hidden3_bias.astype(np.int32)

    d = {'embedding_kernel': embedding_kernel,
         'embedding_bias': embedding_bias,
         'hidden1_kernel': hidden1_kernel, 'hidden1_bias': hidden1_bias,
         'hidden2_kernel': hidden2_kernel, 'hidden2_bias': hidden2_bias,
         'hidden3_kernel': hidden3_kernel, 'hidden3_bias': hidden3_bias}
    return d


def main(_):
    model = NNUELite()
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=model
    )
    manager = tf.train.CheckpointManager(ckpt, FLAGS.model_dir,
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    test_dataset, test_size = make_test_dataset()
    test_score = tf.squeeze(model(test_dataset)) * 600

    # 期待した範囲の値が表示される
    print(test_score.numpy())

    d = extract_parameters(model=model)

    embedding_layer = EmbeddingLite()
    _ = embedding_layer(test_dataset[0])
    embedding_layer.net.embeddings.assign(
        d['embedding_kernel'].astype(np.float32)
    )
    embedding_layer.bias.assign(d['embedding_bias'].astype(np.float32))
    net = tf.keras.Sequential([
        tf.keras.layers.ReLU(max_value=127),
        tf.keras.layers.Dense(
            units=32,
            kernel_initializer=tf.keras.initializers.constant(
                d['hidden1_kernel'].T
            ),
            bias_initializer=tf.keras.initializers.constant(
                d['hidden1_bias']
            )
        ),
        # ここの切り捨ての影響が大きい
        # tf.keras.layers.Lambda(lambda x: tf.math.divide(x, 64.0)),
        tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 64.0)),
        tf.keras.layers.ReLU(max_value=127),
        tf.keras.layers.Dense(
            units=32,
            kernel_initializer=tf.keras.initializers.constant(
                d['hidden2_kernel'].T
            ),
            bias_initializer=tf.keras.initializers.constant(
                d['hidden2_bias']
            )
        ),
        # tf.keras.layers.Lambda(lambda x: tf.math.divide(x, 64.0)),
        tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 64.0)),
        tf.keras.layers.ReLU(max_value=127),
        tf.keras.layers.Dense(
            units=1,
            kernel_initializer=tf.keras.initializers.constant(
                d['hidden3_kernel'].T
            ),
            bias_initializer=tf.keras.initializers.constant(
                d['hidden3_bias']
            )
        ),
        # tf.keras.layers.Lambda(lambda x: tf.math.divide(x, 16.0))
        tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 16.0))
    ])

    tmp_p = embedding_layer(test_dataset[0])
    tmp_q = embedding_layer(test_dataset[1])
    tmp = tf.concat((tmp_p, tmp_q), axis=-1)
    tmp = net(tmp)
    tmp = tf.squeeze(tmp)

    print(tmp.numpy())


if __name__ == '__main__':
    app.run(main)
