#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path

import click
import pandas as pd
import tensorflow as tf
import toml
from tqdm import trange

from nnue import KP, BoostingKP

__author__ = 'Yasuhiro'
__date__ = '2020/02/26'


def make_dataset(record_path, batch_size):
    files = tf.io.gfile.glob(record_path)
    dataset = tf.data.TFRecordDataset(
        filenames=files, compression_type='GZIP', num_parallel_reads=10
    )
    dataset = dataset.shuffle(batch_size * 10)
    dataset = dataset.map(parse, num_parallel_calls=20).batch(batch_size)
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def parse(serialized):
    features = {
        'p': tf.io.FixedLenFeature([38], tf.int64),
        'q': tf.io.FixedLenFeature([38], tf.int64),
        'score': tf.io.FixedLenFeature([], tf.int64),
        'ply': tf.io.FixedLenFeature([], tf.int64),
        'result': tf.io.FixedLenFeature([], tf.int64)
    }
    example = tf.io.parse_single_example(serialized, features)

    score = tf.cast(example['score'], tf.float32) / 1e2
    score = tf.reshape(score, [1])

    size = 1548
    p_ou = tf.math.floordiv(example['p'][0], size)
    q_ou = tf.math.floordiv(example['q'][0], size)
    index = tf.math.mod(example['p'], size)
    # 自分の王の位置に固有なバイアスと相手側の王を追加
    # 自分の王がいる位置に相手の王は絶対に来ないのでバイアスとして使える
    full_index = tf.concat([index, [p_ou + size, q_ou + size]], axis=0)
    p = p_ou * (size + 81) + full_index

    data = {'p': tf.cast(p, dtype=tf.int32), 'target': score}
    return data


# https://www.kaggle.com/philculliton/bert-optimization
class WarmUp(tf.keras.optimizers.schedules.LearningRateSchedule):
    """Applies a warmup schedule on a given learning rate decay schedule."""

    def __init__(
            self,
            initial_learning_rate,
            decay_schedule_fn,
            warmup_steps,
            power=1.0,
            name=None):
        super(WarmUp, self).__init__()
        self.initial_learning_rate = initial_learning_rate
        self.warmup_steps = warmup_steps
        self.power = power
        self.decay_schedule_fn = decay_schedule_fn
        self.name = name

    def __call__(self, step):
        with tf.name_scope(self.name or 'WarmUp') as name:
            # Implements polynomial warmup.
            # i.e., if global_step < warmup_steps, the
            # learning rate will be `global_step/num_warmup_steps * init_lr`.
            global_step_float = tf.cast(step + 1, tf.float32)
            warmup_steps_float = tf.cast(self.warmup_steps, tf.float32)
            warmup_percent_done = global_step_float / warmup_steps_float
            warmup_learning_rate = (
                    self.initial_learning_rate *
                    tf.math.pow(warmup_percent_done, self.power))
            return tf.cond(global_step_float < warmup_steps_float,
                           lambda: warmup_learning_rate,
                           lambda: self.decay_schedule_fn(step),
                           name=name)

    def get_config(self):
        return {
            'initial_learning_rate': self.initial_learning_rate,
            'decay_schedule_fn': self.decay_schedule_fn,
            'warmup_steps': self.warmup_steps,
            'power': self.power,
            'name': self.name
        }


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def fit(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    save_frequency = 100

    n = 8
    network = KP(output_dim=n, use_bias=False)
    # learning_rate = WarmUp(
    #     initial_learning_rate=config['learning_rate'],
    #     decay_schedule_fn=lambda global_step: config['learning_rate'],
    #     warmup_steps=50 * save_frequency
    # )
    learning_rate = config['learning_rate']
    optimizer = tf.keras.optimizers.Ftrl(learning_rate=learning_rate)
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=network, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'])

    criterion = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    metrics = tf.keras.metrics.MeanAbsoluteError()
    metrics_kl = tf.keras.metrics.BinaryCrossentropy(from_logits=True)
    regularizer = tf.keras.metrics.Mean()

    lambda_ = config['lambda']

    @tf.function
    def step(x, y):
        with tf.device('/CPU:0'):
            scaled = y / 6
            t = tf.clip_by_value(tf.nn.sigmoid(scaled),
                                 clip_value_max=0.99, clip_value_min=0.01)
            with tf.GradientTape() as tape:
                p = network(x)
                r = tf.math.reduce_mean(
                    tf.abs(tf.abs(network.losses) - scaled)
                )
                loss = criterion(y_true=t, y_pred=p) + lambda_ * r

            variables = network.trainable_variables
            gradients = tape.gradient(loss, variables)
            optimizer.apply_gradients(zip(gradients, variables))

            metrics.update_state(y_true=y, y_pred=p * 6)
            metrics_kl.update_state(y_true=t, y_pred=p)
            regularizer.update_state(r)
        return p, t

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    for i, data in zip(trange(config['iterations']), dataset):
        predictions, targets = step(data['p'], data['target'])

        if (i + 1) % save_frequency == 0:
            predictions = tf.nn.sigmoid(predictions)
            differences = predictions - targets
            with summary_writer.as_default():
                tf.summary.scalar('loss', metrics.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('kl', metrics_kl.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('regularization', regularizer.result(),
                                  step=int(ckpt.step))

                tf.summary.histogram('difference', differences,
                                     step=int(ckpt.step))
            metrics.reset_states()
            metrics_kl.reset_states()
            regularizer.reset_states()

            ckpt.step.assign_add(1)
            manager.save()
    summary_writer.flush()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def boosting(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    save_frequency = 100

    n = 8
    network = BoostingKP(output_dim=n, use_bias=False)
    # learning_rate = WarmUp(
    #     initial_learning_rate=config['learning_rate'],
    #     decay_schedule_fn=lambda global_step: config['learning_rate'],
    #     warmup_steps=50 * save_frequency
    # )
    learning_rate = config['learning_rate']
    optimizer = tf.keras.optimizers.Ftrl(learning_rate=learning_rate)
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=network, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'])

    criterion = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    metrics = tf.keras.metrics.MeanAbsoluteError()
    metrics_kl = tf.keras.metrics.BinaryCrossentropy(from_logits=True)
    regularizer = tf.keras.metrics.Mean()

    lambda_ = config['lambda']

    @tf.function
    def step(x, y, phase):
        with tf.device('/CPU:0'):
            t = tf.clip_by_value(
                tf.nn.sigmoid(y / 6), clip_value_max=0.99, clip_value_min=0.01
            )
            with tf.GradientTape() as tape:
                p = network([x, phase]) / 6
                r = tf.math.reduce_mean(tf.abs(network.losses - y))
                loss = criterion(y_true=y, y_pred=p) + lambda_ * r

            variables = network.trainable_variables
            gradients = tape.gradient(loss, variables)
            optimizer.apply_gradients(zip(gradients, variables))

            metrics.update_state(y_true=y / 6, y_pred=p)
            metrics_kl.update_state(y_true=t, y_pred=p)
            regularizer.update_state(r)
        return p, t

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    for i, data in zip(trange(config['iterations']), dataset):
        predictions, targets = step(data['p'], data['target'],
                                    tf.math.floordiv(ckpt.step - 1, 10))

        if (i + 1) % save_frequency == 0:
            predictions = tf.nn.sigmoid(predictions)
            differences = predictions - targets
            with summary_writer.as_default():
                tf.summary.scalar('loss', metrics.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('kl', metrics_kl.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('regularization', regularizer.result(),
                                  step=int(ckpt.step))

                tf.summary.histogram('difference', differences,
                                     step=int(ckpt.step))
            metrics.reset_states()
            metrics_kl.reset_states()
            regularizer.reset_states()

            ckpt.step.assign_add(1)
            manager.save()
    summary_writer.flush()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def view_train_score(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    network = KP(return_preactivation=True, output_dim=8, use_bias=False)
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'])
    for data in dataset:
        scores, preactivations = network(data['p'])

        df = pd.DataFrame({'prediction': tf.squeeze(scores).numpy(),
                           'target': tf.squeeze(data['target'] / 6).numpy()})
        for name, v in zip(['positive', 'negative'], preactivations):
            for i in range(int(v.shape[1])):
                df['{}{}'.format(name, i)] = v[:, i].numpy()
        df.to_csv(config_path.with_name(
            'train_data{:04}.csv'.format(int(ckpt.step) - 1)
        ))
        break


def main():
    cmd()


if __name__ == '__main__':
    main()
