#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from pathlib import Path

import click
import cshogi
import gym
import numpy as np
import pandas as pd
import tensorflow as tf
import toml
from scipy.special import logit
from tqdm import trange, tqdm

try:
    import sys

    sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane

from wf_gym import make_usi
from nnue import NNUESigmoid
from make_record2 import fv40
from training_in_gym1 import get_states
from replay_memory import Memory, Tree

__author__ = 'Yasuhiro'
__date__ = '2019/12/15'


class Sampler(object):
    def __init__(self, behavior_model, config):
        self.behavior_model = behavior_model

        usi = make_usi(config=config['engine'])
        env = gym.make(config['gym']['name'], usi=usi,
                       random_rate=config['gym']['random_rate'])
        self.env = env
        self.usi = usi

        ponanza_constant = config['scale']['ponanza_constant']
        pc = tf.constant(ponanza_constant, dtype=tf.float32)
        scale = tf.constant(config['scale']['softmax'], dtype=tf.float32)

        @tf.function(input_signature=[
            tf.TensorSpec(shape=[2, None, 38], dtype=tf.int32)])
        def _select_index(x):
            # 局面は相手の手番
            # 相手の評価値が低いほど選ばれやすい
            scores, _ = behavior_model(x)
            y = -scores / pc
            y = tf.reshape(y, shape=[1, -1]) * scale
            i = tf.random.categorical(logits=y, num_samples=1)
            return i

        self._select_index = _select_index

        self.buffer = np.empty(513, dtype=Tree.dtype)
        # 値がそのまま渡されるので、初期化しておく
        self.buffer['score_flag'] = False
        self.size = 0

        # noinspection PyTypeChecker
        self.result_value = (
            0, logit(0.99) * ponanza_constant, logit(0.01) * ponanza_constant
        )

    def __del__(self):
        self.usi.disconnect()

    def sample(self):
        self.env.reset()
        self.size = 0
        # 初期化
        self.buffer['info'] = cshogi.NOT_REPETITION

        while True:
            inputs, actions = get_states(board=self.env.board)
            if actions is None:
                # 一手詰めのチェックをしているが、王手を解消しながら逆に詰ませる場合があるみたい
                p, q, p_index, q_index = fv40(board=self.env.board)
                p_index = np.asarray(p_index) + p
                q_index = np.asarray(q_index) + q
                self.add(
                    p_index=p_index, q_index=q_index, done=True,
                    next_state=np.empty(32, dtype=np.uint8)
                )

                tmp = self.buffer[:self.size]
                tmp['progress'] = np.arange(1, self.size + 1) / self.size
                tmp[-1]['info'] = cshogi.NOT_REPETITION
                # 現局面はagentの詰み
                tmp['result'][-1] = -1
                # 他の入力局面は一手進んだ状態なので、環境の勝ち
                tmp['result'][:-1] = 1
                break

            index = int(self._select_index(tf.convert_to_tensor(inputs)))
            selected_action = actions[index]

            observation, reward, done, info = self.env.step(selected_action)

            self.add(
                p_index=inputs[0][index], q_index=inputs[1][index], done=done,
                next_state=observation
            )
            if done:
                tmp = self.buffer[:self.size]
                tmp['progress'] = np.arange(1, self.size + 1) / self.size
                tmp[-1]['info'] = info['draw']
                # 入力局面は環境の手番なので、値を反転
                tmp['result'] = -reward
                break
        return self.buffer, self.size

    def add(self, p_index, q_index, done, next_state):
        data = self.buffer[self.size]
        data['p'] = p_index
        data['q'] = q_index
        data['done'] = done
        data['state'] = next_state

        self.size += 1


def sample_episodes(replay_memory, sampler_list, desc):
    progress = tqdm(total=replay_memory.capacity, initial=replay_memory.size,
                    desc=desc)
    while True:
        for sampler in sampler_list:
            buffer, size = sampler.sample()
            replay_memory.add_batch(buffer=buffer, size=size)

            progress.update(size)

            if replay_memory.size + 512 >= replay_memory.capacity:
                # 容量がいっぱいになったので、サンプル収集を終わる
                progress.close()

                return replay_memory


class TargetWorker(object):
    def __init__(self, target_model):
        self.target_model = target_model

    def __call__(self, p, q):
        v, _ = self.target_model([p, q])
        return v


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def learn(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    behavior_network = NNUESigmoid()
    target_network = NNUESigmoid()

    optimizer = tf.keras.optimizers.SGD(
        learning_rate=config['optimizer']['sgd_lr'], momentum=0.9
    )

    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), behavior_network=behavior_network,
        target_network=target_network,
        optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)
    if not manager.latest_checkpoint:
        half_kp_size = 125388
        tmp = np.random.randint(half_kp_size, size=[2, 38])
        behavior_network([tmp, tmp])
        target_network([tmp, tmp])
        # 値を同期
        for b, t in zip(behavior_network.variables, target_network.variables):
            t.assign(b)

    target_worker = TargetWorker(target_model=target_network)
    sampler_list = [Sampler(behavior_model=behavior_network, config=config)]

    test_replay_memory = Memory(
        capacity=config['replay_memory']['test_size'],
        target_worker=target_worker,
        ponanza_constant=config['scale']['ponanza_constant'],
        elmo_lambda=config['scale']['elmo_lambda']
    )
    test_data_path = model_dir / 'test_dataset.pickle'
    if test_data_path.exists():
        print('load test data from {}'.format(test_data_path))
        test_replay_memory.load(test_data_path)
    else:
        test_replay_memory = sample_episodes(
            replay_memory=test_replay_memory, sampler_list=sampler_list,
            desc='sampling test data'
        )
        test_replay_memory.dump(test_data_path)

    train_replay_memory = Memory(
        capacity=config['replay_memory']['train_size'],
        target_worker=target_worker,
        ponanza_constant=config['scale']['ponanza_constant'],
        elmo_lambda=config['scale']['elmo_lambda']
    )
    train_data_path = model_dir / 'train_dataset.pickle'
    if train_data_path.exists():
        print('load train data from {}'.format(test_data_path))
        train_replay_memory.load(train_data_path)
    else:
        train_replay_memory = sample_episodes(
            replay_memory=train_replay_memory, sampler_list=sampler_list,
            desc='sampling train data'
        )
        train_replay_memory.dump(train_data_path)
        train_replay_memory.compute_cost(behavior_model=behavior_network)

    train_nnue_loss = tf.keras.metrics.Mean('train_nnue_loss')
    train_ce = tf.keras.metrics.BinaryCrossentropy('train_cross_entropy',
                                                   from_logits=True)
    train_entropy = tf.keras.metrics.BinaryCrossentropy('train_entropy',
                                                        from_logits=False)
    test_nnue_loss = tf.keras.metrics.Mean('test_nnue_loss')
    test_ce = tf.keras.metrics.BinaryCrossentropy('test_cross_entropy',
                                                  from_logits=True)
    test_entropy = tf.keras.metrics.BinaryCrossentropy('test_entropy',
                                                       from_logits=False)
    for m in (train_nnue_loss, train_ce, train_entropy,
              test_nnue_loss, test_ce, test_entropy):
        m.reset_states()

    train_log_dir = str(config_path.parent / 'logs/gradient_tape/train')
    test_log_dir = str(config_path.parent / 'logs/gradient_tape/test')
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)

    # pc = tf.constant(float(config['scale']['ponanza_constant']))

    @tf.function
    def train_step(x, y):
        with tf.GradientTape() as tape:
            behavior, histogram = behavior_network(x)
            logits_q = behavior

            p = tf.reshape(y, [-1, 1])
            y_pred = tf.clip_by_value(p, clip_value_min=0.01,
                                      clip_value_max=0.99)

            cross_entropy = tf.squeeze(tf.nn.sigmoid_cross_entropy_with_logits(
                labels=p, logits=logits_q
            ))
            entropy = tf.keras.losses.binary_crossentropy(
                y_true=p, y_pred=y_pred, from_logits=False
            )
            loss = cross_entropy - entropy
            mean_loss = tf.reduce_mean(loss)
        gradients = tape.gradient(mean_loss,
                                  behavior_network.trainable_variables)
        optimizer.apply_gradients(zip(
            gradients, behavior_network.trainable_variables
        ))

        train_nnue_loss.update_state(loss)
        train_ce.update_state(y_true=p, y_pred=logits_q)
        train_entropy.update_state(y_true=p, y_pred=y_pred)
        return loss, histogram, gradients

    @tf.function
    def test_step(x, y):
        behavior, histogram = behavior_network(x)
        logits_q = behavior

        p = tf.reshape(y, [-1, 1])
        y_pred = tf.clip_by_value(p, clip_value_min=0.01,
                                  clip_value_max=0.99)

        cross_entropy = tf.squeeze(tf.nn.sigmoid_cross_entropy_with_logits(
            labels=p, logits=logits_q
        ))
        entropy = tf.keras.losses.binary_crossentropy(
            y_true=p, y_pred=y_pred, from_logits=False
        )
        loss = cross_entropy - entropy

        test_nnue_loss.update_state(loss)
        test_ce.update_state(y_true=p, y_pred=logits_q)
        test_entropy.update_state(y_true=p, y_pred=y_pred)
        return loss, histogram

    name_pattern = re.compile(r'((?:transformer|affine[1-3])/'
                              r'(?:kernel|kernel_base|bias)):0')

    def _get_name(variable):
        match = name_pattern.search(variable.name)
        return match.group(1)

    batch_size = config['batch_size']

    def _train_generator():
        for _ in range(config['train_iterations']):
            i, (x, y) = train_replay_memory.sample(batch_size)
            yield i, x, y

    def _test_generator():
        for _ in range(config['test_iterations']):
            i, (x, y) = test_replay_memory.sample(batch_size)
            yield i, x, y

    output_types = (tf.int32, (tf.int32, tf.int32), tf.float32)
    output_shapes = (
        tf.TensorShape([batch_size]),
        (tf.TensorShape([batch_size, 38]), tf.TensorShape([batch_size, 38])),
        tf.TensorShape([batch_size])
    )
    train_dataset = tf.data.Dataset.from_generator(
        generator=_train_generator, output_types=output_types,
        output_shapes=output_shapes
    ).prefetch(tf.data.experimental.AUTOTUNE)
    test_dataset = tf.data.Dataset.from_generator(
        generator=_test_generator, output_types=output_types,
        output_shapes=output_shapes
    ).prefetch(tf.data.experimental.AUTOTUNE)

    for epoch in range(config['epochs']):
        histograms = None
        grads = None
        for indices, train_x, train_y in tqdm(
                train_dataset, desc='train {0:02d}'.format(epoch),
                total=config['train_iterations']):
            costs, histograms, grads = train_step(train_x, train_y)
            train_replay_memory.update(indices=indices.numpy(),
                                       costs=costs.numpy())

        with train_summary_writer.as_default():
            tf.summary.scalar('loss/total', train_nnue_loss.result(),
                              step=int(ckpt.step))
            tf.summary.scalar('loss/cross_entropy', train_ce.result(),
                              step=int(ckpt.step))
            tf.summary.scalar('loss/entropy', train_entropy.result(),
                              step=int(ckpt.step))
            if int(ckpt.step) % config['frequency']['histogram'] == 0:
                for key, value in histograms.items():
                    tf.summary.histogram('train/{}'.format(key), value,
                                         step=int(ckpt.step))
                for g, v in zip(grads, behavior_network.trainable_variables):
                    name = _get_name(v)
                    tf.summary.histogram(
                        'gradient/{}'.format(name),
                        tf.reshape(g, [-1]), step=int(ckpt.step)
                    )
        for m in (train_nnue_loss, train_ce, train_entropy):
            m.reset_states()

        if int(ckpt.step) % config['frequency']['test'] == 0:
            for indices, test_x, test_y in tqdm(
                    test_dataset, desc='test {0:02d}'.format(epoch),
                    total=config['test_iterations']):
                costs, histograms = test_step(test_x, test_y)
                test_replay_memory.update(indices=indices.numpy(),
                                          costs=costs.numpy())

            with test_summary_writer.as_default():
                tf.summary.scalar('loss/total', test_nnue_loss.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('loss/cross_entropy', test_ce.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('loss/entropy', test_entropy.result(),
                                  step=int(ckpt.step))
                for key, value in histograms.items():
                    tf.summary.histogram('test/{}'.format(key), value,
                                         step=int(ckpt.step))

                for v in behavior_network.trainable_variables:
                    name = _get_name(v)
                    tf.summary.histogram(
                        'variable/{}'.format(name), tf.reshape(v, [-1]),
                        step=int(ckpt.step)
                    )

            for m in (test_nnue_loss, test_ce, test_entropy):
                m.reset_states()

            if epoch != config['epochs'] - 1:
                # まだ学習が続くなら訓練データを更新
                train_replay_memory.shrink()
                train_replay_memory = sample_episodes(
                    replay_memory=train_replay_memory,
                    sampler_list=sampler_list,
                    desc='update training replay memory'
                )

                train_replay_memory.compute_cost(
                    behavior_model=behavior_network
                )
                train_replay_memory.dump(train_data_path)

        if int(ckpt.step) % config['frequency']['synchronize'] == 0:
            # target networkを更新
            for t, b in zip(target_network.variables,
                            behavior_network.variables):
                t.assign(b)

            # target networkを更新したので、キャッシュをクリア
            train_replay_memory.reset_score_flag()
            test_replay_memory.reset_score_flag()

            train_replay_memory.compute_cost(behavior_model=behavior_network)
            train_replay_memory.dump(train_data_path)
        ckpt.step.assign_add(1)
        manager.save()

    train_replay_memory.dump(train_data_path)
    test_replay_memory.dump(test_data_path)

    del sampler_list


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def view(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    behavior_network = NNUESigmoid()
    target_network = NNUESigmoid()
    ckpt = tf.train.Checkpoint(behavior_network=behavior_network,
                               target_network=target_network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    target_worker = TargetWorker(target_model=target_network)
    test_replay_memory = Memory(
        capacity=config['replay_memory']['test_size'],
        target_worker=target_worker,
        ponanza_constant=config['scale']['ponanza_constant'],
        elmo_lambda=config['scale']['elmo_lambda']
    )
    test_data_path = model_dir / 'test_dataset.pickle'
    test_replay_memory.load(test_data_path)

    indices, (x, y) = test_replay_memory.sample(10000)
    scores, _ = behavior_network(tf.convert_to_tensor(x))

    df = pd.DataFrame({'behavior': np.squeeze(scores.numpy()), 'target': y,
                       'index': indices})
    df.to_csv(config_path.with_name('test_value.csv'))


def main():
    cmd()


if __name__ == '__main__':
    main()
