#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NNUEのパラメータの初期値の設定や変化を確認するためのスクリプト
"""

import struct
from dataclasses import dataclass
from pathlib import Path

import click
import joblib
import numpy as np

__author__ = 'Yasuhiro'
__date__ = '2019/05/13'


version = 0x7AF32F16
hash_value = 1046128366
architecture = ('Features=HalfKP(Friend)[125388->256x2],'
                'Network=AffineTransform[1<-32](ClippedReLU[32]('
                'AffineTransform[32<-32](ClippedReLU[32]('
                'AffineTransform[32<-512](InputSlice[512(0:512)])))))')
transformer_hash = 1567217592
network_hash = 1664315734


@dataclass
class Parameter(object):
    kernel: np.ndarray
    bias: np.ndarray


def unpack_uint32(file):
    return struct.unpack('I', file.read(4))[0]


def pack_uint32(v):
    return struct.pack('I', v)


def read_header(file):
    v = unpack_uint32(file)
    assert v == version, (v, version)

    # ネットワークの構造を変えるとハッシュが変わる
    h = unpack_uint32(file)
    assert h == hash_value

    size = unpack_uint32(file)

    model = file.read(size)
    # print(model.decode())
    assert model.decode() == architecture
    return size


def write_header(file):
    file.write(pack_uint32(version))
    file.write(pack_uint32(hash_value))
    s = architecture.encode(encoding='utf-8')
    file.write(pack_uint32(len(s)))
    file.write(s)


def read_transformer(file):
    h = unpack_uint32(file)
    # print(h)
    assert h == transformer_hash

    # 入力：125388
    # 出力: 256*2

    # int16_t
    bias = np.array(struct.unpack('256h', file.read(256 * 2)),
                    dtype=np.int16)
    size = 256 * 125388
    kernel = np.array(struct.unpack('{}h'.format(size), file.read(size * 2)),
                      dtype=np.int16).reshape([125388, 256])

    p = Parameter(kernel=kernel, bias=bias)
    return p


def write_transformer(file, p):
    file.write(pack_uint32(transformer_hash))

    file.write(p.bias.tobytes())
    file.write(p.kernel.tobytes())


def read_affine_layer(file, input_size, output_size, flag=False):
    if flag:
        h = unpack_uint32(file)
        # print(h)
        assert h == network_hash

    # int32_t
    bias = np.array(struct.unpack('{}i'.format(output_size),
                                  file.read(output_size * 4)),
                      dtype=np.int32)
    n = input_size * output_size
    # int8_t
    kernel = np.array(struct.unpack('{}b'.format(n), file.read(n * 1)),
                      dtype=np.int8)

    p = Parameter(kernel=kernel, bias=bias)
    return p


def write_affine_layer(file, p, flag=False):
    if flag:
        file.write(pack_uint32(network_hash))

    file.write(p.bias.tobytes())
    file.write(p.kernel.tobytes())


def read(input_path):
    input_path = Path(input_path)
    with input_path.open('rb') as f:
        read_header(f)
        parameters = [
            read_transformer(f),
            read_affine_layer(f, input_size=256 * 2, output_size=32,
                              flag=True),
            read_affine_layer(f, input_size=32, output_size=32),
            read_affine_layer(f, input_size=32, output_size=1)
        ]
    return parameters


def write(output_path, parameters):
    output_path = Path(output_path)
    with output_path.open('wb') as f:
        write_header(f)

        write_transformer(f, parameters[0])
        write_affine_layer(f, parameters[1], flag=True)
        write_affine_layer(f, parameters[2])
        write_affine_layer(f, parameters[3])


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--input-file', type=click.Path(exists=True))
@click.option('--output-file', type=click.Path())
def read_parameter(input_file, output_file):
    parameters = read(input_path=input_file)
    output_file = Path(output_file)
    if not output_file.parent.exists():
        output_file.parent.mkdir(parents=True)
    joblib.dump(parameters, str(output_file))


@cmd.command()
@click.option('--output-file', type=click.Path())
def write_parameter(output_file):
    pass


def main():
    cmd()


if __name__ == '__main__':
    main()
