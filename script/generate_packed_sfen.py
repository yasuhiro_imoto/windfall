#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
YaneuraOuの110億の教師データの一部を利用してpacked sfenの読み込みテストのデータを作る
"""

import json
from pathlib import Path

import numpy as np
import cshogi
from absl import flags, app

__author__ = 'Yasuhiro'
__date__ = '2020/10/12'


FLAGS = flags.FLAGS
flags.DEFINE_string('output_path', default=None, help='')
flags.DEFINE_integer('n_samples', default=100, help='')
flags.DEFINE_string('data_path', default='../data/x036', help='')


def generate():
    # noinspection PyStatementEffect
    psfens = np.fromfile(FLAGS.data_path, dtype=cshogi.PackedSfenValue,
                         count=FLAGS.n_samples)

    board = cshogi.Board()

    results = []
    for data in psfens:
        board.set_psfen(data['sfen'])
        results.append({'packed_sfen': data['sfen'].tolist(),
                        'sfen': board.sfen()})

    return results


def main(_):
    sfen_list = generate()

    output_path = Path(FLAGS.output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)
    with output_path.open('w') as f:
        json.dump(sfen_list, f, indent=4)


if __name__ == '__main__':
    app.run(main)
