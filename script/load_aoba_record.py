#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Aoba Zeroの棋譜ファイルを読み込む
"""

import argparse
import re
from enum import Enum
from pathlib import Path

import tensorflow as tf
from absl import app, flags
from tqdm import tqdm

__author__ = 'Yasuhiro'
__date__ = '2021/02/06'


class GameWinner(Enum):
    black = 0
    white = 1
    draw = 2


def load_record(file_path, index):
    """
    Aoba Zeroの棋譜ファイルから1局分の棋譜を取り出す
    :param file_path:
    :param index: 0から9999までの整数
    :return:
    """
    count = -1

    record = []
    result = GameWinner.draw
    result_text = ''
    with open(file_path) as f:
        for line in f.readlines():
            if count < index:
                if line == '+\n':
                    count += 1
                continue
            else:
                if line[0] == '%':
                    result_text = line.rstrip()
                    break
                else:
                    record.append(line)

    if result_text == '%TORYO':
        result = GameWinner(len(record) % 2)
    elif result_text == 'WIN':
        result = GameWinner(1 - len(record) % 2)

    d = {'record': record, 'result': result, 'result_text': result_text}
    return d


def _int64_feature(value):
    """bool / enum / int / uint 型から Int64_list を返す"""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """string / byte 型から byte_list を返す"""
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _make_sequence_example(result, record):
    context = tf.train.Features(feature={
        'winner': _int64_feature(result.value)
    })
    # 指し手と探索回数の部分に分けて変換
    move_feature = []
    policy_feature = []
    for r in record:
        # 文字列はスカラー扱い
        move_feature.append(_bytes_feature(r[:7].encode()))
        policy_feature.append(_bytes_feature(r[9:].encode()))
    feature_lists = tf.train.FeatureLists(
        feature_list={'move': tf.train.FeatureList(feature=move_feature),
                      'policy': tf.train.FeatureList(feature=policy_feature)}
    )

    sequence_example = tf.train.SequenceExample(context=context,
                                                feature_lists=feature_lists)
    return sequence_example


def make_tf_examples(file_path):
    progress = tqdm(total=10000)

    parse_flag = False
    example_list = []
    with file_path.open() as f:
        record = []
        for line in f.readlines():
            if parse_flag:
                line = line.rstrip()
                if line[0] == '%':
                    result_text = line

                    if result_text == '%TORYO':
                        result = GameWinner(len(record) % 2)
                    elif result_text == 'WIN':
                        result = GameWinner(1 - len(record) % 2)
                    else:
                        # 引き分けの影響がわからないので、今回は学習対象に含めないものとする
                        result = GameWinner.draw

                    # 現状では決着がついた場合のみを扱う
                    if result != GameWinner.draw:
                        sequence_example = _make_sequence_example(
                            result=result, record=record
                        )
                        example_list.append(
                            sequence_example.SerializeToString()
                        )

                    parse_flag = False
                    # 棋譜をクリア
                    record = []

                    progress.update()
                else:
                    record.append(line)
            else:
                if line == '+\n':
                    parse_flag = True

    return example_list


def parse(sequence_example):
    context, feature_lists = tf.io.parse_single_sequence_example(
        sequence_example,
        context_features={'winner': tf.io.FixedLenFeature([], dtype=tf.int64)},
        sequence_features={
            'move': tf.io.FixedLenSequenceFeature([], dtype=tf.string),
            'policy': tf.io.FixedLenSequenceFeature([], dtype=tf.string)
        }
    )
    return context['winner'], feature_lists


def main():
    # スコープを限定するためにabslではなくargparseを使う
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', help='変換したい棋譜があるディレクトリ',
                        type=str)
    parser.add_argument('--output_dir', help='変換したい棋譜があるディレクトリ',
                        type=str)
    args = parser.parse_args()

    r = re.compile(r'arch(\d+)0000\.csa')
    options = tf.io.TFRecordOptions(compression_type="GZIP")

    input_dir = Path(args.input_dir)
    output_dir = Path(args.output_dir)
    if not output_dir.exists():
        output_dir.mkdir(parents=True)

    for input_path in input_dir.glob('arch*.csa'):
        examples = make_tf_examples(file_path=input_path)

        m = r.search(str(input_path))
        index = int(m.group(1))
        output_path = output_dir / 'aoba{:04d}.tfrecord'.format(index)
        with tf.io.TFRecordWriter(str(output_path), options=options) as writer:
            for e in examples:
                writer.write(e)


if __name__ == '__main__':
    main()
