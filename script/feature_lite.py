#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NNUE LITEの特徴のインデックス
"""
from dataclasses import dataclass, field
from typing import List

import cshogi

__author__ = 'Yasuhiro'
__date__ = '2020/10/15'


@dataclass
class FeatureIndex:
    fu: List[int] = field(default_factory=list)
    ky: List[int] = field(default_factory=list)
    ke: List[int] = field(default_factory=list)
    gi: List[int] = field(default_factory=list)
    ka: List[int] = field(default_factory=list)
    hi: List[int] = field(default_factory=list)
    ki: List[int] = field(default_factory=list)
    ou: int = 0
    ou_square: int = 0

    def __getitem__(self, item):
        return (None, self.fu, self.ky, self.ke, self.gi, self.ka, self.hi,
                self.ki)[item]


@dataclass
class FeatureSet:
    data: List[FeatureIndex] = field(default_factory=list)


def get_feature_index(board):
    b_index, w_index = FeatureIndex(), FeatureIndex()
    # feature_size = 361

    for square in range(81):
        piece = board.piece(square)
        if piece == 0:
            continue

        piece_type = piece & 0xF
        piece_color = piece >> 4
        if piece_type == 8:
            if piece_color == 1:
                b_index.ou = 81 * 2 + 1 + square
                w_index.ou_square = 80 - square
            else:
                w_index.ou = 81 * 2 + 1 + (80 - square)
                b_index.ou_square = square
        else:
            promotion = piece_type >> 3
            raw_type = piece & 0x7

            offset = 82 if promotion else 1
            if piece_color == 0:
                b_index[raw_type].append(square + offset)
                w_index[raw_type].append(80 - square + 2 * 81 + offset)
            else:
                w_index[raw_type].append(80 - square + offset)
                b_index[raw_type].append(square + 2 * 81 + offset)

    pieces_in_hand = board.pieces_in_hand
    offset = 4 * 81 + 1
    # 駒の順序が違うので、入れ替え
    order = (1, 2, 3, 4, 7, 5, 6)
    for color in range(2):
        if color == 0:
            b_offset, w_offset = offset, offset + 18
        else:
            b_offset, w_offset = offset + 18, offset

        for piece_type in range(7):
            piece_count = pieces_in_hand[color][piece_type]
            for i in range(piece_count):
                b_index[order[piece_type]].append(i + b_offset)
                w_index[order[piece_type]].append(i + w_offset)

    if board.turn == cshogi.BLACK:
        return b_index, w_index
    else:
        return w_index, b_index


def main():
    print('a')


if __name__ == '__main__':
    main()
