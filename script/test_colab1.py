#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import cshogi
import numpy as np
from absl import flags
from absl.testing import absltest

import train_in_colab1

__author__ = 'Yasuhiro'
__date__ = '2020/12/23'

FLAGS = flags.FLAGS


class Configure(object):
    def __init__(self):
        engine_path1 = r'D:\Libraries\YaneuraOu\build\NNUE\YaneuraOu-NNUE.exe'
        engine_path2 = r'C:\Users\yasuhiro\Downloads\gikou2_win\gikou.exe'
        self.engine_path1, self.engine_path2 = engine_path1, engine_path2

        self.max_move = 256
        self.depth_limit = 3
        self.gikou_level = 2
        self.byoyomi = 100


class TestInputPair(absltest.TestCase):
    def setUp(self) -> None:
        self.cfg = Configure()
        self.yaneuraou = train_in_colab1.make_yaneuraou_engine(
            r'D:\Libraries\YaneuraOu\build\NNUE\YaneuraOu-NNUE.exe',
            cfg=self.cfg
        )
        self.gikou = train_in_colab1.make_gikou2_engine(
            r'C:\Users\yasuhiro\Downloads\gikou2_win\gikou.exe',
            cfg=self.cfg
        )

        sfen_list = r'C:\Users\yasuhiro\Downloads\taya36.sfen'
        self.sfen_list = train_in_colab1.load_sfen_list(sfen_path=sfen_list)

    def tearDown(self) -> None:
        self.yaneuraou.quit()
        self.gikou.quit()

    def get_result(self, board):
        if board.is_draw() or board.move_number > self.cfg.max_move:
            return train_in_colab1.GameResult.draw
        else:
            n = len(board.legal_moves)
            if n == 0:
                # 詰んでいる
                if board.turn == cshogi.BLACK:
                    return train_in_colab1.GameResult.white_win
                else:
                    return train_in_colab1.GameResult.black_win
            else:
                # 宣言勝ち
                if board.turn == cshogi.BLACK:
                    return train_in_colab1.GameResult.black_win
                else:
                    return train_in_colab1.GameResult.white_win

    def test_run_match0(self):
        board = cshogi.Board()
        usi_moves, result, pvs, start_step = train_in_colab1.run_match(
            black_engine=self.yaneuraou, white_engine=self.gikou,
            sfen=self.sfen_list[0], board=board, target_engine=0, cfg=self.cfg
        )

        n = 36
        self.assertEqual(start_step, n)
        # PVを求めた手数のうちの最小の手数に一致するはず
        self.assertEqual(start_step, min(pvs.keys()))

        self.assertEqual(result, self.get_result(board=board))

        for key in pvs.keys():
            # 先頭との手数の差は2の倍数のはず
            self.assertEqual((key - n) % 2, 0)

    def test_run_match1(self):
        board = cshogi.Board()
        usi_moves, result, pvs, start_step = train_in_colab1.run_match(
            black_engine=self.gikou, white_engine=self.yaneuraou,
            sfen=self.sfen_list[0], board=board, target_engine=1, cfg=self.cfg
        )

        n = 37
        self.assertEqual(start_step, n)
        # PVを求めた手数のうちの最小の手数に一致するはず
        self.assertEqual(start_step, min(pvs.keys()))

        self.assertEqual(result, self.get_result(board=board))

        for key in pvs.keys():
            # 先頭との手数の差は2の倍数のはず
            self.assertEqual((key - n) % 2, 0)

    def test_make_pair(self):
        for case in range(2):
            board = cshogi.Board()
            if case == 0:
                black, white = self.yaneuraou, self.gikou
            else:
                black, white = self.gikou, self.yaneuraou

            usi_moves, result, pvs, start_step = train_in_colab1.run_match(
                black_engine=black, white_engine=white, cfg=self.cfg,
                sfen=self.sfen_list[0], board=board, target_engine=case
            )

            training_pair = train_in_colab1.compute_training_pair(
                yaneuraou=self.yaneuraou, usi_moves=usi_moves, pvs=pvs,
                steps=start_step, cfg=self.cfg
            )

            for i, pv in pvs.items():
                if pv[0] != usi_moves[i]:
                    # 何か変なことになっている
                    continue
                if pv[0] == 'resign' or pv[0] == 'win':
                    continue
                if len(usi_moves) >= i or pv[1] == usi_moves[i + 1]:
                    # 読み筋が一致したので、学習の対象外
                    continue

                self.assertIn(i, training_pair)
                tmp1, tmp2 = training_pair[i]

                board = cshogi.Board()
                for m in usi_moves[:i + 1]:
                    board.push_usi(m)
                move1 = board.move_from_usi(tmp1[0])
                move2 = board.move_from_usi(tmp2[0])
                # ちゃんと手数に対応した指し手
                self.assertTrue(board.is_legal(move1), msg=(board, tmp1[0]))
                self.assertTrue(board.is_legal(move2))

                # 学習対象なので、こっちは実際の指し手を外した
                self.assertNotEqual(tmp1[0], usi_moves[i + 1])
                # 実際の指し手を見た後の読み筋
                self.assertEqual(tmp2[0], usi_moves[i + 1])

    def test_input_feature(self):
        for case in range(2):
            board = cshogi.Board()
            if case == 0:
                black, white = self.yaneuraou, self.gikou
            else:
                black, white = self.gikou, self.yaneuraou

            usi_moves, result, pvs, start_step = train_in_colab1.run_match(
                black_engine=black, white_engine=white, cfg=self.cfg,
                sfen=self.sfen_list[0], board=board, target_engine=case
            )
            training_pair = train_in_colab1.compute_training_pair(
                yaneuraou=self.yaneuraou, usi_moves=usi_moves, pvs=pvs,
                steps=start_step, cfg=self.cfg
            )
            input_features = train_in_colab1.make_input_feature(
                usi_moves=usi_moves, training_pair=training_pair, board=board
            )

            board.reset()
            i = 0
            for j, m in enumerate(usi_moves):
                board.push_usi(m)
                if j in training_pair:
                    for k, pv in enumerate(training_pair[j]):
                        moves = []
                        for move in pv:
                            move = board.move_from_usi(move)
                            # 局面に対応していて、合法手のはず
                            self.assertTrue(board.is_legal(move))

                            board.push(move)
                            moves.append(move)

                        p, q, p_index, q_index = train_in_colab1.fv38(
                            board=board
                        )
                        p_index = np.asarray(p_index) + p * 1548
                        q_index = np.asarray(q_index) + q * 1548

                        # PVに対応する入力の特徴ベクトルを確認
                        tmp = getattr(input_features[i],
                                      'input{}'.format(k + 1))
                        self.assertSetEqual(set(tmp.p_feature),
                                            set(p_index))
                        self.assertSetEqual(set(tmp.q_feature),
                                            set(q_index))

                        if case == board.turn:
                            # 自分の手番の局面
                            self.assertEqual(tmp.sign, 1)
                        else:
                            # 相手の手番の局面
                            self.assertEqual(tmp.sign, -1)

                        # 局面を戻す
                        for move in moves[::-1]:
                            board.pop(move)

                    i += 1

    def test_match_random_start(self):
        for i, sfen in enumerate(self.sfen_list):
            with self.subTest(i=i, sfen=sfen):
                self.sub_match_random_start(sfen=sfen)

    def sub_match_random_start(self, sfen):
        for target in range(2):
            if target == 0:
                black, white = self.yaneuraou, self.gikou
            else:
                black, white = self.gikou, self.yaneuraou

            for step in range(18, 22):
                board = cshogi.Board()
                usi_moves, result, pvs, start_step = train_in_colab1.run_match(
                    black_engine=black, white_engine=white,
                    sfen=sfen, board=board, target_engine=target,
                    cfg=self.cfg, steps=step
                )
                # 自分と違う手番から開始すると自分の手番は、その一つ後になる
                self.assertEqual(start_step, step + (step + target) % 2)
                # 千日手を回避せず、相手の指し手だけで終わった可能性がある
                if len(pvs) > 0:
                    self.assertEqual(min(pvs.keys()), start_step)
                    continue

                board.reset()
                for i, m in enumerate(usi_moves):
                    m = board.move_from_usi(m)
                    self.assertTrue(board.is_legal(m))

                    if i in pvs:
                        # PVの指し手が合法手かチェック
                        move_list = []
                        for pv_move in pvs[i]:
                            tmp_move = board.move_from_usi(pv_move)
                            self.assertTrue(board.is_legal(tmp_move))

                            board.push(tmp_move)
                            move_list.append(tmp_move)

                        for pv_move in move_list[::-1]:
                            board.pop(pv_move)
                    board.push(m)

    def test_pair_random_start(self):
        for i, sfen in enumerate(self.sfen_list):
            with self.subTest(i=i, sfen=sfen):
                self.sub_pair_random_start(sfen=sfen)

    def sub_pair_random_start(self, sfen):
        for target in range(2):
            if target == 0:
                black, white = self.yaneuraou, self.gikou
            else:
                black, white = self.gikou, self.yaneuraou

            for step in range(18, 22):
                board = cshogi.Board()
                usi_moves, result, pvs, start_step = train_in_colab1.run_match(
                    black_engine=black, white_engine=white,
                    sfen=sfen, board=board,
                    target_engine=target, cfg=self.cfg, steps=step
                )

                training_pair = train_in_colab1.compute_training_pair(
                    yaneuraou=self.yaneuraou, usi_moves=usi_moves, pvs=pvs,
                    steps=start_step, cfg=self.cfg
                )

                board.reset()
                for i, usi_move in enumerate(usi_moves):
                    tmp = board.move_from_usi(usi_move)
                    self.assertTrue(board.is_legal(tmp))
                    board.push(tmp)

                    if i in training_pair:
                        pv1, pv2 = training_pair[i]

                        tmp_list = []
                        for m in pv1:
                            m = board.move_from_usi(m)
                            self.assertTrue(board.is_legal(m))

                            board.push(m)
                            tmp_list.append(m)
                        for m in tmp_list[::-1]:
                            board.pop(m)

                        tmp_list = []
                        for m in pv2:
                            m = board.move_from_usi(m)
                            self.assertTrue(board.is_legal(m),
                                            msg=(board, pv1, pv2, i,
                                                 usi_moves))

                            board.push(m)
                            tmp_list.append(m)
                        for m in tmp_list[::-1]:
                            board.pop(m)


if __name__ == '__main__':
    absltest.main()
