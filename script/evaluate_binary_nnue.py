#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import struct
from pathlib import Path
from itertools import product

import click
import cshogi
import numpy as np
import toml
import joblib
from bitarray import bitarray

__author__ = 'Yasuhiro'
__date__ = '2020/04/30'


def evaluate(parameters, inputs):
    p, q = inputs

    # embedding
    kernel = np.round(parameters['embedding/kernel'] * 2 ** 11)
    bias = np.round(parameters['embedding/bias'] * 2 ** 11)
    h_p = np.sum(kernel[p], axis=0) + bias
    h_q = np.sum(kernel[q], axis=0) + bias
    h = np.hstack((h_p, h_q))
    # print(h / 2 ** 11)
    # 0の場合がある
    bh = np.where(h < 0, -1, 1)
    sh = np.sum(np.clip(np.abs(h), a_max=2 ** 11, a_min=0)) // 512
    # print(np.clip(h, a_max=2 ** 11, a_min=-2 ** 11) / 2 ** 11)
    # print(bh)
    # print(sh)

    # tmp = bitarray(512, endian='little')
    # for i in range(512):
    #     tmp[i] = 1 if h[i] < 0 else 0
    # print([hex(v) for v in struct.unpack('8B', tmp[:64].tobytes())])
    # print([hex(v) for v in struct.unpack('8B', tmp[64:128].tobytes())])
    # print([hex(v) for v in struct.unpack('8B', tmp[128:192].tobytes())])
    # print([hex(v) for v in struct.unpack('8B', tmp[192:256].tobytes())])

    assert np.all(bh != 0)
    # dense1
    kernel = parameters['dense1/kernel']
    bias = np.round(parameters['dense1/bias'] * 2 ** (9 + 11 - 1))
    scale = np.round(
        np.mean(np.abs(parameters['dense1/kernel']), axis=0) * 2 ** 9
    )
    h = np.matmul(bh, np.sign(kernel)) * sh * scale // 2 + bias
    # xorでbitが0の個数
    # h = (np.matmul(bh, np.sign(kernel)) + 512) / 2

    # tmp = bitarray(512 * 128, endian='little')
    # for n, m, k, j, i in product(range(16), range(2), range(8), range(4),
    #                              range(64)):
    #     tmp[n * 4096 + m * 2048 + k * 256 + j * 64 + i] = (
    #         1 if kernel[k * 64 + i, n * 8 + m + j * 2] < 0 else 0
    #     )
    # for i in range(9):
    #     print([hex(v) for v in struct.unpack(
    #         '8I', tmp[256 * i:256 * (i + 1)].tobytes()
    #     )])
    # print(bias[:8])
    # print(scale[:8])

    # print(h[:8])
    # print(h[8:16])

    # for i in range(16):
    #     print(h[8 * i:8 * (i + 1)] // 2 ** 11)

    # print(h)
    h = h // 2 ** 10
    bh = np.where(h < 0, -1, 1)
    sh = np.sum(np.clip(np.abs(h), a_min=0, a_max=2 ** 9)) // 128

    # tmp = bitarray(128, endian='little')
    # for m, k, j, i in product(range(4), range(2), range(4), range(4)):
    #     tmp[m * 32 + k * 16 + j * 4 + i] = (
    #         1 if h[m * 32 + k * 4 + j * 8 + i] < 0 else 0
    #     )
    # print([hex(v) for v in struct.unpack('8B', tmp[:64].tobytes())])
    # print([hex(v) for v in struct.unpack('8B', tmp[64:128].tobytes())])

    assert np.all(bh != 0)
    # dense2
    kernel = parameters['dense2/kernel']
    bias = np.round(parameters['dense2/bias'] * 2 ** (11 + 9) / 2)
    scale = np.round(
        np.mean(np.abs(parameters['dense2/kernel']), axis=0) * 2 ** 11
    )
    h = np.matmul(bh, np.sign(kernel)) * sh * scale // 2 + bias
    # xorでbitが0の個数
    # h = (np.matmul(bh, np.sign(kernel)) + 128) / 2
    # h = np.matmul(bh, np.sign(kernel)) // 2

    # print(sh)
    # print(bias[:8])
    # print(scale[:8])

    # tmp = bitarray(128 * 128, endian='little')
    # for q, p, o, n, m, k, j, i in product(range(16), range(2), range(2),
    #                                       range(4), range(2), range(2),
    #                                       range(4), range(4)):
    #     tmp[q * 1024 + p * 512 + o * 256 + n * 64 + m * 32 + k * 16 + j * 4
    #         + i] = (
    #         1 if kernel[o * 64 + m * 32 + k * 4 + j * 8 + i,
    #                     q * 8 + p + n * 2] < 0 else 0
    #     )
    # for i in range(9):
    #     print([hex(v) for v in struct.unpack(
    #         '8I', tmp[256 * i:256 * (i + 1)].tobytes()
    #     )])

    # print(h[:8])
    # print(h[8:16])

    h = h + np.abs(h) // 2
    h = h // 2 ** 16

    # dense3
    kernel = parameters['dense3/kernel']
    bias = np.round(parameters['dense3/bias'] * 2 ** (8 + 3) * 600)
    scale = np.round(
        np.mean(np.abs(parameters['dense3/kernel']), axis=0) * 2 ** 8 * 600
    )

    h = np.matmul(h, np.sign(kernel)) * scale + bias
    h = h // 2 ** 11
    return h[0]


def get_intermediate_values(config, model_dir, inputs):
    import tensorflow as tf

    from learn_nnue import get_model as _get_model

    model_config = config['model']
    network = _get_model(config=model_config)
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    p = tf.keras.Input(shape=(38,), dtype=tf.int32)
    q = tf.keras.Input(shape=(38,), dtype=tf.int32)
    out = network([p, q])
    model = tf.keras.Model([p, q], out)

    d = {'output': out}
    tmp = model.layers[2]
    for i in range(3):
        a = getattr(tmp, 'activation{}'.format(i))
        d['pre{}'.format(i)] = a.input
        d['post{}'.format(i)] = a.output
    model = tf.keras.Model([p, q], d)
    result = model(inputs)

    result = {key: value.numpy() for key, value in result.items()}
    return result


def get_inputs(data):
    from make_record2 import fv40

    board = cshogi.Board()
    size = 1548

    p_values, q_values = [], []
    for d in data:
        board.set_psfen(d['sfen'])
        p, q, p_index, q_index = fv40(board=board)
        p = np.asarray(p_index, dtype=np.int32) + p * size
        q = np.asarray(q_index, dtype=np.int32) + q * size
        p_values.append(p)
        q_values.append(q)
    p_values = np.vstack(p_values)
    q_values = np.vstack(q_values)
    return p_values, q_values


@click.command()
@click.option('--data-path', type=click.Path(exists=True),
              default='../data/x036')
@click.option('--n-samples', type=int, default=10)
@click.option('--model-dir', type=click.Path(exists=True),
              default='../models/binary/case8/500')
def cmd(data_path, n_samples, model_dir):
    model_dir = Path(model_dir)

    output_dir = Path('sample')
    if not output_dir.exists():
        output_dir.mkdir(parents=True)

    input_path = output_dir / 'input.npz'
    if input_path.exists():
        tmp = np.load(str(input_path))
        p = tmp['p']
        q = tmp['q']
    else:
        data = np.fromfile(data_path, dtype=cshogi.PackedSfenValue)
        p, q = get_inputs(data=data[:n_samples])
        np.savez_compressed(str(input_path), p=p, q=q)
    p = p.astype(np.int32)
    q = q.astype(np.int32)

    score_path = output_dir / 'score.npy'
    if score_path.exists():
        data_score = np.load(str(score_path))
        data_sfen = joblib.load(str(output_dir / 'sfen.pickle'))
    else:
        data = np.fromfile(data_path, dtype=cshogi.PackedSfenValue)
        data_score = data[:n_samples]['score']
        np.save(str(score_path), data_score)

        board = cshogi.Board()
        data_sfen = []
        for d in data[:n_samples]:
            board.set_psfen(d['sfen'])
            data_sfen.append(board.sfen())
        joblib.dump(data_sfen, str(output_dir / 'sfen.pickle'))

    output_path = output_dir / 'output.npy'
    if output_path.exists():
        tf_score = np.load(str(output_path))
        parameters = np.load(str(output_dir / 'parameters.npz'))
    else:
        with (model_dir / 'config.toml').open() as f:
            config = toml.load(f)
        d = get_intermediate_values(config=config, model_dir=model_dir,
                                    inputs=[p, q])
        tf_score = d['output']
        for key, value in d.items():
            if key == 'output':
                np.save(str(output_path), value)
            np.savetxt(str(output_dir / (key + '.txt')), value,
                       fmt='%.3g', delimiter=', ')

        from learn_nnue import get_parameters

        parameters = get_parameters(config=config, model_dir=model_dir)
        np.savez_compressed(str(output_dir / 'parameters.npz'), **parameters)

    with (output_dir / 'sample.txt').open('w') as f:
        for i in range(10):
            inputs = (p[i], q[i])
            score = evaluate(parameters=parameters, inputs=inputs)
            s = '{}, {}, {}, {}\n'.format(data_sfen[i], data_score[i], score,
                                          tf_score[i] * 600)
            f.write(s)


def main():
    cmd()


if __name__ == '__main__':
    main()
