#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
YaneuraOuで学習に使ったモデルが正しいパラメータで初期化できているか確認する
"""
import sys

import torch
import numpy as np
import shogi
import cshogi

try:
    sys.path.append('../thirdparty/shogi-eval')
    # noinspection PyUnresolvedReferences
    from evaltest_nnue import fv40
except ImportError:
    print('error')

__author__ = 'Yasuhiro'
__date__ = '2020/12/02'


class Embedding(torch.nn.Module):
    def __init__(self):
        super(Embedding, self).__init__()

        self.embedding = torch.nn.Embedding(125388, 256)
        self.bias = torch.nn.Parameter(torch.zeros(256))

    def forward(self, x):
        h = self.embedding(x)
        h = h.sum(dim=1)
        y = h + self.bias
        return y


class ClippedReLU(torch.nn.Module):
    def __init__(self):
        super(ClippedReLU, self).__init__()

    def forward(self, x):
        h = 1 - torch.relu(1 - x)
        y = torch.relu(h)
        return y


class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        self.feature_transformer = Embedding()
        self.affine1 = torch.nn.Linear(512, 32)
        self.affine2 = torch.nn.Linear(32, 32)
        self.affine3 = torch.nn.Linear(32, 1)
        self.relu = ClippedReLU()

    def forward(self, x):
        p = self.feature_transformer(x[0])
        q = self.feature_transformer(x[1])
        h = torch.cat((p, q), dim=1)

        h = self.affine1(self.relu(h))
        h = self.affine2(self.relu(h))
        y = self.affine3(self.relu(h))
        return y


def main():
    sfen_list = (
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w RGgsn5p 1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b RGgsn5p 1'
    )

    p_array = np.empty((len(sfen_list), 38), dtype=np.int32)
    q_array = np.empty_like(p_array)

    for i, sfen in enumerate(sfen_list):
        p, q, p_index, q_index = fv40(sfen)
        p_array[i] = p_index
        p_array[i] += 1548 * p
        q_array[i] = q_index
        q_array[i] += 1548 * q
    p_array = torch.LongTensor(p_array)
    q_array = torch.LongTensor(q_array)

    model_path = (r'D:\Libraries\YaneuraOu\build\NNUE\evalsave\1'
                  r'\model.pt')
    net = Net()
    module = torch.jit.load(model_path)
    # [32, 512]
    net.affine1.weight = torch.nn.Parameter(module.affine1.weight)
    net.affine1.bias = torch.nn.Parameter(module.affine1.bias)
    net.affine2.weight = torch.nn.Parameter(module.affine2.weight)
    net.affine2.bias = torch.nn.Parameter(module.affine2.bias)
    net.affine3.weight = torch.nn.Parameter(module.affine3.weight)
    net.affine3.bias = torch.nn.Parameter(module.affine3.bias)
    # [125388, 256]
    net.feature_transformer.embedding.weight = torch.nn.Parameter(
        module.feature_transformer.embedding.weight)
    net.feature_transformer.bias = torch.nn.Parameter(
        module.feature_transformer.bias)
    print(net([p_array, q_array]) * 600)


if __name__ == '__main__':
    main()
