#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NNUEを教師あり学習させる
いい感じの初期値を手に入れる
"""

import re
import struct
from itertools import product
from pathlib import Path

import click
import cshogi
import joblib
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_addons as tfa
import toml
from bitarray import bitarray
from tqdm import trange

from make_record2 import fv40
from nnue import NNUESigmoid, BackwardNNUE, BinaryNNUE
from wf_gym import make_usi

__author__ = 'Yasuhiro'
__date__ = '2019/06/12'


def make_dataset(record_path, batch_size, separated):
    files = tf.io.gfile.glob(record_path)
    dataset = tf.data.TFRecordDataset(
        filenames=files, compression_type='GZIP', num_parallel_reads=10
    )
    dataset = dataset.shuffle(batch_size * 10)
    if separated:
        dataset = dataset.map(parse_separated, num_parallel_calls=20)
    else:
        dataset = dataset.map(parse, num_parallel_calls=20)
    dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


def parse(serialized):
    features = {
        'p': tf.io.FixedLenFeature([38], tf.int64),
        'q': tf.io.FixedLenFeature([38], tf.int64),
        'score': tf.io.FixedLenFeature([], tf.int64),
        'ply': tf.io.FixedLenFeature([], tf.int64),
        'result': tf.io.FixedLenFeature([], tf.int64)
    }
    example = tf.io.parse_single_example(serialized, features)

    score = tf.cast(example['score'], tf.float32)
    result = tf.cast(example['result'], tf.float32)
    elmo_lambda = 0.9
    ponanza_constant = 600.0
    target = ((1 - elmo_lambda) * (result + 1) * 0.5 +
              elmo_lambda * tf.math.sigmoid(score / ponanza_constant))
    target = tf.clip_by_value(target, clip_value_max=0.99, clip_value_min=0.01)

    data = {
        'p': tf.cast(example['p'], tf.int32),
        'q': tf.cast(example['q'], tf.int32),
        'target': target
    }
    return data


def make_piece_table():
    table = [-9999]

    # 持ち駒
    # 同じ種類の駒の交換が起こりやすいと思うので、自分と相手の駒が交互に並ぶ順で定める
    piece_size = [18, 4, 4, 4, 4, 2, 2]
    # 持ち駒の番号
    # 最後はKA, HI, KIの順
    order = (0, 1, 2, 3, 6, 4, 5)
    for i, size in zip(order, piece_size):
        table.extend([i * 2 + 0 for _ in range(size + 1)])
        table.extend([i * 2 + 1 for _ in range(size + 1)])

    n = len(table)
    # 盤上の駒
    # FU, KY, KE, GI, KA, HI, KI, UM, RYの順に番号を割り当てる
    # 持ち駒と同様に自分と相手の駒が交互に並ぶ
    order = (0, 1, 2, 3, 6, 4, 7, 5, 8)
    for i in order:
        table.extend([14 + i * 2 + 0 for _ in range(81)])
        table.extend([14 + i * 2 + 1 for _ in range(81)])

    # FUの先頭を削る
    table.pop(n)
    assert len(table) == 1548, len(table)
    return np.asarray(table, dtype=np.int32)


def make_square_table():
    table = [-9999]

    # 持ち駒
    # 同じ種類の駒の交換が起こりやすいと思うので、自分と相手の駒が交互に並ぶ順で定める
    piece_size = [18, 4, 4, 4, 4, 2, 2]
    for size in piece_size:
        # 自分
        table.extend([i for i in range(size + 1)])
        # 相手
        table.extend([i for i in range(size + 1)])

    n = len(table)
    # 盤上の駒
    # 駒は9種類
    for _ in range(9 * 2):
        table.extend([19 + i for i in range(81)])

    # FUの先頭を削る
    table.pop(n)
    assert len(table) == 1548
    return np.asarray(table, dtype=np.int32)


PIECE_TABLE = make_piece_table()
SQUARE_TABLE = make_square_table()


def parse_separated(serialized):
    data = parse(serialized=serialized)

    p_ou = tf.math.floordiv(data['p'][0], 1548)
    q_ou = tf.math.floordiv(data['p'][0], 1548)
    p = tf.math.floormod(data['p'], 1548)
    q = tf.math.floormod(data['q'], 1548)

    p_piece = tf.nn.embedding_lookup(PIECE_TABLE, p)
    p_square = tf.nn.embedding_lookup(SQUARE_TABLE, p)
    q_piece = tf.nn.embedding_lookup(PIECE_TABLE, q)
    q_square = tf.nn.embedding_lookup(SQUARE_TABLE, q)

    # 相手の王の座標は自分から見た座標にする
    p_piece = p_piece + (p_ou * 81 + (80 - q_ou)) * 32
    q_piece = q_piece + (q_ou * 81 + (80 - p_ou)) * 32

    p_square = p_square + p_ou * (81 + 19)
    q_square = q_square + q_ou * (81 + 19)

    result = {
        'p_piece': p_piece, 'q_piece': q_piece,
        'p_square': p_square, 'q_square': q_square,
        'target': data['target']
    }
    return result


def get_model(config):
    name = config['name']
    if name == 'vanilla':
        network = NNUESigmoid()
    elif name == 'backward':
        print(config)
        network = BackwardNNUE(
            embedding_size=config['embedding_size'],
            units1=config['units1'], units2=config['units2']
        )
    elif name == 'binary':
        network = BinaryNNUE(
            embedding_size=config['embedding_size'],
            units1=config['units1'], units2=config['units2'],
            separated=config.get('separated', False)
        )
    else:
        raise ValueError(name)
    return network


def get_optimizer(config):
    if config['name'] == 'sgd':
        optimizer = tf.keras.optimizers.SGD(
            learning_rate=config['learning_rate'], momentum=config['momentum']
        )
    elif config['name'] == 'yogi':
        optimizer = tfa.optimizers.Yogi(
            learning_rate=config['learning_rate']
        )
    else:
        raise ValueError(config['name'])
    return optimizer


class Max(tf.keras.metrics.Metric):
    def __init__(self, name='max', **kwargs):
        super(Max, self).__init__(name=name, **kwargs)
        self.m = self.add_weight(
            name='max', initializer=tf.keras.initializers.constant(-1e3)
        )

    def update_state(self, value):
        self.m.assign(tf.math.maximum(self.m, tf.math.reduce_max(value)))

    def result(self):
        return self.m


class Min(tf.keras.metrics.Metric):
    def __init__(self, name='min', **kwargs):
        super(Min, self).__init__(name=name, **kwargs)
        self.m = self.add_weight(
            name='min', initializer=tf.keras.initializers.constant(1e3)
        )

    def update_state(self, value):
        self.m.assign(tf.math.minimum(self.m, tf.math.reduce_min(value)))

    def result(self):
        return self.m


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def initialize(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    model_config = config['model']
    network = get_model(config=model_config)
    optimizer = get_optimizer(config=config['optimizer'])
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=network, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)

    if config['model']['separated']:
        inputs_p_piece = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs_q_piece = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs_p_square = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs_q_square = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs = [[inputs_p_piece, inputs_p_square],
                  [inputs_q_piece, inputs_q_square]]
        model_index = 4
    else:
        inputs_p = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs_q = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs = [inputs_p, inputs_q]
        model_index = 2
    outputs = network(inputs)
    base_model = tf.keras.Model(inputs, outputs)
    model = tf.keras.Model(
        inputs,
        [outputs, base_model.layers[model_index].affine1.input,
         base_model.layers[model_index].affine2.input,
         base_model.layers[model_index].affine3.input]
    )

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'],
                           separated=config['model']['separated'])

    criterion = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    metrics_ce = tf.keras.metrics.BinaryCrossentropy(
        'cross_entropy', from_logits=True
    )
    metrics_entropy = tf.keras.metrics.BinaryCrossentropy(
        'entropy', from_logits=False
    )
    metrics_loss = tf.keras.metrics.Mean('loss')

    max1, min1 = Max('affine1_max'), Min('affine1_min')
    max2, min2 = Max('affine2_max'), Min('affine2_min')
    max3, min3 = Max('affine3_max'), Min('affine3_min')
    metrics = {'min1': min1, 'min2': min2, 'min3': min3,
               'max1': max1, 'max2': max2, 'max3': max3}

    r = re.compile(r'((?:affine[1-3]|feature)/'
                   r'(?:kernel|bias|piece_embedding|square_embedding))')

    def _get_name(variable):
        match = r.search(variable.name)
        return match.group(1)

    def step_train(x, y):
        with tf.GradientTape() as tape:
            scores, in1, in2, in3 = model(x)
            scores = tf.squeeze(scores)

            loss = criterion(y_true=y, y_pred=scores)
        gradients = tape.gradient(loss, network.trainable_variables)
        optimizer.apply_gradients(zip(gradients,
                                      network.trainable_variables))
        for j, obj in enumerate((in1, in2, in3), start=1):
            metrics['min{}'.format(j)].update_state(obj)
            metrics['max{}'.format(j)].update_state(obj)

        metrics_ce.update_state(y_true=y, y_pred=scores)
        metrics_loss.update_state(loss)
        metrics_entropy.update_state(y_true=y, y_pred=y)

        return gradients

    @tf.function
    def step_optimization(dataset_inputs):
        gradients = step_train(
            [[dataset_inputs['p_piece'], dataset_inputs['p_square']],
             [dataset_inputs['q_piece'], dataset_inputs['q_square']]],
            dataset_inputs['target']
        )
        return gradients

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    for i, data in zip(trange(config['iterations']), dataset):
        grads = step_optimization(data)

        if (i + 1) % 100 == 0:
            with summary_writer.as_default():
                tf.summary.scalar('metrics/cross_entropy', metrics_ce.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar(
                    'metrics/kl',
                    metrics_ce.result() - metrics_entropy.result(),
                    step=int(ckpt.step)
                )
                tf.summary.scalar(
                    'loss/total', metrics_loss.result(), step=int(ckpt.step)
                )

                for k, name in product(range(1, 4), ('min', 'max')):
                    tf.summary.scalar(
                        'input/affine{k}/{name}'.format(k=k, name=name),
                        metrics['{name}{k}'.format(name=name, k=k)].result(),
                        step=int(ckpt.step)
                    )
                for k in range(1, 4):
                    tmp = getattr(base_model.layers[model_index],
                                  'affine{}'.format(k))
                    tf.summary.scalar(
                        'kernel/affine{}/max'.format(k),
                        tf.math.reduce_max(tf.abs(tmp.kernel)),
                        step=int(ckpt.step)
                    )

                if int(ckpt.step) % 10 == 0:
                    for g, v in zip(grads, network.trainable_variables):
                        # print(v.name)
                        name = _get_name(v)
                        tf.summary.histogram(
                            'variable/{}'.format(name), tf.reshape(v, [-1]),
                            step=int(ckpt.step)
                        )
                        tf.summary.histogram(
                            'gradient/{}'.format(name), tf.reshape(g, [-1]),
                            step=int(ckpt.step)
                        )

            metrics_ce.reset_states()
            metrics_entropy.reset_states()
            metrics_loss.reset_states()

            for m in metrics.values():
                m.reset_states()

            ckpt.step.assign_add(1)
            manager.save()
    summary_writer.flush()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def initialize_multi(config_path):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        for k in range(len(physical_devices)):
            tf.config.experimental.set_memory_growth(physical_devices[k], True)
            print('memory growth:', tf.config.experimental.get_memory_growth(
                physical_devices[k]))
    else:
        print("Not enough GPU hardware devices available")

    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    mirrored_strategy = tf.distribute.MirroredStrategy()
    with mirrored_strategy.scope():
        model_config = config['model']
        network = get_model(config=model_config)

        optimizer = get_optimizer(config=config['optimizer'])

        metrics_ce = tf.keras.metrics.BinaryCrossentropy(
            'cross_entropy', from_logits=True
        )
        metrics_entropy = tf.keras.metrics.BinaryCrossentropy(
            'entropy', from_logits=False
        )
        metrics_loss = tf.keras.metrics.Mean('loss')

        max1, min1 = Max('affine1_max'), Min('affine1_min')
        max2, min2 = Max('affine2_max'), Min('affine2_min')
        max3, min3 = Max('affine3_max'), Min('affine3_min')
        metrics = {'min1': min1, 'min2': min2, 'min3': min3,
                   'max1': max1, 'max2': max2, 'max3': max3}

        # noinspection PyTypeChecker
        ckpt = tf.train.Checkpoint(
            step=tf.Variable(1), network=network, optimizer=optimizer
        )
        manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                             max_to_keep=2)
        ckpt.restore(manager.latest_checkpoint)

        inputs_p = tf.keras.Input(shape=[38], dtype=tf.int32)
        inputs_q = tf.keras.Input(shape=[38], dtype=tf.int32)
        outputs = network([inputs_p, inputs_q])
        base_model = tf.keras.Model([inputs_p, inputs_q], outputs)
        model = tf.keras.Model(
            [inputs_p, inputs_q],
            [outputs, base_model.layers[2].affine1.input,
             base_model.layers[2].affine2.input,
             base_model.layers[2].affine3.input]
        )

    global_batch_size = config['batch_size']
    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=global_batch_size,
                           separated=config['model']['separated'])
    dist_dataset = mirrored_strategy.experimental_distribute_dataset(dataset)

    # r = re.compile(r'((?:transformer|affine[1-3])/'
    #                r'(?:kernel|kernel_base|bias)):0')
    #
    # def _get_name(variable):
    #     match = r.search(variable.name)
    #     return match.group(1)

    weight = tf.constant(1.0 / global_batch_size)

    with mirrored_strategy.scope():
        def step_train(dataset_inputs):
            x = (dataset_inputs['p'], dataset_inputs['q'])
            y = dataset_inputs['target']

            with tf.GradientTape() as tape:
                scores, in1, in2, in3 = model(x)
                scores = tf.squeeze(scores)

                cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=y, logits=scores
                )
                loss = tf.reduce_sum(cross_entropy) * weight
            gradients = tape.gradient(loss, network.trainable_variables)
            optimizer.apply_gradients(zip(
                gradients, network.trainable_variables
            ))

            for j, obj in enumerate((in1, in2, in3), start=1):
                metrics['min{}'.format(j)].update_state(obj)
                metrics['max{}'.format(j)].update_state(obj)

            metrics_ce.update_state(y_true=y, y_pred=scores)
            metrics_loss.update_state(loss)
            metrics_entropy.update_state(y_true=y, y_pred=y)
            return loss

        # @tf.function
        def distributed_step(dataset_inputs):
            per_replica_losses = mirrored_strategy.experimental_run_v2(
                step_train, args=(dataset_inputs,))
            return mirrored_strategy.reduce(
                tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None
            )

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    with mirrored_strategy.scope():
        for i, inputs in zip(trange(config['iterations']), dist_dataset):
            distributed_step(inputs)

            if (i + 1) % 100 == 0:
                with summary_writer.as_default():
                    tf.summary.scalar(
                        'metrics/cross_entropy', metrics_ce.result(),
                        step=int(ckpt.step.values[0])
                    )
                    tf.summary.scalar(
                        'metrics/kl',
                        metrics_ce.result() - metrics_entropy.result(),
                        step=int(ckpt.step.values[0])
                    )
                    tf.summary.scalar(
                        'loss/total', metrics_loss.result(),
                        step=int(ckpt.step.values[0])
                    )

                    for k, name in product(range(1, 4), ('min', 'max')):
                        tf.summary.scalar(
                            'input/affine{k}/{name}'.format(k=k, name=name),
                            metrics[
                                '{name}{k}'.format(name=name, k=k)].result(),
                            step=int(ckpt.step)
                        )
                    for k in range(1, 4):
                        tmp = getattr(base_model.layers[2],
                                      'affine{}'.format(k))
                        tf.summary.scalar(
                            'kernel/affine{}/max'.format(k),
                            tf.math.reduce_max(tf.abs(tmp.kernel)),
                            step=int(ckpt.step)
                        )

                metrics_ce.reset_states()
                metrics_entropy.reset_states()
                metrics_loss.reset_states()

                for m in metrics.values():
                    m.reset_states()

                ckpt.step.assign_add(1)
                manager.save()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def view_train_score(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    network = NNUESigmoid()
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'],
                           separated=config['model']['separated'])
    for data in dataset:
        scores, _ = network([data['p'], data['q']])
        predictions = tf.squeeze(tf.math.sigmoid(scores))

        df = pd.DataFrame({'prediction': predictions.numpy(),
                           'target': data['target'].numpy()})
        df.to_csv(config_path.with_name('train_data.csv'))
        break


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def export_train_score(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    network = NNUESigmoid()
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'],
                           separated=config['model']['separated'])
    p_list, q_list = [], []
    prediction_list, target_list = [], []
    for i, data in enumerate(dataset):
        if i > 10:
            break

        scores, _ = network([data['p'], data['q']])
        predictions = tf.squeeze(tf.math.sigmoid(scores))

        prediction_list.append(predictions)
        target_list.append(data['target'])
        p_list.append(data['p'])
        q_list.append(data['q'])

    p_list = tf.concat(p_list, axis=0).numpy()
    q_list = tf.concat(q_list, axis=0).numpy()
    prediction_list = tf.concat(prediction_list, axis=0).numpy()
    target_list = tf.concat(target_list, axis=0).numpy()

    np.savez_compressed(
        str(model_dir / 'sampled_data{:04}.npz'.format(int(ckpt.step))),
        p=p_list, q=q_list, prediction=prediction_list, target=target_list
    )


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def make_test_data(config_path):
    config_path = Path(config_path)
    with config_path.open('r') as f:
        config = toml.load(f)

    usi = make_usi(config=config['engine'])
    records = []
    for _ in range(10):
        sfen = 'startpos moves'
        move_list = []
        score_list = []
        while True:
            usi.usi_position(sfen)
            usi.usi_go_and_wait_bestmove("time 0 byoyomi 1000")
            best_move = usi.think_result.bestmove
            score = usi.think_result.pvs[0].eval

            score = 0 if score is None else int(score)

            move_list.append(best_move)
            score_list.append(score)

            # 投了 or 宣言勝ち
            if best_move == "resign" or best_move == "win":
                break

            sfen += ' ' + best_move
        records.append({'move': move_list, 'score': score_list})
    joblib.dump(records, config_path.with_name('sample_record.pickle'))

    usi.disconnect()


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def view_test_score(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent

    network = NNUESigmoid()
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    records = joblib.load(config_path.with_name('sample_record.pickle'))
    for j, record in enumerate(records):
        board = cshogi.Board()

        n = len(record['move']) - 1
        p_list = np.empty([n, 38], dtype=np.int32)
        q_list = np.empty_like(p_list)
        for i, move in enumerate(record['move'][:-1]):
            p, q, p_index, q_index = fv40(board=board)
            p_list[i] = p_index
            p_list[i] += p
            q_list[i] = q_index
            q_list[i] += q

            board.push_usi(move)

        p_list = tf.convert_to_tensor(p_list)
        q_list = tf.convert_to_tensor(q_list)
        behavior_score, _ = network([p_list, q_list])
        prediction = tf.squeeze(behavior_score)
        target = np.asarray(record['score'][:-1]) / 600.0

        df = pd.DataFrame({'prediction': prediction.numpy(),
                           'target': target})
        df.to_csv(config_path.with_name('test_data{}.csv'.format(j)))


@tf.function
def get_alpha(step):
    step = tf.cast(step, dtype=tf.float32)
    pi = tf.constant(np.pi, dtype=tf.float32)
    return (tf.math.cos(step * pi / 1e2) + 1.0) * 0.5


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def convert(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    network = NNUESigmoid()
    optimizer = tf.keras.optimizers.SGD(
        learning_rate=config['learning_rate'], momentum=0.9
    )
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=network, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint)
    if not manager.latest_checkpoint:
        print('loading the initialized model')
        # noinspection PyTypeChecker
        tmp_ckpt = tf.train.Checkpoint(
            step=tf.Variable(1), network=network, optimizer=optimizer
        )
        tmp_ckpt.restore(config['initialized_model'])

    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=config['batch_size'],
                           separated=config['model']['separated'])

    criterion = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    metrics = tf.keras.metrics.BinaryCrossentropy('cross_entropy',
                                                  from_logits=True)

    r = re.compile(r'((?:transformer|affine[1-3])/'
                   r'(?:kernel|kernel_base|bias)):0')

    def _get_name(variable):
        match = r.search(variable.name)
        return match.group(1)

    @tf.function
    def step(x, y, a):
        gradients = None
        for _ in range(5):
            with tf.GradientTape() as tape:
                scores, histograms = network(x, mask=a)
                scores = tf.squeeze(scores)

                loss = criterion(y_true=y, y_pred=scores)
            gradients = tape.gradient(loss, network.trainable_variables)
            optimizer.apply_gradients(zip(gradients,
                                          network.trainable_variables))

            metrics.update_state(y_true=y, y_pred=scores)
        return gradients

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    alpha = get_alpha(ckpt.step)

    for i, data in zip(trange(config['iterations']), dataset):
        grads = step([data['p'], data['q']], data['target'], alpha)

        if (i + 1) % 1000 == 0:
            with summary_writer.as_default():
                tf.summary.scalar('cross_entropy', metrics.result(),
                                  step=int(ckpt.step))
                tf.summary.scalar('alpha', alpha, step=int(ckpt.step))

                if int(ckpt.step) % 10 == 0:
                    for g, v in zip(grads, network.trainable_variables):
                        name = _get_name(v)
                        tf.summary.histogram(
                            'variable/{}'.format(name), tf.reshape(v, [-1]),
                            step=int(ckpt.step)
                        )
                        tf.summary.histogram(
                            'gradient/{}'.format(name), tf.reshape(g, [-1]),
                            step=int(ckpt.step)
                        )
            metrics.reset_states()

            ckpt.step.assign_add(1)
            manager.save()

            alpha = get_alpha(ckpt.step)


@cmd.command()
@click.option('--config-path', type=click.Path(dir_okay=False, exists=True))
def convert_multi(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    mirrored_strategy = tf.distribute.MirroredStrategy()
    with mirrored_strategy.scope():
        network = NNUESigmoid()
        optimizer = tf.keras.optimizers.SGD(
            learning_rate=config['learning_rate'], momentum=0.9
        )
        metrics = tf.keras.metrics.Mean('cross_entropy')

        # noinspection PyTypeChecker
        ckpt = tf.train.Checkpoint(
            step=tf.Variable(1), network=network, optimizer=optimizer
        )
        manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                             max_to_keep=2)
        ckpt.restore(manager.latest_checkpoint)
        if not manager.latest_checkpoint:
            print('loading the initialized model')
            # noinspection PyTypeChecker
            tmp_ckpt = tf.train.Checkpoint(
                step=tf.Variable(1), network=network, optimizer=optimizer
            )
            tmp_ckpt.restore(config['initialized_model'])

    global_batch_size = config['batch_size']
    dataset = make_dataset(record_path=config['record_path'],
                           batch_size=global_batch_size,
                           separated=config['model']['separated'])
    dist_dataset = mirrored_strategy.experimental_distribute_dataset(dataset)

    with mirrored_strategy.scope():
        def step(dataset_inputs, a):
            x = (dataset_inputs['p'], dataset_inputs['q'])
            y = dataset_inputs['target']

            for _ in range(10):
                with tf.GradientTape() as tape:
                    scores, histograms = network(x, mask=a)
                    scores = tf.squeeze(scores)

                    cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(
                        labels=y, logits=scores
                    )
                    loss = (tf.reduce_sum(cross_entropy) *
                            (1.0 / global_batch_size))
                gradients = tape.gradient(loss, network.trainable_variables)
                optimizer.apply_gradients(zip(
                    gradients, network.trainable_variables
                ))

                metrics.update_state(loss)
            return cross_entropy

        @tf.function
        def distributed_step(dataset_inputs, a):
            per_replica_losses = mirrored_strategy.experimental_run_v2(
                step, args=(dataset_inputs, a))
            return mirrored_strategy.reduce(
                tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None
            )

        @tf.function
        def _get_alpha(step_count):
            step_count = tf.cast(step_count, dtype=tf.float32)
            pi = tf.constant(np.pi, dtype=tf.float32)
            return (tf.math.cos(step_count * pi / 1e2) + 1.0) * 0.5

    log_dir = str(config_path.parent / 'logs/gradient_tape')
    summary_writer = tf.summary.create_file_writer(log_dir)

    with mirrored_strategy.scope():
        alpha = _get_alpha(ckpt.step)

        for i, inputs in zip(trange(config['iterations']), dist_dataset):
            distributed_step(inputs, alpha)

            if (i + 1) % 1000 == 0:
                with summary_writer.as_default():
                    tf.summary.scalar('cross_entropy', metrics.result(),
                                      step=int(ckpt.step.values[0]))
                    tf.summary.scalar('alpha', alpha,
                                      step=int(ckpt.step.values[0]))

                metrics.reset_states()

                ckpt.step.assign_add(1)
                manager.save()

                alpha = _get_alpha(ckpt.step)


# noinspection SpellCheckingInspection
@cmd.command()
@click.option('--config-path', type=click.Path(exists=True))
def view(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent

    network = NNUESigmoid()
    ckpt = tf.train.Checkpoint(network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    sfen_list = (
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w RGgsn5p '
        '1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b RGgsn5p 1'
    )

    # a = 0.17706499785191226
    size = 1548

    for sfen in sfen_list:
        board = cshogi.Board(sfen)
        p, q, p_list, q_list = fv40(board=board)
        p_list = np.asarray(p_list) + p * size
        q_list = np.asarray(q_list) + q * size

        p_list = np.reshape(p_list, [1, -1])
        q_list = np.reshape(q_list, [1, -1])

        score, histograms = network([p_list, q_list], mask=0)
        print(score.numpy() * 600)
        # value = histograms['post/activation3'].numpy()
        # print(np.round(value, 2) * 127)


@cmd.command()
@click.option('--config-path', type=click.Path(exists=True))
def export(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent

    network = NNUESigmoid()
    ckpt = tf.train.Checkpoint(network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    dummy = np.zeros([1, 38], dtype=np.int32)
    network([dummy, dummy])

    a = 0.17706499785191226

    with (model_dir / 'nn.bin').open('wb') as f:
        f.write(struct.pack('I', 0x7AF32F16))
        f.write(struct.pack('I', 1046128366))
        architecture = (
            'Features=HalfKP(Friend)[125388->256x2],'
            'Network=AffineTransform[1<-32](ClippedReLU[32]('
            'AffineTransform[32<-32](ClippedReLU[32]('
            'AffineTransform[32<-512](InputSlice[512(0:512)])))))'
        )
        f.write(struct.pack('I', len(architecture)))
        f.write(architecture.encode('utf-8'))
        f.write(struct.pack('I', 0x5D69D7B8))

        # transformer
        kernel = network.transformer.kernel
        kernel_base = network.transformer.kernel_base
        w = tf.tile(kernel_base, multiples=[81, 1]) + kernel
        w = tf.cast(tf.round(w * a * 127), dtype=tf.int16)
        bias = network.transformer.bias
        b = tf.cast(tf.round((bias * a + 0.5) * 127), dtype=tf.int16)

        f.write(b.numpy().tobytes())
        f.write(w.numpy().tobytes())
        f.write(struct.pack('I', 0x63337156))

        for i, layer in enumerate((network.affine1, network.affine2,
                                   network.affine3)):
            if i == 2:
                w = tf.round(600.0 * 16 / 127 * layer.kernel)
                print(tf.squeeze(w).numpy())
                b = tf.cast(tf.round(600.0 * 16 * layer.bias),
                            dtype=tf.int32)
            else:
                w = tf.round(64 * a * layer.kernel)
                b = tf.cast(tf.round(127 * 64 * (layer.bias * a + 0.5)),
                            dtype=tf.int32)
            w = tf.clip_by_value(w, clip_value_min=-128, clip_value_max=127)
            w = tf.cast(w, dtype=tf.int8)
            w = tf.transpose(w, perm=[1, 0])

            f.write(b.numpy().tobytes())
            f.write(w.numpy().tobytes())


@cmd.command()
@click.option('--config-path', type=click.Path(exists=True))
def export_parameter(config_path):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    d = get_parameters(config=config, model_dir=model_dir)
    np.savez_compressed(str(model_dir / 'parameters.npz'), **d)


def get_parameters(config, model_dir):
    model_config = config['model']
    network = get_model(config=model_config)
    # noinspection PyTypeChecker
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), network=network)
    manager = tf.train.CheckpointManager(ckpt, str(model_dir), max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()

    dummy = np.zeros([1, 38], dtype=np.int32)
    network([dummy, dummy])

    d = {}
    # transformer
    kernel = network.feature.kernel
    kernel_base = network.feature.kernel_base
    d['embedding/kernel'] = (
            tf.tile(kernel_base, multiples=[81, 1]) + kernel
    ).numpy()
    d['embedding/bias'] = network.feature.bias.numpy()

    d['dense1/kernel'] = network.affine1.kernel.numpy()
    d['dense1/bias'] = network.affine1.bias.numpy()

    d['dense2/kernel'] = network.affine2.kernel.numpy()
    d['dense2/bias'] = network.affine2.bias.numpy()

    d['dense3/kernel'] = network.affine3.kernel.numpy()
    d['dense3/bias'] = network.affine3.bias.numpy()

    return d


@cmd.command()
@click.option('--config-path', type=click.Path(exists=True))
@click.option('--output-dir', type=click.Path())
def export_binary(config_path, output_dir):
    config_path = Path(config_path)
    model_dir = config_path.parent
    with config_path.open('r') as f:
        config = toml.load(f)

    parameters = get_parameters(config=config, model_dir=model_dir)

    version = 0x7AF32F16
    total_hash = 0x24E85A0B
    feature_hash = 0x5D69D7B3
    network_hash = 0x79818DB8
    with (Path(output_dir) / 'nn.bin').open('wb') as f:
        f.write(struct.pack('I', version))
        f.write(struct.pack('I', total_hash))
        architecture = (
            'Features=HalfKP(Friend)[125388->256x2],'
            'Network=BinaryInnerProduct[1<-128](LeakyReLU[128]('
            'BinaryAffineTransform[128<-128](Binarization[128]('
            'BinaryAffineTransform[128<-512](InputSlice[512(0:512)])))))'
        )
        f.write(struct.pack('I', len(architecture)))
        f.write(architecture.encode('utf-8'))
        f.write(struct.pack('I', feature_hash))

        # Feature
        write_feature(parameters=parameters, f=f)

        f.write(struct.pack('I', network_hash))

        # network
        write_dense1(parameters=parameters, f=f)
        # f.write(struct.pack('I', 0xA5B6C7D0 + 512))
        write_dense2(parameters=parameters, f=f)
        # f.write(struct.pack('I', 0xA5B6C7D0 + 128))
        write_dense3(parameters=parameters, f=f)


def write_feature(parameters, f):
    # 128bitずつ [v0 v2] [v1 v3]
    # 256 / 2 / 16 = 8
    # 8要素を一つのまとまりとする
    kernel = np.reshape(parameters['embedding/kernel'], [-1, 32, 8])
    w = np.empty_like(kernel)
    for i in range(kernel.shape[0]):
        for j in range(kernel.shape[1] // 4):
            w[i, j * 4 + 0] = kernel[i, j * 4 + 0]
            w[i, j * 4 + 1] = kernel[i, j * 4 + 2]
            w[i, j * 4 + 2] = kernel[i, j * 4 + 1]
            w[i, j * 4 + 3] = kernel[i, j * 4 + 3]
    bias = np.reshape(parameters['embedding/bias'], [32, 8])
    b = np.empty_like(bias)
    for i in range(bias.shape[0] // 4):
        b[i * 4 + 0] = bias[i * 4 + 0]
        b[i * 4 + 1] = bias[i * 4 + 2]
        b[i * 4 + 2] = bias[i * 4 + 1]
        b[i * 4 + 3] = bias[i * 4 + 3]
    w = np.round(w * 2 ** 11).astype(np.int16)
    b = np.round(b * 2 ** 11).astype(np.int16)
    f.write(b.tobytes())
    f.write(w.tobytes())


def write_dense1(parameters, f):
    kernel = parameters['dense1/kernel']
    assert np.all(kernel.shape == (512, 128))

    tmp = bitarray(512 * 128, endian='little')
    for n, m, k, j, i in product(range(16), range(2), range(8), range(4),
                                 range(64)):
        tmp[n * 4096 + m * 2048 + k * 256 + j * 64 + i] = (
            1 if kernel[k * 64 + i, n * 8 + m + j * 2] < 0 else 0
        )

    # 10乗ではちょっとだけオーバーする
    r = 2 ** 9

    scale = np.mean(np.abs(parameters['dense1/kernel']), axis=0)
    s = np.round(scale * r).astype(np.uint16)
    assert np.all(s < 2 ** 7), np.max(s)
    # 必ず偶数なので1bitシフトできる
    assert np.max(s) * (256 // 2) < 2 ** 15
    assert len(s) == 128
    s = np.tile(np.reshape(s, [-1, 1]), [1, 2]).reshape([-1])

    b = np.round(parameters['dense1/bias'] * r * 2 ** 11 / 2).astype(np.int32)
    f.write(b.tobytes())
    # f.write(struct.pack('I', 0xA5B6C7D8))
    f.write(tmp.tobytes())
    f.write(s.tobytes())


def write_dense2(parameters, f):
    kernel = parameters['dense2/kernel']
    tmp = bitarray(128 * 128, endian='little')
    for q, p, o, n, m, k, j, i in product(range(16), range(2), range(2),
                                          range(4), range(2), range(2),
                                          range(4), range(4)):
        tmp[q * 1024 + p * 512 + o * 256 + n * 64 + m * 32 + k * 16 + j * 4
            + i] = (
            1 if kernel[o * 64 + m * 32 + k * 4 + j * 8 + i,
                        q * 8 + p + n * 2] < 0 else 0
        )

    r = 2 ** 11

    scale = np.mean(np.abs(parameters['dense2/kernel']), axis=0)
    s = np.round(scale * r).astype(np.uint16)
    assert np.all(s < 2 ** 9)
    # 必ず偶数なので1bitシフトできる
    assert np.max(s) * (64 // 2) < 2 ** 15
    s = np.tile(np.reshape(s, [-1, 1]), [1, 2]).reshape([-1])

    b = np.round(parameters['dense2/bias'] * r * 2 ** 9 / 2).astype(np.int32)

    f.write(b.tobytes())
    f.write(tmp.tobytes())
    f.write(s.tobytes())


def write_dense3(parameters, f):
    kernel = np.reshape(parameters['dense3/kernel'], [-1])
    assert len(kernel) == 128
    w = np.asarray([0xFFFFFFFF if k < 0 else 0 for k in kernel],
                   dtype=np.uint32)
    assert len(w) == 128
    # -1の個数
    # 2の補数で1を足す回数
    count = np.sum([1 if k < 0 else 0 for k in kernel])

    r = 2 ** 8 * 600

    scale = np.mean(np.abs(parameters['dense3/kernel']))
    s = np.round(scale * r).astype(np.uint32)
    # 途中でいくらかの桁を打ち消したので、残り3桁
    b = np.round(parameters['dense3/bias'] * r * 2 ** 3).astype(np.int32)
    b += count * s

    assert len(b) == 1
    # assert len(s) == 1
    assert isinstance(s, np.uint32)
    f.write(b.tobytes())
    f.write(w.tobytes())
    f.write(s.tobytes())


def main():
    cmd()


if __name__ == '__main__':
    main()
