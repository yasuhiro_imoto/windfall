#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
王手がかかっているかのテスト用にランダムに局面を生成する
"""

import random
import json
from pathlib import Path

import cshogi
from absl import flags, app

__author__ = 'Yasuhiro'
__date__ = '2020/07/18'

FLAGS = flags.FLAGS
flags.DEFINE_string('output_path', default=None, help='')
flags.DEFINE_integer('n_samples', default=100, help='')


def generate():
    positive_samples, negative_samples = [], []
    board = cshogi.Board()

    while True:
        legal_moves = list(board.legal_moves)
        if len(legal_moves) == 0:
            # reset
            board = cshogi.Board()
        else:
            if len(legal_moves) == 1:
                i = 0
            else:
                i = random.randrange(1, len(legal_moves))
            board.push(legal_moves[i])
            if board.is_check():
                positive_samples.append(board.sfen())
            else:
                negative_samples.append(board.sfen())

        if (len(positive_samples) >= FLAGS.n_samples // 2 and
                len(negative_samples) >= FLAGS.n_samples // 2):
            break

    if len(positive_samples) > FLAGS.n_samples // 2:
        positive_samples = random.sample(positive_samples,
                                         FLAGS.n_samples // 2)
    if len(negative_samples) > FLAGS.n_samples // 2:
        negative_samples = random.sample(negative_samples,
                                         FLAGS.n_samples // 2)
    return positive_samples, negative_samples


def main(_):
    positive_samples, negative_samples = generate()
    d = {'positive': positive_samples, 'negative': negative_samples}

    path = Path(FLAGS.output_path)
    if not path.parent.exists():
        path.parent.mkdir(parents=True)
    with path.open('w') as f:
        json.dump(d, f, indent=4)


if __name__ == '__main__':
    app.run(main)
