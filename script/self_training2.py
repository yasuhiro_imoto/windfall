#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path

import click
import cshogi
import joblib
import numpy as np
import tensorflow as tf
import xarray as xr
from tqdm import tqdm

try:
    import sys
    sys.path.append('../thirdparty/Ayane/source/shogi')
finally:
    import Ayane as ayane
from make_record2 import fv40
from nnue import NNUE

__author__ = 'Yasuhiro'
__date__ = '2019/10/06'


class Agent(object):
    def __init__(self, option, engine_path):
        usi = ayane.UsiEngine()
        usi.set_engine_options(option)
        usi.connect(engine_path)

        self.usi = usi

    def disconnect(self):
        self.usi.disconnect()

    def evaluate(self, sfen):
        self.usi.usi_position(sfen)
        self.usi.usi_go_and_wait_bestmove("time 0 byoyomi 1000")
        return self.usi.think_result


class LearningAgent(Agent):
    def __init__(self, option, engine_path):
        super().__init__(option=option, engine_path=engine_path)

    def evaluate(self, sfen):
        """
        multi pvの指し手と評価値を返す
        :param sfen:
        :return:
        """
        result = super().evaluate(sfen=sfen)
        move_list, eval_list = [], []
        for pv in result.pvs:
            move = pv.pv.split(' ')[0]
            move_list.append(move)

            eval_list.append(pv.eval)

        return move_list, np.asarray(eval_list)


class Worker(object):
    def __init__(self, option, engine_path):
        self.agent = Agent(option=option, engine_path=engine_path)

    def disconnect(self):
        self.agent.disconnect()

    def evaluate(self, sfen):
        result = self.agent.evaluate(sfen=sfen)
        return result.bestmove


class Environment(object):
    def __init__(self, color, env_id, worker, book_moves):
        self.env_id = env_id
        self.worker = worker
        self.book_moves = book_moves

        self.board = cshogi.Board()
        self.sfen = "startpos moves"
        # learning agentがどちらの手番(先手=0 , 後手=1)を持っているか
        self.turn = color

        # Q-learning agentが勝つと1、負けると-1、引き分けは0
        self.game_result = None

        self._initialize_state()

        self.records = []

    @property
    def state(self):
        return self.board

    def _update_sfen(self, move):
        self.sfen += ' {}'.format(move)

    def step(self, move):
        # stateとactionの組を記録
        state = np.empty(1, cshogi.PackedSfenValue)
        self.board.to_psfen(state)
        action = self.board.move_from_usi(move)
        self.records.append((np.squeeze(state['sfen']), action))

        if move == 'resign':
            self.game_result = -1
            return

        self.board.push_usi(move)
        if self.is_draw:
            self.game_result = 0
            return

        self._update_sfen(move)

        # 環境エージェントが手番を更新
        self._update_environment()

    @property
    def is_draw(self):
        return self.board.is_draw() == cshogi.REPETITION_DRAW

    @property
    def is_game_over(self):
        return self.game_result is not None

    def _update_environment(self):
        # workerの現局面に対する応答
        best_move = self.worker.evaluate(sfen=self.sfen)
        if best_move == 'resign':
            self.game_result = 1
            return

        # stateを更新
        self.board.push_usi(best_move)
        if self.is_draw:
            self.game_result = 0
            return

        self._update_sfen(best_move)

    def _initialize_state(self):
        # 定跡の範囲を自動で進める
        for i in range(self.book_moves):
            self._update_environment()

        if self.book_moves % 2 != self.turn:
            # 環境エージェントの手番
            self._update_environment()

    def reset(self):
        self.board.reset()

        self.sfen = "startpos moves"
        self.game_result = None
        # 先後を入れ替える
        self.turn ^= 1

        self._initialize_state()
        self.records.clear()

    def get_states(self, move_list, multi_pv):
        if len(move_list) == 1:
            # 行動がresignの場合にpush_usiが対応できないので事前に確認
            p_index_list = np.zeros([multi_pv, 38], dtype=np.int32)
            q_index_list = np.zeros_like(p_index_list)
            return p_index_list, q_index_list

        p_index_list = np.empty([multi_pv, 38], dtype=np.int32)
        q_index_list = np.empty_like(p_index_list)

        for i, move in enumerate(move_list):
            m = self.board.push_usi(move)

            p, q, p_index, q_index = fv40(self.board)
            if len(p_index) < 38:
                print(self.board)
            p_index_list[i] = p_index
            q_index_list[i] = q_index
            p_index_list[i] += p
            q_index_list[i] += q

            self.board.pop(m)
        p_index_list[len(move_list):] = 0
        q_index_list[len(move_list):] = 0

        return p_index_list, q_index_list

    def get_records(self):
        return self.records, self.game_result


class RandomNetwork(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.embedding = tf.keras.layers.Embedding(input_dim=125388,
                                                   output_dim=512)
        self.net = tf.keras.Sequential([
            tf.keras.layers.Dense(units=512, activation='softplus'),
            tf.keras.layers.Dense(units=512, activation='tanh'),
            tf.keras.layers.Dense(units=256)
        ])

    # noinspection PyMethodOverriding
    def call(self, p, q):
        emb_p = tf.reduce_mean(self.embedding(p), axis=1)
        emb_q = tf.reduce_mean(self.embedding(q), axis=1)

        h = tf.concat([emb_p, emb_q], axis=1)
        h = tf.nn.selu(h)
        y = self.net(h)
        return y


class BonusNetwork(tf.keras.Model):
    def __init__(self):
        super().__init__()
        self.prediction = RandomNetwork()
        self.target = RandomNetwork()

        self.target.trainable = False

    # noinspection PyMethodOverriding
    def call(self, inputs):
        p, q = tf.unstack(inputs, num=2, axis=0)
        prediction = self.prediction(p, q)
        target = self.target(p, q)

        loss = tf.keras.losses.logcosh(tf.stop_gradient(target), prediction)
        return loss


def compute_target_value(dataset, behavior_model, target_model,
                         ponanza_constant, bonus_network, elmo_lambda):
    """
    状態遷移後の最適な行動の評価を取得する
    学習の結果、とんでもない行動の価値が高くなっているかもしれないので、合法手全てについて調べる

    :param dataset: 一局分のデータ
    :param behavior_model:
    :param target_model:
    :param bonus_network:
    :param ponanza_constant:
    :param elmo_lambda:
    :return:
    """
    board = cshogi.Board()

    p_list, q_list = [], []
    p_index_list, q_index_list = [], []
    count_list = [0]
    count = 0

    if dataset.action[-1] == 0:
        # 最後は投了
        states = dataset.state[1:-1]
        resign_flag = True
    else:
        states = dataset.state[1:]
        resign_flag = False

    for i, state in enumerate(states):
        board.set_psfen(state)
        moves = board.legal_moves()
        # 合法手が残っている状況でも投了できるので、合法手の個数では判定できない

        for move in moves:
            m = board.push_usi(move)

            p, q, p_index, q_index = fv40(board=board)
            p_list.append(p)
            q_list.append(q)
            p_index_list.append(p_index)
            q_index_list.append(q_index)

            board.pop(m)

        count += len(moves)
        count_list.append(count)

    size = 1548
    p = np.asarray(p_list, dtype=np.int32)
    q = np.asarray(q_list, dtype=np.int32)
    p_value = np.asarray(p_index_list, dtype=np.int32)
    q_value = np.asarray(q_index_list, dtype=np.int32)
    p_value += np.reshape(p, [-1, 1]) * size
    q_value += np.reshape(q, [-1, 1]) * size

    values = tf.squeeze(behavior_model(p_value, q_value)).numpy()
    selected_action_list = [i + np.argmax(values[i:j])
                            for i, j in zip(count_list[:-1], count_list[1:])]
    selected_action_list = np.asarray(selected_action_list)

    p, q = p_value[selected_action_list], q_value[selected_action_list]
    # 手番が一つ進んでいるので、評価を反転
    target_values = -target_model(p, q)
    bonus = bonus_network(p, q)
    target = target_values / ponanza_constant + bonus
    target = tf.nn.sigmoid(target)
    if dataset.result == 1:
        # 勝ったときは全体が正しい行動なので価値を高くする
        target = elmo_lambda * 1 + (1 - elmo_lambda) * target

    if resign_flag:
        # 投了に対応するスコアを追加
        target = tf.concat([target, [0]], axis=0)
    return target, target_values


def get_state(dataset):
    board = cshogi.Board()

    p_list, q_list = [], []
    p_index_list, q_index_list = [], []
    for state, action in zip(dataset.state[:-1].values,
                             dataset.action[:-1].values):
        board.set_psfen(state)
        board.push_move16(action)

        p, q, p_index, q_index = fv40(board=board)
        p_list.append(p)
        q_list.append(q)
        p_index_list.append(p_index)
        q_index_list.append(q_index)
    p_list = np.asarray(p_list, dtype=np.int32).reshape([-1, 1])
    q_list = np.asarray(q_list, dtype=np.int32).reshape([-1, 1])
    p_index_list = np.asarray(p_index_list, dtype=np.int32)
    q_index_list = np.asarray(q_index_list, dtype=np.int32)

    p_value = p_index_list + p_list
    q_value = q_index_list + q_list

    return p_value, q_value


def make_dataset(dataset, batch_size, behavior_model, target_model,
                 ponanza_constant, bonus_network, elmo_lambda):
    # 順に並んでいることを利用する
    count_list = np.hstack(([0], np.cumsum(dataset.data_size)))

    p_value_list, q_value_list = [], []
    target_value_list, raw_value_list = [], []
    for i, j in zip(count_list[:-1], count_list[1:]):
        d = dataset.isel(sample_id=slice(i, j))
        input_states = get_state(dataset=d)
        target_values, raw_values = compute_target_value(
            dataset=d, behavior_model=behavior_model,
            target_model=target_model, ponanza_constant=ponanza_constant,
            bonus_network=bonus_network, elmo_lambda=elmo_lambda
        )

        p_value_list.append(input_states[0])
        q_value_list.append(input_states[1])
        target_value_list.append(target_values)
        raw_value_list.append(raw_values)

    p_value_list = np.vstack(p_value_list)
    q_value_list = np.vstack(q_value_list)
    target_value_list = tf.concat(target_value_list)
    raw_value_list = tf.concat(raw_value_list)

    ds = tf.data.Dataset.from_tensor_slices((
        (p_value_list, q_value_list), target_value_list, raw_value_list
    )).shuffle(batch_size * 100).batch(batch_size)

    return ds


@click.group()
def cmd():
    pass


@cmd.command()
@click.option('--data-path', type=click.Path(exists=True))
@click.option('--model-dir', type=click.Path())
@click.option('--batch-size', type=int, default=1000)
@click.option('--epochs', type=int, default=10)
@click.option('--ponanza-constant', type=float, default=600)
@click.option('--elmo-lambda', type=float, default=0.9)
def learn(data_path, model_dir, batch_size, epochs, ponanza_constant,
          elmo_lambda):
    behavior_nnue = NNUE()
    nnue_optimizer = tf.keras.optimizers.Adam()
    behavior_ckpt = tf.train.Checkpoint(step=tf.Variable(1), net=behavior_nnue,
                                        optimizer=nnue_optimizer)
    behavior_manager = tf.train.CheckpointManager(
        behavior_ckpt, str(model_dir / 'nnue' / 'behavior'), max_to_keep=3
    )
    behavior_ckpt.restore(behavior_manager.latest_checkpoint)

    target_nnue = NNUE()
    target_ckpt = tf.train.Checkpoint(step=tf.Variable(1), net=target_nnue)
    target_manager = tf.train.CheckpointManager(
        target_ckpt, str(model_dir / 'nnue' / 'target'), max_to_keep=3
    )
    target_ckpt.restore(target_manager.latest_checkpoint)

    bonus_network = BonusNetwork()
    bonus_optimizer = tf.keras.optimizers.Adam()
    bonus_ckpt = tf.train.Checkpoint(step=tf.Variable(1), net=bonus_network,
                                     optimizer=bonus_optimizer)
    bonus_manager = tf.train.CheckpointManager(
        bonus_ckpt, str(model_dir / 'bonus'), max_to_keep=3
    )
    bonus_ckpt.restore(bonus_manager.latest_checkpoint)

    dataset = joblib.load(data_path)
    ds = make_dataset(dataset=dataset, batch_size=batch_size,
                      behavior_model=behavior_nnue, target_model=target_nnue,
                      ponanza_constant=ponanza_constant,
                      bonus_network=bonus_network, elmo_lambda=elmo_lambda)

    criterion = tf.keras.losses.KLDivergence()

    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_accuracy = tf.keras.metrics.LogCoshError(name='train_accuracy')
    # test_loss = tf.keras.metrics.Mean(name='test_loss')
    # test_accuracy = tf.keras.metrics.LogCoshError(name='test_accuracy')

    bonus_loss = tf.keras.metrics.Mean(name='bonus_loss')

    @tf.function
    def train_step(x, y, raw):
        with tf.GradientTape() as tape:
            # 一つ手番が進んでいるので、評価を反転
            predictions = -behavior_nnue(x)
            q = tf.nn.sigmoid(predictions / ponanza_constant)
            loss = criterion(y, q)
        gradients = tape.gradient(loss, behavior_nnue.trainable_variables)
        nnue_optimizer.apply_gradients(zip(
            gradients, behavior_nnue.trainable_variables
        ))

        train_loss.update_state(loss)
        train_accuracy.update_state(y_true=raw, y_pred=predictions)

        with tf.GradientTape() as tape:
            p = bonus_network(x)
            loss = tf.reduce_mean(p)
        gradients = tape.gradient(loss, bonus_network.trainable_variables)
        bonus_optimizer.apply_gradients(zip(
            gradients, bonus_network.trainable_variables
        ))

        bonus_loss.update_state(loss)

    # @tf.function
    # def test_set(x, y, raw):
    #     # 一つ手番が進んでいるので、評価を反転
    #     predictions = -behavior_nnue(x)
    #     q = tf.nn.sigmoid(predictions / ponanza_constant)
    #     loss = criterion(y, q)
    #
    #     test_loss.update_state(loss)
    #     test_accuracy.update_state(y_true=raw, y_pred=predictions)

    current_time = datetime.now().strftime("%Y%m%d-%H%M%S")
    train_log_dir = 'logs/gradient_tape/' + current_time + '/train'
    # test_log_dir = 'logs/gradient_tape/' + current_time + '/test'
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    # test_summary_writer = tf.summary.create_file_writer(test_log_dir)

    for i in range(epochs):
        for data in tqdm(ds, total=len(dataset)):
            train_step(*data)

        with train_summary_writer.as_default():
            tf.summary.scalar('loss', train_loss.result(),
                              step=int(behavior_ckpt.step))
            tf.summary.scalar('accuracy', train_accuracy.result(),
                              step=int(behavior_ckpt.step))
            tf.summary.scalar('bonus/loss', bonus_loss.result(),
                              step=int(bonus_ckpt.step))

        train_loss.reset_states()
        train_accuracy.reset_states()

        bonus_loss.reset_states()

        behavior_ckpt.step.assign_add(1)
        behavior_manager.save()

        bonus_ckpt.step.assign_add(1)
        bonus_manager.save()

    # target networkを更新
    for target, behavior in zip(target_nnue.variables,
                                behavior_nnue.variables):
        target.assign(behavior)
    # target networkを保存
    target_ckpt.step.assign_add(1)
    target_manager.save()


@cmd.command()
@click.option('--log-dir', type=click.Path())
@click.option('--engine-path', type=click.Path(exists=True))
@click.option('--env-eval-dir', type=click.Path(exists=True))
@click.option('--agent-eval-dir', type=click.Path(exists=True))
@click.option('--hash-size', type=int, default=4096)
@click.option('--threads', type=int, default=4)
@click.option('--depth-limit', type=int, default=2)
@click.option('--multi-pv', type=int, default=16)
@click.option('--resign-value', type=int, default=99999)
@click.option('--n-episodes', type=int, default=1)
@click.option('--softmax-scale', type=float, default=10.0,
              help='行動をsoftmax policyで選ぶ際の逆温度')
@click.option('--book-moves', type=int, default=16)
@click.option('--n-environments', type=int, default=32)
@click.option('--ponanza-constant', type=float, default=600)
def collect(log_dir, engine_path, env_eval_dir, agent_eval_dir, hash_size,
            threads, depth_limit, multi_pv, resign_value, n_episodes,
            softmax_scale, book_moves, n_environments, ponanza_constant):
    log_dir = Path(log_dir)

    env_usi_option = {
        'Hash': str(hash_size),
        'Threads': str(threads),
        'NetworkDelay': '0',
        'NetworkDelay2': '0',
        'DepthLimit': str(depth_limit),
        'ResignValue': str(resign_value),
        # 'BookMoves': '0',
        'EvalDir': env_eval_dir
    }
    worker = Worker(option=env_usi_option, engine_path=engine_path)
    env_list = [Environment(color=i % 2, env_id=i, worker=worker,
                            book_moves=book_moves)
                for i in range(n_environments)]

    usi_option = {
        'Hash': str(hash_size),
        'Threads': str(threads),
        'NetworkDelay': '0',
        'NetworkDelay2': '0',
        'DepthLimit': 1,
        'ResignValue': str(resign_value),
        # 'BookMoves': '0',
        'EvalDir': agent_eval_dir,
        'MultiPV': str(multi_pv)
    }
    agent = LearningAgent(option=usi_option, engine_path=engine_path)

    bonus_network = BonusNetwork()
    ckpt = tf.train.Checkpoint(step=tf.Variable(1), net=bonus_network)
    manager = tf.train.CheckpointManager(ckpt, str(log_dir / 'bonus'),
                                         max_to_keep=1)
    ckpt.restore(manager.latest_checkpoint)
    if manager.latest_checkpoint:
        print('Restored from {}'.format(manager.latest_checkpoint))
    else:
        print('Initializing from scratch.')

    eval_buffer = np.zeros([n_environments, multi_pv], dtype=np.int32)
    eval_mask = np.zeros_like(eval_buffer, dtype=np.float32)
    loop_counter = np.zeros(n_environments, dtype=np.int32)

    game_id = 0
    state_list, action_list = [], []
    result_list = []
    id_list, count_list = [], []

    while True:
        # 複数の局面から収集
        p_state_list, q_state_list = [], []
        eval_mask[:] = 0
        move_lists = []
        for i, env in enumerate(env_list):
            sfen = env.sfen
            move_list, eval_list = agent.evaluate(sfen=sfen)
            p_states, q_states = env.get_states(move_list=move_list,
                                                multi_pv=multi_pv)
            p_state_list.append(p_states)
            q_state_list.append(q_states)

            n = len(eval_list)
            eval_buffer[i, :n] = eval_list
            eval_mask[i, :n] = 1

            move_lists.append(move_list)
        p_state_list = np.vstack(p_state_list)
        q_state_list = np.vstack(q_state_list)

        # 各局面の探索ボーナス
        bonus = bonus_network(p_state_list, q_state_list)
        bonus = tf.reshape(bonus, [-1, multi_pv])
        total_logits = (eval_buffer / ponanza_constant + bonus) * softmax_scale
        logits = eval_mask * total_logits + (1 - eval_mask) * 1e-9

        selected_index = tf.random.categorical(logits, num_samples=1)
        selected_index = tf.squeeze(selected_index).numpy()

        # 局面を進める
        for i, (env, move_list) in enumerate(zip(env_list, move_lists)):
            index = min(selected_index[i], len(move_list) - 1)
            move = move_list[index]
            env.step(move=move)

            if env.is_game_over:
                records, game_result = env.get_records()
                for state, action in records:
                    state_list.append(state)
                    action_list.append(action)
                n = len(records)
                id_list.extend([game_id] * n)

                result_list.append(game_result)
                count_list.append(n)

                game_id += 1

                loop_counter[i] += 1
                env.reset()

        if np.all(loop_counter >= n_episodes):
            break

    print('completed')
    states = np.asarray(state_list, dtype=np.uint8)
    actions = np.asarray(action_list, dtype=np.uint16)
    results = np.asarray(result_list, dtype=np.int8)
    id_list = np.asarray(id_list, dtype=np.uint32)
    count_list = np.asarray(count_list, dtype=np.uint16)
    ds = xr.Dataset(
        {'state': (('sample_id', 'element'), states),
         'action': ('sample_id', actions),
         'game_id': ('sample_id', id_list)},
        attrs={'data_size': count_list, 'result': results}
    )
    t = datetime.now()
    i = 0
    while True:
        path = log_dir / t.strftime('%Y%m%d-{}.pickle'.format(i))
        if path.exists():
            i += 1
            continue
        if not log_dir.exists():
            log_dir.mkdir(parents=True)

        joblib.dump(ds, path, compress=3)
        break
    print('saved')
    if not manager.latest_checkpoint:
        save_path = manager.save()
        print('Saved checkpoint: {}'.format(save_path))

    worker.disconnect()
    agent.disconnect()


@cmd.command()
@click.option('--weight-path', type=click.Path(exists=True, dir_okay=False))
@click.option('--save-dir', type=click.Path(file_okay=False))
def import_raw(weight_path, save_dir):
    weights = joblib.load(weight_path)

    sfen_list = [
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
        'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
        'RGgsn5p 1',
        'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
        'RGgsn5p 1'
    ]
    p_values = np.empty([len(sfen_list), 38], dtype=np.int32)
    q_values = np.empty_like(p_values)
    size = 1548
    for i, sfen in enumerate(sfen_list):
        board = cshogi.Board(sfen=sfen)
        p, q, fv38p, fv38q = fv40(board=board)
        p_values[i] = [size * p + v for v in fv38p]
        q_values[i] = [size * q + v for v in fv38q]

    model = NNUE()
    model((p_values, q_values))

    model.transformer.kernel.assign(weights['transformer']['weight'])
    model.transformer.bias.assign(weights['transformer']['bias'])
    model.network.layers[1].kernel.assign(weights['affine32x512']['weight'])
    model.network.layers[1].bias.assign(weights['affine32x512']['bias'])
    model.network.layers[4].kernel.assign(weights['affine32x32']['weight'])
    model.network.layers[4].bias.assign(weights['affine32x32']['bias'])
    model.network.layers[7].kernel.assign(weights['affine1x32']['weight'])
    model.network.layers[7].bias.assign(weights['affine1x32']['bias'])

    values = model((p_values, q_values))
    print(values)

    save_dir = Path(save_dir)
    for name in ('behavior', 'target'):
        ckpt = tf.train.Checkpoint(step=tf.Variable(1), net=model)
        manager = tf.train.CheckpointManager(
            ckpt, str(save_dir / 'nnue' / name), max_to_keep=1
        )
        save_path = manager.save()
        print("Saved checkpoint: {}".format(save_path))


def main():
    cmd()


if __name__ == '__main__':
    main()
