#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
google colabでひたすら対局を行ってそれに基づいてパラメータの更新を行う
"""

import re
from dataclasses import dataclass
from enum import IntEnum
from multiprocessing import Pool
from pathlib import Path

import cshogi
import numpy as np
import tensorflow as tf
from absl import app, flags
from cshogi.usi import Engine
from sklearn.utils import shuffle
from tqdm import tqdm

import nn_parameter
from nnue import NNUE
from nnue.feature_half_kp import fv38

__author__ = 'Yasuhiro'
__date__ = '2020/12/18'

FLAGS = flags.FLAGS
flags.DEFINE_integer('max_move', default=512, help='最大の手数', lower_bound=1)
flags.DEFINE_string('engine_path1', default='', help='')
flags.DEFINE_string('engine_path2', default='', help='')
flags.DEFINE_integer('gikou_level', default=1, help='')
flags.DEFINE_string('sfen_list', default='taya36.sfen', help='')
flags.DEFINE_integer('byoyomi', default=1000, help='')
flags.DEFINE_integer('depth_limit', default=2, help='やねうら王の探索深さ')

flags.DEFINE_string('model_dir', default='models',
                    help='TensorFlowのモデルの保存場所')
flags.DEFINE_float('lr', default=1e-3, help='学習係数')
flags.DEFINE_integer('threads', default=4, help='同時対局数')
flags.DEFINE_integer('seed', default=0, help='')
flags.DEFINE_integer('batch_size', default=10000, help='')
flags.DEFINE_integer('loop', default=10, help='学習ループの回数')


class GameResult(IntEnum):
    black_win = 0
    white_win = 1
    draw = 2


@dataclass
class InputFeature:
    p_feature: np.ndarray
    q_feature: np.ndarray
    sign: int


@dataclass
class InputPair:
    # 現在の読み筋の局面
    input1: InputFeature
    # 読んで欲しい局面
    input2: InputFeature


def count_moves(sfen: str):
    # startpos moves から始まっているものとする
    tokens = sfen.split(' ')
    return len(tokens) - 2


class PvListener(object):
    def __init__(self):
        self.r = re.compile(r'^info .*pv ((?:[1-9a-zA-Z*+]{4,5} ?)+).*$')
        self.line = ''

    def __call__(self, line):
        head = line[:4]
        if head == 'info':
            # 最後のinfoだけを記録しておけば十分なはず
            self.line = line

    def get_pvs(self):
        m = self.r.match(self.line)
        if m is None:
            return None
        else:
            pvs = m.group(1).strip().split(' ')
            return pvs

    def clear(self):
        self.line = ''


def run_match(black_engine, white_engine, cfg, sfen, board, target_engine,
              steps=None):
    """
    対局を行う
    どちらか一方が学習したい評価関数を使っているエンジン

    :param black_engine: 先手のエンジン
    :param white_engine: 後手のエンジン
    :param cfg:
    :param sfen: usi formatでの対局を開始する局面
                 平手の初期配置からの指し進めて局面を指定する
    :param board:
    :param target_engine: 学習したいエンジン
                          0なら先手、1なら後手
    :param steps: 対局開始の手数
                  sfenの手数より大きい場合はsfenに従って最後まで進めた局面かた開始する
    :return:
    """
    engines = (black_engine, white_engine)
    # 対局前に置換表などのクリアを実行
    for engine in engines:
        engine.isready()
        engine.usinewgame()

    board.reset()
    pvs = {}
    # engineに渡す指し手の系列
    usi_moves = sfen.split(' ')[2:]
    if steps is not None and steps < len(usi_moves):
        usi_moves = usi_moves[:steps]
    # 初手は0
    ply = len(usi_moves)
    for move in usi_moves:
        board.push_usi(move)
    turn = ply % 2
    if turn == 0:
        if target_engine == 0:
            start_step = ply
        else:
            start_step = ply + 1
    else:
        if target_engine == 0:
            start_step = ply + 1
        else:
            start_step = ply
    # start_step = ply + int(turn != target_engine)

    # infoからpv取得する
    pv_listener = PvListener()

    result = GameResult.draw
    while ply < cfg.max_move:
        engine = engines[turn]
        engine.position(usi_moves)
        # 探索深さでコントロールするので、時間は長めに設定
        best_move, ponder = engine.go(byoyomi=cfg.byoyomi,
                                      listener=pv_listener)

        if best_move == 'resign':
            result = GameResult(1 - turn)
            break
        elif best_move == 'win':
            result = GameResult(turn)
            break

        if target_engine == turn:
            pv = pv_listener.get_pvs()
            pvs[ply] = pv

        board.push_usi(best_move)
        usi_moves.append(best_move)

        turn ^= 1
        ply += 1
        pv_listener.clear()

        if board.is_draw() == cshogi.REPETITION_DRAW:
            break
    return usi_moves, result, pvs, start_step


def make_engine(engine_path, options):
    engine = Engine(engine_path, connect=False)
    engine.connect(listener=None)
    for key, value in options.items():
        engine.setoption(key, value)
    engine.isready(listener=None)

    engine.name = Path(engine_path).stem
    return engine


def make_yaneuraou_engine(engine_path, cfg):
    options = {"USI_Hash": 128, "Threads": 1, "NetworkDelay": 0,
               "NetworkDelay2": 0, "MaxMovesToDraw": cfg.max_move,
               "MinimumThinkingTime": 0, 'DepthLimit': cfg.depth_limit}
    return make_engine(engine_path=engine_path, options=options)


def make_gikou2_engine(engine_path, cfg):
    options = {"Threads": 1, 'DepthLimit': cfg.gikou_level}
    return make_engine(engine_path=engine_path, options=options)


def load_sfen_list(sfen_path):
    """
    対局を開始する局面週の読み込み

    :param sfen_path:
    :return:
    """
    with open(sfen_path, 'r') as f:
        sfen_list = f.readlines()
    # 改行コードを取り除く
    sfen_list = [sfen.strip() for sfen in sfen_list]
    return sfen_list


def make_input_feature(usi_moves, training_pair, board):
    """
    予測と実際の局面の組の学習データに変換する

    :param usi_moves: 対局の棋譜
    :param training_pair: 各手番での予測PVと正解PV
    :param board:
    :return:
    """
    board.reset()
    input_values = []
    for i, move in enumerate(usi_moves):
        board.push_usi(move)
        if i in training_pair:
            # iは自分の手番の手数
            # 予測PVと正解PV
            pv1, pv2 = training_pair[i]

            input_pair = InputPair(input1=compute_feature(board=board, pv=pv1),
                                   input2=compute_feature(board=board, pv=pv2))
            input_values.append(input_pair)
    return input_values


def compute_feature(board, pv):
    """
    PVの終端の局面を特徴のインデックスに変換する

    :param board: PVの直前の局面
    :param pv:
    :return:
    """
    moves = []
    for tmp in pv:
        m = board.push_usi(tmp)
        moves.append(m)

    p, q, p_index, q_index = fv38(board=board)

    p_index = np.asarray(p_index, dtype=np.int32) + p * 1548
    q_index = np.asarray(q_index, dtype=np.int32) + q * 1548
    # 手番に対応する符号を設定する
    sign = -1 if len(pv) % 2 == 0 else + 1

    input_data = InputFeature(p_feature=p_index, q_feature=q_index, sign=sign)

    for m in moves[::-1]:
        board.pop(m)

    return input_data


def compute_training_pair(yaneuraou, usi_moves, pvs, steps, cfg):
    """
    読み筋が異なった部分について対局中のPVと実際の相手の指し手を見た後のPVの組を作る

    :param yaneuraou:
    :param usi_moves: 対局の棋譜
    :param pvs: 対局中のPV
    :param steps: pvsに含まれる最初のPVの手数
    :param cfg:
    :return:
    """
    current_depth_limit = 1
    yaneuraou.setoption('DepthLimit', current_depth_limit)

    # 指し手をコピー
    tmp_moves = [m for m in usi_moves[:steps]]
    training_data = {}
    # infoからpv取得する
    pv_listener = PvListener()
    # 終局に近すぎる部分は、探索で詰みが見えるので学習の対象に含めない
    for i in range(steps, len(usi_moves) - cfg.depth_limit * 2, 2):
        usi_move = usi_moves[i]
        pv = pvs[i]

        if 'resign' in pv or 'win' in pv:
            # 十分に終局が近いのでこの寄付の利用を終了する
            break

        tmp_moves.extend(usi_moves[i:i + 2])
        if pv is None or pv[0] != usi_move:
            # こんなのは少数だと思うので学習対象から取り除くだけにする
            continue
        if (len(pv) >= 2 and i < len(usi_moves) - 1 and
                pv[1] != usi_moves[i + 1]):
            # 予測と違う

            if len(pv) == 2:
                # i手目での次の数手に関する予測と実際の結果
                training_data[i] = ((pv[1],), (usi_moves[i + 1],))
            else:
                if len(pv) - 2 != current_depth_limit:
                    current_depth_limit = len(pv) - 2
                    yaneuraou.setoption('DepthLimit', current_depth_limit)

                yaneuraou.position(tmp_moves)
                best_move, _ = yaneuraou.go(byoyomi=100, listener=pv_listener)

                pv2 = pv_listener.get_pvs()
                pv_listener.clear()
                if pv2 is not None:
                    if 'resign' in pv2 or 'win' in pv2:
                        # 十分に終局が近いのでこの寄付の利用を終了する
                        break
                    # pv[1:]とsfen[i + 1] + pv2の長さは違うかもしれないが、
                    # 探索が延長される条件は常に一定のはずなので、比較対象の組として適切
                    training_data[i] = (pv[1:], [usi_moves[i + 1]] + pv2)

    return training_data


def initialize_model(net, parameter_path):
    # ダミーデータを入れて変数を実体化させる
    tmp = np.random.randint(10, size=(1, 32))
    net((tmp, tmp))

    parameters = nn_parameter.read(input_path=parameter_path)

    net.embedding.kernel.assign(
        (parameters[0].kernel / 127.0).astype(np.float32)
    )
    net.embedding.bias.assign(
        (parameters[0].bias / 127.0).astype(np.float32)
    )

    net.affine1.kernel.assign(
        (parameters[1].kernel.reshape((32, 512)).T / 64.0).astype(np.float32)
    )
    net.affine1.bias.assign(
        (parameters[1].bias / (127.0 * 64.0)).astype(np.float32)
    )

    net.affine2.kernel.assign(
        (parameters[2].kernel.reshape((32, 32)).T / 64.0).astype(np.float32)
    )
    net.affine2.bias.assign(
        (parameters[2].bias / (127.0 * 64.0)).astype(np.float32)
    )

    net.affine3.kernel.assign(
        (parameters[3].kernel.reshape((1, 32)).T /
         (600.0 * 16.0 / 127.0)).astype(np.float32)
    )
    net.affine3.bias.assign(
        (parameters[3].bias / (600.0 * 16.0)).astype(np.float32)
    )


def extract_parameter(net):
    embedding = nn_parameter.Parameter()
    embedding.kernel = tf.cast(tf.math.round(net.embedding.kernel * 127.0),
                               dtype=tf.int16).numpy()
    embedding.bias = tf.cast(tf.math.round(net.embedding.bias * 127.0),
                             dtype=tf.int16).numpy()

    hidden1 = nn_parameter.Parameter()
    hidden1.kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(net.affine1.kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden1.bias = tf.cast(tf.round(net.affine1.bias * 127 * 64),
                           dtype=tf.int32).numpy()

    hidden2 = nn_parameter.Parameter()
    hidden2.kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(net.affine2.kernel, perm=[1, 0]) * 64,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden2.bias = tf.cast(tf.round(net.affine2.bias * 127 * 64),
                           dtype=tf.int32).numpy()

    hidden3 = nn_parameter.Parameter()
    hidden3.kernel = tf.cast(
        tf.round(
            tf.clip_by_value(
                tf.transpose(net.affine3.kernel, perm=[1, 0]) *
                600 * 16 / 127,
                clip_value_min=-128, clip_value_max=127
            )
        ),
        dtype=tf.int8
    ).numpy()
    hidden3.bias = tf.cast(tf.round(net.affine3.bias * 600 * 16),
                           dtype=tf.int32).numpy()

    parameters = [embedding, hidden1, hidden2, hidden3]
    return parameters


def learn_wrapper(args):
    return learn_single(*args)


def learn_single(sfen_list, thread_id, cfg):
    board = cshogi.Board()

    yaneuraou = make_yaneuraou_engine(engine_path=cfg.engine_path1, cfg=cfg)
    gikou2 = make_gikou2_engine(engine_path=cfg.engine_path2, cfg=cfg)

    input_feature_list = []
    win_count = 0
    draw_count = 0

    if thread_id == 0:
        sfen_list = tqdm(sfen_list)
    for sfen in sfen_list:
        random_steps = np.random.randint(low=12, high=30)
        for target_engine in range(2):
            if target_engine == 0:
                black, white = yaneuraou, gikou2
            else:
                black, white = gikou2, yaneuraou
            yaneuraou.setoption('DepthLimit', cfg.depth_limit)
            # 対局
            usi_moves, result, pvs, start_step = run_match(
                black_engine=black, white_engine=white, sfen=sfen,
                board=board, target_engine=target_engine, cfg=cfg,
                steps=random_steps
            )
            if result == GameResult.draw:
                draw_count += 1
            elif (target_engine == 0) == (result == GameResult.black_win):
                win_count += 1

            # 読みが外れた所をピックアップ
            training_pair = compute_training_pair(
                yaneuraou=yaneuraou, usi_moves=usi_moves, pvs=pvs,
                steps=start_step, cfg=cfg
            )
            # ピックアップした局面を特徴ベクトルに変換
            features = make_input_feature(
                usi_moves=usi_moves, training_pair=training_pair, board=board
            )
            input_feature_list.extend(features)

    yaneuraou.quit()
    gikou2.quit()

    return input_feature_list, (win_count, draw_count)


def evaluate_wrapper(args):
    return evaluate_single(*args)


def evaluate_single(sfen_list, thread_id, cfg):
    board = cshogi.Board()

    yaneuraou = make_yaneuraou_engine(engine_path=cfg.engine_path1, cfg=cfg)
    gikou2 = make_gikou2_engine(engine_path=cfg.engine_path2, cfg=cfg)

    win_count = 0
    draw_count = 0

    if thread_id == 0:
        sfen_list = tqdm(sfen_list)
    for sfen in sfen_list:
        for target_engine in range(2):
            if target_engine == 0:
                black, white = yaneuraou, gikou2
            else:
                black, white = gikou2, yaneuraou
            yaneuraou.setoption('DepthLimit', cfg.depth_limit)
            # 対局
            usi_moves, result, pvs, start_step = run_match(
                black_engine=black, white_engine=white, sfen=sfen,
                board=board, target_engine=target_engine, cfg=cfg
            )
            if result == GameResult.draw:
                draw_count += 1
            elif (target_engine == 0) == (result == GameResult.black_win):
                win_count += 1
    return win_count, draw_count


def evaluate_position(engine, moves, listener=None):
    """
    やねうら王エンジンで指定した局面の評価を行う

    :param engine: やねうら王エンジン
    :param moves: 初手からの指し手
    :param listener:
    :return:
    """
    if engine.debug:
        listener = print

    engine.position(moves)
    # evalコマンドの呼び出し
    cmd = 'eval'
    if listener:
        listener(cmd)
    engine.proc.stdin.write(cmd.encode('ascii') + b'\n')
    engine.proc.stdin.flush()

    # 結果を取得する
    s = 'eval = '
    while True:
        engine.proc.stdout.flush()
        line = engine.proc.stdout.readline().strip().decode('shift-jis')
        if listener:
            listener(line)
        if line[:len(s)] == s:
            value = int(line[len(s):])
            break
    return value


def make_dataset(input_features):
    """
    学習のためのtf.data.Datasetを作る

    :param input_features:
    :return:
    """
    input_features = shuffle(input_features, random_state=FLAGS.seed)

    def generator():
        for feature in input_features:
            student = feature.input1
            teacher = feature.input2

            yield (((student.p_feature, student.q_feature), (student.sign,)),
                   ((teacher.p_feature, teacher.q_feature), (teacher.sign,)))

    types = (tf.int32, tf.int32), tf.int32
    shapes = (tf.TensorShape([38]), tf.TensorShape([38])), tf.TensorShape([1])

    dataset = tf.data.Dataset.from_generator(
        generator=generator,
        output_types=(types, types),
        output_shapes=(shapes, shapes)
    ).batch(FLAGS.batch_size).prefetch(tf.data.experimental.AUTOTUNE)
    return dataset


class Configure(object):
    def __init__(self):
        keys = ('engine_path1', 'engine_path2', 'gikou_level', 'depth_limit',
                'max_move', 'byoyomi')
        for key in keys:
            setattr(self, key, getattr(FLAGS, key))


def main(_):
    model_dir = Path(FLAGS.model_dir)
    if not model_dir.exists():
        model_dir.mkdir(parents=True)

    net = NNUE()
    optimizer = tf.keras.optimizers.SGD(learning_rate=FLAGS.lr)
    ckpt = tf.train.Checkpoint(
        step=tf.Variable(1), network=net, optimizer=optimizer
    )
    manager = tf.train.CheckpointManager(ckpt, str(model_dir),
                                         max_to_keep=2)
    ckpt.restore(manager.latest_checkpoint).expect_partial()
    if not manager.latest_checkpoint:
        nn_path = Path(FLAGS.engine_path1).parent / 'eval/nn.bin'
        initialize_model(net=net, parameter_path=nn_path)

    metric = tf.keras.metrics.Mean()

    cfg = Configure()

    sfen_list = load_sfen_list(sfen_path=FLAGS.sfen_list)
    # 後ろの100個はテスト用に置いておく
    train_list = sfen_list[:-100]
    test_list = sfen_list[-100:]
    # スレッドの個数に訓練データを分割
    train_set = [(train_list[i::FLAGS.threads], i, cfg)
                 for i in range(FLAGS.threads)]
    test_set = [(test_list[i::FLAGS.threads], i, cfg)
                for i in range(FLAGS.threads)]

    evaluation_set = []
    for sfen in train_list[:16]:
        moves = sfen.split(' ')[2:]
        evaluation_set.append(moves)

    # rank学習のマージン
    margin = tf.constant(1e-4, dtype=tf.float32)

    yaneuraou_bin_path = Path(FLAGS.engine_path1).parent / 'eval/nn.bin'
    # 学習状況の進み具合の参考として局面を評価させる
    # noinspection PyTypeChecker
    yaneuraou = make_yaneuraou_engine(FLAGS.engine_path1, cfg=cfg)

    @tf.function
    def train_step(student, teacher):
        with tf.GradientTape() as tape:
            s_sign = tf.cast(student[1], dtype=tf.float32)
            t_sign = tf.cast(teacher[1], dtype=tf.float32)
            v_student = net(student[0], training=True) * s_sign
            v_teacher = net(teacher[0], training=True) * t_sign

            # studentの読み筋の評価値を大きくする
            # studentの局面が自分の手番：
            #   その局面が選択されるかは相手の行動による
            #   studentの局面の評価が高くなると相手は別の手を選択することになる
            # studentの局面が相手の手番：
            #   符号が反転しているので、評価を下げるように学習する
            #   その局面を選ぶのは自分なので、低くなると別の手を選ぶようになる
            # teacherについてはstudentと逆の動きになる
            losses1 = tf.nn.relu(v_teacher - v_student + margin)
            # 値の大小関係だけでは全てを0に近づけることでロスを小さくできるので、不十分
            # 二つの値を入れ替えるようにする
            losses2 = (
                tf.math.squared_difference(
                    v_teacher, tf.stop_gradient(v_student)
                ) +
                tf.math.squared_difference(
                    tf.stop_gradient(v_teacher), v_student
                )
            )
            losses = losses1 + losses2
            loss = tf.math.reduce_mean(losses)
        gradients = tape.gradient(loss, net.trainable_variables)
        optimizer.apply_gradients(zip(gradients, net.trainable_variables))

        metric.update_state(losses)

    for loop in range(FLAGS.loop):
        # 評価関数を更新
        # 直前のディレクトリ名を覚えていて無視するので、別のディレクトリを設定してやる
        yaneuraou.setoption('EvalDir', 'foo')
        yaneuraou.setoption('EvalDir', str(yaneuraou_bin_path.parent))
        yaneuraou.isready()

        # 参考のために平手局面の評価値
        print('Hirate: {}'.format(evaluate_position(yaneuraou, None)))
        # 他の局面も評価値を表示
        score_list = [evaluate_position(yaneuraou, moves=m)
                      for m in evaluation_set]
        n = 8
        for i in range((len(score_list) + n - 1) // n):
            tmp = score_list[i * n:(i + 1) * n]
            print('\t'.join(map(str, tmp)))

        total_win_count = 0
        total_draw_count = 0
        input_features = []
        with Pool(FLAGS.threads) as pool:
            for feature_list, counts in pool.imap_unordered(learn_wrapper,
                                                            train_set):
                total_win_count += counts[0]
                total_draw_count += counts[1]

                input_features.extend(feature_list)

        total = len(sfen_list)
        total_lose_count = total - total_draw_count - total_win_count
        print(
            '[train {:02d}/{:02d}] '
            'win/draw/lose: {}/{}/{} ({}/{}/{})'.format(
                loop + 1, FLAGS.loop,
                total_win_count, total_draw_count, total_lose_count,
                total_win_count / total, total_draw_count / total,
                total_lose_count / total
            )
        )

        n = ((len(input_features) + FLAGS.batch_size - 1) //
             FLAGS.batch_size)
        dataset = make_dataset(input_features=input_features)
        for s, t in tqdm(dataset, total=n, desc='update parameters'):
            train_step(student=s, teacher=t)

        print('mean loss: {}'.format(metric.result()))
        metric.reset_states()

        ckpt.step.assign_add(1)
        manager.save()

        parameters = extract_parameter(net=net)
        nn_parameter.write(output_path=yaneuraou_bin_path,
                           parameters=parameters)
        print('updated parameters: {}'.format(yaneuraou_bin_path))

        total_win_count = 0
        total_draw_count = 0
        with Pool(FLAGS.threads) as pool:
            for counts in pool.imap_unordered(evaluate_wrapper, test_set):
                total_win_count += counts[0]
                total_draw_count += counts[1]
        total = len(test_list)
        total_lose_count = total - total_win_count - total_draw_count
        print(
            '[test {:02d}/{:02d}] '
            'win/draw/lose: {}/{}/{} ({}/{}/{})'.format(
                loop + 1, FLAGS.loop,
                total_win_count, total_draw_count, total_lose_count,
                total_win_count / total, total_draw_count / total,
                total_lose_count / total
            )
        )


if __name__ == '__main__':
    app.run(main)
