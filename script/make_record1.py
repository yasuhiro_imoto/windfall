#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import namedtuple
from multiprocessing import Pool
from pathlib import Path

import click
import shogi
import tensorflow as tf
from tqdm import tqdm

__author__ = 'Yasuhiro'
__date__ = '2019/6月/19'

Data = namedtuple('Data', ['sfen', 'move', 'ply', 'score', 'result'])


def load_data(file_name):
    with open(file_name, 'r') as f:
        for line in f:
            sfen = line
            move = next(f)
            score = next(f)
            ply = next(f)
            result = next(f)
            next(f)

            data = (sfen, move, ply, score, result)

            yield data


def convert_data(data):
    data = Data._make(data)

    # 先頭の'sfen 'を取り除く
    sfen = data.sfen[5:]
    move = data.move.strip()[5:]

    board = shogi.Board(sfen=sfen)
    board.push_usi(usi=move)

    p, q, fv38p, fv38q = fv40(board=board)

    size = 1548
    p_index = [p * size + i for i in fv38p]
    q_index = [q * size + i for i in fv38q]

    example = tf.train.Example(features=tf.train.Features(feature={
        'p': tf.train.Feature(int64_list=tf.train.Int64List(value=p_index)),
        'q': tf.train.Feature(int64_list=tf.train.Int64List(value=q_index)),
        'score': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[-int(data.score.strip()[6:])]
        )),
        'ply': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[int(data.ply.strip()[4:])]
        )),
        'result': tf.train.Feature(int64_list=tf.train.Int64List(
            value=[-int(data.result.strip()[7:])]
        ))
    }))
    return example.SerializeToString()


SQUARES_R90 = [shogi.SQUARES_L90.index(s) for s in shogi.SQUARES]
BONA_PIECE_ZERO = 0
BonaPiece = [
    BONA_PIECE_ZERO + 1,  # //0//0+1
    20,  # //f_hand_pawn + 19,//19+1
    39,  # //e_hand_pawn + 19,//38+1
    44,  # //f_hand_lance + 5,//43+1
    49,  # //e_hand_lance + 5,//48+1
    54,  # //f_hand_knight + 5,//53+1
    59,  # //e_hand_knight + 5,//58+1
    64,  # //f_hand_silver + 5,//63+1
    69,  # //e_hand_silver + 5,//68+1
    74,  # //f_hand_gold + 5,//73+1
    79,  # //e_hand_gold + 5,//78+1
    82,  # //f_hand_bishop + 3,//81+1
    85,  # //e_hand_bishop + 3,//84+1
    88,  # //f_hand_rook + 3,//87+1
    90,  # //e_hand_rook + 3,//90
    # f_pawn = fe_hand_end,
    90 + 81,  # e_pawn = f_pawn + 81,
    90 + 81 * 2,  # f_lance = e_pawn + 81,
    90 + 81 * 3,  # e_lance = f_lance + 81,
    90 + 81 * 4,  # f_knight = e_lance + 81,
    90 + 81 * 5,  # e_knight = f_knight + 81,
    90 + 81 * 6,  # f_silver = e_knight + 81,
    90 + 81 * 7,  # e_silver = f_silver + 81,
    90 + 81 * 8,  # f_gold = e_silver + 81,
    90 + 81 * 9,  # e_gold = f_gold + 81,
    90 + 81 * 10,  # f_bishop = e_gold + 81,
    90 + 81 * 11,  # e_bishop = f_bishop + 81,
    90 + 81 * 12,  # f_horse = e_bishop + 81,
    90 + 81 * 13,  # e_horse = f_horse + 81,
    90 + 81 * 14,  # f_rook = e_horse + 81,
    90 + 81 * 15,  # e_rook = f_rook + 81,
    90 + 81 * 16,  # f_dragon = e_rook + 81,
    90 + 81 * 17,  # e_dragon = f_dragon + 81,
    90 + 81 * 18,  # fe_old_end = e_dragon + 81,
]


def fv40(board):
    p_index, q_index = [], []
    for square in shogi.SQUARES:
        piece = board.piece_at(square)
        # index = 0
        # index2 = 0
        if piece and piece.piece_type != 8:
            p_value, q_value = 12 + piece.color, 12 + 1 - piece.color
            if piece.piece_type < 7:
                index = p_value + piece.piece_type * 2
                index2 = q_value + piece.piece_type * 2
            elif piece.piece_type in {9, 10, 11, 12}:
                index = p_value + 5 * 2
                index2 = q_value + 5 * 2
            elif piece.piece_type == 7:
                index = p_value + 8 * 2
                index2 = q_value + 8 * 2
            elif piece.piece_type == 13:
                index = p_value + 7 * 2
                index2 = q_value + 7 * 2
            elif piece.piece_type == 14:
                index = p_value + 9 * 2
                index2 = q_value + 9 * 2
            else:
                raise ValueError(piece.piece_type)
            p_index.append(SQUARES_R90[square] + BonaPiece[index])
            q_index.append(80 - SQUARES_R90[square] + BonaPiece[index2])

    for color in shogi.COLORS:
        for piece_type, piece_count in board.pieces_in_hand[color].items():
            index = BonaPiece[color + (piece_type - 1) * 2]
            index2 = BonaPiece[1 - color + (piece_type - 1) * 2]
            for i in range(piece_count):
                p_index.append(index + i)
                q_index.append(index2 + i)

    p = SQUARES_R90[board.king_squares[0]]
    q = 80 - SQUARES_R90[board.king_squares[1]]
    if board.turn == shogi.BLACK:
        return p, q, p_index, q_index
    else:
        return q, p, q_index, p_index


@click.command()
@click.option('--input-path', type=click.Path(exists=True, dir_okay=False))
@click.option('--output-path', type=click.Path())
def cmd(input_path, output_path):
    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    options = tf.python_io.TFRecordOptions(
        tf.python_io.TFRecordCompressionType.GZIP
    )
    with Pool(20) as p:
        with tf.python_io.TFRecordWriter(str(output_path),
                                         options=options) as writer:
            for record in tqdm(p.imap_unordered(convert_data,
                                                load_data(input_path)),
                               total=100000000):
                writer.write(record)


def main():
    cmd()


if __name__ == '__main__':
    main()
