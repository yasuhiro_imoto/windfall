#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TanukiColiseumで対局のログから読み筋が外れた部分を抽出する
その局面を調べてパラメータを変更する場所を決定する参考にする
"""

from pathlib import Path
import json
import sqlite3

import numpy as np
import pandas as pd
import cshogi
from absl import app, flags

import nn_parameter
from nnue.feature_half_kp import fv38

__author__ = 'Yasuhiro'
__date__ = '2021/01/16'


FLAGS = flags.FLAGS
flags.DEFINE_string('db_path', default='', help='')


def load_db(db_path):
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()

    df = pd.read_sql('SELECT * FROM move', conn)
    result = pd.read_sql('SELECT * FROM game', conn)

    cur.close()
    conn.close()

    return df, result


def extract_lose(result, player=0):
    """
    負けた対局のgame_idを抽出する
    :param result:
    :param player: engine1なら0、engine2なら1
    :return:
    """
    if player == 0:
        # 先手番を持った
        black = result[np.logical_and(result['id'] % 2 == 1,
                                      result['winner'] == 1)]
        # 後手番を持った
        white = result[np.logical_and(result['id'] % 2 == 0,
                                      result['winner'] == 0)]
    else:
        black = result[np.logical_and(result['id'] % 2 == 0,
                                      result['winner'] == 1)]
        white = result[np.logical_and(result['id'] % 2 == 1,
                                      result['winner'] == 0)]
    return black['id'], white['id']


def get_misprediction(df, black_ids, white_ids):
    """
    読み筋が外れた局面の王の座標を取得する
    :param df:
    :param black_ids: 先手番で負けたgame_id
    :param white_ids: 後手番で負けたgame_id
    :return:
    """
    board = cshogi.Board()
    squares = []
    for i, ids in enumerate((black_ids, white_ids)):
        for game_id in ids:
            board.reset()

            tmp = df[df['game_id'] == game_id]
            for index, row in tmp.iterrows():
                if (row['book'] == 0 and row['play'] % 2 == i and
                        row['next'] != 'none' and index + 1 in tmp.index and
                        row['next'] != tmp.loc[index + 1]['best']):
                    # 読みを外した局面

                    # ちょっと無駄が多いが、王の座標を直接的に取り出す方法がない
                    p, q, _, _ = fv38(board=board)
                    # 先後で違いがあるか調べるために情報を一緒に保存
                    squares.append((i, p, q))
                board.push_usi(row['best'])

    squares = pd.DataFrame(squares, columns=('turn', 'p', 'q'))
    return squares


def get_lose_start_position(df, black_ids, white_ids):
    """
    対局の棋譜から序盤の指し手を拾う
    :param df:
    :param black_ids: 先手番で負けたgame_id
    :param white_ids: 後手番で負けたgame_id
    :return:
    """
    sfen_list = [[], []]
    for i, ids in enumerate((black_ids, white_ids)):
        for game_id in ids:
            sfen = 'startpos moves'
            tmp = df[df['game_id'] == game_id]
            for _, row in tmp.iterrows():
                if row['book'] == 0:
                    break
                sfen += ' {}'.format(row['best'])
            sfen_list[i].append(sfen)
    return sfen_list


def main(_):
    df, result = load_db(FLAGS.db_path)
    black_ids, white_ids = extract_lose(result=result)
    king_squares = get_misprediction(df=df, black_ids=black_ids,
                                     white_ids=white_ids)

    path = Path(FLAGS.db_path)
    king_squares.to_csv(path.parent / 'lose.csv')
    pass


if __name__ == '__main__':
    app.run(main)
