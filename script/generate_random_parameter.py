#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NNUE LITEのランダムなパラメータを生成する

オーバーフローが発生しない範囲にする
"""

import struct
from pathlib import Path

import msgpack
import numpy as np
from absl import app, flags

__author__ = 'Yasuhiro'
__date__ = '2020/10/25'

FLAGS = flags.FLAGS
flags.DEFINE_string('output_path', default='nn.mpac', help='')


def generate(output_path):
    output_path = Path(output_path)
    if not output_path.parent.exists():
        output_path.parent.mkdir(parents=True)

    with output_path.open('wb') as f:
        header = {b'author': b'Imoto Yasuhiro',
                  b'version': 'Fluke 1.0'.encode('utf-8'),
                  b'comment': b'NNUE LITE random parameter'}
        tmp = msgpack.packb(header)
        f.write(tmp)

        kernel = np.random.randint(-1000, 1000, 361 * 81 * 32, dtype=np.int16)
        bias = np.random.randint(-1000, 1000, 256, dtype=np.int16)
        feature = {b'kernel': kernel.tobytes(), b'bias': bias.tobytes()}
        tmp = msgpack.packb(feature, use_bin_type=False)
        f.write(tmp)

        kernel1 = np.random.randint(-100, 100, 512 * 32, dtype=np.int8)
        kernel2 = np.random.randint(-100, 100, 32 * 32, dtype=np.int8)
        kernel3 = np.random.randint(-100, 100, 32, dtype=np.int8)
        bias1 = np.random.randint(-100, 100, 32, dtype=np.int32)
        bias2 = np.random.randint(-100, 100, 32, dtype=np.int32)
        bias3 = np.random.randint(-100, 100, 1, dtype=np.int32)
        hidden1 = {b'kernel': kernel1.tobytes(),
                   b'bias': bias1.tobytes(),
                   b'previous': []}
        hidden2 = {b'kernel': kernel2.tobytes(),
                   b'bias': bias2.tobytes(),
                   b'previous': {b'previous': hidden1}}
        hidden3 = {b'kernel': kernel3.tobytes(),
                   b'bias': bias3.tobytes(),
                   b'previous': {b'previous': hidden2}}
        tmp = msgpack.packb(hidden3, use_bin_type=False)
        f.write(tmp)


def main(_):
    generate(FLAGS.output_path)


if __name__ == '__main__':
    app.run(main)
