#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
shogi-evalのnnueの特徴量と一致するか確認
"""

import unittest
import random
import sys
from pathlib import Path
from itertools import product

import numpy as np
import tensorflow as tf
import shogi
import cshogi

import make_record2
from nnue.nnue import FeatureTransformer
from convert_bin_to_mpac import load_parameter

try:
    sys.path.append('../thirdparty/shogi-eval')
    # noinspection PyUnresolvedReferences
    from evaltest_nnue import (fv40, bias1, weight1s, bias2, weight2,
                               bias3, weight3, bias4, weight4)
except ImportError:
    print('error')

__author__ = 'Yasuhiro'
__date__ = '2020/11/10'


class TestEval(unittest.TestCase):
    def test1(self):
        sfen = ('lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b ' 
                '- 1')

        p1, q1, p_index1, q_index1 = fv40(sfen)

        board = cshogi.Board(sfen)
        p2, q2, p_index2, q_index2 = make_record2.fv40(board)

        self.assertEqual(p1, p2)
        self.assertEqual(q1, q2)
        self.assertEqual(set(p_index1), set(p_index2))
        self.assertEqual(set(q_index1), set(q_index2))

    def test2(self):
        sfen = ('lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w ' 
                '- 1 ')

        p1, q1, p_index1, q_index1 = fv40(sfen)

        board = cshogi.Board(sfen)
        p2, q2, p_index2, q_index2 = make_record2.fv40(board)

        self.assertEqual(p1, p2)
        self.assertEqual(q1, q2)
        self.assertEqual(set(p_index1), set(p_index2))
        self.assertEqual(set(q_index1), set(q_index2))

    def test3(self):
        sfen = ('l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
                'RGgsn5p 1')

        p1, q1, p_index1, q_index1 = fv40(sfen)

        board = cshogi.Board(sfen)
        p2, q2, p_index2, q_index2 = make_record2.fv40(board)

        self.assertEqual(p1, p2)
        self.assertEqual(q1, q2)
        self.assertEqual(set(p_index1), set(p_index2))
        self.assertEqual(set(q_index1), set(q_index2))

    def test4(self):
        sfen = ('l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
                'RGgsn5p 1')

        p1, q1, p_index1, q_index1 = fv40(sfen)

        board = cshogi.Board(sfen)
        p2, q2, p_index2, q_index2 = make_record2.fv40(board)

        self.assertEqual(p1, p2)
        self.assertEqual(q1, q2)
        self.assertEqual(set(p_index1), set(p_index2))
        self.assertEqual(set(q_index1), set(q_index2))

    def test_embedding_parameter(self):
        d = load_parameter(Path('eval/nn.bin'))
        d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256]).T

        self.assertListEqual(list(d['feature_kernel'].shape),
                             list(weight1s.shape))
        self.assertListEqual(list(d['feature_bias'].shape),
                             list(bias1.shape))

        # テスト時間が長すぎる
        # for i, j in product(range(weight1s.shape[0]),
        #                     range(weight1s.shape[1])):
        #     self.assertEqual(d['feature_kernel'][i, j], weight1s[i, j])

        for i in range(bias1.shape[0]):
            # 全部の要素のテストは長いので、一部だけ確認
            self.assertEqual(d['feature_kernel'][i, i], weight1s[i, i])

            self.assertEqual(d['feature_bias'][i], bias1[i])

    def test_embedding_layer(self):
        d = load_parameter(Path('eval/nn.bin'))
        # embedding layerに渡す時は転置は要らない
        d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256])

        sfen_list = (
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
            'RGgsn5p 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
            'RGgsn5p 1'
        )
        p_array = np.empty((4, 38), dtype=np.int32)
        q_array = np.empty_like(p_array)

        result1 = np.empty((4, 512))
        for i, sfen in enumerate(sfen_list):
            p, q, p_index, q_index = fv40(sfen)

            p_array[i] = p_index
            q_array[i] = q_index
            p_array[i] += 1548 * p
            q_array[i] += 1548 * q

            tmp = np.zeros(125388)
            tmp[p_array[i]] = 1
            result1[i, :256] = bias1 + weight1s.dot(tmp)
            tmp[:] = 0
            tmp[q_array[i]] = 1
            result1[i, 256:] = bias1 + weight1s.dot(tmp)

        embedding_layer = FeatureTransformer(d)
        tmp_p = embedding_layer(p_array)
        tmp_q = embedding_layer(q_array)
        tmp = tf.concat((tmp_p, tmp_q), axis=-1)
        result2 = tmp.numpy()

        for i, j in product(range(4), range(512)):
            with self.subTest(i=i, j=j):
                self.assertEqual(result1[i, j], result2[i, j])

    def test_dense1(self):
        d = load_parameter(Path('eval/nn.bin'))
        # embedding layerに渡す時は転置は要らない
        d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256])
        d['hidden1_kernel'] = np.reshape(d['hidden1_kernel'], [32, 512]).T

        sfen_list = (
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
            'RGgsn5p 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
            'RGgsn5p 1'
        )
        p_array = np.empty((4, 38), dtype=np.int32)
        q_array = np.empty_like(p_array)

        result1 = np.empty((4, 32))
        for i, sfen in enumerate(sfen_list):
            p, q, p_index, q_index = fv40(sfen)

            p_array[i] = p_index
            q_array[i] = q_index
            p_array[i] += 1548 * p
            q_array[i] += 1548 * q

            tmp = np.zeros(125388)
            tmp[p_array[i]] = 1
            tmp_p = bias1 + weight1s.dot(tmp)
            tmp[:] = 0
            tmp[q_array[i]] = 1
            tmp_q = bias1 + weight1s.dot(tmp)
            tmp = np.hstack((tmp_p, tmp_q))
            tmp = np.clip(tmp, a_min=0, a_max=127)
            result1[i] = bias2 + weight2.dot(tmp)

        embedding_layer = FeatureTransformer(d)
        net = tf.keras.Sequential([
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden1_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden1_bias']
                )
            )
        ])
        tmp_p = embedding_layer(p_array)
        tmp_q = embedding_layer(q_array)
        tmp = tf.concat((tmp_p, tmp_q), axis=-1)
        tmp = net(tmp)
        result2 = tmp.numpy()

        for i, j in product(range(4), range(32)):
            with self.subTest(i=i, j=j):
                self.assertEqual(result1[i, j], result2[i, j])

    def test_dense2(self):
        d = load_parameter(Path('eval/nn.bin'))
        # embedding layerに渡す時は転置は要らない
        d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256])
        d['hidden1_kernel'] = np.reshape(d['hidden1_kernel'], [32, 512]).T
        d['hidden2_kernel'] = np.reshape(d['hidden2_kernel'], [32, 32]).T

        sfen_list = (
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
            'RGgsn5p 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
            'RGgsn5p 1'
        )
        p_array = np.empty((4, 38), dtype=np.int32)
        q_array = np.empty_like(p_array)

        result1 = np.empty((4, 32))
        for i, sfen in enumerate(sfen_list):
            p, q, p_index, q_index = fv40(sfen)

            p_array[i] = p_index
            q_array[i] = q_index
            p_array[i] += 1548 * p
            q_array[i] += 1548 * q

            tmp = np.zeros(125388)
            tmp[p_array[i]] = 1
            tmp_p = bias1 + weight1s.dot(tmp)
            tmp[:] = 0
            tmp[q_array[i]] = 1
            tmp_q = bias1 + weight1s.dot(tmp)
            tmp = np.hstack((tmp_p, tmp_q))
            tmp = np.clip(tmp, a_min=0, a_max=127)
            tmp = bias2 + weight2.dot(tmp)
            tmp = np.clip(tmp // 64, a_min=0, a_max=127)
            result1[i] = bias3 + weight3.dot(tmp)

        embedding_layer = FeatureTransformer(d)
        net = tf.keras.Sequential([
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden1_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden1_bias']
                )
            ),
            tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 64.0)),
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden2_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden2_bias']
                )
            )
        ])
        tmp_p = embedding_layer(p_array)
        tmp_q = embedding_layer(q_array)
        tmp = tf.concat((tmp_p, tmp_q), axis=-1)
        tmp = net(tmp)
        result2 = tmp.numpy()

        for i, j in product(range(4), range(32)):
            with self.subTest(i=i, j=j):
                self.assertEqual(result1[i, j], result2[i, j])

    def test_dense3(self):
        d = load_parameter(Path('eval/nn.bin'))
        # embedding layerに渡す時は転置は要らない
        d['feature_kernel'] = np.reshape(d['feature_kernel'], [125388, 256])
        d['hidden1_kernel'] = np.reshape(d['hidden1_kernel'], [32, 512]).T
        d['hidden2_kernel'] = np.reshape(d['hidden2_kernel'], [32, 32]).T
        d['hidden3_kernel'] = np.reshape(d['hidden3_kernel'], [1, 32]).T

        sfen_list = (
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1',
            'lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL w - 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL w '
            'RGgsn5p 1',
            'l6nl/5+P1gk/2np1S3/p1p4Pp/3P2Sp1/1PPb2P1P/P5GS1/R8/LN4bKL b '
            'RGgsn5p 1'
        )
        p_array = np.empty((4, 38), dtype=np.int32)
        q_array = np.empty_like(p_array)

        result1 = np.empty((4, 1))
        for i, sfen in enumerate(sfen_list):
            p, q, p_index, q_index = fv40(sfen)

            p_array[i] = p_index
            q_array[i] = q_index
            p_array[i] += 1548 * p
            q_array[i] += 1548 * q

            tmp = np.zeros(125388)
            tmp[p_array[i]] = 1
            tmp_p = bias1 + weight1s.dot(tmp)
            tmp[:] = 0
            tmp[q_array[i]] = 1
            tmp_q = bias1 + weight1s.dot(tmp)
            tmp = np.hstack((tmp_p, tmp_q))
            tmp = np.clip(tmp, a_min=0, a_max=127)
            tmp = bias2 + weight2.dot(tmp)
            tmp = np.clip(tmp // 64, a_min=0, a_max=127)
            tmp = bias3 + weight3.dot(tmp)
            tmp = np.clip(tmp // 64, a_min=0, a_max=127)
            result1[i] = (bias4 + weight4.dot(tmp)) // 16

        embedding_layer = FeatureTransformer(d)
        net = tf.keras.Sequential([
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden1_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden1_bias']
                )
            ),
            tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 64.0)),
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=32,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden2_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden2_bias']
                )
            ),
            tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 64.0)),
            tf.keras.layers.ReLU(max_value=127),
            tf.keras.layers.Dense(
                units=1,
                kernel_initializer=tf.keras.initializers.constant(
                    d['hidden3_kernel']
                ),
                bias_initializer=tf.keras.initializers.constant(
                    d['hidden3_bias']
                )
            ),
            tf.keras.layers.Lambda(lambda x: tf.math.floordiv(x, 16.0))
        ])
        tmp_p = embedding_layer(p_array)
        tmp_q = embedding_layer(q_array)
        tmp = tf.concat((tmp_p, tmp_q), axis=-1)
        tmp = net(tmp)
        result2 = tmp.numpy()

        for i, j in product(range(4), range(1)):
            with self.subTest(i=i, j=j):
                self.assertEqual(result1[i, j], result2[i, j])


if __name__ == '__main__':
    unittest.main()
