#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ランダムな摂動を重みに加えた時、性能に与える影響を評価する
"""

import json
from pathlib import Path

from absl import app, flags
import numpy as np
from tqdm import tqdm

import nn_parameter

__author__ = 'Yasuhiro'
__date__ = '2021/01/03'

FLAGS = flags.FLAGS
flags.DEFINE_integer('seed', default=0, help='')
flags.DEFINE_string('input_bin', default='', help='改造元のnn.bin')
flags.DEFINE_string('output_dir', default='', help='変更したパラメータの出力先')
flags.DEFINE_integer('n', default=5, help='')
flags.DEFINE_float('ratio', default=0.01, help='')


def make_mutation(data, min_value=-128, max_value=127):
    flat = data.flatten()
    tmp = np.empty_like(flat)
    for i in range(len(tmp)):
        if np.random.rand() < FLAGS.ratio:
            if np.random.rand() < 0.5:
                tmp[i] = max(flat[i] - 1, min_value)
            else:
                tmp[i] = min(flat[i] + 1, max_value)
        else:
            tmp[i] = flat[i]

    tmp = np.reshape(tmp, data.shape)
    return tmp


def main(_):
    parameters = nn_parameter.read(FLAGS.input_bin)

    min8, max8 = -128, 127
    min16, max16 = -2 ** 15, 2 ** 15 - 1
    min32, max32 = -2 ** 31, 2 ** 31 - 1

    output_dir = Path(FLAGS.output_dir)
    for i in tqdm(range(FLAGS.n)):
        np.random.seed(FLAGS.seed + i)

        new_parameters = [
            nn_parameter.Parameter(
                kernel=make_mutation(parameters[0].kernel,
                                     min_value=min16, max_value=max16),
                bias=make_mutation(parameters[0].bias,
                                   min_value=min16, max_value=max16)
            )
        ]
        for p in parameters[1:]:
            new_parameters.append(
                nn_parameter.Parameter(
                    kernel=make_mutation(p.kernel,
                                         min_value=min8, max_value=max8),
                    bias=make_mutation(p.bias,
                                       min_value=min32, max_value=max32)
                )
            )

        output_path = output_dir / str(i) / 'nn.bin'
        if not output_path.parent.exists():
            output_path.parent.mkdir(parents=True)
        nn_parameter.write(output_path=output_path, parameters=new_parameters)

    settings = {
        'seed': FLAGS.seed, 'input_bin': FLAGS.input_bin, 'ratio': FLAGS.ratio
    }
    setting_path = output_dir / 'setting.json'
    with setting_path.open('w') as f:
        json.dump(settings, f)


if __name__ == '__main__':
    app.run(main)
