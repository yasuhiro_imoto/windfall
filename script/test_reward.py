#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

import tensorflow as tf
import cshogi
import numpy as np
from scipy.special import logit

from training_in_gym1 import (EpisodeList, SingleEpisode, NNUESigmoid,
                              BonusNetwork)

__author__ = 'Yasuhiro'
__date__ = '2019/12/07'


class TestTargetValue(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # テスト用のデータの読み込み
        tmp = np.load('test_dataset.npz', allow_pickle=True)['arr_0']
        episodes = EpisodeList()
        for d in tmp:
            e = SingleEpisode()
            e.p = d['p']
            e.q = d['q']
            e.next_state = d['next_state']
            e.record = d['record']
            e.result = d['result']
            e.draw = d['draw']
            episodes.episodes.append(e)
        cls.episodes = episodes.episodes    # type: list[SingleEpisode]

    def test_terminal_value(self):
        self.assertEqual(SingleEpisode.result_value[0], 0)
        self.assertGreater(SingleEpisode.result_value[1], 0)
        self.assertLess(SingleEpisode.result_value[-1], 0)

    def test_last_target_value(self):
        behavior_network = NNUESigmoid()
        target_network = NNUESigmoid()
        bonus_network = BonusNetwork()

        config = {'ponanza_constant': 600, 'bonus': 5, 'softmax': 3}

        for i, e in enumerate(self.episodes):
            inputs, targets = e.make_pair(behavior_network=behavior_network,
                                          target_network=target_network,
                                          bonus_network=bonus_network,
                                          config=config)
            with self.subTest(i=i, e=e):
                if e.result == -1:
                    # 負け
                    # 比較は小数点以下が微妙に異なるので、条件を緩める
                    # noinspection PyTypeChecker
                    self.assertAlmostEqual(
                        np.round(targets[-1].numpy(), 3),
                        np.round(logit(0.01) * 600, 3),
                        places=4
                    )
                elif e.result == 1:
                    # 勝ち
                    # noinspection PyTypeChecker
                    self.assertAlmostEqual(
                        np.round(targets[-1].numpy(), 3),
                        np.round(logit(0.99) * 600, 3),
                        places=4
                    )
                else:
                    if e.draw:
                        # 千日手
                        # noinspection PyTypeChecker
                        self.assertAlmostEqual(
                            np.round(targets[-1].numpy(), 3),
                            np.round(0, 3),
                            places=4
                        )

    def test_target_value(self):
        # 目標値の計算を確認する
        behavior_network = NNUESigmoid()
        target_network = NNUESigmoid()
        bonus_network = BonusNetwork()

        config = {'ponanza_constant': 600, 'bonus': 5, 'softmax': 3}
        pc = tf.constant(float(config['ponanza_constant']))
        bs = tf.constant(float(config['bonus']))

        for i, e in enumerate(self.episodes):
            def _generator():
                for p, q in e.next_state[1:]:
                    yield p, q

            dataset = tf.data.Dataset.from_generator(
                _generator,
                output_types=(tf.int32, tf.int32),
                output_shapes=(tf.TensorShape([None, 38]),
                               tf.TensorShape([None, 38]))
            ).prefetch(tf.data.experimental.AUTOTUNE)

            p_list, q_list, bonus_list = [], [], []
            for inputs in dataset:
                (best_p, best_q), bonus = evaluate_behavior(
                    inputs=tf.convert_to_tensor(inputs),
                    behavior_network=behavior_network,
                    bonus_network=bonus_network, pc=pc, bs=bs
                )
                p_list.append(best_p)
                q_list.append(best_q)
                bonus_list.append(bonus)
            p_list = tf.convert_to_tensor(p_list)
            q_list = tf.convert_to_tensor(q_list)
            bonus_list = tf.convert_to_tensor(bonus_list)
            target, _ = target_network([p_list, q_list])
            target = tf.squeeze(-target)

            expected_values = target / pc + bonus_list * bs
            if e.result == 1 or e.result == -1 or (e.result == 0 and e.draw):
                final_value = SingleEpisode.result_value[e.result]
                expected_values = tf.concat(
                    [expected_values, [final_value]],
                    axis=0
                )

            _, actual_values = e.make_pair(
                behavior_network=behavior_network,
                target_network=target_network, bonus_network=bonus_network,
                config=config
            )

            self.assertEqual(len(actual_values), len(expected_values))
            for j, (actual, expected) in enumerate(zip(actual_values,
                                                       expected_values)):
                self.assertAlmostEqual(actual.numpy(), expected.numpy(),
                                       delta=5)


@tf.function(experimental_relax_shapes=True)
def evaluate_behavior(inputs, behavior_network, bonus_network, pc, bs):
    behavior, _ = behavior_network(inputs)
    behavior = tf.squeeze(-behavior)

    bonus = bonus_network(inputs)

    values = behavior / pc + bonus * bs

    index = int(tf.argmax(values))

    return (inputs[0][index], inputs[1][index]), bonus[index]


if __name__ == '__main__':
    unittest.main()
