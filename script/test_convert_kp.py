#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from unittest import TestCase

import cshogi
import numpy as np

from make_record2 import fv40
from convert_kp import make_state

__author__ = 'Yasuhiro'
__date__ = '2020/02/02'


class TestState(TestCase):
    def setUp(self) -> None:
        pass

    def test_feature(self):
        size = 1548

        board1 = cshogi.Board()
        count = 0
        while True:
            legal_moves = board1.legal_moves
            if len(legal_moves) == 0:
                break

            p, q, p_list, q_list = fv40(board1)
            p_list = np.asarray(p_list) + p * size
            q_list = np.asarray(q_list) + q * size
            if board1.turn == cshogi.WHITE:
                p_list, q_list = q_list, p_list

            board2 = make_state(p_list, q_list)

            for i in range(81):
                piece = board1.piece(i)
                piece_type = piece & 0xF
                if cshogi.PROM_PAWN <= piece_type <= cshogi.PROM_SILVER:
                    piece_color = piece >> 4
                    if piece_color == cshogi.BLACK:
                        self.assertEqual(cshogi.BGOLD, board2.piece(i))
                    else:
                        self.assertEqual(cshogi.WGOLD, board2.piece(i))
                else:
                    self.assertEqual(piece, board2.piece(i))

            for color in range(2):
                for piece_type in range(7):
                    self.assertEqual(board1.pieces_in_hand[color][piece_type],
                                     board2.pieces_in_hand[color][piece_type],
                                     msg=(color, piece_type, board1, board2))

            move = list(legal_moves)[np.random.choice(len(legal_moves))]
            board1.push(move)

            count += 1
            if count > 512:
                break


if __name__ == '__main__':
    unittest.main()
