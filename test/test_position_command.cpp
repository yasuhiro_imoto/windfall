#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../state_action/bitboard.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../usi.h"
#include "../utility.h"
#include "command.h"

using namespace usi;

namespace {
class TestPositionCommand : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    search::thread_pool.Set(1);
    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() {}

  state_action::Position position;
  state_action::StateListPtr states;
};

TEST_F(TestPositionCommand, ErrorCase1) {
  // 後手の王が消失している局面があったので、確認

  std::istringstream iss(
      "startpos moves 7g7f 3c3d 2g2f 2b8h+ 7i8h 3a2b 3i3h 6a5b 3g3f 1c1d 8h7g "
      "2b3c 4g4f 7c7d 4i4h 4c4d 2i3g 7d7e 7f7e B*6e 6i7h 6c6d 3h4g 6e5d 2f2e "
      "5b6c 7g6f 8b4b 9g9f 9c9d 5g5f 6d6e 6f7g 6c6d 5f5e 5d3b 5i6h 5a6a 3f3e "
      "3b4c 3e3d 4c3d 2e2d 2c2d 4h5g 6a7b 6h7i 3d1b 1g1f 7a6b 1f1e 1d1e 1i1e "
      "P*1d 1e1d P*1c P*2e 1c1d 2e2d P*2b 2h2f 6b6c 5g5h 7b8b 5h6h 6e6f 6g6f "
      "L*7a 4g5h 4b7b 4f4e 4d4e 9f9e 9d9e 2d2c+ 2b2c P*1c 2a1c 6f6e 6d6e P*6b "
      "7b6b 7g6f 6c7b 6f6e 6b6e 5e5d 3c4d B*2b 4d5e G*7f 5e4d 7f6e 9e9f 2b4d+ "
      "9f9g+ P*9b 9a9b 5d5c+ S*8h 7i6i 8a7c S*6b 8h9i 6b7a+ P*6a 7a7b");

  command::SetPosition(position, states, iss);

  EXPECT_EQ(position.types<Piece::OU>().PopCount(), 2);
}

TEST_F(TestPositionCommand, ErrorCase2) {
  // 何かよくわからないがアサーションで失敗したので、その局面を記録
  //
  // 終盤で詰みを読み切ったはずなのに詰みまでの手数が増えたり
  // 詰みが解除されたりと怪しい挙動があった

  std::istringstream iss(
      "position startpos moves 2g2f 4a3b 7g7f 8c8d 2f2e 8d8e 8h7g 3c3d 7i7h "
      "5c5d 2e2d 2c2d 2h2d 7a6b 2d2h P*2c 3i3h 6b5c 3g3f 5d5e 6g6f 7c7d 7h6g "
      "7d7e 7f7e 5a4a 6g7f 8b7b 4i5h 9c9d 5h6g 3a4b 3h3g 5c6d 6i7h 2b3c 3g4f "
      "4c4d 5i6h 4b4c 3f3e 6d7e 7f7e 7b7e P*7f 7e7b 6f6e 8a7c S*7d S*5d 3e3d "
      "4c3d 7d8c+ 7b7a 8c8b 4d4e 8b7a 6a7a 4f3g 7c6e R*7d P*7c 7d5d 3d4c P*3d "
      "6e7g+ 8i7g 3c2d 5d5c+ S*5b 5c5e P*3f 3g4h B*6d 5e6d 6c6d P*2b 4e4f "
      "2b2a+ R*4i S*3c 3f3g+ 3c3b 4c3b G*5i 4i5i+ 4h5i 3g2h R*3a 4a4b N*4d "
      "R*3h 6h7i 4b4c 4d5b+ 4f4g+ 3a3b+ 4c5d N*6f 5d4e B*5d 4e3e S*4d 3e4d "
      "3b4c 4d3e 4c4g S*3f");

  EXPECT_TRUE(true);
}

TEST_F(TestPositionCommand, ErrorCase3) {
  // アサーションは失敗していないが、詰みの挙動がおかしい
  //
  // 途中でmateが表示されたが、すぐに消えた
  // 最終盤で評価値が互いに0だが急にmate 5が出て、そのまま詰んだ

  std::istringstream iss(
      "position startpos moves 2g2f 3c3d 2f2e 2b3c 7g7f 3a2b 3i4h 4a3b 4g4f "
      "8c8d 8h3c+ 2b3c 7i7h 8d8e 7h7g 7a6b 3g3f 4c4d 2i3g 6a5b 6i7h 6c6d 4h4g "
      "6b6c 5i6h 5a4b 4g5f 6c5d 4i3h 7c7d 4f4e 4d4e 3g4e 3c4d P*4f 8a7c 2e2d "
      "2c2d 2h2d P*2c 2d2i 1c1d 9g9f 4b3a 3f3e 5d6e 5f6e 7c6e B*7c S*4g 3h3g "
      "8b7b 7c6d+ 7b6b 6d7c 4g4h 7c6b 5b6b P*2b 3a2b R*4a P*4b 3e3d 4h5g+ 6h7i "
      "B*5f S*3c 2a3c 3d3c+ 4d3c N*2e 3c2d P*3d 2d3c 3d3c+ 3b3c 2e3c+ 2b1b");

  EXPECT_TRUE(true);
}

TEST_F(TestPositionCommand, ErrorCase4) {
  // 何かよくわからないがアサーションで失敗したので、その局面を記録

  std::istringstream iss(
      "position startpos moves 2g2f 3c3d 7g7f 4c4d 4g4f 8b4b 2h4h 2a3c 3i3h "
      "9c9d 9g9f 3a3b 5i6h 3b4c 3h4g 5a6b 4g5f 5c5d 7i7h 6b7b 4i5h 4a3b 6g6f "
      "3d3e 6f6e 4b4a 6h7i 7a6b 4h3h 4c3d 3g3f 3e3f 3h3f P*3e 3f3h 1c1d 3h4h "
      "2b1c 8h6f 3e3f 4h3h 1c4f 3h3f 3d3e 3f4f 3e4f P*3d 3c4e 5f4e R*3i 4e4d "
      "4f5e 4d5e 4a4i+ S*4h 3i2i+ 5h5i 4i3h 3d3c+ N*7d 3c3b 5d5e 6f7e P*4g "
      "4h4g 3h4g B*2e 4g4d 2e6a+ 7b6a 3b4b B*6f G*5b 6a7a 5b6b");

  EXPECT_TRUE(true);
}

TEST_F(TestPositionCommand, ErrorCase5) {
  // アサーションで王手を放置か空き王をやらかしているらしいことが判明

  std::istringstream iss(
      "position startpos moves 2g2f 3c3d 7g7f 4a3b 2f2e 2b8h+ 7i8h 3a2b 2e2d "
      "2c2d 2h2d B*3e 2d2h 3e5g+ 6i5h 5g3e B*6h 3e4d 6h7g 4d5d 3i3h 8b5b 5i6h "
      "5a6b 6h7h 6b7b 4g4f 2a3c 5h4g 7b8b 3g3f 4c4d 4i5h 7a7b 9g9f 9c9d 2i3g "
      "5b5a 2h2f 5a2a 4f4e 3c4e 9f9e 9d9e P*9d 2b2c 3g4e 4d4e 3f3e N*4f 4g4f "
      "4e4f 9d9c+ 8a9c 9i9e 9c8e 9e9a+ 8e7g+ 8i7g P*9g P*9i 9g9h+ 9i9h 8b9a "
      "N*8e P*9b N*9d L*8a");

  EXPECT_TRUE(true);
}

TEST_F(TestPositionCommand, ErrorCase6) {
  // 75手目2h1hで飛をただで捨てている
  // 直後に捕られてしまうのを読めていない
  //
  // 捕られた局面を検討すると評価値が悪くなっているので、
  // どこかで枝刈りされていると思われる

  std::istringstream iss(
      "position startpos moves 7g7f 8c8d 2g2f 8d8e 8h7g 3c3d 7i7h 4a3b 2f2e "
      "2b7g+ 7h7g 3a2b 2e2d 2c2d 2h2d B*3e 2d2h 3e5g+ 6i7h 5g4g 4i5h 4g7d 5i6i "
      "P*2c 3i4h 7a6b 4h5g 6a5b 6i7i 9c9d 5g4f 6c6d 3g3f 6b6c 2i3g 6c5d 7i8h "
      "5a4b P*5e 5d6c 5h5g 4c4d 3f3e 3d3e B*1h 3b3c 4f3e P*3d 3e4f 1c1d 6g6f "
      "4b3b 1g1f 1d1e 7f7e 7d8d 1f1e 7c7d P*4e 1a1e 1h3f 1e1i+ 3g2e 3c2d P*3c "
      "2a3c 2e3c+ 3b3c 4e4d 7d7e 3f6c+ 5b6c P*2e 2d1e 2h1h 1i1h N*7d 6c7d 5e5d "
      "5c5d 4f4e 1e2e 4d4c+ 3c2d 4c4d 2d1e 4e3d 2e2d P*2e 2d3d 4d3d 1e2f 5g4g "
      "2f1g S*3g R*2i 3d2c 2b2c 2e2d 2c2d 4g3f L*2e P*2f P*3e 3f4f 2e2f 3g4h "
      "1g2h G*3i 2h2g 3i2i 1h2i R*2c 2g1h 2c2d+ S*1g 4f4g 2f2h+ 4g3g P*2f 3g2f "
      "1h1i S*2g 7e7f 4h3g 7f7g+ 7h7g P*7f 7g7f 8e8f 8g8f P*8g 8h7g P*7e 2d3e "
      "7e7f 7g6g N*5e 3e5e 5d5e 3g2h 1i2h 6g7f S*7e 7f7g S*7f 7g6h G*6g 6h5i "
      "G*5h");

  EXPECT_TRUE(true);
}
}  // namespace