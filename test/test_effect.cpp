#include "stdafx.h"

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "bitboard.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestEffect : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    InitializeBitboards();
    Position::Initialize();
  }

  static void TearDownTestCase() {}
};

TEST_F(TestEffect, BlackFu) {
  for (const Square source : SQUARE) {
    if (source.rank() == Rank::_1) {
      continue;
    }
    const Bitboard b = GetFuEffect(Color::BLACK, source);

    // 一つ前の升
    const Square s(source.file(), source.rank() - 1);
    const Bitboard effect(s);

    EXPECT_EQ(b, effect);
  }
}

TEST_F(TestEffect, WhiteFu) {
  for (const Square source : SQUARE) {
    if (source.rank() == Rank::_9) {
      continue;
    }
    const Bitboard b = GetFuEffect(Color::WHITE, source);

    // 一つ前の升
    const Square s(source.file(), source.rank() + 1);
    const Bitboard effect(s);

    EXPECT_EQ(b, effect);
  }
}

TEST_F(TestEffect, KaNoObstacles) {
  // 他に駒がない場合の角の利き

  for (const Square square : SQUARE) {
    const Bitboard b = GetKaEffect(square, Bitboard::Zero);

    Bitboard effect = Bitboard::Zero;
    for (const SquareWithWall delta :
         {SquareWithWall::RU, SquareWithWall::RD, SquareWithWall::LU,
          SquareWithWall::LD}) {
      SquareWithWall target(square);
      while (true) {
        target += delta;
        if (!target.ok()) {
          break;
        }

        effect |= Bitboard(static_cast<Square>(target));
      }
    }

    EXPECT_EQ(b, effect);
  }
}

TEST_F(TestEffect, HiNoObstacles) {
  // 他に駒がない場合の飛の利き

  for (const Square square : SQUARE) {
    const Bitboard b = GetHiEffect(square, Bitboard::Zero);

    Bitboard effect = Bitboard::Zero;
    for (const SquareWithWall delta : {SquareWithWall::U, SquareWithWall::D,
                                       SquareWithWall::R, SquareWithWall::L}) {
      SquareWithWall target(square);
      while (true) {
        target += delta;
        if (!target.ok()) {
          break;
        }

        effect |= Bitboard(static_cast<Square>(target));
      }
    }

    EXPECT_EQ(b, effect);
  }
}

// Square全体を一度にテストするとテストエクスプローラが重たくなるので分割する
class TestEffectSlice : public TestEffect,
                        public ::testing::WithParamInterface<int> {};
INSTANTIATE_TEST_SUITE_P(Slice, TestEffectSlice, ::testing::Range(0, 9));

template <typename T>
struct NoOp {
  constexpr T operator()(const T x, const T /*y*/) const { return x; }
};

template <typename OpFile, typename OpRank>
Bitboard MakeOccupied(const int i, const int n, const Square square,
                      const OpFile& opf, const OpRank& opr) {
  Bitboard occupied = Bitboard::Zero;
  for (int j = 0; j < n; ++j) {
    // 起点となるsquareの隣が0bit目
    if (i & (1 << j)) {
      occupied |=
          Bitboard(Square(opf(square.file(), static_cast<File::Value>(j + 1)),
                          opr(square.rank(), static_cast<Rank::Value>(j + 1))));
    }
  }
  return occupied;
}

template <SquareWithWall::Value delta1, SquareWithWall::Value delta2,
          SquareWithWall::Value delta3, typename OpFile, typename OpRank>
Bitboard ComputeEffect(const int i, const int n, const Square square,
                       const OpFile& opf, const OpRank& opr) {
  Bitboard effect = Bitboard::Zero;
  for (const SquareWithWall delta : {delta1, delta2, delta3}) {
    SquareWithWall target(square);
    while (true) {
      target += delta;
      if (!target.ok()) {
        break;
      }

      effect |= Bitboard(static_cast<Square>(target));
    }
  }

  for (int j = 0; j < n; ++j) {
    effect |=
        Bitboard(Square(opf(square.file(), static_cast<File::Value>(j + 1)),
                        opr(square.rank(), static_cast<Rank::Value>(j + 1))));
    if (i & (1 << j)) {
      break;
    }
  }

  return effect;
}

template <SquareWithWall::Value delta1, SquareWithWall::Value delta2,
          SquareWithWall::Value delta3, typename OpFile, typename OpRank>
void TestLongEffect(const int n, const Square square, const OpFile& opf,
                    const OpRank& opr,
                    Bitboard (*f_effect)(const Square, const Bitboard&)) {
  // 何もないパターンは別のテストでチェックしたのでスキップ
  for (int i = 1; i < (1 << n); ++i) {
    SCOPED_TRACE(boost::str(boost::format("square: %1%, i:%2%") % square % i));

    const Bitboard occupied = MakeOccupied(i, n, square, opf, opr);
    const Bitboard b = f_effect(square, occupied);

    const Bitboard effect =
        ComputeEffect<delta1, delta2, delta3>(i, n, square, opf, opr);

    EXPECT_EQ(b, effect);
  }
}

TEST_P(TestEffectSlice, KaOstaclesRU) {
  // 右上側に駒がある場合の角の利き

  const File file = static_cast<File::Value>(GetParam());
  if (file == File::_1) {
    // 右上はないのでスキップ
    EXPECT_TRUE(true);
    return;
  }
  for (Rank rank = Rank::_2; rank <= Rank::_9; ++rank) {
    const Square square(file, rank);

    const int n = std::min<int>(file, rank);
    TestLongEffect<SquareWithWall::RD, SquareWithWall::LU, SquareWithWall::LD>(
        n, square, std::minus<File>(), std::minus<Rank>(), GetKaEffect);
  }
}

TEST_P(TestEffectSlice, KaOstaclesRD) {
  // 右下側に駒がある場合の角の利き

  const File file = static_cast<File::Value>(GetParam());
  if (file == File::_1) {
    // 右下はないのでスキップ
    EXPECT_TRUE(true);
    return;
  }
  for (Rank rank = Rank::_1; rank <= Rank::_8; ++rank) {
    const Square square(file, rank);

    const int n = std::min<int>(file, Rank::_9 - rank);
    TestLongEffect<SquareWithWall::RU, SquareWithWall::LU, SquareWithWall::LD>(
        n, square, std::minus<File>(), std::plus<Rank>(), GetKaEffect);
  }
}

TEST_P(TestEffectSlice, KaOstaclesLU) {
  // 左上側に駒がある場合の角の利き

  const File file = static_cast<File::Value>(GetParam());
  if (file == File::_9) {
    // 左上はないのでスキップ
    EXPECT_TRUE(true);
    return;
  }
  for (Rank rank = Rank::_2; rank <= Rank::_9; ++rank) {
    const Square square(file, rank);

    const int n = std::min<int>(File::_9 - file, rank);
    TestLongEffect<SquareWithWall::RU, SquareWithWall::RD, SquareWithWall::LD>(
        n, square, std::plus<File>(), std::minus<Rank>(), GetKaEffect);
  }
}

TEST_P(TestEffectSlice, KaOstaclesLD) {
  // 左下側に駒がある場合の角の利き

  const File file = static_cast<File::Value>(GetParam());
  if (file == File::_9) {
    // 左下はないのでスキップ
    EXPECT_TRUE(true);
    return;
  }
  for (Rank rank = Rank::_1; rank <= Rank::_8; ++rank) {
    const Square square(file, rank);

    const int n = std::min<int>(File::_9 - file, Rank::_9 - rank);
    TestLongEffect<SquareWithWall::RU, SquareWithWall::RD, SquareWithWall::LU>(
        n, square, std::plus<File>(), std::plus<Rank>(), GetKaEffect);
  }
}

TEST_P(TestEffectSlice, HiObstaclesU) {
  // 上側に駒がある場合の飛の利き

  const File file = static_cast<File::Value>(GetParam());
  for (Rank rank = Rank::_2; rank <= Rank::_9; ++rank) {
    const Square square(file, rank);

    const int n = square.rank();
    TestLongEffect<SquareWithWall::D, SquareWithWall::R, SquareWithWall::L>(
        n, square, NoOp<File>(), std::minus<Rank>(), GetHiEffect);
  }
}

TEST_P(TestEffectSlice, HiObstaclesD) {
  // 下側に駒がある場合の飛の利き

  const File file = static_cast<File::Value>(GetParam());
  for (Rank rank = Rank::_1; rank <= Rank::_8; ++rank) {
    const Square square(file, rank);

    const int n = Rank::_9 - square.rank();
    TestLongEffect<SquareWithWall::U, SquareWithWall::R, SquareWithWall::L>(
        n, square, NoOp<File>(), std::plus<Rank>(), GetHiEffect);
  }
}

TEST_P(TestEffectSlice, HiObstaclesR) {
  // 右側に駒がある場合の飛の利き

  const Rank rank = static_cast<Rank::Value>(GetParam());
  for (File file = File::_2; file <= File::_9; ++file) {
    const Square square(file, rank);

    const int n = square.file();
    TestLongEffect<SquareWithWall::U, SquareWithWall::D, SquareWithWall::L>(
        n, square, std::minus<File>(), NoOp<Rank>(), GetHiEffect);
  }
}

TEST_P(TestEffectSlice, HiObstaclesL) {
  // 左側に駒がある場合の飛の利き

  const Rank rank = static_cast<Rank::Value>(GetParam());
  for (File file = File::_1; file <= File::_8; ++file) {
    const Square square(file, rank);

    const int n = File::_9 - square.file();
    TestLongEffect<SquareWithWall::U, SquareWithWall::D, SquareWithWall::R>(
        n, square, std::plus<File>(), NoOp<Rank>(), GetHiEffect);
  }
}

TEST_P(TestEffectSlice, BlackKy) {
  const File file = static_cast<File::Value>(GetParam());
  for (Rank rank = Rank::_2; rank <= Rank::_9; ++rank) {
    const Square square(file, rank);

    const int n = square.rank();
    // 何もないパターンは別のテストでチェックしたのでスキップ
    for (int i = 1; i < (1 << n); ++i) {
      SCOPED_TRACE(
          boost::str(boost::format("square: %1%, i:%2%") % square % i));

      const Bitboard occupied =
          MakeOccupied(i, n, square, NoOp<File>(), std::minus<Rank>());
      const Bitboard b = GetKyEffect(Color::BLACK, square, occupied);

      Bitboard effect = Bitboard::Zero;
      for (int j = 0; j < n; ++j) {
        effect |= Bitboard(Square(square.file(), square.rank() - j - 1));
        if (i & (1 << j)) {
          break;
        }
      }

      EXPECT_EQ(b, effect);
    }
  }
}

TEST_P(TestEffectSlice, WhiteKy) {
  const File file = static_cast<File::Value>(GetParam());
  for (Rank rank = Rank::_1; rank <= Rank::_8; ++rank) {
    const Square square(file, rank);

    const int n = Rank::_9 - square.rank();
    // 何もないパターンは別のテストでチェックしたのでスキップ
    for (int i = 1; i < (1 << n); ++i) {
      SCOPED_TRACE(
          boost::str(boost::format("square: %1%, i:%2%") % square % i));

      const Bitboard occupied =
          MakeOccupied(i, n, square, NoOp<File>(), std::plus<Rank>());
      const Bitboard b = GetKyEffect(Color::WHITE, square, occupied);

      Bitboard effect = Bitboard::Zero;
      for (int j = 0; j < n; ++j) {
        effect |= Bitboard(Square(square.file(), square.rank() + j + 1));
        if (i & (1 << j)) {
          break;
        }
      }

      EXPECT_EQ(b, effect);
    }
  }
}

}  // namespace