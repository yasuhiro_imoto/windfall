#pragma once
#ifndef STDAFX_H_INCLUDED
#define STDAFX_H_INCLUDED

#ifdef _MSC_VER
#define NOMINMAX
#endif  // _MSC_VER

#include <emmintrin.h>
#include <immintrin.h>

#include <array>
#include <climits>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <queue>
#include <random>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#pragma warning(push)
#pragma warning(disable : 26451 26495 26812)
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/operators.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/range/algorithm/find.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm/lexicographical_compare.hpp>
#include <boost/range/algorithm/transform.hpp>
#include <boost/scope_exit.hpp>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable : 4566 26812 26451 26439 28020 26495 6387 28182)
#include <gtest/gtest.h>
#define SPDLOG_EOL "\n"
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/ostream_sink.h>
#include <spdlog/spdlog.h>
#include <msgpack.hpp>
#include <nlohmann/json.hpp>
#pragma warning(pop)
#endif  // !STDAFX_H_INCLUDED
