#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../state_action/bitboard.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../usi.h"
#include "../utility.h"
#include "history.h"
#include "move_picker.h"
#include "pick_state.h"
#include "search_stack.h"
#include "thread.h"

using namespace search;

namespace {
class TestMovePicker : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<MainThread>(0); }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<Thread> thread;

  std::array<Stack, 5> stack;
};

/**
 * @brief MovePickerの内部状態を確認するためのクラス
 */
class MovePickerTester : public MovePickerNormal {
  // friend宣言でも直接はアクセスできない
  // friend class TestMovePicker;

 public:
  // コンストラクタをそのまま使う
  using MovePickerNormal::MovePickerNormal;

  State GetState() const { return state_; }
};

TEST_F(TestMovePicker, TestNormalInitialization) {
  // 通常探索でMovePickerを初期化したときの状態の確認

  position.SetHirate(&rb, thread.get());

  const PieceTargetHistory* continuation_history[4] = {
      stack[3].continuation_history, stack[2].continuation_history, nullptr,
      stack[0].continuation_history};
  const state_action::Move tt_move = state_action::Move::NONE;
  const state_action::Move counter_move = state_action::Move::NONE;

  MovePickerTester mp(position, tt_move, Depth::ONE, &thread->main_history,
                      &thread->capture_history, continuation_history,
                      counter_move, stack[4].killlers);

  EXPECT_EQ(mp.GetState(), State::CAPTURE_INITIAL);
}
}  // namespace
