#include "stdafx.h"

#include "evaluation_list.h"

using namespace evaluation;

namespace {
TEST(TestEvaluationList, CaseBlack1) {
  // BonaPieceの定義をいじくったので、正しく変換できているか確認
  // 先手

  EvaluationList list;

  list.Update(static_cast<PieceNumber>(0), Color::BLACK, Piece::FU, 0);
  const auto ebp = list.bona_piece(static_cast<PieceNumber>(0));

  EXPECT_EQ(ebp.fb, F_HAND_FU);
  EXPECT_EQ(ebp.fw, E_HAND_FU);
}

TEST(TestEvaluationList, CaseWhite1) {
  // BonaPieceの定義をいじくったので、正しく変換できているか確認
  // 後手

  EvaluationList list;

  list.Update(static_cast<PieceNumber>(0), Color::WHITE, Piece::FU, 0);
  const auto ebp = list.bona_piece(static_cast<PieceNumber>(0));

  EXPECT_EQ(ebp.fb, E_HAND_FU);
  EXPECT_EQ(ebp.fw, F_HAND_FU);
}

TEST(TestEvaluationList, CaseBlackAll) {
  // 先手の持ち駒をBonaPieceから取り出せるか確認

  EvaluationList list;

  // 全て先手番で持ち駒を登録
  constexpr std::array<int, 7> piece_count = { 18, 4, 4, 4, 2, 2, 4 };
  int no = 0;
  for (int i = 0; i < 7; ++i) {
    const Piece piece(Color::BLACK, static_cast<Piece::Value>(i + 1));
    for (int j = 0; j < piece_count[i]; ++j) {
      list.Update(static_cast<PieceNumber>(no), Color::BLACK, piece.type(), j);
      ++no;
    }
  }

  constexpr std::array<PieceNumber, 7> pn_list = {
      PieceNumber::FU, PieceNumber::KY, PieceNumber::KE, PieceNumber::GI,
      PieceNumber::KA, PieceNumber::HI, PieceNumber::KI};
  for (int i = 0; i < 7; ++i) {
    SCOPED_TRACE(i);
    const auto bp = kpp_hand_index[Color::BLACK][i + 1].fb;
    for (int j = 0; j < piece_count[i]; ++j) {
      SCOPED_TRACE(j);

      // BonaPieceからPieceNumberへの変換を確認
      EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(bp + j)),
                static_cast<PieceNumber>(pn_list[i] + j));

      // PieceNumberから BonaPieceへの変換を確認
      EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(pn_list[i] + j)).fb,
                static_cast<BonaPiece>(bp + j));
    }
  }
}

TEST(TestEvaluationList, CaseWhiteAll) {
  // 後手の持ち駒をBonaPieceから取り出せるか確認

  EvaluationList list;

  // 全て後手番で持ち駒を登録
  constexpr std::array<int, 7> piece_count = {18, 4, 4, 4, 2, 2, 4};
  int no = 0;
  for (int i = 0; i < 7; ++i) {
    const Piece piece(Color::WHITE, static_cast<Piece::Value>(i + 1));
    for (int j = 0; j < piece_count[i]; ++j) {
      list.Update(static_cast<PieceNumber>(no), Color::WHITE, piece.type(), j);
      ++no;
    }
  }

  constexpr std::array<PieceNumber, 7> pn_list = {
      PieceNumber::FU, PieceNumber::KY, PieceNumber::KE, PieceNumber::GI,
      PieceNumber::KA, PieceNumber::HI, PieceNumber::KI};
  for (int i = 0; i < 7; ++i) {
    SCOPED_TRACE(i);
    // GetPieceNumberHandは先手から見たBonaPiece
    const auto bp = kpp_hand_index[Color::WHITE][i + 1].fb;
    for (int j = 0; j < piece_count[i]; ++j) {
      SCOPED_TRACE(j);

      // BonaPieceからPieceNumberへの変換を確認
      EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(bp + j)),
                static_cast<PieceNumber>(pn_list[i] + j));

      // PieceNumberから BonaPieceへの変換を確認
      EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(pn_list[i] + j)).fb,
                static_cast<BonaPiece>(bp + j));
    }
  }
}

TEST(TestEvaluationList, BorderFU) {
  // 持ち駒を管理する配列で間違ってインデックスの上書きがないか確認
  // 歩の前後でインデックスの重複がないか確認

  EvaluationList list;
  // 全部で歩が36枚あるが、問題ない
  for (int i = 0; i < 18; ++i) {
    list.Update(static_cast<PieceNumber>(2 * i + 0), Color::BLACK, Piece::FU,
                i);
    list.Update(static_cast<PieceNumber>(2 * i + 1), Color::WHITE, Piece::FU,
                i);
  }
  // 歩の次は香
  list.Update(static_cast<PieceNumber>(38), Color::BLACK, Piece::KY, 0);

  for (int i = 0; i < 18; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    // 先手の駒
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(F_HAND_FU + i)),
              static_cast<PieceNumber>(2 * i + 0));
    // 後手の駒
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(E_HAND_FU + i)),
              static_cast<PieceNumber>(2 * i + 1));

    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(2 * i + 0)).fb,
              static_cast<BonaPiece>(F_HAND_FU + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(2 * i + 1)).fb,
              static_cast<BonaPiece>(E_HAND_FU + i));
  }
  EXPECT_EQ(list.GetPieceNumberHand(F_HAND_KY), static_cast<PieceNumber>(38));
  EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(38)).fb, F_HAND_KY);
}

TEST(TestEvaluationList, BorderKY) {
  // 持ち駒を管理する配列で間違ってインデックスの上書きがないか確認
  // 香の前後でインデックスの重複がないか確認

  EvaluationList list;

  for (int i = 0; i < 4; ++i) {
    list.Update(static_cast<PieceNumber>(PieceNumber::KY + i), Color::BLACK,
                Piece::KY, i);
    // 前後を埋める必要があるので、わかりやすくなるように空いている番号を利用する
    list.Update(static_cast<PieceNumber>(PieceNumber::GI + i), Color::WHITE,
                Piece::KY, i);
  }
  // 先手の香の前後に登録
  for (int i = 0; i < 18; ++i) {
    list.Update(static_cast<PieceNumber>(i), Color::WHITE, Piece::FU, i);
  }
  for (int i = 0; i < 4; ++i) {
    list.Update(static_cast<PieceNumber>(PieceNumber::KE + i), Color::BLACK,
                Piece::KE, i);
  }
  // 後手の香の前後に登録
  for (int i = 0; i < 2; ++i) {
    list.Update(static_cast<PieceNumber>(PieceNumber::HI + i), Color::BLACK,
                Piece::HI, i);
  }
  for (int i = 0; i < 4; ++i) {
    list.Update(static_cast<PieceNumber>(PieceNumber::KI + i), Color::WHITE,
                Piece::KE, i);
  }

  // 先手の香の前
  for (int i = 0; i < 18; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(E_HAND_FU + i)),
              static_cast<PieceNumber>(i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(i)).fb,
              static_cast<BonaPiece>(E_HAND_FU + i));
  }
  // 先手の香の後
  for (int i = 0; i < 4; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(F_HAND_KE + i)),
      static_cast<PieceNumber>(PieceNumber::KE + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(PieceNumber::KE + i)).fb,
      static_cast<BonaPiece>(F_HAND_KE + i));
  }

  // 後手の香の前
  for (int i = 0; i < 2; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(F_HAND_HI + i)),
      static_cast<PieceNumber>(PieceNumber::HI + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(PieceNumber::HI + i)).fb,
      static_cast<BonaPiece>(F_HAND_HI + i));
  }
  // 後手の香の後
  for (int i = 0; i < 4; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(E_HAND_KE + i)),
      static_cast<PieceNumber>(PieceNumber::KI + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(PieceNumber::KI + i)).fb,
      static_cast<BonaPiece>(E_HAND_KE + i));
  }

  // 先手の香
  for (int i = 0; i < 4; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(F_HAND_KY + i)),
              static_cast<PieceNumber>(PieceNumber::KY + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(PieceNumber::KY + i)).fb,
              static_cast<BonaPiece>(F_HAND_KY + i));
  }
  // 後手の香
  for (int i = 0; i < 4; ++i) {
    // GetPieceNumberHandの引数は先手側から見たBonaPiece
    EXPECT_EQ(list.GetPieceNumberHand(static_cast<BonaPiece>(E_HAND_KY + i)),
              static_cast<PieceNumber>(PieceNumber::GI + i));
    EXPECT_EQ(list.bona_piece(static_cast<PieceNumber>(PieceNumber::GI + i)).fb,
              static_cast<BonaPiece>(E_HAND_KY + i));
  }
}

TEST(TestEvaluationList, Border) {
  // インデックスの重複がないか全パターンを調べる

  EvaluationList list;

  PieceNumber no1(0), no2(1);
  constexpr std::array<int, 7> piece_count = {18, 4, 4, 4, 2, 2, 4};

  for (const auto color1 : COLOR) {
    for (int i1 = 0; i1 < 7; ++i1) {
      for (int j1 = 0; j1 < piece_count[i1]; ++j1) {
        list.Update(no1, color1, static_cast<Piece::Value>(i1 + 1), j1);

        const auto bp1 =
            static_cast<BonaPiece>(kpp_hand_index[color1][i1 + 1].fb + j1);
        for (const auto color2 : COLOR) {
          for (int i2 = 0; i2 < 7; ++i2) {
            for (int j2 = 0; j2 < piece_count[i2]; ++j2) {
              if (color1 == color2 && i1 == i2 && j1 == j2) {
                continue;
              }

              list.Update(no2, color2, static_cast<Piece::Value>(i2 + 1), j2);

              const auto bp2 = static_cast<BonaPiece>(
                  kpp_hand_index[color2][i2 + 1].fb + j2);

              // 重複がないならそれぞれを取り出せるはず

              EXPECT_EQ(list.bona_piece(no1).fb, bp1);
              EXPECT_EQ(list.bona_piece(no2).fb, bp2);
              // GetPieceNumberHandの引数は先手側から見たBonaPiece
              EXPECT_EQ(list.GetPieceNumberHand(bp1), no1);
              EXPECT_EQ(list.GetPieceNumberHand(bp2), no2);
            }
          }
        }
      }
    }
  }
}
}  // namespace