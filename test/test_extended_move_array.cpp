#include "stdafx.h"

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "extended_move.h"
#include "move.h"

using namespace state_action;

namespace {
TEST(Extended, Add1) {
  // push_backの動作を確認する

  ExtendedMoveArray e;

  EXPECT_EQ(e.size(), 0);

  const Move tmp1(Square::_11, Square::_12);
  e.push_back(tmp1, 1);

  EXPECT_EQ(e.size(), 1);

  const Move move1(tmp1, 1);
  EXPECT_EQ(e[0].move, move1);

  const Move tmp2(Square::_55, Square::_85);
  e.push_back(tmp2, 2);

  EXPECT_EQ(e.size(), 2);

  const Move move2(tmp2, 2);
  EXPECT_EQ(e[1].move, move2);
}
}  // namespace
