#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "packed_sfen.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestPositionPacked : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() {
    thread1 = std::make_unique<search::MainThread>(0);
    thread2 = std::make_unique<search::SubordinateThread>(1);
  }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/position_from_packed_sfen.json");
    ifs >> json;
  }

  Position position1, position2;
  Rollback rb1, rb2;
  std::unique_ptr<search::Thread> thread1, thread2;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};

TEST_F(TestPositionPacked, LoadPackedSfen) {
  LoadSfenFromFile();

  PackedSfen sfen;
  for (size_t i = 0; i < json.size(); ++i) {
    std::copy(json[i]["packed_sfen"].cbegin(), json[i]["packed_sfen"].cend(),
              sfen.data.begin());
    position1.Set(sfen, &rb1, thread1.get());

    position2.Set(json[i]["sfen"], &rb2, thread1.get());

    for (const auto square : SQUARE) {
      EXPECT_EQ(position1[square], position2[square]);
    }
    for (const auto color : COLOR) {
      EXPECT_EQ(position1[color], position2[color]);
    }

    EXPECT_EQ(position1.side(), position2.side());
  }
}
}  // namespace
