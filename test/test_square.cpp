// clang-format off
#include "stdafx.h"
// clang-format on

#include "../rule/square.h"

namespace {
TEST(TestSquare, FileOverload1) {
  // Fileのコンストラクタのオーバロードを確認

  constexpr File file1(0);
  EXPECT_TRUE(file1.ok());
  EXPECT_EQ(file1, File::_1);

  constexpr File file2('1');
  EXPECT_TRUE(file2.ok());
  EXPECT_EQ(file2, File::_1);

  constexpr uint8_t u = 2;
  constexpr int8_t s = 3;
  constexpr File file3(u), file4(s);
  EXPECT_TRUE(file3.ok());
  EXPECT_TRUE(file4.ok());
  EXPECT_EQ(file3, File::_3);
  EXPECT_EQ(file4, File::_4);
}

TEST(TestSquare, FileOverload2) {
  // 念のために網羅的にテスト

  EXPECT_EQ(File(0), File::_1);
  EXPECT_EQ(File(1), File::_2);
  EXPECT_EQ(File(2), File::_3);
  EXPECT_EQ(File(3), File::_4);
  EXPECT_EQ(File(4), File::_5);
  EXPECT_EQ(File(5), File::_6);
  EXPECT_EQ(File(6), File::_7);
  EXPECT_EQ(File(7), File::_8);
  EXPECT_EQ(File(8), File::_9);

  EXPECT_EQ(File('1'), File::_1);
  EXPECT_EQ(File('2'), File::_2);
  EXPECT_EQ(File('3'), File::_3);
  EXPECT_EQ(File('4'), File::_4);
  EXPECT_EQ(File('5'), File::_5);
  EXPECT_EQ(File('6'), File::_6);
  EXPECT_EQ(File('7'), File::_7);
  EXPECT_EQ(File('8'), File::_8);
  EXPECT_EQ(File('9'), File::_9);

  File file = File::_1;
  for (int i = 0; i < 9; ++i) {
    EXPECT_EQ(File(i), file);

    const char c = '1' + i;
    EXPECT_EQ(File(c), file);

    ++file;
  }
}

TEST(TestSquare, RankOverload1) {
  // Rankのコンストラクタのオーバロードを確認

  constexpr Rank rank1(0);
  EXPECT_TRUE(rank1.ok());
  EXPECT_EQ(rank1, Rank::_1);

  constexpr Rank rank2('a');
  EXPECT_TRUE(rank2.ok());
  EXPECT_EQ(rank2, Rank::_1);

  constexpr uint8_t u = 2;
  constexpr int8_t s = 3;
  constexpr Rank rank3(u), rank4(s);
  EXPECT_TRUE(rank3.ok());
  EXPECT_TRUE(rank4.ok());
  EXPECT_EQ(rank3, Rank::_3);
  EXPECT_EQ(rank4, Rank::_4);
}

TEST(TestSquare, RankOverload2) {
  // 念のために網羅的にテスト

  EXPECT_EQ(Rank(0), Rank::_1);
  EXPECT_EQ(Rank(1), Rank::_2);
  EXPECT_EQ(Rank(2), Rank::_3);
  EXPECT_EQ(Rank(3), Rank::_4);
  EXPECT_EQ(Rank(4), Rank::_5);
  EXPECT_EQ(Rank(5), Rank::_6);
  EXPECT_EQ(Rank(6), Rank::_7);
  EXPECT_EQ(Rank(7), Rank::_8);
  EXPECT_EQ(Rank(8), Rank::_9);

  EXPECT_EQ(Rank('a'), Rank::_1);
  EXPECT_EQ(Rank('b'), Rank::_2);
  EXPECT_EQ(Rank('c'), Rank::_3);
  EXPECT_EQ(Rank('d'), Rank::_4);
  EXPECT_EQ(Rank('e'), Rank::_5);
  EXPECT_EQ(Rank('f'), Rank::_6);
  EXPECT_EQ(Rank('g'), Rank::_7);
  EXPECT_EQ(Rank('h'), Rank::_8);
  EXPECT_EQ(Rank('i'), Rank::_9);

  Rank rank = Rank::_1;
  for (int i = 0; i < 9; ++i) {
    EXPECT_EQ(Rank(i), rank);

    const char c = 'a' + i;
    EXPECT_EQ(Rank(c), rank);

    ++rank;
  }
}
}  // namespace
