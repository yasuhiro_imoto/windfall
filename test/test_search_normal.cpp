#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../state_action/bitboard.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../usi.h"
#include "../utility.h"
#include "history.h"
#include "move_picker.h"
#include "pick_state.h"
#include "search_stack.h"
#include "thread.h"

using namespace search;

namespace {
class TestSearchNormal : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<MainThread>(0); }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<Thread> thread;
};

TEST_F(TestSearchNormal, MoveKeepedPiece) {
  // if文で処理しているはずの内容がアサーションで引っ掛かるという事態が
  // engine_normal.cpp:321で発生した
  // その部分の簡易的なコードで振る舞いを確認する

  // 先手番
  position.SetHirate(&rb, thread.get());

  for (const auto color : COLOR) {
    for (Piece piece = Piece::EMPTY; piece <= Piece::RY; ++piece) {
      SCOPED_TRACE(boost::format("color: %1%, piece: %2%") % color % piece);

      const Piece moved_piece(color, piece);

      // 移動先、移動元は関係ない
      const state_action::Move tmp(Square::_11, Square::_12);
      state_action::Move move(tmp, static_cast<uint16_t>(moved_piece));

      EXPECT_EQ(position.moved_piece(move), moved_piece);

      if (const auto mp = position.moved_piece(move);
          mp == Piece::EMPTY || mp.color() != position.side()) {
        move = state_action::Move::NONE;
      }

      if (move) {
        EXPECT_EQ(color, Color::BLACK);
        EXPECT_NE(piece, Piece::EMPTY);

        // 当然、一致するはず
        EXPECT_EQ(position.moved_piece(move).color(), position.side());
      } else {
        if (piece != Piece::EMPTY) {
          // 先手番でもEMPTYの場合はこちらに来る
          EXPECT_EQ(color, Color::WHITE);
        }
      }
    }
  }
}

TEST_F(TestSearchNormal, LeftChech) {
  // 王手を放置している局面が見つかったので、確認

  // この指し手がおかしい
  const state_action::Move move(static_cast<state_action::Move::Value>(529827));
  // 上の指し手で、この局面になった
  position.Set(
      "7+Ll/6k2/4p3p/2S3p2/s2l1n3/2+b1P1P2/8P/5S1R1/1+p1G1KbNL w R2G5Pgs2n6p "
      "134",
      &rb, thread.get());

  EXPECT_EQ(position.side(), Color::WHITE);
  // 王の移動が合法でないので、利きが残っている
  EXPECT_TRUE(
      position.IsEffected(Color::WHITE, position.ou_square<Color::BLACK>()));

  // 上の指し手で移動する前の局面
  position.Set(
      "7+Ll/6k2/4p3p/2S3p2/s2l1n3/2+b1P1P2/8P/4KS1R1/1+p1G2bNL b R2G5Pgs2n6p "
      "134",
      &rb, thread.get());
  EXPECT_TRUE(position.IsPseudoLegal(move));
  // 王が馬の利きから逃れられていないので違法
  EXPECT_FALSE(position.IsLegal(move));
}
}  // namespace
