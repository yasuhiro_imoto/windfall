#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestPosition : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() {
    utility::FinalizeLogger();
  }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/position_from_sfen.json");
    ifs >> json;
  }

  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};

TEST(Sample, Case1) { EXPECT_TRUE(true); }

TEST(Sample, Case2) { EXPECT_FALSE(false); }

TEST_F(TestPosition, HirateLocation) {
  // 平手で初期化した場合に駒が正しい位置にあるか
  position.SetHirate(&rb, thread.get());

  for (File f = File::_1; f <= File::_9; ++f) {
    EXPECT_EQ(position[Square(f, Rank::_7)], Piece(Piece::BLACK_FU));
    EXPECT_EQ(position[Square(f, Rank::_3)], Piece(Piece::WHITE_FU));
  }
  for (const File& f : {File::_1, File::_9}) {
    EXPECT_EQ(position[Square(f, Rank::_9)], Piece(Piece::BLACK_KY));
    EXPECT_EQ(position[Square(f, Rank::_1)], Piece(Piece::WHITE_KY));
  }
  for (const File& f : {File::_2, File::_8}) {
    EXPECT_EQ(position[Square(f, Rank::_9)], Piece(Piece::BLACK_KE));
    EXPECT_EQ(position[Square(f, Rank::_1)], Piece(Piece::WHITE_KE));
  }
  for (const File& f : {File::_3, File::_7}) {
    EXPECT_EQ(position[Square(f, Rank::_9)], Piece(Piece::BLACK_GI));
    EXPECT_EQ(position[Square(f, Rank::_1)], Piece(Piece::WHITE_GI));
  }
  for (const File& f : {File::_4, File::_6}) {
    EXPECT_EQ(position[Square(f, Rank::_9)], Piece(Piece::BLACK_KI));
    EXPECT_EQ(position[Square(f, Rank::_1)], Piece(Piece::WHITE_KI));
  }
  EXPECT_EQ(position[Square(File::_5, Rank::_9)], Piece(Piece::BLACK_OU));
  EXPECT_EQ(position[Square(File::_5, Rank::_1)], Piece(Piece::WHITE_OU));

  // 空いているマスを確認
  for (const Rank& r : {Rank::_2, Rank::_8}) {
    for (File f = File::_1; f <= File::_9; ++f) {
      if (f == File::_2 || f == File::_8) {
        continue;
      }

      EXPECT_EQ(position[Square(f, r)], Piece(Piece::EMPTY));
    }
  }
  for (Rank r = Rank::_4; r <= Rank::_6; ++r) {
    for (File f = File::_1; f <= File::_9; ++f) {
      EXPECT_EQ(position[Square(f, r)], Piece(Piece::EMPTY));
    }
  }

  // 成り駒がないことを確認
  for (const auto& piece :
       {Piece::TO, Piece::NY, Piece::NK, Piece::NG, Piece::UM, Piece::RY}) {
    for (const auto& square : SQUARE) {
      EXPECT_NE(position[square], Piece(Color::BLACK, piece));
      EXPECT_NE(position[square], Piece(Color::WHITE, piece));
    }
  }

  // 持ち駒がないことを確認
  for (const auto& piece : {Piece::FU, Piece::KY, Piece::KE, Piece::GI,
                            Piece::KA, Piece::HI, Piece::KI}) {
    EXPECT_EQ(position[Color::BLACK].count(piece), 0);
    EXPECT_EQ(position[Color::WHITE].count(piece), 0);
  }
}

TEST_F(TestPosition, FileOk) {
  // テスト内容を作ったファイルを見つけられるかを確認
  const auto path = std::filesystem::path("patterns/position_from_sfen.json");
  EXPECT_TRUE(std::filesystem::exists(path));
}
TEST_F(TestPosition, RandomLocation) {
  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    const std::vector<int> board = data["board"].get<std::vector<int>>();
    for (int j = 0; j < 81; ++j) {
      EXPECT_EQ(position[Square(j)], Piece(board[j]));
    }

    const std::vector<int> hand_black = data["hand"][0].get<std::vector<int>>();
    const std::vector<int> hand_white = data["hand"][1].get<std::vector<int>>();
    for (int j = 0; j < 7; ++j) {
      EXPECT_EQ(position[Color::BLACK].count(Piece(j + 1)), hand_black[j]);
      EXPECT_EQ(position[Color::WHITE].count(Piece(j + 1)), hand_white[j]);
    }
  }
}
}  // namespace
