#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "mate/mate_1ply.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestMate1Ply : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;
};

TEST_F(TestMate1Ply, Case1) {
  // 1手詰めの判定が間違っている局面を見つけたので、テスト

  const std::string sfen(
      "ln7/1r5s1/3gp1kp1/p+b1p1Pp2/1pp1P2Pg/3P1S3/PPS1G4/1KG4R1/LN6+l b "
      "SNbnl5p 75");
  position.Set(sfen, &rb, thread.get());

  const Move move = position.Mate1ply();

  EXPECT_EQ(move, Move(Move::NONE));
}

TEST_F(TestMate1Ply, Case2) {
  EXPECT_TRUE(true);
}
}  // namespace
