#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace std {
template <>
struct hash<Square> {
  std::size_t operator()(const Square& sq) const { return sq.value; }
};

template <>
struct equal_to<Square> {
  bool operator()(const Square& sq1, const Square& sq2) const {
    return sq1 == sq2;
  }
};

template <>
struct hash<Piece> {
  std::size_t operator()(const Piece& p) const { return p.value; }
};
}  // namespace std

namespace {
class TestAttacker : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() {
    thread = std::make_unique<search::MainThread>(0);

    // それぞれの手番にとっての正面
    constexpr SquareWithWall forward[2] = {SquareWithWall::U,
                                           SquareWithWall::D};
    // それぞれの手番にとっての斜め前
    constexpr SquareWithWall forward_side[2][2] = {
        {SquareWithWall::LU, SquareWithWall::RU},
        {SquareWithWall::LD, SquareWithWall::RD}};

    // 駒の利きの方向を初期化
    boost::copy(forward, cross_.begin());
    cross_[2] = SquareWithWall::L;
    cross_[3] = SquareWithWall::R;
    for (int i = 0; i < std::size(forward_side); ++i) {
      std::copy_n(std::begin(forward_side[i]), std::size(forward_side[i]),
                  diagonal_.begin() + i * std::size(forward_side[i]));
    }

    for (const Color color : COLOR) {
      move_map_[Piece(color, Piece::FU)] = {forward[color]};
    }

    move_map_[Piece::BLACK_KE] = {SquareWithWall::LUU, SquareWithWall::RUU};
    move_map_[Piece::WHITE_KE] = {SquareWithWall::LDD, SquareWithWall::RDD};

    for (const Color color : COLOR) {
      const auto gi = Piece(color, Piece::GI);
      move_map_[gi] = {move_map_[Piece(color, Piece::FU)]};
      boost::copy(diagonal_, std::back_inserter(move_map_[gi]));
    }
    for (const Color color : COLOR) {
      auto& tmp = move_map_[Piece(color, Piece::KI)];
      boost::copy(forward_side[color], std::back_inserter(tmp));
      boost::copy(cross_, std::back_inserter(tmp));
      for (const auto p : {Piece::TO, Piece::NY, Piece::NK, Piece::NG}) {
        move_map_[Piece(color, p)] = tmp;
      }
    }
    for (const Color color : COLOR) {
      auto& tmp = move_map_[Piece(color, Piece::OU)];
      boost::copy(cross_, std::back_inserter(tmp));
      boost::copy(diagonal_, std::back_inserter(tmp));
    }

    // 近接だけ設定
    for (const Color color : COLOR) {
      boost::copy(cross_,
                  std::back_inserter(move_map_[Piece(color, Piece::UM)]));
      boost::copy(diagonal_,
                  std::back_inserter(move_map_[Piece(color, Piece::RY)]));
    }
  }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/position_from_sfen.json");
    ifs >> json;
  }

  void SetPosition(const std::string &sfen) {
    position.Set(sfen, &rb, thread.get());
    for (const Square source : SQUARE) {
      if (position[source] == Piece::EMPTY) {
        continue;
      }
      MakeEffect(position[source], source);
    }
  }
  void SetPosition() {
    SetPosition(Position::sfen_hirate);
  }


  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;

  using SquareSet = std::unordered_set<Square>;
  using EffectArray = std::array<SquareSet, Square::SIZE>;
  /**
   * @brief targetのマスに利きのあるcolor側の駒のsourceの集合
   */
  std::array<EffectArray, Color::SIZE> attacker_list;

  std::array<SquareWithWall, 4> cross_, diagonal_;
  std::unordered_map<Piece, std::vector<SquareWithWall>> move_map_;

  /**
   * @brief 指定されたcolor側の香の利きを計算
   * @param color 利きの発生源の駒を持つ手番側
   * @param source 起点となる香のある升
   */
  void MakeKyEffect(const Color color, const Square source) {
    MakeEffect(color, source, move_map_[Piece(color, Piece::FU)][0]);
  }

  /**
   * @brief 指定されたcolor側の角と馬の長い利きを計算
   * @param color 利きの発生源の駒を持つ手番側
   * @param source 起点となる角か馬のある升
   */
  void MakeKaEffect(const Color color, const Square source) {
    for (const SquareWithWall& delta : diagonal_) {
      MakeEffect(color, source, delta);
    }
  }

  /**
   * @brief 指定されたcolor側の飛と龍の長い利きを計算
   * @param color 利きの発生源の駒を持つ手番側
   * @param source 起点となる飛か龍のある升
   */
  void MakeHiEffect(const Color color, const Square source) {
    for (const SquareWithWall& delta : cross_) {
      MakeEffect(color, source, delta);
    }
  }

  void MakeEffect(const Color color, const Square source,
                  const SquareWithWall delta) {
    SquareWithWall target = source;
    while (true) {
      target += delta;
      if (!target.ok()) {
        // 盤の外になった
        break;
      }

      const auto t = static_cast<Square>(target);
      attacker_list[color][t].emplace(source);
      if (position[t] != Piece::EMPTY) {
        break;
      }
    }
  }

  void MakeEffect(const Piece piece, const Square source) {
    const Color color = piece.color();
    if (piece.type() == Piece::KY) {
      MakeKyEffect(color, source);
      return;
    } else if (piece.raw_type() == Piece::KA) {
      MakeKaEffect(color, source);
      if (piece.type() != Piece::UM) {
        return;
      }
    } else if (piece.raw_type() == Piece::HI) {
      MakeHiEffect(color, source);
      if (piece.type() != Piece::RY) {
        return;
      }
    }
    const SquareWithWall sq(source);
    for (const SquareWithWall delta : move_map_[piece]) {
      const SquareWithWall target = sq + delta;
      if (target.ok()) {
        attacker_list[color][static_cast<Square>(target)].emplace(source);
      }
    }
  }

  Bitboard GetEffect(const Color color, const Square target) const {
    Bitboard b = Bitboard::Zero;
    const auto& source_list = attacker_list[color][target];
    for (const Square source : source_list) {
      b |= source;
    }
    return b;
  }
};

TEST_F(TestAttacker, EffectHirate) {
  // 平手の開始局面の利きをテスト

  SetPosition();

  for (const Square target : SQUARE) {
    for (const Color color : COLOR) {
      SCOPED_TRACE(boost::str(boost::format("target: %1%, color: %2%") %
                              target % color));
      const Bitboard b = GetEffect(color, target);
      const Bitboard attacker = position.attacker(color, target);

      EXPECT_EQ(attacker, b);
    }
    SCOPED_TRACE(boost::str(boost::format("all target: %1%") % target));

    const Bitboard b =
        GetEffect(Color::BLACK, target) | GetEffect(Color::WHITE, target);
    const Bitboard attacker = position.attacker(target);

    EXPECT_EQ(attacker, b);
  }
}

TEST_F(TestAttacker, EffectKy) {
  // 香の利きが何かおかしいのでそれだけでテスト

  // 特徴ベクトルの計算のために王が必要なので、香と反対の角においておく
  SetPosition("k7l/9/9/9/9/9/9/9/K7L b - 1");

  const Square target = Square::_12;
  for (const Color color : COLOR) {
    const Bitboard b = GetEffect(color, target);
    const Bitboard attacker = position.attacker(color, target);

    EXPECT_EQ(attacker, b);
  }

  const Bitboard b =
      GetEffect(Color::BLACK, target) | GetEffect(Color::WHITE, target);
  const Bitboard attacker = position.attacker(target);

  EXPECT_EQ(attacker, b);
}
}  // namespace
