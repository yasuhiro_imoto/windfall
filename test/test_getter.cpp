#include "stdafx.h"

// templateの引数がenumの場合に上手くオーバーロードを解決できないようで、
// バージョンアップで修正されていないか確認する

namespace {
enum EnumA { V1, V2 };

enum EnumB { N, M };

struct Foo {
  Foo() {
    x[0] = 0;
    x[1] = 1;

    y[0] = 10;
    y[1] = 20;
  }

  std::array<int, 2> x, y;

  template <EnumA V>
  int get() const {
    return x[static_cast<int>(V)];
  }

  template <EnumB K>
  int get() const {
    return y[static_cast<int>(K)];
  }

  int func(const EnumA v) const { return x[static_cast<int>(v)]; }

  int func(const EnumB k) const { return y[static_cast<int>(k)]; }
};

TEST(TemplateEnum, Case1) {
  Foo foo;

  // やっぱりエラーになる
  // gccでは通る
  // EXPECT_EQ(foo.get<EnumA::V1>(), 0);
  // EXPECT_EQ(foo.get<EnumB::N>(), 10);

  EXPECT_EQ(foo.func(EnumA::V2), 1);
  EXPECT_EQ(foo.func(EnumB::M), 20);
}

}  // namespace
