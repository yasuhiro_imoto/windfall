#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "move_generator.h"
#include "move_generator/generator_check.h"
#include "move_generator/generator_drop.h"
#include "move_generator/generator_evasion.h"
#include "move_generator/generator_piece.h"
#include "move_generator/generator_type.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestMoveGenerator : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/legal_moves.json");
    ifs >> json;
  }

  void SetPosition() { position.SetHirate(&rb, thread.get()); }
  void SetPosition(const std::string sfen) {
    position.Set(sfen, &rb, thread.get());
  }

  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};

TEST_F(TestMoveGenerator, LegalMoves) {
  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    const auto moves = data["moves"].get<std::vector<std::string>>();
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      move_set.emplace(position, m);
    }

    const MoveList<MoveGeneratorType::LEGAL_ALL> move_list(position);

    EXPECT_EQ(move_list.size(), move_set.size());

    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      // 集合に含まれるはず
      EXPECT_TRUE(it != move_set.end());
    }
  }
}

template <Piece::Value P>
void TestPieceMove(const Position& position,
                   const nlohmann::json::value_type& data) {
  if (position.checked()) {
    // 王手がかかっていると合法手でない可能性が高いので、スキップ
    return;
  }

  const auto moves = data["moves"].get<std::vector<std::string>>();
  std::unordered_set<Move> move_set;
  for (const auto& m : moves) {
    const Move tmp(position, m);
    const auto p = static_cast<Piece::Value>(
        (position.side() == Color::BLACK ? Piece::BLACK : Piece::WHITE) | P);
    if (!tmp.drop() && position.moving_piece(tmp) == p) {
      // 目的の駒だけを抽出
      move_set.emplace(tmp);
    }
  }

  const auto side = position.side();
  ExtendedMoveArray move_list;
  if (side == Color::BLACK) {
    const Bitboard target_bb = ~position.color<Color::BLACK>();
    move_generator::GeneratePieceMoves<
        move_generator::MoveGeneratorType::NON_EVASIONS, P, Color::BLACK,
        true>()(position, target_bb, move_list);
  } else {
    const Bitboard target_bb = ~position.color<Color::WHITE>();
    move_generator::GeneratePieceMoves<
        move_generator::MoveGeneratorType::NON_EVASIONS, P, Color::WHITE,
        true>()(position, target_bb, move_list);
  }
  // ピンされている可能性があるので、チェックが必要
  move_list.RemovePsuedoIllegalMoves(position);
  move_list.RemoveIllegalMoves(position);

  EXPECT_EQ(move_list.size(), move_set.size());
  for (const auto m : move_list) {
    const auto it = boost::range::find(move_set, m);
    EXPECT_TRUE(it != move_set.end());
  }
}
TEST_F(TestMoveGenerator, LegalFu) {
  // 歩についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    TestPieceMove<Piece::FU>(position, data);
  }
}

TEST_F(TestMoveGenerator, LegalKy) {
  // 香についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    TestPieceMove<Piece::KY>(position, data);
  }
}

TEST_F(TestMoveGenerator, LegalKe) {
  // 桂についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    TestPieceMove<Piece::KE>(position, data);
  }
}

TEST_F(TestMoveGenerator, LegalGi) {
  // 銀についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    TestPieceMove<Piece::GI>(position, data);
  }
}

TEST_F(TestMoveGenerator, LegalBR) {
  // 角と飛についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    if (position.checked()) {
      // 王手がかかっていると合法手でない可能性が高いので、スキップ
      continue;
    }

    const auto moves = data["moves"].get<std::vector<std::string>>();
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      const Move tmp(position, m);
      const auto ka =
          (position.side() == Color::BLACK ? Piece::BLACK_KA : Piece::WHITE_KA);
      const auto hi =
          (position.side() == Color::BLACK ? Piece::BLACK_HI : Piece::WHITE_HI);
      const auto p = position.moving_piece(tmp);
      if (!tmp.drop() && (p == ka || p == hi)) {
        // 目的の駒だけを抽出
        move_set.emplace(tmp);
      }
    }

    const auto side = position.side();
    ExtendedMoveArray move_list;
    if (side == Color::BLACK) {
      const Bitboard target_bb = ~position.color<Color::BLACK>();
      move_generator::GeneratePieceMoves<
          move_generator::MoveGeneratorType::NON_EVASIONS, Piece::GPM_BR,
          Color::BLACK, true>()(position, target_bb, move_list);
    } else {
      const Bitboard target_bb = ~position.color<Color::WHITE>();
      move_generator::GeneratePieceMoves<
          move_generator::MoveGeneratorType::NON_EVASIONS, Piece::GPM_BR,
          Color::WHITE, true>()(position, target_bb, move_list);
    }
    // ピンされている可能性があるので、チェックが必要
    move_list.RemovePsuedoIllegalMoves(position);
    move_list.RemoveIllegalMoves(position);

    EXPECT_EQ(move_list.size(), move_set.size());
    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      EXPECT_TRUE(it != move_set.end());
    }
  }
}

TEST_F(TestMoveGenerator, LegalGHDK) {
  // 金、成金、馬、龍、王についての指し手をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    if (position.checked()) {
      // 王手がかかっていると合法手でない可能性が高いので、スキップ
      continue;
    }

    const auto moves = data["moves"].get<std::vector<std::string>>();
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      const Move tmp(position, m);
      // KI以降は全て成れない駒
      const auto lower =
          position.side() == Color::BLACK ? Piece::BLACK_KI : Piece::WHITE_KI;
      const auto upper =
          position.side() == Color::BLACK ? Piece::BLACK_RY : Piece::WHITE_RY;
      const auto p = position.moving_piece(tmp);
      if (!tmp.drop() && (p >= lower && p <= upper)) {
        // 目的の駒だけを抽出
        move_set.emplace(tmp);
      }
    }

    const auto side = position.side();
    ExtendedMoveArray move_list;
    if (side == Color::BLACK) {
      const Bitboard target_bb = ~position.color<Color::BLACK>();
      move_generator::GeneratePieceMoves<
          move_generator::MoveGeneratorType::NON_EVASIONS, Piece::GPM_GHDK,
          Color::BLACK, true>()(position, target_bb, move_list);
    } else {
      const Bitboard target_bb = ~position.color<Color::WHITE>();
      move_generator::GeneratePieceMoves<
          move_generator::MoveGeneratorType::NON_EVASIONS, Piece::GPM_GHDK,
          Color::WHITE, true>()(position, target_bb, move_list);
    }
    // ピンされている可能性があるので、チェックが必要
    move_list.RemovePsuedoIllegalMoves(position);
    move_list.RemoveIllegalMoves(position);

    EXPECT_EQ(move_list.size(), move_set.size());
    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      EXPECT_TRUE(it != move_set.end());
    }
  }
}

TEST_F(TestMoveGenerator, LegalDrop) {
  // 駒を打つ処理をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    if (position.checked()) {
      // 王手がかかっていると合法手でない可能性が高いので、スキップ
      continue;
    }

    const auto moves = data["moves"].get<std::vector<std::string>>();
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      const Move tmp(position, m);
      if (tmp.drop()) {
        // 目的の駒だけを抽出
        move_set.emplace(tmp);
      }
    }

    const auto side = position.side();
    ExtendedMoveArray move_list;
    const Bitboard target_bb = position.empties();
    if (side == Color::BLACK) {
      move_generator::GenerateDropMoves<Color::BLACK>()(position, target_bb,
                                                        move_list);
    } else {
      move_generator::GenerateDropMoves<Color::WHITE>()(position, target_bb,
                                                        move_list);
    }

    EXPECT_EQ(move_list.size(), move_set.size());
    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      EXPECT_TRUE(it != move_set.end());
    }
  }
}

TEST_F(TestMoveGenerator, EvasionMoves) {
  // 王手を回避する指し手生成をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    if (!position.checked()) {
      // 王手でない時はここでは扱わないので、スキップ
      continue;
    }

    const auto moves = data["moves"].get<std::vector<std::string>>();
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      const Move tmp(position, m);
      if (tmp.drop()) {
        // 目的の駒だけを抽出
        move_set.emplace(tmp);
      }
    }

    const auto side = position.side();
    ExtendedMoveArray move_list;
    const Bitboard target_bb = position.empties();
    if (side == Color::BLACK) {
      move_generator::GenerateEvasions<Color::BLACK, true>(position, move_list);
    } else {
      move_generator::GenerateEvasions<Color::WHITE, true>(position, move_list);
    }

    EXPECT_EQ(move_list.size(), move_set.size());
    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      EXPECT_TRUE(it != move_set.end());
    }
  }
}

TEST_F(TestMoveGenerator, CheckMoves) {
  // 王手をかける指し手生成をテスト

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    const auto moves = data["moves"].get<std::vector<std::string>>();
    // 王手になる指し手
    std::unordered_set<Move> move_set;
    for (const auto& m : moves) {
      const Move tmp(position, m);
      if (position.IsCheckMove(tmp)) {
        move_set.emplace(tmp);
      }
    }

    const auto side = position.side();
    ExtendedMoveArray move_list;
    const Bitboard target_bb = position.empties();
    if (side == Color::BLACK) {
      move_generator::GenerateChecks<move_generator::MoveGeneratorType::CHECKS,
                                     Color::BLACK, true>(position, move_list);
    } else {
      move_generator::GenerateChecks<move_generator::MoveGeneratorType::CHECKS,
                                     Color::WHITE, true>(position, move_list);
    }

    EXPECT_EQ(move_list.size(), move_set.size());
    for (const auto m : move_list) {
      const auto it = boost::range::find(move_set, m);
      EXPECT_TRUE(it != move_set.end());
    }
  }
}
}  // namespace
