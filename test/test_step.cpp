#include "stdafx.h"

#include "../evaluation/evaluator.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
bool IsEquals(const Bitboard& x, const Bitboard& y) { return !(x ^ y); }
template <Color::Value C>
bool IsEqualsColor(const Position& x, const Position& y) {
  return IsEquals(x.color<C>(), y.color<C>());
}
template <Piece::Value P>
bool IsEqualsType(const Position& x, const Position& y) {
  return IsEquals(x.types<P>(), y.types<P>());
}

class TestPositionStep : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    InitializeBitboards();
    Position::Initialize();

    evaluation::Initialize();
    evaluation::LoadParameter();
  }

  static void TearDownTestCase() {}

  virtual void SetUp() {
    thread = std::make_unique<search::MainThread>(0);
    states = std::make_unique<state_action::StateList>(1);
  }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/move_sequence.json");
    ifs >> json;
  }

  void CheckPosition(const std::string sfen, const int ply) const {
    Position tmp_position;
    Rollback tmp_rb;
    // スレッドは使わないので、動かしているPositionと同じものを与える
    tmp_position.Set(sfen, &tmp_rb, thread.get());

    // 正しく遷移できているかひたすら確認

    EXPECT_EQ(position.side(), tmp_position.side());
    // 初期局面の手数は1
    EXPECT_EQ(position.game_ply(), ply);
    EXPECT_EQ(position.key(), tmp_position.key());
    EXPECT_TRUE(IsEqualsColor<Color::BLACK>(position, tmp_position));
    EXPECT_TRUE(IsEqualsColor<Color::WHITE>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::ALL_PIECES>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::FU>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::KY>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::KE>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::GI>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::KA>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::HI>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::KI>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::OU>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::TO>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::NY>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::NK>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::NG>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::UM>(position, tmp_position));
    EXPECT_TRUE(IsEqualsType<Piece::RY>(position, tmp_position));
    for (const auto square : SQUARE) {
      EXPECT_EQ(position[square], tmp_position[square]);
    }
    EXPECT_EQ(position[Color::BLACK], tmp_position[Color::BLACK]);
    EXPECT_EQ(position[Color::WHITE], tmp_position[Color::WHITE]);
  }

  Position position;
  StateListPtr states;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};


TEST_F(TestPositionStep, ForwardBackward) {
  // 指し手を進めた場合と戻した場合が正しい局面になっているか確認する

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    states->clear();

    // スタートは初期配置
    states->emplace_back();
    position.SetHirate(&states->back(), thread.get());

    const auto data = json[i];

    // 局面を戻す時用に指し手を保存する
    std::vector<state_action::Move> move_list;
    move_list.reserve(data.size());

    // 局面を進める
    for (int j = 0; j < data.size(); ++j) {
      SCOPED_TRACE(boost::str(boost::format("forward: %1%") % j));

      const auto sfen_move = data[j]["move"].get<std::string>();
      const auto move = Move(position, sfen_move);

      // 進める
      states->emplace_back();
      position.StepForward(move, states->back());

      move_list.push_back(move);

      const auto sfen = data[j]["sfen"].get<std::string>();
      // 正しく遷移できているかひたすら確認
      CheckPosition(sfen, j + 2);
    }

    // 局面を戻す
    for (int j = static_cast<int>(data.size()) - 1; j >= 0; --j) {
      SCOPED_TRACE(boost::str(boost::format("backward: %1%") % j));

      // 戻す
      position.StepBackward(move_list[j]);

      std::string sfen;
      if (j == 0) {
        sfen = Position::sfen_hirate;
      } else {
        // 一つ前の状態
        sfen = data[j - 1]["sfen"].get<std::string>();
      }
      CheckPosition(sfen, j + 1);
    }
  }
}
}  // namespace