#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../search/thread.h"
#include "../state_action.h"
#include "../usi.h"
#include "../utility.h"
#include "bonanza_piece.h"
#include "evaluator.h"
#include "kpp_index.h"
#include "nn/feature/index_list.h"
#include "nn/nnue_common.h"

using namespace evaluation;

namespace {
template <Color::Value C>
constexpr int GetSquareIndex(const Square square) {
  return C == Color::BLACK ? square : 80 - square;
}

#ifdef NNUE_LITE
template <Color::Value C>
std::unordered_set<int> ComputeIndex(const state_action::Position& position) {
  static_assert(FE_END == 361, "");

  const Square ou_square = position.ou_square<C>();
  const int offset = FE_END * GetSquareIndex<C>(ou_square);

  std::unordered_set<int> indices;
  for (const Square square : SQUARE) {
    const Piece piece = position[square];
    if (piece == Piece::EMPTY) {
      continue;
    } else if (piece.type() == Piece::OU && piece.color() == C) {
      // 自分の王はインデックスに含めない
      continue;
    }

    // C側に対応するpieceのbona_piece
    const auto bp = kpp_board_index[piece].from[C];
    // 盤上の駒のBonaPiece
    const int value = bp + GetSquareIndex<C>(square);
    indices.emplace(offset + value);
  }

  for (const Color color : COLOR) {
    const state_action::Hand hand = position[color];
    for (int i = 1; i <= 7; ++i) {
      const auto bp = kpp_hand_index[color][i].from[C];
      const Piece piece(static_cast<Piece::Value>(i));

      const int count = hand.count(piece);
      for (int j = 0; j < count; ++j) {
        indices.emplace(offset + bp + j);
      }
    }
  }

  return indices;
}
#else
template <Color::Value C>
std::unordered_set<int> ComputeIndex(const state_action::Position& position) {
  static_assert(FE_END == 1548, "");

  const Square ou_square = position.ou_square<C>();
  const int offset = 1548 * (C == Color::BLACK ? ou_square : 80 - ou_square);

  constexpr int board_index[] = {-99,                               // empty
                                 2,   4,  6,  8,  12, 16, 10, -99,  //
                                 10,  10, 10, 10, 14, 18};

  std::unordered_set<int> indices;
  for (const Square square : SQUARE) {
    const Piece piece = position[square];
    if (piece == Piece::EMPTY) {
      continue;
    } else if (piece.type() == Piece::OU) {
      continue;
    }

    // 盤上のBonaPieceについて駒の種類についてのインデックス
    const int index = board_index[piece.type()] + (piece.color() == C ? 0 : 1);
    // 盤上の駒のBonaPiece
    // 90は持ち駒のサイズ
    const int value =
        90 + index * 81 + (C == Color::BLACK ? square : 80 - square);
    indices.emplace(offset + value);
  }

  constexpr int hand_index[] = {
      -99, -99,  // unused
      1,   20,   // FU
      39,  44,   // KY
      49,  54,   // KE
      59,  64,   // GI
      79,  82,   // KA
      85,  88,   // HI
      69,  74    // KI
  };
  for (const Color color : COLOR) {
    const state_action::Hand hand = position[color];
    for (int i = 1; i <= 7; ++i) {
      const int index = hand_index[i * 2 + (color == C ? 0 : 1)];
      const Piece piece(static_cast<Piece::Value>(i));

      const int count = hand.count(piece);
      for (int j = 0; j < count; ++j) {
        indices.emplace(offset + index + j);
      }
    }
  }

  return indices;
}
#endif  // NNUE_LITE

class TestFeatureIndex : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();

    evaluation::LoadParameter();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<search::Thread> thread;

#ifdef NNUE_LITE
  // 相手の王が増えた
  static constexpr size_t index_size = 39;
#else
  // 駒落ちはないので、38枚
  static constexpr size_t index_size = 38;
#endif // NNUE_LITE

};

TEST_F(TestFeatureIndex, FeatureIndexSize) {
  // 平手局面の特徴量のインデックスの個数を確認する

  position.SetHirate(&rb, thread.get());

  std::array<nn::feature::IndexList, 2> active_indices;
  // 最初は0のはず
  EXPECT_EQ(active_indices[0].size(), 0);
  EXPECT_EQ(active_indices[1].size(), 0);

  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  EXPECT_EQ(active_indices[0].size(), index_size);
  EXPECT_EQ(active_indices[1].size(), index_size);
}

TEST_F(TestFeatureIndex, SameFeature) {
  // 平手の初期局面は先後同型なので、インデックスはおなじになる
  // テストのためのテスト

  position.SetHirate(&rb, thread.get());

  std::array<nn::feature::IndexList, 2> active_indices;
  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  // 平手局面のインデックス
  const auto black_indices = ComputeIndex<Color::BLACK>(position);
  const auto white_indices = ComputeIndex<Color::WHITE>(position);

  for (const auto i : black_indices) {
    EXPECT_TRUE(boost::find(white_indices, i) != white_indices.end());
  }
  for (const auto i : white_indices) {
    EXPECT_TRUE(boost::find(black_indices, i) != black_indices.end());
  }
}

TEST_F(TestFeatureIndex, IndexOrder) {
  // 駒番号の順にインデックスが作られているはずなので、
  // ソートせずとも歩から並んでいるはず
  // インデックスの順序を確認する

  position.SetHirate(&rb, thread.get());

  std::array<nn::feature::IndexList, 2> active_indices;
  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  for (size_t i = 0; i < index_size; ++i) {
    if (i < PieceNumber::KY) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::FU));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::FU));
    } else if (i < PieceNumber::KE) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::KY));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::KY));
    } else if (i < PieceNumber::GI) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::KE));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::KE));
    } else if (i < PieceNumber::KA) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::GI));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::GI));
    } else if (i < PieceNumber::HI) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::KA));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::KA));
    } else if (i < PieceNumber::KI) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::HI));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::HI));
    } else if (i < PieceNumber::OU) {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::KI));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::KI));
    } else {
      EXPECT_EQ(active_indices[0][i] >> 16, static_cast<int>(Piece::OU));
      EXPECT_EQ(active_indices[1][i] >> 16, static_cast<int>(Piece::OU));
    }
  }
}

#ifdef NNUE_LITE
TEST_F(TestFeatureIndex, kpp_table1) {
  // テストでもkpp indexを利用しているので、そのkpp indexが正しいか確認する
  // テーブルの中身を確認する

  // black
  EXPECT_EQ(kpp_board_index[Piece::BLACK_FU].fb, F_FU);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_TO].fb, F_TO);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KY].fb, F_KY);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NY].fb, F_NY);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KE].fb, F_KE);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NK].fb, F_NK);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_GI].fb, F_GI);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NG].fb, F_NG);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KA].fb, F_KA);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_UM].fb, F_UM);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_HI].fb, F_HI);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_RY].fb, F_RY);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KI].fb, F_KI);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_OU].fb, F_OU);

  // white
  EXPECT_EQ(kpp_board_index[Piece::WHITE_FU].fw, F_FU);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_TO].fw, F_TO);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KY].fw, F_KY);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NY].fw, F_NY);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KE].fw, F_KE);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NK].fw, F_NK);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_GI].fw, F_GI);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NG].fw, F_NG);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KA].fw, F_KA);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_UM].fw, F_UM);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_HI].fw, F_HI);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_RY].fw, F_RY);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KI].fw, F_KI);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_OU].fw, F_OU);

  // black
  EXPECT_EQ(kpp_board_index[Piece::BLACK_FU].fw, E_FU);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_TO].fw, E_TO);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KY].fw, E_KY);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NY].fw, E_NY);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KE].fw, E_KE);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NK].fw, E_NK);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_GI].fw, E_GI);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_NG].fw, E_NG);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KA].fw, E_KA);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_UM].fw, E_UM);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_HI].fw, E_HI);
  EXPECT_EQ(kpp_board_index[Piece::BLACK_RY].fw, E_RY);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_KI].fw, E_KI);

  EXPECT_EQ(kpp_board_index[Piece::BLACK_OU].fw, E_OU);

  // white
  EXPECT_EQ(kpp_board_index[Piece::WHITE_FU].fb, E_FU);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_TO].fb, E_TO);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KY].fb, E_KY);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NY].fb, E_NY);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KE].fb, E_KE);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NK].fb, E_NK);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_GI].fb, E_GI);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_NG].fb, E_NG);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KA].fb, E_KA);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_UM].fb, E_UM);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_HI].fb, E_HI);
  EXPECT_EQ(kpp_board_index[Piece::WHITE_RY].fb, E_RY);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_KI].fb, E_KI);

  EXPECT_EQ(kpp_board_index[Piece::WHITE_OU].fb, E_OU);

  // black
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::FU].fb, F_HAND_FU);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KY].fb, F_HAND_KY);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KE].fb, F_HAND_KE);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::GI].fb, F_HAND_GI);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KA].fb, F_HAND_KA);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::HI].fb, F_HAND_HI);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KI].fb, F_HAND_KI);

  // white
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::FU].fw, F_HAND_FU);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KY].fw, F_HAND_KY);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KE].fw, F_HAND_KE);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::GI].fw, F_HAND_GI);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KA].fw, F_HAND_KA);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::HI].fw, F_HAND_HI);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KI].fw, F_HAND_KI);

  // black
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::FU].fw, E_HAND_FU);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KY].fw, E_HAND_KY);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KE].fw, E_HAND_KE);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::GI].fw, E_HAND_GI);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KA].fw, E_HAND_KA);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::HI].fw, E_HAND_HI);
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::KI].fw, E_HAND_KI);

  // white
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::FU].fb, E_HAND_FU);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KY].fb, E_HAND_KY);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KE].fb, E_HAND_KE);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::GI].fb, E_HAND_GI);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KA].fb, E_HAND_KA);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::HI].fb, E_HAND_HI);
  EXPECT_EQ(kpp_hand_index[Color::WHITE][Piece::KI].fb, E_HAND_KI);
}

TEST_F(TestFeatureIndex, kpp_table2) {
  // bona pieceの値が適切に設定されているか確認する

  for (Piece piece = Piece::FU; piece <= Piece::OU; ++piece) {
    const auto black = piece.value + Piece::BLACK;
    const auto white = piece.value + Piece::WHITE;

    const auto& index1 = kpp_board_index[black];
    const auto& index3 = kpp_board_index[white];

    EXPECT_EQ(index1.fb, (piece.value << 16) + 1);

    EXPECT_EQ(index3.fb - index1.fb, 81 * 2);
    if (piece != Piece::KI && piece != Piece::OU) {
      const auto& index2 = kpp_board_index[black + Piece::PROMOTION];
      const auto& index4 = kpp_board_index[white + Piece::PROMOTION];

      EXPECT_EQ(index2.fb - index1.fb, 81);
      EXPECT_EQ(index4.fb - index3.fb, 81);
    }

    if (piece == Piece::OU) {
      continue;
    }

    const auto& black_hand = kpp_hand_index[Color::BLACK][piece];
    const auto& white_hand = kpp_hand_index[Color::WHITE][piece];
    EXPECT_EQ(black_hand.fb - index3.fb, 81 * 2);
    EXPECT_EQ(white_hand.fb - black_hand.fb, 18);

    EXPECT_EQ((white_hand.fb + 18) & 0xFFFF, FE_END);
  }
}

TEST_F(TestFeatureIndex, kpp_table3) {
  // 持ち駒の枚数が0の時はhand_indexを使わない
  // 枚数が1枚の時に{fb, fw} + 0を使う
  // なので、+0から+17までしか使わない
  EXPECT_EQ(kpp_hand_index[Color::BLACK][Piece::FU].fb + 18,
            kpp_hand_index[Color::BLACK][Piece::FU].fw + 0);

  EXPECT_EQ(static_cast<int>(kpp_hand_index[Color::BLACK][Piece::FU].fw),
            kpp_hand_index[Color::BLACK][Piece::FU].fw + 0);
}

TEST_F(TestFeatureIndex, FeatureIndex) {
  // 平手局面の特徴量のインデックスの内容を確認する

  position.SetHirate(&rb, thread.get());

  std::array<nn::feature::IndexList, 2> active_indices;
  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  // 平手局面のインデックス
  const auto black_indices = ComputeIndex<Color::BLACK>(position);
  const auto white_indices = ComputeIndex<Color::WHITE>(position);
  for (int i = 0; i < 39; ++i) {
    SCOPED_TRACE(i);
    const auto active = active_indices[0][i], inactive = active_indices[1][i];
    if (position.side() == Color::BLACK) {
      EXPECT_TRUE(boost::find(black_indices, active) != black_indices.end());
      EXPECT_TRUE(boost::find(white_indices, inactive) != white_indices.end());
    } else {
      EXPECT_TRUE(boost::find(white_indices, active) != white_indices.end());
      EXPECT_TRUE(boost::find(black_indices, inactive) != black_indices.end());
    }
  }
}
#else
TEST_F(TestFeatureIndex, FeatureIndex) {
  // 平手局面の特徴量のインデックスの内容を確認する

  position.SetHirate(&rb, thread.get());

  std::array<nn::feature::IndexList, 2> active_indices;
  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  // 平手局面のインデックス
  const std::unordered_set<uint32_t> black_indices{
      68445, 68285, 68208, 68372, 68616, 69103, 68294, 68217, 69352, 68543,
      68787, 68303, 68226, 68714, 68958, 68312, 68235, 68885, 68321, 68244,
      68976, 68330, 68253, 68903, 68823, 68339, 68262, 68750, 68670, 69481,
      68348, 68271, 69082, 68597, 68517, 68357, 68280, 68444};
  const std::unordered_set<uint32_t> white_indices{
      68444, 68280, 68357, 68517, 68597, 69082, 68271, 68348, 69481, 68670,
      68750, 68262, 68339, 68823, 68903, 68253, 68330, 68976, 68244, 68321,
      68885, 68235, 68312, 68958, 68714, 68226, 68303, 68787, 68543, 69352,
      68217, 68294, 69103, 68616, 68372, 68208, 68285, 68445};
  // const auto black_indices = ComputeIndex<Color::BLACK>(position);
  // const auto white_indices = ComputeIndex<Color::WHITE>(position);
  for (const auto i : active_indices[0]) {
    if (position.side() == Color::BLACK) {
      EXPECT_TRUE(boost::find(black_indices, i) != black_indices.end());
    } else {
      EXPECT_TRUE(boost::find(white_indices, i) != white_indices.end());
    }
  }
  for (const auto i : active_indices[1]) {
    if (~position.side() == Color::BLACK) {
      EXPECT_TRUE(boost::find(black_indices, i) != black_indices.end());
    } else {
      EXPECT_TRUE(boost::find(white_indices, i) != white_indices.end());
    }
  }
}

#endif  // NNUE_LITE

TEST_F(TestFeatureIndex, FeatureIndexBlackFu) {
  // squareの位置に歩を置く
  for (const Square square : SQUARE) {
    if (square.rank() == Rank::_1) {
      continue;
    }

    std::stringstream ss;
    for (int rank = 0; rank < 9; ++rank) {
      if (rank == 0) {
        // 11に王を置く
        ss << "k7K";
      } else if (square.rank() == Rank(rank)) {
        if (square.file() == File::_9) {
          ss << "/P8";
        } else if (square.file() == File::_1) {
          ss << "/8P";
        } else {
          ss << '/' << static_cast<int>(8 - square.file().value) << 'P'
             << static_cast<int>(square.file().value);
        }
      } else {
        ss << "/9";
      }
    }
    ss << " b - 1";

    position.Set(ss.str(), &rb, thread.get());

    std::array<nn::feature::IndexList, 2> active_indices;
    nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                         active_indices);

    const Square black_ou = position.ou_square<Color::BLACK>(),
                 white_ou = position.ou_square<Color::WHITE>();
    EXPECT_EQ(active_indices[0][0],
              FE_END * black_ou.value + F_FU + square.value);
    EXPECT_EQ(active_indices[1][0],
              FE_END * (80 - white_ou.value) + E_FU + (80 - square.value));
  }
}

TEST_F(TestFeatureIndex, FeatureIndexBlackHand) {
  // 手番側の持ち駒についてインデックスをテスト

  // 王をそれぞれの初期配置に設定
  constexpr Square black_ou = Square::_59, white_ou = Square::_51;
  const std::string sfen_board = "4k4/9/9/9/9/9/9/9/4K4 b ";

  constexpr char piece_list[] = {'P', 'L', 'N', 'S', 'B', 'R', 'G'};
  constexpr int piece_size[] = {18, 4, 4, 4, 2, 2, 4};

  constexpr BonaPiece bona_list_f[] = {F_HAND_FU, F_HAND_KY, F_HAND_KE,
                                       F_HAND_GI, F_HAND_KA, F_HAND_HI,
                                       F_HAND_KI};
  constexpr BonaPiece bona_list_e[] = {E_HAND_FU, E_HAND_KY, E_HAND_KE,
                                       E_HAND_GI, E_HAND_KA, E_HAND_HI,
                                       E_HAND_KI};

  for (int i = 0; i < 7; ++i) {
    for (int j = 1; j <= piece_size[i]; ++j) {
      SCOPED_TRACE(boost::str(boost::format("%1%, %2%") % i % j));

      std::stringstream ss;
      ss << sfen_board;
      if (j != 1) {
        ss << j;
      }
      ss << piece_list[i] << " 1";

      position.Set(ss.str(), &rb, thread.get());

      std::array<nn::feature::IndexList, 2> active_indices;
      nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                           active_indices);

      // 特徴量の次元数は38で固定
      // 駒がない場合はBONA_PIECE_ZEROが使われる
      EXPECT_EQ(active_indices[0].size(), index_size);
      EXPECT_EQ(active_indices[1].size(), index_size);
      for (size_t k = 0; k < index_size; ++k) {
        SCOPED_TRACE(k);

        // 王の位置に対応したオフセット
        // 駒落ちの場合の値でもある
        constexpr auto black_base = FE_END * black_ou.value + BONA_PIECE_ZERO;
        constexpr auto white_base =
            FE_END * (80 - white_ou.value) + BONA_PIECE_ZERO;

#ifdef NNUE_LITE
        if (active_indices[0][k] >> 16 == i + 1) {
          // 目的の種類の駒
          // 駒落ちが含まれているかも
          if ((active_indices[0][k] & 0xFFFF) != black_base) {
            EXPECT_GE(active_indices[0][k],
                      static_cast<uint32_t>(black_base + bona_list_f[i] + 0));
            EXPECT_LT(active_indices[0][k],
                      static_cast<uint32_t>(black_base + bona_list_f[i] + j));
          }
        }
        if (active_indices[1][k] >> 16 == i + 1) {
          // 目的の種類の駒
          // 駒落ちが含まれているかも
          if ((active_indices[1][k] & 0xFFFF) != white_base) {
            EXPECT_GE(active_indices[1][k],
                      static_cast<uint32_t>(white_base + bona_list_e[i] + 0));
            EXPECT_LT(active_indices[1][k],
                      static_cast<uint32_t>(white_base + bona_list_e[i] + j));
          }
        }
#else
        if (active_indices[0][k] != black_base &&
            active_indices[0][k] != black_base + E_OU + white_ou) {
          EXPECT_GE(active_indices[0][k],
                    static_cast<uint32_t>(black_base + bona_list_f[i] + 0));
          EXPECT_LT(active_indices[0][k],
                    static_cast<uint32_t>(black_base + bona_list_f[i] + j));
        }
        if (active_indices[1][k] != white_base &&
            active_indices[1][k] != white_base + E_OU + (80 - black_ou)) {
          EXPECT_GE(active_indices[1][k],
                    static_cast<uint32_t>(white_base + bona_list_e[i] + 0));
          EXPECT_LT(active_indices[1][k],
                    static_cast<uint32_t>(white_base + bona_list_e[i] + j));
        }
#endif  // NNUE_LITE
      }
    }
  }
}

TEST_F(TestFeatureIndex, FeatureIndexWhiteHand) {
  // 非手番側の持ち駒についてインデックスをテスト

  // 王をそれぞれの初期配置に設定
  constexpr Square black_ou = Square::_59, white_ou = Square::_51;
  const std::string sfen_board = "4k4/9/9/9/9/9/9/9/4K4 b ";

  constexpr char piece_list[] = {'p', 'l', 'n', 's', 'b', 'r', 'g'};
  constexpr int piece_size[] = {18, 4, 4, 4, 2, 2, 4};

  constexpr BonaPiece bona_list_f[] = {F_HAND_FU, F_HAND_KY, F_HAND_KE,
                                       F_HAND_GI, F_HAND_KA, F_HAND_HI,
                                       F_HAND_KI};
  constexpr BonaPiece bona_list_e[] = {E_HAND_FU, E_HAND_KY, E_HAND_KE,
                                       E_HAND_GI, E_HAND_KA, E_HAND_HI,
                                       E_HAND_KI};

  for (int i = 0; i < 7; ++i) {
    for (int j = 1; j <= piece_size[i]; ++j) {
      SCOPED_TRACE(boost::str(boost::format("%1%, %2%") % i % j));

      std::stringstream ss;
      ss << sfen_board;
      if (j != 1) {
        ss << j;
      }
      ss << piece_list[i] << " 1";

      position.Set(ss.str(), &rb, thread.get());

      std::array<nn::feature::IndexList, 2> active_indices;
      nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                           active_indices);

      // 特徴量の次元数は38で固定
      // 駒がない場合はBONA_PIECE_ZEROが使われる
      EXPECT_EQ(active_indices[0].size(), index_size);
      EXPECT_EQ(active_indices[1].size(), index_size);
      for (size_t k = 0; k < index_size; ++k) {
        SCOPED_TRACE(k);

        constexpr auto black_base = FE_END * black_ou.value + BONA_PIECE_ZERO;
        constexpr auto white_base =
            FE_END * (80 - white_ou.value) + BONA_PIECE_ZERO;

#ifdef NNUE_LITE
        if (active_indices[0][k] >> 16 == i + 1) {
          // 目的の種類の駒
          // 駒落ちが含まれているかも
          if ((active_indices[0][k] & 0xFFFF) != black_base) {
            EXPECT_GE(active_indices[0][k],
                      static_cast<uint32_t>(black_base + bona_list_e[i] + 0));
            EXPECT_LT(active_indices[0][k],
                      static_cast<uint32_t>(black_base + bona_list_e[i] + j));
          }
        }
        if (active_indices[1][k] >> 16 == i + 1) {
          // 目的の種類の駒
          // 駒落ちが含まれているかも
          if ((active_indices[1][k] & 0xFFFF) != white_base) {
            EXPECT_GE(active_indices[1][k],
                      static_cast<uint32_t>(white_base + bona_list_f[i] + 0));
            EXPECT_LT(active_indices[1][k],
                      static_cast<uint32_t>(white_base + bona_list_f[i] + j));
          }
        }
#else
        if (active_indices[0][k] != black_base &&
            active_indices[0][k] != black_base + E_OU + white_ou) {
          EXPECT_GE(active_indices[0][k],
                    static_cast<uint32_t>(black_base + bona_list_e[i] + 0));
          EXPECT_LT(active_indices[0][k],
                    static_cast<uint32_t>(black_base + bona_list_e[i] + j));
        }
        if (active_indices[1][k] != white_base &&
            active_indices[1][k] != white_base + E_OU + (80 - black_ou)) {
          EXPECT_GE(active_indices[1][k],
                    static_cast<uint32_t>(white_base + bona_list_f[i] + 0));
          EXPECT_LT(active_indices[1][k],
                    static_cast<uint32_t>(white_base + bona_list_f[i] + j));
        }
#endif  // NNUE_LITE
      }
    }
  }
}
}  // namespace