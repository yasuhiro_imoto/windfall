#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../state_action/bitboard.h"
#include "../state_action/packed_sfen.h"
#include "../state_action/position.h"
#include "../state_action/rollback.h"
#include "../usi.h"
#include "../utility.h"
#include "history.h"
#include "move_picker.h"
#include "pick_state.h"
#include "search_initialization.h"
#include "search_stack.h"
#include "thread.h"
#include "../evaluation/nn/network_architecture.h"
#include "../evaluation/nn/feature/index_list.h"

using namespace search;

namespace {
class TestSearchFeature : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    Initialize();
    thread_pool.Set(usi::options["Threads"]);

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<MainThread>(0); }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<Thread> thread;

#ifdef NNUE_LITE
  static constexpr size_t feature_size = 39;
#else
  static constexpr size_t feature_size = 38;
#endif  // NNUE_LITE
};
// 探索の途中で局面がおかしくなっているのを見つけた
// feature indexが40個になっていて、
// 持ち駒の角の特徴インデックスの値が重複していた
// UpdateAccumulatorで差分の不正なインデックスを掴んでエラーになる

TEST_F(TestSearchFeature, Step1) {
  // packed sfenの読み込みが正しいか調べる

  state_action::PackedSfen packed_sfen;
  packed_sfen.data = {33,  138, 17,  41,  12, 146, 128, 53,  197, 114, 240,
                      158, 193, 243, 176, 18, 7,   237, 124, 229, 14,  4,
                      240, 43,  241, 99,  68, 138, 1,   30,  36,  159};
  position.Set(packed_sfen, &rb, thread.get());

  const std::string sfen(
      "l7l/1r1sng1k1/3g1snp1/1pP2Pp2/p2pP3p/3+nS1PP1/P4GN1P/LR3GSK1/4+p3L w "
      "BPb3p 1");
  state_action::Position position2;
  state_action::Rollback rb2;
  std::unique_ptr<Thread> thread2 = std::make_unique<SubordinateThread>(1);
  position2.Set(sfen, &rb, thread2.get());

  for (const auto square : SQUARE) {
    EXPECT_EQ(position[square], position2[square]);
  }
  for (const auto color : COLOR) {
    EXPECT_EQ(position[color], position2[color]);
  }

  const auto& evaluation_list = *position.evaluation_list();
  std::unordered_set<evaluation::BonaPiece> black_set, white_set;
  for (size_t i = 0; i < 40; ++i) {
    black_set.emplace(evaluation_list.piece_list_fb()[i]);
    white_set.emplace(evaluation_list.piece_list_fw()[i]);
  }
  // インデックスに重複がないなら40個ある
  EXPECT_EQ(black_set.size(), 40);
  EXPECT_EQ(white_set.size(), 40);

  // 現状では特徴インデックスの個数はfeature_size個
  std::array<evaluation::nn::feature::IndexList, 2> indices;
  evaluation::nn::RawFeatures::AppendActiveIndices(
      position, evaluation::nn::refresh_triggers[0], indices);
  EXPECT_EQ(indices[0].size(), feature_size);
  EXPECT_EQ(indices[1].size(), feature_size);
  // 駒の種類を識別する値がある
  for (size_t i = 0; i < feature_size; ++i) {
    const int32_t index0 = indices[0][i] >> 16;
    const int32_t index1 = indices[1][i] >> 16;
    EXPECT_GT(index0, 0);
    EXPECT_GT(index1, 0);

    EXPECT_LE(index0, 8);
    EXPECT_LE(index1, 8);
  }
  std::unordered_set<uint32_t> feature_set0, feature_set1;
  for (size_t i = 0; i < feature_size; ++i) {
    feature_set0.emplace(indices[0][i]);
    feature_set1.emplace(indices[1][i]);
  }
  EXPECT_EQ(feature_set0.size(), feature_size);
  EXPECT_EQ(feature_set1.size(), feature_size);

  // 先手と後手の両方が角を持ち駒にしている
  const auto ka_b1 = evaluation_list.piece_list_fb()[PieceNumber::KA + 0];
  const auto ka_b2 = evaluation_list.piece_list_fb()[PieceNumber::KA + 1];
  EXPECT_NE(ka_b1, ka_b2);
  if (ka_b1 < ka_b2) {
    EXPECT_EQ(ka_b1, evaluation::F_HAND_KA);
    EXPECT_EQ(ka_b2, evaluation::E_HAND_KA);
  } else {
    EXPECT_EQ(ka_b1, evaluation::E_HAND_KA);
    EXPECT_EQ(ka_b2, evaluation::F_HAND_KA);
  }
  const auto ka_w1 = evaluation_list.piece_list_fw()[PieceNumber::KA + 0];
  const auto ka_w2 = evaluation_list.piece_list_fw()[PieceNumber::KA + 1];
  EXPECT_NE(ka_w1, ka_w2);
  if (ka_w1 < ka_w2) {
    EXPECT_EQ(ka_w1, evaluation::F_HAND_KA);
    EXPECT_EQ(ka_w2, evaluation::E_HAND_KA);
  } else {
    EXPECT_EQ(ka_w1, evaluation::E_HAND_KA);
    EXPECT_EQ(ka_w2, evaluation::F_HAND_KA);
  }

  EXPECT_EQ(PieceNumber::KA + 0, evaluation_list.GetPieceNumberHand(ka_b1));
  EXPECT_EQ(PieceNumber::KA + 1, evaluation_list.GetPieceNumberHand(ka_b2));
  // GetPieceNumberHandは先手側から見たBonaPieceを与える必要がある
  // EXPECT_EQ(PieceNumber::KA + 0, evaluation_list.GetPieceNumberHand(ka_w1));
  // EXPECT_EQ(PieceNumber::KA + 1, evaluation_list.GetPieceNumberHand(ka_w2));
}
}  // namespace
