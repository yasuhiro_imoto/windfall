// clang-format off
#include "stdafx.h"
// clang-format on

#include "position.h"
#include "bitboard.h"
#include "rollback.h"
#include "../search/thread.h"
#include "../rule/color.h"
#include "../rule/square.h"
#include "../rule/piece.h"

using namespace state_action;

namespace {
class TestBitboard : public ::testing::Test {
protected:
  static void SetUpTestCase() {
    InitializeBitboards();
    Position::Initialize();
  }

  static void TearDownTestCase() {}
};

TEST_F(TestBitboard, Table) {
  // SquareからBitboardへの変換が正しいかどうか

  for (const Square source : SQUARE) {
    const Bitboard actual(source);

    uint64_t p0, p1;
    const uint64_t s = enum_cast<>(source.value);
    if (s < 63) {
      p0 = 1ull << s;
      p1 = 0;
    } else {
      p0 = 0;
      p1 = 1ull << (s - 63);
    }
    const Bitboard expected(p0, p1);

    EXPECT_EQ(actual, expected);
  }
}
}  // namespace