#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "move.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestUsiMove : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/legal_moves.json");
    ifs >> json;
  }

  void SetPosition() { position.SetHirate(&rb, thread.get()); }
  void SetPosition(const std::string sfen) {
    position.Set(sfen, &rb, thread.get());
  }

  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};

TEST_F(TestUsiMove, FromUsi1) {
  // USIからの指し手の変換をテスト

  position.SetHirate(&rb, thread.get());

  const Move move1(position, "1g1f");

  Move move2(Square::_17, Square::_16);
  move2 = position.ConvertMove16ToMove(move2);

  EXPECT_EQ(move1, move2);
}

TEST_F(TestUsiMove, FromUsi2) {
  // USIからの指し手の変換をテスト
  // 上位16bitは設定しない

  position.SetHirate(&rb, thread.get());

  const Move move1("1g1f");

  const Move move2(Square::_17, Square::_16);

  EXPECT_EQ(move1, move2);
}

TEST_F(TestUsiMove, HirateFu) {
  // 初期配置の歩の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (int i = 1; i <= 9; ++i) {
    std::string s = boost::str(boost::format("%1%g%1%f") % i);
    const Move move1(position, s);

    Move move2(Square(File(i - 1), Rank::_7), Square(File(i - 1), Rank::_6));
    move2 = position.ConvertMove16ToMove(move2);

    EXPECT_EQ(move1, move2);
  }
}

TEST_F(TestUsiMove, HirateKy) {
  // 初期配置の香の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (const int i : {1, 9}) {
    std::string s = boost::str(boost::format("%1%i%1%h") % i);
    const Move move1(position, s);

    Move move2(Square(File(i - 1), Rank::_9), Square(File(i - 1), Rank::_8));
    move2 = position.ConvertMove16ToMove(move2);

    EXPECT_EQ(move1, move2);
  }
}

TEST_F(TestUsiMove, HirateGi) {
  // 初期配置の銀の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (const int i : {3, 7}) {
    for (const int j : {-1, 0, 1}) {
      if (i + j == 2 || i + j == 8) {
        continue;
      }

      std::string s = boost::str(boost::format("%1%i%2%h") % i % (i + j));
      const Move move1(position, s);

      Move move2(Square(File(i - 1), Rank::_9),
                 Square(File(i + j - 1), Rank::_8));
      move2 = position.ConvertMove16ToMove(move2);

      EXPECT_EQ(move1, move2);
    }
  }
}

TEST_F(TestUsiMove, HirateKi) {
  // 初期配置の金の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (const int i : {4, 6}) {
    for (const int j : {-1, 0, 1}) {
      std::string s = boost::str(boost::format("%1%i%2%h") % i % (i + j));
      const Move move1(position, s);

      Move move2(Square(File(i - 1), Rank::_9),
                 Square(File(i + j - 1), Rank::_8));
      move2 = position.ConvertMove16ToMove(move2);

      EXPECT_EQ(move1, move2);
    }
  }
}

TEST_F(TestUsiMove, HirateHi) {
  // 初期配置の飛の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (const int i : {1, 3, 4, 5, 6, 7}) {
    std::string s = boost::str(boost::format("2h%1%h") % i);
    const Move move1(position, s);

    Move move2(Square(File::_2, Rank::_8), Square(File(i - 1), Rank::_8));
    move2 = position.ConvertMove16ToMove(move2);

    EXPECT_EQ(move1, move2);
  }
}

TEST_F(TestUsiMove, HirateOu) {
  // 初期配置の王の指し手について確認

  position.SetHirate(&rb, thread.get());

  for (const int i : {4, 5, 6}) {
    std::string s = boost::str(boost::format("5i%1%h") % i);
    const Move move1(position, s);

    Move move2(Square(File::_5, Rank::_9), Square(File(i - 1), Rank::_8));
    move2 = position.ConvertMove16ToMove(move2);

    EXPECT_EQ(move1, move2);
  }
}

TEST_F(TestUsiMove, LegalMoves) {
  const std::unordered_map<char, Piece::Value> drop_map = {
      {'P', Piece::FU}, {'L', Piece::KY}, {'N', Piece::KE}, {'S', Piece::GI},
      {'B', Piece::KA}, {'R', Piece::HI}, {'G', Piece::KI}};
  const std::unordered_map<char, File::Value> file_map = {
      {'1', File::_1}, {'2', File::_2}, {'3', File::_3},
      {'4', File::_4}, {'5', File::_5}, {'6', File::_6},
      {'7', File::_7}, {'8', File::_8}, {'9', File::_9}};
  const std::unordered_map<char, Rank::Value> rank_map = {
      {'a', Rank::_1}, {'b', Rank::_2}, {'c', Rank::_3},
      {'d', Rank::_4}, {'e', Rank::_5}, {'f', Rank::_6},
      {'g', Rank::_7}, {'h', Rank::_8}, {'i', Rank::_9}};

  LoadSfenFromFile();
  for (int i = 0; i < json.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = json[i];

    const std::string sfen = data["sfen"].get<std::string>();
    position.Set(sfen, &rb, thread.get());

    const auto moves = data["moves"].get<std::vector<std::string>>();

    for (const auto& s : moves) {
      const Move m(position, s);

      // drop
      EXPECT_EQ(m.drop(), s[1] == '*');

      if (m.drop()) {
        EXPECT_EQ(m.dropped_piece(), drop_map.at(s[0]));
      } else {
        EXPECT_EQ(m.source(), Square(file_map.at(s[0]), rank_map.at(s[1])));
      }
      EXPECT_EQ(m.target(), Square(file_map.at(s[2]), rank_map.at(s[3])));

      if (m.drop()) {
        const auto c =
            position.side() == Color::BLACK ? Piece::BLACK : Piece::WHITE;
        EXPECT_EQ(
            static_cast<Piece::Value>(m.dropped_piece().value + enum_cast<>(c)),
            static_cast<Piece::Value>(m.value >> 16));
      } else {
        if (s.size() == 5) {
          const Piece promoted(static_cast<Piece::Value>(
              position[m.source()].value + Piece::PROMOTION));
          EXPECT_EQ(promoted, static_cast<Piece::Value>(m.value >> 16));
        } else {
          EXPECT_EQ(position[m.source()],
                    static_cast<Piece::Value>(m.value >> 16));
        }
      }
    }
  }
}
}  // namespace
