#include "stdafx.h"

#include "../evaluation.h"
#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi.h"
#include "../utility.h"
#include "bitboard.h"
#include "position.h"
#include "rollback.h"

using namespace state_action;

namespace {
class TestChecked : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();

    evaluation::LoadParameter();
  }

  static void TearDownTestCase() { utility::FinalizeLogger(); }

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  /**
   * @brief cshogiでランダムに作成したパターンを読み込む
   */
  void LoadSfenFromFile() {
    std::ifstream ifs("patterns/is_checked.json");
    ifs >> json;
  }

  void SetPosition() { position.SetHirate(&rb, thread.get()); }
  void SetPosition(const std::string sfen) {
    position.Set(sfen, &rb, thread.get());
  }

  Position position;
  Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief ファイルから読み込んだjson
   */
  nlohmann::json json;
};

TEST_F(TestChecked, CheckHirate) {
  // 平手局面で王手されているか

  SetPosition();
  EXPECT_FALSE(position.checked());
}

TEST_F(TestChecked, FileOk) {
  // テスト内容を作ったファイルを見つけられるかを確認
  const auto path = std::filesystem::path("patterns/is_checked.json");
  EXPECT_TRUE(std::filesystem::exists(path));
}

TEST_F(TestChecked, RandomPositionTrue) {
  LoadSfenFromFile();
  const auto positive_data = json["positive"];
  for (int i = 0; i < positive_data.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = positive_data[i];
    const std::string sfen = data.get<std::string>();
    position.Set(sfen, &rb, thread.get());

    EXPECT_TRUE(position.checked());
  }
}

TEST_F(TestChecked, RandomPositionFalse) {
  LoadSfenFromFile();
  const auto negative_data = json["negative"];
  for (int i = 0; i < negative_data.size(); ++i) {
    SCOPED_TRACE(boost::str(boost::format("loop: %1%") % i));

    const auto data = negative_data[i];
    const std::string sfen = data.get<std::string>();
    position.Set(sfen, &rb, thread.get());

    EXPECT_FALSE(position.checked());
  }
}
}  // namespace
