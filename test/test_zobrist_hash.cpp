#include "stdafx.h"

#include "zobrist_hash.h"

using namespace state_action;

namespace {
class TestHash : public ::testing::Test {
 protected:
  static void SetUpTestCase() { zobrist::Initialize(); }
  static void TearDownTestCase() {}
};

TEST_F(TestHash, Board) {
  // YaneuraOuとハッシュの値が同じであるか確認する

  std::filesystem::path path("hash/board.csv");
  EXPECT_TRUE(std::filesystem::exists(path));

  std::vector<std::vector<Key>> table(Square::SIZE_PLUS1);
  {
    std::ifstream ifs(path);
    std::string buffer;
    int i = 0;
    while (std::getline(ifs, buffer)) {
      std::vector<std::string> result;
      boost::algorithm::split(result, buffer, boost::is_any_of(","));

      table[i].reserve(Piece::SIZE);

      boost::range::transform(result, std::back_inserter(table[i]),
                              [](const std::string& s) {
#ifdef _MSC_VER
								// なぜlexical_castでエラーになる
                                return boost::lexical_cast<uint64_t>(s);
#else
								return std::stoull(s);
#endif	// _MSC_VER
                              });
      ++i;
    }
  }

  for (size_t i = 0; i < table.size(); ++i) {
    SCOPED_TRACE(i);

    EXPECT_EQ(table[i].size(), enum_cast<>(Piece::SIZE));
    for (size_t j = 0; j < table[i].size(); ++j) {
      SCOPED_TRACE(j);
      EXPECT_EQ(table[i][j], zobrist::psq[i][j]);
    }
  }
}

TEST_F(TestHash, Hand) {
  // YaneuraOuとハッシュの値が同じであるか確認する

  std::filesystem::path path("hash/hand.csv");
  EXPECT_TRUE(std::filesystem::exists(path));

  std::vector<std::vector<Key>> table(Color::SIZE);
  {
    std::ifstream ifs(path);
    std::string buffer;
    int i = 0;
    while (std::getline(ifs, buffer)) {
      std::vector<std::string> result;
      boost::algorithm::split(result, buffer, boost::is_any_of(","));

      table[i].reserve(Piece::HAND_SIZE);
      boost::range::transform(result, std::back_inserter(table[i]),
                              [](const std::string& s) {
#ifdef _MSC_VER
								// なぜlexical_castでエラーになる
                                return boost::lexical_cast<uint64_t>(s);
#else
								return std::stoull(s);
#endif	// _MSC_VER
                              });

      ++i;
    }
  }

  for (size_t i = 0; i < table.size(); ++i) {
    SCOPED_TRACE(i);

    EXPECT_EQ(table[i].size(), enum_cast<>(Piece::HAND_SIZE));
    for (size_t j = 0; j < table[i].size(); ++j) {
      SCOPED_TRACE(j);
      EXPECT_EQ(table[i][j], zobrist::hand[i][j]);
    }
  }
}

TEST_F(TestHash, Depth) {
  // YaneuraOuとハッシュの値が同じであるか確認する

  std::filesystem::path path("hash/depth.csv");
  EXPECT_TRUE(std::filesystem::exists(path));

  std::vector<Key> table;
  {
    std::ifstream ifs(path);
    std::string buffer;
    int i = 0;
    while (std::getline(ifs, buffer)) {
      std::vector<std::string> result;
      boost::algorithm::split(result, buffer, boost::is_any_of(","));

      boost::range::transform(result, std::back_inserter(table),
                              [](const std::string& s) {
#ifdef _MSC_VER
								// なぜlexical_castでエラーになる
                                return boost::lexical_cast<uint64_t>(s);
#else
								return std::stoull(s);
#endif	// _MSC_VER
                              });

      ++i;
    }
  }

  EXPECT_EQ(table.size(), std::size(zobrist::depth));
  for (size_t i = 0; i < table.size(); ++i) {
    SCOPED_TRACE(i);

    EXPECT_EQ(table[i], zobrist::depth[i]);
  }
}
}  // namespace

