#include "stdafx.h"

#define NNUE_LEARN
#include "../evaluation.h"
#include "../rule/color.h"
#include "../search/thread.h"
#include "../state_action.h"
#include "../state_action/move_generator.h"
#include "../usi.h"
#include "../usi/command.h"
#include "../utility.h"
#include "learner.h"
#include "search_initialization.h"

using namespace search;

namespace {
class TestTrainingPair : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    Initialize();
    thread_pool.Set(usi::options["Threads"]);

    evaluation::Initialize();

    usi::command::GetReady(false);
  }

  static void TearDownTestCase() {}

  virtual void SetUp() {
    thread = std::make_unique<search::SubordinateThread>(0);
  }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<search::Thread> thread;

  /**
   * @brief 教師データの読み筋として使うダミーデータを作成する
   * @param depth
   * @return
   */
  std::vector<state_action::Move> MakeRandomPV(const int depth) {
    // 再現性のためにシードを固定
    std::mt19937 engine(0);

    std::vector<state_action::Move> pv(depth);
    std::vector<state_action::Rollback> states(depth);
    for (int i = 0; i < depth; ++i) {
      // 合法手からランダムに選ぶ
      state_action::LegalMoveList move_list(position);
      std::uniform_int_distribution<> dist(
          0, static_cast<int>(move_list.size()) - 1);

      const int index = dist(engine);
      pv[i] = move_list[index];

      position.StepForward(move_list[index], states[i]);
    }

    // 局面を巻き戻す
    for (auto it = pv.rbegin(); it != pv.rend(); ++it) {
      position.StepBackward(*it);
    }

    return pv;
  }
};

/**
 * @brief 特徴量のインデックスがPVの終端の局面のものになっているかテスト
 * @param position
 * @param states
 * @param pv
 * @param target_indices
 */
void CheckPvIndices(
    state_action::Position& position,
    std::vector<state_action::Rollback>& states,
    const std::vector<state_action::Move>& pv,
    const std::array<evaluation::nn::feature::IndexList, 2>& target_indices) {
  states.resize(pv.size());
  // 局面を進める
  for (size_t i = 0; i < pv.size(); ++i) {
    position.StepForward(pv[i], states[i]);
  }
  std::array<evaluation::nn::feature::IndexList, 2> indices;
  evaluation::nn::RawFeatures::AppendActiveIndices(
      position, evaluation::nn::refresh_triggers[0], indices);
  // 局面を戻す
  for (auto it = pv.rbegin(); it != pv.rend(); ++it) {
    position.StepBackward(*it);
  }

  // 特徴量が同じかテスト
  for (int i = 0; i < 2; ++i) {
    EXPECT_EQ(target_indices[i].size(), indices[i].size());
    for (size_t j = 0; j < indices[i].size(); ++j) {
      EXPECT_EQ(target_indices[i][j], indices[i][j]);
    }
  }
}

#ifdef _DEBUG
TEST_F(TestTrainingPair, Pair1) {
  // 初期局面で教師データが正しく作れているか確かめる

  evaluation::LoadParameter();

  position.SetHirate(&rb, thread.get());
  std::vector<state_action::Move> teacher_pv = MakeRandomPV(3);

  learner::TrainingPair training_pair;

  const bool result = learner::GetTrainingPair(position, teacher_pv.begin(),
                                               teacher_pv.end(), training_pair);

  // ランダムな読み筋と一致することはないはず
  EXPECT_TRUE(result);

  EXPECT_EQ(teacher_pv.size(), training_pair.teacher_pv.size());
  for (size_t i = 0; i < teacher_pv.size(); ++i) {
    EXPECT_EQ(teacher_pv[i], training_pair.teacher_pv[i]);
  }

  // GetTrainingPairで局面を変更したままなので、再設定
  position.SetHirate(&rb, thread.get());

  const auto n = std::max(training_pair.teacher_pv.size(),
                          training_pair.student_pv.size());
  std::vector<state_action::Rollback> states;
  states.reserve(n);

  // 教師局面を確認
  {
    SCOPED_TRACE("teacher");
    CheckPvIndices(position, states, training_pair.teacher_pv,
                   training_pair.teacher_indices);
  }
  // 生徒局面を確認
  {
    SCOPED_TRACE("student");
    CheckPvIndices(position, states, training_pair.student_pv,
                   training_pair.student_indices);
  }

  const auto m = std::min(training_pair.teacher_pv.size(),
                          training_pair.student_pv.size());
  for (size_t i = 0; i < m; ++i) {
    if (training_pair.teacher_pv[i] == training_pair.student_pv[i]) {
      continue;
    }
    // 少なくとも一つは読み筋が違うはず

    if ((training_pair.teacher_pv.size() - i) % 2 == 1) {
      EXPECT_EQ(-1.0, training_pair.teacher_sign);
    } else {
      EXPECT_EQ(1.0, training_pair.teacher_sign);
    }
    if ((training_pair.student_pv.size() - i) % 2 == 1) {
      EXPECT_EQ(-1.0, training_pair.student_sign);
    } else {
      EXPECT_EQ(1.0, training_pair.student_sign);
    }
    break;
  }
}

TEST_F(TestTrainingPair, Pair2) {
  // 教師データはランダムなので、1回目のdepth 1の探索で読み筋が一致しないはず
  // 1回目の探索を一致させて2回目の探索に移行するか確認する

  evaluation::LoadParameter();

  position.SetHirate(&rb, thread.get());
  std::vector<state_action::Move> teacher_pv = MakeRandomPV(3);

  learner::TrainingPair training_pair1;

  learner::GetTrainingPair(position, teacher_pv.begin(), teacher_pv.end(),
                           training_pair1);
  for (size_t i = 0; i < teacher_pv.size(); ++i) {
    SCOPED_TRACE(fmt::format("preparing:{}", i));

    if (i == teacher_pv.size() - 1) {
      // 1回目のdepth 1の探索で読み筋を一致させる
      teacher_pv[i] = training_pair1.student_pv[i];
    } else {
      // depth 1の探索なので、まだ分岐していないと想定
      EXPECT_EQ(teacher_pv[i], training_pair1.student_pv[i]);
    }
  }

  // 読み筋を探索し直す
  position.SetHirate(&rb, thread.get());
  learner::TrainingPair training_pair2;
  const bool result = learner::GetTrainingPair(
      position, teacher_pv.begin(), teacher_pv.end(), training_pair2);
  EXPECT_TRUE(result);

  for (size_t i = 0; i < teacher_pv.size(); ++i) {
    SCOPED_TRACE(fmt::format("take:{}", i));
    // これはすべて一致するはず
    EXPECT_EQ(teacher_pv[i], training_pair2.teacher_pv[i]);
    if (i >= teacher_pv.size() - 2) {
      // 最後の2個だけ違う
      EXPECT_NE(teacher_pv[i], training_pair2.student_pv[i]);
      break;
    } else {
      // それまでは一致
      EXPECT_EQ(teacher_pv[i], training_pair2.student_pv[i]);
    }
  }

  const auto n = std::max(training_pair2.teacher_pv.size(),
                          training_pair2.student_pv.size());
  position.SetHirate(&rb, thread.get());
  std::vector<state_action::Rollback> states;
  states.reserve(n);
  {
    SCOPED_TRACE("teacher");
    CheckPvIndices(position, states, training_pair2.teacher_pv,
                   training_pair2.teacher_indices);
  }
  {
    SCOPED_TRACE("student");
    CheckPvIndices(position, states, training_pair2.student_pv,
                   training_pair2.student_indices);
  }

  // 2手先の局面で学習するので、評価値の符号はそのまま
  EXPECT_EQ(training_pair2.teacher_sign, 1);
  // 局面によっては探索が延長されるかも
  // 全体の長さから一致している部分の長さを引くことで分岐の後の長さを求める
  const auto k = training_pair2.teacher_pv.size() - 2;
  if ((training_pair2.student_pv.size() - k) % 2 == 1) {
    EXPECT_EQ(training_pair2.student_sign, -1.0);
  } else {
    EXPECT_EQ(training_pair2.student_sign, 1.0);
  }
}
#endif	// _DEBUG
}  // namespace
