#include "stdafx.h"

#include <spdlog/fmt/ostr.h>

#include "../rule/color.h"
#include "../rule/piece.h"
#include "../rule/square.h"
#include "../search/thread.h"
#include "../usi/protocol.h"
#include "bitboard.h"
#include "move.h"

using namespace state_action;

namespace {
TEST(TestMove, ConstructorTest) {
  // 何かよくわからない現象を見つけたのでそのテスト

  const Square source = Square::_82;
  const Square target = Square::_62;

  const Move m(source, target, false);

  EXPECT_EQ(m.source(), source);
  EXPECT_EQ(m.target(), target);
  EXPECT_FALSE(m.promotion());
  EXPECT_FALSE(m.drop());
}

TEST(TestMove, UsiOutput1) {
  const Move m(Square::_77, Square::_76);

  std::ostringstream oss;
  oss << m;

  EXPECT_EQ(oss.str(), "7g7f");
}

TEST(TestMove, UsiOutput2) {
  // spdlogを使って指し手を出力するときの確認

  std::ostringstream oss;
  auto sink = std::make_shared<spdlog::sinks::ostream_sink_mt>(oss);
  auto sync_logger = std::make_shared<spdlog::logger>("stream_sync", sink);
  sync_logger->set_pattern("%v");

  const Move m(Square::_77, Square::_76);

  sync_logger->info("bestmove {}", m);

  EXPECT_EQ(oss.str(), "bestmove 7g7f\n");
}

TEST(TestMove, UsiOutputResign) {
  // spdlogを使って指し手を出力するときの確認
  // 投了

  std::ostringstream oss;
  auto sink = std::make_shared<spdlog::sinks::ostream_sink_mt>(oss);
  auto sync_logger = std::make_shared<spdlog::logger>("stream_sync", sink);
  sync_logger->set_pattern("%v");

  sync_logger->info("bestmove {}", Move(Move::RESIGN));
  EXPECT_EQ(oss.str(), "bestmove resign\n");
}

TEST(TestMove, UsiOutputWin) {
  // spdlogを使って指し手を出力するときの確認
  // 宣言勝ち

  std::ostringstream oss;
  auto sink = std::make_shared<spdlog::sinks::ostream_sink_mt>(oss);
  auto sync_logger = std::make_shared<spdlog::logger>("stream_sync", sink);
  sync_logger->set_pattern("%v");

  sync_logger->info("bestmove {}", Move(Move::WIN));
  EXPECT_EQ(oss.str(), "bestmove win\n");
}
}  // namespace