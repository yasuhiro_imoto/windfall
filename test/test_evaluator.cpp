#include "stdafx.h"

#include "../rule/color.h"
#include "../search/thread.h"
#include "../state_action.h"
#include "../usi.h"
#include "../utility.h"
#include "evaluator.h"
#include "nn/feature/index_list.h"
#include "nn/nnue_common.h"
#include "bonanza_piece.h"
#include "nn/feature_embedding.h"
#include "nn/network.h"

using namespace evaluation;

namespace {
struct FeatureEmbeddingDummy {
  static constexpr nn::IndexType HalfDimensions = nn::EmbeddingDimensions;
  static constexpr nn::IndexType InputDimensions = nn::RawFeatures::dimensions;

 public:
  using BiasType = int16_t;
  using KernelType = int16_t;

  union {
    alignas(nn::cache_line_bytes) BiasType biases[HalfDimensions];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(nn::cache_line_bytes) char biases_msgpack[HalfDimensions *
                                                      sizeof(BiasType)];
  };

  union {
#ifdef NNUE_LITE
    alignas(nn::cache_line_bytes)
        KernelType kernels[HalfDimensions * InputDimensions / 8];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(nn::cache_line_bytes) char kernels_msgpack[HalfDimensions *
                                                       InputDimensions *
                                                       sizeof(KernelType) / 8];
#else
    alignas(nn::cache_line_bytes)
        KernelType kernels[HalfDimensions * InputDimensions];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(nn::cache_line_bytes) char kernels_msgpack[HalfDimensions *
                                                       InputDimensions *
                                                       sizeof(KernelType)];
#endif // NNUE_LITE
  };

  MSGPACK_DEFINE_MAP(MSGPACK_NVP("bias", biases_msgpack),
                     MSGPACK_NVP("kernel", kernels_msgpack));
};

struct InputSliceDummy {
  std::vector<int32_t> operator()(const std::vector<int32_t>& features) const {
    return features;
  }

  MSGPACK_DEFINE();
};

template <typename Previous, size_t Din, size_t Dout>
struct DenseDummy {
  std::vector<int32_t> operator()(const std::vector<int32_t>& features) const {
    const auto inputs = previous_layer(features);

    std::vector<int32_t> outputs(Dout);
    for (size_t i = 0; i < Dout; ++i) {
      outputs[i] = std::inner_product(inputs.begin(), inputs.end(),
                                      kernels + Din * i, biases[i]);
    }
    return outputs;
  }

  using BiasType = int32_t;
  using KernelType = int8_t;

  //! 一つ前の層
  Previous previous_layer;

  union {
    alignas(nn::cache_line_bytes) BiasType biases[Dout];
    // msgpackでraw byte arrayとして読み書きさせる
    alignas(nn::cache_line_bytes) char biases_msgpack[Dout * sizeof(BiasType)];
  };
  union {
    alignas(nn::cache_line_bytes) KernelType kernels[Dout * Din];
    alignas(nn::cache_line_bytes) char kernels_msgpack[Dout * Din *
                                                       sizeof(KernelType)];
  };

  MSGPACK_DEFINE_MAP(MSGPACK_NVP("bias", biases_msgpack),
                     MSGPACK_NVP("kernel", kernels_msgpack),
                     MSGPACK_NVP("previous", previous_layer));
};

template <typename Previous>
struct ReLUDummy {
  std::vector<int32_t> operator()(const std::vector<int32_t>& features) const {
    const auto inputs = previous_layer(features);
    std::vector<int32_t> outputs(inputs.size());
    boost::transform(inputs, outputs.begin(), [](const int32_t v) {
      return std::clamp(v / 64, 0, 127);
    });
    return outputs;
  }

  //! 一つ前の層
  Previous previous_layer;

  MSGPACK_DEFINE_MAP(MSGPACK_NVP("previous", previous_layer));
};

using Hidden1 = ReLUDummy<DenseDummy<InputSliceDummy, 512, 32>>;
using Hidden2 = ReLUDummy<DenseDummy<Hidden1, 32, 32>>;
using NetworkDummy = DenseDummy<Hidden2, 32, 1>;

/**
 * @brief 動作確認用にメンバ変数がpublicな構造体に値を読み込む
 * 
 * 読み込んだパラメータの値そのものが意図した値であるかはここでは気にしない
 * @param feature 
 * @param network
*/
void Load(std::unique_ptr<FeatureEmbeddingDummy>& feature,
          std::unique_ptr<NetworkDummy>& network) {
  const auto full_dir = std::filesystem::current_path() /
                        static_cast<std::string>(usi::options["EvalDir"]);
  std::cout << full_dir;

  const auto path = full_dir / "nn.mpac";
  std::ifstream ifs(path, std::ios::binary);
  std::istreambuf_iterator<char> first(ifs), last;
  const std::string buffer(first, last);

  try {
    std::size_t offset = 0;
    msgpack::object_handle handle0 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);
    msgpack::object_handle handle1 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);
    msgpack::object_handle handle2 =
        msgpack::unpack(buffer.data(), buffer.size(), offset);

    handle1->convert(*feature);
    // メモリが確保されている場合に初期化する
    if (network) {
      handle2->convert(network);
    }
  } catch (msgpack::unpack_error&) {
    std::cout << "info string unpack failed";
  } catch (msgpack::type_error&) {
    std::cout << "info string type error";
  }
}

/**
 * @brief 特徴ベクトルを計算する
 * 
 * 特徴ベクトルだけの場合とスコアを計算する場合の両方から呼び出す
 * @param position 
 * @param feature 
 * @return 
*/
std::vector<int32_t> ComputeEmbedding(
    const state_action::Position& position,
    const std::unique_ptr<FeatureEmbeddingDummy>& feature) {
  std::array<nn::feature::IndexList, 2> active_indices;
  nn::RawFeatures::AppendActiveIndices(position, nn::refresh_triggers[0],
                                       active_indices);

  std::vector<int32_t> results(nn::FeatureEmbedding::BufferSize);

  constexpr auto half_size = nn::FeatureEmbedding::BufferSize / 2;
  const std::array<Color, 2> perspective = {position.side(), ~position.side()};
  for (int i = 0; i < 2; ++i) {
    auto buffer = results.data() + i * half_size;
    boost::copy(feature->biases, buffer);
    for (const auto index : active_indices[perspective[i]]) {
#ifdef NNUE_LITE
      const auto piece_type = index >> 16;
      const auto bona_index = index & 0xFFFF;
      constexpr int sections = half_size / 8;
#pragma warning(push)
#pragma warning(disable : 26451)
      const auto kernel = feature->kernels + bona_index * sections;
      std::transform(kernel, kernel + sections,
                     buffer + (piece_type - 1) * sections,
                     buffer + (piece_type - 1) * sections,
                     [](const int32_t x, const int32_t y) { return x + y; });
#pragma warning(pop)
#else
      const auto kernel = feature->kernels + index * half_size;
      std::transform(kernel, kernel + half_size, buffer, buffer,
                     [](const int32_t x, const int32_t y) { return x + y; });
#endif // NNUE_LITE
    }
  }

#ifdef NNUE_LITE
  // scaleで割る
  constexpr std::array<int, 7> scale = { 16, 4, 4, 4, 2, 2, 4 };
  constexpr auto n = nn::FeatureEmbedding::BufferSize / 2;
  constexpr auto s = n / 8;
  for (size_t i = 0; i < scale.size(); ++i) {
    for (size_t j = 0; j < s; ++j) {
      results[i * s + j + n * 0] /= scale[i];
      results[i * s + j + n * 1] /= scale[i];
    }
  }
#endif // NNUE_LITE

  
  boost::transform(results, results.begin(),
                   [](const int32_t v) { return std::clamp(v, 0, 127); });

  return results;
}

/**
 * @brief 局面の特徴ベクトルを計算する
 * @param position 
 * @return 
*/
std::vector<int32_t> ComputeEmbedding(const state_action::Position& position) {
  auto feature(std::make_unique<FeatureEmbeddingDummy>());
  std::unique_ptr<NetworkDummy> network;
  Load(feature, network);

  return ComputeEmbedding(position, feature);
}

/**
 * @brief 局面のスコアを計算する
 * @param position 
 * @return 
*/
int32_t ComputeScore(const state_action::Position& position) {
  auto feature(std::make_unique<FeatureEmbeddingDummy>());
  auto network(std::make_unique<NetworkDummy>());
  Load(feature, network);

  const auto embedding = ComputeEmbedding(position, feature);
  const auto results = (*network)(embedding);
  return results[0];
}

class TestEvaluator : public ::testing::Test {
 protected:
  static void SetUpTestCase() {
    utility::InitializeLogger();

    usi::Initialization(usi::options);

    state_action::InitializeBitboards();
    state_action::Position::Initialize();

    evaluation::Initialize();
  }

  static void TearDownTestCase() {}

  virtual void SetUp() { thread = std::make_unique<search::MainThread>(0); }

  state_action::Position position;
  state_action::Rollback rb;
  std::unique_ptr<search::Thread> thread;
};

TEST_F(TestEvaluator, LoadParameter) {
  // パラメータの読み込みでエラーが起きないかどうか

  LoadParameter();

  EXPECT_TRUE(true);
}

TEST_F(TestEvaluator, EmbeddingValue) {
  // Feature Embedding layerの値が正しいか確認する

  LoadParameter();

  position.SetHirate(&rb, thread.get());

  alignas(nn::cache_line_bytes)
    nn::FeatureType embedded_features[nn::FeatureEmbedding::BufferSize];
  nn::feature_embedding->Transform(position, embedded_features, true);

  const std::vector<int32_t> results = ComputeEmbedding(position);

  // 先後同型なので、同じ値
  for (int i = 0; i < 256; ++i) {
    EXPECT_EQ(results[i], results[i + 256]);
    EXPECT_EQ(embedded_features[i], embedded_features[i + 256]);
  }

  for (size_t i = 0; i < nn::FeatureEmbedding::BufferSize; ++i) {
    SCOPED_TRACE(i);

    EXPECT_EQ(static_cast<int32_t>(embedded_features[i]), results[i]);
  }
}

TEST_F(TestEvaluator, NetworkValue) {
  LoadParameter();

  position.SetHirate(&rb, thread.get());

  alignas(nn::cache_line_bytes)
      nn::FeatureType embedded_features[nn::FeatureEmbedding::BufferSize];
  nn::feature_embedding->Transform(position, embedded_features, false);

  alignas(nn::cache_line_bytes) char output_buffer[nn::Network::buffer_size];
  const auto output = (*nn::network)(embedded_features, output_buffer);
  const auto score = std::clamp(static_cast<Value>(output[0] / nn::fv_scale),
                                -Value::MAX, Value::MAX);

  const auto result = ComputeScore(position) / nn::fv_scale;

  EXPECT_EQ(static_cast<int32_t>(score), result);
}

TEST_F(TestEvaluator, EmbeddingValueDelta) {
  // 評価値を差分更新できるか確認

  LoadParameter();

  position.SetHirate(&rb, thread.get());
  // 現局面の特徴ベクトルを計算
  alignas(nn::cache_line_bytes)
    nn::FeatureType embedded_features[nn::FeatureEmbedding::BufferSize];
  nn::feature_embedding->Transform(position, embedded_features, false);

  state_action::Rollback rb2;
  state_action::Move move(Square::_27, Square::_26);
  move = position.ConvertMove16ToMove(move);
  // 1手進める
  position.StepForward(move, rb2, false);

  // 未だ計算していない
  EXPECT_FALSE(rb2.accumulator.computed_accumulation);
  // trueが返ってきて、かつ特徴ベクトルが更新されているはず
  EXPECT_TRUE(nn::feature_embedding->UpdateAccumulatorIfPossible(position));

  // reluの計算だけでベクトルはそのまま
  // NOTE: 内部仕様に依存しすぎ
  nn::feature_embedding->Transform(position, embedded_features, false);

  const std::vector<int32_t> results = ComputeEmbedding(position);
  for (size_t i = 0; i < nn::FeatureEmbedding::BufferSize; ++i) {
    SCOPED_TRACE(i);

    EXPECT_EQ(static_cast<int32_t>(embedded_features[i]), results[i]);
  }
}

}  // namespace
